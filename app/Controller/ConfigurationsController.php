<?php

class ConfigurationsController extends AppController {

	/*
	 * http://fr.slideshare.net/markfabianscherer/cakephp-andajax
	 */

	public function delete($id = null){

		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if($this->Configuration->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}			
		}
	}

	public function save(){
		if ($this->request->is('post') || $this->request->is('put')) {
			//debug($this->request->data);exit;
			foreach($this->request->data['Configuration'] as $config){
				if($this->Configuration->saveAssociated(array(
						'Configuration' => $config,
						'Place' => array('id' => $this->request->data['Place']['id'])
					))){
					$this->Session->setFlash(__('The configurations of the place have been saved.'), 'alert', array('type' => 'success'));
				}
			}
            if($this->request->data['destination'] == 'edit'){
                return $this->redirect(array('controller' => 'places', 'action' => 'edit', $this->request->data['Place']['id']));
            } else {
                return $this->redirect(array('controller' => 'places', 'action' => 'index'));                    
            }
        }
	}

	public function getOption($id, $date, $morning = 0, $evening = 0){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$conditions = array(
				'configuration_id' => $id,
				'date' => $date
			);
			if($morning && !$evening){
				$conditions['morning'] = array();
				array_push($conditions['morning'],1);
			}
			if($evening && !$morning){
				$conditions['evening'] = array();
				array_push($conditions['evening'],1);
			}
			if($morning && $evening){
				$conditions['OR']['morning'] = array();
				$conditions['OR']['evening'] = array();
				array_push($conditions['OR']['morning'],1);
				array_push($conditions['OR']['evening'],1);
			}
			$data = $this->Configuration->Option->find('first', array(
				'conditions' => $conditions,
				'fields' => array('value')
			));
			if(sizeof($data)){
				$this->response->body(json_encode(array('option' => $data['Option']['value'])));
			} else {
				$this->response->body(json_encode(array('option' => 'free')));
			}
		}
	}

	public function setOption($configuration_id, $date, $value, $morning = 0, $evening = 0){

		$this->autoRender = false;
		if($this->request->is('ajax')){
			$conditions = array(
				'configuration_id' => $configuration_id,
				'date' => $date
			);
			$data = $this->Configuration->Option->find('first', array(
				'conditions' => $conditions
			));
			if(sizeof($data)){
				$this->Configuration->Option->id = $data['Option']['id'];
				$this->Configuration->Option->set('value', $value);
				$this->Configuration->Option->save();
			} else {
				$this->Configuration->create();
				$this->Configuration->Option->set('configuration_id', $configuration_id);
				$this->Configuration->Option->set('value', $value);
				$this->Configuration->Option->set('date', $date);
				$this->Configuration->Option->set('morning', $morning);
				$this->Configuration->Option->set('evening', $evening);
				$this->Configuration->Option->save();
			}
			$this->response->body(json_encode(array('success' => 1)));
		}

	}
}