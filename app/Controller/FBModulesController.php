<?php

class FBModulesController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'FBModule' => array(
				'columns' => array(
					'name',
					'fb_category' => array(
						'useField' => false
					),
					'Actions' => null,
				),
				'order' => array('FBCategory.weight' => 'asc', 'FBModule.name' => 'asc'),
				'contain' => array('FBCategory'),
				'fields' => array('FBCategory.name', 'FBModule.id'),
 				'autoData' => false
			)
		)
	);

	public function index() {
		$this->DataTable->setViewVar(array('FBModule'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['FBModule']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['FBModule']['columns']['fb_category']['label'] = __('Category');
	}


	public function add() {
		$fb_categories = $this->FBModule->FBCategory->find('list', array(
			'fields' => array('FBCategory.id', 'FBCategory.name'),
			'order' => 'FBCategory.weight'
		));
		$this->set(compact('fb_categories'));

		if($this->request->is('post') || $this->request->is('put')){
			if($this->FBModule->save($this->request->data)){
				$this->Session->setFlash(__(sprintf('F&B module %s has been saved.', $this->request->data['FBModule']['name'])), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->FBModule->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('F&B module has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function view( $id = null ) {
		return $this->redirect(array('action' => 'edit', $id));
	}
	public function edit( $id = null ) {
		$fb_categories = $this->FBModule->FBCategory->find('list', array(
			'fields' => array('FBCategory.id', 'FBCategory.name'),
			'order' => 'FBCategory.weight'
		));
		$this->set(compact('fb_categories'));

		// Prepare photos and send them to view
		$photos = $this->FBModule->Document->getFiles($id, 'fb_module_photo');
		$docs = $this->FBModule->Document->getFiles($id, 'fb_module_document');
		$documents = array();
		$documents['photos'] = $photos;
		$documents['documents'] = $docs;
		$this->set(compact('documents'));

		if($this->request->is('post') || $this->request->is('put')){
			if(!empty($this->request->data['PlanningBoardMoment'])){
				foreach($this->request->data['PlanningBoardMoment'] as $k => $moment){
					if(empty($moment['name'])){
						unset($this->request->data['PlanningBoardMoment'][$k]);
						if(!empty($moment['id'])){
							$this->FBModule->PlanningBoardMoment->delete($moment['id']);
						}
					}
				}
			}
			if($this->FBModule->saveAssociated($this->request->data)){
				$this->Session->setFlash(__(sprintf('F&B module %s has been saved.', $this->request->data['FBModule']['name'])), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->FBModule->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('F&B module has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			$this->FBModule->contain('PlanningBoardMoment');
			$fbModule = $this->FBModule->findById($id);
			$this->request->data = $fbModule;
		}
	}

	public function delete($id = null){
		if(!empty($id) && $this->FBModule->delete($id)){
			$this->Session->setFlash(__(sprintf('F&B module has been deleted.')), 'alert', array('type' => 'success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('F&B module has not been deleted. Please try again.'), 'alert', array('type' => 'danger'));
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function updateNonStandardName(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$eventFBModule = $this->FBModule->EventFBModule->findById($this->request->data['id']);
			if(!empty($eventFBModule)){
				$eventFBModule['EventFBModule']['name'] = $this->request->data['name'];
				if($this->FBModule->EventFBModule->save($eventFBModule, array('callbacks' => false))){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

	public function updateInfoField(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$em = $this->FBModule->EventFBModule->findById($this->request->data['id']);
			if(!empty($em)){
				$em['EventFBModule'][$this->request->data['field']] = $this->request->data['value'];
				if($this->FBModule->EventFBModule->save($em, array('callbacks' => false))){
					$this->FBModule->OrderItem->updateQuantities(array(
						'order_model' => 'event',
						'order_model_id' => $em['EventFBModule']['event_id'],
						'item_model' => 'fb_module',
						'item_model_id' => $em['EventFBModule']['fb_module_id']
					));
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

}
