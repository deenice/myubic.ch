<?php

class OrderItemsController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$numberOfItems = $this->OrderItem->find('count', array(
				'conditions' => array(
					'order_group_id' => empty($this->request->params['named']['order_group_id']) ? $this->request->data['order_group_id'] : $this->request->params['named']['order_group_id']
				)
			));
			if(!empty($this->request->data['stock_item_id'])){
				$stockitem = $this->OrderItem->StockItem->findById($this->request->data['stock_item_id']);
				$item = array(
					'OrderItem' => array(
						'quantity_option' => 'fixed',
						'order_group_id' => $this->request->data['order_group_id'],
						'stock_item_id' => $this->request->data['stock_item_id'],
						'name' => $stockitem['StockItem']['name'],
						'stock_location_id' => $stockitem['StockItem']['stock_location_id'],
						'supplier_id' => !empty($stockitem['StockItem']['supplier_id']) ? $stockitem['StockItem']['supplier_id'] : '',
						'default_quantity' => 1,
						'quantity' => '',
						'weight' => empty($numberOfItems) ? 0 : $numberOfItems
					)
				);
			} else {
				$item = array(
					'OrderItem' => array(
						'quantity_option' => 'fixed',
						'order_group_id' => empty($this->request->params['named']['order_group_id']) ? null : $this->request->params['named']['order_group_id'],
						'name' => '',
						'default_quantity' => 1,
						'quantity' => '',
						'weight' => empty($numberOfItems) ? 0 : $numberOfItems
					)
				);
			}
			if($this->OrderItem->save($item)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function update(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data)){
				if(!empty($this->request->data['OrderItem']['quantity_option']) && $this->request->data['OrderItem']['quantity_option'] != 'quantity_per_person'){
					$this->request->data['OrderItem']['quantity_unit'] = null;
				}
				if($this->OrderItem->save($this->request->data)){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['items'])){
				foreach($this->request->data['items'] as $weight => $item){
					$this->OrderItem->id = $item;
					$this->OrderItem->saveField('weight', $weight, array('callbacks' => false));
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->OrderItem->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateField(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->OrderItem->id = $this->request->data['id'];
			if($this->OrderItem->saveField($this->request->data['field'], $this->request->data['value'], array('callbacks' => false))){
				$this->OrderItem->updateUnavailability();
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	// public function update_unavailability($id = ''){
	// 	if($this->request->is('ajax')){
	// 		$this->autoRender = false;
	// 		$this->OrderItem->id = empty($this->request->data['id']) ? $id : $this->request->data['id'];
	// 		if($this->OrderItem->updateUnavailability()){
	// 			$this->response->body(json_encode(array('success' => 1)));
	// 		} else {
	// 			$this->response->body(json_encode(array('success' => 0)));
	// 		}
	// 	}
	//
	// }

}
