<?php

class MyToolsController extends AppController {

	public function server(){
		print_r($_SERVER);
		exit;
	}

	public function getTravelTime($origin = '', $destination = ''){

		function file_get_contents_curl($url) {
		    $ch = curl_init();

		    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

		    $data = curl_exec($ch);
		    curl_close($ch);

		    return $data;
		}

		if($this->request->is('ajax')){
			$this->autoRender = false;

			$destination = str_replace(' ', '+', $destination);
			$origin = str_replace(' ', '+', $origin);

            $this->loadModel('Direction');
            $data = $this->Direction->find('first', array(
                'conditions' => array(
                    'origin' => $origin,
                    'destination' => $destination
                )
            ));

            if(!empty($data)){
            	$distance = round( ($data['Direction']['value'] / 1000), 2);
                $infos['time'] = $data['Direction']['time'];
                $infos['distance'] = $distance;
                $infos['end_address'] = $data['Direction']['end_address'];
            } else {
                $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey');
                $travel = json_decode(file_get_contents_curl($url), true);
                if($travel['status'] == 'OK'){
                    $infos['distance'] = $travel['routes'][0]['legs'][0]['distance']['text'];
                    $infos['time'] = $travel['routes'][0]['legs'][0]['duration']['text'];
                    $infos['end_address'] = $travel['routes'][0]['legs'][0]['end_address'];
                    $this->Direction->create();
                    $direction = array(
                        'Direction' => array(
                            'origin' => $origin,
                            'destination' => $destination,
                            'time' => $infos['time'],
                            'end_address' => $infos['end_address'],
                            'distance' => $travel['routes'][0]['legs'][0]['distance']['text'],
                            'value' => $travel['routes'][0]['legs'][0]['distance']['value']
                        )
                    );
                    $this->Direction->save($direction);
                } else {
                    $infos['time'] = 0;
                    $infos['distance'] = 0;
                    $infos['travel'] = $travel;
                }
            }
            $this->response->body(json_encode($infos));
		}
	}

	public function test(){
		App::uses('CakeEmail', 'Network/Email');

		$Email = new CakeEmail('festiloc');
		$Email->from(array('denis@une-bonne-idee.ch' => 'Denis'));
		$Email->to(array('denis.bossy@unifr.ch', 'denis.bossy@gmail.com'));
		$Email->subject('About');
		$Email->send('My message');


		exit;
		$to      = 'denis.bossy@unifr.ch';
		$subject = 'le sujet';
		$message = 'Bonjour !';
		$headers = 'From: info@festiloc.ch' . "\r\n" .
		'Reply-To: info@festiloc.ch' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();

		print_r(mail($to, $subject, $message, $headers));

		exit;
	}

	public function verify_email($toemail, $fromemail, $getdetails = false){
		$this->autoRender = false;
		$details = '';
		$email_arr = explode("@", $toemail);
		$domain = array_slice($email_arr, -1);
		$domain = $domain[0];
		// Trim [ and ] from beginning and end of domain string, respectively
		$domain = ltrim($domain, "[");
		$domain = rtrim($domain, "]");
		if( "IPv6:" == substr($domain, 0, strlen("IPv6:")) ) {
			$domain = substr($domain, strlen("IPv6") + 1);
		}
		$mxhosts = array();
		if( filter_var($domain, FILTER_VALIDATE_IP) )
			$mx_ip = $domain;
		else
			getmxrr($domain, $mxhosts, $mxweight);
		if(!empty($mxhosts) )
			$mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
		else {
			if( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
				$record_a = dns_get_record($domain, DNS_A);
			}
			elseif( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
				$record_a = dns_get_record($domain, DNS_AAAA);
			}
			if( !empty($record_a) )
				$mx_ip = $record_a[0]['ip'];
			else {
				$result   = "invalid";
				$details .= "No suitable MX records found.";
				return ( (true == $getdetails) ? array($result, $details) : $result );
			}
		}

		$connect = @fsockopen($mx_ip, 25);
		if($connect){
			if(preg_match("/^220/i", $out = fgets($connect, 1024))){
				fputs ($connect , "HELO $mx_ip\r\n");
				$out = fgets ($connect, 1024);
				$details .= $out."\n";

				fputs ($connect , "MAIL FROM: <$fromemail>\r\n");
				$from = fgets ($connect, 1024);
				$details .= $from."\n";
				fputs ($connect , "RCPT TO: <$toemail>\r\n");
				$to = fgets ($connect, 1024);
				$details .= $to."\n";
				fputs ($connect , "QUIT");
				fclose($connect);
				if(!preg_match("/^250/i", $from) || !preg_match("/^250/i", $to)){
					$result = "invalid";
				}
				else{
					$result = "valid";
				}
			}
		}
		else{
			$result = "invalid";
			$details .= "Could not connect to server";
		}
		if($getdetails){
			$output = array($result, $details);
		}
		else{
			$output = array($result);
		}

		if($this->request->is('ajax')){
			$this->response->body(json_encode($output));
		} else {
			return $output;
		}
	}


}
