<?php 

class ModulesController extends AppController {

	public function index(){
		$this->set('modules', $this->Module->find('all', array(
			'contain' => array(
				'ModuleCategory',
				'ModuleSubcategory'
			),
			'order' => 'Module.name'
		)));
	}

    public function add(){

        $categories = $this->Module->ModuleCategory->find('list');
        $this->set(compact('categories'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Module->create();
            if ($this->Module->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('Module has been saved.'), 'alert', array('type' => 'success'));
                if($this->request->data['destination'] == 'edit'){
                    return $this->redirect(array('action' => 'edit', $this->Module->id));
                } else {
                    return $this->redirect(array('action' => 'index'));                    
                }
            } else {
                $this->Session->setFlash(__('Module has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
        }

    }

    public function edit($id = null){

        $this->Module->contain(array('Document' => array('User')));
        $module = $this->Module->read(null, $id);

        $categories = $this->Module->ModuleCategory->find('list');
        $this->set(compact('categories'));

        $subcategories = $this->Module->ModuleSubcategory->find('list', array(
            'conditions' => array(
                'module_category_id' => $module['Module']['module_category_id']
            )
        ));
        $this->set(compact('subcategories'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Module->create();
            if ($this->Module->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('Module has been saved.'), 'alert', array('type' => 'success'));
                if($this->request->data['destination'] == 'edit'){
                    return $this->redirect(array('action' => 'edit', $this->Module->id));
                } else {
                    return $this->redirect(array('action' => 'index'));                    
                }
            } else {
                $this->Session->setFlash(__('Module has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
        } else {
            $this->request->data = $module;
        }

    }

    public function view($id = null){
        $this->Module->contain('User');
        $this->set('module', $this->Module->read(null, $id));
        $users = $this->Module->ModuleUser->find('all', array(
            'joins' => array(
                // array(
                //     'table' => 'modules_users',
                //     'alias' => 'ModuleUser',
                //     'conditions' => array(
                //         'ModuleUser.module_id = Module.id'
                //     )
                // ),
                array(
                    'table' => 'users',
                    'alias' => 'User1',
                    'conditions' => array(
                        'User1.id = ModuleUser.user_id'
                    )
                )
            ),
            'conditions' => array(
                'ModuleUser.module_id' => $id,
                'User.role IN' => array('fixed', 'temporary', 'candidate')
            ),
            'order' => 'User1.first_name',
            'group' => 'ModuleUser.user_id',
            'contain' => 'User'
        ));
        $this->set(compact('users'));

        $users1 = $this->User->find('all', array(
            'conditions' => array(
                'User.role' => 'temporary'
            ),
            'contain' => array(
                'ModuleUser'
            ),
            'order' => 'User.full_name'
        ));
        foreach($users1 as $k => $user){
            foreach($user['ModuleUser'] as $module){
                if(!empty($module)){
                    if($module['module_id'] != $id){
                        continue;
                    }
                    if($module['module_id'] == $id){
                        unset($users1[$k]);
                    }
                }
                
            }
        }
        //sort($users2);
        $this->set(compact('users1'));
    }

	public function getSubcategories(){
    	$this->autoRender = false;
        if ($this->request->is('ajax')) {
        	$subcategories = $this->Module->ModuleSubcategory->find('list', array(
	    		'conditions' => array(
	    			'module_category_id' => $_POST['module_category_id']
	    		),
	    		'fields' => array(
	    			'ModuleSubcategory.id', 'ModuleSubcategory.name'
	    		)
	    	));
	    	$this->response->body(json_encode($subcategories)); 
        }    	
    }

    public function getStatus( $value ){
    	$this->autoRender = false;
    	if($this->request->is('ajax')){
    		$moduleUser = $this->Module->ModuleUser->find('first', array(
    			'conditions' => array(
    				'module_id' => $_POST['module_id'],
    				'user_id' => $_POST['user_id'],
                    'value IN' => array($value, 'not_' . $value)
    			)
    		));
    		if(!empty($moduleUser)){
    			$output['checked'] = true;
                $output['ModuleUserId'] = $moduleUser['ModuleUser']['id'];
    			$output['value'] = $moduleUser['ModuleUser']['value'];
    		} else {
    			$output['checked'] = false;
    		}
    		$this->response->body(json_encode($output));
    	}
    }

    public function changeStatus( $value ){
    	$this->autoRender = false;
        if ($this->request->is('ajax')) {
        	$this->response->body(json_encode($_POST)); 
        	if($_POST['status'] == "true"){
        		if($this->Module->ModuleUser->save(array(
        			'ModuleUser' => array(
                        'id' => empty($_POST['module_user_id']) ? '' : $_POST['module_user_id'],
        				'user_id' => $_POST['user_id'],
        				'module_id' => $_POST['module_id'],
                        'value' => $value
        			)
        		))){
        			$output['success'] = 1;
        			$output['ModuleUserId'] = $this->Module->ModuleUser->id;
        		} else {
        			$output['success'] = 0;
        		}
        	} else {
        		if($this->Module->ModuleUser->delete($_POST['module_user_id'])){
        			$output['success'] = 1;
        		} else {
        			$output['success'] = 0;
        		}
        	}
	    	$this->response->body(json_encode($output)); 
        } 
    }

}