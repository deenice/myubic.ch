<?php

class MyjeuController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Filters' => [
				'model' => 'Myjeu',
				'contain' => array('Tag'),
				'joins' => array(
					array(
						'table' => 'myjeu_tags',
						'alias' => 'mt',
						'type' => 'left',
						'conditions' => array('Myjeu.id = mt.myjeu_id')
					)
				),
				'columns' => [
					'name',
					'pax' => array(
						'useField' => false
					),
					'tags' => array(
						'useField' => false
					),
					'Actions' => null
				],
				'fields' => array('Myjeu.id', 'Myjeu.min_pax', 'Myjeu.max_pax'),
				'order' => array('name'),
				'autoData' => false,
				'group' => 'Myjeu.id'
			],
		],
	];

	public function index(){

		$this->Session->write('MyjeuSearch.pax', empty($this->request->data['MyjeuSearch']['pax']) ? null : $this->request->data['MyjeuSearch']['pax']);
		$this->Session->write('MyjeuSearch.tags', empty($this->request->data['MyjeuSearch']['tags']) ? null : serialize($this->request->data['MyjeuSearch']['tags']));
		$this->DataTable->setViewVar(array('Filters'));

		$tags = $this->Myjeu->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'myjeu')));
		$this->set(compact('tags'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Filters']['columns']['name']['label'] = __('Name');
	}

	public function add(){
		$tags = $this->Myjeu->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'myjeu')));
		$this->set(compact('tags'));
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Myjeu->create();
			if ($this->Myjeu->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Myjeu has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Myjeu->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Artist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function edit( $id = null ){

		$this->Myjeu->id = $id;
		$this->Myjeu->contain('Tag');

		if (!$this->Myjeu->exists()) {
			throw new NotFoundException(__('Invalid Myjeu'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Myjeu->create();
			if ($this->Myjeu->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Myjeu has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Myjeu->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Myjeu has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$myjeu = $this->Myjeu->findById($id);
			$tags = $this->Myjeu->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'myjeu')));
			$this->set(compact('tags'));
			$this->set(compact('myjeu'));
			$this->request->data = $myjeu;
			$myjeutags = array();
			if(!empty($myjeu['Tag'])){
				foreach($myjeu['Tag'] as $tag){
					$myjeutags[] = $tag['id'];
				}
			}
			$this->set(compact('myjeutags'));

			// Prepare photos and send them to view
			$images = $this->Myjeu->Document->getFiles($id, 'myjeu_image');
			$docs = $this->Myjeu->Document->getFiles($id, 'myjeu_document');
			$documents = array();
			$documents['documents'] = $docs;
			$documents['images'] = $images;

			$this->set(compact('documents'));
		}
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->Myjeu->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		} else {
			if($this->Myjeu->delete($id)){
				$this->Session->setFlash(__('Myjeu has been deleted.'), 'alert', array('type' => 'success'));
			} else {
				$this->Session->setFlash(__('Myjeu has not been deleted.'), 'alert', array('type' => 'warning'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function filters( ){
		$conditions = array();
		$pax = $this->Session->read('MyjeuSearch.pax');
		$tags = $this->Session->read('MyjeuSearch.tags');
		if(!empty($pax)){
			$conditions[] = array(
				'Myjeu.min_pax <=' => $pax,
				'Myjeu.max_pax >=' => $pax
			);
		}
		if(!empty($tags)){
			$tags = unserialize($tags);
			$conditions[] = array(
				'mt.tag_id' => $tags
			);
		}
		$this->DataTable->paginate(null, $conditions);
	}

}
