<?php

class JobsController extends AppController {

	public function redoAssociations(){
		exit;
		$jobs = $this->Job->find('all', array(
			'contain' => array(
				'User'
			)
		));
		foreach($jobs as $job){
			if(!$job['Job']['user_id']){
				continue;
			}
			$user = $this->Job->User->findById($job['Job']['user_id']);
			$job['User'] = $user;
			$job['Job']['user_id'] = null;
			$this->Job->create();
			$this->Job->saveAssociated($job);
		}
		exit;
	}

	public function save(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {

			if(empty($_POST['amount'])){
				$amount = 1;
			} else {
				$amount = $_POST['amount'];
			}
			$i=0;
			$output = array();
			while($i < $amount && $i >= 0){
				$data = array(
					'Job' => array(
						'id' => !empty($_POST['id']) ? $_POST['id'] : '',
						'user_id' => !empty($_POST['worker_id']) ? $_POST['worker_id'] : '',
						'event_id' => !empty($_POST['event_id']) ? $_POST['event_id'] : '',
						'sector' => $_POST['sector'],
						'hierarchy' => $_POST['hierarchy'],
						'activity_id' => !empty($_POST['activity']) ? $_POST['activity'] : '',
						'job' => $_POST['job'],
						'name' => $_POST['name'],
						'start_time' => !empty($_POST['start_time']) ? $_POST['start_time'] : '',
						'end_time' => !empty($_POST['end_time']) ? $_POST['end_time'] : '',
						//'salary' => !empty($_POST['salary']) ? $_POST['salary'] : '',
						'languages' => !empty($_POST['languages']) ? implode(',',$_POST['languages']) : '',
						'remarks' => !empty($_POST['remarks']) ? $_POST['remarks'] : ''
					)
				);
				if(!empty($_POST['id'])){
					$this->Job->create();
				} else {
					$this->Job->id = $_POST['id'];
				}
				if($this->Job->save($data)){
					$output['success'] = 1;
					$output['id'] = $this->Job->id;
				} else {
					$output['success'] = 0;
				}
				$i++;
			}
			$this->response->body(json_encode($output));
		}
	}

	public function getStaff(){
		if($this->request->is('ajax')){

			$this->Job->contain(array('Round'));
			$job = $this->Job->findById($this->request->data['jobId']);
			if(empty($job['Round'])){
				$this->Job->save($job);
				$job = $this->Job->findById($this->request->data['jobId']);
			}
			$event = $this->Job->Event->findById($this->request->data['eventId']);
			$this->Job->Round->contain(array('RoundStep'));
			$round = $this->Job->Round->findByJobId($job['Job']['id']);

			$day = date('N', strtotime($event['Event']['confirmed_date']));

			$availabilitiesTagsByDay = Configure::read('Users.availabilitiesTagsByDay');
			$confirmed_day = strtolower(date('l', strtotime($event['Event']['confirmed_date'])));
			$availabilitiesTags = $availabilitiesTagsByDay[$confirmed_day];
			$options = array();

			$options['joins'] = array(
				array('table' => 'tags_users',
					'alias' => 'TagsUsers1',
					'conditions' => array(
						'TagsUsers1.user_id = User.id'
					)
				),
				array('table' => 'tags',
					'alias' => 'Section',
					'type' => 'LEFT',
					'conditions' => array(
						'Section.id = TagsUsers1.tag_id',
						'Section.category = "section"'
					)
				),
				array('table' => 'competences',
					'alias' => 'Competence',
					'conditions' => array(
						'Competence.user_id = User.id'
					)
				)
			);

			$options['group'] = array('User.id');

			$options['contain'] = array(
				'ScheduleDay' => array(
					'conditions' => array(
						'ScheduleDay.day' => $day
					),
					'order' => 'ScheduleDay.hour'
				),
				//'Job' => array('Event'),
				//'Talents',
				'Portrait',
				'Competence' => array(
					'conditions' => array(
						'Competence.job' => $job['Job']['job'],
						'Competence.sector' => $job['Job']['sector'],
						'Competence.activity_id' => empty($job['Job']['activity_id']) ? null : $job['Job']['activity_id']
					)
				)
			);

			//http://stackoverflow.com/questions/1751686/conditions-in-associated-models-using-model-find-cakephp
			// General conditions
			$options['conditions'] = array();
			$options['conditions'][] = array('User.role IN' => array('fixed', 'temporary', 'candidate'));
			$options['conditions'][] = array(
				'OR' => array(
					'User.end_of_collaboration >=' => $event['Event']['confirmed_date'],
					'User.end_of_collaboration' => null
				)
			);
			// if(!empty($availabilitiesTags)){
			// 		$options['conditions'][] = array('Availabilities.id IN' => $availabilitiesTags);
			// }
			if(!empty($job['Job']['sector'])){
				if($job['Job']['sector'] == 'animation'){
					$sectorId = 173;
				} elseif($job['Job']['sector'] == 'logistics'){
					$sectorId = 174;
				} elseif($job['Job']['sector'] == 'fb'){
					$sectorId = 175;
				}
				$options['conditions'][] = array(
					'OR' => array(
						'Section.id' => $sectorId,
						'Competence.sector' => $job['Job']['sector']
					)
				);
			}
			if(!empty($job['Job']['job'])){
				if($job['Job']['job'] == 'event_assistant'){
          // event managers should be retrieved for event assistant jobs
					$options['conditions'][] = array(
						'OR' => array(
							array('Competence.job' => $job['Job']['job']),
							array('Competence.job' => 'event_manager')
						)
					);
				} elseif($job['Job']['job'] == 'service_crew'){
          // test extras should be retrieved for service crew jobs
					$options['conditions'][] = array(
						'OR' => array(
							array('Competence.job' => $job['Job']['job']),
							array('Competence.job' => 'test')
						)
					);
				} else {
					$options['conditions'][] = array('Competence.job' => $job['Job']['job']);
				}
			}
			if(!empty($job['Job']['hierarchy'])){
				if($job['Job']['hierarchy'] <= 2){
					$options['conditions'][] = array('Competence.hierarchy >=' => 1);
				} else {
					$options['conditions'][] = array('Competence.hierarchy >=' => $job['Job']['hierarchy']);
				}
			}
			if(!empty($job['Job']['activity_id'])){
				$options['conditions'][] = array('Competence.activity_id' => $job['Job']['activity_id']);
			}
			if(!empty($job['Job']['fb_module_id'])){
				$options['conditions'][] = array('Competence.fb_module_id' => $job['Job']['fb_module_id']);
			}
			if(!empty($job['Job']['languages'])){
				$jobLanguages = explode(',', $job['Job']['languages']);
				foreach($jobLanguages as $lang){
					if($lang == 'fr'){
						$options['conditions'][] = array('User.french_level >= "b2"');
					}
					if($lang == 'de'){
						$options['conditions'][] = array('User.german_level >= "b2"');
					}
					if($lang == 'en'){
						$options['conditions'][] = array('User.english_level >= "b2"');
					}
				}
			}
			if(!empty($this->request->data['Search']['french_level'])){
				$options['conditions'][] = array('User.french_level >=' => $this->request->data['Search']['french_level']);
			}
			if(!empty($this->request->data['Search']['german_level'])){
				$options['conditions'][] = array('User.german_level >=' => $this->request->data['Search']['german_level']);
			}
			if(!empty($this->request->data['Search']['english_level'])){
				$options['conditions'][] = array('User.english_level >=' => $this->request->data['Search']['english_level']);
			}
			$users = $this->Job->JobUser->User->find('all', $options);

			foreach($users as $k => $user){
				$this->Job->JobUser->User->id = $user['User']['id'];
				$enrolled = $this->Job->JobUser->User->isEnrolledOnJob($job['Job']['id']);
				$availability = $this->Job->JobUser->User->getAvailability($job['Job']['id']);

				if($availability['availability'] == 'yes'){
					$key = $k + 1000;
				}
				elseif($availability['availability'] == 'maybe'){
					$key = $k + 2000;
				}
				elseif($availability['availability'] == 'no'){
					$key = $k + 3000;
				}
				elseif($availability['availability'] == 'unknown'){
					$key = $k + 4000;
				}
				if(!empty($user['Competence'][0]['hierarchy'])){
					$key = $key + 100 * (5 - $user['Competence'][0]['hierarchy']);
				}
				$users[$key] = $users[$k];
				$users[$key]['availability'] = $availability;
				$users[$key]['enrolled'] = $enrolled;
				$users[$key]['answers'] = array();
				if(!empty($round)){
					$this->Job->Round->id = $round['Round']['id'];
					$users[$key]['answers'] = $this->Job->Round->getAnswersByUser($user['User']['id']);
				}
				unset($users[$k]);
			}
			ksort($users);
			$this->set(compact('users'));
			$this->set(compact('job'));
			$this->set(compact('event'));
			$this->set(compact('round'));
			$this->set('hierarchies', Configure::read('Competences.hierarchies'));
		}
	}

	public function modal( $id = '', $event_id = '' ){
		$this->Job->contain(array(
			'User',
			'Outfit'
		));

		$activities = $this->Job->Event->Activity->find('list', array(
			'conditions' => array(
				'Activity.place_id' => null,
			),
			'contain' => array('Company'),
			'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
		));
		$this->set(compact('activities'));

		$fbmodules = $this->Job->Event->FBModule->find('list', array(
			'conditions' => array('FBModule.with_competences' => 1),
			'fields' => array('FBModule.id', 'FBModule.name')
		));
		$this->set(compact('fbmodules'));

		$users = $this->Job->User->find('list', array(
			'joins' => array(
				array(
					'table' => 'jobs_users',
					'alias' => 'ju',
					'type' => 'left',
					'conditions' => array(
						'ju.user_id = User.id'
					)
				)
			),
			'conditions' => array(
				'User.first_name <>' => '',
				'OR' => array(
					array('User.role' => array('fixed', 'temporary')),
					array(
						'User.role' => array('retired'),
						'ju.job_id' => $id
					)
				)
			),
			'order' => array('User.first_name'),
			'fields' => array('User.id', 'User.full_name')
		));
		$this->set(compact('users'));

		if(!empty($event_id)){
			$event = $this->Job->Event->findById($event_id);
			$this->set(compact('event'));
		}


		if(!empty($id)){
			$job = $this->Job->findById($id);

			$job['user_ids'] = array();
			if(!empty($job['User'])){
				$userIds = array();
				foreach($job['User'] as $user){
					$userIds[] = $user['id'];
				}
				$job['user_ids'] = $userIds;
			}
			$this->set(compact('job'));
		}
	}

	public function edit( $id = '' ){
		$animationJobs = Configure::read('Competences.animation_jobs');
		$logisticsJobs = Configure::read('Competences.logistics_jobs');
		$fbJobs = Configure::read('Competences.fb_jobs');
		$hierarchies = Configure::read('Competences.hierarchies');

		if($this->request->is('ajax')){
			$this->autoRender = false;

			if(!empty($this->request->data['Job']['languages'])){
				if(is_array($this->request->data['Job']['languages'])){
					$this->request->data['Job']['languages'] = implode(',', $this->request->data['Job']['languages']);
				}
			}
			$name = '';
			if(!empty($this->request->data['Job']['job1'])){
				$name = $fbJobs[$this->request->data['Job']['job1']];
				$this->request->data['Job']['job'] = $this->request->data['Job']['job1'];
			}
			if(!empty($this->request->data['Job']['job2'])){
				$name = $logisticsJobs[$this->request->data['Job']['job2']];
				$this->request->data['Job']['job'] = $this->request->data['Job']['job2'];
			}
			if(!empty($this->request->data['Job']['job3'])){
				$name = $animationJobs[$this->request->data['Job']['job3']];
				$this->request->data['Job']['job'] = $this->request->data['Job']['job3'];
			}
			if(!empty($this->request->data['Job']['job4'])){
				$name = $animationJobs[$this->request->data['Job']['job4']];
				$this->request->data['Job']['job'] = $this->request->data['Job']['job4'];
			}

			if(!empty($this->request->data['Job']['activity_id'])){
				$activity = $this->Job->Activity->findById($this->request->data['Job']['activity_id']);
				$name .= ' ' . $activity['Activity']['name'];
			}

			if(!empty($this->request->data['Job']['fb_module_id'])){
				$module = $this->Job->FBModule->findById($this->request->data['Job']['fb_module_id']);
				$name .= ' ' . $module['FBModule']['name'];
			}

			if(!empty($this->request->data['Job']['hierarchy'])){
				$name .= ' ' . $hierarchies[$this->request->data['Job']['hierarchy']];
			}

			$jobusers = $this->Job->JobUser->find('list', array(
				'conditions' => array('JobUser.job_id' => $id),
				'fields' => array('user_id', 'id')
			));

			if(!empty($this->request->data['Job']['user_ids'])){
				foreach($this->request->data['Job']['user_ids'] as $k => $userId){
					if(isset($jobusers[$userId])){
						unset($jobusers[$userId]);
					}
					$ju = $this->Job->JobUser->find('first', array(
						'conditions' => array(
							'job_id' => $id,
							'user_id' => $userId
						)
					));
					if(!empty($ju)){
						$this->request->data['JobUser'][$k] = array(
							'id' => $ju['JobUser']['id'],
							'job_id' => $id,
							'user_id' => $userId
						);
					} else {
						$this->request->data['JobUser'][$k] = array(
							'job_id' => $id,
							'user_id' => $userId
						);
					}
				}
			} else {
				$this->request->data['User'] = array();
				$this->request->data['JobUser'] = array();
			}

			if(!empty($jobusers)){
				foreach($jobusers as $user => $id){
					$this->Job->JobUser->delete($id);
				}
			}

			if(!empty($id)){
				$this->Job->UserUnavailability->deleteAll(array(
					'model_id' => $id,
					'model' => 'job'
				));
			}

			if(!empty($this->request->data['Job']['default_outfit_id'])){
				$this->request->data['Job']['outfit_id'] = $this->request->data['Job']['default_outfit_id'];
			}

			if(!empty($this->request->data['Job']['activity_outfit_id'])){
				$this->request->data['Job']['outfit_id'] = $this->request->data['Job']['activity_outfit_id'];
			}

			if(!empty($this->request->data['Job']['fb_module_outfit_id'])){
				$this->request->data['Job']['outfit_id'] = $this->request->data['Job']['fb_module_outfit_id'];
			}

			$this->request->data['Job']['name'] = $name;
			if($this->Job->saveAssociated($this->request->data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		}
	}

	public function delete(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if($this->Job->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function save_route(){
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			if(!empty($this->request->data['id'])){
				$this->Job->JobUser->id = $this->request->data['id'];
				if($this->Job->JobUser->saveField($this->request->data['field'], $this->request->data['value'])){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}
}
