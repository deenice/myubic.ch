<?php

App::import("Vendor", "phpqrcode", array('file' => "phpqrcode/phpqrcode.php"));

class StockItemsController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Online' => [
				'model' => 'StockItem',
				'columns' => [
					'image' => array(
						'label' => false,
						'useField' => false,
						'bSortable' => false,
						'bSearchable' => false
					),
					'code',
					'name' => array(
						'bSortable' => false
					),
					'price' => array(
						'useField' => false
					),
					'category' => array(
						'useField' => false
					),
					'section' => array(
						'useField' => false
					),
					'family' => array(
						'useField' => false
					),
					'quantity' => array(
						'bSortable' => false
					),
					'Actions' => null
				],
				'conditions' => array(
					'StockItem.online' => 1,
					'StockItem.archived' => 0,
					'StockItem.company_id' => 3
				),
				'contain' => array(
					'Document',
					'StockCategory',
					'StockSection',
					'StockFamily'
				),
				'order' => array(
					'StockItem.code' => 'ASC',
					'StockCategory.code' => 'ASC'
				),
				'fields' => array(
					'StockItem.id', 'StockCategory.name', 'StockSection.name', 'StockFamily.name', 'StockItem.price'
				),
				'autoData' => false
			],
			'ToCheck' => [
				'model' => 'StockItem',
				'columns' => [
					'image' => array(
						'label' => false,
						'useField' => false,
						'bSortable' => false,
						'bSearchable' => false
					),
					'code',
					'name' => array(
						'bSortable' => false
					),
					'price' => array(
						'useField' => false
					),
					'category' => array(
						'useField' => false
					),
					'section' => array(
						'useField' => false
					),
					'family' => array(
						'useField' => false
					),
					'quantity' => array(
						'bSortable' => false
					),
					'Actions' => null
				],
				'conditions' => array(
					'StockItem.online' => 1,
					'StockItem.archived' => 0,
					'StockItem.to_process' => 'to_check',
					'StockItem.company_id' => 3
				),
				'contain' => array(
					'Document',
					'StockCategory',
					'StockSection',
					'StockFamily'
				),
				'order' => array(
					'StockItem.code' => 'ASC',
					'StockCategory.code' => 'ASC'
				),
				'fields' => array(
					'StockItem.id', 'StockCategory.name', 'StockSection.name', 'StockFamily.name', 'StockItem.price'
				),
				'autoData' => false
			],
			'Offline' => [
				'model' => 'StockItem',
				'columns' => [
					'image' => array(
						'label' => false,
						'useField' => false,
						'bSortable' => false,
						'bSearchable' => false
					),
					'code',
					'name' => array(
						'bSortable' => false
					),
					'category' => array(
						'useField' => false
					),
					'section' => array(
						'useField' => false
					),
					'family' => array(
						'useField' => false
					),
					'quantity' => array(
						'bSortable' => false
					),
					'Actions' => null
				],
				'conditions' => array(
					'StockItem.online' => 0,
					'StockItem.archived' => 0,
					'StockItem.company_id' => 3
				),
				'contain' => array(
					'Document',
					'StockCategory',
					'StockSection',
					'StockFamily'
				),
				'order' => array(
					'StockCategory.code' => 'ASC',
					'StockItem.code' => 'ASC'
				),
				'fields' => array(
					'StockItem.id', 'StockCategory.name', 'StockSection.name', 'StockFamily.name'
				),
				'autoData' => false
			],
			'byCompany' => array(
				'model' => 'StockItem',
				'contain' => array(
					'Document',
					'StockCategory',
					'StockSection',
					'StockFamily'
				),
				'columns' => array(
					'image' => array(
						'label' => false,
						'useField' => false,
						'bSortable' => false,
						'bSearchable' => false
					),
					'code',
					'name' => array(
						'bSortable' => false
					),
					'StockCategory.name' => array(
						'bSortable' => false,
						//'bSearchable' => 'customSearchStockItem'
					),
					'StockSection.name' => array(
						'bSortable' => false,
						//'bSearchable' => 'customSearchStockItem'
					),
					'StockFamily.name' => array(
						'bSortable' => false,
						//'bSearchable' => 'customSearchStockItem'
					),
					'quantity' => array(
						'bSortable' => false
					),
					'Actions' => null
				),
				'order' => array(
					'StockCategory.code' => 'ASC',
					'StockItem.code' => 'ASC'
				),
				'fields' => array(
					'StockItem.id', 'StockCategory.name', 'StockSection.name', 'StockFamily.name'
				),
				'conditions' => array(
					'StockItem.archived' => 0
				),
				'autoData' => false
			)
		]
	];

	public function beforeFilter(){
		parent::beforeFilter();
		$this->DataTable->settings['Online']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Online']['columns']['category']['label'] = __('Category');
		$this->DataTable->settings['Online']['columns']['section']['label'] = __('Section');
		$this->DataTable->settings['Online']['columns']['family']['label'] = __('Family');
		$this->DataTable->settings['Online']['columns']['quantity']['label'] = __('Quantity');
		$this->DataTable->settings['Online']['columns']['price']['label'] = __('Price');
		$this->DataTable->settings['ToCheck']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['ToCheck']['columns']['category']['label'] = __('Category');
		$this->DataTable->settings['ToCheck']['columns']['section']['label'] = __('Section');
		$this->DataTable->settings['ToCheck']['columns']['family']['label'] = __('Family');
		$this->DataTable->settings['ToCheck']['columns']['quantity']['label'] = __('Quantity');
		$this->DataTable->settings['ToCheck']['columns']['price']['label'] = __('Price');
		$this->DataTable->settings['Offline']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Offline']['columns']['category']['label'] = __('Category');
		$this->DataTable->settings['Offline']['columns']['section']['label'] = __('Section');
		$this->DataTable->settings['Offline']['columns']['family']['label'] = __('Family');
		$this->DataTable->settings['Offline']['columns']['quantity']['label'] = __('Quantity');
		$this->DataTable->settings['byCompany']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['byCompany']['columns']['StockCategory.name']['label'] = __('Category');
		$this->DataTable->settings['byCompany']['columns']['StockSection.name']['label'] = __('Section');
		$this->DataTable->settings['byCompany']['columns']['StockFamily.name']['label'] = __('Family');
		$this->DataTable->settings['byCompany']['columns']['quantity']['label'] = __('Quantity');
	}

	public function index(){
		$this->DataTable->setViewVar(array('Online', 'Offline', 'ToCheck'));
	}

	public function company( $companyId = null ){
		if($companyId == 3){
			return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
		}
		$this->Session->write('StockItem.company', $companyId);
		$company = $this->StockItem->Company->findById($companyId);
		$this->DataTable->setViewVar(array('byCompany'));
		$this->set(compact('company'));
	}

	public function byCompany(){
		$this->DataTable->paginate(null, [
      'StockItem.company_id' => $this->Session->read('StockItem.company'),
    ]);
	}

	public function add( $companyId = null ){

		$companies = $this->StockItem->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name'),
		));
		$this->set(compact('companies'));
		$this->set(compact('companyId'));

		if(!empty($companyId)){
			$company = $this->StockItem->Company->findById($companyId);
			$this->set(compact('company'));
		}

		$categories = $this->StockItem->StockCategory->find('list', array(
			'fields' => array(
				'StockCategory.id',
				'StockCategory.code_name'
			),
			'order' => 'StockCategory.code'
		));
		$this->set(compact('categories'));

		$suppliers = $this->StockItem->Supplier->find('list', array(
			'fields' => array(
				'Supplier.id',
				'Supplier.name'
			),
			'order' => 'Supplier.name'
		));
		$this->set(compact('suppliers'));

		if($this->request->is('post') || $this->request->is('put')){

			$maximalCode = $this->StockItem->find('first', array(
				'conditions' => array(
					'stock_category_id' => $this->request->data['StockItem']['stock_category_id'],
					'stock_section_id' => $this->request->data['StockItem']['stock_section_id'],
					'stock_family_id' => $this->request->data['StockItem']['stock_family_id']
				),
				'fields' => array(
					'MAX(StockItem.code) as code'
				)
			));
			if(!is_null($maximalCode[0]['code'])){
				$newCode = $maximalCode[0]['code'] + 1;
			} else {
				$stockCategory = $this->StockItem->StockCategory->findById($this->request->data['StockItem']['stock_category_id']);
				$stockSection = $this->StockItem->StockSection->findById($this->request->data['StockItem']['stock_section_id']);
				$stockFamily = $this->StockItem->StockFamily->findById($this->request->data['StockItem']['stock_family_id']);
				$newCode = $stockCategory['StockCategory']['code'];
				$newCode .= ($stockSection['StockSection']['code'] < 10) ? '0' . $stockSection['StockSection']['code'] : $stockSection['StockSection']['code'];
				$newCode .= ($stockFamily['StockFamily']['code'] < 10) ? '0' . $stockFamily['StockFamily']['code'] : $stockFamily['StockFamily']['code'];
				$newCode .= '01';
			}
		   	$this->request->data['StockItem']['code'] = $newCode;

			if($this->StockItem->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockItem']['name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->StockItem->id));
				} elseif($this->request->data['StockItem']['company_id'] == 3) { // festiloc
					return $this->redirect(array('action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockItem']['company_id']));
				}
			}
		}
	}

	public function edit($id = null){
		$this->StockItem->id = $id;

		if (!$this->StockItem->exists($id)) {
			throw new NotFoundException(__('Invalid StockItem'));
		}

		$companies = $this->StockItem->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name'),
		));
		$this->set(compact('companies'));

		$stockitem = $this->StockItem->find('first', array(
			'conditions' => array('id' => $id),
			'contain' => array(
				'Document' => array('User'),
				'StockInventory',
				'StockItemUnavailability' => array('StockOrder')
			)
		));

		$categories = $this->StockItem->StockCategory->find('list', array(
			'fields' => array(
				'StockCategory.id', 'StockCategory.code_name'
			),
			'order' => 'StockCategory.id'
		));
		$this->set(compact('categories'));

		$suppliers = $this->StockItem->Supplier->find('list', array(
			'fields' => array(
				'Supplier.id',
				'Supplier.name'
			),
			'order' => 'Supplier.name'
		));
		$this->set(compact('suppliers'));

		$sections = $this->StockItem->StockSection->find('list', array(
			'conditions' => array(
				'StockSection.stock_category_id' => $stockitem['StockItem']['stock_category_id']
			),
			'fields' => array('StockSection.id', 'StockSection.code_name')
		));
		$this->set(compact('sections'));
		$families = $this->StockItem->StockFamily->find('list', array(
			'conditions' => array(
				'StockFamily.stock_category_id' => $stockitem['StockItem']['stock_category_id'],
				'StockFamily.stock_section_id' => $stockitem['StockItem']['stock_section_id']
			),
			'fields' => array('StockFamily.id', 'StockFamily.code_name')
		));
		$this->set(compact('families'));

		$inventories = $this->StockItem->StockInventory->find('list', array(
			'order' => 'start',
			'fields' => array('StockInventory.id', 'StockInventory.name_dates')
		));
		$this->set(compact('inventories'));

		$storageTypes = $this->StockItem->StockStorageType->find('list', array(
			'order' => 'name',
			'fields' => array('StockStorageType.id', 'StockStorageType.name')
		));
		$this->set(compact('storageTypes'));

		$locations = $this->StockItem->StockLocation->find('list', array(
			'fields' => array('StockLocation.id', 'StockLocation.name')
		));
		$this->set(compact('locations'));

		if ($this->request->is('post') || $this->request->is('put')) {

			if(!empty($this->request->data['StockInventory']['ids'])){
				foreach($this->request->data['StockInventory']['ids'] as $kk => $id){
					$this->request->data['StockInventory'][$kk]['id'] = $id;
				}
				unset($this->request->data['StockInventory']['ids']);
			} else {
				unset($this->request->data['StockInventory']['ids']);
			}

			if($this->StockItem->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('%s has been saved.', $stockitem['StockItem']['name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->StockItem->id));
				} elseif($this->request->data['StockItem']['company_id'] == 3) { // festiloc
					return $this->redirect(array('action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockItem']['company_id']));
				}
			} else {
				$this->Session->setFlash(__('StockItem has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return false;
			}
		} else {
			if(!empty($stockitem['StockInventory'])){
				$stockitem['inventories'] = array();
				foreach($stockitem['StockInventory'] as $inventory){
					$stockitem['inventories'][] = $inventory['id'];
				}
			}
			$this->set(compact('stockitem'));
			$this->request->data = $stockitem;
		}
	}

	public function view($id = null){
		$view = new View($this);
		$time = $view->loadHelper('Time');
		$stockitem = $this->StockItem->find('first', array(
			'contain' => array(
				'StockCategory',
				'StockSection',
				'StockFamily',
				'Document.category = "stockitem"'
			),
			'conditions' => array('StockItem.id' => $id)
		));
		$this->set(compact('stockitem'));
		$this->StockItem->id = $id;
		$this->set('actualQuantity', $this->StockItem->getActualAvailibility());
		$quantities = array();
		$numberOfDays = 10;
		$date = date('Y-m-d');
		for($i=0;$i<$numberOfDays;$i++){
			$nextDate = strtotime($date . $i . "days");
			$quantities[$i]['day'] = $time->format($nextDate, '%d.%m.%Y');
			$nextDate = date('Y-m-d 00:00:00', $nextDate);
			$quantities[$i]['actualQuantity'] = $this->StockItem->getAvailibility($nextDate);
			$quantities[$i]['reconditionning'] = $this->StockItem->getUnavailibility($nextDate, 'reconditionning');
			$quantities[$i]['confirmed'] = $this->StockItem->getUnavailibility($nextDate, 'confirmed');
			$quantities[$i]['reparation'] = $this->StockItem->getUnavailibility($nextDate, 'reparation');
			$quantities[$i]['potential'] = $this->StockItem->getUnavailibility($nextDate, 'potential');
			$quantities[$i]['other'] = $this->StockItem->getUnavailibility($nextDate, 'other');
			$quantities[$i]['quantity'] = $stockitem['StockItem']['quantity'];
		}
		$this->set(compact('quantities'));

		$stockFromSameFamily = $this->StockItem->find('all', array(
			'conditions' => array(
				'StockItem.stock_family_id' => $stockitem['StockFamily']['id'],
				'StockItem.stock_section_id' => $stockitem['StockSection']['id'],
				'StockItem.stock_category_id' => $stockitem['StockCategory']['id'],
				'StockItem.id <>' => $stockitem['StockItem']['id']
			),
			'contain' => 'Document.category = "stockitem"',
			'order' => 'StockItem.code'
		));
		$this->set(compact('stockFromSameFamily'));

		$orders = $this->StockItem->StockOrderBatch->find('all', array(
			'joins' => array(
			  array(
				'table' => 'stock_orders',
				'alias' => 'StockOrder1',
				'conditions' => array('StockOrderBatch.stock_order_id = StockOrder1.id')
			  )
			),
			'conditions' => array(
				'StockOrderBatch.stock_item_id' => $id,
				'StockOrder1.status NOT' => array('cancelled')
			),
			'contain' => array(
				'StockOrder' => array('Client')
			),
			'order' => 'StockOrder.service_date_begin DESC'
		));
		$this->set(compact('orders'));

		$company = $this->StockItem->Company->findById($stockitem['StockItem']['company_id']);
		$this->set(compact('company'));
	}

	public function delete($id = null){
		$stockitem = $this->StockItem->findById($id);
		$this->StockItem->id = $id;
		if($this->StockItem->saveField('archived', 1)){
			$this->StockItem->saveField('online', 0);
			$this->Session->setFlash(__('%s has been correctly deleted.', $stockitem['StockItem']['name']), 'alert', array('type' => 'success'));
		} else {
			$this->Session->setFlash(__('%s has not been deleted.', $stockitem['StockItem']['name']), 'alert', array('type' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function get() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$output = $this->StockItem->find('all', array(
				'conditions' => array(
					'OR' => array(
						'code LIKE' => '%'.$this->request->data['term'].'%',
						'name LIKE' => '%'.$this->request->data['term'].'%'
					),
					'company_id' => $this->request->data['company_id']
				),
				'order' => array('code')
			));
			$this->response->body(json_encode($output));
		}
	}

	public function pricelist($category = null, $section = null){
		ini_set('memory_limit', '-1');
		set_time_limit(0);

		$categories = $this->StockItem->StockCategory->find('list', array(
			'fields' => array(
				'StockCategory.id', 'StockCategory.name'
			),
			'order' => 'StockCategory.id'
		));
		$this->set(compact('categories'));

		$conditions[] = array(
			'StockItem.online' => 1,
			'StockItem.archived' => 0,
			'StockItem.company_id' => 3,
      'StockItem.stock_category_id <>' => 8
		);

		if($this->request->is('post') || $this->request->is('put')){

			if(!empty($this->request->data['PriceList']['category_id'])){
				$conditions[] = array(
					'StockItem.stock_category_id' => $this->request->data['PriceList']['category_id']
				);
				$this->set('category', $this->request->data['PriceList']['category_id']);
				if(!empty($this->request->data['PriceList']['section_id'])){
					$conditions[] = array(
						'StockItem.stock_section_id' => $this->request->data['PriceList']['section_id']
					);
					$this->set('section', $this->request->data['PriceList']['section_id']);
					$this->set('sections', $this->StockItem->StockSection->find('list', array(
						'conditions' => array(
							'StockSection.stock_category_id' => $this->request->data['PriceList']['category_id']
						)
					)));
				}
			}
		}

		if(!empty($category) && $category > 0){
			$conditions[] = array(
				'StockItem.stock_category_id' => $category
			);
			$categoryName = $this->StockItem->StockCategory->find('first', array(
				'conditions' => array(
					'StockCategory.id' => $category
				),
				'fields' => array('StockCategory.name')
			));
			$filename = 'festiloc_liste_de_prix_'.strtolower($categoryName['StockCategory']['name']).'.pdf';
			if(!empty($subcategory) && $subcategory > 0){
				$conditions[] = array(
					'StockItem.stock_section_id' => $subcategory
				);
				$sectionName = $this->StockItem->StockSection->find('first', array(
					'conditions' => array(
						'StockSection.id' => $subcategory
					),
					'fields' => array('StockSection.name')
				));
				$filename = 'festiloc_liste_de_prix_'.strtolower($categoryName['StockCategory']['name']).'_'.strtolower($sectionName['StockSection']['name']).'.pdf';
			}
		} else {
			$filename = 'festiloc_liste_de_prix.pdf';
		}


		$this->pdfConfig = array(
			'orientation' => 'portrait',
			'filename' => $filename,
			'options' => array(
				'footer-center' => '[page]',
				'footer-font-size' => 9
			)
		);

		$data = $this->StockItem->find('all', array(
			'conditions' => !empty($conditions) ? $conditions : array(),
			'contain' => array(
				'StockCategory',
				'StockSection',
				'StockFamily',
				'Document' => array(
					'conditions' => array(
						'Document.category' => 'stockitem'
					),
					'order' => 'Document.filesize'
				)
			),
			'order' => 'StockItem.code ASC',
			//'limit' => 10
		));
		$articles = array();
		foreach($data as $item){
			if(empty($item['Document'])){
				//continue;
			}
			$articles[$item['StockSection']['id']][] = $item;
			//debug(strlen($item['Document'][0]['filename']));exit;
		}
		$this->layout = 'default';
		$this->set(compact('articles'));
	}

	public function getSections(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$sections = $this->StockItem->StockSection->find('list', array(
				'conditions' => array(
					'stock_category_id' => $_POST['stock_category_id']
				),
				'fields' => array(
					'StockSection.id', 'StockSection.code_name'
				)
			));
			$this->response->body(json_encode($sections));
		}
	}

	public function getFamilies(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$families = $this->StockItem->StockFamily->find('list', array(
				'conditions' => array(
					'stock_category_id' => $_POST['stock_category_id'],
					'stock_section_id' => $_POST['stock_section_id']
				),
				'fields' => array(
					'StockFamily.id', 'StockFamily.code_name'
				),
				'order' => 'StockFamily.name'
			));
			$this->response->body(json_encode($families));
		}
	}

	public function getImage($id = null){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$stockitem = $this->StockItem->find('first',array(
				'conditions' => array(
					'StockItem.id' => $id
				),
				'contain' => array('Document.category = "stockitem"')
			));
			$view = new View($this);
			$html = $view->loadHelper('Html');
			if(!empty($stockitem['Document'][1]['url'])){
				$image = $html->image(DS . $stockitem['Document'][1]['url'], array('class' => 'img-responsive'));
				$this->response->body(json_encode(array('image' => $image, 'success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		}
	}

	public function json(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if(!empty($_POST['show_off'])){
				if($_POST['show_off']){
					$online = array(1);
				} else {
					$online = array(0,1);
				}
			} else {
				$online = array(0,1);
			}
			$data = $this->StockItem->find('all', array(
				// join orders which occur during delivery date and return date
				/*'joins' => array(
					array(

					)
				),*/
				'contain' => array(
					// 'StockOrderBatch' => array(
					// 	'StockOrder'
					// ),
					'StockSection'
				),
				'conditions' => array(
					'OR' => array(
						'StockItem.name LIKE' => "%" . $_POST['term'] . "%",
						'StockItem.code LIKE' => "%" . $_POST['term'] . "%"
					),
					'StockItem.status' => 'ok',
					'StockItem.archived' => 0,
					'StockItem.company_id' => 3,
					'StockItem.online' => $online
				),
				'cache' => $_POST['term'],
				'cacheConfig' => 'short'
			));
			$stockitems = Cache::read('stockitems_' . date('dmY') . '_' . $_POST['term'], 'long');
			$stockitems = array();
			if(empty($stockitems)){
				foreach($data as $k => $item){
					if($item['StockItem']['status'] != 'ok'){
						continue;
					}
					$this->StockItem->id = $item['StockItem']['id'];
					$stockitems[$k]['id'] = $item['StockItem']['id'];
					$stockitems[$k]['text'] = $item['StockItem']['code'] . ' ' . $item['StockItem']['name'] . '   (Stock total: '.$item['StockItem']['quantity'].')';
					$stockitems[$k]['name'] = $item['StockItem']['name'];
					$stockitems[$k]['code'] = $item['StockItem']['code'];
					$stockitems[$k]['price'] = $item['StockItem']['price'];
					$stockitems[$k]['subject_to_discount'] = $item['StockItem']['subject_to_discount'];
					$stockitems[$k]['totalQuantity'] = $item['StockItem']['quantity'];
					$stockitems[$k]['section'] = $item['StockSection']['name'];
					$stockitems[$k]['quantity'] = $_POST['quantity'];
				}
			}
			//Cache::write('stockitems_' . date('dmY') . '_' . $_POST['term'], 'long');
			$this->response->body(json_encode($stockitems));
		}
	}

	public function json_advanced(){
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$data = $this->StockItem->find('all', array(
				'contain' => array(
					'Company',
					'StockCategory',
					'StockSection',
					'StockFamily'
				),
				'conditions' => array(
					'OR' => array(
						'StockItem.name LIKE' => "%" . $this->request->data['term'] . "%",
						'StockItem.code LIKE' => "%" . $this->request->data['term'] . "%"
					),
					'StockItem.status' => 'ok',
					'StockItem.archived' => 0,
					//'StockItem.company_id' => 3,
					'StockItem.online' => 1
				),
				'order' => array(
					'StockItem.company_id'
				)
			));
			$stockitems = array();
			if(empty($stockitems)){
				foreach($data as $k => $item){
					$stockitems[$k]['id'] = $item['StockItem']['id'];
					$stockitems[$k]['text'] = $item['StockItem']['code'] . ' ' . $item['StockItem']['name'];
					$stockitems[$k]['infos'] = $item['StockCategory']['name'] . '/' . $item['StockSection']['name'] . '/' . $item['StockFamily']['name'];
					$stockitems[$k]['btn'] = '<span class="btn btn-company btn-xs btn-company-'.$item['Company']['class'].'"></span> ';
				}
			}
			$this->response->body(json_encode($stockitems));
		}
	}

	public function checkAvailability(){
		// $this->StockItem->id = 1099;
		// debug($data = $this->StockItem->checkAvailability(31, 1, '2015-09-28', 2312));
		// exit;
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->StockItem->id = $this->request->data['stock_item_id'];
			$eventId = null;
			if(!empty($this->request->data['event_id'])){
				$eventId = $this->request->data['event_id'];
				$this->loadModel('Event');
				$event = $this->Event->findById($this->request->data['event_id']);
				$this->request->data['startDate'] = $event['Event']['confirmed_date'];
			} elseif(empty($this->request->data['startDate']) && empty($this->request->data['event_id'])){
				$this->request->data['startDate'] = date('Y-m-d');
			}
			if(empty($this->request->data['stock_order_id'])){
				$stockOrderId = null;
			} else {
				$stockOrderId = $this->request->data['stock_order_id'];
			}
			$total = $this->StockItem->field('quantity');
			if(empty($total)){
				$data = array('no_quantity' => 1);
			} else {
				$data = $this->StockItem->checkAvailability($this->request->data['duration'], $this->request->data['amount'], $this->request->data['startDate'], $stockOrderId, $eventId);
			}
			$this->response->body(json_encode($data));
			//$this->response->body(json_encode($_POST));
		}
	}

	public function checkAvailabilityByHour(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->StockItem->id = $this->request->data['stock_item_id'];
			if($this->request->data['model'] == 'event'){
				$this->loadModel('Event');
				$event = $this->Event->find('first', array(
					'conditions' => array(
						'id' => $this->request->data['model_id']
					),
					'fields' => array('confirmed_date', 'start_hour', 'end_hour')
				));
				$start = $event['Event']['confirmed_date'] . ' ' . $event['Event']['start_hour'];
				$end = $event['Event']['confirmed_date'] . ' ' . $event['Event']['end_hour'];
			} else {
				//TODO
			}
			$available = $this->StockItem->checkAvailabilityByHour($this->request->data['amount'], $start, $end, $this->request->data['model'], $this->request->data['model_id']);
			if($available == 1){
				$data = array('sufficientStock' => true, 'emptyStock' => false);
			} elseif($available == 2){
				$data = array('emptyStock' => true, 'sufficientStock' => true);
			} else {
				$data = array('sufficientStock' => false);
			}
			$this->response->body(json_encode($data));
		}
	}

	public function daterangepicker(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->StockItem->id = $_POST['stock_item_id'];
			$start = strtotime($_POST['start']); // or your date as well
			$end = strtotime($_POST['end']); // or your date as well
			$datediff = $end - $start;
			$numberOfDays = floor($datediff/(60*60*24));
			$quantities = array();
			for($i=0;$i<$numberOfDays;$i++){
				$nextDate = strtotime(date('d-m-Y',$start) . $i . "days");
				$quantities[$i]['day'] = date('d.m.Y', $nextDate);
				$quantities[$i]['quantity'] = $this->StockItem->getAvailibility(date('Y-m-d', $nextDate));
				$quantities[$i]['reconditionning'] = $this->StockItem->getUnavailibility(date('Y-m-d', $nextDate), 'reconditionning');
				$quantities[$i]['confirmed'] = $this->StockItem->getUnavailibility(date('Y-m-d', $nextDate), 'confirmed');
				$quantities[$i]['reparation'] = $this->StockItem->getUnavailibility(date('Y-m-d', $nextDate), 'reparation');
				$quantities[$i]['potential'] = $this->StockItem->getUnavailibility(date('Y-m-d', $nextDate), 'potential');
				$quantities[$i]['other'] = $this->StockItem->getUnavailibility(date('Y-m-d', $nextDate), 'other');
				$quantities[$i]['totalQuantity'] = $this->StockItem->field('quantity');
			}
			$this->response->body(json_encode($quantities));
			//$this->response->body(json_encode($_POST));
		}
	}

	public function showConflicts($id = null, $amount = 0, $startDate, $duration, $mode = null) {

		$this->StockItem->id = $id;
		$stockitem = $this->StockItem->read(null, $id);

		$dates = array();
		$start = strtotime($startDate);
		for($i=0;$i<=$duration;$i++){
			$nextDate = strtotime(date('d-m-Y',$start) . $i . "days");
			$dates[] = date('Y-m-d 00:00:00', $nextDate);
		}
		$conflicts = array();
		$quantities = array();

		$this->loadModel('Event');
		$this->loadModel('StockOrder');

		foreach($dates as $k => $date){
			$end = strtotime(date('d-m-Y',strtotime($date)) . "1 days");
			$end = date('Y-m-d 00:00:00', $end);

			$events = array();
			$orders = array();

			// find batches with corresponding product
			$unavailabilities = $this->StockItem->StockItemUnavailability->find('all', array(
				'conditions' => array(
					'StockItemUnavailability.stock_item_id' => $id,
					'StockItemUnavailability.quantity >' => 0,
					'OR' => array(
						array('StockItemUnavailability.start_date BETWEEN ? AND ?' => array($date, $end)),
						array('? BETWEEN StockItemUnavailability.start_date AND StockItemUnavailability.end_date' => $date)
					)
				),
				// 'fields' => array(
				// 	'StockItemUnavailability.event_id',
				// 	'StockItemUnavailability.stock_order_id',
				// 	'StockItemUnavailability.quantity',
				// 	'StockItemUnavailability.status',
				// 	'StockItemUnavailability.start_date',
				// 	'StockItemUnavailability.end_date'
				// )
			));

			if(!empty($unavailabilities)){
				foreach($unavailabilities as $k => $tmp){
					if(!empty($tmp['StockItemUnavailability']['event_id']) && $stockitem['StockItem']['company_id'] != 3){
						$events[$k] = $this->Event->find('first', array(
							'contain' => array('Client'),
							'conditions' => array(
								'Event.id' => $tmp['StockItemUnavailability']['event_id']
							)
						));
						$events[$k]['unavailability_status'] = $tmp['StockItemUnavailability']['status'];
						$events[$k]['unavailability_start'] = $tmp['StockItemUnavailability']['start_date'];
						$events[$k]['unavailability_end'] = $tmp['StockItemUnavailability']['end_date'];
						$quantity = $this->Event->Order->OrderGroup->OrderItem->find('all', array(
							'joins' => array(
								array(
									'table' => 'order_groups',
									'alias' => 'og',
									'conditions' => array('OrderItem.order_group_id = og.id')
								),
								array(
									'table' => 'orders',
									'alias' => 'o',
									'conditions' => array('og.order_id = o.id', 'o.model_id' => $tmp['StockItemUnavailability']['event_id'], 'o.model' => 'event')
								)
							),
							'conditions' => array(
								'OrderItem.stock_item_id' => $id,
								'OrderItem.quantity >' => 0
							),
							'fields' => array(
								'SUM(OrderItem.quantity) as total'
							)
						));
						if(!is_null($quantity[0][0]['total'])){
							$quantity = $quantity[0][0]['total'];
						} else {
							unset($events[$k]);
						}
						$quantities[$date][$tmp['StockItemUnavailability']['event_id']] = $quantity;
					}
					if(!empty($tmp['StockItemUnavailability']['stock_order_id'])){
						$orders[] = $this->StockOrder->find('first', array(
							'contain' => array('Client'),
							'conditions' => array(
								'StockOrder.id' => $tmp['StockItemUnavailability']['stock_order_id']
							)
						));
						$quantity = $this->StockOrder->StockOrderBatch->find('first', array(
							'conditions' => array(
								'StockOrderBatch.stock_order_id' => $tmp['StockItemUnavailability']['stock_order_id'],
								'StockOrderBatch.stock_item_id' => $id
							),
							'fields' => array(
								'StockOrderBatch.quantity'
							)
						));
						if(empty($quantity)){
							$quantity = $tmp['StockItemUnavailability']['quantity'];
						} else {
							$quantity = $quantity['StockOrderBatch']['quantity'];
						}
						$quantities[$date][$tmp['StockItemUnavailability']['stock_order_id']] = $quantity;
					}
				}
				$conflicts[$date]['events'] = $events;
				$conflicts[$date]['orders'] = $orders;
			}
		}
		$this->set(compact('conflicts'));
		$this->set(compact('quantities'));
		$this->set(compact('stockitem'));

		$this->set('siu_statuses', Configure::read('StockItemUnavailability.statuses'));
	}

	public function showConflicts1($id = null, $amount = 0, $startDate, $duration, $mode = null) {

		$this->StockItem->id = $id;
		$stockitem = $this->StockItem->read(null, $id);

		$dates = array();
		$start = strtotime($startDate);
		for($i=0;$i<=$duration;$i++){
			$nextDate = strtotime(date('d-m-Y',$start) . $i . "days");
			$dates[] = date('Y-m-d 00:00:00', $nextDate);
		}
		$conflicts = array();
		$quantities = array();

		if($mode == 'event'){
			$this->loadModel('Event');
			foreach($dates as $k => $date){
				$end = strtotime(date('d-m-Y',strtotime($date)) . "1 days");
				$end = date('Y-m-d 00:00:00', $end);
				// find batches with corresponding product
				$ids = $this->StockItem->StockItemUnavailability->find('all', array(
					'conditions' => array(
						'StockItemUnavailability.stock_item_id' => $id,
						'OR' => array(
		          array('StockItemUnavailability.start_date BETWEEN ? AND ?' => array($date, $end)),
		          array('? BETWEEN StockItemUnavailability.start_date AND StockItemUnavailability.end_date' => $date)
		        )
					),
					'fields' => array('StockItemUnavailability.event_id', 'StockItemUnavailability.quantity')
				));
				$events = array();
				if(!empty($ids)){
					foreach($ids as $tmp){
						$events[] = $this->Event->find('first', array(
							'contain' => array('Client'),
							'conditions' => array(
								'Event.id' => $tmp['StockItemUnavailability']['event_id']
							)
						));
						$quantity = $this->Event->Order->OrderGroup->OrderItem->find('first', array(
							'joins' => array(
								array(
									'table' => 'order_groups',
									'alias' => 'og',
									'conditions' => array('OrderItem.order_group_id = og.id')
								),
								array(
									'table' => 'orders',
									'alias' => 'o',
									'conditions' => array('og.order_id = o.id', 'o.model_id' => $tmp['StockItemUnavailability']['event_id'], 'o.model' => 'event')
								)
							),
							'conditions' => array(
								'OrderItem.stock_item_id' => $id
							),
							'fields' => array(
								'OrderItem.quantity'
							)
						));
						if(empty($quantity)){
							$quantity = $tmp['StockItemUnavailability']['quantity'];
						} else {
							$quantity = $quantity['OrderItem']['quantity'];
						}
						$quantities[$date][$tmp['StockItemUnavailability']['event_id']] = $quantity;
					}
					$conflicts[$date]['events'] = $events;
				} else {
					$conflicts[$date]['message'] = true;
				}
			}
		} else {
			$this->loadModel('StockOrder');
			foreach($dates as $k => $date){
				//$availability = $this->StockItem->getAvailibility($date);
				// find batches with corresponding product
				$ids = $this->StockItem->StockItemUnavailability->find('all', array(
					'conditions' => array(
						'StockItemUnavailability.stock_item_id' => $id,
						'StockItemUnavailability.start_date <=' => $date,
						'StockItemUnavailability.end_date >=' => $date
					),
					'fields' => array('StockItemUnavailability.stock_order_id', 'StockItemUnavailability.quantity')
				));
				$orders = array();
				if(!empty($ids)){
					foreach($ids as $tmp){
						$orders[] = $this->StockOrder->find('first', array(
							'contain' => array('Client'),
							'conditions' => array(
								'StockOrder.id' => $tmp['StockItemUnavailability']['stock_order_id']
							)
						));
						$quantity = $this->StockOrder->StockOrderBatch->find('first', array(
							'conditions' => array(
								'StockOrderBatch.stock_order_id' => $tmp['StockItemUnavailability']['stock_order_id'],
								'StockOrderBatch.stock_item_id' => $id
							),
							'fields' => array(
								'StockOrderBatch.quantity'
							)
						));
						if(empty($quantity)){
							$quantity = $tmp['StockItemUnavailability']['quantity'];
						} else {
							$quantity = $quantity['StockOrderBatch']['quantity'];
						}
						$quantities[$date][$tmp['StockItemUnavailability']['stock_order_id']] = $quantity;
					}
					$conflicts[$date]['orders'] = $orders;
				}
			}
		}
		$this->set(compact('conflicts'));
		$this->set(compact('quantities'));
		$this->set(compact('stockitem'));

	}

	public function conflicts(){
		$now = date('Y-m-d');
		$data = array();
		for($i = 0; $i < 40; $i++){
			$date = date('Y-m-d', strtotime("+" . $i . " days"));
			$unavailabilities = $this->StockItem->StockItemUnavailability->find('all', array(
				'joins' => array(
					array(
						'table' => 'stock_orders',
						'alias' => 'SO',
						'conditions' => array(
							'SO.id = StockItemUnavailability.stock_order_id'
						)
					),
					// array(
					// 	'table' => 'stock_order_batches',
					// 	'alias' => 'SOB',
					// 	//'type' => 'left',
					// 	'conditions' => array(
					// 		'SOB.stock_order_id = StockItemUnavailability.stock_order_id',
					// 		'SOB.stock_item_id = StockItemUnavailability.stock_item_id',
					// 	)
					// ),
					array(
						'table' => 'stock_items',
						'alias' => 'SI',
						'conditions' => array(
							'SI.id = StockItemUnavailability.stock_item_id'
						)
					),
					array(
						'table' => 'clients',
						'alias' => 'C',
						'type' => 'left',
						'conditions' => array(
							'C.id = SO.client_id'
						)
					)
				),
				'conditions' => array(
					'StockItemUnavailability.start_date <=' => $date,
					'StockItemUnavailability.end_date >=' => $date,
					'StockItemUnavailability.stock_item_id <>' => -1,
					//'StockItemUnavailability.stock_item_id' => 97,
				),
				//'group' => 'StockItemUnavailability.stock_item_id',
				'fields' => array(
					'StockItemUnavailability.id',
					'StockItemUnavailability.stock_item_id',
					'StockItemUnavailability.stock_order_id',
					'StockItemUnavailability.status',
					'StockItemUnavailability.quantity',
					'SI.code',
					'SI.name',
					'SI.quantity',
					'SO.id',
					'SO.name',
					'SO.status',
					'SO.client_id',
					'C.name',
					//'SOB.quantity'
				),
				'order' => 'StockItemUnavailability.stock_item_id'
			));
			//debug($unavailabilities);exit;
			foreach($unavailabilities as $k => $item){
				$this->StockItem->id = $item['StockItemUnavailability']['stock_item_id'];
				$availability = $this->StockItem->getAvailibility($date);
				if($availability < 0){
					$data[$date][$this->StockItem->id]['stock_item'] = array(
						'code' => $item['SI']['code'],
						'name' => $item['SI']['name'],
						'quantity' => $item['SI']['quantity'],
						'availability' => $availability,
					);
					if($item['StockItemUnavailability']['stock_order_id'] == 1){
						$key = 'manual';
					} else {
						$key = $k;
					}
					$data[$date][$this->StockItem->id]['orders'][$key] = array(
						'stock_order_id' => $item['StockItemUnavailability']['stock_order_id'],
						'client_id' => $item['SO']['client_id'],
						'name' => $item['SO']['name'],
						'status' => $item['SO']['status'],
						'unavailability_status' => $item['StockItemUnavailability']['status'],
						'unavailability_id' => $item['StockItemUnavailability']['id'],
						'client_name' => $item['C']['name'],
						'quantity' => $item['StockItemUnavailability']['quantity']
					);
				}
			}
		}
		$this->set(compact('data'));
	}

	public function conflicts_detail($date = null, $stock_item_id = null){
		$this->loadModel('StockOrder');
		$orders = $this->StockOrder->find('all', array(
			'joins' => array(
				array(
					'table' => 'stock_order_batches',
					'alias' => 'SOB',
					'conditions' => array(
						sprintf("SOB.stock_item_id = %s", $stock_item_id)
					)
				)
			),
			'conditions' => array(
				'StockOrder.delivery_date <=' => $date,
				'StockOrder.return_date >=' => $date,
				'StockOrder.status <>' => 'cancelled'
			)
		));
		$this->set(compact('orders'));
	}

	public function conflicts_ajax(){

		$this->loadModel('StockOrder');

		$data = $this->StockItem->StockItemUnavailability->find('all', array(
			'joins' => array(
				array(
					'table' => 'stock_items',
					'alias' => 'StockItem1',
					'conditions' => array(
						'StockItem1.id = StockItemUnavailability.stock_item_id'
					)
				),
				array(
					'table' => 'stock_orders',
					'alias' => 'StockOrder1',
					'conditions' => array(
						'StockOrder1.id = StockItemUnavailability.stock_order_id'
					)
				)
			),
			'conditions' => array(
				'StockItemUnavailability.status <>' => '',
				'StockItem1.quantity >' => 0,
				'StockOrder1.status NOT' => array("cancelled"),
				'OR' => array(
					array(
						'StockItemUnavailability.start_date <=' => date('Y-m-d'),
						'StockItemUnavailability.end_date >=' => date('Y-m-d')
					),
					array(
						'StockItemUnavailability.start_date >=' => date('Y-m-d'),
						'StockItemUnavailability.end_date >=' => date('Y-m-d')
					)
				)
			),
			'contain' => array(
				'StockItem',
				'StockOrder'
			),
			'order' => array('start_date'),
			'fields' => array(
				'StockItem.name',
				'StockItem.code',
				'StockItemUnavailability.start_date',
				'StockItemUnavailability.end_date',
				'StockItemUnavailability.quantity',
				'StockItemUnavailability.status',
				'StockOrder.name'
			),
			'limit' => 200
		));

		$stockitems = array();
		$stockorders = array();
		foreach($data as $item){
			if($item['StockItemUnavailability']['start_date'] == '1970-01-01' || $item['StockItemUnavailability']['end_date'] == '1970-01-01'){
				continue;
			}
			//$this->StockItem->id = $item['StockItem']['id'];
			$start = strtotime($item['StockItemUnavailability']['start_date']); // or your date as well
			$end = strtotime($item['StockItemUnavailability']['end_date']);
			$duration = floor( ($end - $start) / (60*60*24) ) ;
			$stockorder = $this->StockOrder->find('first', array(
				'conditions' => array(
					'StockOrder.id' => $item['StockOrder']['id']
				),
				'contain' => array('Client'),
				'fields' => array('Client.name', 'StockOrder.name', 'StockOrder.id', 'StockOrder.order_number', 'StockOrder.status')
			));
			$batch = $this->StockOrder->StockOrderBatch->find('first', array(
				'conditions' => array(
					'StockOrderBatch.stock_order_id' => $item['StockOrder']['id'],
					'StockOrderBatch.stock_item_id' => $item['StockItem']['id']
				),
				'fields' => array('StockOrderBatch.quantity')
			));
			$stockorder['batch'] = $batch;
			$stockorder['unavailability_status'] = $item['StockItemUnavailability']['status'];
			for($i=0; $i<$duration; $i++){
				$date = date('Y-m-d', strtotime($item['StockItemUnavailability']['start_date'] . "+" . $i . " days"));
				$stockitems[$item['StockItem']['id']][$date] = $date;
				$stockorders[$date][$item['StockItem']['id']][] = $stockorder;
			}
		}
		$conflicts = array();
		foreach($stockitems as $id => $dates){
			$this->StockItem->id = $id;
			$stockitem = $this->StockItem->read(array('id', 'name', 'code', 'quantity'), $id);
			foreach($dates as $date){
				$availability = $this->StockItem->getAvailibility($date);
				if($availability > 0){

				} elseif($availability == 0) {
					// $conflicts[$date][$id]['stockitem'] = $this->StockItem->read(null, $id);
					// $conflicts[$date][$id]['orders'] = $stockorders[$id];
					// $conflicts[$date][$id]['class'] = 'bg-warning';
				} elseif($availability <= 0) {
					$conflicts[$date][$id]['stockitem'] = $stockitem;
					$conflicts[$date][$id]['class'] = 'bg-danger1';
					$conflicts[$date][$id]['availability'] = $availability;
				}
			}
		}
		ksort($conflicts);
		$this->set(compact('conflicts'));
		$this->set(compact('stockorders'));
	}


	public function csv(){

		$data = $this->StockItem->find('all', array(
			'contain' => array(
				'StockSection'
			),
			'order' => 'StockItem.code ASC'
		));
		$output = "Online,Section,RefArticle,NomArticle,PrixLocation,QtéDisponible,Localisation,TitrePhoto,TextePhoto,DétailArticle,Commande\n";
		foreach($data as $d){
			if($d['StockItem']['online'] == 0){
				$price = "999.00";
			} else {
				if(!empty($d['StockItem']['price'])){
					$price = $d['StockItem']['price'];
				} else {
					$price = "";
				}
			}
			$a = array();
			$a[0] = '';
			$a[1] = !empty($d['StockSection']['name']) ? '"' . $d['StockSection']['name'] . '"' : '';
			$a[2] = $d['StockItem']['code'];
			$a[3] = '"' . $d['StockItem']['name'] . '"';
			$a[4] = $price;
			$a[5] = !empty($d['StockItem']['quantity']) ? $d['StockItem']['quantity'] : '';
			$a[6] = '';
			$a[7] = '';
			$a[8] = '';
			$a[9] = '';
			$a[10] = '';
			$output .= implode(',', $a) . "\n";
		}
		file_put_contents(WWW_ROOT . 'files/festiloc.csv', $output);

		exit;
	}

	public function duplicateImages(){
		exit;
		$data = explode(".", file_get_contents(WWW_ROOT . 'files/images.csv'));
		foreach($data as $row){
			$row = explode(";",trim($row));
			$stockitem = $this->StockItem->find('first', array(
				'conditions' => array(
					'code' => $row[0]
				)
			));
			$stockitem1 = $this->StockItem->find('first', array(
				'conditions' => array(
					'code' => $row[1]
				)
			));
			$this->loadModel('Document');
			$documentsToDuplicate = $this->Document->find('all', array(
				'conditions' => array(
					'parent_id' => $stockitem['StockItem']['id'],
					'category' => 'stockitem'
				)
			));
			foreach($documentsToDuplicate as $doc){
				$doc['Document']['id'] = null;
				$doc['Document']['parent_id'] = $stockitem1['StockItem']['id'];
				$this->Document->save($doc);
			}
		}
		exit;
	}

	public function import_old(){
		exit;
		if(AuthComponent::user('id') == 1){

			/*$data = explode("\n", file_get_contents(WWW_ROOT . 'files/cuisine.tsv'));
			//debug($data);exit;
			unset($data[0]);
			foreach($data as $key => $row){

				$this->StockItem->StockCategory->create();
				$this->StockItem->StockSection->create();
				$this->StockItem->StockFamily->create();

				$item = explode("\t", $row);
				//debug($item);exit;
				$itemOnline = $item[1];
				$categoryCode = $item[2];
				$categoryName = $item[3];
				$sectionCode = $item[4];
				$sectionName = $item[5];
				$familyCode = $item[6];
				$familyName = $item[7];
				$itemCode = $item[9];
				$itemName = !empty($item[12]) ? $item[12] : $item[13];
				$itemPrice = $item[15];
				$itemQuantity = $item[16];
				$itemRemarks = $item[14];

				$stockcategory = $this->StockItem->StockCategory->find('first', array(
					'conditions' => array(
						'StockCategory.code' => $categoryCode
					)
				));
				$stocksection = $this->StockItem->StockSection->find('first', array(
					'conditions' => array(
						'StockSection.code' => $sectionCode,
						'StockSection.stock_category_id' => !empty($stockcategory) ? $stockcategory['StockCategory']['id'] : ''
					)
				));
				$stockfamily = $this->StockItem->StockFamily->find('first', array(
					'conditions' => array(
						'StockFamily.code' => $familyCode,
						'StockFamily.stock_category_id' => !empty($stockcategory) ? $stockcategory['StockCategory']['id'] : '',
						'StockFamily.stock_section_id' => !empty($stocksection) ? $stocksection['StockSection']['id'] : ''
					)
				));

				if(empty($stockcategory)){
					$this->StockItem->StockCategory->create();
					$tmp = array(
						'StockCategory' => array(
							'name' => $categoryName,
							'code' => $categoryCode
						)
					);
					$this->StockItem->StockCategory->save($tmp);
					$StockCategoryId = $this->StockItem->StockCategory->id;
				} else {
					$StockCategoryId = $stockcategory['StockCategory']['id'];
				}

				if(empty($stocksection)){
					$this->StockItem->StockSection->create();
					$tmp = array(
						'StockSection' => array(
							'name' => $sectionName,
							'stock_category_id' => $StockCategoryId,
							'code' => $sectionCode
						)
					);
					$this->StockItem->StockSection->save($tmp);
					$stockSectionId = $this->StockItem->StockSection->id;
				} else {
					$stockSectionId = $stocksection['StockSection']['id'];
				}

				if(empty($stockfamily)){
					$this->StockItem->StockFamily->create();
					$tmp = array(
						'StockFamily' => array(
							'name' => $familyName,
							'stock_category_id' => $StockCategoryId,
							'stock_section_id' => $stockSectionId,
							'code' => $familyCode
						)
					);
					$this->StockItem->StockFamily->save($tmp);
					$stockFamilyId = $this->StockItem->StockFamily->id;
				} else {
					$stockFamilyId = $stockfamily['StockFamily']['id'];
				}

				$stockitem = array('StockItem' => array(
					'name' => $itemName,
					'code' => $itemCode,
					'price' => $itemPrice,
					'quantity' => intval(preg_replace("/[^0-9\.]/", "", $itemQuantity)),
					'online' => $itemOnline == 'ON' ? 1 : 0,
					'status' => 'ok',
					'stock_category_id' => $StockCategoryId,
					'stock_section_id' => $stockSectionId,
					'stock_family_id' => $stockFamilyId,
					'remarks' => $itemRemarks
				));
				if(empty($this->StockItem->find('first', array( 'conditions' => array('code' => intval(preg_replace("/[^0-9\.]/", "", $itemCode))) )))){
					$this->StockItem->create();
					$this->StockItem->save($stockitem);
				}
			}

			exit;



			App::uses('Folder', 'Utility');
			App::uses('File', 'Utility');

			$dir = new Folder('files/images');
			$images = $dir->find('.*\.jpg');
			//debug($images);exit;
			foreach($images as $img){
				$data = explode('_', $img);
				$code = $data[0];
				$stockitem = $this->StockItem->find('first', array(
					'conditions' => array(
						'code' => $code
					),
					'contain' => 'Document'
				));
				if(!empty($stockitem)){

					$relativeDir = 'files' . DS . 'documents' . DS . 'stockitems' . DS . $stockitem['StockItem']['id'] . DS;
					$absoluteDir = WWW_ROOT . $relativeDir;
					$folder = new Folder($absoluteDir, true, 0777);

					$absoluteFilename = $absoluteDir . $img;
					$relativeFilename = $relativeDir . $img;

					$file = new File(WWW_ROOT . 'files' . DS . 'images' . DS . $img);

					if($file->exists()) {
						if($file->copy($relativeDir . DS . $file->name)){
							$this->loadModel('Document');
							$existingDocument = $this->Document->find('first', array(
								'conditions' => array(
									'Document.name' => $file->name
								)
							));
							if(empty($existingDocument)){
								$this->Document->create();
								$document = array(
									'name' => $img,
									'filename' => $img,
									'url' => $relativeFilename,
									'filesize' => $file->size(),
									'filetype' => $file->mime(),
									'parent_id' => $stockitem['StockItem']['id'],
									'owner' => AuthComponent::user('id'),
									'category' => 'stockitem'
								);
								$this->Document->save(array('Document' => $document));
							}
						}
					}
				}
			}

			exit;

			$data1 = explode("\n", file_get_contents(WWW_ROOT . 'files/deco.tsv'));
			$data2 = explode("\n", file_get_contents(WWW_ROOT . 'files/mobilier.tsv'));
			$data3 = explode("\n", file_get_contents(WWW_ROOT . 'files/vaisselle.tsv'));
			$data4 = explode("\n", file_get_contents(WWW_ROOT . 'files/textiles.tsv'));
			$data5 = explode("\n", file_get_contents(WWW_ROOT . 'files/cuisine.tsv'));
			unset($data1[0]);
			unset($data2[0]);
			unset($data3[0]);
			unset($data4[0]);
			unset($data5[0]);

			$data = array_merge($data1,$data2,$data3,$data4,$data5);
			$files = scandir(WWW_ROOT . 'files/images');
			foreach ($files as $key => $img) {
				$tmp = explode("_", $img);
				if($img != '.' && $img != '..')$images[$tmp[0]] = $img;
			}

			//debug($images);exit;

			$output = "nref\tarticle\n";
			foreach ($data as $key => $row) {
				$row = explode("\t", $row);
				$category = $row[2];

				if(in_array($category, array(3,4,9,7))){
					$productId = $row[9];
				} elseif(in_array($category, array(1,5))){
					$productId = $row[10];
				}

				if(in_array($category, array(3))){
					$title = $row[13];
				} elseif(in_array($category, array(4,9,5))){
					$title = $row[12];
				} elseif(in_array($category, array(1))){
					$title = !empty($row[13]) ? $row[13] : $row[14];
				} elseif(in_array($category, array(7))){
					$title = !empty($row[12]) ? $row[12] : $row[13];
				}


				if(empty($images[$productId])){
					//debug($productId);
					$output .= $productId . "\t" . utf8_decode($title) . "\n";
				}
			}
			file_put_contents(WWW_ROOT . 'files/no_images.txt', $output);


			exit;

			$counter = 0;
			foreach($data as $row){

				$this->StockItem->StockSection->create();
				$this->StockItem->StockSubsection->create();

				$item = explode("\t", $row);
				unset($item[0]);
				$item = array_values($item);
				//debug($item);exit;

				$stocksection = $this->StockItem->StockSection->find('first', array(
					'conditions' => array(
						'StockSection.code' => intval(preg_replace("/[^0-9\.]/", "", $item[1]))
					)
				));
				$stocksubsection = $this->StockItem->StockSubsection->find('first', array(
					'conditions' => array(
						'StockSubsection.code' => intval(preg_replace("/[^0-9\.]/", "", $item[4]))
					)
				));

				if(empty($stocksection)){
					$this->StockItem->StockSection->create();
					$tmp = array(
						'StockSection' => array(
							'name' => $item[2],
							'code' => intval(preg_replace("/[^0-9\.]/", "", $item[1]))
						)
					);
					$this->StockItem->StockSection->save($tmp);
					$stockSectionId = $this->StockItem->StockSection->id;
				} else {
					$stockSectionId = $stocksection['StockSection']['id'];
				}
				if(empty($stocksubsection)){
					$this->StockItem->StockSubsection->create();
					$tmp = array(
						'StockSubsection' => array(
							'name' => $item[3],
							'stock_section_id' => $stockSectionId,
							'code' => intval(preg_replace("/[^0-9\.]/", "", $item[4]))
						)
					);
					$this->StockItem->StockSubsection->save($tmp);
					$stockSubSectionId = $this->StockItem->StockSubsection->id;
				} else {
					$stockSubSectionId = $stocksubsection['StockSubsection']['id'];
				}
				$stockitem = array('StockItem' => array(
					'name' => trim($item[7]),
					'code' => intval(preg_replace("/[^0-9\.]/", "", $item[5])),
					'price' => $item[8],
					'quantity' => intval(preg_replace("/[^0-9\.]/", "", $item[9])),
					'online' => $item[0] == 'ON' ? 1 : 0,
					'status' => 'ok',
					'stock_section_id' => $stockSectionId,
					'stock_subsection_id' => $stockSubSectionId
				));
				if(empty($this->StockItem->find('first', array( 'conditions' => array('code' => intval(preg_replace("/[^0-9\.]/", "", $item[5]))) )))){
					$this->StockItem->create();
					$this->StockItem->save($stockitem);
				}
			}
			exit;
		} else {
			exit;*/
		}
	}

	public function import( $mode = '' ){

		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');

		switch($mode){
			case 'images':
			$dir = new Folder(Configure::read('StockItems.import_images_dir'));
			$images = $dir->find('.*\.jpg');
			$this->set(compact('images'));
			$this->render('import/images');
			break;
			case 'image':
			if($this->request->is('ajax')){
				$this->autoRender = false;
				$filename = $this->request->data['filename'];
				$data = explode('_', $filename);
				$code = $data[0];
				$this->StockItem->contain(array('Document'));
				$stockitem = $this->StockItem->findByCode($code);
				if(!empty($stockitem)){
					$relativeDir = 'files' . DS . 'documents' . DS . 'stockitems' . DS . $stockitem['StockItem']['id'] . DS;
					$absoluteDir = WWW_ROOT . $relativeDir;
					$folder = new Folder($absoluteDir, true, 0777);

					$absoluteFilename = $absoluteDir . $filename;
					$relativeFilename = $relativeDir . $filename;

					$file = new File(WWW_ROOT . 'files' . DS . 'import' . DS . 'images' . DS . $filename);
					if($file->exists()) {
						if($file->copy($relativeDir . DS . $file->name)){

							$existingDocument = $this->StockItem->Document->find('first', array(
								'conditions' => array(
									'Document.name' => $file->name,
                  'Document.parent_id' => $stockitem['StockItem']['id'],
									'Document.category' => 'stockitem'
								)
							));
							$this->StockItem->Document->create();
							$document = array(
								'id' => empty($existingDocument) ? '' : $existingDocument['Document']['id'],
								'name' => $filename,
								'filename' => $filename,
								'url' => $relativeFilename,
								'filesize' => $file->size(),
								'filetype' => $file->mime(),
								'parent_id' => $stockitem['StockItem']['id'],
								'owner' => AuthComponent::user('id'),
								'category' => 'stockitem'
							);
							if($this->StockItem->Document->save(array('Document' => $document))){
								$this->response->body(json_encode(array('success' => 1, 'message' => '')));
								$file->delete();
							} else {
								$this->response->body(json_encode(array('success' => 0, 'message' => '')));
							}
						} else {
							$this->response->body(json_encode(array('success' => 0, 'message' => '')));
						}
					} else {
						$this->response->body(json_encode(array('success' => 0, 'message' => '')));
					}
				} else {
					$this->response->body(json_encode(array('success' => 0, 'message' => 'Pas de produit avec ce code!')));
				}
			}
			break;
		}
	}

	public function images(){
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');

		$dir = new Folder('files/images');
		$images = $dir->find('.*\.jpg');
		foreach($images as $img){
			$data = explode('_', $img);
			$code = $data[0];
			$stockitem = $this->StockItem->find('first', array(
				'conditions' => array(
					'code' => $code
				),
				'contain' => 'Document'
			));
			if(!empty($stockitem)){

				$relativeDir = 'files' . DS . 'documents' . DS . 'stockitems' . DS . $stockitem['StockItem']['id'] . DS;
				$absoluteDir = WWW_ROOT . $relativeDir;
				$folder = new Folder($absoluteDir, true, 0777);

				$absoluteFilename = $absoluteDir . $img;
				$relativeFilename = $relativeDir . $img;

				$file = new File(WWW_ROOT . 'files' . DS . 'images' . DS . $img);

				if($file->exists()) {
					if($file->copy($relativeDir . DS . $file->name)){
						$this->loadModel('Document');
						$existingDocument = $this->Document->find('first', array(
							'conditions' => array(
								'Document.name' => $file->name
							)
						));
						if(empty($existingDocument)){
							$this->Document->create();
							$document = array(
								'name' => $img,
								'filename' => $img,
								'url' => $relativeFilename,
								'filesize' => $file->size(),
								'filetype' => $file->mime(),
								'parent_id' => $stockitem['StockItem']['id'],
								'owner' => AuthComponent::user('id'),
								'category' => 'stockitem'
							);
							$this->Document->save(array('Document' => $document));
						}
					}
				}
			}
		}

		exit;
	}

	public function exportToKameleo($offset = 0) {

		$server = '178.79.163.31';
		$login = 'festilocNew';
		$pass = 'k0#afnSFqeQND';
		$folder = 'database';
		try {
			$conn_id = ftp_connect($server);
		} catch (Exception $e) {
			echo 'Exception reçue : ',  $e->getMessage(), "\n";
		}
		try {
			$login_result = ftp_login($conn_id, $login, $pass);
		} catch (Exception $e) {
			echo 'Exception reçue : ',  $e->getMessage(), "\n";
		}

		if($login_result){
			try {
				ftp_chdir($conn_id, $folder);
			} catch (Exception $e) {
				echo 'Exception reçue : ',  $e->getMessage(), "\n";
			}
			$data = $this->StockItem->find('all', array(
				'conditions' => array(
					'online' => 1,
          'company_id' => 3
				),
				'contain' => 'Document.url',
				'fields' => array('code'),
				'limit' => 500,
				'offset' => $offset
			));
			if(!empty($data)){
				foreach($data as $item){
					$code = $item['StockItem']['code'];
					foreach($item['Document'] as $doc){
						$basename = strtolower(basename($doc['url']));
						if($basename == $code . '_1_s.jpg' OR $basename == $code . '_1_l.jpg'){
							try {
								$fp = fopen($doc['url'], 'r');
								$put = ftp_fput($conn_id, basename($doc['url']), $fp, FTP_BINARY);
							} catch (Exception $e) {
								echo 'Exception reçue : ',  $e->getMessage(), "\n";
							}
							if ($put) {
								//echo "Chargement avec succès du fichier\n";
							} else {
								//echo "Il y a eu un problème lors du chargement du fichier $file\n";
							}
							fclose($fp);
						}
					}
				}
			}
			ftp_close($conn_id);
		} else {
			die();
		}
		exit;

	}

	public function generateQRCodes(){
		ini_set('memory_limit', '-1');
		set_time_limit(0);
		App::uses('File', 'Utility');
		$this->loadModel('MyTools');
		$stockitems = $this->StockItem->find('all', array(
			'joins' => array(
				array(
					'table' => 'stock_inventories_stock_items',
					'alias' => 'sisi',
					'conditions' => array(
						'sisi.stock_item_id = StockItem.id'
					)
				)
			),
			'conditions' => array(
				'sisi.stock_inventory_id' => 1
			),
			'fields' => array('code')
		));
		foreach($stockitems as $item){
			$data = file_get_contents(sprintf('https://api.qrserver.com/v1/create-qr-code/?size=300x300&data=%s', $item['StockItem']['code']));
			$file = new File(WWW_ROOT . 'files' . DS . 'qrcodes' . DS . $item['StockItem']['code'] . '.png');
			$file->write($data);
		}
		exit;
	}


	public function stickers( ){

		ini_set('memory_limit', '-1');
		set_time_limit(0);

		App::uses('CakePdf', 'CakePdf.Pdf');

		$trim = function($val){
			return trim($val);
		};

		$inventories = $this->StockItem->StockInventory->find('list', array(
			'fields' => array('StockInventory.id', 'StockInventory.name')
		));
		$this->set(compact('inventories'));

		$code = empty($this->request->params['named']['code']) ? null : $this->request->params['named']['code'];
		$this->set(compact('code'));

		if($this->request->is('post') || $this->request->is('put')){
			$conditions = array();
			$conditions[] = array('StockItem.online' => 1);
			if(!empty($this->request->data['Sticker']['codes'])){
				$codes = explode(',', $this->request->data['Sticker']['codes']);
				$codes = array_map('trim', $codes);
				$conditions[] = array('StockItem.code' => $codes);
			} elseif(!empty($this->request->data['Sticker']['stock_inventory_id'])) {
				$conditions[] = array(
					'sisi.id <>' => null,
					'sisi.stock_inventory_id' => $this->request->data['Sticker']['stock_inventory_id']
				);
			} else {

			}
			$stockitems = $this->StockItem->find('all', array(
				'joins' => array(
					array(
						'table' => 'stock_inventories_stock_items',
						'alias' => 'sisi',
						'type' => 'left',
						'conditions' => array('sisi.stock_item_id = StockItem.id')
					)
				),
				'contain' => array('Document' => array('order' => 'filesize'), 'StockFamily'),
				'conditions' => $conditions,
				'fields' => array('StockItem.code', 'StockItem.name', 'StockItem.quantity', 'StockFamily.name'),
				'order' => 'StockItem.code'
			));
			if(!empty($stockitems)){
				foreach($stockitems as $k => $item){
					$filename = WWW_ROOT . 'files' . DS . 'qrcodes' . DS . '100x100' . DS . $item['StockItem']['code'] . '.png';
					QRcode::png($item['StockItem']['code'], $filename);
					$stockitems[$k]['StockItem']['qrcode'] = $filename;
				}
			}

			$CakePdf = new CakePdf();
			$CakePdf->template('stickers','default');
			$this->pdfConfig = array(
				'orientation' => 'portrait',
				'filename' => 'denis.pdf'
			);
			$CakePdf->viewVars(array(
				'stockitems' => $stockitems
			));
			$filename = WWW_ROOT . 'files' . DS . 'stickers' . DS . 'stickers.pdf';
			$pdf = $CakePdf->write($filename);
			if($pdf){
				$this->response->file($filename, array('download' => true, 'name' => 'Festiloc étiquettes.pdf'));
			}
		}
	}
}
