<?php

class VehiclesController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Vehicles' => [
				'model' => 'Vehicle',
				'columns' => [
					'name',
					'matriculation',
					'family' => array(
						'useField' => false
					),
					'company' => array(
						'useField' => false
					),
					'model' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'contain' => array(
					'VehicleCompany',
					'VehicleCompanyModel',
				),
				'fields' => array(
					'Vehicle.id',
					'VehicleCompany.name',
					'VehicleCompanyModel.name',
					'VehicleCompanyModel.family'
				),
				'autoData' => false
			]
		],
	];

	public function index() {
		$this->DataTable->setViewVar(array('Vehicles'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Vehicles']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Vehicles']['columns']['matriculation']['label'] = __('Matriculation');
		$this->DataTable->settings['Vehicles']['columns']['company']['label'] = __('Company');
		$this->DataTable->settings['Vehicles']['columns']['model']['label'] = __('Model');
		$this->DataTable->settings['Vehicles']['columns']['family']['label'] = __('Family');
	}

	public function add() {

		$companies = $this->Vehicle->VehicleCompany->find('list');
		$this->set(compact('companies'));

		if($this->request->is('put') || $this->request->is('post')){
			if($this->Vehicle->save($this->request->data)){
				$this->Session->setFlash(__('Vehicle has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Vehicle->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Vehicle has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function edit($id = null){

		$companies = $this->Vehicle->VehicleCompany->find('list');
		$this->set(compact('companies'));

		$models = $this->Vehicle->VehicleCompanyModel->find('list');
		$this->set(compact('models'));

		if($this->request->is('put') || $this->request->is('post')){
			if($this->Vehicle->save($this->request->data)){
				$this->Session->setFlash(__('Vehicle has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Vehicle->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Vehicle has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->request->data = $this->Vehicle->read(null, $id);
		}

	}

	public function get(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->request->data['field'] == 'model'){
				$vehicles = $this->Vehicle->find('all', array(
					'conditions' => array(
						'vehicle_company_model_id' => $_POST['id']
					)
				));
			} elseif($this->request->data['field'] == 'family'){
				$vehicles = $this->Vehicle->find('all', array(
					'joins' => array(
						array(
							'table' => 'vehicle_company_models',
							'alias' => 'vcm',
							'conditions' => array('vcm.id = Vehicle.vehicle_company_model_id')
						)
					),
					'conditions' => array(
						'vcm.family' => $this->request->data['id']
					)
				));
			}

			$this->response->body(json_encode($vehicles));
		}
	}

	public function getAvailable(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$vehicles = $this->Vehicle->getAvailableVehiclesByDate($this->request->data['date']);
			if($vehicles){
				$this->response->body(json_encode(array('vehicles' => $vehicles, 'empty' => 0)));
			} else {
				$this->response->body(json_encode(array('empty' => 1)));
			}
		}

	}

	public function add_reservation(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$data = array(
				'VehicleReservation' => array(
					'vehicle_model_id' => $_POST['vehicle_model_id'],
					'model_id' => $_POST['model_id'],
					'model' => $_POST['model'],
					'type' => $_POST['type'],
					'start' => $_POST['start'],
					'end' => $_POST['end'],
					'start_place' => $_POST['start_place'],
					'end_place' => $_POST['end_place'],
					'confirmed' => 0
				)
			);
			if($this->Vehicle->VehicleReservation->save($data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function reservations(){

		$view = new View($this);
		$html = $view->loadHelper('Html');

		$companies = $this->Vehicle->VehicleCompany->find('all');
		$this->set(compact('companies'));

		$this->loadModel('User');
		$drivers = $this->User->find('list', array(
			'conditions' => array(
				'role' => array('fixed', 'temporary')
			),
			'fields' => array('User.id', 'User.full_name'),
			'order' => array('User.first_name')
		));
		$this->set(compact('drivers'));

		$families = Configure::read('VehiclesTrailers.families');

		if($this->request->is('ajax')){

			$type = $this->request->query['type'];

			$view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
			$ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
			$start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
			$end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
			$start_date = date('Y-m-d', $start);
			$end_date = date('Y-m-d', $end);
			$options = array();

			if ($view == 'month') {
				$month = date('m', $ts);
				$days = date('t', $ts);
				$start_date = date(sprintf('Y-%s-01', $month));
				$end_date = date(sprintf('Y-%s-%s', $month, $days));
				$end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
			}
			$reservations = array();

			if($type == 'reservations'){
				$data = $this->Vehicle->VehicleReservation->find('all', array(
					'contain' => array(
						'Vehicle' => array('fields' => array('name_number')),
						'VehicleTour' => array('fields' => array('code')),
						'VehicleCompanyModel' => array('VehicleCompany' => array('fields' => array('name')), 'fields' => array('name', 'type')),
						'Driver' => array('fields' => array('Driver.full_name')),
						'BackDriver' => array('fields' => array('BackDriver.full_name')),
						'Event' => array(
							'Company' => array('fields' => array('class')),
							'Client' => array('fields' => array('name')),
							'SelectedActivity' => array('Activity' => array('fields' => array('slug'))),
							'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city'))),
							'fields' => array('name', 'code_name', 'confirmed_number_of_persons', 'min_number_of_persons', 'max_number_of_persons', 'type')
							//'PersonInCharge' => array('fields' => array('class'))
						),
					),
					'conditions' => array(
						'VehicleReservation.reason' => null,
						'VehicleReservation.archived' => 0,
						'VehicleReservation.start >=' => $start_date,
						'VehicleReservation.end <=' => $end_date
					)
				));

				$trailerFamilies = Configure::read('Trailers.families');
				$vehicleFamilies = Configure::read('Vehicles.families');

				$startIcon = $html->tag('i', '', array('class' => 'fa fa-arrow-circle-right'));
				$endIcon = $html->tag('i', '', array('class' => 'fa fa-arrow-circle-left'));
				foreach($data as $k => $reservation){
					//$counter = str_replace('-','',date('Y-m-d', strtotime($reservation['VehicleReservation']['start']))) . $reservation['VehicleReservation']['tour_id'];
					$event = array();
					if(!empty($reservation['Event'][0])){
						$event = $reservation['Event'][0];
					}
					$counter = $k;
					$infos = array();
					$title = '';
					$reservations[$counter]['id'] = $reservation['VehicleReservation']['id'];
					$reservations[$counter]['start'] = date('Y-m-d\TH:i:s', strtotime($reservation['VehicleReservation']['start']));
					$reservations[$counter]['end'] = date('Y-m-d\TH:i:s', strtotime($reservation['VehicleReservation']['end']));
					$reservations[$counter]['allDay'] = false;
					$reservations[$counter]['className'] = empty($reservation['VehicleReservation']['confirmed']) ? 'not-confirmed' : 'confirmed';
					$reservations[$counter]['className'] .= ' ' . $reservation['VehicleCompanyModel']['type'];
					if(in_array($reservation['VehicleReservation']['family'], $trailerFamilies)){
						$reservations[$counter]['className'] .= ' trailer';
					}
					if(in_array($reservation['VehicleReservation']['family'], $vehicleFamilies)){
						$reservations[$counter]['className'] .= ' vehicle';
					}

					$this->Vehicle->VehicleReservation->id = $reservation['VehicleReservation']['id'];
					$conflicts = $this->Vehicle->VehicleReservation->searchForConflicts();
					if(!empty($conflicts)){
						$reservations[$counter]['className'] .= ' has-warning';
					}

					if($reservation['VehicleReservation']['cancelled']){
						$reservations[$counter]['className'] .= ' cancelled';
					}

					$hour = date('H:i', strtotime($reservation['VehicleReservation']['start'])) . ' - ' . date('H:i', strtotime($reservation['VehicleReservation']['end']));
					$description = $html->tag('strong', $hour);
					if(!empty($reservation['VehicleReservation']['family'])){
						$description .= $html->tag('span', $families[$reservation['VehicleReservation']['family']], array('class' => 'uppercase'));
					} elseif(!empty($reservation['VehicleCompanyModel']) && !empty($reservation['VehicleCompanyModel']['VehicleCompany'])){
						$description .= $html->tag('span', $reservation['VehicleCompanyModel']['VehicleCompany']['name'] . ' - ' . $reservation['VehicleCompanyModel']['name'], array('class' => 'uppercase'));
					}
					$description .= $html->tag('br');
					if(empty($reservation['VehicleReservation']['vehicle_id'])){
						$title = __('No vehicle');
						$description .= $html->tag('span', __('No vehicle'), array('class' => 'uppercase bg-red-thunderbird', 'style' => 'padding: 0 5px'));
						$reservations[$counter]['className'] .= ' no-vehicle';
					} else {
						$title = $reservation['Vehicle']['name_number'];
						$description .= $html->tag('span', $reservation['Vehicle']['name_number'], array('class' => 'uppercase'));
					}
					$description .= $html->tag('br');
					$description .= empty($reservation['VehicleTour']) ? '' : $html->tag('span', __('Tour') . ' ' . $reservation['VehicleTour'][0]['code'], array('class' => 'uppercase'));

					$description .= empty($reservation['Event'][0]['Client']) ? '' : $html->tag('span', $reservation['Event'][0]['Client']['name'], array('class' => 'uppercase'));
					$description .= empty($reservation['Event']) ? '' : $html->tag('span', $html->tag('br') . $reservation['Event'][0]['code_name'], array('class' => 'uppercase'));
					$activities = '';
					if(!empty($event['SelectedActivity'])){
						foreach($event['SelectedActivity'] as $activity){
							$activities .= sprintf('<i class="fa fa-trophy"></i> %s ', $activity['Activity']['slug']);
						}
						$description .= $html->tag('br') . $activities;
					}
					$description .= $html->tag('br');
					if(!empty($event['ConfirmedEventPlace'])){
						$description .= $event['ConfirmedEventPlace'][0]['Place']['zip_city'];
						$description .= $html->tag('br');
					}
					if(!empty($reservation['VehicleReservation']['start_place'])){
						$description .= $html->tag('br');
						$description .= $html->tag('span', $startIcon . ' ' . $reservation['VehicleReservation']['start_place']);
					}
					if(!empty($reservation['Driver']) && !empty($reservation['Driver']['full_name'])){
						$description .= $html->tag('br');
						$description .= $html->tag('span', $reservation['Driver']['full_name']);
					}
					if(!empty($reservation['VehicleReservation']['end_place'])){
						$description .= $html->tag('br');
						$description .= $html->tag('span', $endIcon . ' ' . $reservation['VehicleReservation']['end_place']);
					}
					if(!empty($reservation['BackDriver']) && !empty($reservation['BackDriver']['full_name'])){
						$description .= $html->tag('br');
						$description .= $html->tag('span', $reservation['BackDriver']['full_name']);
					}
					if(!empty($reservation['VehicleTour'])){
						$reservations[$counter]['company'] = 'festiloc';
						$infos[] = '<span class="bold uppercase">Tournée</span> ' . __('Vehicle tour %s', $reservation['VehicleTour'][0]['code']);
					}
					if(!empty($reservation['Event'][0]['Company'])){
						//$description .= sprintf('<span class="btn btn-xs btn-company btn-company-%s"></span>', $reservation['Event'][0]['Company']['class']);
						$reservations[$counter]['company'] = $reservation['Event'][0]['Company']['class'];
						if($event['code_name']){
							$infos[] = '<span class="bold uppercase">Event</span> ' . $event['code_name'];
						} elseif($event['type'] == 'mission'){
							$infos[] = '<span class="bold uppercase">Mission</span> ' . $event['name'];
						}	else {
							$infos[] = $event['name'];
						}
						if($event['Client']) $infos[] = $event['Client']['name'];
					}
					$reservations[$counter]['description'] = $description;
					$reservations[$counter]['url'] = Router::url(
						array('controller' => 'vehicle_reservations', 'action' => 'detail', $reservation['VehicleReservation']['id'], empty($reservation['VehicleTour']) ? '' : 3)
					);
					if(!empty($activities)) $infos[] = $activities;
					if(!empty($event['ConfirmedEventPlace'])){
						$infos[] = $event['ConfirmedEventPlace'][0]['Place']['zip_city'];
					}
					$reservations[$counter]['title'] = $title;
					$reservations[$counter]['infos'] = implode(' | ', $infos);
					$reservations[$counter]['available'] = true;
				}

			} elseif($type == 'unavailabilities'){
				$reasons = Configure::read('VehicleReservations.reasons');
				$data = $this->Vehicle->VehicleReservation->find('all', array(
					'contain' => array(
						'Vehicle',
						'VehicleCompanyModel' => array('VehicleCompany')
					),
					'conditions' => array(
						'VehicleReservation.reason <>' => null
					)
				));
				foreach($data as $k => $reservation){
					$reservations[$k]['id'] = $k;
					$reservations[$k]['title'] = $reservation['Vehicle']['name_number'];
					$reservations[$k]['title'] .= ' (' . __($reasons[$reservation['VehicleReservation']['reason']]) . ')';
					$reservations[$k]['start'] = date('Y-m-d\TH:i:s', strtotime($reservation['VehicleReservation']['start']));
					$reservations[$k]['end'] = date('Y-m-d\TH:i:s', strtotime($reservation['VehicleReservation']['end']));
					$reservations[$k]['className'] = 'unavailable';
					$reservations[$k]['allDay'] = false;
					$reservations[$k]['available'] = false;
					$reservations[$k]['url'] = Router::url(
						array('controller' => 'vehicle_reservations', 'action' => 'detail', $reservation['VehicleReservation']['id'])
					);
					$reservations[$k]['description'] = $reservations[$k]['title'];
				}
			}
			sort($reservations);
			$this->set('_json', $reservations);

		}
	}

	public function plan($company_id, $start = null){

		if(empty($start)){
			$start = strtotime('monday this week');
		} else {
			$start = strtotime($start);
		}

		$prev = strtotime("-7 days", $start);
		$next = strtotime("+7 days", $start);
		$previousWeek = date("Y-m-d", $prev);
		$nextWeek = date("Y-m-d", $next);

		$this->loadModel('StockOrder');
		$this->loadModel('Vehicle');

		$models['vehicles'] = $this->Vehicle->VehicleCompanyModel->find('list', array(
			'contain' => array('VehicleCompany'),
			'fields' => array('VehicleCompanyModel.id', 'VehicleCompanyModel.name', 'VehicleCompany.name'),
			'conditions' => array(
				'type' => 'vehicle'
			)
		));

		$models['trailers'] = $this->Vehicle->VehicleCompanyModel->find('list', array(
			'contain' => array('VehicleCompany'),
			'fields' => array('VehicleCompanyModel.id', 'VehicleCompanyModel.name', 'VehicleCompany.name'),
			'conditions' => array(
				'type' => 'trailer'
			)
		));
		$this->set(compact('models'));

		if($company_id == 3){ // festiloc
			$end = date('Y-m-d', strtotime("+6 days", strtotime($start)));
			// we need to retrieve all orders which transporter is festiloc and the tour is set
			$deliveries = $this->StockOrder->find('all', array(
				'conditions' => array(
					'status NOT' => array('offer', 'cancelled', 'invoiced', 'to_invoice'),
					'delivery' => 1,
					'OR' => array(
						array('delivery_transporter_id' => 1), // festiloc
						array('delivery_mode' => 'festiloc')
					),
					'delivery_date >=' => $start,
					'delivery_date <=' => $end
				),
				'contain' => array(
					'Client',
					'DeliveryTour'
				)
			));
			$returns = $this->StockOrder->find('all', array(
				'conditions' => array(
					'status NOT' => array('offer', 'cancelled', 'invoiced', 'to_invoice'),
					'return' => 1,
					'OR' => array(
						array('return_transporter_id' => 1), // festiloc
						array('return_mode' => 'festiloc')
					),
					'return_date >=' => $start,
					'return_date <=' => $end
				),
				'contain' => array(
					'Client',
					'ReturnTour'
				)
			));
			$orders = array();
			$codes = array();
			$counter = 0;
			foreach($deliveries as $k => $order){
				if(empty($order['DeliveryTour'])){
					continue;
				}
				$weight = $order['DeliveryTour'][0]['StockOrdersVehicleTour']['weight'];
				if(is_null($weight)) $weight = 100 + $k;

				$this->StockOrder->id = $order['StockOrder']['id'];
				$order['StockOrder']['packaging'] = $this->StockOrder->getPackaging();

				$order['mode'] = 'delivery';
				$orders[$order['StockOrder']['delivery_date']][$order['DeliveryTour'][0]['id']][$weight] = $order;
				$codes[$order['DeliveryTour'][0]['id']] = $order['DeliveryTour'][0]['code'];
			}
			foreach($returns as $k => $order){
				if(empty($order['ReturnTour'])){
					continue;
				}
				$weight = $order['ReturnTour'][0]['StockOrdersVehicleTour']['weight'];
				if(is_null($weight)) $weight = 100 + $k;

				$this->StockOrder->id = $order['StockOrder']['id'];
				$order['StockOrder']['packaging'] = $this->StockOrder->getPackaging();

				$order['mode'] = 'return';
				$orders[$order['StockOrder']['return_date']][$order['ReturnTour'][0]['id']][$weight] = $order;
				$codes[$order['ReturnTour'][0]['id']] = $order['ReturnTour'][0]['code'];
			}
			ksort($orders);
			$reservations = array();
			$routes = array();
			foreach($orders as $date => $tours){
				foreach($tours as $tour => $orders1){
					$route = "https://www.google.ch/maps/dir/Festiloc'+S%C3%A0rl,+Route+du+Tir-F%C3%A9d%C3%A9ral,+Givisiez";
					foreach($orders1 as $order){
						// generate google maps route

						if($order['mode'] == 'delivery'){
							$route .= '/' . urlencode($order['StockOrder']['delivery_address']);
							$route .= ',' . urlencode($order['StockOrder']['delivery_zip_city']);
						}

						if($order['mode'] == 'return'){
							$route .= '/' . urlencode($order['StockOrder']['return_address']);
							$route .= ',' . urlencode($order['StockOrder']['return_zip_city']);
						}

						// search for reservations
						$options = array();
						$options['joins'] = array(
							array(
								'table' => 'vehicle_reservations_vehicle_tours',
								'alias' => 'VRVT',
								'conditions' => array(
									'VRVT.vehicle_tour_id = VehicleTour.id'
								)
							),
							array(
								'table' => 'vehicle_reservations',
								'alias' => 'VehicleReservation',
								'conditions' => array(
									'VehicleReservation.id = VRVT.vehicle_reservation_id'
								)
							),
							array(
								'table' => 'vehicle_company_models',
								'alias' => 'VehicleCompanyModel',
								'conditions' => array(
									'VehicleCompanyModel.id = VehicleReservation.vehicle_model_id'
								)
							),
							array(
								'table' => 'vehicles',
								'alias' => 'Vehicle',
								'conditions' => array(
									'Vehicle.id = VehicleReservation.vehicle_id'
								)
							)
						);
						$options['fields'] = array(
							'VehicleCompanyModel.id',
							'VehicleReservation.id',
							'VehicleReservation.confirmed',
							'Vehicle.id',
							'VehicleReservation.start',
							'VehicleReservation.start_place',
							'VehicleReservation.end',
							'VehicleReservation.end_place'
						);
						$options['conditions'] = array(
							'VehicleTour.id' => $tour,
							'VehicleCompanyModel.type' => 'vehicle'
						);

						$vehicleReservation = $this->Vehicle->VehicleReservation->VehicleTour->find('first', $options);
						if(!empty($vehicleReservation)){
							$reservations[$date][$tour]['vehicle'] = $vehicleReservation;
						}


						$options['conditions'] = array(
							'VehicleTour.id' => $tour,
							'VehicleCompanyModel.type' => 'trailer'
						);
						$trailerReservation = $this->Vehicle->VehicleReservation->VehicleTour->find('first', $options);
						if(!empty($trailerReservation)){
							$reservations[$date][$tour]['trailer'] = $trailerReservation;
						}
					}
					$route .= "/Festiloc'+S%C3%A0rl,+Route+du+Tir-F%C3%A9d%C3%A9ral,+Givisiez";
					$routes[$date][$tour] = $route;
				}
			}
			$this->set(compact('orders'));
			$this->set(compact('codes'));
			$this->set(compact('routes'));
			$this->set(compact('reservations'));
			$this->set(compact('previousWeek'));
			$this->set(compact('nextWeek'));
			$this->set(compact('company_id'));
		}


		if($company_id == 2){ // UBIC

		}
	}


}
