<?php

class JobBriefingsController extends AppController {

  public function modal( $id = null, $eventId = null ){
    if($this->request->is('ajax')){
      $jobs = $this->JobBriefing->Job->find('list', array(
        'conditions' => array(
          'event_id' => $eventId
        )
      ));
      $this->set(compact('jobs'));
      if(!empty($id)){
        $briefing = $this->JobBriefing->findById($id);
        $this->set(compact('briefing'));
      }
    }
  }

  public function edit( $id = null ){

    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->JobBriefing->save($this->request->data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
    }

  }

	public function delete(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if($this->JobBriefing->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}
  
}
