<?php

class StockItemUnavailabilitiesController extends AppController {

	public function add(){
		if($this->request->is('post') OR $this->request->is('put')){
			if(!empty($this->request->data['StockItemUnavailability']['start_date'])){
				$this->request->data['StockItemUnavailability']['start_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['start_date']));
			}
			if(!empty($this->request->data['StockItemUnavailability']['end_date'])){
				$this->request->data['StockItemUnavailability']['end_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['end_date']));
			}
			if($this->StockItemUnavailability->save($this->request->data)){
				$stockitemId = $this->StockItemUnavailability->field('stock_item_id');
				$this->Session->setFlash(__('Unavailability has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'edit', $stockitemId));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Unavailability has not been saved.'), 'alert', array('type' => 'danger'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'edit', $stockitemId));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				}
			}

		}
	}

	public function edit($id = null){
		if($this->request->is('post') OR $this->request->is('put')){
			if(!empty($this->request->data['StockItemUnavailability']['start_date'])){
				$this->request->data['StockItemUnavailability']['start_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['start_date']));
			}
			if(!empty($this->request->data['StockItemUnavailability']['end_date'])){
				$this->request->data['StockItemUnavailability']['end_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['end_date']));
			}
			if($this->StockItemUnavailability->save($this->request->data)){
				$stockitemId = $this->StockItemUnavailability->field('stock_item_id');
				$this->Session->setFlash(__('Unavailability has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'edit', $stockitemId));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Unavailability has not been saved.'), 'alert', array('type' => 'danger'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'edit', $stockitemId));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				}
			}

		} else {
			$unavailability = $this->StockItemUnavailability->read(null, $id);
			$stockitem = $this->StockItemUnavailability->StockItem->findById($unavailability['StockItemUnavailability']['stock_item_id']);
			$this->set(compact('stockitem'));
			$this->request->data = $unavailability;
			if(!empty($this->request->data['StockItemUnavailability']['start_date'])){
				$this->request->data['StockItemUnavailability']['start_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['start_date']));
			}
			if(!empty($this->request->data['StockItemUnavailability']['end_date'])){
				$this->request->data['StockItemUnavailability']['end_date'] = date('Y-m-d', strtotime($this->request->data['StockItemUnavailability']['end_date']));
			}
		}
	}

	public function delete($id = null){
		if(!empty($id)){
			$unavailability = $this->StockItemUnavailability->findById($id);
			if($this->StockItemUnavailability->delete($id)){
				$this->Session->setFlash(__('Unavailability has been deleted.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('controller' => 'stock_items', 'action' => 'edit', $unavailability['StockItemUnavailability']['stock_item_id']));
			}
		} else {

		}
	}

	public function reconditionning(){
		$date = date('Y-m-d');
		$data = $this->StockItemUnavailability->find('all', array(
			'conditions' => array(
				'end_date' => $date,
				'status' => 'confirmed'
			)
		));
		foreach($data as $item){
			$this->StockItemUnavailability->create();
			$start_date = $item['StockItemUnavailability']['end_date'];
			$end_date = date('Y-m-d', strtotime( $start_date . "+ 4 weekday"));
			$temp = array('StockItemUnavailability' => array(
				'status' => 'reconditionning',
				'start_date' => $start_date,
				'end_date' => $end_date,
				'quantity' => $item['StockItemUnavailability']['quantity'],
				'stock_item_id' => $item['StockItemUnavailability']['stock_item_id'],
				'stock_order_id' => $item['StockItemUnavailability']['stock_order_id']
			));
			$this->StockItemUnavailability->save($temp);
		}
		exit;
	}

	public function clean(){
		// $data = $this->StockItemUnavailability->find('all', array(
		// 	'conditions' => array(
		// 		'StockItemUnavailability.end_date <' => date('Y-m-d')
		// 	)
		// ));
		// if(!empty($data)){
		// 	foreach($data as $d){
		// 		$this->StockItemUnavailability->delete($d['StockItemUnavailability']['id']);
		// 	}
		// }
		exit;
	}

	public function adapt(){
		$timestart=microtime(true);
		$today = date('Y-m-d');
		$data = $this->StockItemUnavailability->find('all', array(
			'conditions' => array(
				'start_date >=' => $today,
				'status' => 'reconditionning'
			)
		));
		foreach($data as $item){
			if(empty($item['StockItemUnavailability']['stock_order_id'])){
				continue;
			}
			$order = $this->StockItemUnavailability->StockOrder->find('first', array(
				'conditions' => array(
					'id' => $item['StockItemUnavailability']['stock_order_id']
				),
				'fields' => array('return_date')
			));
			$return_date = $order['StockOrder']['return_date'];
			$day = date('N', strtotime($order['StockOrder']['return_date']));
			$offset1 = 1;
			$offset2 = 4;
			switch($day){
				case 1:
					$offset2 = 2;
					break;
				case 2:
					$offset2 = 1;
					break;
				case 3:
					$offset2 = 4;
					break;
				case 4:
					$offset2 = 3;
					break;
				case 5:
				case 6:
				case 7:
					$offset2 = 2;
					break;
				default:
				break;
			}
			$start_date = date('Y-m-d', strtotime($return_date . sprintf(' +%d weekdays', $offset1)));
			$end_date = date('Y-m-d', strtotime($return_date . sprintf(' +%d weekdays', $offset2)));
			$item['StockItemUnavailability']['start_date'] = $start_date;
			$item['StockItemUnavailability']['end_date'] = $end_date;
			//debug($item);
			$this->StockItemUnavailability->save($item);
		}
		$batches = $this->StockItemUnavailability->StockOrder->StockOrderBatch->find('all', array(
			'contain' => array('StockOrder' => array('fields' => array('return_date'))),
			'conditions' => array(
				'StockOrder.status' => array('offer', 'confirmed'),
				'StockOrder.return_date >=' => $today
			),
			'fields' => array('id', 'reprocessing_duration', 'stock_item_id')
		));
		foreach($batches as $batch){
			$duration = 4; // default
			if(!empty($batch['StockOrder']['return_date'])){
				$day = date('N', strtotime($batch['StockOrder']['return_date']));
				switch($day){
					case 1:
					$duration = 2;
					break;
					case 2:
					$duration = 1;
					break;
					case 3:
					$duration = 4;
					break;
					case 4:
					$duration = 3;
					break;
					case 5:
					$duration = 2;
					break;
					case 6:
					$duration = 2;
					break;
					case 7:
					$duration = 2;
					break;
				}
			}
			if($batch['StockOrderBatch']['stock_item_id'] != -1){
				$item = $this->StockItemUnavailability->StockOrder->StockOrderBatch->StockItem->find('first', array(
					'conditions' => array(
						'id' => $batch['StockOrderBatch']['stock_item_id']
					),
					'fields' => array('default_reconditionning_duration')
				));
			} else {
				$item = array();
			}
			if(!empty($item['StockItem']['default_reconditionning_duration']) && $duration > $item['StockItem']['default_reconditionning_duration']){
				$duration = $item['StockItem']['default_reconditionning_duration'];
			}
			$batch['StockOrderBatch']['reprocessing_duration'] = $duration;
			$this->StockItemUnavailability->StockOrder->StockOrderBatch->save($batch);
		}
		$timeend=microtime(true);
		$time=$timeend-$timestart;
		$page_load_time = number_format($time, 3);
		echo "<br>Script execute en " . $page_load_time . " sec";
		exit;
	}

}
