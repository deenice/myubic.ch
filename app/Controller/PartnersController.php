<?php

class PartnersController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Partners' => [
				'model' => 'ContactPeople',
				'joins' => array(
					array(
						'table' => 'clients',
						'alias' => 'Client1',
						'type' => 'inner',
						'conditions' => array(
							'Client1.id = ContactPeople.client_id',
							'Client1.type = "partner"'
						)
					),
					array(
						'table' => 'clients_companies',
						'alias' => 'ClientCompany',
						'type' => 'inner',
						'conditions' => array(
							'ClientCompany.client_id = Client1.id'
						)
					)
				),
				'columns' => [
					'Partner.name' => array(
					  'bSearchable' => 'customSearchContactPeople'
					),
					'name' => array(
						'bSearchable' => 'customSearchContactPeople'
					),
					'address' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'conditions' => array(
					'ContactPeople.status' => 1,
					'ClientCompany.company_id <>' => 3
				),
				'contain' => array(
					'Partner'
				),
				'fields' => array(
					'Partner.name', 'Partner.address', 'Partner.zip', 'Partner.city', 'Partner.country', 'ContactPeople.full_name', 'ContactPeople.phone', 'ContactPeople.email'
				),
				'autoData' => false
			]
		]
	];

	public function index(){
		$this->DataTable->setViewVar(array('Partners'));
	}

	public function beforeFilter(){
		parent::beforeFilter();
		$this->DataTable->settings['Partners']['columns']['Partner.name']['label'] = __('Partner');
		$this->DataTable->settings['Partners']['columns']['name']['label'] = __('Contact person');
		$this->DataTable->settings['Partners']['columns']['address']['label'] = __('Address');
	}

	public function add( ) {

		$this->set('companies', $this->Partner->Company->find('list'));

		if($this->request->is('post') OR $this->request->is('put')){

			if(!empty($this->request->data['Partner']['companies'])){
				foreach($this->request->data['Partner']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}

			if($this->Partner->saveAssociated($this->request->data)){
				if(!empty($this->request->data['Partner']['logo']['tmp_name'])){
					$this->User->Document->saveFile($this->request->data['Partner']['logo'], $this->Partner->id, array('partners','logo'), 'partner_logo', Configure::read('Documents.Images.allowedExtensions'));
				}
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Partner']['name']), 'alert', array('type' => 'success'));
				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Partner->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'partner_id' => $this->Partner->id));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function edit($id = null){

		$this->Partner->id = $id;
		$this->set('companies', $this->Partner->Company->find('list'));

		$clientCompanies = $this->Client->Company->find('list', array(
			'joins' => array(
				array(
					'table' => 'clients_companies',
					'alias' => 'ClientCompany',
					'type' => 'INNER',
					'conditions' => array(
						'ClientCompany.company_id = Company.id'
					)
				),
				array(
					'table' => 'clients',
					'alias' => 'Client',
					'type' => 'INNER',
					'conditions' => array(
						'Client.id = ClientCompany.client_id'
					)
				)
			),
			'conditions' => array(
				'Client.id' => $id
			),
			'fields' => array('Company.id')
		));
		$this->set(compact('clientCompanies'));

		if($this->request->is('post') OR $this->request->is('put')){

			if(!empty($this->request->data['Partner']['logo']['tmp_name'])){
				$this->Partner->Document->saveFile($this->request->data['Partner']['logo'], $id, array('partners','logo'), 'partner_logo', Configure::read('Documents.Images.allowedExtensions'));
			}

			if(!empty($this->request->data['Partner']['companies'])){
				foreach($this->request->data['Partner']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}

			if($this->Partner->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Partner']['name']), 'alert', array('type' => 'success'));

				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Partner->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'partner_id' => $this->Partner->id));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		} else {
			$partner = $this->Partner->find('first', array(
				'conditions' => array(
					'Partner.id' => $id
				),
				'contain' => array(
					'ContactPeople',
					'Logo'
				)
			));
			$this->request->data = $partner;
		}
	}

}
