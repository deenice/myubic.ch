<?php

class StockOrderBatchesController extends AppController {

	public function validateStep($id){
		$this->StockOrderBatch->id = $id;
		if($this->StockOrderBatch->saveField('processed', 1)){
			$message = 'saved';
		} else {
			$message = 'error';
		}
		$this->StockOrderBatch->StockOrder->id = $this->StockOrderBatch->field('stock_order_id');
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message')
		));
	}

	public function delete(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if($this->StockOrderBatch->delete($_POST['batch_id'], false)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function update(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->StockOrderBatch->id = $_POST['id'];
			if($this->StockOrderBatch->saveField('invoice_replacement', $_POST['invoice_replacement'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateField(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->StockOrderBatch->id = $this->request->data['id'];
			if($this->StockOrderBatch->saveField($this->request->data['field'], $this->request->data['value'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateWeights(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if(!empty($_POST['rows'])){
				foreach($_POST['rows'] as $row){
					$this->StockOrderBatch->id = $row['id'];
					$this->StockOrderBatch->saveField('weight', $row['weight']);
				}
				$this->response->body(json_encode(array('success' => 1)));
			}
		}
	}

}