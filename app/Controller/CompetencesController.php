<?php

class CompetencesController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['to_delete'])){
				$this->Competence->deleteAll(array(
					'Competence.id' => $this->request->data['to_delete']
				));
			}
			$data = array(
				'Competence' => array(
					'id' => !empty($this->request->data['id']) ? $this->request->data['id'] : '',
					'user_id' => !empty($this->request->data['user_id']) ? $this->request->data['user_id'] : '',
					'sector' => !empty($this->request->data['sector']) ? $this->request->data['sector'] : '',
					'hierarchy' => $this->request->data['hierarchy'] >= 0 ? $this->request->data['hierarchy'] : '',
					'activity_id' => !empty($this->request->data['activity_id']) ? $this->request->data['activity_id'] : '',
					'fb_module_id' => !empty($this->request->data['fb_module_id']) ? $this->request->data['fb_module_id'] : '',
					'job' => !empty($this->request->data['job']) ? $this->request->data['job'] : ''
				)
			);
			$this->Competence->create();
			if($this->Competence->save($data)){
				$this->response->body(json_encode(array('success' => 1, 'id' => $this->Competence->id)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function save(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if(empty($_POST['amount'])){
				$amount = 1;
			} else {
				$amount = $_POST['amount'];
			}
			$i=0;
			$output = array();
			while($i < $amount && $i >= 0){
				$i++;
				$data = array(
					'Competence' => array(
						'id' => !empty($_POST['id']) ? $_POST['id'] : '',
						'user_id' => !empty($_POST['user_id']) ? $_POST['user_id'] : '',
						'event_id' => !empty($_POST['event_id']) ? $_POST['event_id'] : '',
						'sector' => $_POST['sector'],
						'hierarchy' => $_POST['hierarchy'],
						'activity_id' => !empty($_POST['activity']) ? $_POST['activity'] : '',
						'job' => $_POST['job'],
						'name' => $_POST['name']
					)
				);
				if(empty($_POST['id'])){
					$this->Competence->create();
				} else {
					$this->Competence->id = $_POST['id'];
				}
				if($this->Competence->save($data)){
					$output['success'] = 1;
					$output['id'] = $this->Competence->id;
				} else {
					$output['success'] = 0;
				}
			}
			$this->response->body(json_encode($output));
		}
	}

	public function delete(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if($this->Competence->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		}
	}

	public function evaluations( $company = null){
		$extras = $this->User->JobUser->find('all', array(
			'joins' => array(
				array(
					'table' => 'users',
					'alias' => 'u',
					'conditions' => array(
						'u.id = JobUser.user_id'
					)
				),
				array(
					'table' => 'jobs',
					'alias' => 'j',
					'conditions' => array(
						'j.id = JobUser.job_id'
					)
				),
				array(
					'table' => 'events',
					'alias' => 'e',
					'conditions' => array(
						'j.event_id = e.id'
					)
				),
				array(
					'table' => 'clients',
					'alias' => 'c',
					'conditions' => array(
						'c.id = e.client_id'
					)
				),
				array(
					'table' => 'companies',
					'alias' => 'co',
					'conditions' => array(
						'co.id = e.company_id'
					)
				),
				array(
					'table' => 'activities',
					'alias' => 'a',
					'type' => 'left',
					'conditions' => array(
						'a.id = j.activity_id'
					)
				),
				array(
					'table' => 'fb_modules',
					'alias' => 'fb',
					'type' => 'left',
					'conditions' => array(
						'fb.id = j.fb_module_id'
					)
				),
				array(
					'table' => 'competences',
					'alias' => 'cp',
					'conditions' => array(
						'cp.user_id = u.id AND cp.job = j.job AND cp.hierarchy = j.hierarchy AND (cp.activity_id = a.id OR cp.fb_module_id = fb.id)'
					)
				)
			),
			'conditions' => array(
				'e.confirmed_date <' => date('Y-m-d'),
				'e.confirmed_date >=' => date('2016-01-01'),
				'j.hierarchy <>' => 4,
				'j.job <>' => null,
				'j.job <>' => array('event_manager', 'bar_manager', 'mg_event_manager', 'mg_event_assistant', 'mg_service_crew', 'mg_test', 'mg_deliverer', 'fondue_kit', 'burger_party', 'griller', 'regional_picnic'),
				'JobUser.evaluation' => 'none',
				'u.role' => array('temporary', 'fixed'),
				'e.code <>' => null
			),
			'fields' => array(
				'JobUser.id', 'j.*', 'u.id', 'u.first_name', 'u.last_name', 'e.code', 'e.name', 'e.confirmed_date', 'c.name', 'a.name', 'fb.name', 'co.class'
			),
			'order' => 'e.confirmed_date'
		));
		// debug(sizeof($extras));
		//	debug($extras);exit;
		$this->set(compact('extras'));
		$this->set('hierarchies', Configure::read('Competences.hierarchies'));
		$this->set('jobs', Configure::read('Competences.jobs'));
	}

	public function evaluate( $job = null, $job_user_id = null, $sector = null, $model_id = null, $rise = false ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->loadModel('JobUser');
			$this->JobUser->contain(array(
				'Job'
			));
			$jobUser = $this->JobUser->findById($job_user_id);
			switch($sector){
				case 'animation':
					$competence = $this->Competence->find('first', array(
						'conditions' => array(
							'user_id' => $jobUser['JobUser']['user_id'],
							'job' => $job,
							'sector' => $sector,
							'activity_id' => $model_id
						)
					));
					if(!empty($competence) && $competence['Competence']['hierarchy'] == $jobUser['Job']['hierarchy']){
						$hierarchy = $competence['Competence']['hierarchy'];
						$competence['Competence']['hierarchy'] = $hierarchy + 1;
					}
				break;
				case 'fb':
					$competence = $this->Competence->find('first', array(
						'conditions' => array(
							'user_id' => $jobUser['JobUser']['user_id'],
							'job' => $job,
							'sector' => $sector,
							'fb_module_id' => $model_id
						)
					));
					if(!empty($competence) && $competence['Competence']['job'] == $jobUser['Job']['job']){
						switch($job){
							case 'test':
							$competence['Competence']['job'] = 'service_crew';
							break;
							case 'service_crew':
							$competence['Competence']['job'] = 'event_assistant';
							break;
							case 'event_assistant':
							$competence['Competence']['job'] = 'event_manager';
							break;
							case 'bar_crew':
							$competence['Competence']['job'] = 'bar_manager';
							break;
						}
					}
				break;
				default:
					$competence = $this->Competence->find('first', array(
						'conditions' => array(
							'user_id' => $jobUser['JobUser']['user_id'],
							'job' => $job,
							'sector' => $sector
						)
					));
					if(!empty($competence) && $competence['Competence']['hierarchy'] == $jobUser['Job']['hierarchy']){
						$hierarchy = $competence['Competence']['hierarchy'];
						$competence['Competence']['hierarchy'] = $hierarchy + 1;
					}
				break;
			}
			if($rise){
				$jobUser['JobUser']['evaluation'] = 'rised';
				if($this->Competence->save($competence) && $this->JobUser->save($jobUser)){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			} else {
				$jobUser['JobUser']['evaluation'] = 'same';
				if($this->JobUser->save($jobUser)){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}
}
