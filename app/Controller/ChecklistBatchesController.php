<?php

class ChecklistBatchesController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'All' => [
				'model' => 'ChecklistBatch',
				'columns' => [
					'name',
					'checklist' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'joins' => array(
					array(
						'table' => 'checklists',
						'alias' => 'c',
						'conditions' => 'c.id = ChecklistBatch.checklist_id'
					)
				),
				'conditions' => array(
					'c.default' => 1
				),
				'contain' => array('Checklist'),
				'fields' => array(
					'Checklist.name', 'ChecklistBatch.id'
				),
				'order' => array('c.id', 'ChecklistBatch.weight'),
				'autoData' => false
			]
		],
	];

	public function index(){
		$this->DataTable->setViewVar(array('All'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['All']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['All']['columns']['checklist']['label'] = __('Checklist');
	}

	public function add(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$batch = array(
				'ChecklistBatch' => array(
					'name' => $this->request->data['name'],
					'checklist_id' => $this->request->params['named']['checklist_id']
				)
			);
			if($this->ChecklistBatch->save($batch)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		} elseif ($this->request->is('post') || $this->request->is('put')) {

			if(!empty($this->request->data['ChecklistBatch']['neighbor'])){
				$batch = $this->ChecklistBatch->findById($this->request->data['ChecklistBatch']['neighbor']);
				if($this->request->data['ChecklistBatch']['position']){ // before
					$this->request->data['ChecklistBatch']['weight'] = $batch['ChecklistBatch']['weight'] - 1;
				} else {
					$this->request->data['ChecklistBatch']['weight'] = $batch['ChecklistBatch']['weight'] + 1;
				}
			} else {
				$batch = $this->ChecklistBatch->find('first', array(
					'conditions' => array(
						'checklist_id' => $this->request->data['ChecklistBatch']['checklist_id']
					),
					'order' => 'weight DESC'
				));
				if(!empty($batch)){
					$this->request->data['ChecklistBatch']['neighbor'] = $batch['ChecklistBatch']['id'];
				}	else {
					$this->request->data['ChecklistBatch']['neighbor'] = null;
				}
				$this->request->data['ChecklistBatch']['position'] = 1;
				$this->request->data['ChecklistBatch']['weight'] = 9999;
			}

			$this->ChecklistBatch->create();
			if ($this->ChecklistBatch->save($this->request->data)) {
				$this->Session->setFlash(__('ChecklistBatch has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->ChecklistBatch->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Artist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->set('checklists', $this->ChecklistBatch->Checklist->find('list', array(
				'conditions' => array(
					'default' => 1
				),
				'order' => 'name',
				'fields' => array('Checklist.id', 'Checklist.name')
			)));
			if(!empty($this->request->params['named']['checklist_id'])){
				$checklist = $this->ChecklistBatch->Checklist->findById($this->request->params['named']['checklist_id']);
				$this->set(compact('checklist'));
				$batchs = $this->ChecklistBatch->find('list', array(
					'conditions' => array(
						'checklist_id' => $this->request->params['named']['checklist_id']
					),
					'order' => 'weight',
					'fields' => array('ChecklistBatch.id', 'ChecklistBatch.name')
				));
				$this->set(compact('phases'));
			}
		}
	}

	public function edit( $id = null ){

		$this->ChecklistBatch->id = $id;

		if (!$this->ChecklistBatch->exists()) {
			throw new NotFoundException(__('Invalid ChecklistBatch'));
		}

		$this->set('checklists', $this->ChecklistBatch->Checklist->find('list', array(
			'conditions' => array(
				'default' => 1
			),
			'order' => 'name',
			'fields' => array('Checklist.id', 'Checklist.name')
		)));

		if ($this->request->is('post') || $this->request->is('put')) {

			if(!empty($this->request->data['ChecklistBatch']['neighbor'])){
				$batch = $this->ChecklistBatch->findById($this->request->data['ChecklistBatch']['neighbor']);
				if($this->request->data['ChecklistBatch']['position']){ // before
					$this->request->data['ChecklistBatch']['weight'] = $batch['ChecklistBatch']['weight'] - 1;
				} else {
					$this->request->data['ChecklistBatch']['weight'] = $batch['ChecklistBatch']['weight'] + 1;
				}
			} else {
				$this->request->data['ChecklistBatch']['weight'] = 9999;
			}
			if ($this->ChecklistBatch->save($this->request->data)) {
				$this->Session->setFlash(__('ChecklistBatch has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->ChecklistBatch->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('ChecklistBatch has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$batch = $this->ChecklistBatch->findById($id);
			$this->set(compact('phase'));
			$this->request->data = $batch;
			if(!empty($batch['ChecklistBatch']['checklist_id'])){
				$batchs = $this->ChecklistBatch->find('list', array(
					'conditions' => array(
						'checklist_id' => $batch['ChecklistBatch']['checklist_id']
					),
					'order' => 'weight',
					'fields' => array('ChecklistBatch.id', 'ChecklistBatch.name')
				));
				$this->set(compact('phases'));
			}
		}

	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->ChecklistBatch->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function progress(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['id']) && $this->request->data['key'] == 'task'){
				$task = $this->ChecklistBatch->ChecklistTask->findById($this->request->data['id']);
				if(!empty($task)){
					$batch = $this->ChecklistBatch->findById($task['ChecklistTask']['checklist_batch_id']);
					$this->response->body(json_encode(array('success' => 1, 'progress' => $batch['ChecklistBatch']['progress'], 'closed_tasks' => $batch['ChecklistBatch']['closed_tasks'])));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			} elseif(!empty($this->request->data['id']) && $this->request->data['key'] == 'batch'){
				$batch = $this->ChecklistBatch->findById($this->request->data['id']);
				$this->response->body(json_encode(array('success' => 1, 'progress' => $batch['ChecklistBatch']['progress'], 'closed_tasks' => $batch['ChecklistBatch']['closed_tasks'])));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function html(){
		if($this->request->is('ajax')){
			$this->ChecklistBatch->contain(array(
				'ChecklistTask'
			));
			$batch = $this->ChecklistBatch->findById($this->request->data['id']);
			$this->set(compact('batch'));
		}
	}

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['batches'])){
				foreach($this->request->data['batches'] as $weight => $batch){
					$data = array(
						'ChecklistBatch' => array(
							'id' => $batch,
							'weight' => $weight
						)
					);
					$this->ChecklistBatch->create();
					$this->ChecklistBatch->save($data, array('callbacks' => false));
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateField(){
		// x-editable
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->ChecklistBatch->id = $this->request->data['pk'];
			if($this->ChecklistBatch->saveField($this->request->data['name'], $this->request->data['value'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function setTasksDueDate(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$tasks = $this->ChecklistBatch->ChecklistTask->find('all', array(
				'conditions' => array(
					'checklist_batch_id' => $this->request->data['id'],
					'due_date' => null
				)
			));
			foreach($tasks as $k => $task){
				$tasks[$k]['ChecklistTask']['due_date'] = $this->request->data['date'];
			}
			if($this->ChecklistBatch->ChecklistTask->saveMany($tasks)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
