<?php

class DashboardController extends AppController {

	public function index(){

		$this->loadModel('User');
		$numberOfUsers = $this->User->find('count');
		$this->set(compact('numberOfUsers'));

		$this->loadModel('Place');
		$numberOfPlaces = $this->Place->find('count');
		$this->set(compact('numberOfPlaces'));

		$this->loadModel('Activity');
		$numberOfActivities = $this->Activity->find('count', array(
			'conditions' => array(
				'category' => null
			)
		));
		$this->set(compact('numberOfActivities'));

		$this->loadModel('StockItem');
		$numberOfItems = $this->StockItem->find('count');
		$this->set(compact('numberOfItems'));

		$crmStatuses = Configure::read('Events.crm_status');
		$this->set(compact('crmStatuses'));

		if(AuthComponent::user('id') && AuthComponent::user('role') == 'fixed'){
			$this->loadModel('Event');
			$offers = $this->Event->find('all', array(
				'contain' => array(
					'Client',
					'Company',
					'Date',
					'Note'
				),
				'conditions' => array(
					'resp_id' => array(AuthComponent::user('id')),
					'crm_status' => array_keys(Configure::read('Events.crm_status_default'))
				),
				'group' => 'Event.id',
				'order' => 'Event.opening_date',
			));
			$this->set(compact('offers'));

			$confirmed = $this->Event->find('all', array(
				'joins' => array(
					array(
						'table' => 'moments',
						'alias' => 'm',
						'type' => 'left',
						'conditions' => array('m.event_id = Event.id')
					)
				),
				'contain' => array(
					'Client',
					'Job' => array('User'),
					'Company',
					'Moment' => array('Place'),
					'Note',
					'SelectedActivity' => array('Activity'),
					'SuggestedActivity' => array('Activity'),
					'SelectedFBModule' => array('FBModule'),
					'SuggestedFBModule' => array('FBModule')
				),
				'conditions' => array(
					'resp_id' => array(AuthComponent::user('id')),
					'crm_status' => 'confirmed',
					'confirmed_date >=' => date('Y-m-d')
				),
				'group' => 'Event.id',
				'order' => 'Event.confirmed_date'
			));
			foreach($confirmed as $k => $event){
				$this->Event->id = $event['Event']['id'];
				$confirmed[$k]['Event']['number_of_jobs'] = $this->Event->getNumberOfJobs();
				$confirmed[$k]['Event']['number_of_filled_jobs'] = $this->Event->getNumberOfJobs('filled');
			}
			$this->set(compact('confirmed'));

			$conditions = array(
				'Event.crm_status' => 'confirmed',
				'confirmed_date <' => date('Y-m-d'),
				'confirmed_date >=' => date('2016-01-01'),
				'Event.code <>' => null,
				'resp_id' => array(AuthComponent::user('id'))
			);

	    $past = $this->Event->find('all', array(
				'contain' => array(
					'Client',
					'PersonInCharge',
					'Company',
					'Note'
				),
	      'conditions' => $conditions,
				'group' => 'Event.id',
				'order' => 'Event.confirmed_date DESC',
			));
			foreach($past as $k => $event){
				$this->Event->id = $event['Event']['id'];
				$past[$k]['Event']['number_of_jobs'] = $this->Event->getNumberOfJobs();
				$past[$k]['Event']['number_of_filled_jobs'] = $this->Event->getNumberOfJobs('filled');
				$past[$k]['Event']['number_of_validated_jobs'] = $this->Event->getNumberOfJobs('validated');
				$past[$k]['Event']['accounting_match'] = $this->Event->accountingMatch();
				$past[$k]['Event']['checklists_complete'] = $this->Event->areChecklistsComplete();
			}
	    $this->set(compact('past'));

			$this->loadModel('Checklist');
			$tasks1 = $this->Checklist->ChecklistBatch->ChecklistTask->find('all', array(
				'contain' => array(
					'ChecklistBatch' => array(
						'Checklist' => array(
							'Event' => array('Client')
						)
					)
				),
				'joins' => array(
					array(
						'table' => 'checklist_batches',
						'alias' => 'cb',
						'conditions' => array(
							'cb.id = ChecklistTask.checklist_batch_id'
						)
					),
					array(
						'table' => 'checklists',
						'alias' => 'c',
						'conditions' => array(
							'c.id = cb.checklist_id'
						)
					)
				),
				'conditions' => array(
					'due_date <>' => null,
					'ChecklistTask.done' => 0,
					'c.user_id' => AuthComponent::user('id'),
					'due_date <=' => date('Y-m-d')
				),
				'order' => 'ChecklistTask.due_date, cb.weight, ChecklistTask.weight'
			));
			$tasks = array();
			$events = array();
			foreach($tasks1 as $k => $task){
				if(!empty($task['ChecklistBatch']['Checklist']['Event'])){
					$key = !empty($task['ChecklistBatch']['Checklist']['Event']['code_name']) ? $task['ChecklistBatch']['Checklist']['Event']['code_name'] : $task['ChecklistBatch']['Checklist']['Event']['name'];
					if(!empty($task['ChecklistBatch']['Checklist']['Event']['Client'])){
						$key .= ' - ' . $task['ChecklistBatch']['Checklist']['Event']['Client']['name'];
					}
					$tasks[$key][] = $task;
					$events[$key] = $task['ChecklistBatch']['Checklist']['Event'];
				}
			}
			$this->set(compact('tasks'));
			$this->set(compact('events'));
			$this->set('numberOfTasks', sizeof($tasks1));
		}

		$data = $this->User->JobUser->find('all', array(
			'joins' => array(
				array(
					'table' => 'jobs_users',
					'alias' => 'ju',
					'type' => 'INNER',
					'conditions' => array('JobUser.id <> ju.id')
				),
				array(
					'table' => 'jobs',
					'alias' => 'j1',
					'type' => 'INNER',
					'conditions' => array('JobUser.job_id = j1.id')
				),
				array(
					'table' => 'jobs',
					'alias' => 'j2',
					'type' => 'INNER',
					'conditions' => array('ju.job_id = j2.id')
				),
				array(
					'table' => 'events',
					'alias' => 'e1',
					'type' => 'INNER',
					'conditions' => array('e1.id = j1.event_id')
				),
				array(
					'table' => 'events',
					'alias' => 'e2',
					'type' => 'INNER',
					'conditions' => array('e2.id = j2.event_id')
				)
			),
			'fields' => array(
				'TIME_TO_SEC(TIMEDIFF(j2.start_time,j1.end_time)) AS tdiff'
			),
			'conditions' => array(
				'JobUser.user_id = ju.user_id',
				'e1.confirmed_date = e2.confirmed_date',
				'e1.id <> e2.id'
			),
			'group' => array(
				'JobUser.id HAVING tdiff < 4*60*60'
			)
		));
		//debug($data);
	}

}
