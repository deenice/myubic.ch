<?php

class StockOrdersController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'All' => array(
				'columns' => array(
					'order_number' => array(
						'useField' => true,
						'bSortable' => true,
						'bSearchable' => 'customSearchOrder',
					),
					'status' => array(
						'bSearchable' => false,
						'bSortable' => false,
					),
					'delivery_date' => array(
						'useField' => true,
						'bSearchable' => false,
						'bSortable' => true,
					),
					'return_date' => array(
						'useField' => true,
						'bSearchable' => false,
						'bSortable' => true,
					),
					'service_date_begin' => array(
						'useField' => true,
						'bSearchable' => false,
						'bSortable' => true,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status <>' => null,
				)
			),
			'Offer' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'offer',
				)
			),
			'ToCheck' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.to_check' => 1
				)
			),
			'Confirmed' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'confirmed',
				),
			),
			'ToProceed' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'to_proceed',
				),
			),
			'InProgress' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'in_progress',
				),
			),
			'Processed' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'processed',
				),
			),
			'Delivered' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'service_date_begin' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'delivered',
				),
			),
			'ToInvoice' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'company' => array(
						'useField' => false,
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'to_invoice',
				),
			),
			'Invoiced' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'company' => array(
						'useField' => false,
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'invoiced',
				),
			),
			'Cancelled' => array(
				'columns' => array(
					'order_number' => array(
						'bSearchable' => 'customSearchOrder',
					),
					'company' => array(
						'useField' => false,
					),
					'delivery_date' => array(
						'bSearchable' => false,
					),
					'return_date' => array(
						'bSearchable' => false,
					),
					'Actions' => null,
				),
				'conditions' => array(
					'StockOrder.status' => 'cancelled',
				),
			),
		),
	);

	public function beforeFilter() {
		parent::beforeFilter();

		$configs = array(
			'All', 'Offer', 'ToCheck', 'Confirmed', 'ToProceed', 'InProgress', 'Processed', 'Delivered', 'ToInvoice', 'Invoiced', 'Cancelled',
		);

		$contain = array(
			'Client',
			'Company',
			'History',
			'OfferSendHistory',
			'OfferReviveHistory',
			'ContactPeopleClient',
		);
		$fields = array(
			'StockOrder.name', 'StockOrder.to_check', 'StockOrder.status', 'StockOrder.modified', 'StockOrder.net_total', 'StockOrder.export_status', 'StockOrder.remarks', 'Client.id', 'Client.name', 'ContactPeopleClient.first_name', 'ContactPeopleClient.last_name', 'Company.name',
		);
		foreach ($configs as $config) {
			$this->DataTable->settings[$config]['contain'] = $contain;
			$this->DataTable->settings[$config]['fields'] = $fields;
			if ($config == 'All') {
				$this->DataTable->settings[$config]['order'] = array('StockOrder.id DESC');
			}
			$this->DataTable->settings[$config]['autoData'] = false;
			$this->DataTable->settings[$config]['model'] = 'StockOrder';
			$this->DataTable->settings[$config]['conditions'][] = array('StockOrder.client_id <>' => null);
		}

		$this->DataTable->settings['All']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['All']['columns']['status']['label'] = __('Status');
		$this->DataTable->settings['All']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['All']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['All']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['Offer']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Offer']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Offer']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Offer']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['ToCheck']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['ToCheck']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['ToCheck']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['ToCheck']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['Confirmed']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Confirmed']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Confirmed']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Confirmed']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['InProgress']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['InProgress']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['InProgress']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['InProgress']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['Processed']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Processed']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Processed']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Processed']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['Delivered']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Delivered']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Delivered']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Delivered']['columns']['service_date_begin']['label'] = __('Date of service');
		$this->DataTable->settings['ToInvoice']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['ToInvoice']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['ToInvoice']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['ToInvoice']['columns']['company']['label'] = __('Company');
		$this->DataTable->settings['Invoiced']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Invoiced']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Invoiced']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Invoiced']['columns']['company']['label'] = __('Company');
		$this->DataTable->settings['Cancelled']['columns']['order_number']['label'] = __('Order');
		$this->DataTable->settings['Cancelled']['columns']['delivery_date']['label'] = __('Delivery date');
		$this->DataTable->settings['Cancelled']['columns']['return_date']['label'] = __('Return date');
		$this->DataTable->settings['Cancelled']['columns']['company']['label'] = __('Company');

	}

	public function index()
	{
		$this->DataTable->setViewVar(array('All', 'Offer', 'ToCheck', 'Confirmed', 'ToProceed', 'InProgress', 'Processed', 'Delivered', 'ToInvoice', 'Invoiced', 'Cancelled'));

		$ordersToExport = $this->StockOrder->find('all', array(
			'conditions' => array(
				'export_status' => 'to_export',
			),
		));
		$numberOfOrdersToExport = $this->StockOrder->find('count', array(
			'conditions' => array(
				'export_status' => 'to_export',
			),
		));
		$this->set(compact('ordersToExport'));
		$this->set(compact('numberOfOrdersToExport'));

		// Generate json feed
		if (!empty($this->request->query['order_id']) and empty($this->request->query['product_id'])) {
			$stockorder = $this->StockOrder->find('first', array(
				'conditions' => array(
					'StockOrder.id' => $this->request->query['order_id'],
				),
				'contain' => array(
					'StockOrderBatch' => array(
						'StockItem' => 'Document.category = "stockitem"',
					),
				),
				// 'cache' => $this->request->query['order_id'],
				// 'cacheConfig' => 'short'
			));
			$statuses = Configure::read('StockOrders.status');
			if ($stockorder['StockOrder']['status']) {
				$stockorder['StockOrder']['status_text'] = $statuses[$stockorder['StockOrder']['status']];
			} else {
				$stockorder['StockOrder']['status_text'] = 'undefined';
			}
			// if(!empty($stockorder['StockOrderBatch'])){
			// 	$this->loadModel('Warehouse');
			// 	$route = $this->Warehouse->getShortestRoute($stockorder['StockOrderBatch'], 1);
			// 	$data = array();
			// 	$ids = array();
			// 	foreach($stockorder['StockOrderBatch'] as $batch){
			// 		$ids[] = $batch['StockItem']['id'];
			// 	}
			// 	$alley = 0;
			// 	foreach($route as $spid){
			// 		$sp = $this->Warehouse->WarehouseStoragePoint->find('first', array(
			// 			'conditions' => array(
			// 				'WarehouseStoragePoint.id' => $spid
			// 			)
			// 		));
			// 		$passage = $this->Warehouse->WarehousePassage->find('first', array(
			// 			'conditions' => array(
			// 				'WarehousePassage.id' => $sp['WarehouseStoragePoint']['adjacency_passage_id']
			// 			),
			// 			'cache' => $sp['WarehouseStoragePoint']['adjacency_passage_id'],
			// 			'cacheConfig' => 'short'
			// 		));
			// 		$stockitems = $this->StockOrder->StockOrderBatch->StockItem->find('all', array(
			// 			'conditions' => array(
			// 				'StockItem.warehouse_storage_point_id' => $spid,
			// 				'StockItem.id' => $ids
			// 			)
			// 		));
			// 		$products = array();
			// 		foreach($stockitems as $k => $item){
			// 			$product = array();
			// 			$batch = $this->StockOrder->StockOrderBatch->find('first', array(
			// 				'conditions' => array(
			// 					'StockOrderBatch.stock_item_id' => $item['StockItem']['id'],
			// 					'StockOrderBatch.stock_order_id' => $this->request->query['order_id']
			// 				)
			// 			));
			// 			$product['StockItem']['name'] = $item['StockItem']['name'];
			// 			$product['StockItem']['code'] = $item['StockItem']['code'];
			// 			$product['StockItem']['position'] = $item['StockItem']['warehouse_storage_point_id'];
			// 			$product['quantity'] = $batch['StockOrderBatch']['quantity'];
			// 			$product['processed'] = $batch['StockOrderBatch']['processed'];
			// 			$product['stock_order_id'] = $batch['StockOrderBatch']['stock_order_id'];
			// 			$product['stock_item_id'] = $batch['StockOrderBatch']['stock_item_id'];
			// 			$data[$passage['WarehousePassage']['name']][] = $product;
			// 		}
			// 		//$data[$passage['WarehousePassage']['name']] = $products;
			// 	}
			// 	$stockorder['route'] = $data;
			// }
			$product['StockItem']['name'] = 'Carrare - Assiette à soupe';
			$product['StockItem']['code'] = 1010802;
			$product['StockItem']['position'] = 44;
			$product['quantity'] = 100;
			$product['processed'] = 0;
			$product['stock_order_id'] = 35;
			$product['stock_item_id'] = 26;
			$data['Allée 3'][] = $product;
			$product['StockItem']['name'] = 'Canapé Arizona';
			$product['StockItem']['code'] = 4010006;
			$product['StockItem']['position'] = 130;
			$product['quantity'] = 2;
			$product['processed'] = 0;
			$product['stock_order_id'] = 35;
			$product['stock_item_id'] = 737;
			$data['Allée 3'][] = $product;
			$product['StockItem']['name'] = 'Double corbeille en inox';
			$product['StockItem']['code'] = 9040015;
			$product['StockItem']['position'] = 78;
			$product['quantity'] = 4;
			$product['processed'] = 1;
			$product['stock_order_id'] = 35;
			$product['stock_item_id'] = 923;
			$data['Allée 3'][] = $product;
			$product['StockItem']['name'] = 'Friteuse de table électrique 2x12 lt.';
			$product['StockItem']['code'] = 7020301;
			$product['StockItem']['position'] = 212;
			$product['quantity'] = 1;
			$product['processed'] = 0;
			$product['stock_order_id'] = 35;
			$product['stock_item_id'] = 1067;
			$data['Allée 4'][] = $product;
			$stockorder['route'] = $data;
			$this->set(compact('stockorder'));
			$this->set('_serialize', array('stockorder'));
		} elseif (!empty($this->request->query['order_id']) and !empty($this->request->query['product_id'])) {
			$this->loadModel('StockOrderBatch');
			$stockorderbatch = $this->StockOrderBatch->find('first', array(
				'conditions' => array(
					'StockOrderBatch.stock_item_id' => $this->request->query['product_id'],
					'StockOrderBatch.stock_order_id' => $this->request->query['order_id'],
				),
				'contain' => array(
					'StockItem' => array(
						'StockCategory',
						'StockSection',
						'StockFamily',
						'Document',
						'StockZone' => array('Beacon'),
					),
				),
			));
			$this->set(compact('stockorderbatch'));
			$this->set('_serialize', array('stockorderbatch'));
		} elseif (!empty($this->request->query['code'])) {
			$this->loadModel('StockItem');
			$stockitem = $this->StockItem->find('first', array(
				'conditions' => array(
					'StockItem.code' => $this->request->query['code'],
				),
				'contain' => array(
					'StockCategory',
					'StockSection',
					'StockFamily',
					'Document',
				),
			));
			$this->set(compact('stockitem'));
			$this->set('_serialize', array('stockitem'));
		} else {
			$this->set('_serialize', array('stockorders'));
		}
	}

	public function index_json()
	{
		$view = new View($this);
		$html = $view->loadHelper('Html');
		$time = $view->loadHelper('Time');
		$statuses = Configure::read('StockOrders.status');
		$status = $this->request->query['status'];

		if ($status != 'all') {
			$stockorders = $this->StockOrder->find('all', array(
				'contain' => array(
					'Client',
					'Company',
					'History',
					'ContactPeopleClient',
				),
				'conditions' => array(
					'StockOrder.name <>' => '',
					'StockOrder.status' => $status,
					'StockOrder.status <>' => null,
				),
				'order' => 'StockOrder.id DESC',
				// 'cache' => date('dmYH'),
				// 'cacheConfig' => 'short'
			));
		} else {
			$stockorders = $this->StockOrder->find('all', array(
				'contain' => array(
					'Client',
					'Company',
					'History',
					'ContactPeopleClient',
				),
				'conditions' => array(
					'StockOrder.client_id <>' => '',
					'StockOrder.status <>' => null,
				),
				'order' => 'StockOrder.id DESC',
				'cache' => date('dmYH'),
				'cacheConfig' => 'short',
			));
		}

		$data = array();
		if (empty($data)) {
			foreach ($stockorders as $k => $order) {
				$links1 = '';
				$links2 = '';
				$data[$status][$k]['StockOrder']['order_number'] = $html->tag('strong', $order['StockOrder']['order_number']);
				$data[$status][$k]['StockOrder']['order_number'] .= $html->tag('br');
				$data[$status][$k]['StockOrder']['order_number'] .= $html->link(
					$order['StockOrder']['name'],
					array('controller' => 'stock_orders', 'action' => 'edit', $order['StockOrder']['id'])
				);
				$data[$status][$k]['StockOrder']['order_number'] .= $html->tag('hr');
				$data[$status][$k]['StockOrder']['order_number'] .= $html->link(
					$order['Client']['name'],
					array('controller' => 'clients', 'action' => 'edit', $order['Client']['id']),
					array('target' => '_blank')
				);
				$data[$status][$k]['StockOrder']['order_number'] .= $html->tag('br');
				$data[$status][$k]['StockOrder']['order_number'] .= $order['ContactPeopleClient']['name'];
				$data[$status][$k]['StockOrder']['status'] = $statuses[$order['StockOrder']['status']];
				$data[$status][$k]['StockOrder']['delivery_date'] = $time->format($order['StockOrder']['delivery_date'], '%A %d %B %Y');
				$data[$status][$k]['StockOrder']['return_date'] = $time->format($order['StockOrder']['return_date'], '%A %d %B %Y');
				$data[$status][$k]['StockOrder']['service_date_begin'] = $time->format($order['StockOrder']['service_date_begin'], '%A %d %B %Y');
				$data[$status][$k]['StockOrder']['Company']['name'] = $order['Company']['name'];
				if (in_array($order['StockOrder']['status'], array('to_invoice', 'invoiced'))) {
					$links1 = $html->link(
						'<i class="fa fa-edit"></i> '.__('Edit'),
						array('controller' => 'stock_orders', 'action' => 'edit_invoice', $order['StockOrder']['id']),
						array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
					);
				} else {
					$links1 = $html->link(
						'<i class="fa fa-edit"></i> '.__('Edit'),
						array('controller' => 'stock_orders', 'action' => 'edit', $order['StockOrder']['id']),
						array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
					);
				}
				if ($order['StockOrder']['status'] == 'offer') {
					$links1 .= $html->link(
						'<i class="fa fa-check"></i> '.__('Confirm'),
						array('controller' => 'stock_orders', 'action' => 'confirm', $order['StockOrder']['id']),
						array('class' => 'btn btn-success btn-xs margin-bottom-5', 'escape' => false)
					);
				}
				$links1 .= $html->link(
					'<i class="fa fa-times"></i> '.__('Cancel'),
					array('controller' => 'stock_orders', 'action' => 'cancel', $order['StockOrder']['id']),
					array('class' => 'btn btn-danger btn-xs margin-bottom-5', 'escape' => false)
				);
				$links1 = $html->tag('div', $links1, array('class' => 'btn-group'));

				if (!in_array($order['StockOrder']['status'], array('to_invoice', 'invoiced'))) {
					$links2 = $html->link(
						'<i class="fa fa-download"></i> '.__('Download'),
						array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'].'.pdf'),
						array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
					);
					$links2 .= $html->link(
						'<i class="fa fa-file-o"></i> '.__('View offer'),
						array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'].'.pdf', '?' => 'download=0'),
						array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
					);
					if (empty($order['History'])) {
						$links2 .= $html->link(
							'<i class="fa fa-send-o"></i> '.__('Send to client'),
							array('controller' => 'stock_orders', 'action' => 'send', $order['StockOrder']['id'], 'client', 'offer_sent'),
							array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
						);
					} elseif (!empty($order['History']) and $order['StockOrder']['modified'] >= $order['History'][0]['date']) {
						$links2 .= $html->link(
							'<i class="fa fa-send-o"></i> '.__('Resend to client'),
							array('controller' => 'stock_orders', 'action' => 'send',
								$order['StockOrder']['id'], 'client', 'offer_sent', ),
							array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
						);
					}
				} else {
					$links2 = $html->link(
						'<i class="fa fa-file-o"></i> '.__('View invoice'),
						array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'].'.pdf', '?' => 'download=0'),
						array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
					);
				}

				$links2 = $html->tag('div', $links2, array('class' => 'btn-group'));

				$data[$status][$k]['actions'][] = $links1;
				$data[$status][$k]['actions'][] = $links2;
			}
		}
		debug($data);
		exit;
		$this->set(compact('data'));
		$this->set('_serialize', array('data'));

		//debug($this->request->query['order_id']);exit;
	}

	public function add(){
		$this->StockOrder->create();
		$order = array('StockOrder' => array('status' => 'offer'));
		if(!empty($this->request->params['named']['client_id'])){
			$this->StockOrder->Client->contain('ContactPeople', 'Company');
			$client = $this->StockOrder->Client->findById($this->request->params['named']['client_id']);
			$order['StockOrder']['client_id'] = $client['Client']['id'];
			if(!empty($client['Client']['address'])){
				$order['StockOrder']['invoice_address'] = $client['Client']['address'];
			}
			if(!empty($client['Client']['zip'])){
				$order['StockOrder']['invoice_zip'] = $client['Client']['zip'];
			}
			if(!empty($client['Client']['city'])){
				$order['StockOrder']['invoice_city'] = $client['Client']['city'];
			}
			if(!empty($client['ContactPeople'])){
				$order['StockOrder']['contact_people_id'] = $client['ContactPeople'][0]['id'];
				$order['StockOrder']['delivery_contact_people_id'] = $client['ContactPeople'][0]['id'];
				$order['StockOrder']['return_contact_people_id'] = $client['ContactPeople'][0]['id'];
				if(!empty($client['ContactPeople'][0]['phone'])) $order['StockOrder']['invoice_phone'] = $client['ContactPeople'][0]['phone'];
				if(!empty($client['ContactPeople'][0]['email'])) $order['StockOrder']['invoice_email'] = $client['ContactPeople'][0]['email'];
			}
		}
		$this->StockOrder->save($order);

		return $this->redirect(array('action' => 'edit', $this->StockOrder->id));
	}

	public function edit($id = null) {
		$this->StockOrder->id = $id;

		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}

		if ($this->request->is('post') || $this->request->is('put') or $this->request->is('ajax')) {

			$callbacks = true;
			if ($this->request->is('ajax')) {
				$this->autoRender = false;
				$this->request->data = $_POST['data'];
				$callbacks = false;
				$this->StockOrder->id = $this->request->data['StockOrder']['id'];
			}

			$oldStatus = $this->StockOrder->field('status');
			if(in_array($oldStatus, array('confirmed', 'delivered', 'in_progress', 'processed'))){
			  $this->request->data['StockOrder']['notify_cancellation'] = 1;
			}
			if (!empty($this->request->data['StockOrder']['delivery_date'])) {
				$this->request->data['StockOrder']['delivery_date'] = date('Y-m-d', strtotime($this->request->data['StockOrder']['delivery_date']));
			} else {
				$this->request->data['StockOrder']['delivery_date'] = '1970-01-01';
			}
			if (!empty($this->request->data['StockOrder']['return_date'])) {
				$this->request->data['StockOrder']['return_date'] = date('Y-m-d', strtotime($this->request->data['StockOrder']['return_date']));
			} else {
				$this->request->data['StockOrder']['return_date'] = '1970-01-01';
			}
			if (!empty($this->request->data['StockOrder']['service_date_begin'])) {
				$this->request->data['StockOrder']['service_date_begin'] = date('Y-m-d', strtotime($this->request->data['StockOrder']['service_date_begin']));
			}
			if (!empty($this->request->data['StockOrder']['service_date_end'])) {
				$this->request->data['StockOrder']['service_date_end'] = date('Y-m-d', strtotime($this->request->data['StockOrder']['service_date_end']));
			}
			if (!empty($this->request->data['StockOrder']['date_of_invoice'])) {
				$this->request->data['StockOrder']['date_of_invoice'] = date('Y-m-d', strtotime($this->request->data['StockOrder']['date_of_invoice']));
			}
			if (!empty($this->request->data['StockOrder']['delivery_place'])) {
				$this->request->data['StockOrder']['delivery_place'] = rtrim($this->request->data['StockOrder']['delivery_place'], ',');
			}
			if (!empty($this->request->data['StockOrder']['return_place'])) {
				$this->request->data['StockOrder']['return_place'] = rtrim($this->request->data['StockOrder']['return_place'], ',');
			}
			if (!empty($this->request->data['StockOrder']['delivery_mode']) && $this->request->data['StockOrder']['delivery_mode'] == 'festiloc') {
				$this->request->data['StockOrder']['delivery_transporter_id'] = 1;
			}
			if (!empty($this->request->data['StockOrder']['return_mode']) && $this->request->data['StockOrder']['return_mode'] == 'festiloc') {
				$this->request->data['StockOrder']['return_transporter_id'] = 1;
			}

			if ($this->request->data['StockOrder']['type'] == 'sale') {
				$this->request->data['StockOrder']['return_date'] = $this->request->data['StockOrder']['delivery_date'];
			} elseif($this->request->data['StockOrder']['type'] == 'delivery') {
				$this->request->data['StockOrder']['return_date'] = $this->request->data['StockOrder']['delivery_date'];
			} elseif($this->request->data['StockOrder']['type'] == 'withdrawal') {
				$this->request->data['StockOrder']['delivery_date'] = $this->request->data['StockOrder']['return_date'];
        if(!in_array($this->request->data['StockOrder']['status'], array('invoiced', 'to_invoice', 'cancelled'))){
          $this->request->data['StockOrder']['status'] = 'delivered';
        }
			}

			if (!empty($this->request->data['StockOrderBatch'])) {
				$numberOfProducts = 0;
				$data = array();
				//$batches = array(); // this array is used to check if there are identical rows (same product)
				// determine reprocessing duration according to return date
				$duration = 4; // default
				if(!empty($this->request->data['StockOrder']['return_date'])){
					$day = date('N', strtotime($this->request->data['StockOrder']['return_date']));
					switch($day){
						case 1:
						$duration = 2;
						break;
						case 2:
						$duration = 1;
						break;
						case 3:
						$duration = 4;
						break;
						case 4:
						$duration = 3;
						break;
						case 5:
						$duration = 2;
						break;
						case 6:
						$duration = 2;
						break;
						case 7:
						$duration = 2;
						break;
					}
				}
				for ($i = 0; $i < count($this->request->data['StockOrderBatch']['stock_item_id']) - 1; ++$i) {
					$this->StockOrder->StockOrderBatch->create();
					if($this->request->data['StockOrderBatch']['stock_item_id'][$i] != -1){
						$item = $this->StockOrder->StockOrderBatch->StockItem->find('first', array(
							'conditions' => array(
								'id' => $this->request->data['StockOrderBatch']['stock_item_id'][$i]
							),
							'fields' => array('default_reconditionning_duration')
						));
					} else {
						$item = array();
					}
					if(!empty($item['StockItem']['default_reconditionning_duration']) && $duration > $item['StockItem']['default_reconditionning_duration']){
						$duration1 = $item['StockItem']['default_reconditionning_duration'];
					} else {
						$duration1 = '';
					}
					$obj = array(
						'id' => !empty($this->request->data['StockOrderBatch']['id'][$i]) ? $this->request->data['StockOrderBatch']['id'][$i] : '',
						'stock_item_id' => $this->request->data['StockOrderBatch']['stock_item_id'][$i],
						'stock_order_id' => $id,
						'quantity' => $this->request->data['StockOrderBatch']['quantity'][$i],
						'coefficient' => $this->request->data['StockOrderBatch']['coefficient'][$i],
						'discount' => $this->request->data['StockOrderBatch']['discount'][$i],
						'weight' => $this->request->data['StockOrderBatch']['weight'][$i],
						'price' => $this->request->data['StockOrderBatch']['price'][$i],
						'name' => !empty($this->request->data['StockOrderBatch']['name'][$i]) ? $this->request->data['StockOrderBatch']['name'][$i] : '',
						'reprocessing_duration' => !empty($duration1) ? $duration1 : $duration
					);
					//$batches[$obj['stock_item_id']][$i] = $obj['quantity'];
					if (!empty($obj['quantity'])) {
						$this->request->data['StockOrderBatch'][] = $obj;
					} elseif ($obj['stock_item_id'] == -1) {
						$this->request->data['StockOrderBatch'][] = $obj;
					}
					$numberOfProducts += $this->request->data['StockOrderBatch']['quantity'][$i];
				}
				unset($this->request->data['StockOrderBatch']['quantity']);
				unset($this->request->data['StockOrderBatch']['stock_item_id']);
				unset($this->request->data['StockOrderBatch']['id']);
				unset($this->request->data['StockOrderBatch']['price']);
				unset($this->request->data['StockOrderBatch']['coefficient']);
				unset($this->request->data['StockOrderBatch']['discount']);
				unset($this->request->data['StockOrderBatch']['weight']);
				unset($this->request->data['StockOrderBatch']['name']);
				unset($this->request->data['StockOrderBatch']['subject_to_discount']);
				$this->request->data['StockOrder']['number_of_products'] = $numberOfProducts;

			}

			if(!empty($this->request->data['StockOrder']['batches_to_delete'])){
				$batches = explode(',', $this->request->data['StockOrder']['batches_to_delete']);
				foreach($batches as $bid){
					$this->StockOrder->StockOrderBatch->delete($bid, false);
				}
			}

      $this->StockOrder->saveTotals();

			if ($this->request->is('ajax')) {
				if ($this->StockOrder->saveAssociated($this->request->data, array('create_version' => false))) {
					$this->StockOrder->contain('StockOrderBatch');
					$order = $this->StockOrder->findById($id);
					$this->response->body(json_encode($order['StockOrderBatch']));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			} else {
				if ($this->StockOrder->saveAssociated($this->request->data)) {
					$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
					if ($this->request->data['destination'] == 'edit') {
						return $this->redirect(array('action' => 'edit', $this->StockOrder->id));
					} elseif ($this->request->data['destination'] == 'send' && $this->request->data['StockOrder']['status'] != 'cancelled') {
						$modifiedDate = strtotime($this->StockOrder->field('modified'));
						$this->StockOrder->saveField('modified', date('Y-m-d H:i:s', $modifiedDate - 10), array('callbacks' => false));
						return $this->redirect(array('action' => 'send', $this->StockOrder->id, 'client', 'offer_sent'));
					} elseif ($this->request->data['destination'] == 'validate') {
						$this->StockOrder->saveField('status', 'invoiced', array('callbacks' => false));
						return $this->redirect(array('action' => 'edit_invoice', $this->StockOrder->id));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					$this->Session->setFlash(__('StockOrder has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				}
			}
		} else {

			$this->loadModel('User');
			$controllers = $this->User->find('list', array(
				'fields' => array('User.id', 'User.full_name'),
			));
			$this->set(compact('controllers'));

			$clients = $this->StockOrder->Client->find('list', array(
				'joins' => array(
					array(
						'table' => 'clients_companies',
						'alias' => 'cc',
						'conditions' => array(
							'cc.client_id = Client.id',
							'cc.company_id' => 3
						)
					)
				)
			));
			$this->set(compact('clients'));

			$companies = $this->StockOrder->Client->Company->find('list', array(
				'fields' => array('Company.id', 'Company.name'),
			));
			$this->set(compact('companies'));

			$this->loadModel('Place');
			$places = $this->Place->find('list', array(
				'fields' => array('Place.id', 'Place.name'),
				'order' => array('Place.name'),
			));
			$this->set(compact('places'));

			$stockorder = $this->StockOrder->find('first', array(
				'conditions' => array(
					'StockOrder.id' => $id,
				),
				'contain' => array(
					'StockOrderBatch' => array(
						'StockItem',
					),
					'Client',
					'Company',
				),
			));
			if(!empty($stockorder['StockOrder']['date_of_invoice'])){
				$invoiceDate = $stockorder['StockOrder']['date_of_invoice'];
			} elseif(in_array($stockorder['StockOrder']['status'], array('invoiced', 'to_invoice')) AND empty($stockorder['StockOrder']['date_of_invoice'])){
				$invoiceDate = $stockorder['StockOrder']['return_date'];
			} else {
				$invoiceDate = '';
			}
			$invoiceDate = !empty($invoiceDate) ? date('d-m-Y', strtotime($invoiceDate)) : '';
			$this->set(compact('invoiceDate'));

			$fidelityDiscountPercentage = 0;

			if (!empty($stockorder['StockOrder']['fidelity_discount_percentage'])) {
				$fidelityDiscountPercentage = $stockorder['StockOrder']['fidelity_discount_percentage'];
			} else {
				if ($stockorder['Client']['festiloc_fidelity_discount'] > 0) {
					$fidelityDiscountPercentage = $stockorder['Client']['festiloc_fidelity_discount'];
				}
			}
			//debug($fidelityDiscountPercentage);

			$this->set(compact('fidelityDiscountPercentage'));

			$contactPeoples = $this->StockOrder->ContactPeopleClient->find('list', array(
				'joins' => array(
					array(
						'table' => 'clients',
						'alias' => 'Client',
						'conditions' => array('Client.id = ContactPeopleClient.client_id'),
					),
				),
				'conditions' => array(
					'ContactPeopleClient.client_id' => $stockorder['StockOrder']['client_id'],
					'Client.id <>' => null,
				),
				'fields' => array(
					'id', 'full_name_phone_email',
				),
			));
			$this->set(compact('contactPeoples'));

			$transporters = $this->StockOrder->Transporter->find('list', array(
				'fields' => array('id', 'name')
			));
			$this->set(compact('transporters'));


			if (!empty($stockorder['StockOrder']['service_date_begin'])) {
				$stockorder['StockOrder']['service_date_begin'] = date('d-m-Y', strtotime($stockorder['StockOrder']['service_date_begin']));
			}
			if (!empty($stockorder['StockOrder']['service_date_end'])) {
				$stockorder['StockOrder']['service_date_end'] = date('d-m-Y', strtotime($stockorder['StockOrder']['service_date_end']));
			}
			if (!empty($stockorder['StockOrder']['date_of_invoice'])) {
				$stockorder['StockOrder']['date_of_invoice'] = date('d-m-Y', strtotime($stockorder['StockOrder']['date_of_invoice']));
			}
			if (!empty($stockorder['StockOrder']['delivery_date'])) {
				$stockorder['StockOrder']['delivery_date'] = date('d-m-Y', strtotime($stockorder['StockOrder']['delivery_date']));
			}
			if (!empty($stockorder['StockOrder']['return_date'])) {
				$stockorder['StockOrder']['return_date'] = date('d-m-Y', strtotime($stockorder['StockOrder']['return_date']));
			}
			$this->set(compact('stockorder'));
			$this->request->data = $stockorder;
		}
	}

	public function duplicate($id = null)
	{
		$this->StockOrder->contain(array(
			'StockOrderBatch',
			'Client',
			'ContactPeopleClient',
			'ContactPeopleDelivery',
			'ContactPeopleReturn',
		));
		$row = $this->StockOrder->findById($id);
		$row['StockOrder']['id'] = '';
		$row['StockOrder']['uuid'] = '';
		$row['StockOrder']['status'] = 'offer';
		$row['StockOrder']['version_id_read'] = '';
		$row['StockOrder']['forced_net_total'] = 0;
		$row['StockOrder']['export_status'] = 'not_exported';
		$row['StockOrder']['number_of_pallets1'] = '';
		$row['StockOrder']['number_of_pallets_xl1'] = '';
		$row['StockOrder']['number_of_rollis1'] = '';
		$row['StockOrder']['number_of_rollis_xl1'] = '';
		$row['StockOrder']['remarks'] = '';
		$row['StockOrder']['delivery_remarks'] = '';
		$row['StockOrder']['return_remarks'] = '';
		$row['StockOrder']['date_of_invoice'] = '';
		if (!empty($row['StockOrderBatch'])) {
			foreach ($row['StockOrderBatch'] as $k => $batch) {
				$row['StockOrderBatch'][$k]['id'] = '';
				$row['StockOrderBatch'][$k]['stock_order_id'] = '';
				$row['StockOrderBatch'][$k]['returned_quantity'] = '';
				$row['StockOrderBatch'][$k]['delivered_quantity'] = '';
				$row['StockOrderBatch'][$k]['reprocessing_duration'] = '';
				$row['StockOrderBatch'][$k]['invoice_replacement'] = 0;
			}
		}
		$this->StockOrder->create();
		if ($this->StockOrder->saveAssociated($row)) {
			$this->Session->setFlash(__('Order has been duplicated. Status has been set to "offer".'), 'alert', array('type' => 'success'));

			return $this->redirect(array('action' => 'edit', $this->StockOrder->id));
		} else {
			$this->Session->setFlash(__('StockOrder has not been duplicated. Please try again.'), 'alert', array('type' => 'danger'));

			return $this->redirect(array('action' => 'index'));
		}
	}

	public function confirm($id = null) {
		$this->StockOrder->id = $id;
		$order = $this->StockOrder->findById($id);
		$order['StockOrder']['status'] = 'confirmed';
		if ($this->StockOrder->save($order, array('callbacks' => false, 'create_version' => true))) {
			$this->Session->setFlash(__('Order has been confirmed.'), 'alert', array('type' => 'success'));
			if (!empty($this->request->data['destination'])) {
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->StockOrder->id));
				} elseif ($this->request->data['destination'] == 'send') {
					$this->Session->setFlash(__('Order has been confirmed and sent.'), 'alert', array('type' => 'success'));

					return $this->redirect(array('action' => 'send', $this->StockOrder->id, 'client', 'offer_sent'));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			$this->Session->setFlash(__('An error occured. Please try again.'), 'alert', array('type' => 'danger'));

			return $this->redirect(array('action' => 'index'));
		}
	}

	public function cancel($id = null)
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['stock_order_id'];
			if ($this->StockOrder->saveField('notify_cancellation', 0, array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		} else {
			$stockorder = $this->StockOrder->read(null, $id);
			$status = $stockorder['StockOrder']['status'];
			$stockorder['StockOrder']['status'] = 'cancelled';
			if (in_array($status, array('in_progress', 'processed', 'delivered', 'confirmed'))) {
				$stockorder['StockOrder']['notify_cancellation'] = 1;
			} else {
				$stockorder['StockOrder']['notify_cancellation'] = 0;
			}
			if ($this->StockOrder->save($stockorder)) {
				//$this->StockOrder->notify('cancellation');
				$this->Session->setFlash(__('Order has been cancelled.'), 'alert', array('type' => 'success'));

				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('An error occured. Please try again.'), 'alert', array('type' => 'danger'));

				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function validate_invoice($id = null) {
	  $this->StockOrder->id = $id;
	  $this->StockOrder->contain('Company');
	  $order = $this->StockOrder->findById($id);
	  if ($this->StockOrder->saveField('status', 'invoiced', array('callbacks' => false))) {
		$this->Session->setFlash(__('Order has been invoiced.'), 'alert', array('type' => 'success'));
		// $this->loadModel('Revelate');
		// if($order['StockOrder']['export_status'] == 'to_export'){
		//   $xml = $this->Revelate->xml('stockorder', $id);
		//   $result = $this->Revelate->post($order['Company']['uuid'], 'salesInvoices', $xml);
		//   if($result->isOk()){
		//	 $this->StockOrder->saveField('export_status', 'exported', array('callbacks' => false));
		//	 $this->Session->setFlash(__('Order has been invoiced and exported to Revelate.'), 'alert', array('type' => 'success'));
		//   }
		// }
		return $this->redirect(array('action' => 'index', '#' => 'invoiced'));
	  } else {
		$this->Session->setFlash(__('An error occured. Please try again.'), 'alert', array('type' => 'danger'));
		return $this->redirect(array('action' => 'index'));
	  }
	}

	public function edit_invoice($id = null) {
		$this->StockOrder->id = $id;
		$this->StockOrder->fixTotalHt();

		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}

		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'StockOrderBatch' => array(
					'StockItem',
				),
				'Client',
				'Company',
				'History',
				'IcingVersion',
				'ContactPeopleClient',
				'ContactPeopleDelivery',
				'ContactPeopleReturn',
				'StockOrderExtraHour',
			),
		));

		if(!empty($stockorder['StockOrderExtraHour'])){
			foreach($stockorder['StockOrderExtraHour'] as $hour){
				if(empty($hour['duration'])){
					$this->StockOrder->StockOrderExtraHour->delete($hour['id']);
				}
			}
		}

		$batchesToReplace = array();
		foreach ($stockorder['StockOrderBatch'] as $k => $batch) {
			if (!is_null($batch['delivered_quantity']) AND $batch['delivered_quantity'] > $batch['returned_quantity']) {
				$batch['replacement_total'] = (is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity'] - $batch['returned_quantity']) * empty($batch['StockItem']['replacement_price']) ? 0 : $batch['StockItem']['replacement_price'];
				$batchesToReplace[$k] = $batch;
			} elseif(is_null($batch['delivered_quantity']) AND $batch['quantity'] > $batch['returned_quantity']){
			  $batch['replacement_total'] = (is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity'] - $batch['returned_quantity']) * empty($batch['StockItem']['replacement_price']) ? 0 : $batch['StockItem']['replacement_price'];
			  $batchesToReplace[$k] = $batch;
			}
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			if(!empty($this->request->data['StockOrderExtraHour'])){
				foreach($this->request->data['StockOrderExtraHour'] as $hour){
					if(empty($hour['duration'])){
						$this->StockOrder->StockOrderExtraHour->delete($hour['id']);
					}
				}
			}

			$this->StockOrder->saveTotals();

			if ($this->StockOrder->saveAssociated($this->request->data, array('callbacks' => false))) {
				$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit_invoice', $this->StockOrder->id));
				} elseif ($this->request->data['destination'] == 'validate') {
					//$this->StockOrder->saveField('status', 'invoiced', array('callbacks' => false));
					return $this->redirect(array('action' => 'validate_invoice', $this->StockOrder->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('StockOrder has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->set(compact('batchesToReplace'));
			$this->set(compact('stockorder'));
			$this->request->data = $stockorder;
		}
	}

	public function invoice_totals(){
		if($this->request->is('ajax')){
			$this->StockOrder->id = $this->request->data['id'];
			if($this->StockOrder->saveTotals($this->request->data['updateInvoicedTotal'])){
        $stockorder = $this->StockOrder->find('first', array(
  				'conditions' => array(
  					'id' => $this->request->data['id']
  				)
  			));
  			$this->set(compact('stockorder'));
      }
		}
	}

	public function invoice($id = null, $status = '') {
		$this->layout = 'default';

		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}
		$this->StockOrder->id = $id;
		$this->StockOrder->saveTotals();
		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'StockOrderBatch' => array(
					'StockItem',
				),
				'Client',
				'Company',
				'ContactPeopleClient',
				'ContactPeopleDelivery',
				'ContactPeopleReturn',
				'StockOrderExtraHour',
			),
		));

		if($stockorder['StockOrder']['status'] == 'confirmed'){
			if(!empty($this->request->query['source']) && $this->request->query['source'] == 'depot'){
				$stockorder['StockOrder']['status'] = 'in_progress';
				$this->StockOrder->save($stockorder, array('callbacks' => false));
			}
		}

		$date = empty($stockorder['StockOrder']['service_date_begin']) ? 'date à définir' : strftime('%d.%m.%Y', strtotime($stockorder['StockOrder']['service_date_begin']));
		$name = $date;

		if (!empty($stockorder['StockOrder']['service_end_begin'])) {
			$toDate = date('d.m.Y', strftime(strtotime($stockorder['StockOrder']['service_end_begin'])));
			$name .= '-'.strftime('%d.%m.%Y', $toDate);
		}
		$name .= ' - '.str_replace('/', '-', $stockorder['Client']['name']);
		$name .= !empty($stockorder['StockOrder']['delivery_city']) ? ' - '.$stockorder['StockOrder']['delivery_city'] : '';
		$filename = $name.'.pdf';

		$download = true;
		$reminder = false;
		if (!empty($this->request->query)) {
			if (!$this->request->query['download']) {
				$download = false;
			}
		}
		if (!empty($this->request->query)) {
			if (!empty($this->request->query['reminder']) and $this->request->query['reminder'] == 1) {
				$reminder = true;
			}
		}
		$options = array(
			'filename' => $filename,
			'download' => $download,
			'options' => array(
				'footer-font-size' => 8,
				'footer-html' => Router::url(array('controller' => 'stock_orders', 'action' => 'pdf_footer', 'full_base' => true))
			),
			'margin' => array(
				'bottom' => 30,
				'left' => 10,
				'right' => 10,
				'top' => 15,
			)
		);
		$this->pdfConfig = $options;

		$this->set(compact('stockorder'));
		$this->set(compact('reminder'));
	}

	public function pdf_footer() {
		$this->layout = 'blank';
		return true;
	}

	public function pallet($id = null) {
		$this->layout = 'default';
		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}

		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'StockOrderBatch' => array(
					'StockItem',
				),
				'Client',
				'Company',
				'ContactPeopleClient',
				'ContactPeopleDelivery',
				'ContactPeopleReturn',
				'DeliveryTransporter',
			),
		));

		$filename = $stockorder['Client']['name'].'_fiche_palette'.'.pdf';

		$download = true;
		if (!empty($this->request->query)) {
			if (!$this->request->query['download']) {
				$download = false;
			}
			if (isset($this->request->query['source']) && $this->request->query['source'] == 'depot') {
				if ($stockorder['StockOrder']['status'] == 'confirmed') {
					$this->StockOrder->id = $id;
					$this->StockOrder->saveField('status', 'processed', array('callbacks' => false));
				}
			}
		}

		$this->pdfConfig = array(
			'filename' => $filename,
			'download' => $download,
		);

		$this->StockOrder->id = $id;
		$packaging = $this->StockOrder->getPackaging();

		$this->set(compact('stockorder'));
		$this->set(compact('packaging'));
	}

	public function transporter_pdf($id = null) {
		$this->layout = 'default';

		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}

		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'Client',
				'Company',
				'ContactPeopleClient',
				'ContactPeopleDelivery',
				'ContactPeopleReturn',
				'DeliveryTour',
				'ReturnTour',
				'StockOrderBatch' => array('StockItem'),
				'DeliveryTransporter',
				'ReturnTransporter'
			),
		));

		$filename = $stockorder['Client']['name'].'_fiche_transporteur'.'.pdf';

		$download = true;
		if (!empty($this->request->query)) {
		  if (!$this->request->query['download']) {
			  $download = false;
		  }
		}

		$this->pdfConfig = array(
			'filename' => $filename,
			'download' => $download,
		);

		$data = array();
		$data['type'] = 'delivery';
		$data['subject'] = __('Delivery');
		if (!empty($stockorder['DeliveryTour'])) {
			$data['type'] = 'delivery';
			$data['subject'] = __('Delivery');
		}
		if (!empty($stockorder['ReturnTour'])) {
			$data['type'] = 'return';
			$data['subject'] = __('Return');
		}
		if(!empty($this->request->query['type'])){
		  $data['type'] = $this->request->query['type'];
		}

		$this->StockOrder->id = $id;
		$packaging = $this->StockOrder->getPackaging();

		$this->set(compact('stockorder'));
		$this->set(compact('packaging'));
		$this->set(compact('data'));
	}

	public function return_pdf($option = null) {
		$this->layout = 'default';

		if(is_numeric($option)){
			$stockorders = $this->StockOrder->find('all', array(
				'conditions' => array(
					'StockOrder.id' => $option,
				),
				'contain' => array(
					'Client',
					'Company',
					'ContactPeopleClient',
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'DeliveryTour',
					'ReturnTour',
					'StockOrderBatch' => array('StockItem'),
					'DeliveryTransporter',
					'ReturnTransporter'
				),
			));
			$filename = $stockorders[0]['Client']['name'].'_fiche_retour'.'.pdf';
		} else {
			$stockorders = $this->StockOrder->find('all', array(
				'conditions' => array(
					'StockOrder.return_date' => $option,
					'StockOrder.status <>' => array('offer', 'to_invoice', 'invoiced', 'cancelled')
				),
				'contain' => array(
					'Client',
					'Company',
					'ContactPeopleClient',
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'DeliveryTour',
					'ReturnTour',
					'StockOrderBatch' => array('StockItem'),
					'DeliveryTransporter',
					'ReturnTransporter'
				),
			));
			$filename = $option.'_fiches_retour'.'.pdf';
		}

		foreach($stockorders as $k => $stockorder){
			$this->StockOrder->id = $stockorder['StockOrder']['id'];
			$packaging = $this->StockOrder->getPackaging();
			$stockorders[$k]['packaging'] = $packaging;
		}

		$download = true;
		if (!empty($this->request->query)) {
		  if (!$this->request->query['download']) {
			  $download = false;
		  }
		}

		$this->pdfConfig = array(
			'filename' => $filename,
			'download' => $download,
		);

		$this->set(compact('stockorders'));
	}

	public function send($id = null, $recipient = '', $reason = '')
	{
		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		} else {
			$this->StockOrder->id = $id;

			$this->StockOrder->contain(array(
				'Client',
				'ContactPeopleClient',
			));

			if ($this->StockOrder->field('status') == 'offer') {
				$subject = 'Offre';
				$mailTemplate = 'stockorder_offer';
			} elseif ($this->StockOrder->field('status') == 'confirmed') {
				$subject = 'Confirmation de commande';
				$mailTemplate = 'stockorder_confirm';
			} else {
				$subject = 'Commande';
				$mailTemplate = 'stockorder_confirm';
			}
			if ($reason == 'revive_offer') {
				$mailTemplate = 'stockorder_revive_offer';
			}
			$subject .= ' no '.$this->StockOrder->field('order_number');
			$dateBegin = $this->StockOrder->field('service_date_begin');
			$dateEnd = $this->StockOrder->field('service_date_end');
			$date = empty($dateBegin) ? 'date à définir' : strftime('%d.%m.%Y', strtotime($dateBegin));
			$name = $date;

			if (!empty($dateEnd) and $dateBegin != $dateEnd) {
				$toDate = date('d.m.Y', strftime(strtotime($dateEnd)));
				$name .= '-'.$toDate;
			}
			$this->StockOrder->Client->id = $this->StockOrder->field('client_id');
			$this->StockOrder->ContactPeopleClient->id = $this->StockOrder->field('contact_people_id');
			$clientName = $this->StockOrder->Client->field('name');
			$clientDeliveryCity = $this->StockOrder->field('delivery_city');

			$name .= ' - '.$clientName;
			$name .= !empty($clientDeliveryCity) ? ' - '.$clientDeliveryCity : '';
			$filename = $name.'.pdf';

			$this->loadModel('User');
			$sender = $this->User->findById(AuthComponent::user('id'));
			$senderName = $sender['User']['full_name'];
			$senderFunction = empty($sender['User']['festiloc_function']) ? 'Conseil à la clientèle' : $sender['User']['festiloc_function'];

			$serviceDateBegin = $this->StockOrder->field('service_date_begin');
			$params = array(
				'recipient' => $recipient,
				'reason' => $reason,
				'mailTemplate' => $mailTemplate,
				'pdfTemplate' => 'invoice',
				'pdfFilename' => $filename,
				'emailVars' => array(
					'to' => $this->StockOrder->ContactPeopleClient->field('email'),
					'cc' => 'info@festiloc.ch',
					'subject' => $subject,
					'senderName' => $senderName,
					'senderFunction' => $senderFunction,
					'stockorder' => $this->StockOrder->find('first', array(
						'conditions' => array('StockOrder.id' => $id),
						'contain' => array('Client', 'ContactPeopleClient'),
					)),
					'civilities' => Configure::read('ContactPeople.civilities'),
					'emptyDate' => empty($serviceDateBegin) ? true : false,
				),
			);
			if (/*$this->StockOrder->verifyEmail($this->StockOrder->ContactPeopleClient->field('email'), 'info@festiloc.ch') == 'valid'*/ 1 == 1) {
				if ($this->StockOrder->send($params)) {
					$this->Session->setFlash(__('Document has been sent.'), 'alert', array('type' => 'success'));

					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('Document has not been sent.'), 'alert', array('type' => 'danger'));

					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Email of the contact people does not exist.'), 'alert', array('type' => 'danger'));

				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function computeDistance() {
		$this->loadModel('MyTools');

		function file_get_contents_curl($url) {
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = curl_exec($ch);
			curl_close($ch);

			return $data;
		}

		if ($this->request->is('ajax')) {
			$this->autoRender = false;

			$destination = $_POST['toAddress'].'+'.$_POST['toZip'].'+'.$_POST['toCity'];

			$destination = str_replace(' ', '+', $destination);
			$originFestiloc = 'Route+du+Tir-Fédéral+10+1762+Givisiez';

			$infos = $this->MyTools->getTravelTime($originFestiloc, $destination);
			$this->response->body(json_encode($infos));
		}
	}

	public function searchGooglePlaces()
	{
		function file_get_contents_curl($url)
		{
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

			$data = curl_exec($ch);
			curl_close($ch);

			return $data;
		}

		if ($this->request->is('ajax') or 1 == 1) {
			$this->autoRender = false;
			$url = sprintf('https://maps.googleapis.com/maps/api/place/textsearch/json?query=%s&key=%s&radius=300&location=46.7996996,7.1192801', urlencode($_POST['term']), Configure::read('GoogleMapsAPIKey'));
			$data = json_decode(file_get_contents_curl($url), true);
			$places = array();
			if ($data['status'] == 'OK') {
				foreach ($data['results'] as $k => $result) {
					if (!empty($result['formatted_address'])) {
						$places[$k]['id'] = $result['id'];
						$places[$k]['text'] = $result['name'];
						$places[$k]['icon'] = $result['icon'];
						$tmp = explode(',', $result['formatted_address']);
						if (sizeof($tmp) == 4) {
							$places[$k]['address'] = trim($tmp[0]).', '.trim($tmp[1]);
							$places[$k]['zip_city'] = trim($tmp[2]);
							$zipcity = explode(' ', trim($tmp[2]));
							$places[$k]['zip'] = trim($zipcity[0]);
							$places[$k]['city'] = trim($zipcity[1]);
							$places[$k]['country'] = trim($tmp[3]);
						} else {
							$places[$k]['address'] = trim($tmp[0]);
							$places[$k]['zip_city'] = trim($tmp[1]);
							$zipcity = explode(' ', trim($tmp[1]));
							$places[$k]['zip'] = trim($zipcity[0]);
							$places[$k]['city'] = trim($zipcity[1]);
							$places[$k]['country'] = trim($tmp[2]);
						}
						// $places[$k]['zip_city'] = trim($tmp[1]);
						// $places[$k]['zip'] = trim($tmp1[0]);
						// $places[$k]['city'] = trim($tmp1[1]);
						// $places[$k]['reference'] = $result['reference'];
						// if(isset($result['photos'][0]['photo_reference']) && 1==2){
						// 	$url1 = sprintf('https://maps.googleapis.com/maps/api/place/photo?photoreference=%s&key=%s&maxwidth=200', $result['photos'][0]['photo_reference'], Configure::read('GoogleMapsAPIKey'));
						// 	if(file_get_contents($url1)){
						// 		$places[$k]['photo'] = $url1;
						// 	} else {
						// 		$places[$k]['photo'] = $result['icon'];
						// 	}
						// }
					}
				}
			}
			$this->response->body(json_encode($places));
		}
	}

	public function check_return($id = null)
	{
		$this->StockOrder->id = $id;

		if (!$this->StockOrder->exists($id)) {
			throw new NotFoundException(__('Invalid Stock order'));
		}

		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'StockOrderBatch' => array(
					'StockItem',
				),
				'Client',
				'Company',
			),
		));

		if ($this->request->is('post') || $this->request->is('put')) {
		} else {
			$this->request->data = $stockorder;
		}
	}

	public function isProcessed($id)
	{
		$this->StockOrder->id = $id;
		$message = $this->StockOrder->isProcessed();
		$this->set(array(
			'message' => $message,
			'_serialize' => array('message'),
		));
	}

	public function export_to_cresus()
	{
		$this->loadModel('Export');
		if ($this->Export->cresus('festiloc', 'clients') && $this->Export->cresus('festiloc', 'factures')) {
			$this->Session->setFlash(__('Export file has been generated. You can now import invoices in Cresus.'), 'alert', array('type' => 'success'));

			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('Something went wrong... Please try again.'), 'alert', array('type' => 'danger'));

			return $this->redirect(array('action' => 'index'));
		}
	}

	public function live($id = null, $mode = '')
	{
		return $this->redirect(array('controller' => 'festiloc', 'action' => 'depot'));
		$this->layout = 'festiloc';

		if (!empty($id)) {
			$stockorder = $this->StockOrder->find('first', array(
				'conditions' => array(
					'StockOrder.id' => $id,
				),
				'contain' => array(
					'StockOrderBatch' => array(
						'StockItem' => array('Document'),
					),
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'ContactPeopleClient',
					'Client',
					'StockOrderExtraHour',
				),
			));
			$this->set(compact('stockorder'));
			$collaborators = $this->StockOrder->Responsable->find('list', array(
				'conditions' => array(
					'Responsable.role' => 'fixed',
				),
				'order' => 'Responsable.full_name',
				'fields' => array('Responsable.id', 'Responsable.full_name'),
			));
			$this->set(compact('collaborators'));
		}

		if (!empty($mode)) {
			if ($mode == 'return') {
				$this->render('live_return');
			}
			if ($mode == 'issue') {
				$this->render('live_issue');
			}
		}
	}

	public function live_list()
	{
		if ($this->request->is('ajax')) {
			$conditions[] = array('StockOrder.status IN' => array('confirmed', 'to_proceed', 'in_progress', 'processed', 'delivered'));
			if (!empty($_POST['dateType'])) {
				$dateType = $_POST['dateType'];
			}
			if (!empty($_POST['fromDate'])) {
				if ($dateType == 'return') {
					$conditions[] = array('StockOrder.return_date >=' => date('Y-m-d', strtotime($_POST['fromDate'])));
				} else {
					$conditions[] = array('StockOrder.delivery_date >=' => date('Y-m-d', strtotime($_POST['fromDate'])));
				}
			}
			if (!empty($_POST['toDate'])) {
				if ($dateType == 'return') {
					$conditions[] = array('StockOrder.return_date <=' => date('Y-m-d', strtotime($_POST['toDate'])));
				} else {
					$conditions[] = array('StockOrder.delivery_date <=' => date('Y-m-d', strtotime($_POST['toDate'])));
				}
			}
			if (empty($_POST['fromDate']) and empty($_POST['toDate'])) {
				$conditions[] = array('StockOrder.delivery_date >=' => date('Y-m-d'));
			}
			if (!empty($_POST['statuses'])) {
				$conditions[] = array('StockOrder.status' => $_POST['statuses']);
			}

			$data = $this->StockOrder->find('all', array(
				'conditions' => $conditions,
				'contain' => array(
					'Client',
					'ContactPeopleClient',
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'StockOrderBatch' => array('StockItem' => array('StockCategory')),
					'IcingVersion',
					'DeliveryTransporter',
				),
				'order' => array('StockOrder.delivery_date'),
				'group' => array('StockOrder.id'),
			));

			$categories = array();
			foreach ($data as $k => $order) {
				$cats = array();
				if (!empty($order['StockOrderBatch'])) {
					foreach ($order['StockOrderBatch'] as $batch) {
						if (!empty($batch['StockItem']['StockCategory']['code'])) {
							$cats[$batch['StockItem']['StockCategory']['code']] = $batch['StockItem']['StockCategory']['name'];
						}
					}
					ksort($cats);
				}
				$categories[$order['StockOrder']['id']] = $cats;
			}

			$ignoredFields = array(
				'status',
				'client_id',
				'contact_people_id',
				'delivery_contact_people_id',
				'return_contact_people_id',
				'total_ht',
				'fidelity_discount_amount',
				'tva',
				'net_total',
				'modified',
				'created',
				'number_of_products',
				'id',
				'import_id',
				'zip',
				'city',
				'packaging_costs',
				'order_number',
				'version_id_read',
				'forced_net_total',
				'quantity_discount',
				'distance_covered',
				'fidelity_discount',
				'quantity_discount_percentage',
				'delivery_distance',
				'invoice_email',
				'return',
				'delivery',
				'controlled',
				'delivery_transportation_costs',
				'return_transportation_costs',
				'forced_delivery_costs',
				'delivery_costs',
				'fidelity_discount_percentage',
				'extrahours_costs',
				'number_of_pallets1',
				'number_of_rollis1',
				'xl_surcharge1',
				'delivery_transporter_id',
				'return_transporter_id',
				'controlled_by',
				'notify_cancellation',
				'first_name',
				'last_name',
				'full_name',
				'full_name_phone_email',
				'deposit',
				'delivery_tour_id',
				'return_tour_id',
				'invoiced_total',
				'cancellation_reason',
				'cancellation_remarks',
				'number_of_pallets_xl',
				'number_of_pallets_xl1',
				'number_of_rollis_xl',
				'number_of_rollis_xl1',
				'xl_surcharge',
				'delivery_return_same',
				'festiloc_fidelity_discount',
			);
			$stockorders = array();
			$versions = array();
			$products = array();
			foreach ($data as $k => $order) {
				$oldProducts = array();
				$actualProducts = array();
				$lastVersion = array();
				$v = array();

				// we have to find the first icing version or the last one which has been read
				if (!empty($order['StockOrder']['version_id_read'])) {
					$v = $this->StockOrder->IcingVersion->find('first', array(
						'conditions' => array(
							'id' => $order['StockOrder']['version_id_read'],
							'model_id' => $order['StockOrder']['id'],
						),
					));
					if(empty($v)){
					  $v = $this->StockOrder->IcingVersion->find('first', array(
						  'conditions' => array(
							  'model_id' => $order['StockOrder']['id'],
														  'is_minor_version' => 0,
						  ),
						  'order' => 'IcingVersion.created ASC',
					  ));
					}
				} else {
					$v = $this->StockOrder->IcingVersion->find('first', array(
						'conditions' => array(
							'model_id' => $order['StockOrder']['id'],
														'is_minor_version' => 0,
						),
						'order' => 'IcingVersion.created ASC',
					));
				}
				if (!empty($v['IcingVersion'])) {
					$lastVersion = $this->StockOrder->IcingVersion->find('first', array(
						'joins' => array(
							array(
								'table' => 'stock_orders',
								'alias' => 'StockOrder',
								'conditions' => array(
									'IcingVersion.model_id = StockOrder.id',
								),
							),
						),
						'conditions' => array(
							'IcingVersion.model_id' => $order['StockOrder']['id'],
							'StockOrder.status' => array('in_progress', 'processed', 'delivered'),
							'StockOrder.id' => $order['StockOrder']['id'],
							'IcingVersion.created >' => $v['IcingVersion']['created'],
						),
						'order' => 'IcingVersion.created ASC',
					));
					if (empty($lastVersion)) {
						$lastVersion = $v;
					}
				} else {
					$lastVersion = $this->StockOrder->IcingVersion->find('first', array(
						'joins' => array(
							array(
								'table' => 'stock_orders',
								'alias' => 'StockOrder',
								'conditions' => array(
									'IcingVersion.model_id = StockOrder.id',
								),
							),
						),
						'conditions' => array(
							'IcingVersion.model_id' => $order['StockOrder']['id'],
							'StockOrder.status' => array('in_progress', 'processed', 'delivered'),
						),
						'order' => 'created ASC',
					));
				}
				if (!empty($lastVersion) and !empty($v)) {
					$difference = $this->StockOrder->diffVersion($lastVersion['IcingVersion']['id']);
					$oldOrder = json_decode($lastVersion['IcingVersion']['json'], true);
					$actualOrder = $order;

					if ($lastVersion['IcingVersion']['id'] != $order['StockOrder']['version_id_read']) {
						foreach ($difference as $model => $data) {
							$order[$model]['old'] = array();
							$order[$model]['actual'] = array();
							if (!empty($data)) {
								if ($model == 'StockOrderBatch') {
									$order['StockOrder']['notify'] = 1;
									$oldProducts = array();
									$actualProducts = array();
									foreach ($oldOrder['StockOrderBatch'] as $batch) {
										if ($batch['stock_item_id'] == -1) {
											$key = '9999'.$batch['id'];
											$products[$key] = array('StockItem' => array('name' => $batch['name'], 'code' => '#'));
										} else {
											$key = $batch['stock_item_id'].$batch['id'];
											$products[$key] = $this->StockOrder->StockOrderBatch->StockItem->findById($batch['stock_item_id']);
										}
										$oldProducts[$key] = $batch;
									}
									foreach ($actualOrder['StockOrderBatch'] as $batch) {
										if ($batch['stock_item_id'] == -1) {
											$key = '9999'.$batch['id'];
											$products[$key] = array('StockItem' => array('name' => $batch['name'], 'code' => '#'));
										} else {
											$key = $batch['stock_item_id'].$batch['id'];
											$products[$key] = $this->StockOrder->StockOrderBatch->StockItem->findById($batch['stock_item_id']);
										}
										$actualProducts[$key] = $batch;
									}
									$order[$model]['old'] = $oldProducts;
									$order[$model]['actual'] = $actualProducts;
								} elseif ($model != 'History') {
									foreach ($ignoredFields as $key) {
										unset($data[$key]);
									}
									if (!empty($data)) {
										$order['StockOrder']['notify'] = 1;
										foreach ($data as $key => $value) {
											$versions[$order['StockOrder']['id']][$model][$key]['old'] = empty($oldOrder[$model][$key]) ? '' : $oldOrder[$model][$key];
											$versions[$order['StockOrder']['id']][$model][$key]['new'] = empty($actualOrder[$model][$key]) ? '' : $actualOrder[$model][$key];
										}
									}
								}
							}
						}
					}
				}
				$stockorders[$k] = $order;
			}

			// search for cancellations
			$cancellations = $this->StockOrder->find('all', array(
				'conditions' => array(
					'status' => 'cancelled',
					'notify_cancellation' => 1,
				),
				'contain' => array(
					'Client',
				),
			));

			$this->set(compact('stockorders'));
			$this->set(compact('versions'));
			$this->set(compact('products'));
			$this->set(compact('categories'));
			$this->set(compact('cancellations'));
		}
	}

	public function save($destination = '') {
		App::uses('CakeEmail', 'Network/Email');

		if ($this->request->is('post') or $this->request->is('put')) {
			$this->loadModel('StockItemUnavailability');

			$this->StockOrder->id = $this->request->data['StockOrder']['id'];
			$status = $this->StockOrder->field('status');
			if (!empty($this->request->data['StockOrder']['delivered_quantity'])) {
				$this->request->data['status'] = $status; // we don't change status if we validate issue
				// need to update unavailabilities if quantity is different
				for ($i = 0; $i < count($this->request->data['StockOrder']['delivered_quantity']); ++$i) {
					$this->StockOrder->StockOrderBatch->id = $this->request->data['StockOrder']['stock_order_batch_id'][$i];
					$quantity = $this->StockOrder->StockOrderBatch->field('quantity');

					$this->StockOrder->StockOrderBatch->save(
						array(
							'delivered_quantity' => $this->request->data['StockOrder']['delivered_quantity'][$i],
							'delivery_remarks' => empty($this->request->data['StockOrder']['delivery_remarks'][$i]) ? '' : $this->request->data['StockOrder']['delivery_remarks'][$i],
							'invoice_replacement' => 0,
						)
					);

					if ($quantity != $this->request->data['StockOrder']['delivered_quantity'][$i]) {
						$unavailability = $this->StockItemUnavailability->find('all', array(
							'conditions' => array(
								'StockItemUnavailability.stock_item_id' => isset($this->request->data['StockOrder']['stock_item_id'][$i]) ? $this->request->data['StockOrder']['stock_item_id'][$i] : 0,
								'StockItemUnavailability.stock_order_id' => $this->StockOrder->id,
							),
							'fields' => array(
								'StockItemUnavailability.id',
							),
						));
						if (!empty($unavailability)) {
							foreach ($unavailability as $item) {
								$this->StockItemUnavailability->id = $item['StockItemUnavailability']['id'];
								$this->StockItemUnavailability->saveField('quantity', $this->request->data['StockOrder']['delivered_quantity'][$i], array('callbacks' => false));
							}
						}
					}
				}
			}
			if (!empty($this->request->data['StockOrder']['returned_quantity'])) {
				if($this->request->data['destination'] == 'invoice'){
					$this->request->data['status'] = 'to_invoice';
				}
				for ($i = 0; $i < count($this->request->data['StockOrder']['returned_quantity']); ++$i) {
					$this->StockOrder->StockOrderBatch->id = $this->request->data['StockOrder']['stock_order_batch_id'][$i];
					$this->StockOrder->StockOrderBatch->StockItem->id = $this->request->data['StockOrder']['stock_item_id'][$i];

					$quantityDiff = $this->request->data['StockOrder']['returned_quantity'][$i] - $this->request->data['StockOrder']['delivered_quantity'][$i];

					if(!empty($this->request->data['destination']) && $this->request->data['destination'] == 'invoice' && $quantityDiff < 0){
						$actualQuantity = $this->StockOrder->StockOrderBatch->StockItem->field('quantity');
						$newQuantity = $actualQuantity - abs($quantityDiff);
						$this->StockOrder->StockOrderBatch->StockItem->saveField('quantity', $newQuantity);
					}

					$this->StockOrder->StockOrderBatch->save(
						array(
							'returned_quantity' => $this->request->data['StockOrder']['returned_quantity'][$i],
							'reprocessing_duration' => $this->request->data['StockOrder']['reprocessing_duration'][$i],
							'delivery_remarks' => $this->request->data['StockOrder']['delivery_remarks'][$i],
							'invoice_replacement' => 0,
						)
					);
					if (!empty($this->request->data['StockOrder']['reprocessing_duration'][$i])) {
						//$start_date = $this->request->data['StockOrder']['return_date'];
						$start_date = date('Y-m-d', strtotime($this->request->data['StockOrder']['return_date'].'+ 1 weekday'));
						$end_date = date('Y-m-d', strtotime($start_date.'+ '.$this->request->data['StockOrder']['reprocessing_duration'][$i].' weekday'));
						$existingUnavailability = $this->StockItemUnavailability->find('first', array(
							'conditions' => array(
								'stock_item_id' => isset($this->request->data['StockOrder']['stock_item_id'][$i]) ? $this->request->data['StockOrder']['stock_item_id'][$i] : 0,
								'stock_order_id' => $this->request->data['StockOrder']['id'],
								'status' => 'reconditionning',
							),
						));
						$this->StockItemUnavailability->save(array(
							'StockItemUnavailability' => array(
								'id' => !empty($existingUnavailability) ? $existingUnavailability['StockItemUnavailability']['id'] : '',
								'stock_item_id' => isset($this->request->data['StockOrder']['stock_item_id'][$i]) ? $this->request->data['StockOrder']['stock_item_id'][$i] : 0,
								'stock_order_id' => $this->request->data['StockOrder']['id'],
								'quantity' => $this->request->data['StockOrder']['returned_quantity'][$i],
								'start_date' => $start_date,
								'end_date' => $end_date,
								'status' => 'reconditionning'
							),
						));
						$this->StockOrder->StockOrderBatch->StockItem->id = $this->request->data['StockOrder']['stock_item_id'][$i];
					}
				}
				for ($j = 0; $j < count($this->request->data['StockOrderExtraHour']['duration']); ++$j) {
					$this->StockOrder->StockOrderExtraHour->create();
					$data = array(
						'StockOrderExtraHour' => array(
							'stock_order_id' => $this->request->data['StockOrder']['id'],
							'duration' => $this->request->data['StockOrderExtraHour']['duration'][$j],
							'task' => $this->request->data['StockOrderExtraHour']['task'][$j],
							'id' => empty($this->request->data['StockOrderExtraHour']['id'][$j]) ? '' : $this->request->data['StockOrderExtraHour']['id'][$j],
						),
					);
					if (!empty($data['StockOrderExtraHour']['duration']) and !empty($data['StockOrderExtraHour']['task'])) {
						$this->StockOrder->StockOrderExtraHour->save($data);
					}
				}
			}

			if (!empty($this->request->data['StockOrder']['return_control_remarks'])) {
				$this->StockOrder->saveField('return_control_remarks', $this->request->data['StockOrder']['return_control_remarks'], array('callbacks' => false));
			}

			if (!empty($this->request->data['StockOrder']['payment_method'])) {
				$this->StockOrder->saveField('payment_method', $this->request->data['StockOrder']['payment_method'], array('callbacks' => false));
			}

			if (!empty($this->request->data['StockOrder']['payment_date'])) {
				$this->StockOrder->saveField('payment_date', date('Y-m-d', strtotime($this->request->data['StockOrder']['payment_date'])), array('callbacks' => false));
			}

			if (!empty($this->request->data['StockOrder']['on_site_payment'])) {
				$this->StockOrder->saveField('on_site_payment', $this->request->data['StockOrder']['on_site_payment'], array('callbacks' => false));
			}
			// determine replacement costs (useful when return form is updated during invoicement)
			$this->StockOrder->StockOrderBatch->contain('StockItem');
			$batches = $this->StockOrder->StockOrderBatch->find('all', array(
				'conditions' => array(
					'StockOrderBatch.stock_order_id' => $this->StockOrder->id,
				),
			));
			$replacementCosts = $this->StockOrder->field('replacement_costs');
			$this->StockOrder->saveField('replacement_costs', 0, array('callbacks' => false));
			$this->StockOrder->saveField('extrahours_costs', 0, array('callbacks' => false));
			$this->StockOrder->computeTotal(); // compute new total and tva

			$this->StockOrder->saveField('status', $this->request->data['status'], array('callbacks' => false));
			if (!empty($this->request->data['destination'])) {
				if ($this->request->data['destination'] == 'live') {
					$this->Session->setFlash(__('Stock order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'live'));
				} elseif ($this->request->data['destination'] == 'invoice') {
					$this->Session->setFlash(__('Stock order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'depot', 'controller' => 'festiloc'));
				} elseif ($this->request->data['destination'] == 'return') {
					$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'live', $this->StockOrder->id, 'return'));
				} elseif ($this->request->data['destination'] == 'issue') {
					$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'live', $this->StockOrder->id, 'issue'));
				} elseif ($this->request->data['destination'] == 'depot_issue') {
					$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('controller' => 'festiloc', 'action' => 'depot', 'issue', $this->StockOrder->id));
				} elseif ($this->request->data['destination'] == 'depot_return') {
					$this->Session->setFlash(__('Order has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('controller' => 'festiloc', 'action' => 'depot', 'return', $this->StockOrder->id));
				}
			} else {
				$this->Session->setFlash(__('Stock order has been saved.'), 'alert', array('type' => 'success'));
				if ($this->request->data['status'] == 'to_invoice') {
					$this->Session->setFlash(__('Stock order has been saved and will be invoiced.'), 'alert', array('type' => 'success'));
				}
				$this->set('id', $this->StockOrder->id);
				return $this->redirect(array('controller' => 'festiloc', 'action' => 'depot#'.$this->StockOrder->id));
			}
		}
	}

	public function setStatus()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $this->request->data['stock_order_id'];
			if ($this->StockOrder->saveField('status', $this->request->data['status'], array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updatePackaging()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['id'];
			if (
				$this->StockOrder->saveField('number_of_pallets1', $_POST['numberOfPallets'], array('callbacks' => false)) and
				$this->StockOrder->saveField('number_of_rollis1', $_POST['numberOfRollis'], array('callbacks' => false)) and
				$this->StockOrder->saveField('number_of_pallets_xl1', $_POST['numberOfPalletsXL'], array('callbacks' => false)) and
				$this->StockOrder->saveField('number_of_rollis_xl1', $_POST['numberOfRollisXL'], array('callbacks' => false))
				) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function readModifications()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['stock_order_id'];
			$version = $this->StockOrder->IcingVersion->find('first', array(
				'conditions' => array(
					'IcingVersion.model_id' => $_POST['stock_order_id'],
				),
				'order' => 'IcingVersion.created DESC',
				'fields' => 'IcingVersion.id',
			));
			$data = array('StockOrder' => array(
				'id' => $_POST['stock_order_id'],
				'version_id_read' => $version['IcingVersion']['id'],
				'notify_depot' => 0
			));
			if ($this->StockOrder->save($data, array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function prepareWeek()
	{
		$view = new View($this);
		$html = $view->loadHelper('Html');
		$transporters = $this->StockOrder->Transporter->find('all', array(
				'conditions' => array(),
			));
		$this->set(compact('transporters'));

		if ($this->request->is('ajax')) {
			$mode = $_POST['mode'];
			$displayClients = $_POST['displayClients'];

			$view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
			$ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
			$start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
			$end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
			$start_date = date('Y-m-d', $start);
			$end_date = date('Y-m-d', $end);
			$options = array();

			if ($mode == 'delivery') {
				$options['conditions'] = array(
					'delivery_date >=' => $start_date,
					'delivery_date <=' => $end_date,
					'delivery' => 1,
					'StockOrder.type <>' => 'withdrawal'
				);
				$options['contain'] = array(
					'Client',
					'DeliveryTransporter',
					'DeliveryTransporterHistory',
					'DeliveryTour',
				);
			} elseif ($mode == 'return') {
				$options['conditions'] = array(
					'return_date >=' => $start_date,
					'return_date <=' => $end_date,
					'return' => 1,
					'StockOrder.type <>' => 'delivery'
				);
				$options['contain'] = array(
					'Client',
					'ReturnTransporter',
					'ReturnTransporterHistory',
					'ReturnTour',
				);
			}
			if ($displayClients) {
				unset($options['conditions'][$mode]);
			}

			if( date('W') <= date('W', $end) && date('Y') == date('Y', $end) ){
				$options['conditions'][] = array('status NOT IN' => array('offer', 'to_invoice', 'invoiced', 'cancelled'));
			} else {
				$options['conditions'][] = array('status NOT IN' => array('offer', 'cancelled'));
			}

			$data = $this->StockOrder->find('all', $options);

			$orders = array();
			$view = new View($this);
			$html = $view->loadHelper('Html');
			$moments = Configure::read('StockOrders.delivery_moments');
			foreach ($data as $k => $order) {
				if ($mode == 'delivery') {
					$tourId = !empty($order['DeliveryTour']) ? $order['DeliveryTour'][0]['code'] : 99;
					$vehicleTourId = !empty($order['DeliveryTour']) ? $order['DeliveryTour'][0]['id'] : '';
					$id = $tourId * 100 .$order['StockOrder']['delivery_transporter_id'].$order['StockOrder']['id'].'d';
					$start = $order['StockOrder']['delivery_date'];
					// $className = strtolower($order['DeliveryTransporter']['name']).' delivery';
					// $className .= ' '.$order['StockOrder']['delivery_mode'];
										$className = 'delivery' . ' ' . $order['StockOrder']['delivery_mode'];;
					if($order['DeliveryTransporter']['id'] != 1){
											$bgColor = $order['DeliveryTransporter']['color'];
										} else {
											$bgColor = '';
										}
					$title = $order['StockOrder']['order_number'].' '.$order['Client']['name'];
					$description = $html->tag('h4', $order['StockOrder']['type'] == 'delivery' ? __('DELIVERY') :  __('DELIVERY'));
					$description .= $html->tag('strong', __($moments[$order['StockOrder']['delivery_moment']]));
					$description .= $html->tag('strong', $order['StockOrder']['delivery_address']);
					$description .= !empty($order['StockOrder']['delivery_zip_city']) ? $html->tag('strong', $order['StockOrder']['delivery_zip_city']) : '';
					$address = $order['StockOrder']['delivery_address'].', '.$order['StockOrder']['delivery_city'].', '.$order['StockOrder']['delivery_zip'].', Suisse';
					$transporter = $order['StockOrder']['delivery_mode'] == 'festiloc' ? 'festiloc' : strtolower($order['DeliveryTransporter']['name']);
				}
				if ($mode == 'return') {
					$tourId = !empty($order['ReturnTour']) ? $order['ReturnTour'][0]['code'] : 99;
					$vehicleTourId = !empty($order['ReturnTour']) ? $order['ReturnTour'][0]['id'] : '';
					$id = $tourId * 100 .$order['StockOrder']['return_transporter_id'].$order['StockOrder']['id'].'r';
					$start = $order['StockOrder']['return_date'];
					// $className = strtolower($order['DeliveryTransporter']['name']).' delivery';
					// $className .= ' '.$order['StockOrder']['delivery_mode'];
										$className = 'return' . ' ' . $order['StockOrder']['return_mode'];;
					if($order['ReturnTransporter']['id'] != 1){
											$bgColor = $order['ReturnTransporter']['color'];
										} else {
											$bgColor = '';
										}
					$title = $order['StockOrder']['order_number'].' '.$order['Client']['name'];
					$description = $html->tag('h4', $order['StockOrder']['type'] == 'withdrawal' ? strtoupper(__('Withdrawal')) :  __('RETURN'));
					$description .= $html->tag('strong', __($moments[$order['StockOrder']['return_moment']]));
					$description .= $html->tag('strong', $order['StockOrder']['return_address']);
					$description .= !empty($order['StockOrder']['return_zip_city']) ? $html->tag('strong', $order['StockOrder']['return_zip_city']) : '';
					$address = $order['StockOrder']['return_address'].', '.$order['StockOrder']['return_city'].', '.$order['StockOrder']['return_zip'].', Suisse';
					$transporter = $order['StockOrder']['return_mode'] == 'festiloc' ? 'festiloc' : strtolower($order['ReturnTransporter']['name']);
				}
				$orders[$k]['id'] = $id;
				$orders[$k]['start'] = $start;
				$orders[$k]['className'] = $className;
				$orders[$k]['bgColor'] = $bgColor;
				$orders[$k]['stock_order_id'] = $order['StockOrder']['id'];
				$orders[$k]['tour_id'] = $tourId;
				$orders[$k]['vehicle_tour_id'] = $vehicleTourId;
				$orders[$k]['title'] = $title;
				$orders[$k]['mode'] = $mode;
				$orders[$k]['address'] = $address;
				$orders[$k]['transporter'] = $transporter;
				$orders[$k]['url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'invoice', 'ext' => 'pdf', '?' => 'download=0&showPrices=0', $order['StockOrder']['id']));
				$orders[$k]['transporter_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'transporter_pdf', 'ext' => 'pdf', '?' => 'download=0&type=' . $mode, $order['StockOrder']['id']));
				$orders[$k]['description'] = $description;
				$this->StockOrder->id = $order['StockOrder']['id'];
				$packaging = $this->StockOrder->getPackaging();
				if (!empty($packaging['pallets'])) {
					$orders[$k]['description'] .= $html->tag('strong', $packaging['pallets'].' '.__('pallets'));
				}
				if (!empty($packaging['rollis'])) {
					$orders[$k]['description'] .= $html->tag('strong', $packaging['rollis'].' '.__('rollis'));
				}
				if (!empty($packaging['pallets_xl'])) {
					$orders[$k]['description'] .= $html->tag('strong', $packaging['pallets_xl'].' '.__('pallets XL'));
				}
				if (!empty($packaging['rollis_xl'])) {
					$orders[$k]['description'] .= $html->tag('strong', $packaging['rollis_xl'].' '.__('rollis XL'));
				}
				$orders[$k]['description'] .= $html->tag('br');
				$orders[$k]['description'] .= $html->tag('span', $order['StockOrder']['order_number']);
				$orders[$k]['description'] .= $html->tag('br');
				$orders[$k]['description'] .= $html->tag('span', $order['StockOrder']['name']);
				if (!empty($order['StockOrder']['service_date_begin'])) {
					$orders[$k]['description'] .= $html->tag('br');
					$orders[$k]['description'] .= $html->tag('span', date('d.m.Y', strtotime($order['StockOrder']['service_date_begin'])));
				}
				if (!empty($order['StockOrder']['service_date_end'])) {
					$orders[$k]['description'] .= ' - '.$html->tag('span', date('d.m.Y', strtotime($order['StockOrder']['service_date_end'])));
				}
				if ($mode == 'delivery') {
					if (!empty($order['DeliveryTransporterHistory'])) {
						if ($order['DeliveryTransporterHistory'][0]['date'] >= $order['StockOrder']['modified']) {
							$orders[$k]['className'] .= ' notified';
						}
					}
					$orders[$k]['description'] .= $html->tag('span', empty($order['DeliveryTour']) ? '' : $order['DeliveryTour'][0]['code'], array('class' => 'tour'));
				}
				if ($mode == 'return') {
					if (!empty($order['ReturnTransporterHistory'])) {
						if ($order['ReturnTransporterHistory'][0]['date'] >= $order['StockOrder']['modified']) {
							$orders[$k]['className'] .= ' notified';
						}
					}
					$orders[$k]['description'] .= $html->tag('span', empty($order['ReturnTour']) ? '' : $order['ReturnTour'][0]['code'], array('class' => 'tour'));
				}

				if (!empty($order['VehicleReservation']) && 1 == 2) {
					$orders[$k]['description'] .=
					$html->tag('br').
					$order['VehicleReservation'][0]['Vehicle']['name'].
					' - '.
					date('H:i', strtotime($order['VehicleReservation'][0]['start'])).
					'-'.
					date('H:i', strtotime($order['VehicleReservation'][0]['end']));
				}
				$orders[$k]['allDay'] = true;
				if ($displayClients) {
					$orders[$k]['className'] .= ' client';
				}
				$displayClients = false;
			}
			//$this->set(compact('orders'));
			$this->set('_json', $orders);
		}
	}

	public function portlet($id = null, $mode = null)
	{
		//debug($id);
		$mode = empty($mode) ? $this->request->data['mode'] : $mode;
		$stockorder = $this->StockOrder->find('first', array(
			'conditions' => array(
				'StockOrder.id' => $id,
			),
			'contain' => array(
				'Client',
				'ContactPeopleClient',
				'DeliveryTour',
				'ReturnTour',
			),
		));
		$data = array();
		if ($mode == 'delivery') {
			$data['address'] = $stockorder['StockOrder']['delivery_address'];
			$data['zip_city'] = $stockorder['StockOrder']['delivery_zip_city'];
			$data['tour_id'] = empty($stockorder['DeliveryTour']) ? '' : $stockorder['DeliveryTour'][0]['code'];
		}
		if ($mode == 'return') {
			$data['address'] = $stockorder['StockOrder']['return_address'];
			$data['zip_city'] = $stockorder['StockOrder']['return_zip_city'];
			$data['tour_id'] = empty($stockorder['ReturnTour']) ?  '' : $stockorder['ReturnTour'][0]['code'];
		}
		$this->StockOrder->id = $id;
		$packaging = $this->StockOrder->getPackaging();
		$this->set(compact('stockorder'));
		$this->set(compact('mode'));
		$this->set(compact('data'));
		$this->set(compact('packaging'));
	}

	public function update($field = '') {
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['id'];
			if ($this->StockOrder->saveField($field, $_POST[$field], array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
				if($field == 'delivery_transporter_id' AND $_POST[$field] == 1){
				  $this->StockOrder->saveField('delivery_mode', 'festiloc', array('callbacks' => false));
				} elseif( $field == 'delivery_transporter_id' ) {
				  $this->StockOrder->saveField('delivery_mode', 'transporter', array('callbacks' => false));
				}
				if($field == 'return_transporter_id' AND $_POST[$field] == 1){
				  $this->StockOrder->saveField('return_mode', 'festiloc', array('callbacks' => false));
				} elseif( $field == 'return_transporter_id' ) {
				  $this->StockOrder->saveField('return_mode', 'transporter', array('callbacks' => false));
				}
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function changeDeliveryMode()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['stock_order_id'];
			if ($this->StockOrder->saveField('delivery_mode', $_POST['delivery_mode'], array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function changeReturnMode()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['stock_order_id'];
			if ($this->StockOrder->saveField('return_mode', $_POST['return_mode'], array('callbacks' => false))) {
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function getPlaces() {
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$places1 = $this->StockOrder->find('all', array(
				'conditions' => array(
					'StockOrder.delivery_place LIKE' => '%'.$this->request->data['q'].'%',
				),
				'order' => 'created ASC, delivery_place',
			));
			$places2 = $this->StockOrder->find('all', array(
				'conditions' => array(
					'StockOrder.return_place LIKE' => '%'.$this->request->data['q'].'%',
				),
				'order' => 'created ASC, return_place',
			));
			$data = array();
			foreach ($places1 as $k => $place) {
				if(empty($place['StockOrder']['delivery_zip']) || empty($place['StockOrder']['delivery_city'])){
					continue;
				}
				$data[$place['StockOrder']['delivery_place']]['text'] = $place['StockOrder']['delivery_place'];
				if(empty($place['StockOrder']['delivery_address'])){
					if(!empty($data[$place['StockOrder']['delivery_place']]['address'])){
						$data[$place['StockOrder']['delivery_place']]['address'] = $data[$place['StockOrder']['delivery_place']]['address'];
					}
				} else {
					$data[$place['StockOrder']['delivery_place']]['address'] = $place['StockOrder']['delivery_address'];
				}
				$data[$place['StockOrder']['delivery_place']]['zip'] = $place['StockOrder']['delivery_zip'];
				$data[$place['StockOrder']['delivery_place']]['city'] = $place['StockOrder']['delivery_city'];
				$data[$place['StockOrder']['delivery_place']]['zip_city'] = $place['StockOrder']['delivery_zip_city'];
			}
			foreach ($places2 as $k => $place) {
				if(empty($place['StockOrder']['return_zip']) || empty($place['StockOrder']['return_city'])){
					continue;
				}
				$data[$place['StockOrder']['return_place']]['text'] = $place['StockOrder']['return_place'];
				if(empty($place['StockOrder']['return_address'])){
					if(!empty($data[$place['StockOrder']['return_place']]['address'])){
						$data[$place['StockOrder']['return_place']]['address'] = $data[$place['StockOrder']['return_place']]['address'];
					}
				} else {
					$data[$place['StockOrder']['return_place']]['address'] = $place['StockOrder']['return_address'];
				}
				$data[$place['StockOrder']['return_place']]['zip'] = $place['StockOrder']['return_zip'];
				$data[$place['StockOrder']['return_place']]['city'] = $place['StockOrder']['return_city'];
				$data[$place['StockOrder']['return_place']]['zip_city'] = $place['StockOrder']['return_zip_city'];
			}
			$this->response->body(json_encode($data));
		}
	}

	public function manually_notify_transporter(){
	  $this->loadModel('History');
	  if($this->request->is('ajax')){
		$this->autoRender = false;
		$history = array(
		  'History' => array(
			'date' => date('Y-m-d H:i:s'),
			'reason' => $this->request->data['type'] == 'delivery' ? 'delivery_transporter_notify' : 'return_transporter_notify',
			'parent_id' => $this->request->data['stock_order_id'],
		  )
		);
		if($this->History->save($history)){
		  $this->response->body(json_encode(array('success' => 1)));
		} else {
		  $this->response->body(json_encode(array('success' => 0)));
		}
	  }
	}

	public function notify_transporter() {
		App::uses('CakePdf', 'CakePdf.Pdf');
		App::uses('CakeEmail', 'Network/Email');

		if ($this->request->is('ajax')) {
			$this->autoRender = false;

			if($this->request->data['type'] == 'delivery'){
			  $this->StockOrder->contain(array(
				  'Client',
				  'StockOrderBatch' => array('StockItem'),
				  'DeliveryTransporter' => array('ContactPeople'),
				  'DeliveryTransporterHistory'
			  ));
			} elseif($this->request->data['type'] == 'return'){
			  $this->StockOrder->contain(array(
				  'Client',
				  'StockOrderBatch' => array('StockItem'),
				  'ReturnTransporter' => array('ContactPeople'),
				  'ReturnTransporterHistory'
			  ));
			}

			$order = $this->StockOrder->findById($this->request->data['stock_order_id']);
			$this->StockOrder->id = $order['StockOrder']['id'];

			$output = array();
			$type = '';
			$contactPeoples = array();
			if (!empty($order['DeliveryTransporter']) and $order['DeliveryTransporter']['notify'] == 1 AND $this->request->data['type'] == 'delivery') {
				$contactPeoples = empty($order['DeliveryTransporter']['ContactPeople']) ? array() : $order['DeliveryTransporter']['ContactPeople'];
				$date = strftime('%d.%m.%Y', strtotime($order['StockOrder']['delivery_date']));
				$type = 'delivery';
				$subject = 'Livraison du '.$date;
				$reason = 'delivery_transporter_notify';
				$attachment = $order['DeliveryTransporter']['delivery_attachment'];
			}
			if (!empty($order['ReturnTransporter']) and $order['ReturnTransporter']['notify'] == 1 AND $this->request->data['type'] == 'return') {
				$contactPeoples = empty($order['ReturnTransporter']['ContactPeople']) ? array() : $order['ReturnTransporter']['ContactPeople'];
				$date = strftime('%d.%m.%Y', strtotime($order['StockOrder']['return_date']));
				$type = 'return';
				$subject = 'Reprise du '.$date;
				$reason = 'return_transporter_notify';
				$attachment = $order['ReturnTransporter']['return_attachment'];
			}
			$packaging = $this->StockOrder->getPackaging();

			if (!empty($contactPeoples)) {
				$to = array();
				foreach ($contactPeoples as $cp) {
					$to[] = array($cp['name'] => $cp['email']);
				}

				$params = array(
					'recipient' => 'transporter',
					'reason' => $reason,
					'mailTemplate' => 'stockorder_transporter',
					'pdfTemplate' => 'transporter',
					'pdfFilename' => $order['StockOrder']['id'].'.pdf',
					'emailVars' => array(
						//'to' => $to,
						'to' => 'loris@une-bonne-idee.ch',
						'cc' => 'nicolas@une-bonne-idee.ch',
						'subject' => $subject,
						'stockorder' => $order,
						'toName' => $contactPeoples[0]['full_name'],
						'type' => $type,
						'date' => $date,
						'packaging' => $packaging,
						'weight' => 0,
						'attachment' => $attachment,
					),
				);
				$send = false;
				if ($this->request->data['type'] == 'delivery') {
					if (empty($order['DeliveryTransporterHistory'])) {
						$send = true;
					} else {
						if (strtotime($order['DeliveryTransporterHistory'][0]['date']) < strtotime($order['StockOrder']['modified'])) {
							$send = true;
						}
					}
				}
				if ($this->request->data['type'] == 'return') {
					if (empty($order['ReturnTransporterHistory'])) {
						$send = true;
					} else {
						if (strtotime($order['ReturnTransporterHistory'][0]['date']) < strtotime($order['StockOrder']['modified'])) {
							$send = true;
						}
					}
				}
				if($send){
				  if($this->StockOrder->send($params)){
					$this->response->body(json_encode(array('success' => 1)));
				  } else {
					$this->response->body(json_encode(array('success' => 0)));
				  }
				}

			}

		}
	}

	public function notify_transporters() {
		App::uses('CakePdf', 'CakePdf.Pdf');
		App::uses('CakeEmail', 'Network/Email');

		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$date = $this->request->data['date'];
			$ts = strtotime($date);
			$start = (date('w', $ts) == 1) ? $ts : strtotime('last monday', $ts);
			$start_date = date('Y-m-d', $start);
			$end_date = date('Y-m-d', strtotime('next sunday', $start));

			$deliveries = $this->StockOrder->find('all', array(
				'conditions' => array(
					'delivery_date >=' => $start_date,
					'delivery_date <=' => $end_date,
					'delivery' => 1,
					'StockOrder.status NOT' => array('offer', 'to_invoice', 'invoiced', 'cancelled'),
					'Transporter.notify' => 1,
					'Transporter.id <>' => 1,
				),
				'joins' => array(
					array(
						'table' => 'transporters',
						'alias' => 'Transporter',
						'conditions' => array(
							'Transporter.id = StockOrder.delivery_transporter_id',
						),
					),
				),
				'contain' => array(
					'Client',
					'StockOrderBatch' => array('StockItem'),
					'DeliveryTransporter' => array('ContactPeople'),
					'DeliveryTransporterHistory',
				),
			));

			$returns = $this->StockOrder->find('all', array(
				'conditions' => array(
					'return_date >=' => $start_date,
					'return_date <=' => $end_date,
					'return' => 1,
					'StockOrder.status NOT' => array('offer', 'to_invoice', 'invoiced', 'cancelled'),
					'Transporter.notify' => 1,
					'Transporter.id <>' => 1,
				),
				'joins' => array(
					array(
						'table' => 'transporters',
						'alias' => 'Transporter',
						'conditions' => array(
							'Transporter.id = StockOrder.return_transporter_id',
						),
					),
				),
				'contain' => array(
					'Client',
					'StockOrderBatch' => array('StockItem'),
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'ReturnTransporter' => array('ContactPeople'),
					'ReturnTransporterHistory',
				),
			));
			$counter = 0;
			$orders = array();
			foreach ($deliveries as $order) {
				$order['mode'] = 'delivery';
				$orders[$counter] = $order;
				++$counter;
			}
			foreach ($returns as $order) {
				$order['mode'] = 'return';
				$orders[$counter] = $order;
				++$counter;
			}

			$output = array();
			foreach ($orders as $order) {
				$send = false;
				$type = '';
				$contactPeoples = array();
				if (!empty($order['DeliveryTransporter']) and $order['DeliveryTransporter']['notify'] == 1) {
					$contactPeoples = empty($order['DeliveryTransporter']['ContactPeople']) ? array() : $order['DeliveryTransporter']['ContactPeople'];
					$date = strftime('%d.%m.%Y', strtotime($order['StockOrder']['delivery_date']));
					$type = 'delivery';
					$subject = 'Livraison du '.$date;
					$reason = 'delivery_transporter_notify';
					$attachment = $order['DeliveryTransporter']['delivery_attachment'];
				}
				if (!empty($order['ReturnTransporter']) and $order['ReturnTransporter']['notify'] == 1) {
					$contactPeoples = empty($order['ReturnTransporter']['ContactPeople']) ? array() : $order['ReturnTransporter']['ContactPeople'];
					$date = strftime('%d.%m.%Y', strtotime($order['StockOrder']['return_date']));
					$type = 'return';
					$subject = 'Reprise du '.$date;
					$reason = 'return_transporter_notify';
					$attachment = $order['ReturnTransporter']['return_attachment'];
				}

				$this->StockOrder->id = $order['StockOrder']['id'];
				$packaging = $this->StockOrder->getPackaging();

				if (!empty($contactPeoples)) {
					$to = array();
					foreach ($contactPeoples as $cp) {
						$to[] = array($cp['name'] => $cp['email']);
					}

					$params = array(
						'recipient' => 'transporter',
						'reason' => $reason,
						'mailTemplate' => 'stockorder_transporter',
						'pdfTemplate' => 'transporter',
						'pdfFilename' => $date.'.pdf',
						'emailVars' => array(
							'to' => $to,
							//'to' => 'denis@une-bonne-idee.ch',
							'cc' => 'info@une-bonne-idee.ch',
							'subject' => $subject,
							'stockorder' => $order,
							'toName' => $contactPeoples[0]['full_name'],
							'type' => $type,
							'date' => $date,
							'packaging' => $packaging,
							'weight' => 0,
							'attachment' => $attachment,
						),
					);
					if ($order['mode'] == 'delivery') {
						if (empty($order['DeliveryTransporterHistory'])) {
							$send = true;
						} else {
							if (strtotime($order['DeliveryTransporterHistory'][0]['date']) < strtotime($order['StockOrder']['modified'])) {
								$send = true;
							}
						}
					}
					if ($order['mode'] == 'return') {
						if (empty($order['ReturnTransporterHistory'])) {
							$send = true;
						} else {
							if (strtotime($order['ReturnTransporterHistory'][0]['date']) < strtotime($order['StockOrder']['modified'])) {
								$send = true;
							}
						}
					}

					if ($send) {
						$this->StockOrder->id = $order['StockOrder']['id'];
						$output[] = $this->StockOrder->send($params);
						$send = false;
					}
				}
			}

			//$this->response->body(json_encode(array('success' => 1)));
			$this->response->body(json_encode($orders));
		}
	}

	public function notifyTransporter()
	{
		App::uses('CakePdf', 'CakePdf.Pdf');
		App::uses('CakeEmail', 'Network/Email');

		if ($this->request->is('ajax')) {
			$this->autoRender = false;

			if (!(empty($_POST['stock_order_id']))) {
				$this->StockOrder->id = $_POST['stock_order_id'];
				$this->StockOrder->contain(array('Client', 'ContactPeopleClient'));
				$to = $cc = $toName = $ccName = '';

				switch ($_POST['type']) {
					case 'delivery' :
						$date = strftime('%d.%m.%Y', strtotime($this->StockOrder->field('delivery_date')));
						$subject = 'Livraison du '.$date;
						$type = $_POST['type'];
					break;
					case 'return':
						$date = strftime('%d.%m.%Y', strtotime($this->StockOrder->field('return_date')));
						$subject = 'Reprise du '.$date;
						$type = $_POST['type'];
					break;
					default:
					break;
				}
				switch ($_POST['mode']) {
					case 'logista':
						$to = 'markus.piller@logista.ch';
						$toName = 'Markus Piller';
						$cc = '';
						$ccName = 'Sabri Abdhula';
					break;
					case 'zumwald':

					break;
					default:

					break;
				}

				$params = array(
					'recipient' => 'transporter',
					'reason' => 'transporter_notify',
					'mailTemplate' => 'stockorder_transporter',
					'pdfTemplate' => 'transporter',
					'pdfFilename' => $this->StockOrder->ContactPeopleClient->field('name').' - '.$date.'.pdf',
					'emailVars' => array(
						//'to' => $to,
						'to' => 'denis@une-bonne-idee.ch',
						'cc' => 'info@festiloc.ch',
						'subject' => $subject,
						'stockorder' => $this->StockOrder->find('first', array(
							'conditions' => array('StockOrder.id' => $_POST['stock_order_id']),
							'contain' => array('Client', 'ContactPeopleClient'),
						)),
						'toName' => $toName,
						'type' => $type,
						'date' => $date,
						'numberOfPallets' => $this->StockOrder->field('number_of_pallets'),
						'numberOfRollis' => $this->StockOrder->field('number_of_rollis'),
						'weight' => 0,
					),
				);

				if ($this->StockOrder->send($params) and $_POST['mode'] == 'logista') {
					$output = array('success' => 1);
				} else {
					$output = array('success' => 0);
				}
			}

			$this->response->body(json_encode($output));
		}
	}

	public function conflicts()
	{
		$this->loadModel('StockItem');
		$stockorders = $this->StockOrder->find('all', array(
			'conditions' => array(
				// 'delivery_date >=' => date('Y-m-d'),
				// 'return_date <=' => date('Y-m-d', strtotime(date('Y-m-d') . "+10 days")),
				'status NOT IN' => array('to_invoice', 'cancelled', 'invoiced'),
			),
			'contain' => array('StockOrderBatch'),
		));
		$orders = array();
		$json = array();
		$data = array('orders' => array(), 'dates' => array());
		foreach ($stockorders as $k => $order) {
			foreach ($order['StockOrderBatch'] as $batch) {
				$this->StockOrder->StockOrderBatch->StockItem->id = $batch['stock_item_id'];
				$tmp = $this->StockOrder->StockOrderBatch->StockItem->checkAvailability();
				if (!empty($tmp['conflicts']['dates']) and !empty($tmp['conflicts']['orders'])) {
					foreach ($tmp['conflicts']['orders'] as $orderId => $orders1) {
						foreach ($orders1 as $order1) {
							$data['orders'][$batch['stock_item_id']][$order1['id']][$order1['date']] = $order1;
						}
					}
				} elseif (!empty($tmp['conflicts']['dates']) and empty($tmp['conflicts']['orders'])) {
					$data['dates'][$batch['stock_item_id']] = $tmp['conflicts']['dates'];
					$orders[$batch['stock_item_id']][] = $order;
				}
			}
		}
		$index = 0;
		foreach ($data['orders'] as $stockItemId => $ordersById) {
			$stockitem = $this->StockItem->findById($stockItemId);
			foreach ($ordersById as $orderId => $ordersByDate) {
				$dates = array_keys($ordersByDate);
				$startDate = date('Y-m-d', strtotime($dates[0]));
				$endDate = date('Y-m-d', strtotime(end($dates)));
				if ($ordersByDate[$dates[0]]['needed'] > 0) {
					$json[$index]['title'] = $ordersByDate[$dates[0]]['order_number'].' '.$ordersByDate[$dates[0]]['name']."\n".$stockitem['StockItem']['code'].' '.$stockitem['StockItem']['name']."\n".$ordersByDate[$dates[0]]['needed'].' manquants';
					$json[$index]['start'] = $startDate;
					$json[$index]['end'] = $endDate;
					$json[$index]['backgroundColor'] = '#f2dede';
					$json[$index]['className'] = 'order';
					++$index;
				}
			}
		}
		foreach ($data['dates'] as $stockItemId => $date) {
			$stockitem = $this->StockItem->findById($stockItemId);
			$json[$index]['title'] = $stockitem['StockItem']['code'].' '.$stockitem['StockItem']['name'];
			if (!empty($orders[$stockItemId])) {
				foreach ($orders[$stockItemId] as $o) {
					$json[$index]['title'] .= ' '.$o['StockOrder']['name'];
				}
			}
			$json[$index]['start'] = date('Y-m-d', strtotime($date[0]));
			$json[$index]['end'] = date('Y-m-d', strtotime(end($date)));
			$json[$index]['backgroundColor'] = '#fcf8e3';
			$json[$index]['className'] = 'date';
			++$index;
		}
		sort($json);
		$this->set('_json', $json);
	}

	public function updateCosts()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;

			$this->StockOrder->id = $_POST['stock_order_id'];
			if (!empty($_POST['replacement_costs'])) {
				$this->StockOrder->saveField('replacement_costs', $_POST['replacement_costs'], array('callbacks' => false));
			}
			if (!empty($_POST['extrahours_costs'])) {
				$this->StockOrder->saveField('extrahours_costs', $_POST['extrahours_costs'], array('callbacks' => false));
			}
			if (!empty($_POST['tva'])) {
				$this->StockOrder->saveField('tva', $_POST['tva'], array('callbacks' => false));
			}
			if (!empty($_POST['net_total'])) {
				$this->StockOrder->saveField('net_total', $_POST['net_total'], array('callbacks' => false));
			}
			if (!empty($_POST['forced_net_total'])) {
				$this->StockOrder->saveField('forced_net_total', $_POST['forced_net_total'], array('callbacks' => false));
			}
			if (!empty($_POST['deposit'])) {
				$this->StockOrder->saveField('deposit', $_POST['deposit'], array('callbacks' => false));
			}
			if (!empty($_POST['invoiced_total'])) {
				$this->StockOrder->saveField('invoiced_total', $_POST['invoiced_total'], array('callbacks' => false));
			}

			if (!empty($_POST['stock_order_batch_id'])) {
				$this->StockOrder->StockOrderBatch->id = $_POST['stock_order_batch_id'];
				$this->StockOrder->StockOrderBatch->saveField('invoice_replacement', $_POST['invoice_replacement'], array('callbacks' => false));
			}

			if (!empty($_POST['extrahour_id'])) {
				$this->StockOrder->StockOrderExtraHour->id = $_POST['extrahour_id'];
				$this->StockOrder->StockOrderExtraHour->saveField('invoice', $_POST['invoice_extrahour'], array('callbacks' => false));
			}

			$this->response->body(json_encode(array('success' => 1)));
		}
	}

	public function getContactPeoples()
	{
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->StockOrder->id = $_POST['stock_order_id'];
			$stockOrderClientId = $this->StockOrder->field('client_id');
			if ($stockOrderClientId == $_POST['client_id']) {
				// contact people update
				$stockOrderContactPeopleId = $this->StockOrder->field('contact_people_id');
				$stockOrderDeliveryContactPeopleId = $this->StockOrder->field('delivery_contact_people_id');
				$stockOrderReturnContactPeopleId = $this->StockOrder->field('return_contact_people_id');

				$this->response->body(json_encode(array('update' => 1, 'contacts' => array($stockOrderContactPeopleId, $stockOrderDeliveryContactPeopleId, $stockOrderReturnContactPeopleId))));
			} else {
				// new client selection
				$this->response->body(json_encode(array('update' => 0)));
			}
		}
	}

	/*
public function import1(){
		$this->autoRender = false;
		$data = explode("\n", file_get_contents(WWW_ROOT . 'files/festiloc_clients.txt'));
		foreach($data as $row){
			$this->StockOrder->Client->create();
			$this->StockOrder->Client->ContactPeople->create();

			$row = explode("\t", $row);
			$clientId = trim($row[0]);

			$name = trim($row[61]);
			$address = trim($row[12]);
			$zip = trim($row[94]);
			$city = trim($row[77]);
			$country = ucfirst(strtolower(trim($row[97])));

			$lname = !empty($row[92]) ? trim($row[92]) : '';
			$fname = !empty($row[99]) ? trim($row[99]) : '';
			$email = !empty($row[114]) ? trim($row[114]) : '';
			$phone = !empty($row[116]) ? trim($row[116]) : '';
			$civility = !empty($row[109]) ? trim($row[109]) : '';
			if($civility == 'Madame') $civility = 'ms';
			if($civility == 'Monsieur') $civility = 'mr';
			if($civility == 'Madame et Monsieur') $civility = 'ms_mr';
			if($civility == 'Monsieur et Madame') $civility = 'ms_mr';
			if($civility == 'Docteur') $civility = 'dr';

			$existingClient = $this->StockOrder->Client->findByImportId($clientId);

			$tmp = array(
				'Client' => array(
					'name' => empty($name) ? $fname . ' ' . $lname : $name,
					'address' => $address,
					'zip' => $zip,
					'city' => $city,
					'country' => $country,
					'import_id' => $clientId,
					'id' => empty($existingClient) ? '' : $existingClient['Client']['id']
				),
				'Company' => array('id' => 3)
			);
			if(strlen($tmp['Client']['name']) > 1){
				if($this->StockOrder->Client->save($tmp)){
					$existingContact = $this->StockOrder->Client->ContactPeople->findByClientId($this->StockOrder->Client->id);
					$tmp1 = array('ContactPeople' => array(
						'client_id' => $this->StockOrder->Client->id,
						'id' => empty($existingContact) ? '' : $existingContact['ContactPeople']['id'],
						'name' => (!empty($fname) AND !empty($lname)) ? $fname . ' ' . $lname : $name,
						'email' => $email,
						'phone' => $phone,
						'civility' => $civility,
						'status' => 1
					));
					$this->StockOrder->Client->ContactPeople->save($tmp1);
				}
			}



		}
		exit;
	}

	public function import2(){
		$this->autoRender = false;
		$data = explode("\n", file_get_contents(WWW_ROOT . 'files/confirmations.tsv'));
		unset($data[0]);
		// foreach($data as $row){
		// 	$row = explode("\t", $row);
		// 	if($row[0] == 5112){
		// 		debug($row);
		// 	}
		// }
		// exit;
		foreach($data as $k => $row){
			$this->StockOrder->create();
			$this->StockOrder->Client->create();
			$this->StockOrder->Client->ContactPeople->create();

			$row = explode("\t", $row);
			$clientId = $row[95];
			$client = $this->StockOrder->Client->findByImportId($clientId);
			$contact = $this->StockOrder->Client->ContactPeople->findByClientId($client['Client']['id']);

			$concerns = trim($row[32]);

			$productsCodes = explode("\\", $row[90]);
			$productsQuantities = explode("\\", $row[7]);
			$productsPrices = explode("\\", $row[1]);
			$productsNetPrices = explode("\\", $row[18]);
			$productsCoefficitents = explode("\\", $row[211]);
			$productsDiscounts = explode("\\", $row[10]);

			$tmp = trim($row[156]);

			if($row[154] == false){
				$deliveryTransporter = '';
			} else {
				if(empty($tmp)){
					$deliveryTransporter = 'festiloc';
				} else {
					$deliveryTransporter = trim($row[156]);
				}
			}

			$tmp1 = trim($row[217]);
			if($row[215] == false){
				$returnTransporter = '';
			} else {
				if(empty($tmp1)){
					$returnTransporter = 'festiloc';
				} else {
					$returnTransporter = trim($row[217]);
				}
			}

			$order = array(
				'StockOrder' => array(
					'name' => empty($concerns) ? $client['Client']['name'] : $concerns,
					'client_id' => $client['Client']['id'],
					'contact_people_id' => $contact['ContactPeople']['id'],
					'import_id' => trim($row[0]),
					'status' => 'confirmed',
					'company_id' => 3,
					'transportation_costs' => trim($row[59]),
					'tva' => trim($row[126]),
					'total_ht' => trim($row[16]),
					'net_total' => trim($row[83]),
					'quantity_discount' => abs(trim($row[89])) * 100 / 108,
					'quantity_discount_percentage' => trim($row[88]),
					'invoice_address' => $client['Client']['address'],
					'invoice_zip' => $client['Client']['zip'],
					'invoice_city' => $client['Client']['city'],
					'invoice_phone' => $contact['ContactPeople']['phone'],
					'invoice_email' => $contact['ContactPeople']['email'],
					'service_date_begin' => date('Y-m-d', strtotime(str_replace("/", "-", $row['45']))),
					'service_date_end' => date('Y-m-d', strtotime(str_replace("/", "-", $row['181']))),
					'delivery_date' => date('Y-m-d', strtotime(str_replace("/", "-", $row['153']))),
					'delivery' => $row[154] == true ? 1 : 0,
					'delivery_place' => trim($row[158]),
					'delivery_address' => trim($row[159]),
					'delivery_zip' => trim($row[160]),
					'delivery_city' => trim($row[161]),
					'delivery_moment' => trim($row[155]),
					'delivery_hour' => trim($row[157]),
					'delivery_transporter' => $deliveryTransporter,
					'delivery_remarks' => trim($row[162]),
					'delivery_contact_people_id' => $contact['ContactPeople']['id'],
					'delivery_comments' => implode("\n", explode("|", trim($row[152]))),
					'return_date' => date('Y-m-d', strtotime(str_replace("/", "-", $row['214']))),
					'return' => $row[215] == true ? 1 : 0,
					'return_place' => trim($row[219]) == 'idem' ? trim($row[158]) : trim($row[219]),
					'return_address' => trim($row[219]) == 'idem' ? trim($row[159]) : trim($row[220]),
					'return_zip' => trim($row[219]) == 'idem' ? trim($row[160]) : trim($row[221]),
					'return_city' => trim($row[219]) == 'idem' ? trim($row[161]) : trim($row[222]),
					'return_moment' => trim($row[216]),
					'return_hour' => trim($row[218]),
					'return_transporter' => $returnTransporter,
					'return_remarks' => trim($row[223]),
					'return_contact_people_id' => $contact['ContactPeople']['id'],
					'return_comments' => implode("\n", explode("|", trim($row[213]))),
				)
			);

			$batches = array();
			foreach($productsCodes as $k => $code){
				$stockitem = $this->StockOrder->StockOrderBatch->StockItem->findByCode($code);
				if($productsQuantities[$k] > 0 AND !empty($stockitem)){
					$batches[$k]['stock_item_id'] = $stockitem['StockItem']['id'];
					$batches[$k]['quantity'] = intval($productsQuantities[$k]);
					$batches[$k]['coefficient'] = empty($productsCoefficitents[$k]) ? 1 : $productsCoefficitents[$k];
					$batches[$k]['discount'] = $productsDiscounts[$k];
				}

			}
			$order['StockOrderBatch'] = $batches;
			$this->StockOrder->saveAssociated($order);
		}
		exit;
	}

	public function import3(){
		$this->autoRender = false;
		$data = explode("\n", file_get_contents(WWW_ROOT . 'files/offres.tsv'));
		unset($data[0]);
		// foreach($data as $row){
		// 	$row = explode("\t", $row);
		// 	if($row[0] == 5112){
		// 		debug($row);
		// 	}
		// }
		// exit;
		foreach($data as $k => $row){
			$this->StockOrder->create();
			$this->StockOrder->Client->create();
			$this->StockOrder->Client->ContactPeople->create();

			$row = explode("\t", $row);
			$clientId = $row[95];
			$client = $this->StockOrder->Client->findByImportId($clientId);
			$contact = $this->StockOrder->Client->ContactPeople->findByClientId($client['Client']['id']);

			$concerns = trim($row[32]);

			$productsCodes = explode("\\", $row[90]);
			$productsQuantities = explode("\\", $row[7]);
			$productsPrices = explode("\\", $row[1]);
			$productsNetPrices = explode("\\", $row[18]);
			$productsCoefficitents = explode("\\", $row[211]);
			$productsDiscounts = explode("\\", $row[10]);

			if($row[154] == false){
				$deliveryTransporter = '';
			} else {
				if(empty(trim($row[156]))){
					$deliveryTransporter = 'festiloc';
				} else {
					$deliveryTransporter = trim($row[156]);
				}
			}

			if($row[215] == false){
				$returnTransporter = '';
			} else {
				if(empty(trim($row[217]))){
					$returnTransporter = 'festiloc';
				} else {
					$returnTransporter = trim($row[217]);
				}
			}

			$order = array(
				'StockOrder' => array(
					'name' => empty($concerns) ? $client['Client']['name'] : $concerns,
					'client_id' => $client['Client']['id'],
					'contact_people_id' => $contact['ContactPeople']['id'],
					'import_id' => trim($row[0]),
					'status' => 'offer',
					'company_id' => 3,
					'transportation_costs' => trim($row[59]),
					'tva' => trim($row[126]),
					'total_ht' => trim($row[16]),
					'net_total' => trim($row[83]),
					'quantity_discount' => abs(trim($row[89])),
					'quantity_discount_percentage' => trim($row[88]),
					'invoice_address' => $client['Client']['address'],
					'invoice_zip' => $client['Client']['zip'],
					'invoice_city' => $client['Client']['city'],
					'invoice_phone' => $contact['ContactPeople']['phone'],
					'invoice_email' => $contact['ContactPeople']['email'],
					'service_date_begin' => date('Y-m-d', strtotime(str_replace("/", "-", $row['45']))),
					'service_date_end' => date('Y-m-d', strtotime(str_replace("/", "-", $row['181']))),
					'delivery_date' => date('Y-m-d', strtotime(str_replace("/", "-", $row['153']))),
					'delivery' => $row[154] == true ? 1 : 0,
					'delivery_place' => trim($row[158]),
					'delivery_address' => trim($row[159]),
					'delivery_zip' => trim($row[160]),
					'delivery_city' => trim($row[161]),
					'delivery_moment' => trim($row[155]),
					'delivery_hour' => trim($row[157]),
					'delivery_transporter' => $deliveryTransporter,
					'delivery_remarks' => trim($row[162]),
					'delivery_contact_people_id' => $contact['ContactPeople']['id'],
					'delivery_comments' => implode("\n", explode("|", trim($row[152]))),
					'return_date' => date('Y-m-d', strtotime(str_replace("/", "-", $row['214']))),
					'return' => $row[215] == true ? 1 : 0,
					'return_place' => trim($row[219]) == 'idem' ? trim($row[158]) : trim($row[219]),
					'return_address' => trim($row[219]) == 'idem' ? trim($row[159]) : trim($row[220]),
					'return_zip' => trim($row[219]) == 'idem' ? trim($row[160]) : trim($row[221]),
					'return_city' => trim($row[219]) == 'idem' ? trim($row[161]) : trim($row[222]),
					'return_moment' => trim($row[216]),
					'return_hour' => trim($row[218]),
					'return_transporter' => $returnTransporter,
					'return_remarks' => trim($row[223]),
					'return_contact_people_id' => $contact['ContactPeople']['id'],
					'return_comments' => implode("\n", explode("|", trim($row[213]))),
				)
			);

			$batches = array();
			foreach($productsCodes as $k => $code){
				$stockitem = $this->StockOrder->StockOrderBatch->StockItem->findByCode($code);
				if($productsQuantities[$k] > 0 AND !empty($stockitem)){
					$batches[$k]['stock_item_id'] = $stockitem['StockItem']['id'];
					$batches[$k]['quantity'] = intval($productsQuantities[$k]);
					$batches[$k]['coefficient'] = empty($productsCoefficitents[$k]) ? 1 : $productsCoefficitents[$k];
					$batches[$k]['discount'] = $productsDiscounts[$k];
				}

			}
			$order['StockOrderBatch'] = $batches;
			$this->StockOrder->saveAssociated($order);
		}
		exit;
	}
*/

	public function reset()
	{
		$orders = $this->StockOrder->find('all', array(
			'conditions' => array(
				'status' => array('confirmed'),
			),
			'contain' => array(
				'StockOrderBatch',
			),
		));
		foreach ($orders as $order) {
			$this->StockOrder->save($order);
		}
		exit;
	}

	public function redo_unavailabilities(){
		$this->StockOrder->StockItemUnavailability->deleteAll('1=1');
		$batches = $this->StockOrder->StockOrderBatch->find('all', array(
			'conditions' => array(
				'OR' => array(
					array('SO.delivery_date >=' => '2015-10-01'),
					array('SO.id' => array(969, 1999, 2280, 2365, 2521, 2530)),
				),
				'SO.status NOT' => array('cancelled'),
			),
			'joins' => array(
				array(
					'table' => 'stock_orders',
					'alias' => 'SO',
					'conditions' => array(
						'SO.id = StockOrderBatch.stock_order_id',
					),
				),
			),
			'contain' => array('StockOrder'),
			'fields' => array('StockOrderBatch.quantity', 'StockOrderBatch.delivered_quantity', 'StockOrderBatch.stock_item_id', 'StockOrder.delivery_date', 'StockOrder.return_date', 'StockOrder.status'),
		));
		$ids = array();
		foreach ($batches as $item) {
			if ($item['StockOrder']['status'] == 'offer') {
				$status = 'potential';
			} else {
				$status = 'confirmed';
			}
			$tmp = array(
				'StockItemUnavailability' => array(
					'start_date' => $item['StockOrder']['delivery_date'],
					'end_date' => $item['StockOrder']['return_date'],
					'stock_order_id' => $item['StockOrder']['id'],
					'stock_item_id' => $item['StockOrderBatch']['stock_item_id'],
					'status' => $status,
					'quantity' => is_null($item['StockOrderBatch']['delivered_quantity']) ? $item['StockOrderBatch']['quantity'] : $item['StockOrderBatch']['delivered_quantity'],
				),
			);
			$this->StockOrder->StockItemUnavailability->create();
			$this->StockOrder->StockItemUnavailability->save($tmp);
			$ids[$item['StockOrder']['id']] = $item['StockOrder']['id'];
		}
		foreach ($ids as $id) {
			$this->StockOrder->id = $id;
			$this->StockOrder->addReconditionningUnavailability();
		}
		exit;
	}

	public function updateField(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->StockOrder->id = $this->request->data['id'];
			if($this->StockOrder->saveField($this->request->data['field'], $this->request->data['value'], array('callbacks' => false))){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function saveDepositAndUpdateInvoicedTotal(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->StockOrder->id = $this->request->data['id'];
			$this->StockOrder->saveField('deposit', $this->request->data['value'], array('callbacks' => false));
			if($this->StockOrder->saveTotals(true)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}
}
