<?php

class ReportingController extends AppController {

	public function hours( $mode = '' ){

		$months = array(
			date('Y-m-01', strtotime(date('Y-m')." -3 month")) => array(),
			date('Y-m-01', strtotime(date('Y-m')." -2 month")) => array(),
			date('Y-m-01', strtotime(date('Y-m')." -1 month")) => array(),
			date('Y-m-01', strtotime(date('Y-m'))) => array(),
			date('Y-m-01', strtotime(date('Y-m')." +1 month")) => array(),
			date('Y-m-01', strtotime(date('Y-m')." +2 month")) => array(),
			date('Y-m-01', strtotime(date('Y-m')." +3 month")) => array()
		);

		$this->loadModel('Event');
		if($mode == 'staff'){
			$months = array(
				date('Y-m-01', strtotime(date('Y-m')." -3 month")) => array(),
				date('Y-m-01', strtotime(date('Y-m')." -2 month")) => array(),
				date('Y-m-01', strtotime(date('Y-m')." -1 month")) => array(),
				date('Y-m-01', strtotime(date('Y-m'))) => array(),
				date('Y-m-01', strtotime(date('Y-m')." +1 month")) => array()
			);
			$staff = array();
			foreach ($months as $first_day => $tmp) {
				if($first_day == '2016-05-25'){
					$last_day = '2016-05-31';
				} else {
					$last_day = date('Y-m-t', strtotime($first_day));
				}

				$data = $this->Event->Job->JobUser->find('all', array(
					'contain' => array(
						'User',
						'Job' => array('Event' => array('Company' => array('fields' => array('id', 'class')), 'fields' => array('confirmed_date', 'code_name')))
					),
					'joins' => array(
						array(
							'table' => 'jobs',
							'alias' => 'j',
							'conditions' => array('j.id = JobUser.job_id')
						),
						array(
							'table' => 'events',
							'alias' => 'e',
							'conditions' => array('e.id = j.event_id')
						),
						array(
							'table' => 'users',
							'alias' => 'u',
							'conditions' => array('u.id = JobUser.user_id')
						),
						array(
							'table' => 'companies',
							'alias' => 'c',
							'conditions' => array('c.id = e.company_id')
						)
					),
					'conditions' => array(
						'e.confirmed_date >=' => $first_day,
						'e.confirmed_date <=' => $last_day,
						'JobUser.hours <>' => null,
						'JobUser.validated' => 1,
						'OR' => array(
							array(
								'JobUser.salary <>' => Configure::read('Users.salary_for_fixed'),
								'u.role' => 'fixed',
								'u.trainee' => 0
							),
							array(
								'j.salary <>' => Configure::read('Users.salary_for_fixed'),
								'u.role' => 'fixed',
								'u.trainee' => 0,
							),
							array(
								'JobUser.salary <>' => Configure::read('Users.salary_for_trainee'),
								'u.role' => 'fixed',
								'u.trainee' => 1
							),
							array(
								'u.role' => array('temporary', 'retired')
							)
						)
						//'u.role <>' => 'fixed'
					),
					'order' => 'c.id, User.last_name'
				));
				if(!empty($data)){
					foreach($data as $item){
						$this->Event->Job->JobUser->User->id = $item['User']['id'];
						if($item['Job']['Event']['Company']['id'] == 4 || $item['Job']['Event']['Company']['id'] == 5){
							$key = 'eg';
						} else {
							$key = $item['Job']['Event']['Company']['class'];
						}
						$item['User']['reports'] = $this->Event->Job->JobUser->User->getReporting($item['Job']['Event']['Company']['id'], $first_day, $last_day);
						if(!empty($item['User']['reports']['salary'])) $staff[$first_day][$key][$item['User']['id']] = $item;
					}
				} else {
					$staff[$first_day] = array();
				}
			}
			$this->set(compact('staff'));
			$this->set(compact('months'));
			$this->render('hours_staff');
		}

		if($mode == 'events'){
			$events = array();
			foreach($months as $first_day => $tmp){
				$last_day = date('Y-m-t', strtotime($first_day));
				$data = $this->Event->find('all', array(
					'contain' => array(
						'Client',
						'Company'
					),
					'conditions' => array(
						'confirmed_date >=' => $first_day,
						'confirmed_date <=' => $last_day,
						'OR' => array(
							array(
								array('Event.crm_status' => array('confirmed', 'done')),
								array('Event.type' => 'standard'),
								array('Event.code <>' => null),
							),
							array(
								array('Event.type' => 'mission')
							)
						),
						'Event.company_id <>' => null
					),
					'order' => array('Company.id', 'Event.code')
				));
				if(!empty($data)){
					foreach($data as $event){
						$this->Event->id = $event['Event']['id'];
						$costs = $this->Event->getManpowerCosts();
						if(!empty($costs['salary'])){
							$event['Event']['costs'] = $costs;
						} else {
							continue;
						}
						$events[$first_day][$event['Company']['class']][] = $event;
					}
				}
			}
			$this->set(compact('events'));
			$this->set(compact('months'));
			$this->render('hours_events');
		}
	}

	public function sales( $year = '' ){

		if(!in_array(AuthComponent::user('id'), array(1,14,24,190,326,18,204,112,208))){
			return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
		}

		if(empty($year)){
			return $this->redirect(array('action' => 'sales', date('Y')));
		}

		// 4 companies : ubic, effet gourmand, festiloc, urban gaming

		// UBIC, EG, UG
		$events = array();
		$this->loadModel('Event');
		$sales['ubic'] = $this->Event->sales(2, $year); // 2 is company id for ubic
		$sales['ug'] = $this->Event->sales(6, $year); // 6 is company id for ug
		$sales['eg'] = $this->Event->sales(4, $year); // 4 is company id for eg
		$sales['mg'] = $this->Event->sales(5, $year); // 5 is company id for mg

		// Festiloc
		$this->loadModel('StockOrder');
		$sales['festiloc'] = $this->StockOrder->sales(3, $year); // 3 is company id for festiloc

		$this->set(compact('sales'));
		$this->set(compact('year'));

		$this->set('companies_ids', array('ubic' => 2, 'ug' => 6, 'eg' => 4, 'mg' => 5, 'festiloc' => '3'));

		$this->set('acronyms', Configure::read('Companies.acronyms'));
	}

	public function accounting(){
		$events = array();
		$this->loadModel('Event');
		$data = $this->Event->find('all', array(
			'contain' => array(
				'Client'
			),
			'conditions' => array(
				'crm_status' => 'confirmed',
				'confirmed_date >=' => date('Y-01-01'),
				'confirmed_date <=' => date('Y-12-31'),
				'confirmed_date <>' => null,
			),
			'order' => 'confirmed_date'
		));
		foreach($data as $event){
			$month = date('Y-m-01', strtotime($event['Event']['confirmed_date']));
			$events[$month][] = $event;
		}
		$this->set(compact('events'));
	}

	public function modal($model = '', $model_id = '', $date = '', $status = '', $company = ''){
		$printUrl = Router::url(array('controller' => 'reporting', 'action' => 'modal', $model, $model_id, $date, $status, $company), true);
		$this->set(compact('printUrl'));
		if($model == 'user'){
			$this->loadModel('User');
			$data = $this->User->JobUser->find('all', array(
				'contain' => array(
					'User',
					'Job' => array('Event' => array('Company', 'fields' => array('confirmed_date', 'code_name', 'name', 'id')))
				),
				'joins' => array(
					array(
						'table' => 'jobs',
						'alias' => 'j',
						'conditions' => array('j.id = JobUser.job_id')
					),
					array(
						'table' => 'users',
						'alias' => 'u',
						'conditions' => array('u.id = JobUser.user_id')
					),
					array(
						'table' => 'events',
						'alias' => 'e',
						'conditions' => array('e.id = j.event_id')
					),
					array(
						'table' => 'companies',
						'alias' => 'c',
						'conditions' => array('c.id = e.company_id')
					)
				),
				'conditions' => array(
					'e.confirmed_date >=' => $date,
					'e.confirmed_date <=' => date('Y-m-t', strtotime($date)),
          'e.type' => array('standard', 'mission'),
					'JobUser.hours <>' => null,
					'JobUser.validated' => 1,
					'JobUser.user_id' => $model_id,
					'OR' => array(
						array(
							'JobUser.salary <>' => Configure::read('Users.salary_for_fixed'),
							'u.role' => 'fixed',
							'u.trainee' => 0
						),
						array(
							'j.salary <>' => Configure::read('Users.salary_for_fixed'),
							'u.role' => 'fixed',
							'u.trainee' => 0,
						),
						array(
							'JobUser.salary <>' => Configure::read('Users.salary_for_trainee'),
							'u.role' => 'fixed',
							'u.trainee' => 1
						),
						array(
							'u.role' => array('temporary', 'retired')
						)
					)
				),
				'order' => array('c.id', 'e.confirmed_date')
			));
			$this->set('jobs', $data);
			$this->set('user', $this->User->findById($model_id));
			$this->set(compact('date'));
			$wealthingsText = array();
			foreach($data as $job){
				$string = sprintf(
					'%s,%s,%s,%s,%s;',
					date('d.m.Y', strtotime($job['Job']['Event']['confirmed_date'])),
					!empty($job['Job']['Event']['code_name']) ? $job['Job']['Event']['code_name'] : $job['Job']['Event']['name'],
					sprintf('%s - %s', date('H:i', strtotime(!empty($job['JobUser']['effective_start_hour']) ? $job['JobUser']['effective_start_hour'] : $job['Job']['start_time'])), date('H:i', strtotime(!empty($job['JobUser']['effective_end_hour']) ? $job['JobUser']['effective_end_hour'] : $job['Job']['end_time']))),
					sprintf('%s CHF / h', empty($job['JobUser']['salary']) ? $job['Job']['salary'] : $job['JobUser']['salary']),
					sprintf('%s CHF', $job['JobUser']['total'])
				);
				$wealthingsText[$job['Job']['Event']['Company']['name']][] = trim($string);
			}
			$this->set(compact('wealthingsText'));
		}
		if($model == 'offers' || $model == 'realisations' || $model == 'repartition'){
			$this->loadModel('Event');
			$this->loadModel('StockOrder');
			$company1 = $this->Event->Company->findByClass($company);
			$conditions = array();
			$joins = array();
			if($company1['Company']['id'] == 3){
				$conditions[] = array(
					'StockOrder.return_date >=' => date('Y-m-01', strtotime($date)),
					'StockOrder.return_date <=' => date('Y-m-t', strtotime($date))
				);
			} else {
				if($model == 'offers'){
					$conditions[] = array(
						'Event.opening_date >=' => date('Y-m-01', strtotime($date)),
						'Event.opening_date <=' => date('Y-m-t', strtotime($date)),
            'Event.type' => 'standard'
					);
				} elseif($model == 'realisations' || $model == 'repartition'){
					$conditions[] = array(
						'Event.confirmed_date >=' => date('Y-m-01', strtotime($date)),
						'Event.confirmed_date <=' => date('Y-m-t', strtotime($date)),
            'Event.type' => 'standard'
					);
				}
			}

			if($company1['Company']['id'] != 3){
				$conditions[] = array('Event.company_id' => $company1['Company']['id']);
			}

			$title = 'Offers during month of %s - %s';
			switch($status){
				case 'all':
				break;
				case 'in_progress':
				$title = 'Offers in progress during month of %s - %s';
				if($company1['Company']['id'] == 3){
					$conditions[] = array(
						'StockOrder.status' => 'offer'
					);
				} else {
					$conditions[] = array(
						'Event.crm_status <>' => array('refused', 'null', 'confirmed', 'done', 'partner')
					);
				}
				break;
				case 'sent':
				$title = 'Sent offers during month of %s - %s';
				if($company1['Company']['id'] == 3){
					$joins = array(
						array(
							'table' => 'histories',
							'alias' => 'h',
							'conditions' => array(
								'StockOrder.id = h.parent_id'
							)
						)
					);
					$conditions[] = array(
						'h.reason' => 'offer_sent'
					);
				} else {
					$conditions[] = array(
						'Event.crm_status <>' => 'null'
					);
				}
				break;
				case 'accepted':
				$title = 'Accepted offers during month of %s - %s';
				if($company1['Company']['id'] == 3){
					$conditions[] = array(
						'StockOrder.status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress')
					);
				} else {
					$conditions[] = array(
						'Event.crm_status' => array('confirmed', 'done')
					);
				}
				break;
				case 'refused':
				$title = 'Refused offers during month of %s - %s';
				if($company1['Company']['id'] == 3){
					$conditions[] = array(
						'StockOrder.status' => 'cancelled'
					);
				} else {
					$conditions[] = array(
						'Event.crm_status' => $status
					);
				}
				break;
				case 'null':
				$title = 'Null offers during month of %s - %s';
				$conditions[] = array(
					'Event.crm_status' => $status
				);
				break;
				default:
				break;
			}
			if($company1['Company']['id'] == 3){
				$orders = $this->StockOrder->find('all', array(
					'contain' => array('Client'),
					'joins' => $joins,
					'conditions' => $conditions,
					'order' => 'StockOrder.return_date',
					'group' => 'StockOrder.id'
				));
				$events = array();
			} else {
				$events = $this->Event->find('all', array(
					'contain' => array('Client', 'Date', 'Company'),
					'conditions' => $conditions,
					'order' => 'Event.opening_date'
				));
				$orders = array();
			}

      if(!empty($orders)){
        foreach($orders as $k => $order){
          if(!empty(floatval($order['StockOrder']['total_ht']))){ continue; }
          $this->StockOrder->id = $order['StockOrder']['id'];
          $this->StockOrder->fixTotalHt();
          $this->StockOrder->saveTotals();
          $orders[$k] = $this->StockOrder->find('first', array(
            'contain' => array('Client'),
            'conditions' => array(
              'StockOrder.id' => $order['StockOrder']['id']
            )
          ));
        }
      }

			if($model == 'repartition'){
				$title = "Repartition of month %s - %s";
			}

			$this->set(compact('orders'));
			$this->set(compact('events'));
			$this->set(compact('title'));
			$this->set(compact('date'));
			$this->set(compact('model'));
			$this->set(compact('company'));
			$this->set(compact('company1'));
			$this->set('statuses', Configure::read('Events.crm_status'));
			$this->set('statuses_fl', Configure::read('StockOrders.status'));
		}
	}

}
