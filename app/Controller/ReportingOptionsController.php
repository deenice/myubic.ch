<?php

class ReportingOptionsController extends AppController {

	public function update(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$option = $this->ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $this->request->data['company_id'],
					'month' => date('n', strtotime($this->request->data['month'])),
					'year' => $this->request->data['year'],
					'type' => $this->request->data['type']
				)
			));
			if(!empty($option)){
				$option['ReportingOption']['value'] = $this->request->data['value'];
			} else {
				$option = array(
					'ReportingOption' => array(
						'company_id' => $this->request->data['company_id'],
						'value' => $this->request->data['value'],
						'year' => $this->request->data['year'],
						'month' => date('n', strtotime($this->request->data['month'])),
						'type' => $this->request->data['type']
					)
				);
			}
			$this->ReportingOption->create();
			if($this->ReportingOption->save($option)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}

	}

}
