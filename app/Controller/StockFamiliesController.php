<?php

class StockFamiliesController extends AppController {


	public function add( $companyId = null ){

		$companies = $this->StockFamily->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if(!empty($companyId)){
			$company = $this->StockFamily->Company->findById($companyId);
			$this->set(compact('company'));
		}

		if($this->request->is('put') OR $this->request->is('post')){
			// generate code for stock family
			$family = $this->StockFamily->find('first', array(
				'conditions' => array(
					'StockFamily.stock_section_id' => $this->request->data['StockFamily']['stock_section_id']
				),
				'order' => 'StockFamily.code DESC'
			));
			if(empty($family)){
				$this->request->data['StockFamily']['code'] = 1;
			} else {
				$this->request->data['StockFamily']['code'] = $family['StockFamily']['code'] + 1;
			}
			if($this->StockFamily->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockFamily']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
	        return $this->redirect(array('action' => 'edit', $this->StockFamily->id));
				} elseif($this->request->data['StockFamily']['company_id'] == 3) { // festiloc
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockFamily']['company_id']));
				}
			}
		}
	}

	public function edit( $id = null ){

		$family = $this->StockFamily->findById($id);

		$categories = $this->StockFamily->StockCategory->find('list', array(
			'conditions' => array(
				'StockCategory.company_id' => $family['StockFamily']['company_id']
			),
			'fields' => array(
				'StockCategory.id', 'StockCategory.code_name'
			)
		));
		$sections = $this->StockFamily->StockSection->find('list', array(
			'conditions' => array(
				'StockSection.company_id' => $family['StockFamily']['company_id'],
				'StockSection.stock_category_id' => $family['StockFamily']['stock_category_id']
			),
			'fields' => array(
				'StockSection.id', 'StockSection.code_name'
			)
		));

		$this->set(compact('categories'));
		$this->set(compact('sections'));

		$companies = $this->StockFamily->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if($this->request->is('put') OR $this->request->is('post')){
			if($this->StockFamily->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockFamily']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->StockFamily->id));
				} elseif($this->request->data['StockFamily']['company_id'] == 3) { // festiloc
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockFamily']['company_id']));
				}
			}
		} else {
			$this->request->data = $family;
			$this->set(compact('family'));
		}
	}

	public function json(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$families = $this->StockFamily->find('all', array(
				'conditions' => array(
					'StockFamily.stock_section_id' => $this->request->data['stock_section_id']
				),
				'fields' => array('StockFamily.id', 'StockFamily.code_name'),
				'order' => array('StockFamily.code ASC')
			));
			$this->response->body(json_encode($families));
		}
	}


}
