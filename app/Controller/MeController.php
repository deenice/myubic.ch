<?php

App::uses('CakeEmail', 'Network/Email');

class MeController extends AppController {

	public function profile(){
		$this->loadModel('User');
		$id = AuthComponent::user('id');
		$this->User->id = $id;
		$this->User->contain(
			array(
				'Portrait',
				'Section',
				'Tag',
				'MondayAvailability',
				'TuesdayAvailability',
				'WednesdayAvailability',
				'ThursdayAvailability',
				'FridayAvailability',
				'SaturdayAvailability',
				'SundayAvailability'
			)
		);
		$sections = $this->User->Tag->find('list', array(
			'joins' => array(
				array(
					'table' => 'tags_users',
					'alias' => 'TagsUsers',
					'conditions' => array(
						'TagsUsers.tag_id = Tag.id'
					)
				)
			),
			'conditions' => array(
				'Tag.category' => 'section',
				'TagsUsers.user_id' => $id
			)
		));
		$talents = $this->User->Tag->find('list', array(
			'joins' => array(
				array(
					'table' => 'tags_users',
					'alias' => 'TagsUsers',
					'conditions' => array(
						'TagsUsers.tag_id = Tag.id'
					)
				)
			),
			'conditions' => array(
				'Tag.category' => 'talent',
				'TagsUsers.user_id' => $id
			)
		));

		$user = $this->User->read(null, $id);
		if(!empty($user['User']['date_of_birth'])){
    		$user['User']['date_of_birth'] = date('d-m-Y', strtotime($user['User']['date_of_birth']));
    	}
		$this->set(compact('user'));
		$this->set(compact('sections'));
		$this->set(compact('talents'));
		$this->request->data = $user;
	}

	public function dashboard(){
		$this->loadModel('User');
		$this->loadModel('Job');
		$this->loadModel('RoundAnswer');
		$id = AuthComponent::user('id');
		$this->User->id = $id;
		$this->User->contain(
			array(
				'Portrait',
				'Section'
			)
		);

		$this->set('user', $this->User->read(null, $id));

		$myEvents = $this->Job->JobUser->find('all', array(
			'joins' => array(
				array(
					'table' => 'jobs',
					'alias' => 'j',
					'conditions' => array(
						'j.id = JobUser.job_id'
					)
				),
				array(
					'table' => 'events',
					'alias' => 'e',
					'conditions' => array(
						'e.id = j.event_id'
					)
				)
			),
			'contain' => array(
				'Job' => array('Event' => array('Client'))
			),
			'conditions' => array(
				'JobUser.user_id' => $id,
				'e.confirmed_date >=' => date('Y-m-d')
			),
			'order' => 'e.confirmed_date'
		));
		$this->set(compact('myEvents'));

		$answers = $this->RoundAnswer->find('all', array(
			'contain' => array(
				'RoundStep' => array(
					'Round' => array(
						'Job' => array(
							'Event' => array(
								'Client'
							)
						)
					)
				)
			),
			'conditions' => array(
				'user_id' => $id,
				'answer' => 'waiting'
			)
		));
		$this->set(compact('answers'));
	}


    public function frontendAnimation( $id = null, $token = null){
		$this->loadModel('User');
        $this->User->id = $id;
        if($this->User->saveField('frontend_animation', 1)){
            echo sprintf("Merci %s! Ton profil a &eacute;t&eacute; mis &agrave; jour correctement.", $this->User->field('first_name'));
            exit;
        } else {
            echo sprintf("Une erreur s'est produite... Merci de contacter Floriane par mail pour l'avertir.");
            exit;
        }
    }


    public function frontendAnimationMail(){
		$this->loadModel('User');
        $extras = $this->User->find('all', array(
            'conditions' => array(
                'role' => 'temporary',
                'email <>' => ''
            ),
            'fields' => array('first_name', 'email', 'id')
        ));

        foreach($extras as $k => $user){
            $url = Router::url(array('controller' => 'me', 'action' => 'frontendAnimation', $user['User']['id'], 'full_base' => true));
            $mail = new CakeEmail();
            $mail->from(array('floriane@une-bonne-idee.ch' => 'Floriane'))
                 ->to($user['User']['email'])
                 ->subject('Animation en frontal')
                 ->emailFormat('html')
                 ->template('frontend_animation')
                 ->viewVars(array(
                    'userFirstName' => $user['User']['first_name'],
                    'url' => $url
                    ));
            if($mail->send()){
                echo sprintf("<li>Mail has been sent to %s</li>", $user['User']['email']);
            }
        }
        exit;
    }

}
