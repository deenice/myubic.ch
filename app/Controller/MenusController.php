<?php

class MenusController extends AppController {

	public function update(){
		$this->Menu->id = 167;
		//$this->Menu->save(array('parent_id' => 160));
		$this->Menu->moveUp(167,1);
		exit;
	}
	public function delete1($id){
		$this->Menu->delete($id);
		exit;
	}

	public function add(){

		$menu = $this->Menu->generateTreeList();
		$this->set(compact('menu'));

		if($this->request->is('post') || $this->request->is('put')){
			if(!empty($this->request->data['Menu']['controller'])){
				$url = array('controller' => $this->request->data['Menu']['controller'], 'action' => $this->request->data['Menu']['action']);
				if(!empty($this->request->data['Menu']['params'])){
					$params = explode('/', $this->request->data['Menu']['params']);
					foreach($params as $param){
						array_push($url, $param);
					}
				}
			} else {
				$url = 'javascript:;';
			}
			$this->request->data['Menu']['url'] = serialize($url);

			if($this->Menu->save($this->request->data)){
				$this->Session->setFlash(__('Menu item has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Menu->id));
				} else {
					return $this->redirect(array('action' => 'add'));
				}
			} else {
				$this->Session->setFlash(__('Menu item has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'add'));
			}
		}

	}

	public function edit( $id = null ){

		$menu = $this->Menu->generateTreeList();
		$this->set(compact('menu'));

		if($this->request->is('post') || $this->request->is('put')){
			if(!empty($this->request->data['Menu']['controller'])){
				$url = array('controller' => $this->request->data['Menu']['controller'], 'action' => $this->request->data['Menu']['action']);
				if(!empty($this->request->data['Menu']['params'])){
					$params = explode('/', $this->request->data['Menu']['params']);
					foreach($params as $param){
						array_push($url, $param);
					}
				}
			} else {
				$url = 'javascript:;';
			}
			$this->request->data['Menu']['url'] = serialize($url);

			if($this->Menu->save($this->request->data)){
				$this->Session->setFlash(__('Menu item has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Menu->id));
				} else {
					return $this->redirect(array('action' => 'add'));
				}
			} else {
				$this->Session->setFlash(__('Menu item has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			$item = $this->Menu->findById($id);
			$this->request->data = $item;
			$url = unserialize($item['Menu']['url']);
			if(!empty($url['controller'])){
				$this->request->data['Menu']['controller'] = $url['controller'];
				$this->request->data['Menu']['action'] = $url['action'];
				$this->request->data['Menu']['params'] = array();
				foreach($url as $key => $value){
					if(is_numeric($key)){
						array_push($this->request->data['Menu']['params'], $value);
					}
				}
			}
			$this->set(compact('item'));
		}

	}

	public function delete( $id = null ){
		$this->Menu->id = $id;
		if($this->Menu->delete()){
			$this->Session->setFlash(__('Menu item has been successfully removed.'), 'alert', array('type' => 'success'));
		} else {
			$this->Session->setFlash(__('Menu item has not been removed. Please try again.'), 'alert', array('type' => 'danger'));
		}
		return $this->redirect(array('action' => 'add'));
	}

}
