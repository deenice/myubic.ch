<?php

class OrdersController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'Order' => array(
				'columns' => array(
					'name',
					'company' => array(
						'useField' => false
					),
					'Actions' => null,
				),
				'contain' => array(
					'Company'
				),
				'fields' => array(
					'Company.name',
					'Order.id',
          'Order.remarks'
				),
				'conditions' => array(
					'model_id' => null
				),
				'autoData' => false
			)
		),
	);

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Order']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Order']['columns']['company']['label'] = __('Company');
	}

	public function index() {
		$this->DataTable->setViewVar(array('Order'));
	}

	public function add(){

		$companies = $this->Order->Company->find('list', array());
		$this->set(compact('companies'));

		$categories = $this->Order->FBCategory->find('list', array(
			'order' => 'weight'
		));
		$this->set(compact('categories'));

		if($this->request->is('post') || $this->request->is('put')){
			$this->Order->create();
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('Order model has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Order->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Order model has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}

	}

	public function edit( $id = null){

		$companies = $this->Order->Company->find('list', array());
		$this->set(compact('companies'));

		$categories = $this->Order->FBCategory->find('list', array(
			'order' => 'weight'
		));
		$this->set(compact('categories'));

		$this->Order->id = $id;

		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid Order model'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Order->create();
			if ($this->Order->save($this->request->data)) {
				$this->Session->setFlash(__('Order model has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Order->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Order model has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->Order->contain(array('OrderGroup' => array('OrderItem')));
			$order = $this->Order->findById($id);
			$this->set(compact('order'));
			$this->request->data = $order;
			$firstGroup = array();
			if(!empty($order['OrderGroup'])){
				$firstGroup = array($order['OrderGroup'][0]['id']);
			}
			$this->set(compact('firstGroup'));
		}

	}

	public function view($id = null){
		$this->Order->id = $id;

		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid Order model'));
		}
		$this->Order->contain(array(
			'OrderGroup' => array('OrderItem' => array('StockItem', 'Supplier'))
		));
		$order = $this->Order->findById($id);
		$this->set(compact('order'));
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->Order->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		} else {
			if($this->Order->delete($id)){
				$this->Session->setFlash(__('Order has been deleted.'), 'alert', array('type' => 'success'));
			} else {
				$this->Session->setFlash(__('Order has not been deleted.'), 'alert', array('type' => 'warning'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function associations_old(){

		$orders = $this->Order->find('all', array(
			'contain' => array('Company'),
			'conditions' => array(
				'model_id' => null
			)
		));
		$this->set(compact('orders'));

		$items = array();
		$associations = array();

		$activities = $this->Order->OrderAssociation->Activity->find('all', array(
			'contain' => array('Company'),
			'conditions' => array(
				'company_id <>' => null
			),
			'fields' => array('id', 'Activity.name', 'Company.name')
		));
		foreach($activities as $activity){
			$activity['Activity']['model'] = 'activity';
			$activity['Activity']['orders'] = $this->Order->OrderAssociation->find('list', array(
				'conditions' => array(
					'model_id' => $activity['Activity']['id'],
					'model' => 'activity'
				),
				'fields' => array('id', 'order_id')
			));
			$items[$activity['Company']['name']][] = $activity['Activity'];
		}

		$fb_modules = $this->Order->OrderAssociation->FBModule->find('all', array(
			'fields' => array('id', 'FBModule.name')
		));
		$items['Effet Gourmand'] = array();
		foreach($fb_modules as $module){
			$module['FBModule']['model'] = 'fb_module';
			$module['FBModule']['orders'] = $this->Order->OrderAssociation->find('list', array(
				'conditions' => array(
					'model_id' => $module['FBModule']['id'],
					'model' => 'fb_module'
				),
				'fields' => array('id', 'order_id')
			));
			$items['Effet Gourmand'][] = $module['FBModule'];
		}

		$this->set(compact('items'));

	}

	public function associations( $company_id = null ){

		$options = array(
			'contain' => array(
				'Company',
				'Activity' => array('fields' => array('id', 'name', 'slug')),
				'FBModule' => array('fields' => array('id', 'name', 'slug'))
			),
			'conditions' => array(
				'model_id' => null,
				'Order.name <>' => ''
			),
			'order' => array('company_id', 'Order.name')
		);

		if(!empty($company_id)){
			$options['conditions'][] = array('Order.company_id' => $company_id);
		}

		$orders = $this->Order->find('all', $options);
		$this->set(compact('orders'));

		$companies = $this->Order->Company->find('all', array(
			'joins' => array(
				array(
					'table' => 'orders',
					'alias' => 'o',
					'conditions' => array('o.company_id = Company.id')
				)
			),
			'group' => 'Company.id'
		));
		$this->set(compact('companies'));

		$resources = array();
		$this->loadModel('Activity');
		$resources['Activity'] = $this->Activity->find('all', array(
			'conditions' => array(
				'Activity.category' => null,
				'Activity.name <>' => ''
			),
			'order' => 'Activity.name'
		));
		$this->loadModel('FBModule');
		$resources['FBModule'] = $this->FBModule->find('all', array(
			'conditions' => array(
				'FBModule.name <>' => ''
			),
			'order' => 'FBModule.name'
		));
		$this->set(compact('resources'));

	}

	public function html( $id = null ){
		$this->Order->id = $id;

		if (!$this->Order->exists()) {
			throw new NotFoundException(__('Invalid Order'));
		}

		$locations = $this->Order->OrderGroup->OrderItem->StockItem->StockLocation->find('list');
		$this->set(compact('locations'));

		$suppliers = $this->Order->OrderGroup->OrderItem->Supplier->find('list');
		$this->set(compact('suppliers'));

		if($this->request->is('ajax')){
			$modelId = $this->Order->field('model_id');
			$model = $this->Order->field('model');
			if($model == 'event' && !empty($modelId)){
				$this->set('event', $this->Order->Event->findById($modelId));
			}
			$this->Order->contain(array(
				'OrderGroup' => array(
					'EditableOrderItem' => array(
						'StockItem' => array('fields' => array('code_name', 'id', 'quantity'))
					),
					'UnEditableOrderItem' => array(
						'StockItem' => array('fields' => array('code_name', 'id', 'quantity'))
					)
				)
			));
			$order = $this->Order->findById($id);
			$this->set(compact('order'));
			$openGroups = array();
			if(!empty($this->request->data['open_groups'])){
				$openGroups = explode(',', $this->request->data['open_groups']);
			}
			$this->set(compact('openGroups'));
		}
	}

	public function select( $id = '', $model_id = '' ){

		if($this->request->is('ajax')){
			$this->autoRender = false;
		}

		if(!empty($id)){
			$this->Order->id = $id;
			if($this->Order->assign(array('order_model' => 'event', 'order_model_id' => $model_id))){
				if($this->request->is('ajax')){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					return true;
				}
			} else {
				if($this->request->is('ajax')){
					$this->response->body(json_encode(array('success' => 0)));
				} else {
					return false;
				}
			}
		} else {
			if($this->request->is('ajax')){
				$this->response->body(json_encode(array('success' => 0)));
			} else {
				return false;
			}
		}
	}

}
