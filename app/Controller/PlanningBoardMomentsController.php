<?php

class PlanningBoardMomentsController extends AppController {

  public function add( $board_id = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->set(compact('board_id'));
      $highestWeight = $this->PlanningBoardMoment->find('first', array(
        'conditions' => array(
          'planning_board_id' => $board_id
        ),
        'fields' => array(
          'weight'
        ),
        'order' => 'weight DESC'
      ));
      if(empty($highestWeight)){
        $weight = 0;
      } else {
        $weight = $highestWeight['PlanningBoardMoment']['weight'] + 1;
      }
      $moment = array(
        'PlanningBoardMoment' => array(
          'planning_board_id' => $board_id,
          'weight' => $weight
        )
      );
      $this->set(compact('moment'));
      $board = $this->PlanningBoardMoment->PlanningBoard->findById($board_id);
      $this->set(compact('board'));
      $this->render('modal');
    }
  }

  public function edit( $id = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->PlanningBoardMoment->save($this->request->data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function delete( $id = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->PlanningBoardMoment->delete($id)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function modal($id = ''){
    $moment = $this->PlanningBoardMoment->findById($id);
    $this->set(compact('moment'));
  }

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['moments'])){
				foreach($this->request->data['moments'] as $weight => $moment){
					$this->PlanningBoardMoment->id = $moment;
					$this->PlanningBoardMoment->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
