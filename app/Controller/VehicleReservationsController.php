<?php

class VehicleReservationsController extends AppController {


  public function add(){
    $companies = $this->VehicleReservation->Vehicle->VehicleCompany->find('list');
    $this->set(compact('companies'));

    $drivers = $this->VehicleReservation->Driver->find('list', array(
      'conditions' => array(
        'role' => array('fixed', 'temporary')
      ),
      'fields' => array('Driver.id', 'Driver.full_name'),
      'order' => 'Driver.first_name'
    ));
    $this->set(compact('drivers'));

    if($this->request->is('put') || $this->request->is('post')){
      if(!empty($this->request->data['VehicleReservation']['start'])){
        $this->request->data['VehicleReservation']['start'] = date('Y-m-d H:i:s', strtotime(str_replace(' - ', '', $this->request->data['VehicleReservation']['start'])));
      }
      if(!empty($this->request->data['VehicleReservation']['end'])){
        $this->request->data['VehicleReservation']['end'] = date('Y-m-d H:i:s', strtotime(str_replace(' - ', '', $this->request->data['VehicleReservation']['end'])));
      }
      if(!empty($this->request->data['VehicleReservation']['vehicle_company_model_id'])){
        $this->request->data['VehicleReservation']['vehicle_model_id'] = $this->request->data['VehicleReservation']['vehicle_company_model_id'];
      }
      if($this->VehicleReservation->save($this->request->data)){
        $this->Session->setFlash(__('Unavailability has been saved.'), 'alert', array('type' => 'success'));
          if(!empty($this->request->data['destination'])){
            if($this->request->data['destination'] == 'edit'){
              return $this->redirect(array('action' => 'edit', $this->VehicleReservation->id));
            } else {
              return $this->redirect(array('action' => 'reservations', 'controller' => 'vehicles'));
            }
          } else {
            return $this->redirect(array('action' => 'reservations', 'controller' => 'vehicles'));
          }
      }
    }
  }

  public function save(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      //$this->response->body(json_encode($this->request->data));
      if(!empty($this->request->data['vehicle_id'])){
        $vehicle = $this->VehicleReservation->Vehicle->findById($this->request->data['vehicle_id']);
      }
      if(empty($this->request->data['vehicle_model_id']) && !empty($vehicle)){
        $this->request->data['vehicle_model_id'] = $vehicle['Vehicle']['vehicle_company_model_id'];
      }
      $data = array(
        'VehicleReservation' => array(
          'id' => empty($this->request->data['reservationId']) ? '' : $this->request->data['reservationId'],
          'vehicle_model_id' => empty($this->request->data['vehicle_model_id']) ? '' : $this->request->data['vehicle_model_id'],
          'family' => empty($this->request->data['family']) ? '' : $this->request->data['family'],
          'vehicle_id' => empty($this->request->data['vehicle_id']) ? '' : $this->request->data['vehicle_id'],
          'start' => date('Y-m-d H:i:s', strtotime(str_replace(' - ', '', $this->request->data['start']))),
          'end' => date('Y-m-d H:i:s', strtotime(str_replace(' - ', '', $this->request->data['end']))),
          'start_place' => empty($this->request->data['start_place']) ? '' : $this->request->data['start_place'],
          'end_place' => empty($this->request->data['end_place']) ? '' : $this->request->data['end_place'],
          'confirmed' => empty($this->request->data['confirmed']) ? '' : $this->request->data['confirmed'],
          'archived' => empty($this->request->data['archived']) ? '' : $this->request->data['archived']
        )
      );
      if(!empty($this->request->data['tour'])){
        $data['VehicleTour']['id'] = $this->request->data['tour'];
      }
      if(!empty($this->request->data['event'])){
        $data['Event']['id'] = $this->request->data['event'];
      }
      if($this->VehicleReservation->save($data)){
        $this->response->body(json_encode(array('success' => 1, 'id' => $this->VehicleReservation->id)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function delete( $id = null ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->VehicleReservation->delete($this->request->data['reservationId'])){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function cancel( $id = null ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->VehicleReservation->id = $this->request->data['reservationId'];
      $vehicleId = $this->VehicleReservation->field('vehicle_id');
      if($this->VehicleReservation->saveField('cancelled', 1)){
        $this->loadModel('PlanningResource');
        $this->PlanningResource->remove($this->request->data['eventId'], 'vehicle', $vehicleId);
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }


  public function update(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $reservation = $this->VehicleReservation->find('first', array(
        'conditions' => array(
          'model_id' => $this->request->data['model_id'],
          'model' => $this->request->data['model'],
          'tour_id' => $this->request->data['old_tour_id']
        )
      ));
      if(!empty($reservation)){

        $reservation1 = $this->VehicleReservation->find('first', array(
          'conditions' => array(
            'model' => $this->request->data['model'],
            'tour_id' => $this->request->data['new_tour_id'],
            'start LIKE' => '%' . date('Y-m-d', strtotime($reservation['VehicleReservation']['start'])) . '%',
            'end LIKE' => '%' . date('Y-m-d', strtotime($reservation['VehicleReservation']['end'])) . '%'
          )
        ));

        if(!empty($reservation1)){
          $reservation['VehicleReservation']['start'] = $reservation1['VehicleReservation']['start'];
          $reservation['VehicleReservation']['end'] = $reservation1['VehicleReservation']['end'];
          $reservation['VehicleReservation']['start_place'] = $reservation1['VehicleReservation']['start_place'];
          $reservation['VehicleReservation']['end_place'] = $reservation1['VehicleReservation']['end_place'];
          $reservation['VehicleReservation']['vehicle_id'] = $reservation1['VehicleReservation']['vehicle_id'];
          $reservation['VehicleReservation']['vehicle_model_id'] = $reservation1['VehicleReservation']['vehicle_model_id'];
          $reservation['VehicleReservation']['confirmed'] = $reservation1['VehicleReservation']['confirmed'];
          $reservation['VehicleReservation']['tour_id'] = $this->request->data['new_tour_id'];

          if($this->VehicleReservation->save($reservation)){
            $this->response->body(json_encode(array('success' => 1)));
          } else {
            $this->response->body(json_encode(array('success' => 0)));
          }
        } else {
          if($this->VehicleReservation->delete($reservation['VehicleReservation']['id'])){
            $this->response->body(json_encode(array('success' => 1)));
          } else {
            $this->response->body(json_encode(array('success' => 0)));
          }
        }
      } else {
        $this->response->body(json_encode(array('success' => 2)));
      }
    }
  }

  public function detail($reservationId = null, $company_id = ''){

    $reservation = $this->VehicleReservation->find('first', array(
      'contain' => array(
        'VehicleTour' => array('StockOrder' => array('Client')),
        'Vehicle',
        'VehicleCompanyModel' => array('VehicleCompany'),
        'Event' => array(
          'Client',
          'Company',
          'PersonInCharge',
          'ConfirmedEventPlace' => array('Place'),
          'SuggestedActivity' => array('Activity'),
          'SelectedActivity' => array('Activity'),
          'SuggestedFBModule' => array('FBModule'),
          'SelectedFBModule' => array('FBModule')
        )
      ),
      'conditions' => array(
        'VehicleReservation.id' => $reservationId
      )
    ));

    $vehicles = array();

    if(empty($reservation['VehicleReservation']['family'])){
      $models = $this->VehicleReservation->VehicleCompanyModel->find('list');
    } else {
      $models = array();
      // $vehicleCompanyModels = $this->VehicleReservation->VehicleCompanyModel->find('all', array(
      //   'contain' => array('VehicleCompany')
      // ));
      // foreach($vehicleCompanyModels as $k => $model){
      //   $data = array(
      //     'name' => $model['VehicleCompanyModel']['name'],
      //     'value' => $model['VehicleCompanyModel']['id'],
      //     'data-content' => $model['VehicleCompanyModel']['name'] . ' ('.$model['VehicleCompany']['name'].')'
      //   );
      //   if(!empty($model['VehicleCompanyModel']['family']) && $model['VehicleCompanyModel']['family'] == $reservation['VehicleReservation']['family']){
      //     $key = $k;
      //     $data['data-content'] .= '<span class="label label-sm label-success pull-right">Correspond</span>';
      //   } else {
      //     $key = $k+100;
      //     $data['data-content'] .= '<span class="label label-sm label-warning pull-right">Ne correspond pas</span>';
      //   }
      //   $models[$key] = $data;
      // }
      // ksort($models);
      $vehicles = $this->VehicleReservation->Vehicle->find('list', array(
        'joins' => array(
          array(
            'table' => 'vehicle_company_models',
            'alias' => 'vcm',
            'conditions' => array(
              'vcm.id = Vehicle.vehicle_company_model_id'
            )
          )
        ),
        'conditions' => array(
          'vcm.family' => $reservation['VehicleReservation']['family']
        ),
        'fields' => array(
          'Vehicle.id', 'Vehicle.name_number'
        )
      ));
    }

    if(empty($vehicles)){
      $vehicles = $this->VehicleReservation->Vehicle->find('list', array(
        'conditions' => array(
          'Vehicle.vehicle_company_model_id' => $reservation['VehicleReservation']['vehicle_model_id']
        ),
        'fields' => array(
          'Vehicle.id', 'Vehicle.name_number'
        )
      ));
    }

    $this->set(compact('reservation'));
    $this->set(compact('models'));
    $this->set(compact('vehicles'));
    $this->set('families', Configure::read('VehiclesTrailers.families'));
    $this->set('locations', Configure::read('VehicleTours.locations'));

    switch($company_id){
      case 3: // festiloc
      $orders = array();
      if(!empty($reservation['VehicleTour'][0]['StockOrder'])){
        foreach($reservation['VehicleTour'][0]['StockOrder'] as $order){
          $orders[] = $order;
        }
      }
      $this->set(compact('orders'));
      break;
      case 2: // ubic
      break;
      default:
      $event = array();
      if(!empty($reservation['Event'][0])){
        $event = $reservation['Event'][0];
      }
      $this->set(compact('event'));
      break;
    }
  }

  public function update_schedule(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $data = array(
        'VehicleReservation' => array(
          'id' => $this->request->data['id'],
          $this->request->data['field'] => date('Y-m-d H:i:s', strtotime(str_replace(' - ', '', $this->request->data['date'])))
        )
      );
      if($this->VehicleReservation->save($data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function update_place(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $data = array(
        'VehicleReservation' => array(
          'id' => $this->request->data['id'],
          $this->request->data['field'] => $this->request->data['place']
        )
      );
      if($this->VehicleReservation->save($data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

}
