<?php
class StockSectionsController extends AppController {

	public function add( $companyId = null ){

		$companies = $this->StockSection->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if(!empty($companyId)){
			$company = $this->StockSection->Company->findById($companyId);
			$this->set(compact('company'));
		}

		if($this->request->is('put') OR $this->request->is('post')){
			// generate code for stock section
			$section = $this->StockSection->find('first', array(
				'conditions' => array(
					'StockSection.stock_category_id' => $this->request->data['StockSection']['stock_category_id']
				),
				'order' => 'StockSection.code DESC'
			));
			if(empty($section)){
				$this->request->data['StockSection']['code'] = 1;
			} else {
				$this->request->data['StockSection']['code'] = $section['StockSection']['code'] + 1;
			}
			if($this->StockSection->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockSection']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
	        return $this->redirect(array('action' => 'edit', $this->StockSection->id));
				} elseif($this->request->data['StockSection']['company_id'] == 3) { // festiloc
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockSection']['company_id']));
				}
			}
		}
	}

	public function edit( $id = null ){

		$companies = $this->StockSection->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if($this->request->is('put') OR $this->request->is('post')){
			if($this->StockSection->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockSection']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
	        return $this->redirect(array('action' => 'edit', $this->StockSection->id));
				} elseif($this->request->data['StockSection']['company_id'] == 3) { // festiloc
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockSection']['company_id']));
				}
			}
		} else {
			$section = $this->StockSection->findById($id);
			$categories = $this->StockSection->StockCategory->find('list', array(
				'conditions' => array(
					'StockCategory.company_id' => $section['StockSection']['company_id']
				),
				'fields' => array('StockCategory.id', 'StockCategory.code_name'),
				'order' => array('StockCategory.code')
			));
			$this->set(compact('section'));
			$this->set(compact('categories'));
			$this->request->data = $section;
		}
	}

	public function json(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$sections = $this->StockSection->find('all', array(
				'conditions' => array(
					'StockSection.stock_category_id' => $this->request->data['stock_category_id']
				),
				'fields' => array('StockSection.id', 'StockSection.code_name'),
				'order' => array('StockSection.code ASC')
			));
			$this->response->body(json_encode($sections));
		}
	}

}
