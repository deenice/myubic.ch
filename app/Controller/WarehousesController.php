<?php

App::import("Vendor", "Dijkstra", array('file' => "Dijkstra/dijkstra.php"));

class WarehousesController extends AppController {

	public function init( $id = null ){
		if($this->request->is('ajax') OR 1==1){

			$this->autoRender = false;

			// delete all passages and nodes first
			$this->truncateTables( $id );

			//finding passages among cells
			$passageCells = $this->getAllPassageCells($id);
			$passages = array();
			foreach($passageCells as $cell){
				$x = $cell['WarehouseCell']['x'];
				$y = $cell['WarehouseCell']['y'];
				$cellId = $cell['WarehouseCell']['id'];

				$topNeighbor = $this->Warehouse->WarehouseCell->find('first', array(
					'conditions' => array(
						'WarehouseCell.warehouse_id' => $id,
						'WarehouseCell.y' => ($y - 1),
						'WarehouseCell.x' => $x,
						'WarehouseCell.flag' => 2 // passage flag
					)
				));

				$bottomNeighbor = $this->Warehouse->WarehouseCell->find('first', array(
					'conditions' => array(
						'WarehouseCell.warehouse_id' => $id,
						'WarehouseCell.y' => ($y + 1),
						'WarehouseCell.x' => $x,
						'WarehouseCell.flag' => 2 // passage flag
					)
				));

				if(!empty($topNeighbor) AND empty($bottomNeighbor)){
					//end of vertical passage
					$passages['vertical'][$x][$y] = $cell;
				}
				if(empty($topNeighbor) AND !empty($bottomNeighbor)){
					//begin of vertical passage
					$passages['vertical'][$x][$y] = $cell;
				}

				$leftNeighbor = $this->Warehouse->WarehouseCell->find('first', array(
					'conditions' => array(
						'WarehouseCell.warehouse_id' => $id,
						'WarehouseCell.y' => $y,
						'WarehouseCell.x' => ($x - 1),
						'WarehouseCell.flag' => 2 // passage flag
					)
				));

				$rightNeighbor = $this->Warehouse->WarehouseCell->find('first', array(
					'conditions' => array(
						'WarehouseCell.warehouse_id' => $id,
						'WarehouseCell.y' => $y,
						'WarehouseCell.x' => ($x + 1),
						'WarehouseCell.flag' => 2 // passage flag
					)
				));
				if(!empty($leftNeighbor) AND empty($rightNeighbor)){
					//end of horizontal passage
					$passages['horizontal'][$y][$x] = $cell;
				}
				if(empty($leftNeighbor) AND !empty($rightNeighbor)){
					//begin of horizontal passage
					$passages['horizontal'][$y][$x] = $cell;
				}
			}
			if(!empty($passages)){
				foreach ($passages as $type => $passage) {
					foreach($passage as $p){
						sort($p);
						$begin = $p[0];
						$end = $p[1];
						$begin_x = min($begin['WarehouseCell']['x'], $end['WarehouseCell']['x']);
						$end_x = max($begin['WarehouseCell']['x'], $end['WarehouseCell']['x']);
						$begin_y = min($begin['WarehouseCell']['y'], $end['WarehouseCell']['y']);
						$end_y = max($begin['WarehouseCell']['y'], $end['WarehouseCell']['y']);
						$differenceX = $end_x - $begin_x;
						$differenceY = $end_y - $begin_y;
						if( ($differenceX > 1 AND $type == 'horizontal') OR ($differenceY > 1 AND $type == 'vertical') ){
							$this->Warehouse->WarehousePassage->create();
							$this->Warehouse->WarehousePassage->save(array(
								'WarehousePassage' => array(
									'type' => $type,
									'equation' => '',
									'begin_x' => $begin_x,
									'begin_y' => $begin_y,
									'end_x' => $end_x,
									'end_y' => $end_y,
									'warehouse_id' => $id
								)
							));
						}
					}
				}
			}

			// finding nodes (intersection of passages)
			$passages = $this->getAllPassages($id);
			foreach($passages as $passage){
				$verticalPassages = $this->Warehouse->WarehousePassage->find('all', array(
					'conditions' => array(
						'WarehousePassage.type' => 'vertical',
						'WarehousePassage.warehouse_id' => $id,
						'WarehousePassage.begin_x >=' => $passage['WarehousePassage']['begin_x'],
						'WarehousePassage.end_x <=' => $passage['WarehousePassage']['end_x'],
						'WarehousePassage.begin_y <=' => $passage['WarehousePassage']['begin_y'],
						'WarehousePassage.end_y >=' => $passage['WarehousePassage']['end_y'],
					)
				));
				if(!empty($verticalPassages)){
					foreach($verticalPassages as $verticalPassage){
						// find intersection
						// generate x points
						$pointsX = $pointsY = array();
						for($i = 0; $i <= $passage['WarehousePassage']['end_x']; $i++){
							$pointsX[] = ($passage['WarehousePassage']['begin_x'] + $i) . ':' . $passage['WarehousePassage']['begin_y'];
						}
						// generate y points
						for($j = 0; $j <= ($verticalPassage['WarehousePassage']['end_y'] - $verticalPassage['WarehousePassage']['begin_y']); $j++){
							$pointsY[] = $verticalPassage['WarehousePassage']['begin_x'] . ':' . ($verticalPassage['WarehousePassage']['begin_y'] + $j);
						}
						$intersection = array_intersect($pointsX, $pointsY);
						if(!empty($intersection)){
							sort($intersection);
							$coords = explode(':', $intersection[0]);
							$cell = $this->Warehouse->WarehouseCell->find('first', array(
								'conditions' => array(
									'WarehouseCell.x' => $coords[0],
									'WarehouseCell.y' => $coords[1],
									'WarehouseCell.warehouse_id' => $id
								)
							));
							$this->Warehouse->WarehouseNode->create();
							$node = array(
								'WarehouseNode' => array(
									'warehouse_id' => $id,
									'warehouse_cell_id' => $cell['WarehouseCell']['id'],
									'vertical_passage_id' => $verticalPassage['WarehousePassage']['id'],
									'horizontal_passage_id' => $passage['WarehousePassage']['id'],
									'x' => $coords[0],
									'y' => $coords[1]
								)
							);
							$this->Warehouse->WarehouseNode->save($node);

						}				
					}
				}
			}
			// compute distance between storage points and adjacent nodes
			$storagePoints = $this->Warehouse->WarehouseCell->find('all', array(
				'conditions' => array(
					'WarehouseCell.warehouse_id' => 1,
					'WarehouseCell.flag' => 1
				)
			));

			foreach($storagePoints as $storagePoint){
				$conditions = array();
				if($storagePoint['WarehouseCell']['orientation'] == 'right'){
					$conditions = array(
						'WarehousePassage.warehouse_id' => $id,
						'WarehousePassage.begin_x' => ($storagePoint['WarehouseCell']['x'] + 1),
						'WarehousePassage.begin_y <=' => $storagePoint['WarehouseCell']['y'],
						'WarehousePassage.end_y >=' => $storagePoint['WarehouseCell']['y'],
						'WarehousePassage.type' => 'vertical'
					);
				}
				if($storagePoint['WarehouseCell']['orientation'] == 'left'){
					$conditions = array(
						'WarehousePassage.warehouse_id' => $id,
						'WarehousePassage.begin_x' => ($storagePoint['WarehouseCell']['x'] - 1),
						'WarehousePassage.begin_y <=' => $storagePoint['WarehouseCell']['y'],
						'WarehousePassage.end_y >=' => $storagePoint['WarehouseCell']['y'],
						'WarehousePassage.type' => 'vertical'
					);
				}
				if($storagePoint['WarehouseCell']['orientation'] == 'up'){
					$conditions = array(
						'WarehousePassage.warehouse_id' => $id,
						'WarehousePassage.begin_y' => ($storagePoint['WarehouseCell']['y'] - 1),
						'WarehousePassage.begin_x <=' => $storagePoint['WarehouseCell']['x'],
						'WarehousePassage.end_x >=' => $storagePoint['WarehouseCell']['x'],
						'WarehousePassage.type' => 'horizontal'
					);
				}
				if($storagePoint['WarehouseCell']['orientation'] == 'down'){
					$conditions = array(
						'WarehousePassage.warehouse_id' => $id,
						'WarehousePassage.begin_y' => ($storagePoint['WarehouseCell']['y'] + 1),
						'WarehousePassage.begin_x <=' => $storagePoint['WarehouseCell']['x'],
						'WarehousePassage.end_x >=' => $storagePoint['WarehouseCell']['x'],
						'WarehousePassage.type' => 'horizontal'
					);
				}
				$passage = $this->Warehouse->WarehousePassage->find('first', array(
					'conditions' => $conditions
				));
				if($passage['WarehousePassage']['type'] == 'vertical'){
					$nodes = $this->Warehouse->WarehouseNode->find('all', array(
						'conditions' => array(
							'WarehouseNode.warehouse_id' => $id,
							'WarehouseNode.vertical_passage_id' => $passage['WarehousePassage']['id'],
							'WarehouseNode.y >=' => $passage['WarehousePassage']['begin_y'],
							'WarehouseNode.y <=' => $passage['WarehousePassage']['end_y'],

						),
						'order' => array('WarehouseNode.y')
					));
				}
				if($passage['WarehousePassage']['type'] == 'horizontal'){
					$nodes = $this->Warehouse->WarehouseNode->find('all', array(
						'conditions' => array(
							'WarehouseNode.warehouse_id' => $id,
							'WarehouseNode.horizontal_passage_id' => $passage['WarehousePassage']['id'],
							'WarehouseNode.x >=' => $passage['WarehousePassage']['begin_x'],
							'WarehouseNode.x <=' => $passage['WarehousePassage']['end_x']
						),
						'order' => array('WarehouseNode.x')
					));
				}
				if(!empty($nodes)){
					if(sizeof($nodes) == 1){
						$adjacentNodes[0] = $nodes[0];
						$adjacentNodes[1] = $nodes[0];
						$distance1 = $distance2 = floor(sqrt( pow($adjacentNodes[0]['WarehouseNode']['x'] - $storagePoint['WarehouseCell']['x'], 2) + pow($adjacentNodes[0]['WarehouseNode']['y'] - $storagePoint['WarehouseCell']['y'], 2) ));
					} else {
						for($k = 0; $k < sizeof($nodes); $k++){
							if(!empty($nodes[$k]) AND !empty($nodes[$k+1])){
								if($storagePoint['WarehouseCell']['orientation'] == 'up' OR $storagePoint['WarehouseCell']['orientation'] == 'down'){
									if($nodes[$k]['WarehouseNode']['x'] <= $storagePoint['WarehouseCell']['x'] AND $nodes[$k+1]['WarehouseNode']['x'] >= $storagePoint['WarehouseCell']['x']){
										$aroundNodes[0] = $nodes[$k];
										$aroundNodes[1] = $nodes[$k+1];
									} else {
										$aroundNodes[0] = $nodes[0];
										$aroundNodes[1] = $nodes[0];
										$k = 99999;
									}
									// elseif($storagePoint['WarehouseCell']['x'] == $passage['WarehousePassage']['begin_x'] OR $storagePoint['WarehouseCell']['x'] == $passage['WarehousePassage']['end_x']){
									// 	$aroundNodes[0] = array(
									// 		'WarehouseNode' => array(
									// 			'x' => $storagePoint['WarehouseCell']['x'],
									// 			'y' => $storagePoint['WarehouseCell']['orientation'] == 'up' ? $storagePoint['WarehouseCell']['y']-1 : $storagePoint['WarehouseCell']['y']+1,
									// 		)
									// 	);
									// 	$aroundNodes[1] = $nodes[$k];
									// 	$k = 99999;
									// }
								}
								if($storagePoint['WarehouseCell']['orientation'] == 'left' OR $storagePoint['WarehouseCell']['orientation'] == 'right'){
									if($nodes[$k]['WarehouseNode']['y'] <= $storagePoint['WarehouseCell']['y'] AND $nodes[$k+1]['WarehouseNode']['y'] >= $storagePoint['WarehouseCell']['y']){
										$aroundNodes[0] = $nodes[$k];
										$aroundNodes[1] = $nodes[$k+1];
									} else {
										$aroundNodes[0] = $nodes[0];
										$aroundNodes[1] = $nodes[0];
										$k = 99999;
									}
									// elseif($storagePoint['WarehouseCell']['y'] == $passage['WarehousePassage']['begin_y'] OR $storagePoint['WarehouseCell']['y'] == $passage['WarehousePassage']['end_y']){
									// 	$aroundNodes[0] = array(
									// 		'WarehouseNode' => array(
									// 			'y' => $storagePoint['WarehouseCell']['y'],
									// 			'x' => $storagePoint['WarehouseCell']['orientation'] == 'left' ? $storagePoint['WarehouseCell']['x']-1 : $storagePoint['WarehouseCell']['x']+1,
									// 		)
									// 	);
									// 	$aroundNodes[1] = $nodes[$k];
									// 	$k = 99999;
									// }
								}
							}
						}

						// get closest node
						$nodesWithDistance = array();
						$distance = 0;
						foreach($aroundNodes as $node){
							$distance = floor(sqrt( pow($node['WarehouseNode']['x'] - $storagePoint['WarehouseCell']['x'], 2) + pow($node['WarehouseNode']['y'] - $storagePoint['WarehouseCell']['y'], 2) ));
							$nodesWithDistance[floor($distance)] = $node;
						}
						if(sizeof($nodesWithDistance) > 1){
							ksort($nodesWithDistance);
							$distances = array_keys($nodesWithDistance);
							$adjacentNodes[0] = $nodesWithDistance[$distances[0]];
							$adjacentNodes[1] = $nodesWithDistance[$distances[1]];
							$distance1 = $distances[0];
							$distance2 = $distances[1];
						} else {
							$adjacentNodes[0] = $aroundNodes[0];
							$adjacentNodes[1] = $aroundNodes[0];
							$distance1 = $distance;
							$distance2 = $distance;
						}
					}
				}
				if(!empty($adjacentNodes)){
					$this->Warehouse->WarehouseStoragePoint->create();
					$this->Warehouse->WarehouseStoragePoint->save(array(
						'WarehouseStoragePoint' => array(
							'warehouse_id' => $id,
							'warehouse_cell_id' => $storagePoint['WarehouseCell']['id'],
							'x' => $storagePoint['WarehouseCell']['x'],
							'y' => $storagePoint['WarehouseCell']['y'],
							'adjacency_passage_id' => $passage['WarehousePassage']['id'],
							'adjacency_node1_id' => $adjacentNodes[0]['WarehouseNode']['id'],
							'adjacency_node2_id' => $adjacentNodes[1]['WarehouseNode']['id'],
							'distance_node1' => $distance1,
							'distance_node2' => $distance2
						)
					));
				}
			}

			//compute distance between every two nodes using dijkstra algorithm
			$nodes = $this->Warehouse->WarehouseNode->find('all', array(
				'conditions' => array(
					'WarehouseNode.warehouse_id' => $id
				)
			));

			// let's begin with horizontal passages
			foreach($nodes as $node){
				$nodesInSameHorizontalPassage = $this->Warehouse->WarehouseNode->find('all', array(
					'conditions' => array(
						'WarehouseNode.warehouse_id' => $id,
						'WarehouseNode.horizontal_passage_id' => $node['WarehouseNode']['horizontal_passage_id']
					),
					'order' => 'x'
				));
				for($i=0;$i<sizeof($nodesInSameHorizontalPassage); $i++){
					if(isset($nodesInSameHorizontalPassage[$i]) AND isset($nodesInSameHorizontalPassage[$i+1])){
						$node1 = $nodesInSameHorizontalPassage[$i];
						$node2 = $nodesInSameHorizontalPassage[$i+1];
						$distance = abs($node2['WarehouseNode']['x'] - $node1['WarehouseNode']['x']);
						$route = $node1['WarehouseNode']['id'] . '-' . $node2['WarehouseNode']['id'];
						$exists = $this->Warehouse->WarehouseNodeDistance->find('all', array(
							'conditions' => array(
								'OR' => array(
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $node2['WarehouseNode']['id'],
										'WarehouseNodeDistance.warehouse_node2_id' => $node1['WarehouseNode']['id']
									),
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $node1['WarehouseNode']['id'],
										'WarehouseNodeDistance.warehouse_node2_id' => $node2['WarehouseNode']['id']
									)
								)
							)
						));
						if(empty($exists)){
							$this->Warehouse->WarehouseNodeDistance->create();
							$this->Warehouse->WarehouseNodeDistance->save(array(
								'WarehouseNodeDistance' => array(
									'warehouse_id' => $id,
									'warehouse_node1_id' => $node1['WarehouseNode']['id'],
									'warehouse_node2_id' => $node2['WarehouseNode']['id'],
									'route' => $route,
									'distance' => $distance
								)
							));
						}
					}
				}
			}

			// let's continue with vertical passages
			foreach($nodes as $node){
				$nodesInSameVerticalPassage = $this->Warehouse->WarehouseNode->find('all', array(
					'conditions' => array(
						'WarehouseNode.warehouse_id' => $id,
						'WarehouseNode.vertical_passage_id' => $node['WarehouseNode']['vertical_passage_id']
					),
					'order' => 'y'
				));
				for($i=0;$i<sizeof($nodesInSameVerticalPassage); $i++){
					if(isset($nodesInSameVerticalPassage[$i]) AND isset($nodesInSameVerticalPassage[$i+1])){
						$node1 = $nodesInSameVerticalPassage[$i];
						$node2 = $nodesInSameVerticalPassage[$i+1];
						$distance = abs($node2['WarehouseNode']['y'] - $node1['WarehouseNode']['y']);
						$route = $node1['WarehouseNode']['id'] . '-' . $node2['WarehouseNode']['id'];
						$exists = $this->Warehouse->WarehouseNodeDistance->find('all', array(
							'conditions' => array(
								'OR' => array(
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $node2['WarehouseNode']['id'],
										'WarehouseNodeDistance.warehouse_node2_id' => $node1['WarehouseNode']['id']
									),
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $node1['WarehouseNode']['id'],
										'WarehouseNodeDistance.warehouse_node2_id' => $node2['WarehouseNode']['id']
									)
								)
							)
						));
						if(empty($exists)){
							$this->Warehouse->WarehouseNodeDistance->create();
							$this->Warehouse->WarehouseNodeDistance->save(array(
								'WarehouseNodeDistance' => array(
									'warehouse_id' => $id,
									'warehouse_node1_id' => $node1['WarehouseNode']['id'],
									'warehouse_node2_id' => $node2['WarehouseNode']['id'],
									'route' => $route,
									'distance' => $distance
								)
							));
						}
					}
				}
			}
			$nodesWithDistance = $this->Warehouse->WarehouseNodeDistance->find('all', array(
				'conditions' => array(
					'WarehouseNodeDistance.warehouse_id' => $id
				)
			));
			$matrixWidth = 50;
			$infiniteDistance = 10000;
			$routes = array();
			foreach($nodesWithDistance as $k => $node){
				$routes[$k] = array(intval($node['WarehouseNodeDistance']['warehouse_node1_id']), intval($node['WarehouseNodeDistance']['warehouse_node2_id']), intval($node['WarehouseNodeDistance']['distance']));
			}
			for ($i=0,$m=count($routes); $i<$m; $i++) {
			    $x = $routes[$i][0];
			    $y = $routes[$i][1];
			    $c = $routes[$i][2];
			    $ourMap[$x][$y] = $c;
			    $ourMap[$y][$x] = $c;
			}
			// ensure that the distance from a node to itself is always zero
			// Purists may want to edit this bit out.
			for ($i=0; $i < $matrixWidth; $i++) {
			    for ($k=0; $k < $matrixWidth; $k++) {
			        if ($i == $k) $ourMap[$i][$k] = 0;
			    }
			}
			debug($ourMap);exit;
			$dijkstra = new Dijkstra($ourMap, $infiniteDistance, $matrixWidth);
			$nodes = $this->Warehouse->WarehouseNode->find('all', array(
				'conditions' => array(
					'WarehouseNode.warehouse_id' => $id
				)
			));
			foreach($nodes as $node){
				$dijkstra->findShortestPath($node['WarehouseNode']['id']);
				$paths = $dijkstra->getResults();
				foreach($paths as $toNode => $path){
					if($path['distance'] > 1 AND $path['distance'] < $infiniteDistance){
						$exists = $this->Warehouse->WarehouseNodeDistance->find('all', array(
							'conditions' => array(
								'OR' => array(
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $node['WarehouseNode']['id'],
										'WarehouseNodeDistance.warehouse_node2_id' => $toNode
									),
									array(
										'WarehouseNodeDistance.warehouse_node1_id' => $toNode,
										'WarehouseNodeDistance.warehouse_node2_id' => $node['WarehouseNode']['id']
									)
								)
							)
						));
						if(empty($exists)){
							$this->Warehouse->WarehouseNodeDistance->create();
							$this->Warehouse->WarehouseNodeDistance->save(array(
								'WarehouseNodeDistance' => array(
									'warehouse_id' => $id,
									'warehouse_node1_id' => $node['WarehouseNode']['id'],
									'warehouse_node2_id' => $toNode,
									'route' => $path['route'],
									'distance' => $path['distance']
								)
							));
						}
					}
				}
			}
			$this->response->body(json_encode(array('success' => 1)));
		}
	}

	protected function getAllPassageCells($warehouseId){
		$data = $this->Warehouse->WarehouseCell->find('all', array(
			'conditions' => array(
				'WarehouseCell.warehouse_id' => $warehouseId,
				'WarehouseCell.flag' => 2 // passage flag
			),
			'order' => array('WarehouseCell.x', 'WarehouseCell.y')
		));
		return $data;
	}

	protected function truncateTables( $warehouseId ){
		$this->Warehouse->WarehousePassage->deleteAll(array(
			'WarehousePassage.warehouse_id' => $warehouseId
		));
		$this->Warehouse->WarehousePassage->query('ALTER TABLE `warehouse_passages` AUTO_INCREMENT = 1');
		$this->Warehouse->WarehouseNode->deleteAll(array(
			'WarehouseNode.warehouse_id' => $warehouseId
		));
		$this->Warehouse->WarehouseNode->query('ALTER TABLE `warehouse_nodes` AUTO_INCREMENT = 1');
		$this->Warehouse->WarehouseStoragePoint->deleteAll(array(
			'WarehouseStoragePoint.warehouse_id' => $warehouseId
		));
		$this->Warehouse->WarehouseStoragePoint->query('ALTER TABLE `warehouse_storage_points` AUTO_INCREMENT = 1');
		$this->Warehouse->WarehouseNodeDistance->deleteAll(array(
			'WarehouseNodeDistance.warehouse_id' => $warehouseId
		));
		$this->Warehouse->WarehouseNodeDistance->query('ALTER TABLE `warehouse_node_distances` AUTO_INCREMENT = 1');
	}


	protected function getAllPassages($warehouseId){
		$data = $this->Warehouse->WarehousePassage->find('all', array(
			'conditions' => array(
				'WarehousePassage.warehouse_id' => $warehouseId,
				'WarehousePassage.type' => 'horizontal'
			),
			'order' => 'WarehousePassage.begin_x'
		));
		return $data;
	}

	public function test($id = null){

    	// ini_set('memory_limit', '-1');
    	// set_time_limit(0);

		$numberOfGenerations = 50;
		$populationSize = 20;
		$population = array();

		// $sp = $this->Warehouse->WarehouseStoragePoint->find('all', array(
		// 	'conditions' => array(
		// 		'WarehouseStoragePoint.warehouse_id' => $id
		// 	),
		// 	'limit' => 8,
		// 	'order' => 'rand()'
		// ));
		// $spIds = array(0 => 59);
		// foreach ($sp as $key => $p) {
		// 	$spIds[] = $p['WarehouseStoragePoint']['id'];
		// }
		$spIds = array(44, 42, 54, 143, 124, 135, 89);
		// this is the first generation of the population
		$start = $spIds[0];
		$rest = array_slice($spIds, 1);
		for($i=0;$i<$populationSize;$i++){
			shuffle($rest);
			$population[] = array_merge(array(0 => $start), $rest);
		}
		$training = $population;
		$shortestDistance = 0;
		$bestRoute = array();
		for($generationStep = 0; $generationStep < $numberOfGenerations; $generationStep++){
			// compute distance (score) of generated chromosome, i.e. between each storage point

			foreach($training as $key => $chromosome){
				$distance = 0;
				for($i=0;$i<sizeof($training);$i++){
					if(isset($chromosome[$i]) AND isset($chromosome[$i+1])){
						$distance = $distance + $this->Warehouse->computeDistance($chromosome[$i], $chromosome[$i+1]);
					}
					if($i == (sizeof($training)-1)){
						$training[$key]['score'] = $distance;
					}
				}
			}
			// we need to sort the chromosomes according to the route's distance
			$tmp = array();
			foreach($training as $k => $p){
				$tmp[$k] = $p['score'];
			}
			array_multisort($tmp, SORT_ASC, $training);
			// let's compute fitness degree for each chromosome
			foreach($training as $j => $chromosome){
				$fitness = $j / sizeof($training);
				$training[$j]['fitness'] = $fitness;
			}

			// re-populate training set with new chromosomes based on fitness degree
			$best = $training[0];
			unset($best['score']);
			unset($best['fitness']);
			if($generationStep == $numberOfGenerations - 1){
				$bestRoute = $training[0];
			} else {
				foreach($training as $k => $chromosome){
					if($generationStep < $numberOfGenerations){
						unset($chromosome['score']);
						unset($chromosome['fitness']);
					}
					if($k != 0){
						$start = $best[0];
						//$first = $best[1];
						$rest = array_slice($best, 1);
						shuffle($rest);
						$newSet = array_merge(array(0 => $start), $rest);
						$training[$k] = $newSet;
					} else {
						$training[$k] = $chromosome;
					}
				}
			}
			
		}
		debug($bestRoute);

		$distance = $bestRoute['score'];
		unset($bestRoute['score']);
		unset($bestRoute['fitness']);
		$bestRouteSorted = $bestRoute;
		sort($bestRouteSorted);

		$existingBestRoute = $this->Warehouse->WarehouseBestRoute->find('first', array(
			'conditions' => array(
				'WarehouseBestRoute.warehouse_id' => $id,
				'WarehouseBestRoute.warehouse_storage_point_ids_sorted' => implode(',', $bestRouteSorted)
			),
			'order' => array('distance'),
			'cache' => implode(',', $bestRouteSorted),
			'cacheConfig' => 'short'
		));

		if(!empty($existingBestRoute)){
			if($existingBestRoute['WarehouseBestRoute']['distance'] < $distance){
				debug(explode(',', $existingBestRoute['WarehouseBestRoute']['warehouse_storage_point_ids']));
				debug($existingBestRoute['WarehouseBestRoute']['distance']);
				$bestRoute = explode(',', $existingBestRoute['WarehouseBestRoute']['warehouse_storage_point_ids']);
			} else {
				$this->Warehouse->WarehouseBestRoute->save(array(
					'WarehouseBestRoute' => array(
						'warehouse_id' => $id,
						'warehouse_storage_point_ids' => implode(',',$bestRoute),
						'warehouse_storage_point_ids_sorted' => implode(',',$bestRouteSorted),
						'distance' => $distance
					)
				));
			}
		}


		debug(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"]);
		debug($this->getRoute($bestRoute));

		exit;
	}

	protected function getRoute($spids = array()){
		if(!empty($spids)){
			$route = array();
			foreach($spids as $k => $spid){
				if(is_int($k)){
					$sp = $this->Warehouse->WarehouseStoragePoint->find('first', array(
						'conditions' => array(
							'WarehouseStoragePoint.id' => $spid
						),
						'cache' => $spid,
						'cacheConfig' => 'short'
					));
					if(!empty($sp)){
						$passage = $this->Warehouse->WarehousePassage->find('first', array(
							'conditions' => array(
								'WarehousePassage.id' => $sp['WarehouseStoragePoint']['adjacency_passage_id']
							),
							'cache' => $sp['WarehouseStoragePoint']['adjacency_passage_id'],
							'cacheConfig' => 'short'
						));
					}
					if(!empty($passage)) $route[$passage['WarehousePassage']['name']][] = $spid;
				}
			}
			return $route;
		} else {
			return null;
		}
	}

	public function plan( $id = null ){
		$data = $this->Warehouse->WarehouseCell->find('all', array(
			'conditions' => array(
				'warehouse_id' => $id
			)
		));
		$cells = array();
		foreach($data as $cell){
			$cells[$cell['WarehouseCell']['x']][$cell['WarehouseCell']['y']]['cell'] = $cell;
			$node = $this->Warehouse->WarehouseNode->find('first', array(
				'conditions' => array(
					'WarehouseNode.x' => $cell['WarehouseCell']['x'],
					'WarehouseNode.y' => $cell['WarehouseCell']['y'],
					'WarehouseNode.warehouse_id' => $id
				)
			));
			$sp = $this->Warehouse->WarehouseStoragePoint->find('first', array(
				'conditions' => array(
					'WarehouseStoragePoint.warehouse_id' => $id,
					'WarehouseStoragePoint.warehouse_cell_id' => $cell['WarehouseCell']['id']
				)
			));
			if(!empty($node)){
				$cells[$cell['WarehouseCell']['x']][$cell['WarehouseCell']['y']]['node'] = $node;
			}
			if(!empty($sp)){
				$cells[$cell['WarehouseCell']['x']][$cell['WarehouseCell']['y']]['sp'] = $sp;
			}
		}
		$this->set(compact('cells'));
		$this->set('id', $id);
	}

	public function saveCell(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$cell = array(
				'WarehouseCell' => array(
					'id' => empty($_POST['cell_id']) ? '' : $_POST['cell_id'],
					'x' => $_POST['x'],
					'y' => $_POST['y'],
					'flag' => $_POST['flag'],
					'warehouse_id' => 1
				)
			);
			if($this->Warehouse->WarehouseCell->save($cell)){
				$output['success'] = 1;
				$output['id'] = $this->Warehouse->WarehouseCell->id;
			} else {
				$output['success'] = 0;
			}
			$this->response->body(json_encode($output));
		}
	}

	public function getCell(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$cell = $this->Warehouse->WarehouseCell->findByXAndY($_POST['x'], $_POST['y']);
			if(!empty($cell)){
				$this->response->body(json_encode($cell));
			} else {
				$this->response->body(json_encode(array('empty' => 1)));
			}
			
		}
		
	}


}