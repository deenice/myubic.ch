<?php

class ArtistsController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'All' => [
				'model' => 'Artist',
				'columns' => [
					'name' => array(
						'bSearchable' => 'customSearchArtist'
					),
					'contact' => array(
						'useField' => false,
						'bSearchable' => 'customSearchArtist'
					),
					'tags' => array(
						'useField' => false,
						'bSearchable' => 'customSearchArtist'
					),
					'Actions' => null,
				],
				'contain' => array('Tag'),
				'fields' => array(
					'Artist.id',
					'Artist.contact_person_first_name',
					'Artist.contact_person_last_name',
					'Artist.contact_person_phone',
					'Artist.contact_person_email',
				),
				'autoData' => false
			]
		],
	];

	public function index() {
		$this->DataTable->setViewVar(array('All'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['All']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['All']['columns']['contact']['label'] = __('Contact person');
	}

	public function add(){

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Artist->create();
			if ($this->Artist->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Artist has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Artist->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Artist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}

	}

	public function edit( $id = null ){

		$this->Artist->id = $id;

		if (!$this->Artist->exists()) {
			throw new NotFoundException(__('Invalid Artist'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Artist->create();
			if ($this->Artist->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Artist has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Artist->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Artist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$artist = $this->Artist->find('first', array(
				'conditions' => array(
					'Artist.id' => $id
				),
				'contain' => array(
					'Tag'
				)
			));
			foreach($artist['Tag'] as $tag){
				$tags[$tag['category']][] = $tag['id'];
			}

			$this->set(compact('artist'));
			$this->set(compact('tags'));
			$this->request->data = $artist;
		}

	}


	public function test(){
		$this->Artist->contain('Tag');
		debug($this->Artist->find('all'));
		exit;
	}

}
