<?php

class RoundAnswersController extends AppController {

  public function save($id = null){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data['RoundAnswer'])){
        if(empty($id)){
          $answers = array();
          $this->response->body(json_encode($this->request->data));
          for($k = 0; $k < sizeof($this->request->data['RoundAnswer']['checked']); $k++){
            $answers[$k]['RoundAnswer']['user_id'] = $this->request->data['RoundAnswer']['user_id'][$k];
            if(empty($this->request->data['RoundAnswer']['checked'][$k])){
              $answers[$k]['RoundAnswer']['answer'] = '';
            } elseif(!empty($this->request->data['RoundAnswer']['id'][$k])){
              $answers[$k]['RoundAnswer']['answer'] = $this->request->data['RoundAnswer']['answer'][$k];
            } else {
              $answers[$k]['RoundAnswer']['answer'] = 'waiting';
            }
            $answers[$k]['RoundAnswer']['round_step_id'] = $this->request->data['RoundAnswer']['round_step_id'][$k];
            $answers[$k]['RoundAnswer']['id'] = empty($this->request->data['RoundAnswer']['id'][$k]) ? '' : $this->request->data['RoundAnswer']['id'][$k];
          }
          if($this->RoundAnswer->saveMany($answers)){
            $notSentRoundStep = $this->RoundAnswer->RoundStep->find('first', array(
              'conditions' => array(
                'RoundStep.round_id' => $this->request->data['Round']['id'],
                'RoundStep.sent' => 0
              ),
              'order' => 'RoundStep.weight'
            ));
            if(!empty($notSentRoundStep)){
              $this->RoundAnswer->RoundStep->id = $notSentRoundStep['RoundStep']['id'];
              $this->RoundAnswer->RoundStep->send();
            }
            $this->response->body(json_encode(array('success' => 1)));
          } else {
            $this->response->body(json_encode(array('success' => 0)));
          }
        }
      }
    }
  }

  public function process($token, $status){

    $competencesJobs = Configure::read('Competences.jobs');

    $answer = $this->RoundAnswer->findByToken($token);

    if(empty($answer)){
      return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
    }

    $step = $this->RoundAnswer->RoundStep->findById($answer['RoundAnswer']['round_step_id']);
    $round = $this->RoundAnswer->RoundStep->Round->findById($step['RoundStep']['round_id']);
    $this->RoundAnswer->RoundStep->Round->Job->contain(array('Event'));
    $job = $this->RoundAnswer->RoundStep->Round->Job->findById($round['Round']['job_id']);
    $user = $this->RoundAnswer->User->findById($answer['RoundAnswer']['user_id']);

    $this->Auth->login($user['User']);

    if($answer['RoundAnswer']['answer'] != 'waiting'){
      $this->Session->setFlash(__('You have already answered for this job'), 'alert', array('type' => 'warning'));
      return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
    } else {
      $answer['RoundAnswer']['answer'] = $status;
      $this->RoundAnswer->save($answer);
    }

    if($status == 'not_interested' || $status == 'not_available'){

      // check if all users have answered
      $waiting = $this->RoundAnswer->find('all', array(
        'conditions' => array(
          'round_step_id' => $answer['RoundAnswer']['round_step_id'],
          'answer' => 'waiting'
        )
      ));
      //CakeLog::write('myubic', json_encode($waiting));
      if(empty($waiting)){
        // we can send next step
        $actualStep = $this->RoundAnswer->RoundStep->findById($answer['RoundAnswer']['round_step_id']);
        $nextStep = $this->RoundAnswer->RoundStep->find('first', array(
          'conditions' => array(
            'RoundStep.weight >' => $actualStep['RoundStep']['weight'],
            'RoundStep.round_id' => $actualStep['RoundStep']['round_id'],
            'RoundStep.sent' => 0
          ),
          'order' => 'RoundStep.weight'
        ));
        if(!empty($nextStep)){
          $this->RoundAnswer->RoundStep->id = $nextStep['RoundStep']['id'];
          $this->RoundAnswer->RoundStep->send();
        }
      }

      $this->Session->setFlash(__('Your answer to the job %s has been saved. Thanks for your feedback.', $competencesJobs[$job['Job']['job']]), 'alert', array('type' => 'success'));
      return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
    }

    // check if round is closed or still open
    if($round['Round']['status'] == 'open' && $status == 'interested'){
      // first we need to check if the user is not enrolled on test jobs (EG, MG)
      $event = $this->RoundAnswer->RoundStep->Round->Job->Event->findById($job['Job']['event_id']);
      if(in_array($event['Event']['company_id'], array(4,5))){
        $testJobs = $this->RoundAnswer->RoundStep->Round->Job->find('all', array(
          'joins' => array(
            array(
              'table' => 'jobs_users',
              'alias' => 'ju',
              'conditions' => array('Job.id = ju.job_id', 'ju.user_id' => $user['User']['id'])
            ),
            array(
              'table' => 'events',
              'alias' => 'e',
              'conditions' => array('e.id = Job.event_id')
            )
          ),
          'conditions' => array(
            'Job.sector' => 'fb',
            'Job.job' => 'test',
            'Job.id <>' => $job['Job']['id'],
            'e.confirmed_date >=' => $event['Event']['confirmed_date']
          )
        ));
        if(!empty($testJobs)){
          $this->Session->setFlash(__('We are sorry but you already applied for a test job. You will be able to work on other jobs later.'), 'alert', array('type' => 'warning'));
          return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
        }
      }
      // tell user he's selected for the job
      //$job['Job']['user_id'] = $answer['RoundAnswer']['user_id'];
      $remaining = $job['Job']['staff_needed'] - $job['Job']['staff_enrolled'];
      if($remaining >= 1){
        $exists = $this->RoundAnswer->RoundStep->Round->Job->JobUser->findByJobIdAndUserId($job['Job']['id'], $user['User']['id']);
        $jobUser = array(
          'JobUser' => array(
            'id' => empty($exists) ? '' : $exists['JobUser']['id'],
            'user_id' => $user['User']['id'],
            'job_id' => $job['Job']['id']
          )
        );
        $this->RoundAnswer->RoundStep->Round->Job->JobUser->save($jobUser);
        $this->Session->setFlash(__('You have been assigned to the job %s', $competencesJobs[$job['Job']['job']]), 'alert', array('type' => 'success'));
      } else {
        $this->Session->setFlash(__('We are sorry but the job has been already assigned.'), 'alert', array('type' => 'warning'));
      }

      // reload job to see if staff is complete
      $job = $this->RoundAnswer->RoundStep->Round->Job->findById($round['Round']['job_id']);
      if($job['Job']['staff_needed'] == $job['Job']['staff_enrolled']){
        $round['Round']['status'] = 'closed';
        $this->RoundAnswer->RoundStep->Round->save($round);
      } else {
        // we need to update staff_enrolled
        $actualStaffEnrolled = $job['Job']['staff_enrolled'];
        $job['Job']['staff_enrolled'] = $actualStaffEnrolled + 1;
        $this->RoundAnswer->RoundStep->Round->Job->save($job, array('callbacks' => false));
      }
      return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));

    } elseif($round['Round']['status'] == 'closed'){
      // tell user job has been assigned
      $this->Session->setFlash(__('We are sorry but the job has been already assigned.'), 'alert', array('type' => 'warning'));
      return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
    }
  }

  public function set_to_waiting(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->RoundAnswer->id = $this->request->data['id'];
      $answer = $this->request->data['answer'] == 'waiting' ? 'waiting' : '';
      if($this->RoundAnswer->saveField('answer', $answer, array('callbacks' => true))){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }
}
