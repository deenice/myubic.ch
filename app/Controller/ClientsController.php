<?php

class ClientsController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Clients' => [
				'model' => 'ContactPeople',
				'joins' => array(
					array(
						'table' => 'clients',
						'alias' => 'Client1',
						'type' => 'inner',
						'conditions' => array(
							'Client1.id = ContactPeople.client_id'
						)
					),
					array(
						'table' => 'clients_companies',
						'alias' => 'ClientCompany',
						'type' => 'inner',
						'conditions' => array(
							'ClientCompany.client_id = Client1.id'
						)
					),
					array(
						'table' => 'companies',
						'alias' => 'Company',
						'type' => 'inner',
						'conditions' => array(
							'ClientCompany.company_id = Company.id'
						)
					)
				),
				'columns' => [
					'Client.name' => array(
					  'bSearchable' => 'customSearchContactPeople'
					),
					'name' => array(
						'bSearchable' => 'customSearchContactPeople'
					),
					'company' => array(
						'useField' => false
					),
					'address' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'conditions' => array(
					'ContactPeople.status' => 1,
					'ClientCompany.company_id <>' => 3
				),
				'contain' => array(
					'Client'
				),
				'fields' => array(
					'Client.name', 'Client.address', 'Client.zip', 'Client.city', 'Client.country', 'ContactPeople.full_name', 'ContactPeople.phone', 'ContactPeople.email', 'Company.name'
				),
				'autoData' => false
			]
		]
	];

	public function index(){
		$this->DataTable->setViewVar(array('Clients'));
	}

	public function beforeFilter(){
		parent::beforeFilter();
		$this->DataTable->settings['Clients']['columns']['Client.name']['label'] = __('Client');
		$this->DataTable->settings['Clients']['columns']['name']['label'] = __('Contact person');
		$this->DataTable->settings['Clients']['columns']['address']['label'] = __('Address');
	}

	public function index_json(){

		$view = new View($this);
		$html = $view->loadHelper('Html');

		$this->loadModel('ContactPeople');

		$contactPeoples = $this->ContactPeople->find('all', array(
			'joins' => array(
				array(
					'table' => 'clients',
					'alias' => 'Client1',
					'type' => 'inner',
					'conditions' => array(
						'Client1.id = ContactPeople.client_id'
					)
				),
				array(
					'table' => 'clients_companies',
					'alias' => 'ClientCompany',
					'type' => 'inner',
					'conditions' => array(
						'ClientCompany.client_id = Client.id'
					)
				)
			),
			'contain' => array(
				'Client'
			),
			'conditions' => array(
				'ContactPeople.name <>' => '',
				'ContactPeople.status' => 1,
				'ClientCompany.company_id <>' => 3
			),
			'order' => array('Client.name', 'ContactPeople.created DESC'),
			'cache' => date('dmY'),
			'cacheConfig' => 'short'
		));
		$data = Cache::read('clients_json_' . date('dmY'), 'long');
		if(empty($data)){
			foreach($contactPeoples as $k => $cp){
				$data[$k]['Client']['id'] = $cp['Client']['id'];
				$data[$k]['Client']['name'] = $cp['Client']['name'];
				$data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['name'];
				$data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['email'];
				$data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['phone'];
				$data[$k]['Client']['address'][] = $cp['Client']['address'];
				$data[$k]['Client']['address'][] = $cp['Client']['zip_city'];
				$data[$k]['actions'][] = $html->link(
					'<i class="fa fa-search"></i> ' . __("View"),
					array('controller' => 'clients', 'action' => 'view', $cp['Client']['id']),
					array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
				);
				$data[$k]['actions'][] = $html->link(
					'<i class="fa fa-edit"></i> ' . __("Edit"),
					array('controller' => 'clients', 'action' => 'edit', $cp['Client']['id']),
					array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'target' => '_blank')
				);

			}
		}
		Cache::write('clients_json_' . date('dmY'), $data, 'long');

		$this->set(compact('data'));
		$this->set('_serialize', array('data'));

	}

	public function add( ) {

		$this->set('companies', $this->Client->Company->find('list'));

		$clientCompanies = $this->Client->Company->find('list', array(
			'joins' => array(
				array(
					'table' => 'clients_companies',
					'alias' => 'ClientCompany',
					'type' => 'INNER',
					'conditions' => array(
						'ClientCompany.company_id = Company.id'
					)
				),
				array(
					'table' => 'clients',
					'alias' => 'Client',
					'type' => 'INNER',
					'conditions' => array(
						'Client.id = ClientCompany.client_id'
					)
				)
			),
			'fields' => array('Company.id')
		));
		$this->set(compact('clientCompanies'));

		if($this->request->is('post') OR $this->request->is('put')){

			if($this->Client->saveAssociated($this->request->data)){
				if(!empty($this->request->data['Client']['logo']['tmp_name'])){
					$this->User->Document->saveFile($this->request->data['Client']['logo'], $this->Client->id, array('clients','logo'), 'client_logo', Configure::read('Documents.Images.allowedExtensions'));
				}
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Client']['name']), 'alert', array('type' => 'success'));
				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Client->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'client_id' => $this->Client->id));
					} elseif($this->request->data['destination'] == 'event'){
						return $this->redirect(array('controller' => 'events', 'action' => 'add', $this->Client->id));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		} elseif($this->request->is('ajax')){
			$this->autoRender = false;
			$data = $this->request->query['data'];

			if(!empty($data['Client']['companies'])){
				if(gettype($data['Client']['companies']) == 'string'){
					$data['Company'][0]['id'] = $data['Client']['companies'];
				} else {
					foreach($data['Client']['companies'] as $company){
						$data['Company'][]['id'] = $company;
					}
				}
			}

			$this->Client->create();
			if($this->Client->saveAssociated($data)){
				//$data['ContactPeople']['client_id'] = $this->Client->id;
				//$this->Client->ContactPeople->create();
				// $this->Client->ContactPeople->save($data['ContactPeople']);
				// $this->response->body(json_encode(array(
				//	 'success' => 1,
				//	 'clientId' => $this->Client->id,
				//	 'clientName' => $this->Client->field('name'),
				//	 'contactPeopleId' => $this->Client->ContactPeople->id
				// )));
				$cp = $this->Client->ContactPeople->findByClientId($this->Client->id);
				$this->response->body(json_encode(array(
					'success' => 1,
					'clientId' => $this->Client->id,
					'clientName' => $this->Client->field('name'),
					'contactPeopleId' => $cp['ContactPeople']['id']
				)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		}
	}

	public function edit($id = null){

		$this->Client->id = $id;
		$this->set('companies', $this->Client->Company->find('list'));

		$clientCompanies = $this->Client->Company->find('list', array(
			'joins' => array(
				array(
					'table' => 'clients_companies',
					'alias' => 'ClientCompany',
					'type' => 'INNER',
					'conditions' => array(
						'ClientCompany.company_id = Company.id'
					)
				),
				array(
					'table' => 'clients',
					'alias' => 'Client',
					'type' => 'INNER',
					'conditions' => array(
						'Client.id = ClientCompany.client_id'
					)
				)
			),
			'conditions' => array(
				'Client.id' => $id
			),
			'fields' => array('Company.id')
		));
		$this->set(compact('clientCompanies'));

		if($this->request->is('post') OR $this->request->is('put')){

			if(!empty($this->request->data['Client']['logo']['tmp_name'])){
				$this->Client->Document->saveFile($this->request->data['Client']['logo'], $id, array('clients','logo'), 'client_logo', Configure::read('Documents.Images.allowedExtensions'));
			}

			if(!empty($this->request->data['Client']['companies'])){
				foreach($this->request->data['Client']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}

			//debug($this->request->data);exit;

			if($this->Client->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Client']['name']), 'alert', array('type' => 'success'));

				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Client->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'client_id' => $this->Client->id));
					} elseif($this->request->data['destination'] == 'event'){
						return $this->redirect(array('controller' => 'events', 'action' => 'add', 'client_id' => $this->Client->id));
					} else {
						if($this->request->data['Company'][0]['id'] == 3){
							return $this->redirect(array('action' => 'clients', 'controller' => 'festiloc'));
						} else {
							return $this->redirect(array('action' => 'index'));
						}
					}
				} else {
					if($this->request->data['Company'][0]['id'] == 3){
						return $this->redirect(array('action' => 'clients', 'controller' => 'festiloc'));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				}
			}
		} elseif($this->request->is('ajax')){
			$this->autoRender = false;
			$data = $this->request->query['data'];

			if(!empty($data['Client']['companies'])){
				foreach($data['Client']['companies'] as $company){
					$data['Company'][]['id'] = $company;
				}
			}
			if($this->Client->saveAssociated($data)){
				$this->response->body(json_encode(array(
					'success' => 1,
					'clientId' => $this->Client->id,
					'clientName' => $this->Client->field('name')
				)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		} else {
			$client = $this->Client->find('first', array(
				'conditions' => array(
					'Client.id' => $id
				),
				'contain' => array(
					'ContactPeople',
					'Logo'
				)
			));
			$this->request->data = $client;
		}
	}

	public function view($id = null){

		$view = new View($this);
		$html = $view->loadHelper('Html');
		$time = $view->loadHelper('Time');
		$statuses = Configure::read('StockOrders.status');

		$client = $this->Client->find('first', array(
			'conditions' => array(
				'Client.id' => $id
			),
			'contain' => array(
				'ContactPeople',
				'Company',
				'Logo'
			)
		));
		if(empty($client['Company'])){
			$this->Session->setFlash(__('This client is not associated to a company. Please contact administrator.'), 'alert', array('type' => 'danger'));
			return $this->redirect($this->referer());
		}
		$company = $client['Company'][0];

		$this->set(compact('client'));

		// get data for timeline
		$timeline = array();
		$sizes = array();
		$notes = array();
		$notes = $this->Client->Note->find('all', array(
		  'conditions' => array(
				'Note.model_id' => $id,
				'Note.model' => 'client'
		  ),
			'contain' => array('User')
		));
		$numberOfNotes = sizeof($notes);
		if(!empty($notes)){
		  foreach($notes as $item){
			$ts = strtotime($item['Note']['date']);
			$icon = 'icon-notebook';
			$title = __('Note');
			$description = $item['Note']['message'];
			$timeline[$ts][] = array(
			  'type' => 'note',
			  'title' => $item['Note']['title'],
			  'description' => $description,
			  'date' => $time->format($item['Note']['date'], '%d %B %Y'),
			  'icon' => $icon,
			  'color' => 'font-blue-sharp',
				'author' => $item['User']['full_name'],
				// 'actions' => array(
				// 	array(
				// 		'url' => array('controller' => 'notes', 'action' => 'edit', 'id' => $item['Note']['id']),
				// 		'color' => 'grey-gallery',
				// 		'title' => sprintf('<i class="fa fa-edit"></i> %s', __('Edit'))
				// 	),
				// 	array(
				// 		'url' => array('controller' => 'notes', 'action' => 'delete', 'id' => $item['Note']['id']),
				// 		'color' => 'red',
				// 		'title' => sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete'))
				// 	)
				// )
			);

		  }
		}
		$sticky = array();
		$sticky = $this->Client->Note->find('all', array(
		  'contain' => array('User'),
		  'conditions' => array(
				'Note.model_id' => $id,
				'Note.model' => 'client',
				'Note.sticky' => 1
		  ),
		  'order' => 'Note.date DESC'
		));
		$leads = array();
		$leads = $this->Client->Lead->find('all', array(
		  'conditions' => array(
			'Lead.client_id' => $id
		  )
		));
		if(!empty($leads)){
		  foreach($leads as $item){
			$ts = strtotime($item['Lead']['date']);
			$icon = '';
			if($item['Lead']['type'] == 'call'){
			  $icon = 'icon-call-out';
			  $title = __('Phone call');
			}
			if($item['Lead']['type'] == 'newsletter'){
			  $icon = 'icon-envelope-letter';
			  $title = __('Newsletter');
			}
			$description = $item['Lead']['description'];
			$timeline[$ts][] = array(
			  'type' => 'marketing',
			  'title' => $title,
			  'description' => $description,
			  'date' => $time->format($item['Lead']['date'], '%d %B %Y'),
			  'icon' => $icon,
			  'color' => 'font-green-haze',
				'actions' => array(
					array(
						'url' => array('controller' => 'leads', 'action' => 'view', $item['Lead']['id']),
						'color' => 'grey-gallery',
						'title' => sprintf('<i class="fa fa-search"></i> %s', __('View'))
					)
				)
			);

		  }
		}
		if(in_array($company['id'], array(2,4,5,6))){ // UBIC UG EG MG
			$events = $this->Client->Event->find('all', array(
				'conditions' => array(
				  'Event.client_id' => $id
				),
				'contain' => array(
					'Date',
					'PersonInCharge',
					'SelectedActivity' => array('Activity'),
					'SuggestedActivity' => array('Activity'),
					'SelectedFBModule' => array('FBModule'),
					'SuggestedFBModule' => array('FBModule'),
					'ContactPeople'
				),
				'group' => 'Event.id'
		  ));
			$sizes = array('events' => 0, 'offers' => 0, 'null' => 0, 'refused' => 0, 'partner' => 0);
		  foreach($events as $item){

				$title = !empty($item['Event']['code_name']) ? $item['Event']['code_name'] : $item['Event']['name'];
				if(!empty($item['Event']['confirmed_date'])){
					$date = $time->format($item['Event']['confirmed_date'], '%d %B %Y');
					$ts = strtotime($item['Event']['confirmed_date']);
				} elseif(empty($item['Event']['confirmed_date']) && !empty($item['Date'])){
					$dates = array();
					foreach($item['Date'] as $date){
						$dates[] = $time->format($date['date'], '%d %B %Y');
					}
					$date = implode(__(' or '), $dates);
					$ts = strtotime($item['Date'][0]['date']);
				} elseif(empty($item['Event']['confirmed_date']) && empty($item['Date']) && !empty($item['Event']['period_start'])){
					$date = $time->format($item['Event']['period_start'], '%d %B %Y');
					if(!empty($item['Event']['period_end'])){
						$date .= ' - ' . $time->format($item['Event']['period_end'], '%d %B %Y');
					}
					$ts = strtotime($item['Event']['period_start']);
				} else {
					$date = '&nbsp;';
					$ts = 0;
				}

				$numberOfPersons = 0;
				if(!empty($item['Event']['confirmed_number_of_persons'])){
					$numberOfPersons = $item['Event']['confirmed_number_of_persons'];
				} else {
					if(!empty($item['Event']['min_number_of_persons']) && empty($item['Event']['max_number_of_persons'])){
						$numberOfPersons = $item['Event']['min_number_of_persons'];
					}
					if(empty($item['Event']['min_number_of_persons']) && !empty($item['Event']['max_number_of_persons'])){
						$numberOfPersons = $item['Event']['max_number_of_persons'];
					}
					if(!empty($item['Event']['min_number_of_persons']) && !empty($item['Event']['max_number_of_persons'])){
						$numberOfPersons = sprintf('%s - %s', $item['Event']['min_number_of_persons'], $item['Event']['max_number_of_persons']);
					}
				}
				$description = '';
				$description .= $html->tag('span', sprintf('<i class="fa fa-users"></i> %s', __('%s persons', $numberOfPersons)), array('class' => 'event-info'));
				//$description .= $html->tag('span', sprintf('<i class="fa fa-map-marker"></i> %s', 'Caveau de Buman'), array('class' => 'event-info'));
				//$description .= $html->tag('span', sprintf('<i class="fa fa-trophy"></i> %s', "Casino des Sens"), array('class' => 'event-info'));
				$description .= !empty($item['PersonInCharge']) ? $html->tag('span', sprintf('<i class="fa fa-user"></i> %s', $item['PersonInCharge']['full_name']), array('class' => 'event-info')) : '';
				$description .= !empty($item['ContactPeople']) ? $html->tag('span', sprintf('<i class="fa fa-phone"></i> %s', $item['ContactPeople']['full_name']), array('class' => 'event-info')) : '';
				if(!empty($item['SelectedActivity'])){
					foreach($item['SelectedActivity'] as $activity){
						$description .= $html->tag('span', sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('class' => 'btn btn-xs btn-info uppercase event-info'));
					}
				}
				if(empty($item['SelectedActivity']) && !empty($item['SuggestedActivity'])){
					foreach($item['SuggestedActivity'] as $activity){
						$description .= $html->tag('span', sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('class' => 'btn btn-xs btn-info uppercase event-info'));
					}
				}
				if(!empty($item['SelectedFBModule'])){
					foreach($item['SelectedFBModule'] as $activity){
						$description .= $html->tag('span', sprintf('<i class="fa fa-cutlery"></i> %s', $activity['FBModule']['slug']), array('class' => 'btn btn-xs btn-danger uppercase event-info'));
					}
				}
				if(empty($item['SelectedFBModule']) && !empty($item['SuggestedFBModule'])){
					foreach($item['SuggestedFBModule'] as $activity){
						$description .= $html->tag('span', sprintf('<i class="fa fa-cutlery"></i> %s', $activity['FBModule']['slug']), array('class' => 'btn btn-xs btn-danger uppercase event-info'));
					}
				}
				//$description .= $html->tag('span', sprintf('<i class="fa fa-cutlery"></i> %s', "Maier Grill"), array('class' => 'event-info'));
				switch($item['Event']['crm_status']){
					case 'confirmed':
					case 'done':
						$type = 'event';
						$icon = 'icon-calendar';
						$color = 'font-purple';
						$sizes['events'] += 1;
					break;
					case 'refused':
						$type = 'refused';
						$icon = 'icon-calendar';
						$color = 'font-red';
						$sizes['refused'] += 1;
					break;
					case 'null':
						$type = 'null';
						$icon = 'icon-calendar';
						$color = 'font-grey-cascade';
						$sizes['null'] += 1;
					break;
					case 'partner':
						$type = 'partner';
						$icon = 'icon-calendar';
						$color = 'font-grey-gallery';
						$sizes['partner'] += 1;
					break;
					default: // offers
						$type = 'offer';
						$icon = 'icon-calendar';
						$color = 'font-yellow-casablanca';
						$sizes['offers'] += 1;
					break;
				}
				$timeline[$ts][] = array(
				  'type' => $type,
				  'title' => $title,
				  'description' => $description,
				  'date' => $date,
				  'icon' => $icon,
				  'color' => $color,
					'actions' => array(
						array(
							'url' => array('controller' => 'events', 'action' => 'work', $item['Event']['id']),
							'color' => 'grey-gallery',
							'title' => sprintf('<i class="fa fa-edit"></i> %s', __('Edit'))
						)
					)
				);
			}
		} else {
		  $events = array();
		  $offers = array();
		}
		if($company['id'] == 3){ // FESTILOC
		  $stockorders = $this->Client->StockOrder->find('all', array(
			  'conditions' => array(
				  'StockOrder.client_id' => $id
			  ),
			  'contain' => array(
					'Client'
			  )
		  ));
		  foreach($stockorders as $item){
			$ts = strtotime($item['StockOrder']['delivery_date']);
			$title = $item['StockOrder']['order_number'];
			$description = $html->tag('strong', __('Status'));
			$description .= ' ' . $html->tag('span', $statuses[$item['StockOrder']['status']]);
			$description .= $html->tag('br');
			$description .= $html->tag('strong', __('Date of service'));
			$description .= ' ' . $html->tag('span', $time->format('d.m.Y', $item['StockOrder']['service_date_begin']));
			if($item['StockOrder']['service_date_begin'] != $item['StockOrder']['service_date_end']){
			  $description .= ' - ' . $html->tag('span', $time->format('d.m.Y', $item['StockOrder']['service_date_end']));
			}
			$timeline[$ts][] = array(
			  'type' => 'stockorder',
			  'title' => $title,
			  'description' => $description,
			  'date' => $time->format($item['StockOrder']['delivery_date'], '%d %B %Y'),
			  'icon' => 'icon-basket',
			  'color' => 'font-red-intense',
				'actions' => array(
					array(
						'url' => array('controller' => 'stock_orders', 'action' => 'edit', $item['StockOrder']['id']),
						'color' => 'grey-gallery',
						'title' => sprintf('<i class="fa fa-edit"></i> %s', __('Edit'))
					),
					array(
						'url' => array('controller' => 'stock_orders', 'action' => 'invoice', $item['StockOrder']['id'], 'ext' => 'pdf', '?' => 'download=0'),
						'color' => 'grey-gallery',
						'title' => sprintf('<i class="fa fa-print"></i> %s', __('PDF'))
					)
				)
			);
		  }
		} else {
		  $stockorders = array();
		}
		krsort($timeline);
		$this->set(compact('timeline'));
		$this->set(compact('company'));
		$this->set(compact('sticky'));
		$this->set(compact('numberOfNotes'));

		$sizes['notes'] = sizeof($notes);
		$sizes['leads'] = sizeof($leads);
		$sizes['stockorders'] = sizeof($stockorders);
		$sizes['timeline'] = array_sum($sizes);
		$this->set(compact('sizes'));

	}

	public function delete($id = null){
		if($this->request->is('ajax')){
			$this->autoRender = false;
		}
	}

	public function import(){

		function validateDate($date, $format = 'd/m/Y') {
			$d = DateTime::createFromFormat($format, $date);
			return $d && $d->format($format) == $date;
		}

		$this->autoRender = false;
		if(AuthComponent::user('id') == 1){
			$counter = 1;
			$data = explode("\n", file_get_contents(WWW_ROOT . 'files/clients.tsv'));
			unset($data[0]);

			$clients = array();

			foreach($data as $row) {
				$counter++;
				$row = explode("\t", $row);
				$clients[$row[1]][$row[5]] = $row;
			}

			foreach($clients as $clientId => $client){
				foreach($client as $contactId => $contact){

					$this->Client->create();
					$this->Client->ContactPeople->create();

					// check if client has been already imported
					$clientExists = $this->Client->find('first', array(
						'conditions' => array(
							'Client.import_id' => $contact[1]
						),
						'fields' => array('Client.id')
					));

					if(empty($clientExists)){
						$this->Client->create();
						$client = array(
							'Client' => array(
								'name' => trim($contact[2]),
								'address' => $contact[10],
								'zip' => $contact[11],
								'city' => $contact[12],
								'country' => $contact[13],
								'import_id' => $contact[1],
								'created' => date('Y-m-d', strtotime(str_replace('/','-',$contact[0]))),
								'modified' => date('Y-m-d', strtotime(str_replace('/','-',$contact[0])))
							),
							'Company' => array(
								'id' => 2
							)
						);
						$this->Client->save($client);
					} else {
						$this->Client->id = $clientExists['Client']['id'];
					}

					// check if contact person has been already imported
					$contactExists = $this->Client->ContactPeople->find('first', array(
						'conditions' => array(
							'ContactPeople.import_id' => $contact[5]
						),
						'fields' => array('ContactPeople.id')
					));
					if(empty($contactExists)){
						$this->Client->ContactPeople->create();
						$contactPeople = array(
							'ContactPeople' => array(
								'name' => trim($contact[6] . ' ' .$contact[7]),
								'civility' => $contact[3] == 'Monsieur' ? 'mr' : 'ms',
								'import_id' => $contact[5],
								'email' => $contact[9],
								'phone' => $contact[15],
								'status' => $contact[8],
								'client_id' => $this->Client->id,
								'created' => date('Y-m-d', strtotime(str_replace('/','-',$contact[0]))),
								'modified' => date('Y-m-d', strtotime(str_replace('/','-',$contact[0])))
							)
						);
						$this->Client->ContactPeople->save($contactPeople);
					} else {
						$this->Client->ContactPeople->id = $contactExists['ContactPeople']['id'];
					}

					// Get activity by name
					if(!empty($contact[16])){
						$this->loadModel('Activity');
						$activity = $this->Activity->findByName($contact[16]);
						if(!empty($activity)){
							$activityId = $activity['Activity']['id'];
							$activityName = $activity['Activity']['name'];
						}
					} else {
						$activityId = '';
						$activityName = '';
					}

					// check validity of the given date
					if(validateDate($contact[14])){
						$eventDate = date('Y-m-d', strtotime(str_replace('/','-',$contact[14])));
					} else {
						$eventDate = null;
					}

					// set status of event
					if($eventDate > date('Y-m-d')){
						$status = 'not_confirmed';
					} else {
						$status = 'confirmed';
					}

					// get number of persons for the event
					if(!empty($contact[18])){
						$eventNumberOfPersons = $contact[18];
					} else {
						$eventNumberOfPersons = 0;
					}

					// get place of the event
					if(!empty($contact[19])){
						$eventPlace = $contact[19];
					} else {
						$eventPlace = null;
					}

					// get activity type
					if(!empty($contact[20])){
						if($contact[20] == 'Team Building') $activityType = 'teambuilding';
						elseif($contact[20] == "Sortie d'entreprise") $activityType = 'entreprise';
						elseif($contact[20] == "Création d'événements") $activityType = 'event';
						else $activityType = 'other';
					} else {
						$activityType = 'other';
					}

					if(!empty($contact[21])){
						if($contact[21] == 'Anaïs') $respId = 18;
						if($contact[21] == 'Fabienne') $respId = 15;
						if($contact[21] == 'Floriane') $respId = 18;
						if($contact[21] == 'Lorène') $respId = 17;
						if($contact[21] == 'Michèle') $respId = 20;
						if($contact[21] == 'Nadia') $respId = 18;
						if($contact[21] == 'Vincent') $respId = 18;
						if($contact[21] == 'Stéphanie') $respId = 18;
						if($contact[21] == 'Rachael') $respId = 18;
					} else {
						$respId = '';
					}

					$data = array(
						'Event' => array(
							'name' => trim($contact[2]) . '_' . $eventNumberOfPersons . 'PAX_' . $activityName . '_' . $eventPlace,
							'confirmed_date' => $eventDate,
							'min_number_of_persons' => $eventNumberOfPersons,
							'max_number_of_persons' => $eventNumberOfPersons,
							'place' => $eventPlace,
							'desired_activity_type' => $activityType,
							'client_id' => $this->Client->id,
							'contact_people_id' => $this->Client->id,
							'company_id' => 2,
							'user_id' => $respId,
							'resp_id' => $respId,
							'status' => $status
						),
						'Activity' => array(
							0 => array('id' => $activityId)
						)
					);

					$this->loadModel('Event');
					$this->Event->create();
					$this->Event->saveAssociated($data);

				}
			}

			exit;

			foreach($data as $row) {
				$counter++;
				$row = explode("\t", $row);

				debug($row);exit;

				// set person in charge of the event
				if(!empty($row[18])){
					if($row[18] == 'Anaïs') $respId = 18;
					if($row[18] == 'Fabienne') $respId = 15;
					if($row[18] == 'Floriane') $respId = 18;
					if($row[18] == 'Lorène') $respId = 17;
					if($row[18] == 'Michèle') $respId = 20;
					if($row[18] == 'Nadia') $respId = 18;
					if($row[18] == 'Vincent') $respId = 18;
					if($row[18] == 'Stéphanie') $respId = 18;
					if($row[18] == 'Rachael') $respId = 18;
				} else {
					$respId = '';
				}

				// Get activity by name
				if(!empty($row[13])){
					$this->loadModel('Activity');
					$activity = $this->Activity->findByName('Trophée du Lac');
					if(!empty($activity)){
						$activityId = $activity['Activity']['id'];
					}
				} else {
					$activityId = '';
				}

				// check validity of the given date
				if(validateDate($row[11])){
					$eventDate = date('Y-m-d', strtotime(str_replace('/','-',$row[11])));
				} else {
					$eventDate = null;
				}

				// set status of event
				if($eventDate > date('Y-m-d')){
					$status = 'not_confirmed';
				} else {
					$status = 'confirmed';
				}

				// get number of persons for the event
				if(!empty($row[15])){
					$eventNumberOfPersons = $row[15];
				} else {
					$eventNumberOfPersons = 0;
				}

				// get place of the event
				if(!empty($row[1])){
					$eventPlace = $row[16];
				} else {
					$eventPlace = null;
				}

				// set import informations
				$infos = implode("\n", $row);

				$this->Client->create();
				$this->Client->save(array(
					'Client' => array(
						'name' => $row[1],
						'contact_person_name' => $row[4] . ' ' . $row[5],
						'contact_person_phone' => $row[12],
						'contact_person_email' => $row[7],
						'website' => $row[3],
						'address' => $row[8],
						'zip' => $row[9],
						'city' => $row[10],
						'company_id' => 2,
						'civility' => $row[2],
						'formal' => $row[3],
						'date' => $row[0],
						//'import_infos' => $infos
					))
		  		);
				$data = array(
					'Event' => array(
						'name' => $row[1] . '_' . $eventNumberOfPersons . 'PAX_' . $activity['Activity']['name'] . '_' . $eventPlace,
						'confirmed_date' => $eventDate,
						'place' => $eventPlace,
						'client_id' => $this->Client->id,
						'company_id' => 2,
						'user_id' => $respId,
						'status' => $status
					),
					'Activity' => array(
						0 => array('id' => $activityId)
					)
				);

				$this->loadModel('Event');
				$this->Event->create();
				$this->Event->saveAssociated($data);

			}
		} else {
			return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
		}
	}

	public function getContactPeoples(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$data = $this->Client->ContactPeople->find('all', array(
				'conditions' => array(
					'ContactPeople.client_id' => $_POST['client_id']
				),
				'order' => 'ContactPeople.created DESC'
			));
			$contact_peoples = array();
			foreach($data as $k => $contact){
				$contact_peoples[$k]['id'] = $contact['ContactPeople']['id'];
				$contact_peoples[$k]['text'] = $contact['ContactPeople']['full_name_phone_email'];
			}
			$this->response->body(json_encode($contact_peoples));
		}
	}

	public function get() {
		if($this->request->is('ajax') && !empty($_POST['client_id'])){
			$this->autoRender = false;
			$this->Client->contain('Company');
			$client = $this->Client->findById($_POST['client_id']);
			$this->response->body(json_encode($client));
		}
	}

	public function export($companyId = null){
		if(!empty($companyId)){
			$clients = $this->Client->find('all', array(
				'joins' => array(
					array(
						'table' => 'clients_companies',
						'alias' => 'ClientCompany',
						'conditions' => array(
							'ClientCompany.client_id = Client.id'
						)
					),
					array(
						'table' => 'companies',
						'alias' => 'Company',
						'conditions' => array(
							'Company.id = ClientCompany.company_id'
						)
					)
				),
				'conditions' => array(
					'Company.id' => $companyId
				),
				'contain' => array(
					'ContactPeople'
				)
			));
		} else {
			exit;
		}
		debug($clients);
		exit;
	}

	public function cresus(){
		$clients = $this->Client->getAll( array(3) ); // 3 is for Festiloc
		$this->set(compact('clients'));

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->Client->id = $this->request->data['id'];
			if($this->Client->saveField('import_id', $this->request->data['import_id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function turnover(){
	  $clients = $this->Client->find('all', array(
		'contain' => array('Company'),
		'conditions' => array(
		  'Client.name <>' => ''
		),
		'order' => 'Client.name'
	  ));
	  $this->set(compact('clients'));

	  if($this->request->is('ajax')){
		  $this->autoRender = false;
		  $this->Client->id = $this->request->data['id'];
		  if($this->Client->saveField('2015_turnover', $this->request->data['turnover'])){
			  $this->response->body(json_encode(array('success' => 1)));
		  } else {
			  $this->response->body(json_encode(array('success' => 0)));
		  }
	  }
	}

	public function prepareCSV(){
	  $data = explode("\r\n", file_get_contents(WWW_ROOT . 'files/festiloc_factures2015.txt'));
	  $clients = array();
	  unset($data[0]);
	  foreach($data as $row){
		$cells = explode("\t", $row);
		$client = str_replace('"', '', trim($cells[1]));
		$client = explode('|', $client);
		array_push($client, $cells[3]);
		$key = str_replace(' ', '', implode('', $client));
		if(!empty($clients[$key])){
		  $total = trim($clients[$key]['total']);
		} else {
		  $total = 0;
		}
		$total += trim($cells[2]);
		$clients[$key]['total'] = $total;
		$clients[$key]['client'] = $client;
	  }
	  $csv = "";
	  $i = 0;
	  foreach($clients as $item){
		$csv .= $item['client'][0] . ";" . $item['total'];
		$csv .= ";";
		for($i=0; $i < 10; $i++){
		  $csv .= (!empty($item['client'][$i])) ? $item['client'][$i] : '' ;
		  $csv .= ";";
		}
		$csv .= "\n";
	  }
	  file_put_contents(WWW_ROOT . 'files/festiloc2015.csv', $csv);
	  exit;
	}

	public function prepareCSV1(){
	  $data = explode("\r\n", file_get_contents(WWW_ROOT . 'files/Toutesfactures2015.txt'));
	  $clients = array();
	  unset($data[0]);
	  foreach($data as $row){
		$cells = explode("\t", $row);
		$client = str_replace('"', '', trim($cells[0]));
		$client = explode('|', $client);
		array_push($client, $cells[1]);
		$articles = explode("\\", $cells[4]);
		$key = str_replace(' ', '', implode('', $client));
		if(!empty($clients[$key])){
		  $total = trim($clients[$key]['total']);
		  $art = array_merge($clients[$key]['articles'], $articles);
		} else {
		  $total = 0;
		  $art = $articles;
		}
		$art = array_unique($art);
		$total += trim($cells[2]);
		if(sizeof($articles) == 1 && $articles[0] == 30){
		  continue;
		} else {
		  $clients[$key]['total'] = $total;
		  $clients[$key]['client'] = $client;
		  $clients[$key]['articles'] = $art;
		}

	  }
	  $csv = "";
	  $i = 0;
	  foreach($clients as $item){
		foreach($item['articles'] as $article){
		  if($article == 200){
			$csv .= 'UG;' . $item['client'][0] . ";" . $item['total'];
			$csv .= ";";
			for($i=0; $i < 10; $i++){
			  $csv .= (!empty($item['client'][$i])) ? $item['client'][$i] : '' ;
			  $csv .= ";";
			}
			$csv .= "\n";
		  }
		  if($article == 100 OR $article == 150 OR $article == 300){
			$csv .= 'UBIC;' . $item['client'][0] . ";" . $item['total'];
			$csv .= ";";
			for($i=0; $i < 10; $i++){
			  $csv .= (!empty($item['client'][$i])) ? $item['client'][$i] : '' ;
			  $csv .= ";";
			}
			$csv .= "\n";
		  }
		}

	  }
	  file_put_contents(WWW_ROOT . 'files/ubic+ug2015.csv', $csv);
	  exit;
	}

	public function json(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$clients = $this->Client->find('list', array(
	    	'joins' => array(
	    		array(
	          'table' => 'clients_companies',
	          'alias' => 'ClientCompany',
	          'conditions' => array(
	            'ClientCompany.client_id = Client.id'
	          )
	        ),
	        array(
	          'table' => 'companies',
	          'alias' => 'Company',
	          'conditions' => array(
	            'Company.id = ClientCompany.company_id'
	          )
	        )
	      ),
	      'conditions' => array(
	        'Company.id' => $this->request->data['company_id']
	      ),
	      'order' => 'Client.name',
				'fields' => array('Client.id', 'Client.name_address')
	    ));
			$this->response->body(json_encode($clients));
		}
	}

}
