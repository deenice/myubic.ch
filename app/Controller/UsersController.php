<?php

App::uses('CakeEmail', 'Network/Email');

class UsersController extends AppController {

	public $helpers = array('Availability', 'Languages', 'Talents', 'ImageSize');

	public $components = [
		'DataTable.DataTable' => [
			'FixedUser' => [
				'model' => 'User',
				'columns' => [
					'portrait' => array(
						'bSortable' => false,
						'bSearchable' => false,
						'useField' => false,
						'label' => false
					),
					'full_name',
					'email',
					'Actions' => null,
				],
				'conditions' => ['User.role' => 'fixed'],
				'contain' => ['Portrait'],
				'autoData' => false
			],
			'Extras' => [
				'model' => 'User',
				'columns' => [
					'portrait' => array(
						'bSortable' => false,
						'bSearchable' => false,
						'useField' => false,
						'label' => false
					),
					'full_name' => array(
						'bSearchable' => 'customSearchUser'
					),
					'email' => array(
						'bSearchable' => 'customSearchUser'
					),
					'phone' => array(
						'bSearchable' => false,
						'bSortable' => false
					),
					'section' => array(
						'useField' => false,
						'bSearchable' => 'customSearchUser'
					),
					'Actions' => null,
				],
				'joins' => array(
					array(
						'table' => 'tags_users',
						'alias' => 'tu',
						'conditions' => 'tu.user_id = User.id',
						'type' => 'left'
					),
					array(
						'table' => 'tags',
						'alias' => 't',
						'conditions' => 't.id = tu.tag_id',
						'type' => 'left'
					)
				),
				'conditions' => array(
					'User.role' => 'temporary'
				),
				'fields' => array('t.value'),
				'group' => 'User.id',
				'contain' => ['Portrait', 'Section', 'TagUser'],
				'autoData' => false
			],
			'Candidates' => [
				'model' => 'User',
				'columns' => [
					'portrait' => array(
						'bSortable' => false,
						'bSearchable' => false,
						'useField' => false,
						'label' => false
					),
					'full_name',
					'email',
					'phone',
					'section' => null,
					'remarks',
					'Actions' => null,
				],
				'conditions' => ['User.role' => 'candidate'],
				'contain' => ['Portrait', 'Section'],
				'autoData' => false
			],
			'Retired' => [
				'model' => 'User',
				'columns' => [
					'full_name',
					'Actions' => null,
				],
				'conditions' => ['User.role' => 'retired'],
				'fields' => array('User.id'),
				'autoData' => false
			],
			'Unsuitable' => [
				'model' => 'User',
				'columns' => [
					'full_name',
					'Actions' => null,
				],
				'conditions' => ['User.role' => 'unsuitable'],
				'fields' => array('User.id'),
				'autoData' => false
			],
			'Talents' => [
				'model' => 'User',
				'columns' => [
					'portrait' => array(
						'bSortable' => false,
						'bSearchable' => false,
						'useField' => false,
						'label' => false
					),
					'Talent.name' => array(
						'useField' => false
					),
					'full_name'
				],
				'joins' => array(
					array(
						'table' => 'tags_users',
						'alias' => 'TagsUsers',
						'type' => 'INNER',
						'conditions' => array(
							'TagsUsers.user_id = User.id'
						)
					),
					array(
						'table' => 'tags',
						'alias' => 'Tag',
						'type' => 'INNER',
						'conditions' => array(
							'TagsUsers.tag_id = Tag.id'
						)
					)
				),
				'contain' => array(
					'Portrait'
				),
				'conditions' => array(
					'Tag.category' => 'talent'
				),
				'fields' => array(
					'User.full_name', 'User.id', 'Tag.value'
				),
				'order' => 'Tag.value',
				'autoData' => false
			],
			'Frontend' => [
				'model' => 'User',
				'columns' => [
					'portrait' => array(
						'bSortable' => false,
						'bSearchable' => false,
						'useField' => false,
						'label' => false
					),
					'full_name',
					'email',
					'phone' => array(
						'bSortable' => false
					),
					'section' => array(
						'useField' => false,
						'bSearchable' => true
					),
					'Actions' => null,
				],
				'conditions' => ['User.role' => 'temporary', 'User.frontend_animation' => 1],
				'contain' => ['Portrait', 'Section', 'TagUser'],
				'autoData' => false
			],
		],
	];

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['FixedUser']['columns']['full_name']['label'] = __('Name');
		$this->DataTable->settings['FixedUser']['columns']['portrait']['class'] = 'portrait';

		$this->DataTable->settings['Extras']['columns']['full_name']['label'] = __('Name');
		$this->DataTable->settings['Extras']['columns']['phone']['label'] = __('Phone');
		$this->DataTable->settings['Extras']['columns']['portrait']['class'] = 'portrait';

		$this->DataTable->settings['Retired']['columns']['full_name']['label'] = __('Name');

		$this->DataTable->settings['Unsuitable']['columns']['full_name']['label'] = __('Name');

		$this->DataTable->settings['Candidates']['columns']['full_name']['label'] = __('Name');
		$this->DataTable->settings['Candidates']['columns']['phone']['label'] = __('Phone');
		$this->DataTable->settings['Candidates']['columns']['remarks']['label'] = __('Remarks');
		$this->DataTable->settings['Candidates']['columns']['portrait']['class'] = 'portrait';

		$this->DataTable->settings['Talents']['columns']['full_name']['label'] = __('Name');
		$this->DataTable->settings['Talents']['columns']['Talent.name']['label'] = __('Talent');
		$this->DataTable->settings['Talents']['columns']['portrait']['class'] = 'portrait';
	}

	public function index() {
		// Only required if using the helper
		$this->DataTable->setViewVar(array('FixedUser', 'Extras', 'Candidates', 'Retired', 'Unsuitable' /*'ToEvaluate'*/, 'Talents'));
	}

	public function frontend_animation_extras() {
		// Only required if using the helper
		$this->DataTable->setViewVar(array('Frontend'));
	}

	public function index_json() {

		$view = new View($this);
		$html = $view->loadHelper('Html');
		$time = $view->loadHelper('Time');
		$imgSize = $view->loadHelper('ImageSize');

		$role = $this->request->query['role'];

		if($role == 'fixed'){
			$users = $this->User->find('all', array(
				'contain' => 'Portrait',
				'conditions' => array(
					'role' => 'fixed'
				),
				'order' => 'User.first_name',
				'cache' => 'fixed_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'temporary'){
			$users = $this->User->find('all', array(
				'contain' => array('Portrait', 'Section'),
				'conditions' => array(
					'role' => 'temporary'
				),
				'cache' => 'temporary_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'candidate'){
			$users = $this->User->find('all', array(
				'contain' => array('Portrait', 'Section'),
				'conditions' => array(
					'role' => 'candidate'
				),
				'cache' => 'candidate_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'unsuitable'){
			$users = $this->User->find('all', array(
				'contain' => array('Portrait'),
				'conditions' => array(
					'role' => 'unsuitable'
				),
				'cache' => 'unsuitable_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'retired'){
			$users = $this->User->find('all', array(
				'contain' => array('Portrait'),
				'conditions' => array(
					'role' => 'retired'
				),
				'cache' => 'retired_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'to_evaluate'){
			$users = $this->User->Job->find('all', array(
				'joins' => array(
					array(
						'table' => 'events',
						'alias' => 'Event1',
						'type' => 'INNER',
						'conditions' => array(
							'Event1.id = Job.event_id'
						)
					),
					array(
						'table' => 'users',
						'alias' => 'User1',
						'type' => 'INNER',
						'conditions' => array(
							'User1.id = Job.user_id'
						)
					),
					array(
						'table' => 'competences',
						'alias' => 'Competence',
						'type' => 'INNER',
						'conditions' => array(
							'User1.id = Competence.user_id',
							'Competence.sector = Job.sector',
							'Competence.job = Job.job'
						)
					)
				),
				'contain' => array(
					'User' => array('Portrait'),
					'Event' => array('Client')
				),
				'conditions' => array(
					'Job.hierarchy <' => 4,
					'Job.evaluation' => 'none',
					'Competence.hierarchy <' => 4,
					'Job.user_id <>' => null,
					'Event1.confirmed_date <' => date('Y-m-d')
				),
				'order' => 'Event1.confirmed_date',
				'group' => 'User.id',
				'cache' => 'to_evaluate_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}

		if($role == 'talents'){
			$users = $this->User->find('all', array(
				'joins' => array(
					array(
						'table' => 'tags_users',
						'alias' => 'TagsUsers',
						'type' => 'INNER',
						'conditions' => array(
							'TagsUsers.user_id = User.id'
						)
					),
					array(
						'table' => 'tags',
						'alias' => 'Tag',
						'type' => 'INNER',
						'conditions' => array(
							'TagsUsers.tag_id = Tag.id'
						)
					)
				),
				'contain' => array(
					'Portrait'
				),
				'conditions' => array(
					'Tag.category' => 'talent'
				),
				'fields' => array(
					'User.full_name', 'User.id', 'Tag.value'
				),
				'order' => 'Tag.value',
				'cache' => 'talents_' . date('dmY'),
				'cacheConfig' => 'short'
			));
		}


		//$data = Cache::read('users_json_' . $role . date('dmY'), 'long');
		$data = array();
		if(empty($data)){
			foreach($users as $k => $user){
				if($role == 'talents'){
					$data[$role][$k]['User']['portrait'] = empty($user['Portrait'][0]['id']) ? '' : $html->image(
							$imgSize->crop($user['Portrait'][0]['url'], 60, 60));
					$data[$role][$k]['User']['full_name'] = $user['User']['full_name'];
					$data[$role][$k]['Talent']['name'] = $user['Tag']['value'];
				} elseif($role == 'to_evaluate'){
					$data[$role][$k]['User']['portrait'] = empty($user['User']['Portrait'][0]['id']) ? '' : $html->image(
							$imgSize->crop($user['User']['Portrait'][0]['url'], 60, 60));
					$data[$role][$k]['User']['full_name'] = $user['User']['full_name'];
					$data[$role][$k]['Job']['name'] = $user['Job']['name'];
					$data[$role][$k]['Event']['name'] = $user['Event']['name'];
					$data[$role][$k]['Event']['confirmed_date'] = $time->format($user['Event']['confirmed_date'], '%d %B %Y');
					$data[$role][$k]['Client']['name'] = $user['Event']['Client']['name'];
					$data[$role][$k]['actions'][] = $html->link(
						'<i class="fa fa-thumbs-o-up"></i> ' . __("Rise?"),
						array('controller' => 'users', 'action' => 'rise', $user['User']['id'], $user['Job']['id']),
						array('class' => 'btn default btn-xs', 'escape' => false)
					);
					$data[$role][$k]['actions'][] = $html->link(
						'<i class="fa fa-times"></i> ' . __("Keep same hierarchy"),
						array('controller' => 'users', 'action' => 'rise', $user['User']['id'], $user['Job']['id'], 0),
						array('class' => 'btn default btn-xs', 'escape' => false)
					);

				} else {
					$sections = array();
					$data[$role][$k]['User']['portrait'] = empty($user['Portrait'][0]['id']) ? '' : $html->image(
							$imgSize->crop($user['Portrait'][0]['url'], 60, 60));
					$data[$role][$k]['User']['full_name'] = $user['User']['full_name'];
					$data[$role][$k]['User']['email'] = empty($user['User']['email']) ? '' : $user['User']['email'];
					$data[$role][$k]['User']['phone'] = empty($user['User']['phone']) ? '' : $user['User']['phone'];
					$data[$role][$k]['User']['remarks'] = empty($user['User']['remarks']) ? '' : $user['User']['remarks'];
					if(!empty($user['Section'])){
						foreach($user['Section'] as $section){
							$sections[] = $section['value'];
						}
					}
					$data[$role][$k]['Section'] = $sections;
					$data[$role][$k]['actions'][] = $html->link(
						'<i class="fa fa-search"></i> ' . __("View"),
						array('controller' => 'users', 'action' => 'view', $user['User']['id']),
						array('class' => 'btn default btn-xs', 'escape' => false)
					);
					$data[$role][$k]['actions'][] = $html->link(
						'<i class="fa fa-edit"></i> ' . __("Edit"),
						array('controller' => 'users', 'action' => 'edit', $user['User']['id']),
						array('class' => 'btn btn-warning btn-xs', 'escape' => false)
					);
					$data[$role][$k]['actions'][] = $html->link(
						'<i class="fa fa-trash-o"></i> ' . __("Delete"),
						array('controller' => 'users', 'action' => 'delete', $user['User']['id']),
						array('class' => 'btn btn-danger btn-xs', 'escape' => false)
					);
				}
			}
		}
		//Cache::write('users_json_' . $role . date('dmY'), 'long');

		$this->set(compact('data'));
		$this->set('_serialize', array('data'));
	}

	public function add() {

		$talents = $this->User->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'talent')));
		$this->set(compact('talents'));

		$sections = $this->User->Tag->find('list', array(
			'conditions' => array(
				'Tag.category' => 'section'
			),
			'fields' => array('Tag.id', 'Tag.value'),
			'order' => 'Tag.id'
		));
		$this->set(compact('sections'));

		//$groups = $this->User->Group->find('list');
		//$this->set(compact('groups'));

		if ($this->request->is('post') OR $this->request->is('put')) {
			$this->User->create();
			if(!empty($this->request->data['User']['thumbnail']['tmp_name'])){
				$this->User->Document->saveFile($this->request->data['User']['thumbnail'], $this->User->nextId(), array('users','thumbnail'), 'user_thumbnail', Configure::read('Documents.Images.allowedExtensions'));
			}
			if(!empty($this->request->data['User']['date_of_birth'])){
				$this->request->data['User']['date_of_birth'] = date('Y-m-d', strtotime($this->request->data['User']['date_of_birth']));
			}
			if(!empty($this->request->data['User']['start_of_collaboration'])){
				$this->request->data['User']['start_of_collaboration'] = date('Y-m-d', strtotime($this->request->data['User']['start_of_collaboration']));
			}
			if(!empty($this->request->data['User']['end_of_collaboration'])){
				$this->request->data['User']['end_of_collaboration'] = date('Y-m-d', strtotime($this->request->data['User']['end_of_collaboration']));
			}
			if(!empty($this->request->data['User']['wedding_date'])){
				$this->request->data['User']['wedding_date'] = date('Y-m-d', strtotime($this->request->data['User']['wedding_date']));
			}
			if(!empty($this->request->data['User']['other_licences'])){
				$this->request->data['User']['other_licences'] = implode(',',$this->request->data['User']['other_licences']);
			}
			if(!empty($this->request->data['User']['companies'])){
				foreach($this->request->data['User']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}
			$this->request->data['User']['username'] = Inflector::slug($this->request->data['User']['username'], '');

			if ($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['User']['first_name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->User->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('User has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function edit($id = null){

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid User'));
		}

		$thumbnail = $this->User->Document->getFiles($id ,'user_thumbnail');
		$this->set(compact('thumbnail'));

		$sections = $this->User->Tag->find('list', array(
			'conditions' => array(
				'Tag.category' => 'section'
			),
			'fields' => array('Tag.id', 'Tag.value'),
			'order' => 'Tag.id'
		));
		$this->set(compact('sections'));
		$contain = array(
			'Tag',
			'Competence',
			'Module',
			'Company',
			'ScheduleDay',
			'MondayAvailability',
			'TuesdayAvailability',
			'WednesdayAvailability',
			'ThursdayAvailability',
			'FridayAvailability',
			'SaturdayAvailability',
			'SundayAvailability'
		);
		$user = $this->User->find('first', array(
			'contain' => $contain,
			'conditions' => array(
				'id' => $id
			)
		));
		if(empty($user['ScheduleDay'])){
			$this->User->id = $id;
			$this->User->updateScheduleDays();
			$user = $this->User->find('first', array(
				'contain' => $contain,
				'conditions' => array(
					'id' => $id
				)
			));
		}
		if(!empty($user['Tag'])){
			foreach($user['Tag'] as $tag){
				$tags[$tag['category']][] = $tag['id'];
			}
		}


		$languages = $this->User->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'language')));
		$talents = $this->User->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'talent')));
		$availabilities = $this->User->Tag->find('list', array('fields' => 'value', 'conditions' => array('category' => 'availability')));
		$this->set(compact('languages'));
		$this->set(compact('talents'));
		$this->set(compact('tags'));
		$this->set(compact('availabilities'));

		$this->loadModel('Module');
		$this->set('modules', $this->Module->find('all'), array(
			'contain' => array('ModuleCategory', 'ModuleSubcategory')
		));

		$this->loadModel('Activity');
		$ubicActivities = $this->Activity->find('list', array('conditions' => array('company_id' => 2), 'order' => 'name'));
		$ugActivities = $this->Activity->find('list', array('conditions' => array('company_id' => 6), 'order' => 'name'));
		$this->set('activities', array('ubic'=> $ubicActivities, 'ug' => $ugActivities, 'all' => $ubicActivities+$ugActivities));

		$this->loadModel('FBModule');
		$this->set('fb_modules', $this->FBModule->find('list', array(
			'conditions' => array(
				'with_competences' => 1
			),
			'order' => 'name'
		)));

		$this->set('logistics_jobs', Configure::read('Competences.logistics_jobs'));
		$this->set('other_jobs', Configure::read('Competences.other_jobs'));
		$this->set('fb_jobs_more', Configure::read('Competences.fb_jobs_more'));

		$competences = $this->User->Competence->find('all', array(
			'conditions' => array(
				'user_id' => $id
			)
		));
		$userCompetences = array();
		if(!empty($competences)){
			foreach($competences as $competence){
				if($competence['Competence']['sector'] == 'animation'){
					$userCompetences[$competence['Competence']['sector']][$competence['Competence']['job']][$competence['Competence']['hierarchy']][$competence['Competence']['activity_id']] = array(
						'id' => $competence['Competence']['id']
					);
				}
				elseif($competence['Competence']['sector'] == 'fb' && !empty($competence['Competence']['fb_module_id'])){
					$userCompetences[$competence['Competence']['sector']][$competence['Competence']['job']][$competence['Competence']['fb_module_id']] = array(
						'id' => $competence['Competence']['id']
					);
				}
				elseif($competence['Competence']['sector'] == 'logistics' || $competence['Competence']['sector'] == 'other'){
					$userCompetences[$competence['Competence']['sector']][$competence['Competence']['job']][$competence['Competence']['hierarchy']] = array(
						'id' => $competence['Competence']['id']
					);
				}

			}
			//debug($userCompetences);exit;
		}
		$this->set(compact('userCompetences'));

		$companies = $this->User->Company->find('list');
		$this->set(compact('companies'));

		$userCompanies = $this->User->Company->find('list', array(
			'joins' => array(
				array(
					'table' => 'companies_users',
					'alias' => 'CompanyUser',
					'type' => 'INNER',
					'conditions' => array(
						'CompanyUser.company_id = Company.id'
					)
				),
				array(
					'table' => 'users',
					'alias' => 'User',
					'type' => 'INNER',
					'conditions' => array(
						'User.id = CompanyUser.user_id'
					)
				)
			),
			'conditions' => array(
				'User.id' => $id
			),
			'fields' => array('Company.id')
		));
		$this->set(compact('userCompanies'));
		//$groups = $this->User->Group->find('list');
		//$this->set(compact('groups'));

		if ($this->request->is('post') || $this->request->is('put')) {

			if(!empty($this->request->data['User']['availabilities'])){
				$this->request->data['User']['availabilities'] = implode(',', $this->request->data['User']['availabilities']);
			}
			if(!empty($this->request->data['User']['thumbnail']['tmp_name'])){
				$this->User->Document->saveFile($this->request->data['User']['thumbnail'], $id, array('users','thumbnail'), 'user_thumbnail', Configure::read('Documents.Images.allowedExtensions'));
			}
			if(!empty($this->request->data['User']['password_update'])){
				if(!empty($this->request->data['User']['password_verification'])){
					$passwordVerification = Security::hash($this->request->data['User']['password_verification'], null, true);
					if($user['User']['password'] == $passwordVerification){
						$this->request->data['User']['password'] = $this->request->data['User']['password_update'];
					} else {
						$this->Session->setFlash(__('Please verify your password'), 'alert', array('type' => 'warning'));
						if(in_array(AuthComponent::user('role'), array('temporary', 'candidate'))){
							$this->redirect(array('action' => 'profile', 'controller' => 'me'));
						} else {
							$this->redirect(array('action' => 'edit', $id));
						}
					}
				} else {
					$this->Session->setFlash(__('Please enter your actual password'), 'alert', array('type' => 'warning'));
					if(AuthComponent::user('role') == 'temporary'){
						$this->redirect(array('action' => 'profile', 'controller' => 'me'));
					} else {
						$this->redirect(array('action' => 'edit', $id));
					}
				}
			}
			if(!empty($this->request->data['User']['date_of_birth'])){
				$this->request->data['User']['date_of_birth'] = date('Y-m-d', strtotime($this->request->data['User']['date_of_birth']));
			}
			if(!empty($this->request->data['User']['start_of_collaboration'])){
				$this->request->data['User']['start_of_collaboration'] = date('Y-m-d', strtotime($this->request->data['User']['start_of_collaboration']));
			}
			if(!empty($this->request->data['User']['end_of_collaboration'])){
				$this->request->data['User']['end_of_collaboration'] = date('Y-m-d', strtotime($this->request->data['User']['end_of_collaboration']));
			}
			if(!empty($this->request->data['User']['wedding_date'])){
				$this->request->data['User']['wedding_date'] = date('Y-m-d', strtotime($this->request->data['User']['wedding_date']));
			}
			if(!empty($this->request->data['User']['other_licences'])){
				$this->request->data['User']['other_licences'] = implode(',',$this->request->data['User']['other_licences']);
			}
			if(!empty($this->request->data['User']['companies'])){
				foreach($this->request->data['User']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}
			if($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('%s has been saved.', $user['User']['full_name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->User->id));
				}
				elseif($this->request->data['destination'] == 'profile'){
					$this->Session->setFlash(__('Your profile has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'profile', 'controller' => 'me'));
				}
				elseif($this->request->data['destination'] == 'dashboard'){
					$this->Session->setFlash(__('Your profile has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'dashboard', 'controller' => 'me'));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('User has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return false;
			}
		} else {
			if(!empty($user['User']['date_of_birth'])){
				$user['User']['date_of_birth'] = date('d-m-Y', strtotime($user['User']['date_of_birth']));
			}
			if(!empty($user['User']['start_of_collaboration'])){
				$user['User']['start_of_collaboration'] = date('d-m-Y', strtotime($user['User']['start_of_collaboration']));
			}
			if(!empty($user['User']['end_of_collaboration'])){
				$user['User']['end_of_collaboration'] = date('d-m-Y', strtotime($user['User']['end_of_collaboration']));
			}
			if(!empty($user['User']['wedding_date'])){
				$user['User']['wedding_date'] = date('d-m-Y', strtotime($user['User']['wedding_date']));
			}
			$this->set(compact('user'));
			$this->request->data = $user;
			unset($this->request->data['User']['password']);
		}
	}

	public function view($id = null){
		$user = $this->User->find('first', array(
			'contain' => array(
				'Availabilities',
				'Talents',
				'Competence'
			),
			'conditions' => array('User.id' => $id)
		));
		$this->set(compact('user'));

		$thumbnail = $this->User->Document->getFiles($id ,'user_thumbnail');
		$this->set(compact('thumbnail'));
	}

	public function register(){
		$this->layout = 'login';
		if(!empty($this->request->data)){
			$data = $this->request->data;
			$data['User']['id'] = null;
			$data['User']['activation_token'] = md5(uniqid(mt_rand(), true));
			if($this->User->save($data, true, array('username', 'password', 'email', 'first_name', 'last_name', 'activation_token'))){
				$server = Configure::read('Debug.server');
				if($server == 'local'){
					$to = 'denis@une-bonne-idee.ch';
				} elseif($server == 'test'){
					$to = 'dev@myubic.ch';
				} elseif($server == 'prod'){
					$to = $data['User']['email'];
				}
				$link = array('controller' => 'users', 'action' => 'activate', $this->User->id . '-' . $data['User']['activation_token']);
				$mail = new CakeEmail();
				$mail->from('denis@une-bonne-idee.ch')
					 ->to($to)
					 ->subject(__('Registration on myubic'))
					 ->emailFormat('html')
					 ->template('register')
					 ->viewVars(array('link' => $link))
					 ->send();
				$this->request->data = array();
				$this->Session->setFlash(__('Your account has been created. You need to activate your account first.'), 'alert', array('type' => 'info'));
			} else {
				$this->Session->setFlash(__('Please check your errors.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function activate($token){
		$token = explode('-', $token);
		$user = $this->User->find('first', array(
			'conditions' => array(
				'id' => $token[0],
				'User.activation_token' => $token[1],
				'active' => 0
			)
		));
		if(!empty($user)){
			$server = Configure::read('Debug.server');
			if($server == 'local'){
				$to = 'denis@une-bonne-idee.ch';
			} elseif($server == 'test'){
				$to = 'dev@myubic.ch';
			} elseif($server == 'prod'){
				$to = $user['User']['email'];
			}
			$this->User->id = $token[0];
			$this->User->saveField('active', 1);
			$this->User->saveField('activation_token', '');
			$this->Session->setFlash(__('Your account has been correctly activated.'), 'alert', array('type' => 'success'));
			$this->Auth->login($user['User']);
			$link = array('controller' => 'users', 'action' => 'edit', $user['User']['id']);
			$mail = new CakeEmail();
			$mail->from('denis@une-bonne-idee.ch')
				 ->to($to)
				 ->subject(__('Activation complete on myubic'))
				 ->emailFormat('html')
				 ->template('activation')
				 ->viewVars(array('link' => $link))
				 ->send();
			return $this->redirect(array('action' => 'index', 'controller' => 'dashboard'));
		} else {
			$this->Session->setFlash(__('This activation link is not valid.'), 'alert', array('type' => 'danger'));
			return $this->redirect(array('action' => 'login', 'controller' => 'users'));
		}
	}

	public function login(){
		$this->layout = 'login';
		$this->User->contain('Portrait');
		if(!empty($this->request->data) && $this->request->is('post')){
			if($this->Auth->login()){
				$this->User->id = $this->Auth->user('id');
				if ($this->request->data['User']['remember_me'] == 1) {
          // remove "remember me checkbox"
          unset($this->request->data['User']['remember_me']);
          // hash the user's password
          $this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
          // write the cookie
          $this->Cookie->write('remember_me_cookie', $this->request->data['User'], true, '2 weeks');
        }
				$this->Session->setFlash(__('You have been correctly logged in.'), 'alert', array('type' => 'success'));
				if($this->User->getRole() == 'temporary'){
					return $this->redirect(array('controller' => 'me', 'action' =>'dashboard'));
				} elseif($this->User->getRole() == 'fixed') {
					return $this->redirect($this->Auth->redirectUrl());
				} else {
					$this->Session->setFlash(__('Impossible to log you in. Please try again.'), 'alert', array('type' => 'danger'));
					return $this->redirect(array('controller' => 'users', 'action' => 'login'));
				}
			} else {
				$this->Session->setFlash(__('Impossible to log you in. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('controller' => 'users', 'action' => 'login'));
			}
		}
	}

	public function logout(){
		if($this->Auth->logout()){
			$this->Cookie->delete('remember_me_cookie');
			$this->Session->setFlash(__('You have been correctly logged out.'), 'alert', array('type' => 'success'));
			return $this->redirect(array('controller' => 'users', 'action' => 'login'));
		}
	}

	public function import(){
		exit;
		$this->autoRender = false;
		if(AuthComponent::user('id') == 1){
			$counter = 1;
			$data = explode("\n", file_get_contents(WWW_ROOT . 'files/extras1.csv'));
			unset($data[0]);
			foreach($data as $row) {
				$counter++;
				$row = explode(',', $row);
				if(!empty($row[9])){
					if(strtolower($row[9]) == 'cg') $validatedBy = 14; //Claude
					if(strtolower($row[9]) == 'lc') $validatedBy = 17; //Lorène
					if(strtolower($row[9]) == 'fp') $validatedBy = 18; //Floriane
					if(strtolower($row[9]) == 'rk') $validatedBy = 18; //Floriane
					if(strtolower($row[9]) == 'jg') $validatedBy = 18; //Floriane
					if(strtolower($row[9]) == 'aa') $validatedBy = 24; //Aline
					if(strtolower($row[9]) == 'mg') $validatedBy = 25; //Benoit
				}
				$data = array(
					'User' => array(
						'username' => strtolower(Inflector::slug($row[2].$row[1],'')),
						'password' => Security::hash('123456', null, true),
						'first_name' => $row[2],
						'last_name' => $row[1],
						'city' => $row[3],
						'phone' => $row[4],
						'email' => $row[5],
						'date_of_birth' => date('Y-m-d', strtotime($row[6])),
						'role' => 'temporary',
						'car' => !empty($row[7]) ? 1 : 0,
						'driving_licence' => !empty($row[8]) ? 1 : 0,
						'remarks' => !empty($row[17]) ? $row[17] : '',
						'validated_by' => $validatedBy
					)
				);
				//debug($data);exit;

				$this->User->create();
				if($data['User']['username'] != 'alineandrey'){
					if($this->User->save($data)){
						debug($counter);
					} else {
						debug($this->User->invalidFields());
					}
				}
			}
		} else {
			return $this->redirect(array('controller' => 'users', 'action' => 'index'));
		}
	}

	public function recruit($id = null, $relaunch = 0){
		$this->autoRender = false;
		if($this->request->is('ajax')){

			$url = 'http://feeds.feedburner.com/1-jour?format=xml';
			$xml = simplexml_load_file($url);
			$json = json_encode($xml);
			$array = json_decode($json,TRUE);

			$randomIndex = rand(0,count($array['channel']['item'])-1);
			$title = $array['channel']['item'][$randomIndex]['title'];
			$link = $array['channel']['item'][$randomIndex]['link'];
			$title = explode(':', $title);
			$date = explode(' ',trim($title[0]));
			$date = $date[0] . ' ' . $date[1];
			$title = ucfirst(trim($title[1]));

			$this->loadModel('Search');
			$this->loadModel('Place');
			$this->loadModel('Activity');
			$this->loadModel('Job');
			App::uses('CakeTime', 'Utility');

			$this->Search->Event->contain('Client');

			$search = $this->Search->findById($_POST['search_id']);
			$user = $this->User->findById($_POST['user_id']);
			$event = $this->Search->Event->findById($search['Search']['event_id']);
			$place = (!empty($event['Event']['place_id'])) ? $this->Place->findById($event['Event']['place_id']) : '';
			$eventDate = CakeTime::format($event['Event']['confirmed_date'], '%A %d %B %Y');

			if(!empty($event['Event']['min_number_of_persons'])){
				$eventNumberOfPersons = $event['Event']['min_number_of_persons'];
			}
			if(!empty($event['Event']['max_number_of_persons']) AND !empty($event['Event']['min_number_of_persons'])){
				$eventNumberOfPersons .= ' - ' . $event['Event']['max_number_of_persons'];
			} elseif(!empty($event['Event']['max_number_of_persons']) AND empty($event['Event']['min_number_of_persons'])){
				$eventNumberOfPersons = $event['Event']['max_number_of_persons'];
			}
			if(empty($event['Event']['max_number_of_persons']) AND empty($event['Event']['min_number_of_persons'])){
				$eventNumberOfPersons = '';
			}


			$competencesSectors = Configure::read('Competences.sectors');
			$competencesJobs = array_merge(
				Configure::read('Competences.fb_jobs'),
				Configure::read('Competences.animation_jobs'),
				Configure::read('Competences.logistics_jobs')
			);
			$competencesHierarchies = Configure::read('Competences.hierarchies');

			$this->Job->contain(array(
				'Activity'
			));
			$job = $this->Job->findById($_POST['job_id']);

			if($user['User']['more_than_250_hours'] == 1){
				$salary = Configure::read('Users.minimal_salary');
			} else {
				$salary = $job['Job']['salary'];
			}

			if($relaunch){
				$subject = 'Relance poste "extra" - ' . $eventDate;
				$mailTemplate = 'recruit_relaunch';
				$status = 'relaunched';
			} else {
				$subject = 'Proposition poste "extra" - ' . $eventDate;
				$mailTemplate = 'recruit';
				$status = 'waiting';
			}

			$server = Configure::read('Debug.server');
			if($server == 'local'){
				$to = 'denis@une-bonne-idee.ch';
			} elseif($server == 'test'){
				$to = 'dev@myubic.ch';
			} elseif($server == 'prod'){
				$to = $user['User']['email'];
			}

			$mail = new CakeEmail();
			$mail->from(array('floriane@une-bonne-idee.ch' => 'Floriane'))
				 ->to($to)
				 ->subject($subject)
				 ->emailFormat('html')
				 ->template($mailTemplate)
				 ->viewVars(array(
					'userFirstName' => $user['User']['first_name'],
					'eventDate' => $eventDate,
					'eventPax' => $eventNumberOfPersons,
					'eventName' => $event['Event']['name'],
					'clientName' => $event['Client']['name'],
					'placeName' => (!empty($place['Place']['name'])) ? $place['Place']['name'] : '',
					'jobStartTime' => $job['Job']['start_time'],
					'jobEndTime' => $job['Job']['end_time'],
					'jobJob' => $competencesJobs[$job['Job']['job']],
					'jobSalary' => $salary,
					'jobActivityName' => (!empty($job['Activity']['name'])) ? $job['Activity']['name'] : '',
					'footerTitle' => $title,
					'footerLink' => $link,
					'footerDate' => $date,
					'message' => empty($this->request->data['message']) ? '' : $this->request->data['message']
					));
			if($mail->send()){
				$this->User->StaffQueue->save(array(
					'StaffQueue' => array(
						'user_id' => $_POST['user_id'],
						'job_id' => $_POST['job_id'],
						'status' => $status
					)
				));
			}
			$this->response->body(json_encode(array('success' => 1)));
		}

	}

	public function availabilities() {
		$users = array();
		$users['monday']	= $this->User->available('monday');
		$users['tuesday']   = $this->User->available('tuesday');
		$users['wednesday'] = $this->User->available('wednesday');
		$users['thursday']  = $this->User->available('thursday');
		$users['friday']	= $this->User->available('friday');
		$users['saturday']  = $this->User->available('saturday');
		$users['sunday']	= $this->User->available('sunday');
		$this->set(compact('users'));
	}

	public function rise($user_id, $job_id, $rise = true){
		//$this->autoRender = false;
		if($this->request->is('ajax') OR 1==1){
			$this->User->id = $user_id;
			$job = $this->User->Job->findById($job_id);
			if($rise){

				$job['Job']['evaluation'] = 'rised';
				$this->User->Job->save($job);

				$competence = $this->User->Competence->find('first', array(
					'conditions' => array(
						'sector' => $job['Job']['sector'],
						'job' => $job['Job']['job'],
						'activity_id' => !empty($job['Job']['activity_id']) ? $job['Job']['activity_id'] : '',
						'user_id' => $user_id
					)
				));
				if(!empty($competence)){
					$actualHierarchy = $competence['Competence']['hierarchy'];
					$competence['Competence']['hierarchy'] = $actualHierarchy + 1;
					if($this->User->Competence->save($competence)){
						$this->Session->setFlash(__('Uhas been saved.'), 'alert', array('type' => 'success'));
						return $this->redirect(array('controller' => 'users', 'action' => 'index'));
						//$this->response->body(json_encode(array('success' => 1)));
					} else {
						$this->Session->setFlash(__('User has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
						return $this->redirect(array('controller' => 'users', 'action' => 'index'));
						//$this->response->body(json_encode(array('success' => 0)));
					}
				} else {
					$this->Session->setFlash(__('Uhas been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('controller' => 'users', 'action' => 'index'));
				}
			} else {
				$job['Job']['evaluation'] = 'no rise';
				if($this->User->Job->save($job)){
					$this->Session->setFlash(__('User has been saved.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('controller' => 'users', 'action' => 'index'));
					//$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->Session->setFlash(__('User has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
					return $this->redirect(array('controller' => 'users', 'action' => 'index'));
					//$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

	public function send(){

		$users = $this->User->find('list', array(
			'conditions' => array(
				'role' => array('temporary')
			),
			'fields' => array('User.id', 'User.full_name'),
			'order' => 'full_name'
		));
		$this->set(compact('users'));

		$messages = $this->User->Message->find('all', array(
			'contain' => array('User'),
			'conditions' => array(

			),
			'order' => 'Message.created DESC'
		));

		$lists = $this->User->Tag->find('list', array(
		  'conditions' => array(
			'Tag.category' => 'section'
		  ),
		  'fields' => array('Tag.id', 'Tag.value')
		));
		$this->set(compact('lists'));

		if ($this->request->is('post') || $this->request->is('put')) {

			$recipients = array();
			$to = array();

			if(!empty($this->request->data['send']['recipients'])){
				$recipients = explode(',', $this->request->data['send']['recipients']);
			} elseif(empty($this->request->data['send']['recipients']) && empty($this->request->data['send']['lists'])) {
				$this->Session->setFlash(__('You need to select recipient!'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'send', 'controller' => 'users'));
			}

			$sender = $this->User->findById(AuthComponent::user('id'));

			$to = array();
			foreach($recipients as $recipientId){
				$user = $this->User->findById($recipientId);
				$to[] = $user['User']['email'];
			}

			if(!empty($this->request->data['send']['lists'])){
			  $ids = array();
			  foreach($this->request->data['send']['lists'] as $list){
					$ids[] = $list;
			  }
			  $extras = $this->User->find('all', array(
					'joins' => array(
					  array(
						'table' => 'tags_users',
						'alias' => 'TagsUsers',
						'conditions' => array(
						  'TagsUsers.user_id = User.id'
						)
					  ),
					  array(
						'table' => 'tags',
						'alias' => 'Tags',
						'conditions' => array(
						  'Tags.id = TagsUsers.tag_id',
						  'Tags.category = "section"',
						)
					  )
					),
					'conditions' => array(
						'role' => array('temporary'),
						'email <>' => null,
						'Tags.id' => $ids
					),
					'fields' => array(
						'User.id', 'User.email', 'User.first_name', 'User.username'
					),
					'groupBy' => 'User.id'
			  ));
			  if(!empty($extras)){
					foreach($extras as $extra){
					  $to[] = $extra['User']['email'];
					}
			  }
			}

			$sender = $this->User->findById(AuthComponent::user('id'));
			if($this->request->data['send']['receive_copy']) $to[] = $sender['User']['email'];

			$server = Configure::read('Debug.server');
			if($server == 'local'){
				$to = 'denis@une-bonne-idee.ch';
			} elseif($server == 'test'){
				$to = 'dev@myubic.ch';
			} elseif($server == 'prod'){
				$to = $to;
			}

			if($this->request->data['send']['type'] == 1){
				// MAIL
				$mail = new CakeEmail();
				$mail->from(array($sender['User']['email'] => $sender['User']['full_name']))
					 //->bcc('denis@une-bonne-idee.ch')
					 ->bcc($to)
					 ->subject($this->request->data['send']['subject'])
					 ->emailFormat('html');
				if($mail->send($this->request->data['send']['mailBody'])){
					$this->User->Message->save(array(
						'Message' => array(
							'subject' => $this->request->data['send']['subject'],
							'body' => $this->request->data['send']['mailBody'],
							//'recipients' => $this->request->data['send']['recipients'],
							'type' => 'mail',
							'user_id' => AuthComponent::user('id')
						)
					));
					$this->Session->setFlash(__('The mail has been correctly sent.'), 'alert', array('type' => 'success'));
					return $this->redirect(array('action' => 'send', 'controller' => 'users'));
				}
			} else {
				// SMS
				// $this->Message->save(array(
				//	 'Message' => array(
				//		 'subject' => $this->request->data['send']['subject'],
				//		 'body' => $this->request->data['send']['smsBody'],
				//		 'to' => $this->request->data['send']['recipients'],
				//		 'type' => 'sms',
				//		 'user_id' => AuthComponent::user('id')
				//	 )
				// ));
				$this->Session->setFlash(__('The message has not been sent. SMS are not supported yet.'), 'alert', array('type' => 'warning'));
				return $this->redirect(array('action' => 'send', 'controller' => 'users'));
			}
		}
	}

	public function modal( $id = null, $date = null ){
		if(!empty($date)){
			$day = date('N', strtotime($date));
		} else {
			$day = 0;
		}
		$this->User->contain(
			array(
				'Job' => array('Event' => array('Client')),
				'Competence' => array('Activity', 'FBModule'),
				'Portrait',
				'ScheduleDay' => array(
					'conditions' => array(
						'ScheduleDay.day' => $day
					)
				)
			)
		);
		$user = $this->User->findById($id);
		$this->set(compact('user'));
		$jobs = Configure::read('Competences.jobs');
		$this->set(compact('jobs'));
		$hierarchies = Configure::read('Competences.hierarchies');
		$this->set(compact('hierarchies'));
		$sectors = Configure::read('Competences.sectors');
		$this->set(compact('sectors'));
		$sizes = Configure::read('Users.sizes');
		$this->set(compact('sizes'));
	}

	public function json(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$data = $this->User->find('all', array(
				'conditions' => array(
					'OR' => array(
						'full_name LIKE' => "%" . $_POST['term'] . "%"
					),
					'User.role IN' => array('fixed', 'temporary', 'candidate')
				)
			));
			foreach($data as $k => $item){
				$users[$k]['id'] = $item['User']['id'];
				$users[$k]['text'] = $item['User']['full_name'];
			}
			$this->response->body(json_encode($users));
		}
	}

	public function email(){
		$emails = $this->User->find('all', array(
			'conditions' => array(
				'role' => array('temporary', 'retired', 'fixed'),
				'email <>' => ''
			),
			'fields' => array('email')
		));
		$numberOfEmails = $this->User->find('count', array(
			'conditions' => array(
				'role' => array('temporary', 'retired', 'fixed'),
				'email <>' => ''
			),
			'fields' => array('email')
		));
		//debug($emails);exit;
		echo 'Nombre de mails : ' . $numberOfEmails . '<br><br>';
		foreach($emails as $email){
			echo !empty($email['User']['email']) ? $email['User']['email'] : '';
			echo ', ';
		}

		exit;
	}


	public function rebuildARO() {
		// Build the groups.
		$groups = $this->User->Group->find('all');
		$aro = new Aro();
		foreach($groups as $group) {
			$aro->create();
			$aro->save(array(
			//  'alias'=>$group['Group']['name'],
				'foreign_key' => $group['Group']['id'],
				'model'=>'Group',
				'parent_id' => null
			));
		}

		// Build the users.
		$users = $this->User->find('all');
		$i=0;
		foreach($users as $user) {
			$aroList[$i++]= array(
			//  'alias' => $user['User']['email'],
				'foreign_key' => $user['User']['id'],
				'model' => 'User',
				'parent_id' => $user['User']['group_id']
			);
		}
		foreach($aroList as $data) {
			$aro->create();
			$aro->save($data);
		}

		echo "AROs rebuilt!";
		exit;
	}

	public function missing(){

		$view = new View($this);
		$html = $view->loadHelper('Html');

		$missing = $this->User->find('all', array(
			'conditions' => array(
				'role' => 'temporary',
				'OR' => array(
					array('date_of_birth' => null),
					array('date_of_birth' => '0000-00-00'),
					array('date_of_birth' => '1970-01-01')
				)
			),
			'order' => 'full_name'
		));
		foreach($missing as $user){
			echo $html->link($user['User']['full_name'], array('controller' => 'users', 'action' => 'edit', $user['User']['id']), array('target' => '_blank'));
			echo $html->tag('br');
		}
		//debug($missing);
		exit;
	}

	public function stats(){

		$users = $this->User->find('all', array(
			'conditions' => array(
				//'gender' => 'F',
				'role' => 'temporary'
			)
		));
		foreach($users as $user){
			$data[date('Y', strtotime($user['User']['date_of_birth']))][] = $user;
		}
		ksort($data);
		foreach($data as $year => $extras){
			echo $year . ' - ' . count($extras);
			echo '<br>';
		}
		exit;
	}

	public function mass_mail(){

		$_section = 173; //Animation
		//$_section = 174; //Logistique
		//$_section = 175; //F&B

		$users = $this->User->find('all', array(
			'conditions' => array(
				'role' => array('temporary'),
				'email <>' => null,
				'english_level IN' => array('b2', 'c2', 'c1')
			),
			'fields' => array(
				'User.id', 'User.email', 'User.first_name', 'User.username'
			),
			'contain' => array('Section')
		));
		echo "<ol>";
		foreach($users as $k => $user){
			$send = false;
			if(empty($user['Section'])){
			  $send = false;
			} else {
			  foreach($user['Section'] as $section){
					if($section['id'] == $_section){
					  $send = true;
					  continue;
					}
			  }
			}
			if($send){
				$server = Configure::read('Debug.server');
				if($server == 'local'){
					$to = 'denis@une-bonne-idee.ch';
				} elseif($server == 'test'){
					$to = 'dev@myubic.ch';
				} elseif($server == 'prod'){
					$to = $user['User']['email'];
				}
			  $mail = new CakeEmail();
			  $mail->from(array('fabienne@une-bonne-idee.ch' => 'Fabienne'))
				   ->to($to)
				   //->to('denis@une-bonne-idee.ch')
				   //->cc('fabienne@une-bonne-idee.ch')
				   ->subject('URGENT : Recherche de deux extras anglophones')
				   ->emailFormat('html')
				   ->template('extras_mail_010316')
				   ->viewVars(array(
					  'userFirstName' => $user['User']['first_name']
					  ));
			  if($mail->send()){
				  echo sprintf("<li>Mail has been sent to %s</li>", $user['User']['email']);
			  }
			}
		}
		echo "</ol>";
		exit;
	}

	public function getAvailabilities(){
		$users = array();
		if(!empty($this->request->data['ids']) && !empty($this->request->data['job_id'])){
			foreach($this->request->data['ids'] as $user_id){
				$this->User->id = $user_id;
				$users[$user_id] = $this->User->getAvailability($this->request->data['job_id']);
			}
		}
		$this->set(compact('users'));
		$this->set('availabilities', Configure::read('Users.availabilities'));
	}

}
