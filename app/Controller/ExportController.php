<?php

class ExportController extends AppController {

	public function cresus($company = null, $type = null){

		$date = date('d.m.Y');

		$this->loadModel('Client');
		$this->loadModel('StockOrder');
		$this->loadModel('Event');

		$output = '';
		$separator = ";";
		$extension = "csv";

		$civilities = Configure::read('ContactPeople.civilities1');

		function cleanString($string){
			//return preg_replace("/(?![.=$'€%-])\p{P}/u", "", $string);
			return $string;
		}

		switch($company){
			case 'festiloc':
				if($type == 'clients'){
					$output = 	"NumMyubic".$separator.
								"`Numéro".$separator.
								"`Firme".$separator.
								"`Titre".$separator.
								"`Nom".$separator.
								"`Prénom".$separator.
								"`TélEmail".$separator.
								"`TélProf".$separator.
								"`NPA".$separator.
								"`Localité".$separator.
								"`Adresse\r\n";
					$output = '';
					$clients = $this->Client->find('all', array(
						'contain' => array('ContactPeople'),
						'joins' => array(
							array(
								'table' => 'clients_companies',
								'alias' => 'ClientCompany',
								'conditions' => array(
									'ClientCompany.client_id = Client.id',
									'ClientCompany.company_id = 3'
								)
							)
						)
					));
					foreach($clients as $client){
						if(empty($client['ContactPeople'][0])){
							$cp = array();
							$civility = '';
						} else {
							$cp = $client['ContactPeople'][0];
							$civility = empty($cp['civility']) ? '' : !is_numeric($cp['civility']) ? $civilities[$cp['civility']] : '';
						}
						$output .= $client['Client']['id'];
						$output .= $separator;
						$output .= $client['Client']['import_id'];
						$output .= $separator;
						$output .= sprintf('"%s"',cleanString($client['Client']['name']));
						$output .= $separator;
						$output .= $civility;
						$output .= $separator;
						$output .= empty($cp['last_name']) ? '' : sprintf('"%s"',cleanString($cp['last_name']));
						$output .= $separator;
						$output .= empty($cp['first_name']) ? '' : sprintf('"%s"',cleanString($cp['first_name']));
						$output .= $separator;
						$output .= empty($cp['email']) ? '' : sprintf('"%s"', cleanString($cp['email']));
						$output .= $separator;
						$output .= empty($cp['phone']) ? '' : sprintf('"%s"', cleanString($cp['phone']));
						$output .= $separator;
						$output .= cleanString($client['Client']['zip']);
						$output .= $separator;
						$output .= sprintf('"%s"',cleanString($client['Client']['city']));
						$output .= $separator;
						$output .= sprintf('"%s"',cleanString($client['Client']['address']));
						$output .= "\r\n";
					}
					file_put_contents(WWW_ROOT . sprintf('files/export/export_festiloc_clients.%s', $extension), $output);
				}
				if($type == 'factures'){
					$orders = $this->StockOrder->find('all', array(
						'conditions' => array(
							'status' => array('to_invoice')
						)
					));
					$header = array(
						"`Numéro",
						"`RefClient",
						"`Concerne",
						"`RefArticles",
						"`AQuantité",
						"`APrix"
					);
					$output = implode($separator, $header) . "\r\n";
					$output = '';
					foreach($orders as $order){
						$this->StockOrder->id = $order['StockOrder']['id'];
						$data = array(
							$order['StockOrder']['id'] + 9000000,
							$order['StockOrder']['client_id'],
							sprintf('"%s"',$order['StockOrder']['name']),
							100,
							1,
							$order['StockOrder']['total_ht'], // add discount, replacement price and extra hours
							is_null($order['StockOrder']['forced_delivery_costs']) ? $order['StockOrder']['forced_delivery_costs'] : $order['StockOrder']['delivery_costs']
						);
						$output .= $order['StockOrder']['id'] + 9000000;
						$output .= $separator;
						$output .= $order['StockOrder']['client_id'];
						$output .= $separator;
						$concern = $order['StockOrder']['order_number'];
						if(!empty($order['StockOrder']['name'])) $concern .= ' - ' . $order['StockOrder']['name'];
						$output .= sprintf('"%s"',$concern);
						$output .= $separator;
						$output .= 100; // id articl
						$output .= $separator;
						$output .= 1; // quantity
						$output .= $separator;
						$output .= $order['StockOrder']['net_total'];
						$output .= $separator;
						$output .= "$";
						$output .= "\r\n";
					}
					file_put_contents(WWW_ROOT . sprintf('files/export/export_festiloc_factures.%s', $extension), $output);
				}
			break;

			case 'ubic':
			break;

			default:
			break;
		}
		exit;

	}

}
