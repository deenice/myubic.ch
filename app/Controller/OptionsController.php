<?php

class OptionsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        // For CakePHP 2.1 and up
        $this->Auth->allow();
    }

    public function index(){
    	$json = array();
		$view = new View($this);
        $html = $view->loadHelper('Html');
        $time = $view->loadHelper('Time');
        $options = $this->Option->find('all', array(
        	'conditions' => array(
        		'value' => 'prereserved' 
        	),
        	'contain' => array(
        		'Moment' => array('Event' => array('Client')),
        		'Place'
        	)
        ));
		foreach($options as $k => $option){
			if(empty($option['Moment']['Event'])){
				continue;
			}
			$json[$k]['title'] = empty($option['Moment']['Event']['name']) ? '' : $option['Moment']['Event']['name'] . ' - ' . $option['Moment']['name'] . ' - ' . $option['Moment']['Event']['Client']['name'];
			if(!empty($option['Place'])){
				$json[$k]['title'] .= "\n" . $option['Place']['name'];
			}
			if(!empty($option['Moment']['start_hour']) && !empty($option['Moment']['end_hour'])){
				$json[$k]['title'] .= "\n" . $time->format($option['Moment']['start_hour'], '%H:%M') . ' - ' . $time->format($option['Moment']['end_hour'], '%H:%M');
			}
			$json[$k]['url'] = $html->url(array('controller' => 'events', 'action' => 'edit', $option['Moment']['event_id'], '#' => 'options'));
			if(!empty($option['Moment'])){
				$start_hour = $option['Moment']['start_hour'];
				$end_hour = $option['Moment']['end_hour'];
				$json[$k]['start'] = $option['Option']['date'];
				// $json[$k]['start'] = $option['Option']['date'] . 'T' . $start_hour;
				// $json[$k]['end'] = $option['Option']['date'] . 'T' . $end_hour;
				$json[$k]['allDay'] = true;
			}
		}
		sort($json);
		$this->set('_json', $json);
    }

	public function save(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$output = array();
			$data = array(
				'Option' => array(
					'id' => !empty($_POST['id']) ? $_POST['id'] : '',
					'value' => !empty($_POST['option']) ? $_POST['option'] : '',
					'date' => !empty($_POST['date']) ? $_POST['date'] : '',
					'moment_id' => !empty($_POST['moment_id']) ? $_POST['moment_id'] : '',
					'valid_until' => !empty($_POST['valid_until']) ? date('Y-m-d', strtotime($_POST['valid_until'])) : '',
					'remarks' => !empty($_POST['remarks']) ? $_POST['remarks'] : '',
					'place_id' => !empty($_POST['place_id']) ? $_POST['place_id'] : '',
					'event_id' => !empty($_POST['event_id']) ? $_POST['event_id'] : '',
					'user_id' => !empty($_POST['user_id']) ? $_POST['user_id'] : ''
				)
			);
			if(!empty($data['Option']['id'])){
				$this->Option->id = $_POST['id'];
			} else {
				$this->Option->create();
			}
			if($this->Option->save($data)){
				$output['success'] = 1;
				$output['id'] = $this->Option->id;
			} else {
				$output['success'] = 0;
			}
			$this->response->body(json_encode($output));
		}
	}

	public function getInfos(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$option = $this->Option->find('first', array(
				'conditions' => array(
					'Option.place_id' => $_POST['place_id'],
					'Option.date' => $_POST['date'],
					'Option.moment_id' => !empty($_POST['moment_id']) ? $_POST['moment_id'] : ''
				),
				'contain' => array(
					'User',
					'Moment' => array('Event' => array('Client'))
				)
			));
			$data = array();
			$overlap = 0;
			if(!empty($option)){
				if(!empty($_POST['moment_id'])){
					$moment = $this->Option->Moment->findById($_POST['moment_id']);
					if(
						($moment['Moment']['end_hour'] >= $option['Moment']['start_hour'] AND $moment['Moment']['end_hour'] <= $option['Moment']['end_hour'])
						OR
						($moment['Moment']['start_hour'] >= $option['Moment']['start_hour'] AND $moment['Moment']['start_hour'] <= $option['Moment']['end_hour'])
						OR
						($moment['Moment']['start_hour'] <= $option['Moment']['start_hour'] AND $moment['Moment']['end_hour'] >= $option['Moment']['end_hour'])
						OR
						($moment['Moment']['start_hour'] >= $option['Moment']['start_hour'] AND $moment['Moment']['end_hour'] <= $option['Moment']['end_hour'])
						){
						if($moment['Moment']['id'] != $option['Moment']['id']){
							$overlap = 1;
						} else {
							$overlap = 0;
						}
					} else {
						$overlap = 0;
					}
				}
				$data['Option']['value'] = $option['Option']['value'];
				$data['Option']['id'] = $option['Option']['id'];
				$data['Moment']['id'] = $option['Moment']['id'];
				$data['User']['name'] = $option['User']['full_name'];
				$data['Moment']['name'] = !empty($option['Moment']['name']) ? $option['Moment']['name'] : '';
				$data['Event']['name'] = !empty($option['Moment']['Event']['name']) ? $option['Moment']['Event']['name'] : '';
				$data['Client']['name'] = !empty($option['Moment']['Event']['Client']['name']) ? $option['Moment']['Event']['Client']['name'] : '';
				$data['Moment']['start_hour'] = !empty($option['Moment']['start_hour']) ? $option['Moment']['start_hour'] : '';
				$data['Moment']['end_hour'] = !empty($option['Moment']['end_hour']) ? $option['Moment']['end_hour'] : '';
				$data['empty'] = 0;
				$data['overlap'] = $overlap;
			} else {
				$data['Option']['value'] = 'free';
				$data['empty'] = 1;
				$data['overlap'] = 0;
			}
			$this->response->body(json_encode($data));
		}
	}

	public function update(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->Option->id = $_POST['id'];
			if($this->Option->saveField('value', $_POST['value'])){
				$output = array('success' => 1);
			} else {
				$output = array('success' => 2);
			}
			$this->response->body(json_encode($output));
		}
	}

}