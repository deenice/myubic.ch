<?php

class RevelateController extends AppController {

	public function test(){
		$test = $this->Revelate->get('51602d5d-a0c0-414b-8d7f-0e6995d18abe', 'salesInvoices', 'b5e695d7-6043-4dc1-b188-bf73384dc268');
		debug($test);
		exit;
	}

  public function export($model = '', $companyId = '', $onlyDeposit = false){

		$this->loadModel('Company');
		$this->loadModel('StockOrder');
		$this->loadModel('Client');
		$this->loadModel('Revelate');

		if($model == 'stockorders' && $companyId == 3){

			if($this->request->is('ajax')){
				$this->autoRender = false;

				$company = $this->Company->findById($this->request->data['companyId']);
				$stockorder = $this->StockOrder->findById($this->request->data['itemId']);
				$this->StockOrder->id = $stockorder['StockOrder']['id'];
				$xml = $this->Revelate->xml('stockorder', $stockorder['StockOrder']['id'], array('deposit' => $onlyDeposit));

				if($onlyDeposit && empty($stockorder['StockOrder']['deposit_uuid'])){
					// we need to create the deposit invoice in revelate
					$result = $this->Revelate->post($company['Company']['uuid'], 'salesInvoices', $xml);
					if($result->isOk()){
						$result = Xml::toArray(Xml::build($result->body));
						$this->StockOrder->saveField('deposit_uuid', $result['Identifier']['Id'], array('callbacks' => false));
            $this->StockOrder->saveField('deposit_export_status', 'exported', array('callbacks' => false));
						$this->response->body(json_encode(array('success' => 1)));
					} else {
						$this->response->body(json_encode(array('success' => 0, 'result' => $result)));
					}
				} elseif ($onlyDeposit && !empty($stockorder['StockOrder']['deposit_uuid'])){
					$result = $this->Revelate->put($company['Company']['uuid'], 'salesInvoices', $xml, $stockorder['StockOrder']['deposit_uuid']);
					//CakeLog::write('myubic', json_encode($result));
					if($result->isOk()){
            $this->StockOrder->saveField('deposit_export_status', 'exported', array('callbacks' => false));
						$this->response->body(json_encode(array('success' => 1)));
					} else {
						$this->StockOrder->saveField('deposit_uuid', null, array('callbacks' => false));
						$result = $this->Revelate->post($company['Company']['uuid'], 'salesInvoices', $xml);
						if($result->isOk()){
							$result = Xml::toArray(Xml::build($result->body));
							$this->StockOrder->saveField('deposit_uuid', $result['Identifier']['Id'], array('callbacks' => false));
	            $this->StockOrder->saveField('deposit_export_status', 'exported', array('callbacks' => false));
							$this->response->body(json_encode(array('success' => 1)));
						} else {
							$this->response->body(json_encode(array('success' => 0, 'result' => $result)));
						}
					}
				} else {
					if(empty($stockorder['StockOrder']['uuid'])){
						// we need to create the invoice in revelate
						$result = $this->Revelate->post($company['Company']['uuid'], 'salesInvoices', $xml);
						if($result->isOk()){
							$result = Xml::toArray(Xml::build($result->body));
							$this->StockOrder->saveField('uuid', $result['Identifier']['Id'], array('callbacks' => false));
							$this->StockOrder->saveField('export_status', 'exported', array('callbacks' => false));
							$this->StockOrder->saveField('export_invoiced_total', $stockorder['StockOrder']['invoiced_total'], array('callbacks' => false));
							$this->response->body(json_encode(array('success' => 1)));
						} else {
							$this->response->body(json_encode(array('success' => 0, 'result' => $result)));
						}
					} else {
						// we update the stockorder
						$result = $this->Revelate->put($company['Company']['uuid'], 'salesInvoices', $xml, $stockorder['StockOrder']['uuid']);
						if($result->isOk()){
							$this->StockOrder->saveField('export_status', 'exported', array('callbacks' => false));
							$this->response->body(json_encode(array('success' => 1)));
						} else {
							$this->StockOrder->saveField('uuid', null, array('callbacks' => false));
							$result = $this->Revelate->post($company['Company']['uuid'], 'salesInvoices', $xml);
							if($result->isOk()){
								$result = Xml::toArray(Xml::build($result->body));
								$this->StockOrder->saveField('uuid', $result['Identifier']['Id'], array('callbacks' => false));
								$this->StockOrder->saveField('export_status', 'exported', array('callbacks' => false));
                $this->StockOrder->saveField('export_invoiced_total', $stockorder['StockOrder']['invoiced_total'], array('callbacks' => false));
								$this->response->body(json_encode(array('success' => 1)));
							} else {
								$this->response->body(json_encode(array('success' => 0, 'result' => $result)));
							}
						}
					}
				}
				$this->StockOrder->id = null;
			}
		}
	}
}
