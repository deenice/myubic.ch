<?php

class MomentsController extends AppController {

	public function save(){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$output = array();
			$data = array(
				'Moment' => array(
					'id' => !empty($this->request->data['id']) ? $this->request->data['id'] : '',
					'type' => !empty($this->request->data['type']) ? $this->request->data['type'] : '',
					'event_id' => !empty($this->request->data['event_id']) ? $this->request->data['event_id'] : '',
					'name' => !empty($this->request->data['name']) ? $this->request->data['name'] : '',
					'remarks' => !empty($this->request->data['remarks']) ? $this->request->data['remarks'] : '',
					'start_hour' => !empty($this->request->data['start_hour']) ? $this->request->data['start_hour'].':00' : '',
					'end_hour' => !empty($this->request->data['end_hour']) ? $this->request->data['end_hour'].':00' : '',
					'place_id' => !empty($this->request->data['place_id']) ? $this->request->data['place_id'] : ''
				)
			);
			if(!empty($this->request->data['id'])){
				$this->Moment->create();
			} else {
				$this->Moment->id = $this->request->data['id'];
			}
			if($this->Moment->save($data)){
				$output['success'] = 1;
				$output['id'] = $this->Moment->id;
			} else {
				$output['success'] = 0;
			}
			$this->response->body(json_encode($output));
		}
	}

	public function delete(){
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			if($this->Moment->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}

		}
	}

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['moments'])){
				foreach($this->request->data['moments'] as $weight => $moment){
					$this->Moment->id = $moment;
					$this->Moment->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function modal( $type = '', $id = '' ){
		$moment = $this->Moment->findById($id);
		$this->set(compact('moment'));
		$this->render('modals/edit');
	}
}
