<?php
class ContactPeoplesController extends AppController {

  public $components = array(
    'DataTable.DataTable' => [
      'ContactPeoples' => [
        'model' => 'ContactPeople',
        'columns' => [
          'name',
          'first_name',
          'last_name',
          'Actions' => null,
        ],
        'conditions' => array(
          'ContactPeople.name <>' => null
        ),
        'fields' => array(
          'ContactPeople.id'
        ),
        'autoData' => false
      ]
    ],
  );

	public function add(){

		$this->set('clients', $this->ContactPeople->Client->find('list'));
    $client_id = null;
    $type = 'client';
    $controller = 'clients';
    $pageTitle = __('Clients');
    if(!empty($this->request->params['named']['client_id'])){
      $client_id = $this->request->params['named']['client_id'];
    } elseif(!empty($this->request->params['named']['supplier_id'])){
      $this->set('clients', $this->ContactPeople->Supplier->find('list'));
      $client_id = $this->request->params['named']['supplier_id'];
      $type = 'supplier';
      $controller = 'suppliers';
      $pageTitle = __('Suppliers');
    }
    $this->set(compact('client_id'));
    $this->set(compact('type'));
    $this->set(compact('controller'));
    $this->set(compact('pageTitle'));

		if($this->request->is('post') OR $this->request->is('put')){

      if($this->ContactPeople->saveAssociated($this->request->data)){
        $client = $this->ContactPeople->Client->findById($this->request->data['ContactPeople']['client_id']);
        $supplier = $this->ContactPeople->Supplier->findById($this->request->data['ContactPeople']['client_id']);
        if(!empty($supplier)){
          $controller = 'suppliers';
        } else {
          $controller = 'clients';
        }
        $this->Session->setFlash(__('%s has been saved.', $this->request->data['ContactPeople']['first_name'] . ' ' . $this->request->data['ContactPeople']['last_name']), 'alert', array('type' => 'success'));
        if(!empty($this->request->data['destination'])){
          if($this->request->data['destination'] == 'edit'){
            return $this->redirect(array('controller' => $controller, 'action' => 'edit', $this->request->data['ContactPeople']['client_id']));
          } else {
            return $this->redirect(array('controller' => $controller, 'action' => 'index'));
          }
        } else {
          return $this->redirect(array('controller' => $controller, 'action' => 'index'));
        }
      }

    } elseif($this->request->is('ajax')){
      $this->autoRender = false;
      $data = $this->request->query['data'];
      if($this->ContactPeople->save($data)){
        $this->response->body(json_encode(array(
          'success' => 1,
          'contactPeopleId' => $this->ContactPeople->id,
          'contactPeopleFullName' => $this->ContactPeople->field('full_name'),
          'contactPeopleEmail' => $this->ContactPeople->field('email'),
          'contactPeoplePhone' => $this->ContactPeople->field('phone')
        )));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }

    }
	}

	public function edit( $id = null ){

    $this->set('clients', $this->ContactPeople->Client->find('list'));
		if($this->request->is('post') OR $this->request->is('put')){
      if($this->ContactPeople->saveAssociated($this->request->data)){
        $client = $this->ContactPeople->Client->findById($this->request->data['ContactPeople']['client_id']);
        $partner = $this->ContactPeople->Partner->findById($this->request->data['ContactPeople']['client_id']);
        if(!empty($partner)){
          $controller = 'partners';
        } else {
          $controller = 'clients';
        }
        $this->Session->setFlash(__('Contact person has been saved.'), 'alert', array('type' => 'success'));
        if(!empty($this->request->data['destination'])){
          if($this->request->data['destination'] == 'edit'){
            return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'edit', $this->ContactPeople->id));
          } else {
            return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'control'));
          }
        } else {
          return $this->redirect(array('controller' => $controller, 'action' => 'index'));
        }
      }
    } elseif($this->request->is('ajax')){
      $this->autoRender = false;
      $data = $this->request->query['data'];
      if($this->ContactPeople->save($data)){
        $this->response->body(json_encode(array(
          'success' => 1,
          'contactPeopleId' => $this->ContactPeople->id,
          'contactPeopleFullName' => $this->ContactPeople->field('full_name'),
          'contactPeopleEmail' => $this->ContactPeople->field('email'),
          'contactPeoplePhone' => $this->ContactPeople->field('phone')
        )));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    } else {
      $this->request->data = $this->ContactPeople->read(null, $id);
    }
	}

	public function save(){
		if($this->request->is('post') OR $this->request->is('put')){
			$data = array();
			for($i=0; $i < count($this->request->data['ContactPeople']['id']); $i++){
				$this->ContactPeople->create();
				$obj = array(
					'ContactPeople' => array(
						'id' => $this->request->data['ContactPeople']['id'][$i],
            'first_name' => $this->request->data['ContactPeople']['first_name'][$i],
						'last_name' => $this->request->data['ContactPeople']['last_name'][$i],
						'phone' => $this->request->data['ContactPeople']['phone'][$i],
						'email' => $this->request->data['ContactPeople']['email'][$i],
						'function' => $this->request->data['ContactPeople']['function'][$i],
						'department' => $this->request->data['ContactPeople']['department'][$i],
						'civility' => $this->request->data['ContactPeople']['civility'][$i],
						'language' => $this->request->data['ContactPeople']['language'][$i],
						'client_id' => $this->request->data['Client']['id']
					)
				);
				if(!empty($obj['ContactPeople']['first_name'])) $data[] = $obj;
			}
      $client = $this->ContactPeople->Client->findById($this->request->data['Client']['id']);
      $supplier = $this->ContactPeople->Supplier->findById($this->request->data['Client']['id']);
      if(!empty($supplier)){
        $controller = 'suppliers';
      } else {
        $controller = 'clients';
      }
			if($this->ContactPeople->saveMany($data)){
				$this->Session->setFlash(__('Contact persons have been saved.'), 'alert', array('type' => 'success'));
        if(!empty($this->request->data['destination'])){
          if($this->request->data['destination'] == 'edit'){
            return $this->redirect(array('controller' => $controller, 'action' => 'edit', $this->request->data['Client']['id']));
          } else {
            return $this->redirect(array('action' => 'index', 'controller' => $controller));
          }
        } else {
        	return $this->redirect(array('action' => 'index', 'controller' => $controller));
        }
			}
		}
	}

	public function delete( $id = null ){
		if($this->request->is('post') OR $this->request->is('put')){

		} elseif($this->request->is('ajax')){
      //ARCHIVE CONTACT PERSON
			$this->autoRender = false;
			if($this->ContactPeople->delete($id)){
				$output = array('success' => 1);
			} else {
				$output = array('success' => 0);
			}
			$this->response->body(json_encode($output));
		} else {
			$this->ContactPeople->id = $id;
			$this->ContactPeople->Client->id = $this->ContactPeople->field('client_id');
			if($this->ContactPeople->delete($id)){
				$this->Session->setFlash(__('Contact person has been deleted.'), 'alert', array('type' => 'success'));
                return $this->redirect(array('action' => 'edit', 'controller' => 'clients', $this->ContactPeople->Client->id));
			}
		}
	}

    public function get() {
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $contactPeople = $this->ContactPeople->findById($_POST['contact_people_id']);
            $this->response->body(json_encode($contactPeople));
        }
    }

    public function beautify(){
        if(AuthComponent::user('id') == 1){
            $data = $this->ContactPeople->find('all', array(
                'fields' => array('id', 'name')
            ));
            foreach($data as $cp){
                if(empty($cp['ContactPeople']['name'])){
                    continue;
                }
                $this->ContactPeople->id = $cp['ContactPeople']['id'];
                $name = explode(' ', $cp['ContactPeople']['name']);
                $firstName = $name[0];
                unset($name[0]);
                $lastName = trim(implode(' ', $name));
                $this->ContactPeople->saveField('first_name', $firstName);
                $this->ContactPeople->saveField('last_name', $lastName);
            }
        }
        exit;
    }

    public function control() {
        $this->DataTable->setViewVar('ContactPeoples');
    }


}
