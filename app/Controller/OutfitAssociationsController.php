<?php

class OutfitAssociationsController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$ass = array(
				'OutfitAssociation' => array(
					'outfit_id' => $this->request->data['outfit_id'],
					'model' => $this->request->data['model'],
					'model_id' => $this->request->data['model_id']
				)
			);
			if($this->OutfitAssociation->save($ass)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->OutfitAssociation->deleteAll(array(
				'OutfitAssociation.outfit_id' => $this->request->data['outfit_id'],
				'OutfitAssociation.model' => $this->request->data['model'],
				'OutfitAssociation.model_id' => $this->request->data['model_id']
			))){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}

	}

}
