<?php

class BeaconsController extends AppController {

	public function getBeaconsInZone($id) {
		$this->loadModel('StockOrderBatch');
		if(!empty($id)){
			$this->StockOrderBatch->id = $id;
			$this->StockOrderBatch->StockItem->id = $this->StockOrderBatch->field('stock_item_id');
			$beacons = $this->Beacon->find('all', array(
				'conditions' => array(
					'stock_zone_id' => $this->StockOrderBatch->StockItem->field('stock_zone_id')
				),
				'cache' => $this->StockOrderBatch->StockItem->field('stock_zone_id'),
				'cacheConfig' => 'short'
			));
			$this->set(compact('beacons'));
			$this->set('_serialize', array('beacons'));
		}
	}

}