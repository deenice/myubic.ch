<?php

class GamesController extends AppController {

	public function add(){

		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Game->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('Game has been saved.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('controller' => 'activities', 'action' => 'edit', $this->request->data['Game']['activity_id'], '#' => 'game' . $this->Game->id));
			} else {
				$this->Session->setFlash(__('Game has not been saved.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('controller' => 'activities', 'action' => 'edit', $this->request->data['Game']['activity_id']));
			}
		}

	}

	public function edit( $id = null ){

		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Game->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('Game has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('controller' => 'activities', 'action' => 'edit', $this->request->data['Game']['activity_id'], '#' => 'game' . $this->Game->id));
				} else {
					return $this->redirect(array('controller' => 'activities', 'action' => 'index'));
				}
			}
		}

	}

}
