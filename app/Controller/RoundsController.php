<?php

class RoundsController extends AppController {

  public function notify( $mode = '', $step = '' ){
    switch($mode){
      case 'relaunch':
        // resend an email to extras who did not answer
        $answers = $this->Round->RoundStep->RoundAnswer->find('all', array(
          'conditions' => array(
            'RoundAnswer.round_step_id' => $step,
            'RoundAnswer.answer' => 'waiting'
          )
        ));
        if(!empty($answers)){
          foreach($answers as $answer){
            $this->Round->RoundStep->RoundAnswer->id = $answer['RoundAnswer']['id'];
            $this->Round->RoundStep->RoundAnswer->notify();
          }
        }
      break;
      default:
        // we have to get all steps which have to be sent today
        $date = date('Y-m-d');
        $steps = $this->Round->RoundStep->find('all', array(
          'joins' => array(
            array(
              'table' => 'rounds',
              'alias' => 'r',
              'conditions' => array(
                'r.id = RoundStep.round_id'
              )
            ),
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array(
                'j.id = r.job_id',
                // to be sure we dont send emails if jobs are all filled
                'j.staff_enrolled < j.staff_needed'
              )
            ),
            array(
              'table' => 'events',
              'alias' => 'e',
              'conditions' => array(
                'e.id = j.event_id',
                // we dont want to send mails if event is past!
                'e.confirmed_date >=' => $date
              )
            )
          ),
          'conditions' => array(
            'RoundStep.date' => $date,
            'RoundStep.sent' => 0,
            'RoundStep.sent_date <>' => null
          ),
          'fields' => array('e.confirmed_date', 'RoundStep.*')
        ));
        if(!empty($steps)){
          foreach($steps as $step){
            $this->Round->RoundStep->id = $step['RoundStep']['id'];
            $this->Round->RoundStep->send();
          }
        }
      break;
    }
    exit;
  }

}
