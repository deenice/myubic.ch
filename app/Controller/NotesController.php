<?php

class NotesController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->Note->save($this->request->data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = null){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->Note->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function modal( $mode = '', $model = '', $model_id = '', $category = '', $sticky = 0 ){
		if(!empty($model)) $this->loadModel($model);
		switch($mode){
			case 'portlet':
				$conditions = array(
					'model' => strtolower($model),
					'model_id' => $model_id
				);
				if(!empty($category)){
					$conditions[] = array('category' => $category);
				}
				if($sticky){
					$conditions[] = array('sticky' => 1);
				}
				$notes = $this->Note->find('all', array(
					'contain' => array('User' => array('Portrait')),
					'conditions' => $conditions,
					'order' => 'Note.sticky DESC, Note.created DESC'
				));
				$this->set(compact('notes'));
				$this->set('model', $model);
				$item = $this->$model->findById($model_id);
				$this->render('modal_portlet');
				break;
			case 'list':
				$conditions = array(
					'model' => strtolower($model),
					'model_id' => $model_id
				);
				if(!empty($category)){
					$conditions[] = array('category' => $category);
				}
				$notes = $this->Note->find('all', array(
					'contain' => array('User' => array('Portrait')),
					'conditions' => $conditions,
					'order' => 'Note.sticky DESC, Note.created DESC'
				));
				$this->set(compact('notes'));
				$this->set('model', $model);
				$item = $this->$model->findById($model_id);
				$title = __('Notes - %s', !empty($item[$model]['modal_title']) ? $item[$model]['modal_title'] : $item[$model]['name']);
				$this->set(compact('title'));
				$this->render('modal_list');
			break;
			case 'modal':
			$this->autoRender = false;
			$note = $this->Note->findById($this->request->data['id']);
			$this->response->body(json_encode($note));
			break;
			default:
			break;
		}
	}

}
