<?php

App::import("Vendor", "Polyline", array('file' => "Polyline/Polyline.php"));

class VehicleToursController extends AppController {

	public function add(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$existingTour = $this->VehicleTour->findByCodeAndDate($this->request->data['code'], $this->request->data['date']);
			if(empty($existingTour)){
				$tour = array(
					'VehicleTour' => array(
						'code' => $this->request->data['code'],
						'date' => !empty($this->request->data['date']) ? date('Y-m-d', strtotime($this->request->data['date'])) : ''
					)
				);
				$this->VehicleTour->create();
				$this->VehicleTour->save($tour);
				$data = array(
					'StockOrderVehicleTour' => array(
						'type' => $this->request->data['type'],
						'stock_order_id' => $this->request->data['stock_order_id'],
						'vehicle_tour_id' => $this->VehicleTour->id
					)
				);
			} else {
				$tour = $existingTour;
				$data = array(
					'StockOrderVehicleTour' => array(
						'type' => $this->request->data['type'],
						'stock_order_id' => $this->request->data['stock_order_id'],
						'vehicle_tour_id' => $existingTour['VehicleTour']['id']
					)
				);
			}
			if($this->VehicleTour->StockOrderVehicleTour->save($data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode($data));
			}
		}
	}

	public function edit( $id = null ){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$tour = $this->VehicleTour->findById($id);
			if($this->request->data['code'] == 0){
				$this->VehicleTour->StockOrderVehicleTour->deleteAll(array(
					'StockOrderVehicleTour.vehicle_tour_id' => $id,
					'StockOrderVehicleTour.stock_order_id' => $this->request->data['stock_order_id'],
					'StockOrderVehicleTour.type' => $this->request->data['type']
				));
				$this->VehicleTour->VehicleTourMoment->deleteAll(array(
					'VehicleTourMoment.vehicle_tour_id' => $id,
					'VehicleTourMoment.stock_order_id' => $this->request->data['stock_order_id']
				));
			}
			elseif($tour['VehicleTour']['code'] != $this->request->data['code'] && $this->request->data['code'] > 0){
				// tour code has been updated
				// let's find a tour with this code at the corresponding date
				$potentialTour = $this->VehicleTour->findByCodeAndDate($this->request->data['code'], $this->request->data['date']);
				if(empty($potentialTour)){
					// first we delete stockorder vehicle tour
					$this->VehicleTour->StockOrderVehicleTour->deleteAll(array(
						'StockOrderVehicleTour.vehicle_tour_id' => $id,
						'StockOrderVehicleTour.stock_order_id' => $this->request->data['stock_order_id'],
						'StockOrderVehicleTour.type' => $this->request->data['type']
					));

					// no tour with this code => create a new one
					$data = array(
						'VehicleTour' => array(
							'code' => $this->request->data['code'],
							'date' => !empty($this->request->data['date']) ? date('Y-m-d', strtotime($this->request->data['date'])) : ''
						)
					);
					$this->VehicleTour->create();
					$this->VehicleTour->save($data);

					$this->VehicleTour->StockOrderVehicleTour->create();
					$this->VehicleTour->StockOrderVehicleTour->save(array(
						'StockOrderVehicleTour' => array(
							'stock_order_id' => $this->request->data['stock_order_id'],
							'vehicle_tour_id' => $this->VehicleTour->id,
							'type' => $this->request->data['type']
						)
					));
					$this->VehicleTour->VehicleTourMoment->deleteAll(array(
						'VehicleTourMoment.stock_order_id' => $this->request->data['stock_order_id']
					));
				} else {
					// first we delete stockorder vehicle tour
					$this->VehicleTour->StockOrderVehicleTour->deleteAll(array(
						'StockOrderVehicleTour.vehicle_tour_id' => $id,
						'StockOrderVehicleTour.stock_order_id' => $this->request->data['stock_order_id'],
						'StockOrderVehicleTour.type' => $this->request->data['type']
					));

					// a tour with this code exists => update stockorder tour_id
					$this->VehicleTour->StockOrderVehicleTour->create();
					$this->VehicleTour->StockOrderVehicleTour->save(array(
						'StockOrderVehicleTour' => array(
							'stock_order_id' => $this->request->data['stock_order_id'],
							'vehicle_tour_id' => $potentialTour['VehicleTour']['id'],
							'type' => $this->request->data['type']
						)
					));
					$this->VehicleTour->VehicleTourMoment->deleteAll(array(
						'VehicleTourMoment.stock_order_id' => $this->request->data['stock_order_id']
					));
				}
				$this->VehicleTour->VehicleTourMoment->get($id);
			}
			$this->response->body(json_encode(array('success' => 1)));
		}
	}

	public function documents( $id = null ){

		$this->layout = 'default';
		$this->loadModel('MyTools');

		$originFestiloc = 'Route+du+Tir-Fédéral+10+1762+Givisiez';

		$this->VehicleTour->contain(
			array(
				'StockOrder' => array(
					'StockOrderBatch' => array(
	        			'StockItem'
	        		),
	        		'Client',
	        		'Company',
	        		'ContactPeopleClient',
	        		'ContactPeopleDelivery',
	        		'ContactPeopleReturn'
				)
			)
		);
		$data = $this->VehicleTour->findById($id);

		$addresses = array();
		if(!empty($data['StockOrder'])){
			foreach($data['StockOrder'] as $order){
				switch($order['StockOrdersVehicleTour']['type']){
					case 'delivery':
					$addresses[] = urlencode($order['delivery_address'] . ' ' . $order['delivery_zip_city']);
					break;
					case 'return':
					$addresses[] = urlencode($order['return_address'] . ' ' . $order['return_zip_city']);
					break;
					default;
					break;
				}
			}
		}
		$waypoints = implode('|', $addresses);

		$url = 	'https://maps.googleapis.com/maps/api/directions/json?origin='.$originFestiloc
				.'&destination='.$originFestiloc
				.'&waypoints='.$waypoints.'&language=fr&key='
				. Configure::read('GoogleMapsAPIKey');
        $travel = json_decode($this->MyTools->get_data($url), true);

        $points = array();
        $markers = array();
        foreach($travel['routes'] as $route){
        	foreach($route['legs'] as $k => $leg){
        		$markers[] = '&markers=label:'.$k.'|' . $leg['start_location']['lat'] . ',' . $leg['start_location']['lng'];
        		foreach($leg['steps'] as $step){
        			$points[] = array($step['start_location']['lat'], $step['start_location']['lng']);
        			$points[] = array($step['end_location']['lat'], $step['end_location']['lng']);
        		}
        	}
        }
        $polyline = Polyline::Encode($points);
        $markers = implode('', $markers);

        $filename = $data['VehicleTour']['date'] . '_' . $data['VehicleTour']['code'] . '.pdf';
        $this->pdfConfig = array(
            'filename' => $filename,
            'download' => false
        );

        $this->set(compact('data'));
        $this->set(compact('travel'));
        $this->set(compact('polyline'));
        $this->set(compact('markers'));
	}

	public function generateRouteUrl($id = null){

		$this->VehicleTour->contain('StockOrder');
		$data = $this->VehicleTour->findById($id);
		if(!empty($data['StockOrder'])){
			// first we sort orders according to weight moment
			foreach($data['StockOrder'] as $k => $order){
				$tmp = $this->VehicleTour->VehicleTourMoment->find('first', array(
					'conditions' => array(
						'VehicleTourMoment.stock_order_id' => $order['id'],
						'VehicleTourMoment.vehicle_tour_id' => $id
					),
					'fields' => array('weight')
				));
				$data['StockOrder'][$tmp['VehicleTourMoment']['weight']+100] = $order;
				unset($data['StockOrder'][$k]);
			}
			ksort($data['StockOrder']);

			$adresses = array();
			$addresses[0] = "https://www.google.ch/maps/dir/Festiloc'+S%C3%A0rl,+Route+du+Tir-F%C3%A9d%C3%A9ral,+Givisiez";
			foreach($data['StockOrder'] as $k => $order){
				switch($order['StockOrdersVehicleTour']['type']){
					case 'delivery':
					$addresses[] =  urlencode($order['delivery_address'] . ' ' . $order['delivery_zip_city']);
					break;
					case 'return':
					$addresses[] =  urlencode($order['return_address'] . ' ' . $order['return_zip_city']);
					break;
					default;
					break;
				}
			}
			$addresses[999] = "Festiloc'+S%C3%A0rl,+Route+du+Tir-F%C3%A9d%C3%A9ral,+Givisiez";
			$url = implode('/', $addresses);
			$this->redirect($url);
		} else {

		}
	}

	public function updateWeights(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if(!empty($_POST['rows'])){
				foreach($_POST['rows'] as $row){
					$this->VehicleTour->StockOrdersVehicleTour->id = $row['id'];
					$this->VehicleTour->StockOrdersVehicleTour->saveField('weight', $row['weight']);
				}
				$this->response->body(json_encode(array('success' => 1)));
			}
		}
	}

	public function day($start = null, $index = 0){
		// we need to get all tours of corresponding day
		$date = date('Y-m-d', strtotime($start . sprintf(' + %d days', $index) ));

		$tours = $this->VehicleTour->find('all', array(
			'contain' => array('Driver', 'VehicleTourMoment', 'StockOrder', 'VehicleReservation' => array('VehicleCompanyModel')),
			'conditions' => array(
				'date' => $date
			),
			'order' => 'code ASC'
		));
		foreach($tours as $k => $tour){
			if(empty($tour['StockOrder'])){
				$tourId = $tour['VehicleTour']['id'];
				unset($tours[$k]);
				$this->VehicleTour->VehicleTourMoment->deleteAll(
					array(
						'VehicleTourMoment.vehicle_tour_id' => $tourId
					)
				);
			}
			if(!empty($tour['VehicleReservation'])){
				foreach ($tour['VehicleReservation'] as $key => $reservation) {
					if(empty($reservation['VehicleCompanyModel']['type'])){
						continue;
					}
					if($reservation['VehicleCompanyModel']['type'] == 'vehicle'){
						$tours[$k]['VehicleReservationVehicle'] = $reservation;
					}
					if($reservation['VehicleCompanyModel']['type'] == 'trailer'){
						$tours[$k]['VehicleReservationTrailer'] = $reservation;
					}
				}
			}
		}
		$this->set(compact('tours'));

		$date1 = date('d.m.Y', strtotime($start . sprintf(' + %d days', $index) ));
		$this->set('date', $date1);

		$drivers = $this->VehicleTour->Driver->find('list', array(
			'conditions' => array(
				'Driver.role' => array('fixed')
			),
			'fields' => array('Driver.id', 'Driver.full_name')
		));
		$this->set(compact('drivers'));

		$companies = $this->VehicleTour->VehicleReservation->VehicleCompanyModel->VehicleCompany->find('list', array(
			'fields' => array('VehicleCompany.id', 'VehicleCompany.name')
		));
		$this->set(compact('companies'));
	}

	public function portlet($id = null){

		$this->loadModel('MyTools');
		$this->loadModel('VehicleCompany');

		// first we look if the tour has stored moments
		//$this->VehicleTour->VehicleTourMoment->clean();
		$moments = $this->VehicleTour->VehicleTourMoment->get($id);

		$this->VehicleTour->id = $id;

		$this->VehicleTour->contain(array(
			'StockOrder' => array(
				'ContactPeopleClient',
				'ContactPeopleDelivery',
				'ContactPeopleReturn',
				'StockOrderBatch' => array(
					'StockItem'
				),
				'Client'
			),
			'Driver',
			'VehicleReservation' => array(
				'conditions' => array(
					'cancelled' => 0
				),
				'VehicleCompanyModel' => array('VehicleCompany'),
				'Vehicle'
			)
		));
		$this->VehicleTour->create();
		$tour = $this->VehicleTour->findById($id);

		if(!empty($tour['VehicleReservation'])){
			foreach ($tour['VehicleReservation'] as $key => $reservation) {
				if(empty($reservation['VehicleCompanyModel']['type'])){
					continue;
				}
				if($reservation['VehicleCompanyModel']['type'] == 'vehicle'){
					$tour['VehicleReservationVehicle'] = $reservation;
				}
				if($reservation['VehicleCompanyModel']['type'] == 'trailer'){
					$tour['VehicleReservationTrailer'] = $reservation;
				}
			}
		}

		$companies = $this->VehicleCompany->find('list', array(
			'order' => 'VehicleCompany.weight'
		));
		$this->set(compact('companies'));

		$drivers = $this->VehicleTour->Driver->find('list', array(
			'fields' => array('Driver.id', 'Driver.full_name')
		));
		$this->set(compact('drivers'));

		if(empty($moments)){
			// first we set start/end location to festiloc depot
			$tour['VehicleTour']['start_location'] = 'festiloc';
			$tour['VehicleTour']['end_location'] = 'festiloc';
			$tour['VehicleTour']['start_address'] = 'Route du Tir Fédéral 10, 1762 Givisiez';
			$tour['VehicleTour']['end_address'] = 'Route du Tir Fédéral 10, 1762 Givisiez';
			$this->VehicleTour->save($tour);

			// we need to create the initial moments (start, routes, orders and return)
			$data = array();
			$start = array(
				'VehicleTourMoment' => array(
					'vehicle_tour_id' => $id,
					'type' => 'start',
					'weight' => 0
				)
			);
			array_push($data, $start);
			$weight = 1;
			foreach($tour['StockOrder'] as $k => $order){
				if($k==0){
					$route = array(
						'VehicleTourMoment' => array(
							'weight' => $weight++,
							'vehicle_tour_id' => $id,
							'type' => 'route'
						)
					);
					array_push($data, $route);
				}
				$temp = array(
					'VehicleTourMoment' => array(
						'weight' => $weight++,
						'vehicle_tour_id' => $id,
						'stock_order_id' => $order['id'],
						'type' => $order['StockOrdersVehicleTour']['type']
					)
				);
				array_push($data, $temp);
				$route = array(
					'VehicleTourMoment' => array(
						'weight' => $weight++,
						'vehicle_tour_id' => $id,
						'type' => 'route'
					)
				);
				array_push($data, $route);
			}
			$end = array(
				'VehicleTourMoment' => array(
					'vehicle_tour_id' => $id,
					'type' => 'end',
					'weight' => $weight
				)
			);
			array_push($data, $end);
			$this->VehicleTour->VehicleTourMoment->saveMany($data);
			$this->VehicleTour->VehicleTourMoment->clean();
			$moments = $this->VehicleTour->VehicleTourMoment->find('all', array(
				'conditions' => array(
					'vehicle_tour_id' => $id
				),
				'contain' => array(
					'StockOrder'
				),
				'order' => 'VehicleTourMoment.weight'
			));
		}

		$start_hour = empty($tour['VehicleTour']['start']) ? strtotime('00:00:00') : strtotime($tour['VehicleTour']['start']);
		$start = $start_hour;

		$moments = $this->VehicleTour->prepareMoments($moments, $tour, $start);
		$lastMoment = end($moments);

		$duration = gmdate("H:i", $start_hour - $lastMoment['VehicleTourMoment']['hour']);
		$start_hour_reservation = empty($tour['VehicleTour']['start']) ? strtotime('08:00:00') : strtotime($tour['VehicleTour']['start']);
		$start_date_reservation = date('d.m.Y', strtotime($tour['VehicleTour']['date'])) . ' - ' . date('H:i', $start_hour_reservation);
		$end_date_reservation = date('d.m.Y', strtotime($tour['VehicleTour']['date'])) . ' - ' . date('H:i', $lastMoment['VehicleTourMoment']['hour']);

		if(!empty($tour['StockOrder'])){
			$this->loadModel('StockOrder');
			$deliveries = array();
			foreach($tour['StockOrder'] as $k => $order){
				$this->StockOrder->id = $order['id'];
				$packaging = $this->StockOrder->getPackaging();
				$order['packaging'] = $packaging;
				$tmp = $this->VehicleTour->VehicleTourMoment->find('first', array(
					'conditions' => array(
						'VehicleTourMoment.stock_order_id' => $order['id'],
						'VehicleTourMoment.vehicle_tour_id' => $tour['VehicleTour']['id']
					),
					'fields' => array('weight')
				));
				$tour['StockOrder'][$tmp['VehicleTourMoment']['weight']+100] = $order;
				if($order['StockOrdersVehicleTour']['type'] == 'delivery'){
					$deliveries[] = $order['order_number'];
				}
				unset($tour['StockOrder'][$k]);
			}
		}
		ksort($tour['StockOrder']);
		rsort($deliveries);

		$this->set(compact('tour'));
		$this->set(compact('deliveries'));
		$this->set(compact('moments'));
		$this->set(compact('duration'));
		$this->set('start_date_reservation', $start_date_reservation);
		$this->set('end_date_reservation', $end_date_reservation);

		if(!empty($this->request->params['ext']) && $this->request->params['ext'] == 'pdf'){
			$this->layout = 'default';
		}

		$filename = $tour['VehicleTour']['date'] . '_' . $tour['VehicleTour']['code'] . '.pdf';
		$this->pdfConfig = array(
				'filename' => $filename,
				'download' => false
		);

		$addresses = array();
		if(!empty($tour['StockOrder'])){
			foreach($tour['StockOrder'] as $order){
				switch($order['StockOrdersVehicleTour']['type']){
					case 'delivery':
					$addresses[] = urlencode($order['delivery_address'] . ' ' . $order['delivery_zip_city']);
					break;
					case 'return':
					$addresses[] = urlencode($order['return_address'] . ' ' . $order['return_zip_city']);
					break;
					default;
					break;
				}
			}
		}
		$waypoints = implode('|', $addresses);
		$originFestiloc = 'Route+du+Tir-Fédéral+10+1762+Givisiez';
		$url = 	'https://maps.googleapis.com/maps/api/directions/json?origin='.$originFestiloc
				.'&destination='.$originFestiloc
				.'&waypoints='.$waypoints.'&language=fr&key='
				. Configure::read('GoogleMapsAPIKey');
    $travel = json_decode($this->MyTools->get_data($url), true);

    $points = array();
    $markers = array();
    foreach($travel['routes'] as $route){
    	foreach($route['legs'] as $k => $leg){
    		$markers[] = '&markers=label:'.$k.'|' . $leg['start_location']['lat'] . ',' . $leg['start_location']['lng'];
    		foreach($leg['steps'] as $step){
    			$points[] = array($step['start_location']['lat'], $step['start_location']['lng']);
    			$points[] = array($step['end_location']['lat'], $step['end_location']['lng']);
    		}
    	}
    }
    $polyline = Polyline::Encode($points);
    $markers = implode('', $markers);

    $this->set(compact('travel'));
    $this->set(compact('polyline'));
    $this->set(compact('markers'));
	}

	public function update(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->response->body(json_encode($this->request->data));
			if(!empty($this->request->data['VehicleTour'])){
				if($this->VehicleTour->save($this->request->data)){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
			if(!empty($this->request->data['items'])){
				$moments = array();
				foreach($this->request->data['items'] as $item){
					$moment = array(
						'VehicleTourMoment' => array(
							'id' => empty($item['id']) ? '' : $item['id'],
							'stock_order_id' => empty($item['stock_order_id']) ? '' : $item['stock_order_id'],
							'vehicle_tour_id' => $item['tour'],
							'weight' => $item['weight'],
							'type' => empty($item['type']) ? '' : $item['type']
						)
					);
					$moments[] = $moment;
				}
				if($this->VehicleTour->VehicleTourMoment->saveMany($moments)){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

	public function assign_driver(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->VehicleTour->save($this->request->data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function notify_driver($tour_id){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			App::uses('CakePdf', 'CakePdf.Pdf');
			App::uses('CakeEmail', 'Network/Email');

			$this->loadModel('History');
			$this->loadModel('MyTools');

			$this->VehicleTour->contain(array(
				'StockOrder' => array(
					'ContactPeopleClient',
					'ContactPeopleDelivery',
					'ContactPeopleReturn',
					'StockOrderBatch' => array(
						'StockItem'
					),
					'Client'
				),
				'Driver'
			));
			$tour = $this->VehicleTour->findById($tour_id);
			$start_hour = empty($tour['VehicleTour']['start']) ? strtotime('00:00:00') : strtotime($tour['VehicleTour']['start']);

			$moments = $this->VehicleTour->VehicleTourMoment->get($tour_id);
			$moments = $this->VehicleTour->prepareMoments($moments, $tour, $start_hour);

			if(!empty($tour['StockOrder'])){
				$this->loadModel('StockOrder');
				$deliveries = array();
				foreach($tour['StockOrder'] as $k => $order){
					$this->StockOrder->id = $order['id'];
					$packaging = $this->StockOrder->getPackaging();
					$order['packaging'] = $packaging;
					$tmp = $this->VehicleTour->VehicleTourMoment->find('first', array(
						'conditions' => array(
							'VehicleTourMoment.stock_order_id' => $order['id'],
							'VehicleTourMoment.vehicle_tour_id' => $tour['VehicleTour']['id']
						),
						'fields' => array('weight')
					));
					$tour['StockOrder'][$tmp['VehicleTourMoment']['weight']+100] = $order;
					if($order['StockOrdersVehicleTour']['type'] == 'delivery'){
						$deliveries[] = $order['order_number'];
					}
					unset($tour['StockOrder'][$k]);
				}
			}
			ksort($tour['StockOrder']);
			rsort($deliveries);

			$addresses = array();
			if(!empty($tour['StockOrder'])){
				foreach($tour['StockOrder'] as $order){
					switch($order['StockOrdersVehicleTour']['type']){
						case 'delivery':
						$addresses[] = urlencode($order['delivery_address'] . ' ' . $order['delivery_zip_city']);
						break;
						case 'return':
						$addresses[] = urlencode($order['return_address'] . ' ' . $order['return_zip_city']);
						break;
						default;
						break;
					}
				}
			}
			$waypoints = implode('|', $addresses);
			$originFestiloc = 'Route+du+Tir-Fédéral+10+1762+Givisiez';
			$url = 	'https://maps.googleapis.com/maps/api/directions/json?origin='.$originFestiloc
					.'&destination='.$originFestiloc
					.'&waypoints='.$waypoints.'&language=fr&key='
					. Configure::read('GoogleMapsAPIKey');
	    $travel = json_decode($this->MyTools->get_data($url), true);

	    $points = array();
	    $markers = array();
	    foreach($travel['routes'] as $route){
	    	foreach($route['legs'] as $k => $leg){
	    		$markers[] = '&markers=label:'.$k.'|' . $leg['start_location']['lat'] . ',' . $leg['start_location']['lng'];
	    		foreach($leg['steps'] as $step){
	    			$points[] = array($step['start_location']['lat'], $step['start_location']['lng']);
	    			$points[] = array($step['end_location']['lat'], $step['end_location']['lng']);
	    		}
	    	}
	    }
	    $polyline = Polyline::Encode($points);
	    $markers = implode('', $markers);

			$history = array(
				'History' => array(
					'parent_id' => $tour_id,
					'reason' => 'notify_driver',
					'date' => date('Y-m-d H:i:s')
				)
			);
			$this->History->save($history);
			$filename = sprintf('Tour %s - %s.pdf', $tour['VehicleTour']['code'], date('d.m.Y'));
			$relativeUrl = 'files' . DS . 'documents' . DS . 'tours' . DS . $tour_id . DS . $this->History->id . DS . $filename;
			$absoluteUrl = WWW_ROOT . $relativeUrl;

			$CakePdf = new CakePdf();
			$CakePdf->template('driver_documents','default');
			$this->pdfConfig = array(
				'orientation' => 'portrait',
				'filename' => $filename
			);
			$CakePdf->viewVars(array(
				'tour' => $tour,
				'moments' => $moments,
				'travel' => $travel,
				'polyline' => $polyline,
				'markers' => $markers,
				'deliveries' => $deliveries
			));
			$CakePdf->write($absoluteUrl);


			$mail = new CakeEmail();
			$server = Configure::read('Debug.server');
			if($server == 'local'){
				$to = 'denis@une-bonne-idee.ch';
			} elseif($server == 'test'){
				$to = 'dev@myubic.ch';
			} elseif($server == 'prod'){
				$to = $tour['Driver']['email'];
			}
			$mail->from(array(AuthComponent::user('email') => AuthComponent::user('first_name')))
					 ->to($to)
					 ->subject(__('Tour %s - %s', $tour['VehicleTour']['code'], date('d.m.Y', strtotime($tour['VehicleTour']['date']))))
					 ->emailFormat('html')
					 ->template('notify_driver')
					 ->viewVars(array(
							'first_name' => $tour['Driver']['first_name'],
							'code' => $tour['VehicleTour']['code'],
							'date' => $tour['VehicleTour']['date'],
							'loggedUser' => AuthComponent::user('first_name')
							));
			$mail->attachments(array(
				$filename => array(
					'file' => $relativeUrl,
					'mimetype' => 'application/pdf'
				)
			));
			if($mail->send()){
					$this->response->body(json_encode(array('success' => 1)));
			}
		}
	}

}
