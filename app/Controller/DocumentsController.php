<?php

App::uses('Folder', 'Utility');

class DocumentsController extends AppController {

	public function places(){
		exit;
		$docs = $this->Document->find('all', array(
			'conditions' => array(
				'category' => 'place'
			)
		));
		foreach($docs as $doc){
			// if($doc['Document']['category'] == 'client-photo'){
			// 	$doc['Document']['group'] = 'client';
			// }
			// $doc['Document']['category'] = 'place';
			$doc['Document']['url'] = str_replace('/photo/', '/place/', $doc['Document']['url']);
			//$doc['Document']['url'] = str_replace('internal-photo', 'photo', $doc['Document']['url']);
			$this->Document->save($doc);
		}
		exit;
	}

	public function upload (){
		$this->autoRender = false;
		if($this->Document->saveFile(
			$_FILES['file'],
			$this->request->data['Document']['parent_id'],
			array($this->request->data['Document']['controller'], $this->request->data['Document']['category']),
			$this->request->data['Document']['category'],
			$this->request->data['Document']['extensions']
		)){
			return true;
		} else {
			return false;
		}
	}

	public function rename($id, $newName){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->Document->id = $id;
			if($this->Document->saveField('name', $newName)){
				$output = array('success' => 1);
			} else {
				$output = array('success' => 0);
			}
			$this->response->body(json_encode($output));
		}
	}

	public function edit($id, $newName, $group = null){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->Document->id = $id;
			$category = $this->Document->field('category');
			$refresh = 0;
			if($category == 'stockitem'){ // we need to rename filename and url too!
				$name = $this->Document->field('name');
				$url = $this->Document->field('url');
				App::uses('File', 'Utility');
				$file = new File(WWW_ROOT . $url);
				$filename = basename($url);
				$newUrl = str_replace($filename, $newName, $url);
				if($file->copy(WWW_ROOT . $newUrl)){
					$this->Document->saveField('filename', $newName);
					$this->Document->saveField('url', $newUrl);
					$refresh = 1;
				}
			}
			if( $this->Document->saveField('name', $newName) && $this->Document->saveField('group', $group)){
				$output = array('success' => 1, 'refresh' => $refresh);
			} else {
				$output = array('success' => 0);
			}
			$this->response->body(json_encode($output));
		}
	}

	public function delete(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if(!empty($_POST['documentId'])){
				$document = $this->Document->findById($_POST['documentId']);
				if($this->Document->delete($_POST['documentId'])){
					unlink(WWW_ROOT . $document['Document']['url']);
					$output = array('success' => 1);
				} else {
					$output = array('success' => 0);
				}
			}
			$this->response->body(json_encode($output));
		}
	}

	public function editGroup($id, $group){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->Document->id = $id;
			if( $this->Document->saveField('group', $group)){
				$groups = Configure::read('Documents.Images.groups');
				$output = array('success' => 1);
				$output = array('group' => $groups[$group]);
			} else {
				$output = array('success' => 0);
			}
			$this->response->body(json_encode($output));
		}
	}

}
