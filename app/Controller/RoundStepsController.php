<?php

class RoundStepsController extends AppController {

  public function send($id = null){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($id)){
        $this->RoundStep->id = $id;
        if($this->RoundStep->send()){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }
    }
  }

  public function save($id = null){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data['RoundStep'])){
        if(!empty($this->request->data['RoundStep']['date'])){
          $this->request->data['RoundStep']['date'] = date('Y-m-d', strtotime($this->request->data['RoundStep']['date']));
        }
        if(empty($id)){
          if($this->RoundStep->save($this->request->data)){
            $this->response->body(json_encode(array('success' => 1)));
          } else {
            $this->response->body(json_encode(array('success' => 0)));
          }
        }
      }
    }
  }
}
