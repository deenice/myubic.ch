<?php

class WishesController extends AppController {

  public function compose(){
    $this->loadModel('Event');
    $wishes = $this->Wish->find('all', array(
      'contain' => array(
        'Client' => array('fields' => array('name')),
        'ContactPeople' => array('fields' => array('full_name', 'email')),
        'User' => array('fields' => array('full_name')),
      ),
      'conditions' => array(
        //'user_id' => AuthComponent::user('id')
        //'user_id' => 15
      ),
      'order' => array('sent ASC', 'User.full_name', 'Client.name')
    ));
    foreach($wishes as $k => $wish){
      $wishes[$k]['events'] = array();
      $events = unserialize($wish['Wish']['events']);
      foreach($events as $eventId){
        $event = $this->Event->find('first', array(
          'contain' => array(
            'Date' => array('fields' => array('date'))
          ),
          'conditions' => array(
            'id' => $eventId
          ),
          'fields' => array('confirmed_date', 'period_start', 'period_end', 'crm_status')
        ));
        $wishes[$k]['events'][] = $event;
      }
    }
    $this->set(compact('wishes'));
    $this->set('statuses', Configure::read('Events.crm_status'));
  }

  public function prepare(){
    $this->loadModel('Event');
    $this->Event->contain('Client');
    $this->Wish->deleteAll(array('Wish.id <>' => null));
    $events = $this->Event->find('all', array(
      'joins' => array(
        array(
          'table' => 'contact_peoples',
          'alias' => 'cp',
          'conditions' => array(
            'cp.id = Event.contact_people_id',
            'cp.email <>' => null
          )
        )
      ),
      'conditions' => array(
        'Event.company_id' => 2,
        'Event.type' => 'standard',
        'Event.client_id <>' => null,
        'resp_id <>' => null,
        'crm_status <>' => null,
        'OR' => array(
          array(
            array('opening_date >=' => '2016-01-01'),
            array('opening_date <=' => '2016-12-31')
          ),
          array(
            array('confirmed_date >=' => '2016-01-01'),
            array('confirmed_date <=' => '2016-12-31')
          )
        )
      ),
      'fields' => array('id', 'client_id', 'contact_people_id', 'resp_id', 'crm_status', 'Client.name')
    ));
    foreach($events as $event){
      if(empty($event['Event']['crm_status'])){ continue; }
      $existingWish = $this->Wish->find('first', array(
        'conditions' => array(
          'client_id' => $event['Event']['client_id']
        )
      ));
      $url = 'http://www.une-bonne-idee.ch/voeux2017?client=' . urlencode($event['Client']['name']);
      if(empty($existingWish)){
        $wish = array(
          'Wish' => array(
            'client_id' => $event['Event']['client_id'],
            'contact_people_id' => $event['Event']['contact_people_id'],
            'user_id' => $event['Event']['resp_id'],
            'events' => serialize(array($event['Event']['id'])),
            'url' => $url
          )
        );
        $this->Wish->create();
        $this->Wish->save($wish);
      } else {
        $history = unserialize($existingWish['Wish']['events']);
        $history[] = $event['Event']['id'];
        array_unique($history);
        $existingWish['Wish']['events'] = serialize($history);
        $this->Wish->save($existingWish);
      }
    }
    return $this->redirect(array('action' => 'compose'));
  }

  public function send(){
    if($this->request->is('post') || $this->request->is('put')){
      if(!empty($this->request->data['SendWish']['send'])){

        foreach($this->request->data['SendWish']['send'] as $wishId){
          $this->Wish->id = $wishId;
          $wish = $this->Wish->find('first', array(
            'contain' => array(
              'ContactPeople' => array('fields' => array('first_name', 'last_name')),
              'User' => array('fields' => array('full_name'))
            ),
            'conditions' => array(
              'Wish.id' => $wishId
            )
          ));
          $message = $this->request->data['SendWish']['message'];
          $message = str_replace('%first_name%', $wish['ContactPeople']['first_name'], $message);
          $message = str_replace('%last_name%', $wish['ContactPeople']['last_name'], $message);
          $message = str_replace('%url%', $wish['Wish']['url'], $message);
          $message = str_replace('%resp_full_name%', $wish['User']['full_name'], $message);
          $this->Wish->saveField('message', $message);
          if($this->Wish->send()){
            $this->Wish->saveField('sent', 1);
          }
        }
        $this->Session->setFlash(__('Les voeux ont été envoyés.'), 'alert', array('type' => 'success'));
        return $this->redirect(array('action' => 'compose'));
      } else {
        return $this->redirect(array('action' => 'compose'));
      }
    }
  }

}