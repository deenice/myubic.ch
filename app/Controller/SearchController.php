<?php

App::uses('GeocodeLib', 'Tools.Lib');
App::uses('CakeTime', 'Utility');

class SearchController extends AppController {

	public $components = array('Paginator');

	public function index( ){

	  $this->loadModel('Client');
	  $this->loadModel('Partner');
	  $this->loadModel('ContactPeople');
	  $this->loadModel('Event');
	  $this->loadModel('StockOrder');

	  $term = $this->request->query['term'];
	  $this->set(compact('term'));

	  $clients = $this->Client->find('all', array(
			'contain' => array('Company', 'ContactPeople'),
			'conditions' => array(
			  'OR' => array(
					'Client.name LIKE' => '%' . $term . '%',
					'Client.id LIKE' => '%' . $term . '%'
			  )
			),
			'order' => 'Client.name'
	  ));

	  $contacts = $this->ContactPeople->find('all', array(
			'contain' => array('Client' => array('Company')),
			'conditions' => array(
			  'OR' => array(
					'ContactPeople.first_name LIKE' => '%' . $term . '%',
					'ContactPeople.last_name LIKE' => '%' . $term . '%',
					'ContactPeople.email LIKE' => '%' . $term . '%'
			  )
			),
			'order' => 'ContactPeople.full_name'
	  ));

	  $events = $this->Event->find('all', array(
			'joins' => array(
				array(
					'table' => 'clients',
					'alias' => 'c',
					'conditions' => array('c.id = Event.client_id')
				)
			),
			'contain' => array('Client' => array('Company')),
			'conditions' => array(
			  'OR' => array(
					'Event.code LIKE' => '%' . $term . '%',
					'Event.name LIKE' => '%' . $term . '%',
					'c.name LIKE' => '%' . $term . '%'
			  )
			),
			'order' => 'Event.code',
			'group' => 'Event.id'
	  ));

	  $orders = $this->StockOrder->find('all', array(
			'joins' => array(
				array(
					'table' => 'clients',
					'alias' => 'c',
					'conditions' => array('c.id = StockOrder.client_id')
				)
			),
			'contain' => array('Client', 'ContactPeopleClient'),
			'conditions' => array(
			  'OR' => array(
					'StockOrder.name LIKE' => '%' . $term . '%',
					'StockOrder.id LIKE' => '%' . $term . '%',
					'c.name LIKE' => '%' . $term . '%',
					'c.id LIKE' => '%' . $term . '%'
			  )
			),
			'order' => 'StockOrder.id'
	  ));

	  $results = array_merge($clients, $contacts, $events, $orders);
	  $this->set(compact('results'));
		$this->set('sizes', array(
			'clients' => sizeof($clients),
			'events' => sizeof($events),
			'contacts' => sizeof($contacts),
			'orders' => sizeof($orders),
			'total' => sizeof($results)
		));
	}

	public function place($id = null){
		$this->loadModel('Place');
		$this->Place->contain(array(
			'Thumbnail',
			'Option',
			'Configuration'
		));
		$place = $this->Place->findById($id);
		$this->layout = 'place';
		$this->set(compact('place'));
	}

	public function places($id = null, $page = null){

		$this->Search->contain(array(
			'Event' => array('Client', 'Date', 'Moment'),
			'User'
		));
		$this->loadModel('Place');
		$this->loadModel('Client');
		$this->loadModel('Event');

		$this->Geocode = new GeocodeLib();

		$search = $this->Search->read(null, $id);
		$this->set('data', $search);

		$clients = $this->Client->find('list');
		$this->set(compact('clients'));

		$events1 = $this->Event->find('all', array(
			'contain' => 'Client',
			'conditions' => array(
				'Event.confirmed_date >=' => date('Y-m-d'),
				'Event.type' => 'standard'
			),
			'order' => 'Event.confirmed_date',
			'fields' => array('Event.code_name', 'Event.id', 'Event.confirmed_date', 'Client.name')
		));
		foreach($events1 as $event){
			$events[$event['Event']['id']] = $event['Event']['code_name'] . ' - ' . $event['Client']['name'] . ' (' . date('d.m.Y', strtotime($event['Event']['confirmed_date'])) . ')';
		}
		$this->set(compact('events'));

		$complexSearch = false;
		if(empty($search['Search']['event_id']) OR empty($search['Search']['zip_city'])){
			$complexSearch = false;
		} else {
			$complexSearch = true;
		}
		$this->set(compact('complexSearch'));

		$displayResults = true;
		if(isset($this->request->query['places'])){
			if($this->request->query['places'] == 0){
				$displayResults = false;
			}
		}
		$this->set(compact('displayResults'));

		// Create/edit search instance
		if($this->request->is('post') || $this->request->is('put')){

			if(isset($this->request->data['Search']['id'])){
				//$this->Search->id = $id;
			} else {
				$this->Search->create();
			}

			if(!empty($this->request->data['Search']['place_types'])){
				$this->request->data['Search']['place_type'] = serialize($this->request->data['Search']['place_types']);
			} else {
				$this->request->data['Search']['place_type'] = '';
			}

			if ($this->Search->saveAssociated($this->request->data)) {
				if(!isset($id)) $id = $this->Search->getLastInsertId();
				$this->Session->setFlash(__('Search has been saved.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('action' => 'places', $id));
			} else {
				$this->Session->setFlash(__('Search has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->request->data = $search;
		}

		if(empty($id)){
			//debug(AuthComponent::user('id'));exit;
			// get previous searches (not archived) for connecting user
			$previousSearches = $this->Search->find('all', array(
				'conditions' => array(
					'Search.user_id' => AuthComponent::user('id'),
					'Search.model' => 'place',
					'Search.archived' => 0
				)
			));
			$this->set(compact('previousSearches'));
		}

		// get event information if selected, return event date otherwise
		$dates = array();
		if($complexSearch){
			if(!empty($search['Event']['confirmed_date']) AND $search['Event']['confirmed_date'] != '0000-00-00'){
				$dates[0]['date'] = $search['Event']['confirmed_date'];
				$dates[0]['status'] = 'confirmed';
			} elseif(!empty($search['Event']['Date'])){
				foreach($search['Event']['Date'] as $k => $date){
					$dates[$k]['date'] = $date['date'];
					$dates[$k]['status'] = 'potential';
				}
			}
			$this->set(compact('dates'));

			// get moments of event
			if(!empty($search['Event']['Moment'])){
				$this->set('moments', $search['Event']['Moment']);
			}
		}


		// get places according to search criteria
		if(!empty($this->request->data['Search']) AND $displayResults){

			if(!empty($this->request->data['Search']['place_zip'])){
				$this->loadModel('Commune');
				$commune = $this->Commune->find('first', array(
					'conditions' => array(
						'name LIKE' => $this->request->data['Search']['place_city'],
						'zip LIKE' => $this->request->data['Search']['place_zip']
					)
				));
				$this->set(compact('commune'));
			}

			$options = array();

			$options['joins'] = array(
				array(
					'table' => 'places_tags',
					'alias' => 'PlacesTags',
					'type' => 'LEFT',
					'foreignKey' => false,
					'conditions' => array(
						'PlacesTags.place_id = Place.id',
					)
				),
				array('table' => 'tags',
					'alias' => 'IdealFor',
					'type' => 'LEFT',
					'conditions' => array(
						'IdealFor.id = PlacesTags.tag_id',
						'IdealFor.category = "ideal_for"'
					)
				)
			);

			if(!empty($this->request->data['Search']['place_number_of_persons'])){
				$options['conditions'][] = array(
					'OR' => array(
						'Place.max_number_of_persons >=' => $this->request->data['Search']['place_number_of_persons'],
						'Place.max_number_of_persons' => 0
						)
					);
			}
			if(!empty($this->request->data['Search']['place_type'])){
				$options['conditions'][] = array('type' => unserialize($this->request->data['Search']['place_type']));
			}
			if(!empty($this->request->data['Search']['place_parking'])){
				$options['conditions'][] = array('parking' => 1);
			}
			if(!empty($this->request->data['Search']['place_reduced_mobility'])){
				$options['conditions'][] = array('reduced_mobility' => 1);
			}
			if(!empty($this->request->data['Search']['place_internal_caterer'])){
				$options['conditions'][] = array('internal_caterer' => 1);
			}
			if(!empty($this->request->data['Search']['place_external_caterer'])){
				$options['conditions'][] = array('external_caterer' => 1);
			}
			if(!empty($this->request->data['Search']['place_number_of_rooms'])){
				$options['conditions'][] = array('configuration_count >=' => $this->request->data['Search']['place_number_of_rooms']);
			}
			if(!empty($this->request->data['Search']['place_maximum_surface'])){
				$options['conditions'][] = array('max_surface <=' => $this->request->data['Search']['place_maximum_surface']);
			}
			if(!empty($this->request->data['Search']['place_number_of_rooms'])){
				$options['conditions'][] = array('number_of_rooms >=' => $this->request->data['Search']['place_number_of_rooms']);
			}
			if(!empty($this->request->data['Search']['place_ideal_for'])){
				$options['conditions'][] = array('IdealFor.value' => $this->request->data['Search']['place_ideal_for']);
			}
			if(!empty($this->request->data['Search']['place_degree'])){
				$options['conditions'][] = array('degree >=' => $this->request->data['Search']['place_degree']);
			}


			$sf = 3.14159 / 180; // scaling factor
			$er = 6371; // earth radius in km, approximate
			$mr = !empty($this->request->data['Search']['radius']) ? $this->request->data['Search']['radius'] : 50; // max radius
			$placesToShow = 100;

			if(!empty($commune)){
				$lat = $commune['Commune']['latitude'];
				$lon = $commune['Commune']['longitude'];
				$travelOrigin = $commune['Commune']['zip'] . '+' . $commune['Commune']['name'];
				if($lat && $lon){
				  $options['conditions'][] = array("$er * ACOS(SIN(Place.latitude*$sf)*SIN($lat*$sf) + COS(Place.latitude*$sf)*COS($lat*$sf)*COS((Place.longitude - $lon)*$sf)) <=" => $mr);
				  $options['order'][] = "ACOS(SIN(latitude*$sf)*SIN($lat*$sf) + COS(latitude*$sf)*COS($lat*$sf)*COS((longitude - $lon)*$sf))";
				}
			}
			$options['group'] = array('Place.id');
			$options['fields'] = array('Place.id', 'Place.name', 'Place.latitude', 'Place.longitude', 'Place.degree');
			if(!$complexSearch){
				$numberOfPlaces = $this->Place->find('count', $options);
				$numberOfPages = ceil($numberOfPlaces / $placesToShow);
				$activePage = !empty($page) ? $page : 0;
				$prevPage = $activePage - 1 < 0 ? 0 : $activePage - 1;
				$nextPage = $activePage + 1 > $numberOfPages - 1 ? $numberOfPages - 1 : $activePage + 1;
				$offset = $activePage * $placesToShow;
				$options['order'] = 'Place.name';
				$options['limit'] = $placesToShow;
				$options['offset'] = $activePage * $placesToShow;
				$this->set(compact('numberOfPages'));
				$this->set(compact('activePage'));
				$this->set(compact('prevPage'));
				$this->set(compact('nextPage'));
			}
			$places = $this->Place->find('all', $options);
			unset($options['limit']);
			$numberOfPlaces = $this->Place->find('count', $options);

			/*$places = array();
			foreach($data as $k => $place){
				if(!empty($commune)){
					$pointA = array('lat' => $commune['Commune']['latitude'], 'lng' => $commune['Commune']['longitude'], 'title' => $commune['Commune']['name']);
					$pointB = array('lat' => $place['Place']['latitude'], 'lng' => $place['Place']['longitude']);
					$pointUbic = array('lat' => 46.7997, 'lng' => 7.11928);

					$travelOrigin = $commune['Commune']['zip'] . '+' . $commune['Commune']['name'];
					$travelDestination = $place['Place']['zip'] . '+' . $place['Place']['city'];

					$distance = $this->Geocode->distance($pointA, $pointB);
					$distanceFromUbic = $this->Geocode->distance($pointB, $pointUbic);

					if(!empty($this->request->data['Search']['radius']) && $this->request->data['Search']['radius'] >= $distance){
						$places[$k] = $place;
						$places[$k]['Place']['distance'] = $distance;
						$places[$k]['Place']['distance_from_ubic'] = $distanceFromUbic;
					} elseif (empty($this->request->data['Search']['radius'])) {
						$places[$k] = $place;
						$places[$k]['Place']['distance'] = $distance;
						$places[$k]['Place']['distance_from_ubic'] = $distanceFromUbic;
					}
					$this->set('origin', $pointA);
				}
			}*/
			foreach($places as $k => $place){
				if($place['Place']['degree'] == 0){
					$notInterestingPlaces[] = $place;
					unset($places[$k]);
				}
			}

			$this->set(compact('places'));
			$this->set(compact('notInterestingPlaces'));
			$this->set(compact('numberOfPlaces'));

		}

	}

	public function getDistance(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->Geocode = new GeocodeLib();
			$pointA = array('lat' => $_POST['latA'], 'lng' => $_POST['lngA']);
			$pointB = array('lat' => $_POST['latB'], 'lng' => $_POST['lngB']);
			$distance = $this->Geocode->distance($pointA, $pointB);
			$this->response->body(json_encode($distance));
		}
	}

	public function getTravelTime(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			if(!empty($_POST['origin']) && !empty($_POST['destination'])){

				$origin = str_replace(' ', '+', $_POST['origin']);
				$originUBIC = 'Route+du+Petit-Moncor+1c+1752+Villars-sur-Glane';
				$destination = str_replace(' ', '+', $_POST['destination']);

				$this->loadModel('Direction');
				$data = $this->Direction->find('first', array(
					'conditions' => array(
						'origin' => $origin,
						'destination' => $destination
					)
				));
				$dataUbic = $this->Direction->find('first', array(
					'conditions' => array(
						'origin' => $originUBIC,
						'destination' => $destination
					)
				));
				if(!empty($data['Direction']['time']) && !empty($data['Direction']['distance'])){
					$infos['time'] = $data['Direction']['time'];
					$infos['distance'] = $data['Direction']['distance'];
				} else {
					$url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey');
					$travel = json_decode(file_get_contents($url), true);
					if($travel['status'] == 'OK'){
						$infos['distance'] = $travel['routes'][0]['legs'][0]['distance']['text'];
						$infos['time'] = $travel['routes'][0]['legs'][0]['duration']['text'];
						$this->Direction->create();
						$direction = array(
							'Direction' => array(
								'origin' => $origin,
								'destination' => $destination,
								'time' => $infos['time'],
								'distance' => $infos['distance']
							)
						);
						$this->Direction->save($direction);
					} else {
						$infos['time'] = 0;
						$infos['distance'] = 0;
					}
				}
				if(!empty($dataUbic)){
					$infos['timeUBIC'] = $dataUbic['Direction']['time'];
					$infos['distanceUBIC'] = $dataUbic['Direction']['distance'];
				} else {
					$travel2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$originUBIC.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey')), true);
					if($travel2['status'] == 'OK'){
						$infos['distanceUBIC'] = $travel2['routes'][0]['legs'][0]['distance']['text'];
						$infos['timeUBIC'] = $travel2['routes'][0]['legs'][0]['duration']['text'];
						$this->Direction->create();
						$direction = array(
							'Direction' => array(
								'origin' => $originUBIC,
								'destination' => $destination,
								'time' => $infos['timeUBIC'],
								'distance' => $infos['distanceUBIC']
							)
						);
						$this->Direction->save($direction);
					} else {
						$infos['timeUBIC'] = 0;
						$infos['distanceUBIC'] = 0;
					}
				}
				$this->response->body(json_encode($infos));
			} else {
				$this->response->body(json_encode(array(__('not available'))));
			}
		}
	}

	public function users($id = null){

		$this->Search->contain(array(
			'Event' => array('Client', 'Date'),
			'User'
		));
		$this->loadModel('Place');
		$this->loadModel('Client');
		$this->loadModel('Event');

		$search = $this->Search->read(null, $id);
		$this->set('data', $search);

		$events1 = $this->Event->find('all', array(
			'contain' => 'Client',
			'conditions' => array(
				'OR' => array(
					array('Event.confirmed_date >=' => date('Y-m-d'))
				)
			)
		));
		foreach($events1 as $event){
			$events[$event['Event']['id']] = $event['Event']['name'] . ' - ' . $event['Client']['name'];
		}
		$this->set(compact('events'));

		$clients = $this->Client->find('list');
		$this->set(compact('clients'));

		// Get all activities (UBIC and UG)
		$this->loadModel('Activity');
		$activities = $this->Activity->find('list', array(
			'contain' => 'Company',
			'conditions' => array(
				'category' => null
			),
			'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
			'order' => 'Activity.company_id, Activity.name'
		));
		$this->set(compact('activities'));

		// Create/edit search instance
		if($this->request->is('post') || $this->request->is('put')){
			//debug($this->request->data);exit;
			if(isset($this->request->data['Search']['id'])){
				//$this->Search->id = $id;
			} else {
				$this->Search->create();
			}

			if ($this->Search->saveAssociated($this->request->data)) {
				if(!isset($id)) $id = $this->Search->getLastInsertId();
				$this->Session->setFlash(__('Search has been saved.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('action' => 'users', $id));
			} else {
				$this->Session->setFlash(__('Search has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->request->data = $this->Search->read(null, $id);
		}

		// get users according to search criteria
		if(!empty($this->request->data['Search'])){

			$availabilitiesTags = array();
			$users = array();

			if(!empty($this->request->data['Search']['event_id'])){
				$event = $this->Event->find('first', array(
					'contain' => array(
						'Job'
					),
					'conditions' => array(
						'Event.id' => $this->request->data['Search']['event_id']
					)
				));
				$availabilitiesTagsByDay = Configure::read('Users.availabilitiesTagsByDay');
				$confirmed_day = strtolower(date('l', strtotime($event['Event']['confirmed_date'])));
				$availabilitiesTags = $availabilitiesTagsByDay[$confirmed_day];

				$options['joins'] = array(
					array(
						'table' => 'tags_users',
						'alias' => 'TagsUsers',
						'type' => 'INNER',
						'foreignKey' => false,
						'conditions' => array(
							'TagsUsers.user_id = User.id',
						)
					),
					array(
						'table' => 'tags_users',
						'alias' => 'TagsUsers1',
						'type' => 'INNER',
						'foreignKey' => false,
						'conditions' => array(
							'TagsUsers1.user_id = User.id',
						)
					),
					array('table' => 'competences',
						'alias' => 'Competence',
						'type' => 'LEFT', // Set to LEFT otherwise the users without competence won't be retrieved!
						'conditions' => array(
							'Competence.user_id = User.id',
						)
					),
					array('table' => 'tags',
						'alias' => 'Availabilities',
						'type' => 'INNER',
						'conditions' => array(
							'Availabilities.id = TagsUsers.tag_id',
							'Availabilities.category = "availability"'
						)
					),
					array('table' => 'tags',
						'alias' => 'Section',
						'type' => 'LEFT',
						'conditions' => array(
							'Section.id = TagsUsers1.tag_id',
							'Section.category = "section"'
						)
					)
				);

				$options['group'] = array('User.id');

				foreach($event['Job'] as $k => $job){

					$options['contain'] = array(
						'Availabilities' => array(
							'conditions' => array(
								'TagsUser.tag_id IN' => $availabilitiesTags
							)
						),
						'Job',
						'Talents',
						'Portrait',
						'StaffQueue' => array(
							'conditions' => array(
								'StaffQueue.job_id' => $job['id']
							),
							'limit' => 1
						),
						'Competence' => array(
							'conditions' => array(
								'Competence.job' => $job['job'],
								'Competence.sector' => $job['sector'],
								'Competence.activity_id' => empty($job['activity_id']) ? null : $job['activity_id']
							)
						)
					);

					//http://stackoverflow.com/questions/1751686/conditions-in-associated-models-using-model-find-cakephp
					// General conditions
					$options['conditions'] = array();
					$options['conditions'][] = array('User.role IN' => array('fixed', 'temporary', 'candidate'));
					$options['conditions'][] = array(
						'OR' => array(
							'User.end_of_collaboration >=' => $event['Event']['confirmed_date'],
							'User.end_of_collaboration' => null
						)
					);
					/*array_push($options['joins'],
						array('table' => 'jobs',
							'alias' => 'Job'.$k,
							'type' => 'INNER',
							'conditions' => array(
								'Job'.$k.'.user_id <> User.id'
							)
						)
					);*/

					// Conditions according to availabilities and job requirements
					if(!empty($availabilitiesTags)){
						$options['conditions'][] = array('Availabilities.id IN' => $availabilitiesTags);
					}
					if(!empty($job['sector'])){
						if($job['sector'] == 'animation'){
							$sectorId = 173;
						} elseif($job['sector'] == 'logistics'){
							$sectorId = 174;
						} elseif($job['sector'] == 'fb'){
							$sectorId = 175;
						}
						$options['conditions'][] = array(
							'OR' => array(
								'Section.id' => $sectorId,
								'Competence.sector' => $job['sector']
							)
						);
					}
					if(!empty($job['job'])){
						$options['conditions'][] = array('Competence.job' => $job['job']);
					}
					if(!empty($job['hierarchy'])){
						if($job['hierarchy'] <= 2){
							$options['conditions'][] = array('Competence.hierarchy >=' => 1);
						} else {
							$options['conditions'][] = array('Competence.hierarchy >=' => $job['hierarchy']);
						}
					}
					if(!empty($job['activity_id'])){
						$options['conditions'][] = array('Competence.activity_id' => $job['activity_id']);
					}
					if(!empty($this->request->data['Search']['french_level'])){
						$options['conditions'][] = array('User.french_level >=' => $this->request->data['Search']['french_level']);
					}
					if(!empty($this->request->data['Search']['german_level'])){
						$options['conditions'][] = array('User.german_level >=' => $this->request->data['Search']['german_level']);
					}
					if(!empty($this->request->data['Search']['english_level'])){
						$options['conditions'][] = array('User.english_level >=' => $this->request->data['Search']['english_level']);
					}
					$this->loadModel('User');
					$users[$job['id']] = $this->User->find('all', $options);
					$jobs[$job['id']] = $job;
					$this->set(compact('users'));
					$this->set(compact('jobs'));
				}
			}
		}
	}

	public function history($user_id = null){
		$searches['places'] = $this->Search->find('all', array(
			'contain' => array('Event' => 'Client', 'User'),
			'conditions' => array(
				'Search.user_id' => AuthComponent::user('id'),
				'Search.model' => 'place',
				'Search.archived' => 0
			)
		));
		$searches['users'] = $this->Search->find('all', array(
			'contain' => array('Event' => 'Client', 'User'),
			'conditions' => array(
				'Search.user_id' => AuthComponent::user('id'),
				'Search.model' => 'user'
			)
		));
		$this->set(compact('searches'));
	}

	public function archive($id = null){
		$this->Search->id = $id;
		if($this->Search->saveField('archived', 1)){
			$this->Session->setFlash(__('Search has been archived.'), 'alert', array('type' => 'success'));
			if(!empty($this->request->query['destination'])){
				if($this->request->query['destination'] == 'places'){
					return $this->redirect(array('action' => 'places'));
				} else {
					return $this->redirect(array('action' => 'history'));
				}
			} else {
				return $this->redirect(array('action' => 'history'));
			}
		} else {
			$this->Session->setFlash(__('Search has not been archived.'), 'alert', array('type' => 'error'));
			return $this->redirect(array('action' => 'history'));
		}
	}




	public function places1($id = null, $page = null){

		$this->Search->contain(array(
			'Event' => array('Client', 'Date', 'Moment'),
			'User'
		));
		$this->loadModel('Place');
		$this->loadModel('Client');
		$this->loadModel('Event');

		$search = $this->Search->read(null, $id);
		$this->set('data', $search);

		$clients = $this->Client->find('list');
		$this->set(compact('clients'));
		$events = $this->Event->find('list', array(
			'joins' => array(
				array(
					'table' => 'clients',
					'alias' => 'Client',
					'conditions' => array(
						'Client.id = Event.client_id'
					)
				)
			),
			'contain' => 'Date',
			'conditions' => array(
				'OR' => array(
					array('Event.confirmed_date >=' => date('Y-m-d')),
					array('Event.confirmed_date' => null)
				)
			),
			'order' => array('Event.confirmed_date DESC', 'Event.created DESC'),
			'fields' => array('Event.id', 'Event.name', 'Client.name')
		));
		$this->set(compact('events'));

		if(empty($id)){
			// get previous searches (not archived) for connecting user
			$previousSearches = $this->Search->find('all', array(
				'conditions' => array(
					'Search.user_id' => AuthComponent::user('id'),
					'Search.model' => 'place',
					'Search.archived' => 0
				)
			));
			$this->set(compact('previousSearches'));
		}

		$complexSearch = false;
		if(empty($search['Search']['event_id']) OR empty($search['Search']['zip_city'])){
			$complexSearch = false;
		} else {
			$complexSearch = true;
		}
		$this->set(compact('complexSearch'));

		// get event information if selected, return event date otherwise
		$dates = array();
		if($complexSearch){
			if(!empty($search['Event']['confirmed_date']) AND $search['Event']['confirmed_date'] != '0000-00-00'){
				$dates[0]['date'] = $search['Event']['confirmed_date'];
				$dates[0]['status'] = 'confirmed';
			} elseif(!empty($search['Event']['Date'])){
				foreach($search['Event']['Date'] as $k => $date){
					$dates[$k]['date'] = $date['date'];
					$dates[$k]['status'] = 'potential';
				}
			}
			$this->set(compact('dates'));

			// get moments of event
			if(!empty($search['Event']['Moment'])){
				$this->set('moments', $search['Event']['Moment']);
			}
		}

		// Create/edit search instance
		if($this->request->is('post') || $this->request->is('put')){

			if(isset($this->request->data['Search']['id'])){
				$this->Search->id = $id;
			} else {
				$this->Search->create();
			}

			if(!empty($this->request->data['Search']['place_types'])){
				$this->request->data['Search']['place_type'] = serialize($this->request->data['Search']['place_types']);
			} else {
				$this->request->data['Search']['place_type'] = '';
			}

			if ($this->Search->saveAssociated($this->request->data)) {
				$id = $this->Search->id;
				$this->Session->setFlash(__('Search has been saved.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('action' => 'places1', $id));
			} else {
				$this->Session->setFlash(__('Search has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$this->request->data = $search;
		}

		$displayResults = true;
		if(isset($this->request->query['places'])){
			if($this->request->query['places'] == 0){
				$displayResults = false;
			}
		}
		$this->set(compact('displayResults'));

		// get places according to search criteria
		if(!empty($this->request->data['Search']) AND $displayResults){

			$this->Geocode = new GeocodeLib();

			if(!empty($this->request->data['Search']['place_zip'])){
				$this->loadModel('Commune');
				$commune = $this->Commune->find('first', array(
					'conditions' => array(
						'name LIKE' => $this->request->data['Search']['place_city'],
						'zip LIKE' => $this->request->data['Search']['place_zip']
					)
				));
				$this->set(compact('commune'));
			}

			$options = array();
			$options['limit'] = 6;
			$options['joins'] = array(
				array(
					'table' => 'places_tags',
					'alias' => 'PlacesTags',
					'type' => 'LEFT',
					'foreignKey' => false,
					'conditions' => array(
						'PlacesTags.place_id = Place.id',
					)
				),
				array('table' => 'tags',
					'alias' => 'IdealFor',
					'type' => 'LEFT',
					'conditions' => array(
						'IdealFor.id = PlacesTags.tag_id',
						'IdealFor.category = "ideal_for"'
					)
				)
			);

			$options['contain'] = array(
				'Thumbnail'
			);

			if(!empty($this->request->data['Search']['place_number_of_persons'])){
				$options['conditions'][] = array(
					'OR' => array(
						'Place.max_number_of_persons >=' => $this->request->data['Search']['place_number_of_persons'],
						'Place.max_number_of_persons' => 0
						)
					);
			}
			if(!empty($this->request->data['Search']['place_type'])){
				$options['conditions'][] = array('type' => unserialize($this->request->data['Search']['place_type']));
			}
			if(!empty($this->request->data['Search']['place_parking'])){
				$options['conditions'][] = array('parking' => 1);
			}
			if(!empty($this->request->data['Search']['place_reduced_mobility'])){
				$options['conditions'][] = array('reduced_mobility' => 1);
			}
			if(!empty($this->request->data['Search']['place_internal_caterer'])){
				$options['conditions'][] = array('internal_caterer' => 1);
			}
			if(!empty($this->request->data['Search']['place_external_caterer'])){
				$options['conditions'][] = array('external_caterer' => 1);
			}
			if(!empty($this->request->data['Search']['place_number_of_rooms'])){
				$options['conditions'][] = array('configuration_count >=' => $this->request->data['Search']['place_number_of_rooms']);
			}
			if(!empty($this->request->data['Search']['place_maximum_surface'])){
				$options['conditions'][] = array('max_surface <=' => $this->request->data['Search']['place_maximum_surface']);
			}
			if(!empty($this->request->data['Search']['place_number_of_rooms'])){
				$options['conditions'][] = array('number_of_rooms >=' => $this->request->data['Search']['place_number_of_rooms']);
			}
			if(!empty($this->request->data['Search']['place_ideal_for'])){
				$options['conditions'][] = array('IdealFor.value' => $this->request->data['Search']['place_ideal_for']);
			}
			if(!empty($this->request->data['Search']['place_degree'])){
				$options['conditions'][] = array('degree >=' => $this->request->data['Search']['place_degree']);
			}


			$sf = 3.14159 / 180; // scaling factor
			$er = 6371; // earth radius in km, approximate
			$mr = !empty($this->request->data['Search']['radius']) ? $this->request->data['Search']['radius'] : 50; // max radius

			if(!empty($commune)){
				$lat = $commune['Commune']['latitude'];
				$lon = $commune['Commune']['longitude'];
				$travelOrigin = $commune['Commune']['zip'] . '+' . $commune['Commune']['name'];
				$options['conditions'][] = array("$er * ACOS(SIN(Place.latitude*$sf)*SIN($lat*$sf) + COS(Place.latitude*$sf)*COS($lat*$sf)*COS((Place.longitude - $lon)*$sf)) <=" => $mr);
				$options['order'][] = "ACOS(SIN(latitude*$sf)*SIN($lat*$sf) + COS(latitude*$sf)*COS($lat*$sf)*COS((longitude - $lon)*$sf))";
			}
			$options['group'] = array('Place.id');
			// $places = $this->Place->find('all', $options);
			// unset($options['limit']);
			// $numberOfPlaces = $this->Place->find('count', $options);

			$this->Paginator->settings = $options;
			$data = $this->Paginator->paginate('Place');

			$places = array();
			foreach($data as $k => $place){
				$places[$k] = $place;
				if(!empty($place['Place']['unavailabilities']) OR !empty($place['Place']['to_check'])){
					$places[$k]['Place']['show_remarks'] = 1;
				} else {
					$places[$k]['Place']['show_remarks'] = 0;
				}
				if(!empty($commune)){
					//$places[$k]['distances'] = $this->Place->getTravelTime($commune['Commune']['zip'] . '+' . $commune['Commune']['name'], $place['Place']['zip'] . '+' . $place['Place']['city']);
				}
			}
			$this->set(compact('places'));
			// foreach($places as $k => $place){
			//	 if($place['Place']['degree'] == 0){
			//		 $notInterestingPlaces[] = $place;
			//		 unset($places[$k]);
			//	 }
			// }

			// $this->set(compact('places'));
			// $this->set(compact('notInterestingPlaces'));
			// $this->set(compact('numberOfPlaces'));

		}

	}

}
