<?php

class PlanningResourcesController extends AppController {

  public function add( ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $existing = $this->PlanningResource->find('first', array(
        'conditions' => array(
          'resource_model' => $this->request->data['resource_model'],
          'resource_id' => $this->request->data['resource_id'],
          'planning_board_moment_id' => $this->request->data['planning_board_moment_id']
        )
      ));
      if(empty($existing)){
        $this->PlanningResource->create();
        if($this->PlanningResource->save(array(
          'PlanningResource' => array(
            'resource_model' => $this->request->data['resource_model'],
            'resource_id' => $this->request->data['resource_id'],
            'planning_board_moment_id' => $this->request->data['planning_board_moment_id'],
            'slug' => $this->PlanningResource->getSlug($this->request->data['resource_id'], $this->request->data['resource_model'])
          )
        ))){
          $this->response->body(json_encode(array('success' => 1, 'existing' => 0)));
        } else {
          $this->response->body(json_encode(array('success' => 0, 'existing' => 0)));
        }
      } else {
        $this->response->body(json_encode(array('existing' => 1)));
      }
    }
  }

  public function delete($id = ''){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->PlanningResource->delete($this->request->data['resource_id'])){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

}
