<?php

class VehicleCompanyModelsController extends AppController {

	public $components = [
    'DataTable.DataTable' => [
      'Models' => [
      	'model' => 'VehicleCompanyModel',
          'columns' => [
              'name',
              'family' => array(
              	'useField' => false
              ),
              'company' => array(
              	'useField' => false
              ),
              'type',
              'Actions' => null,
          ],
          'contain' => array(
          	'VehicleCompany'
          ),
          'fields' => array(
          	'VehicleCompanyModel.id',
          	'VehicleCompany.name',
          	'VehicleCompanyModel.family'
          ),
          'autoData' => false
      ]
    ],
  ];

	public function index() {
		$this->DataTable->setViewVar(array('Models'));
	}

  public function beforeFilter() {
    parent::beforeFilter();
    $this->DataTable->settings['Models']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['Models']['columns']['company']['label'] = __('Company');
    $this->DataTable->settings['Models']['columns']['type']['label'] = __('Type');
    $this->DataTable->settings['Models']['columns']['family']['label'] = __('Family');
  }

	public function add($companyId = null) {

		$companies = $this->VehicleCompanyModel->VehicleCompany->find('list');
		$this->set(compact('companies'));

		if(!empty($companyId)){
			$this->set('vehicle_company_id', $companyId);
		}

		if($this->request->is('put') || $this->request->is('post')){
      if(in_array($this->request->data['VehicleCompanyModel']['family'], Configure::read('Vehicles.families'))){
        $this->request->data['VehicleCompanyModel']['type'] = 'vehicle';
      }
      if(in_array($this->request->data['VehicleCompanyModel']['family'], Configure::read('Trailers.families'))){
        $this->request->data['VehicleCompanyModel']['type'] = 'trailer';
      }
			if($this->VehicleCompanyModel->save($this->request->data)){
				$this->Session->setFlash(__('Vehicle model has been saved.'), 'alert', array('type' => 'success'));
          if($this->request->data['destination'] == 'edit'){
            return $this->redirect(array('action' => 'edit', $this->VehicleCompanyModel->id));
          } else {
            return $this->redirect(array('action' => 'index'));
          }
			} else {
        $this->Session->setFlash(__('Vehicle model has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
      }
		}
	}

	public function edit($id = null){

		$companies = $this->VehicleCompanyModel->VehicleCompany->find('list');
		$this->set(compact('companies'));

		if($this->request->is('put') || $this->request->is('post')){
      if(in_array($this->request->data['VehicleCompanyModel']['family'], Configure::read('Vehicles.families'))){
        $this->request->data['VehicleCompanyModel']['type'] = 'vehicle';
      }
      if(in_array($this->request->data['VehicleCompanyModel']['family'], Configure::read('Trailers.families'))){
        $this->request->data['VehicleCompanyModel']['type'] = 'trailer';
      }
			if($this->VehicleCompanyModel->save($this->request->data)){
				$this->Session->setFlash(__('Vehicle model has been saved.'), 'alert', array('type' => 'success'));
        if($this->request->data['destination'] == 'edit'){
          return $this->redirect(array('action' => 'edit', $this->VehicleCompanyModel->id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
			} else {
        $this->Session->setFlash(__('Vehicle model has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
      }
		} else {
			$this->request->data = $this->VehicleCompanyModel->read(null, $id);
		}

	}

  public function get(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $models = $this->VehicleCompanyModel->find('all', array(
        'conditions' => array(
          'vehicle_company_id' => $_POST['vehicle_company_id']
        ),
        'fields' => array(
          'VehicleCompanyModel.id', 'VehicleCompanyModel.name'
        )
      ));
      $this->response->body(json_encode($models));
    }
  }


}
