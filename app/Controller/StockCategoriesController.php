<?php
class StockCategoriesController extends AppController {

	public function add( $companyId = null){

		$companies = $this->StockCategory->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if(!empty($companyId)){
			$company = $this->StockCategory->Company->findById($companyId);
			$this->set(compact('company'));
		}

		if($this->request->is('put') OR $this->request->is('post')){
			if($this->StockCategory->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockCategory']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
	        return $this->redirect(array('action' => 'edit', $this->StockCategory->id));
				} elseif($this->request->data['StockCategory']['company_id'] == 3) { // festiloc
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockCategory']['company_id']));
				}
			}
		}
	}

	public function edit( $id = null ){

		$companies = $this->StockCategory->Company->find('list', array(
			'fields' => array('Company.id', 'Company.name')
		));
		$this->set(compact('companies'));

		if($this->request->is('put') OR $this->request->is('post')){
			if($this->StockCategory->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockCategory']['name']), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
	        return $this->redirect(array('action' => 'edit', $this->StockCategory->id));
				} elseif($this->request->data['StockCategory']['company_id'] == 3) { // festiloc 
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'index'));
				} else {
					return $this->redirect(array('controller' => 'stock_items', 'action' => 'company', $this->request->data['StockCategory']['company_id']));
				}
			}
		} else {
			$category = $this->StockCategory->findById($id);
			$this->set(compact('category'));
			$this->request->data = $category;
		}
	}

	public function products($id = null){

		$this->layout = 'kameleo';
		$this->view = 'listing';
		$this->loadModel('StockItem');
		$products = $this->StockItem->find('all', array(
			'contain' => array(
				'StockCategory',
				'StockSection',
				'StockFamily',
				'Document'
			),
			'conditions' => array(
				'StockItem.stock_category_id' => $id,
				'StockItem.online' => 1
			),
			'order' => 'StockItem.code'
		));
		$data = array();
		foreach($products as $k => $product){
			$data[$k]['online'] = $product['StockItem']['online'] == 1 ? 'ON' : 'OFF';
			$data[$k]['codecatégorie'] = $product['StockCategory']['code'];
			$data[$k]['catégorie'] = $product['StockCategory']['name'];
			$data[$k]['codesection'] = $product['StockSection']['code'];
			$data[$k]['section2chiffres'] = $product['StockSection']['name'];
			$data[$k]['codesoussection'] = $product['StockFamily']['code'];
			$data[$k]['soussection2chiffres'] = $product['StockFamily']['name'];
			$data[$k]['nouveaunoproduit'] = $product['StockItem']['code'];
			$data[$k]['nouveaunomarticle'] = $product['StockItem']['name'];
			$data[$k]['nomarticle'] = $product['StockItem']['name'];
			$data[$k]['prixlocation'] = $product['StockItem']['price'];
			$data[$k]['qtédisponible'] = $product['StockItem']['quantity'];
			if(!empty($product['Document'])){
				foreach ($product['Document'] as $key => $doc) {
					$data[$k]['images'][] = Router::fullbaseUrl(). $this->webroot . $doc['url'];
				}
			}
		}
		$this->set('data', $data);
	}

	public function json(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$categories = $this->StockCategory->find('all', array(
				'conditions' => array(
					'StockCategory.company_id' => $this->request->data['company_id']
				),
				'fields' => array('StockCategory.id', 'StockCategory.code_name'),
				'order' => array('StockCategory.code ASC')
			));
			$this->response->body(json_encode($categories));
		}
	}

	public function fix_codes( $company = ''){
		$data = $this->StockCategory->StockSection->find('all', array(
			'conditions' => array(
				'company_id' => $company
			),
			'order' => array('StockSection.stock_category_id', 'StockSection.code'),
			'fields' => array(
				'StockSection.id', 'StockSection.name', 'StockSection.code', 'StockSection.stock_category_id'
			)
		));
		$sections = array();
		foreach($data as $item){
			$sections[$item['StockSection']['stock_category_id']][] = $item;
		}
		foreach($sections as $category => $sectionss){
			$code = 1;
			foreach($sectionss as $section){
				$section['StockSection']['code'] = $code++;
				$this->StockCategory->StockSection->create();
				if($this->StockCategory->StockSection->save($section)){
					echo 'La section a bien été modifiée. <br /> ';
				}
			}
		}
		$data = $this->StockCategory->StockSection->StockFamily->find('all', array(
			'conditions' => array(
				'company_id' => $company
			),
			'order' => array('StockFamily.stock_section_id', 'StockFamily.code'),
			'fields' => array(
				'StockFamily.id', 'StockFamily.name', 'StockFamily.code', 'StockFamily.stock_section_id'
			)
		));
		$families = array();
		foreach($data as $item){
			$families[$item['StockFamily']['stock_section_id']][] = $item;
		}
		foreach($families as $section => $familiess){
			if(is_null($section)){continue;}
			$code = 1;
			foreach($familiess as $family){
				$family['StockFamily']['code'] = $code++;
				$this->StockCategory->StockSection->StockFamily->create();
				if($this->StockCategory->StockSection->StockFamily->save($family)){
					echo 'La famille a bien été modifiée. <br /> ';
				}
			}
		}
		$items = $this->StockCategory->StockSection->StockFamily->StockItem->find('all', array(
			'contain' => array(
				'StockCategory' => array('fields' => array('code')),
				'StockSection' => array('fields' => array('code')),
				'StockFamily' => array('fields' => array('code'))
			),
			'conditions' => array(
				'StockItem.company_id' => $company
			),
			'fields' => array('id', 'code', 'name'),
			'order' => array('StockItem.stock_family_id', 'StockItem.stock_section_id', 'StockItem.stock_category_id', 'StockItem.code')
		));
		$code = 1;
		foreach($items as $k => $item){
			if(isset($items[$k+1]) && $items[$k]['StockFamily']['id'] != $items[$k+1]['StockFamily']['id']){
				$code = 1;
			}
			$newCode = $item['StockCategory']['code'];
			$newCode .= ($item['StockSection']['code'] < 10) ? '0' . $item['StockSection']['code'] : $item['StockSection']['code'];
			$newCode .= ($item['StockFamily']['code'] < 10) ? '0' . $item['StockFamily']['code'] : $item['StockFamily']['code'];
			$newCode .= ($code < 10) ? '0' . $code : $code;
			$item['StockItem']['code'] = $newCode;
			$this->StockCategory->StockSection->StockFamily->StockItem->create();
			if($this->StockCategory->StockSection->StockFamily->StockItem->save($item)){
				echo 'Le produit a bien été modifiée. <br /> ';
				$code++;
			}
		}
		exit;
	}

}
