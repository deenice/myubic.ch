<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright	 Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link		  http://cakephp.org CakePHP(tm) Project
 * @package	   app.Controller
 * @since		 CakePHP(tm) v 0.2.9
 * @license	   http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('DataTableRequestHandlerTrait', 'DataTable.Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

  use DataTableRequestHandlerTrait;

  public $components = array(
    'RequestHandler',
    //'Acl',
    'Auth'/* => array(
      'authorize' => array(
        'Actions' => array('actionPath' => 'controllers')
      )
    )*/,
    'Session',
    'Cookie',
    //'DebugKit.Toolbar'
  );

  public $helpers = array(
    'MenuBuilder.MenuBuilder' => array(
      'firstClass' => 'start',
      'noLinkFormat' => '<a href="javascript:;">%s</a>',
      'authVar' => 'loggedUser'
    ),
    'DataTable.DataTable' => array(
      'js' => array(
        //'responsive' => true,
        'stateSave' => true,
        'pageLength' => 20,
        'lengthMenu' => [
          [10, 20, 100, -1],
          [10, 20, 100, "All"] // change per page values here
        ],
        'language' => array(
          'sProcessing' => array("Traitement en cours..."),
          'sInfo' => array("Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments"),
          'sInfoEmpty' => array("Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ments"),
          'sInfoFiltered' => array("(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)"),
          'sInfoPostFix' => array(""),
          'sLoadingRecords' => array("Chargement en cours..."),
          'sEmptyTable' => array("Aucune donn&eacute;e disponible dans le tableau"),
          'sZeroRecords' => array("Aucun &eacute;l&eacute;ment &agrave; afficher"),
          'oAria' => array(
            'sortAscending' => ": activer pour trier la colonne par ordre croissant",
            'sortDescending' => ": activer pour trier la colonne par ordre décroissant"
          ),
          //'sSearch' => array("Rechercher"),
          //'sLengthMenu' => array("Afficher _MENU_ &eacute;l&eacute;ments"),
        )
      ),
      'table' => array(
        'class' => 'dataTable table table-striped table-bordered table-hover'
      )
    )
  );

  public function beforeFilter() {
    global $COMPANIES;
    $this->loadModel('Company');
    $COMPANIES = $this->Company->find('list', array(
      'fields' => array(
        'Company.id', 'Company.name'
      )
    ));

    //$this->layout = 'default';
    $this->layout = 'default_v3';

    $this->Cookie->key = 'qSI21~_+!@#HK&sXOw32qs*XSL#$%)asGb$@1!adre@34SAv!@*(is~#^';
    $this->Cookie->httpOnly = true;
    $this->loadModel('User');

    if(Configure::read('debug') > 0) $this->response->header('Access-Control-Allow-Origin', '*');
    if($this->request->is('ajax')){
      $this->Auth->allow();
    } elseif(in_array($this->params['ext'], array('json', 'ics')) || ($this->params['controller'] == 'stock_categories' AND $this->params['action'] == 'products') || $this->params['controller'] == 'round_answers' || $this->params['controller'] == 'rounds' ){
      $this->Auth->allow();
    } else {
      $this->Auth->allow('logout', 'exportToKameleo', 'clean', 'reconditionning', 'pdf_footer', 'frontendAnimation'/*, 'view', 'index'*/);
    }
    if (!$this->Auth->loggedIn() && $this->Cookie->read('remember_me_cookie')) {
      $cookie = $this->Cookie->read('remember_me_cookie');
      $user = $this->User->find('first', array(
        'conditions' => array(
          'User.username' => $cookie['username'],
          'User.password' => $cookie['password']
        )
      ));
      if ($user && !$this->Auth->login($user['User'])) {
        $this->redirect('/users/logout'); // destroy session & cookie
      }
    } else {
      // $user = $this->Auth->user();
      // $this->set('loggedUser', array('User' => $user));
    }

    if(empty($user)){
      //$this->Auth->logout();
    } else {
      $this->User->id = $user['User']['id'];

      if($this->User->hasRights($this->params['controller'], $this->params['action'])){
        return true;
      } else {
        $this->Session->setFlash(__('You are not allowed to access this page or perform this task.'), 'alert', array('type' => 'danger'));
        if(in_array($this->User->getRole(), array('temporary', 'candidate'))){
          return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
        } elseif(in_array($this->User->getRole(), array('retired', 'unsuitable'))){
          $this->Session->setFlash(__("Vous n'êtes pas autorisé à accéder au site."), 'alert', array('type' => 'danger'));
          $this->Auth->logout();
          return $this->redirect(array('controller' => 'users', 'action' => 'logout'));
        } else {
          return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
        }
      }
    }

  }

  // public function appError($error) {
  //	 // custom logic goes here.
  //	 $this->Session->setFlash(__('Something went wrong...'), 'alert', array('type' => 'danger'));
  //	 return $this->redirect(array('controller' => 'me', 'action' => 'dashboard'));
  // }

}
