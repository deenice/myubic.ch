<?php

class StockOrderExtraHoursController extends AppController {

	public function updateField(){
		$this->autoRender = false;
		if($this->request->is('ajax')){
			$this->StockOrderExtraHour->id = $this->request->data['id'];
			if($this->StockOrderExtraHour->saveField($this->request->data['field'], $this->request->data['value'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}