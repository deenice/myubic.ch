<?php

class PlacesController extends AppController {
  public $helpers = array('Availability', 'Languages', 'Talents', 'ImageSize');

  public function beforeFilter() {
    parent::beforeFilter();
    $this->Auth->allow('index', 'view');

    $this->DataTable->settings['ToBeCheckedPlaces']['conditions']['user_id <>'] = AuthComponent::user('id');
    $this->DataTable->settings['Places']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['Places']['columns']['zip_city']['label'] = __('ZIP / City');
    $this->DataTable->settings['Places']['columns']['last_modification']['label'] = __('Last modification');
    $this->DataTable->settings['PrivatePlaces']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['PrivatePlaces']['columns']['zip_city']['label'] = __('ZIP / City');
    $this->DataTable->settings['PrivatePlaces']['columns']['last_modification']['label'] = __('Last modification');
    $this->DataTable->settings['ToBeCheckedPlaces']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['ToBeCheckedPlaces']['columns']['zip_city']['label'] = __('ZIP / City');
    $this->DataTable->settings['ToBeCheckedPlaces']['columns']['last_modification']['label'] = __('Last modification');
    $this->DataTable->settings['IncompletePlaces']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['IncompletePlaces']['columns']['zip_city']['label'] = __('ZIP / City');
    $this->DataTable->settings['IncompletePlaces']['columns']['last_modification']['label'] = __('Last modification');
  }

  public $components = [
        'DataTable.DataTable' => [
            'Places' => [
                'model' => 'Place',
                'columns' => [
                    'checked' => array(
                        'bSortable' => false,
                        'bSearchable' => false,
                        'label' => false,
                    ),
                    'name',
                    'zip_city',
                    'type',
                    'last_modification' => array(
                        'useField' => false,
                    ),
                    'Actions' => null,
                ],
                'order' => array('Place.degree' => 'DESC', 'Place.name' => 'ASC'),
                'contain' => array('LastModifiedBy', 'Note'),
                'fields' => array(
                    'id', 'modified', 'LastModifiedBy.first_name', 'LastModifiedBy.last_name', 'degree',
                ),
                'conditions' => array('Place.private' => 0),
                'autoData' => false,
            ],
            'PrivatePlaces' => [
                'model' => 'Place',
                'columns' => [
                    'checked' => array(
                        'bSortable' => false,
                        'bSearchable' => false,
                        'label' => false,
                    ),
                    'name',
                    'zip_city',
                    'type',
                    'last_modification' => array(
                        'useField' => false,
                    ),
                    'Actions' => null,
                ],
                'order' => array('Place.degree' => 'DESC', 'Place.name' => 'ASC'),
                'contain' => array('LastModifiedBy', 'Note'),
                'fields' => array(
                    'id', 'modified', 'LastModifiedBy.first_name', 'LastModifiedBy.last_name', 'degree',
                ),
                'conditions' => array('Place.private' => 1),
                'autoData' => false,
            ],
            'IncompletePlaces' => [
                'model' => 'Place',
                'columns' => [
                    'checked' => array(
                        'bSortable' => false,
                        'bSearchable' => false,
                        'label' => false,
                    ),
                    'name',
                    'zip_city',
                    'type',
                    'last_modification' => array(
                        'useField' => false,
                    ),
                    'Actions' => null,
                ],
                'order' => array('Place.degree' => 'DESC', 'Place.name' => 'ASC'),
                'contain' => array('LastModifiedBy', 'Note'),
                'fields' => array(
                    'id', 'modified', 'LastModifiedBy.first_name', 'LastModifiedBy.last_name', 'degree',
                ),
                'conditions' => [
                    'checked' => 0,
                    'private' => 0,
                ],
                'autoData' => false,
            ],
            'ToBeCheckedPlaces' => [
                'model' => 'Place',
                'columns' => [
                    'checked' => array(
                        'bSortable' => false,
                        'bSearchable' => false,
                        'label' => false,
                    ),
                    'name',
                    'zip_city',
                    'type',
                    'last_modification' => array(
                        'useField' => false,
                    ),
                    'Actions' => null,
                ],
                'order' => array('Place.degree' => 'DESC', 'Place.name' => 'ASC'),
                'contain' => array('LastModifiedBy', 'Note'),
                'fields' => array(
                    'id', 'modified', 'LastModifiedBy.first_name', 'LastModifiedBy.last_name', 'degree',
                ),
                'conditions' => [
                    'checked' => 0,
                    'private' => 0,
                    //'user_id <>' => AuthComponent::user('id')
                ],
                'autoData' => false,
            ],
        ],
    ];

    public function index()
    {
        $this->DataTable->setViewVar(array('Places', 'IncompletePlaces', 'ToBeCheckedPlaces', 'PrivatePlaces'));
    }

    public function index_json()
    {
        $types = Configure::read('Places.types');

        $view = new View($this);
        $html = $view->loadHelper('Html');
        $time = $view->loadHelper('Time');

        // get all places
        $places = $this->Place->find('all', array(
            'order' => 'degree DESC, name ASC',
            'contain' => array('LastModifiedBy', 'User'),
            'cache' => date('dmY'),
            'cacheConfig' => 'short',
        ));

        $data = Cache::read('places_json_'.date('dmY'), 'long');
        if (empty($data)) {
            foreach ($places as $k => $p) {
                $data['all'][$k]['Place']['checked'] = $p['Place']['checked'] ? $html->tag('i', '', array('class' => 'fa fa-check-circle font-green')) : $html->tag('i', '', array('class' => 'fa fa-circle-thin'));
                $data['all'][$k]['Place']['name'] = $html->link($p['Place']['name'], array('controller' => 'places', 'action' => 'view', $p['Place']['id']));
                $data['all'][$k]['Place']['city'] = $p['Place']['city'];
                $data['all'][$k]['Place']['type'] = !empty($p['Place']['type']) ? $types[$p['Place']['type']] : '';
                $data['all'][$k]['Place']['modified'][] = $time->format('d.m.Y H:i', strtotime($p['Place']['modified']));
                $data['all'][$k]['Place']['modified'][] = $p['LastModifiedBy']['full_name'];
                $data['all'][$k]['actions'][] = $html->link(
                    '<i class="fa fa-search"></i> '.__('View'),
                    array('controller' => 'places', 'action' => 'view', $p['Place']['id']),
                    array('class' => 'btn default btn-xs', 'escape' => false)
                );
                $data['all'][$k]['actions'][] = $html->link(
                    '<i class="fa fa-print"></i> '.__('Print'),
                    array('controller' => 'places', 'action' => 'pdf', $p['Place']['id']),
                    array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
                );
                $data['all'][$k]['actions'][] = $html->link(
                    '<i class="fa fa-edit"></i> '.__('Edit'),
                    array('controller' => 'places', 'action' => 'edit', $p['Place']['id']),
                    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
                );
                if (empty($p['Place']['to_check'])) {
                    $data['incomplete'][] = $data['all'][$k];
                }
                if ($p['Place']['user_id'] != AuthComponent::user('id')) {
                    $data['to_check'][] = $data['all'][$k];
                }
            }
        }
        Cache::write('places_json_'.date('dmY'), $data, 'long');

        $this->set(compact('data'));
        $this->set('_serialize', array('data'));
    }

    public function add()
    {

        // Get all material tags
        $materials = $this->Place->Tag->find('all', array(
            'conditions' => array(
                'category' => 'material',
            ),
            'order' => 'value',
        ));
        $this->set(compact('materials'));

        // Get fixed collaborators
        $fixedCollaborators = $this->Place->User->find('list', array(
            'conditions' => array('role' => 'fixed'),
            'fields' => array('full_name'),
            'order' => 'first_name ASC',
        ));
        $this->set(compact('fixedCollaborators'));

        // Get all activities (UBIC and UG)
        $activities = $this->Place->Activity->find('list', array(
            'contain' => 'Company',
            'conditions' => array(
                'category' => null,
            ),
            'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
            'order' => 'Activity.company_id, Activity.name',
        ));
        $this->set(compact('activities'));

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Place->create();
            if (!empty($this->request->data['Place']['thumbnail']['tmp_name'])) {
                $this->Place->Document->saveFile($this->request->data['Place']['thumbnail'], $this->Place->nextId(), array('places', 'thumbnail'), 'place_thumbnail', Configure::read('Documents.Images.allowedExtensions'));
            }
            if ($this->Place->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('%s has been saved.', $this->request->data['Place']['name']), 'alert', array('type' => 'success'));
                if ($this->request->data['destination'] == 'edit') {
                    return $this->redirect(array('action' => 'edit', $this->Place->id));
                } else {
                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('Place has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
        }
    }

    public function edit($id = null)
    {
        $this->Place->id = $id;
        $this->Place->contain(array(
            'Tag',
            'Configuration' => array('Tag'),
            'Activity',
            'Document' => array('User'),
            'User',
            'Owner',
            'Referer',
            'Material',
            'IdealFor',
        ));
        $place = $this->Place->find('first', array(
            'conditions' => array(
                'Place.id' => $id,
            ),
        ));
        //debug($place);exit;
        $this->set(compact('place'));
        //$this->request->data = $place;

        $thumbnail = $this->Place->Document->getFiles($id, 'place_thumbnail');
        $this->set(compact('thumbnail'));

        // Prepare photos and send them to view
        $photos = $this->Place->Document->getFiles($id, 'place');
        $docs = $this->Place->Document->getFiles($id, 'document');
        $documents = array();
        $documents['documents'] = $docs;
        $documents['photos'] = $photos;
        $this->set(compact('documents'));

        // Prepare tags values grouped by category for the place
        $tags = array();
        foreach ($place['Tag'] as $tag) {
            $tags[$tag['category']][$tag['id']] = $tag['id'];
        }
        // Prepare tags values grouped by category for the configurations
        foreach ($place['Configuration'] as $config) {
            foreach ($config['Tag'] as $tag) {
                $tags['Configuration'][$tag['ConfigurationsTag']['configuration_id']][$tag['id']] = $tag['id'];
            }
        }
        $this->set(compact('tags'));

        // Get all material tags
        $materials = $this->Place->Tag->find('all', array(
            'conditions' => array(
                'category' => 'material',
            ),
            'order' => 'value',
        ));
        $this->set(compact('materials'));

        // Get fixed collaborators
        $fixedCollaborators = $this->Place->User->find('list', array(
            'conditions' => array('role' => 'fixed'),
            'fields' => array('full_name'),
            'order' => 'first_name ASC',
        ));
        $this->set(compact('fixedCollaborators'));

        // Get all activities (UBIC and UG)
        $activities = $this->Place->Activity->find('list', array(
            'contain' => 'Company',
            'conditions' => array(
                'category' => null,
            ),
            'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
            'order' => 'Activity.company_id, Activity.name',
        ));
        $this->set(compact('activities'));

        if (!$this->Place->exists()) {
            throw new NotFoundException(__('Invalid Place'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if (!empty($this->request->data['Place']['thumbnail']['tmp_name'])) {
                $this->Place->Document->saveFile($this->request->data['Place']['thumbnail'], $id, array('places', 'thumbnail'), 'place_thumbnail', Configure::read('Documents.Images.allowedExtensions'));
            }
            if ($this->Place->saveAssociated($this->request->data)) {
                $this->Session->setFlash(__('Place has been saved.'), 'alert', array('type' => 'success'));
                if ($this->request->data['destination'] == 'edit') {
                    return $this->redirect(array('action' => 'edit', $this->Place->id));
                } else {
                    return $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash(__('Place has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
        } else {
            $this->request->data = $this->Place->read(null, $id);
        }
    }

    public function view($id = null) {
      $this->Place->id = $id;

      if (!$this->Place->exists()) {
          throw new NotFoundException(__('Invalid Place'));
      }
      $place = $this->Place->find('first', array(
        'contain' => array(
          'Configuration' => array('Tag.category = "configuration_type"'),
          'Document',
          'User',
          'LastModifiedBy',
          'Referer',
          'Option',
          'Material',
          'IdealFor',
          'Activity',
          'EventPlace' => array(
            'Event'
          )
        ),
        'conditions' => array('Place.id' => $id),
      ));
      $this->set(compact('place'));

      $thumbnail = $this->Place->Document->getFiles($id, 'place_thumbnail');
      $this->set(compact('thumbnail'));

      // Prepare photos and send them to view
      $docs = $this->Place->Document->getFiles($id, 'document');
      $photos = $this->Place->Document->getFiles($id, 'place');
      $documents = array();
      if (!empty($photos)) {
        foreach ($photos as $doc) {
          if (!empty($doc['Document']['group'])) {
            $group = $doc['Document']['group'];
          } else {
            $group = 'undefined';
          }
          $documents['photos'][$group][] = $doc;
        }
      }
      if (!empty($docs)) {
        foreach ($docs as $doc) {
          if (!empty($doc['Document']['group'])) {
            $group = $doc['Document']['group'];
          } else {
            $group = 'undefined';
          }
          $documents['documents'][$group][] = $doc;
        }
      }
      $this->set(compact('documents'));

      $months = array();
      $year = !empty($this->request->params['named']['year']) ? $this->request->params['named']['year'] : date('Y');
      for ($m=1; $m<=12; $m++) {
        $month = date('F', mktime(0,0,0,$m, 1, $year));
        $key = date('n', mktime(0,0,0,$m, 1, $year));
        $months[$key] = $month;
      }
      $this->set(compact('months'));
      $this->set(compact('year'));
      $options = array();
      if(!empty($place['EventPlace'])){
        foreach($place['EventPlace'] as $option){
          $year1 = date('Y', strtotime($option['Event']['confirmed_date']));
          $month = date('n', strtotime($option['Event']['confirmed_date']));
          $key = strtotime($option['Event']['confirmed_date']) . time();
          if($year1 == $year) $options[$month][$key] = $option;
        }
        if(!empty($options[$month])) ksort($options[$month]);
      }
      $this->set(compact('options'));
      $this->set('placesOptions', Configure::read('Places.options'));
    }

    public function agenda()
    {
        $events = $this->Place->Option->find('all');
        foreach ($events as $event) {
            $json[]['title'] = $event['Option']['id'];
        }
        $this->set('_json', $json);
    }

    public function pdf($id = null)
    {
        return $this->view($id);
    }

    public function csv()
    {
        exit;
        $output = utf8_decode("Lieu\tPersonne de contact\tEmail\tAdresse\tNPA\tLocalité\tSite web\n");
        $data = $this->Place->find('all', array(
            'contain' => array(),
            'order' => 'name ASC',
        ));
        //debug($data);
        foreach ($data as $key => $place) {
            $output .= ''
                        .utf8_decode($place['Place']['name'])."\t"
                        .utf8_decode($place['Place']['contact_person_name'])."\t"
                        .utf8_decode($place['Place']['contact_person_email'])."\t"
                        .utf8_decode($place['Place']['address'])."\t"
                        .utf8_decode($place['Place']['zip'])."\t"
                        .utf8_decode($place['Place']['city'])."\t"
                        .utf8_decode($place['Place']['website'])."\n";
        }
        file_put_contents(WWW_ROOT.'files/places.csv', $output);
        exit;
    }

    public function options()
    {
        $data = $this->Place->Option->find('all', array(
            'contain' => array(
                'Moment' => array('Event'),
                'Place',
                'User',
            ),
            'order' => 'Option.date',
        ));
        $options = array();
        foreach ($data as $option) {
            $options[$option['Option']['value']][] = $option;
        }
        $this->set(compact('options'));
    }

    public function renamePhoto()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $oldname = IMAGES.'places'.DS.
                        $_POST['data']['photo']['category'].DS.
                        $_POST['data']['photo']['place'].DS.
                        $_POST['data']['photo']['oldname'];
            $newname = IMAGES.'places'.DS.
                        $_POST['data']['photo']['category'].DS.
                        $_POST['data']['photo']['place'].DS.
                        Inflector::slug($_POST['data']['photo']['newname']).'.'.
                        $_POST['data']['photo']['extension'];
            $output = array('success' => rename($oldname, $newname), 'newname' => $_POST['data']['photo']['newname']);
            $this->response->body(json_encode($output));
        }
    }

    public function deletePhoto()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $filename = IMAGES.'places'.DS.
                        $_POST['data']['photo']['category'].DS.
                        $_POST['data']['photo']['place'].DS.
                        $_POST['data']['photo']['oldname'];
            $output = array('success' => unlink($filename));
            $this->response->body(json_encode($output));
        }
    }

    public function getLatLng()
    {
        //$key = Configure::read('GoogleMapsAPIKey');
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $search = $_POST['address'].'+'.$_POST['zip'].'+'.$_POST['city'];
            $search = str_replace(' ',  '-', $search);
            $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$search.'&sensor=false';
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $data = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($data);
            $lat = $data->results[0]->geometry->location->lat;
            $lng = $data->results[0]->geometry->location->lng;
            $output = array('latitude' => $lat, 'longitude' => $lng);
            $this->response->body(json_encode($output));
        }
    }

    public function getOption($id, $date, $morning = 0, $evening = 0)
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $conditions = array(
                'place_id' => $id,
                'date' => $date,
            );
            if ($morning && !$evening) {
                $conditions['morning'] = array();
                array_push($conditions['morning'], 1);
            }
            if ($evening && !$morning) {
                $conditions['evening'] = array();
                array_push($conditions['evening'], 1);
            }
            if ($morning && $evening) {
                $conditions['OR']['morning'] = array();
                $conditions['OR']['evening'] = array();
                array_push($conditions['OR']['morning'], 1);
                array_push($conditions['OR']['evening'], 1);
            }
            $data = $this->Place->Option->find('first', array(
                'conditions' => $conditions,
                'contain' => array('User'),
                //'fields' => array('value')
            ));
            if (sizeof($data)) {
                App::uses('CakeTime', 'Utility');
                $this->response->body(json_encode(array(
                    'option' => $data['Option']['value'],
                    'full_name' => $data['Option']['value'] != 'free' ? $data['User']['full_name'] : '',
                    'valid_until' => !empty($data['Option']['valid_until']) ? CakeTime::format($data['Option']['valid_until'], '%d-%m-%Y') : '',
                )));
            } else {
                $this->response->body(json_encode(array('option' => 'free')));
            }
        }
    }

    public function setOption()
    {
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
            $conditions = array(
                'place_id' => $_POST['place_id'],
                'date' => $_POST['date'],
            );
            $data = $this->Place->Option->find('first', array(
                'conditions' => $conditions,
            ));
            if (sizeof($data)) {
                $this->Place->Option->id = $data['Option']['id'];
                $this->Place->Option->set('value', $_POST['value']);
                $this->Place->Option->set('user_id', $_POST['user_id']);
                if (!empty($_POST['valid_until'])) {
                    $this->Place->Option->set('valid_until', date('Y-m-d', strtotime($_POST['valid_until'])));
                }
                $this->Place->Option->save();
            } else {
                $this->Place->create();
                $this->Place->Option->set('place_id', $_POST['place_id']);
                $this->Place->Option->set('value', $_POST['value']);
                $this->Place->Option->set('date', $_POST['date']);
                $this->Place->Option->set('morning', $_POST['morning']);
                $this->Place->Option->set('evening', $_POST['evening']);
                $this->Place->Option->set('user_id', $_POST['user_id']);
                $this->Place->Option->set('event_id', $_POST['event_id']);
                if (!empty($_POST['valid_until'])) {
                    $this->Place->Option->set('valid_until', date('Y-m-d', strtotime($_POST['valid_until'])));
                }
                $this->Place->Option->save();
            }

            if ($_POST['value'] == 'confirmed' && !empty($_POST['event_id'])) {
                $this->loadModel('Event');
                $this->Event->id = $_POST['event_id'];
                $this->Event->set('place_id', $_POST['place_id']);
                $this->Event->save();
            } elseif ($_POST['value'] == 'free') {
                $this->loadModel('Event');
                $this->Event->set('place_id', null);
                $this->Event->set('user_id', null);
                $this->Event->save();
            } else {
                $this->loadModel('Event');
                $this->Event->id = $_POST['event_id'];
                $this->Event->set('place_id', null);
                $this->Event->save();
            }
            $this->response->body(json_encode(array('success' => 1)));
        }
    }

    public function get()
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;
            if (!empty($_POST['id'])) {
                $place = $this->Place->find('first', array(
                    'conditions' => array(
                        'Place.id' => $_POST['id'],
                    ),
                    'fields' => array('Place.zip', 'Place.city', 'Place.zip_city', 'Place.address'),
                ));
                $this->response->body(json_encode($place));
            }
        }
    }

    public function fabienne()
    {
        $data = $this->Place->Document->find('all', array(
            'joins' => array(
                array(
                    'table' => 'places',
                    'alias' => 'Place1',
                    'conditions' => array(
                        'Place1.id = Document.parent_id',
                    ),
                ),
            ),
            'conditions' => array(
                'Document.category' => 'place',
                'Document.group' => 'client',
            ),
            'fields' => array(
                'Document.url', 'Document.parent_id', 'Place1.name', 'Place1.id',
            ),
        ));
        $places = array();
        foreach ($data as $k => $item) {
            $places[$item['Place1']['id']]['name'] = $item['Place1']['name'];
            $places[$item['Place1']['id']]['images'][$k] = $item['Document']['url'];
        }
        $this->set(compact('places'));
    }

  public function json() {
    $this->autoRender = false;
    $data = $this->Place->find('all', array(
      'conditions' => array(
        'OR' => array(
          'Place.name LIKE' => '%'.$this->request->data['q'].'%',
          'Place.city LIKE' => '%'.$this->request->data['q'].'%',
        ),
      ),
    ));
    $places = array();
    if (empty($places)) {
      foreach ($data as $k => $item) {
        if (empty($item['Place']['name'])) {
          continue;
        }
        $places[$k]['id'] = $item['Place']['id'];
        $places[$k]['text'] = $item['Place']['private'] ? 'PRIVÉ - ' : '';
        $places[$k]['text'] .= $item['Place']['name'];
        if (!empty($item['Place']['zip_city'])) {
          $places[$k]['text'] .= ' - '.$item['Place']['zip_city'];
        }
        $places[$k]['name'] = $item['Place']['name'];
      }
    }
    $this->response->body(json_encode($places));
  }

  public function updateNonStandardName() {
    if ($this->request->is('ajax')) {
      $this->autoRender = false;
      $eventPlace = $this->Place->EventPlace->findById($this->request->data['event_place_id']);
      if (!empty($eventPlace)) {
        $eventPlace['EventPlace']['name'] = $this->request->data['name'];
        if ($this->Place->EventPlace->save($eventPlace, array('callbacks' => false))) {
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }
    }
  }

  public function calendar(){

    if($this->request->is('ajax')){
      $view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
  		$ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
  		$start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
  		$end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
  		$start_date = date('Y-m-d', $start);
  		$end_date = date('Y-m-d', $end);
  		$options = array();

  		if ($view == 'month') {
  			$month = date('m', $ts);
  			$days = date('t', $ts);
  			$start_date = date(sprintf('Y-%s-01', $month));
  			$end_date = date(sprintf('Y-%s-%s', $month, $days));
  			$end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
  		}

      $conditions = array();
      if(!empty($this->request->data['filters'])){
        foreach($this->request->data['filters'] as $filter){
          if($filter['field'] == 'option' && !empty($filter['value'])){
  					$conditions[] = array('EventPlace.option' => $filter['value']);
  				}
        }
      }
      $conditions[] = array(
        'EventPlace.option <>' => 'managed_by_client',
        'Event.crm_status <>' => array('partner', 'cancelled', 'null'),
        'OR' => array(
          array(
            array('Event.confirmed_date <>' => null),
            array('Event.confirmed_date >=' => $start_date),
            array('Event.confirmed_date <=' => $end_date)
          ),
          array(
            array('Event.confirmed_date' => null),
            array('Date.date >=' => $start_date),
            array('Date.date <=' => $end_date)
          ),
        )
      );

  		$data = $this->Place->EventPlace->find('all', array(
  			'joins' => array(
          array(
            'table' => 'events',
            'alias' => 'Event',
            'conditions' => array(
              'Event.id = EventPlace.event_id'
            )
          ),
          array(
            'table' => 'places',
            'alias' => 'Place',
            'conditions' => array(
              'EventPlace.place_id = Place.id'
            )
          ),
          array(
            'table' => 'clients',
            'alias' => 'Client',
            'conditions' => array(
              'Event.client_id = Client.id'
            )
          ),
          array(
            'table' => 'companies',
            'alias' => 'Company',
            'conditions' => array(
              'Event.company_id = Company.id'
            )
          ),
          array(
            'table' => 'dates',
            'alias' => 'Date',
            'type' => 'LEFT',
            'conditions' => array(
              'Date.event_id = Event.id',
              'Event.confirmed_date' => null
            )
          )
  			),
        'fields' => array('Event.code', 'Event.name', 'Event.confirmed_date', 'Company.class', 'Client.name', 'Place.name', 'Place.zip', 'Place.city', 'Date.*', 'EventPlace.id', 'EventPlace.place_id', 'EventPlace.option'),
  			'conditions' => $conditions,
        'order' => 'Event.id'
  		));
  		$options = array();
      $counter = 0;
  		if(!empty($data)){
  			foreach($data as $k => $option){
          $this->Place->id = $option['EventPlace']['place_id'];
          $classes = array('place');
          $classes[] = 'place--' . $option['EventPlace']['option'];

          if(!empty($option['Event']['confirmed_date'])){
            $date = $option['Event']['confirmed_date'];
          } elseif(empty($option['Event']['confirmed_date']) && !empty($option['Date'])){
            $date = $option['Date']['date'];
            $classes[] = 'place--potential';
          } else {
            continue;
          }

          $html = $this->Place->generateCalendarHtml($option, $date);
          $options[$counter++] = array(
            'id' => $option['EventPlace']['id'],
            'url' => Router::url(array('controller' => 'places', 'action' => 'view', $option['EventPlace']['place_id'])),
            'html' => $html,
            'start' => $date,
            'allDay' => true,
            'className' => implode(' ', $classes)
          );
  			}
  		}
  		$this->set('_json', $options);
    }
	}
}
