<?php

class SuppliersController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Suppliers' => [
				'model' => 'Supplier',
				'joins' => array(
					array(
						'table' => 'clients_companies',
						'alias' => 'ClientCompany',
						'type' => 'inner',
						'conditions' => array(
							'ClientCompany.client_id = Supplier.id'
						)
					)
				),
				'columns' => [
					'name',
					'Actions' => null,
				],
				'conditions' => array(
					'Supplier.type' => 'supplier'
				),
				'fields' => array(
					'Supplier.name', 'Supplier.address', 'Supplier.zip', 'Supplier.city', 'Supplier.country', 'Supplier.id'
				),
				'autoData' => false
			]
		]
	];

	public function index(){
		$this->DataTable->setViewVar(array('Suppliers'));
	}

	public function beforeFilter(){
		parent::beforeFilter();
		$this->DataTable->settings['Suppliers']['columns']['name']['label'] = __('Supplier');
	}

	public function add( ) {

		$this->set('companies', $this->Supplier->Company->find('list'));

		if($this->request->is('post') OR $this->request->is('put')){

			if($this->Supplier->saveAssociated($this->request->data)){
				if(!empty($this->request->data['Supplier']['logo']['tmp_name'])){
					$this->User->Document->saveFile($this->request->data['Supplier']['logo'], $this->Supplier->id, array('suppliers','logo'), 'supplier_logo', Configure::read('Documents.Images.allowedExtensions'));
				}
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Supplier']['name']), 'alert', array('type' => 'success'));
				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Supplier->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'supplier_id' => $this->Supplier->id));
					}  else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function edit($id = null){

		$this->Supplier->id = $id;
		$this->set('companies', $this->Supplier->Company->find('list'));

		$supplierCompanies = $this->Supplier->Company->find('list', array(
			'joins' => array(
				array(
					'table' => 'clients_companies',
					'alias' => 'ClientCompany',
					'type' => 'INNER',
					'conditions' => array(
						'ClientCompany.company_id = Company.id'
					)
				),
				array(
					'table' => 'clients',
					'alias' => 'Supplier',
					'type' => 'INNER',
					'conditions' => array(
						'Supplier.id = ClientCompany.client_id'
					)
				)
			),
			'conditions' => array(
				'Supplier.id' => $id
			),
			'fields' => array('Company.id')
		));
		$this->set(compact('supplierCompanies'));

		if($this->request->is('post') OR $this->request->is('put')){

			if(!empty($this->request->data['Supplier']['logo']['tmp_name'])){
				$this->Supplier->Document->saveFile($this->request->data['Supplier']['logo'], $id, array('suppliers','logo'), 'supplier_logo', Configure::read('Documents.Images.allowedExtensions'));
			}

			if(!empty($this->request->data['Supplier']['companies'])){
				foreach($this->request->data['Supplier']['companies'] as $company){
					$this->request->data['Company'][]['id'] = $company;
				}
			}

			//debug($this->request->data);exit;

			if($this->Supplier->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['Supplier']['name']), 'alert', array('type' => 'success'));

				if(!empty($this->request->data['destination'])){
					if($this->request->data['destination'] == 'edit'){
						return $this->redirect(array('action' => 'edit', $this->Supplier->id));
					} elseif($this->request->data['destination'] == 'contactpeople'){
						return $this->redirect(array('controller' => 'contact_peoples', 'action' => 'add', 'supplier_id' => $this->Supplier->id));
					} else {
						return $this->redirect(array('action' => 'index'));
					}
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		} else {
			$supplier = $this->Supplier->find('first', array(
				'conditions' => array(
					'Supplier.id' => $id
				),
				'contain' => array(
					'ContactPeople',
					'Logo'
				)
			));
			$this->request->data = $supplier;
		}
	}

}
