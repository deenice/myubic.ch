<?php

class OrderGroupsController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$lastGroup = $this->OrderGroup->find('first', array(
				'conditions' => array(
					'order_id' => $this->request->params['named']['order_id']
				),
				'order' => 'weight DESC'
			));
			$group = array(
				'OrderGroup' => array(
					'order_id' => empty($this->request->params['named']['order_id']) ? null : $this->request->params['named']['order_id'],
					'name' => $this->request->data['name'],
					'weight' => empty($lastGroup) ? 0 : $lastGroup['OrderGroup']['weight']+1
				)
			);
			if($this->OrderGroup->save($group)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->OrderGroup->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function rename($id = null){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->OrderGroup->id = $id;
			if($this->OrderGroup->saveField('name', $this->request->data['name'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['groups'])){
				foreach($this->request->data['groups'] as $weight => $group){
					$this->OrderGroup->id = $group;
					$this->OrderGroup->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
