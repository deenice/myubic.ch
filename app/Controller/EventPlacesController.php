<?php

class EventPlacesController extends AppController {

	public function update( $id = null ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->EventPlace->id = $this->request->data['id'];
			if($this->EventPlace->saveField('option', $this->request->data['option'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = null ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$ep = $this->EventPlace->findById($this->request->data['id']);
			if($this->EventPlace->delete($this->request->data['id'])){
				$this->loadModel('PlanningResource');
				$this->PlanningResource->remove($ep['EventPlace']['event_id'], 'place', $ep['EventPlace']['place_id']);
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
