<?php

class OrderAssociationsController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
      $existing = $this->OrderAssociation->find('first', array(
        'conditions' => array(
          'order_id' => $this->request->data['order_id'],
					'model' => $this->request->data['model'],
					'model_id' => $this->request->data['model_id']
        )
      ));
      if(empty($existing)){
        $ass = array(
  				'OrderAssociation' => array(
  					'order_id' => $this->request->data['order_id'],
  					'model' => $this->request->data['model'],
  					'model_id' => $this->request->data['model_id']
  				)
  			);
  			if($this->OrderAssociation->save($ass)){
  				$this->response->body(json_encode(array('success' => 1)));
  			} else {
  				$this->response->body(json_encode(array('success' => 0)));
  			}
      } else {
        $this->response->body(json_encode(array('existing' => 1)));
      }
		}
	}

	public function delete(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->OrderAssociation->deleteAll(array(
				'OrderAssociation.order_id' => $this->request->data['order_id'],
				'OrderAssociation.model' => $this->request->data['model'],
				'OrderAssociation.model_id' => $this->request->data['model_id']
			))){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}

	}

}
