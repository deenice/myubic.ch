<?php

class ChecklistTasksController extends AppController {

	public function add(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['checklist_batch_id'])){
				$batch_id = $this->request->data['checklist_batch_id'];
			} elseif(!empty($this->request->params['named']['checklist_batch_id'])){
				$batch_id = $this->request->params['named']['checklist_batch_id'];
			}
			$task = array(
				'ChecklistTask' => array(
					'name' => $this->request->data['name'],
					'checklist_batch_id' => $batch_id
				)
			);
			if($this->ChecklistTask->save($task)){
				$this->response->body(json_encode(array('success' => 1, 'id' => $this->ChecklistTask->id)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function edit( $id = null ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$task = array(
				'ChecklistTask' => array(
					'id' => $this->request->data['ChecklistTask']['id'],
					'due_date' => empty($this->request->data['ChecklistTask']['due_date']) ? null : date('Y-m-d', strtotime($this->request->data['ChecklistTask']['due_date'])),
					'done' => $this->request->data['ChecklistTask']['done']
				)
			);
			if($this->ChecklistTask->save($task)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function done(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['id'])){
				$this->ChecklistTask->id = $this->request->data['id'];
				if($this->ChecklistTask->saveField('done', $this->request->data['done'])){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['tasks'])){
				foreach($this->request->data['tasks'] as $weight => $task){
					$this->ChecklistTask->id = $task;
					$this->ChecklistTask->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->ChecklistTask->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function updateField(){
		// x-editable
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$this->ChecklistTask->id = $this->request->data['pk'];
			if($this->ChecklistTask->saveField($this->request->data['name'], $this->request->data['value'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function modal( $id = null ){
		$this->ChecklistTask->id = $id;

		$task = $this->ChecklistTask->findById($id);
		$this->set(compact('task'));
	}

}
