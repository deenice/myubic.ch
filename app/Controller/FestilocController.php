<?php

class FestilocController extends AppController {

  public $components = [
    'DataTable.DataTable' => [
      'ContactPeoples' => [
        'model' => 'ContactPeople',
        'columns' => [
          'Client.name' => array(
            'bSearchable' => true
          ),
          'name' => array(
            'bSearchable' => true
          ),
          'address' => array(
            'useField' => false,
            'bSearchable' => false
          ),
          'Actions' => null
        ],
        'joins' => array(
            array(
              'table' => 'clients',
              'alias' => 'Client1',
              'type' => 'inner',
              'conditions' => array(
                'Client1.id = ContactPeople.client_id'
              )
            ),
            array(
              'table' => 'clients_companies',
              'alias' => 'ClientCompany',
              'type' => 'inner',
              'conditions' => array(
                'ClientCompany.client_id = Client.id'
              )
            ),
            array(
              'table' => 'companies',
              'alias' => 'Company',
              'type' => 'inner',
              'conditions' => array(
                'ClientCompany.company_id = Company.id'
              )
            )
        ),
        'contain' => array(
          'Client'
        ),
        'conditions' => array(
          'ContactPeople.name <>' => '',
          'ContactPeople.status' => 1,
          'ClientCompany.company_id' => 3
        ),
        'fields' => array('Company.name', 'Client.address', 'Client.zip', 'Client.country', 'Client.city', 'Client.id', 'Client.name', 'ContactPeople.full_name', 'ContactPeople.email', 'ContactPeople.phone'),
        'order' => array('Client.name', 'ContactPeople.created DESC'),
        'autoData' => false
      ]
    ],
  ];

  public function beforeFilter(){
    parent::beforeFilter();
    $this->DataTable->settings['ContactPeoples']['columns']['Client.name']['label'] = __('Client');
    $this->DataTable->settings['ContactPeoples']['columns']['name']['label'] = __('Contact person');
    $this->DataTable->settings['ContactPeoples']['columns']['address']['label'] = __('Address');
  }

  public function clients(){
    $this->DataTable->setViewVar(array('ContactPeoples'));
  }

  public function clients_json(){

    $view = new View($this);
    $html = $view->loadHelper('Html');

    $this->loadModel('ContactPeople');

    $contactPeoples = $this->ContactPeople->find('all', array(
      'joins' => array(
        array(
          'table' => 'clients',
          'alias' => 'Client1',
          'type' => 'inner',
          'conditions' => array(
            'Client1.id = ContactPeople.client_id'
          )
        ),
        array(
          'table' => 'clients_companies',
          'alias' => 'ClientCompany',
          'type' => 'inner',
          'conditions' => array(
            'ClientCompany.client_id = Client.id'
          )
        )
      ),
      'contain' => array(
        'Client'
      ),
      'conditions' => array(
        'ContactPeople.name <>' => '',
        'ContactPeople.status' => 1,
        'ClientCompany.company_id' => 3
      ),
      'order' => array('Client.name', 'ContactPeople.created DESC'),
      'cache' => '_fl_' . date('dmY'),
      'cacheConfig' => 'short'
    ));
    $data = Cache::read('clients_fl_json_' . date('dmY'), 'long');
    if(empty($data)){
      foreach($contactPeoples as $k => $cp){
        $data[$k]['Client']['id'] = $cp['Client']['id'];
        $data[$k]['Client']['name'] = $cp['Client']['name'];
        $data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['name'];
        $data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['email'];
        $data[$k]['ContactPeople']['name'][] = $cp['ContactPeople']['phone'];
        $data[$k]['Client']['address'][] = $cp['Client']['address'];
        $data[$k]['Client']['address'][] = $cp['Client']['zip_city'];
        $data[$k]['actions'][] = $html->link(
          '<i class="fa fa-search"></i> ' . __("View"),
          array('controller' => 'clients', 'action' => 'view', $cp['Client']['id']),
          array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
        );
        $data[$k]['actions'][] = $html->link(
          '<i class="fa fa-edit"></i> ' . __("Edit"),
          array('controller' => 'clients', 'action' => 'edit', $cp['Client']['id']),
          array('class' => 'btn btn-warning btn-xs', 'escape' => false, 'target' => '_blank')
        );

      }
    }
    Cache::write('clients_fl_json_' . date('dmY'), $data, 'long');

    $this->set(compact('data'));
    $this->set('_serialize', array('data'));

  }

  public function turnover(){
    $this->loadModel('StockOrder');
    if($this->request->is('post')){
      if(!empty($this->request->data['Turnover']['from']) && !empty($this->request->data['Turnover']['to'])){
        $data = $this->StockOrder->find('all', array(
          'conditions' => array(
            'return_date >=' => date('Y-m-d', strtotime($this->request->data['Turnover']['from'])),
            'return_date <=' => date('Y-m-d', strtotime($this->request->data['Turnover']['to'])),
            'status' => $this->request->data['Turnover']['status']
          ),
          'fields' => array(
            'StockOrder.net_total',
            'StockOrder.tva',
            'StockOrder.delivery_costs',
            'StockOrder.invoiced_total',
            'StockOrder.deposit',
            'StockOrder.total_deposit',
            'StockOrder.total_ht',
            'StockOrder.subtotal_ht',
            'StockOrder.fidelity_discount_amount',
            'StockOrder.quantity_discount',
            'StockOrder.type',
            'StockOrder.status',
          )
        ));
        $deliveryCosts = 0;
        $sales = 0;
        $rents = 0;
        foreach($data as $order){
          if($order['StockOrder']['status'] == 'invoiced'){
            $projected = $order['StockOrder']['invoiced_total'] - $order['StockOrder']['tva'] + $order['StockOrder']['total_deposit'];
          } else {
            $projected = $order['StockOrder']['total_ht'] - $order['StockOrder']['fidelity_discount_amount'] - $order['StockOrder']['quantity_discount'];
          }
          if($order['StockOrder']['type'] == 'sale'){
            $sales += $projected;
          } else {
            $rents += $projected;
          }
          if(!empty($order['StockOrder']['forced_delivery_costs'])){
            $deliveryCosts += $order['StockOrder']['forced_delivery_costs'];
          } else {
            $deliveryCosts += $order['StockOrder']['delivery_costs'];
          }
        }
        $deliveryCosts = number_format($deliveryCosts, 2, '.','');
        $sales = number_format($sales, 2, '.','');
        $rents = number_format($rents, 2, '.','');
        $this->set(compact('sales'));
        $this->set(compact('rents'));
        $this->set(compact('deliveryCosts'));
      }
    }
  }

  public function depot($view = '', $id = ''){

    $this->loadModel('StockOrder');
    $this->layout = 'festiloc';

    $this->set('statuses', Configure::read('StockOrders.status'));
    $this->set('delivery_modes', Configure::read('StockOrders.delivery_modes'));
    $this->set('return_modes', Configure::read('StockOrders.return_modes'));
    $this->set('moments', Configure::read('StockOrders.delivery_moments'));

    switch($view){
      case 'live':
      case '':
      $this->view = 'depot_live';

      break;

      case 'portlet':
      case 'portlet_modifications':
      $this->layout = 'ajax';
      $this->view = 'depot_portlet';
      $this->StockOrder->id = $id;
      $this->set('data', $this->StockOrder->getModifications(true));
      break;

      case 'detail':
      case 'issue':
      case 'return':
      $this->view = 'depot_' . $view;
      $stockorder = $this->StockOrder->find('first', array(
        'conditions' => array(
          'StockOrder.id' => $id,
        ),
        'contain' => array(
          'StockOrderBatch' => array(
            'StockItem' => array('Document'),
          ),
          'ContactPeopleDelivery',
          'ContactPeopleReturn',
          'ContactPeopleClient',
          'Client',
          'StockOrderExtraHour',
        )
      ));
      if(!empty($stockorder['StockOrder']['payment_date'])){
        $stockorder['StockOrder']['payment_date'] = date('d-m-Y', strtotime($stockorder['StockOrder']['payment_date']));
      }
      $this->set(compact('stockorder'));
      $collaborators = $this->StockOrder->Responsable->find('list', array(
        'conditions' => array(
          'Responsable.role' => 'fixed',
        ),
        'order' => 'Responsable.full_name',
        'fields' => array('Responsable.id', 'Responsable.full_name'),
      ));
      $this->set(compact('collaborators'));
      break;

      case 'json':
      if($this->request->data['show'] == 'false'){
        $this->set('_json', array());
      } else {
        $mode = $this->request->data['mode'];
        $view = $this->request->data['view'];
        $ts = strtotime($this->request->data['date']);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
        $end = (date('w', $ts) == 0) ? $ts : strtotime('next sunday', $ts);
        $start_date = date('Y-m-d', $start);
        $end_date = date('Y-m-d', $end);
        $options = array();

        if($view == 'month'){
          $month = date('m', $ts);
          $days = date('t', $ts);
          $start_date = date(sprintf('Y-%s-01', $month));
          $end_date = date(sprintf('Y-%s-%s', $month, $days));
          $end_date = date('Y-m-d', strtotime($end_date . '+ 7 days'));
        }

        $options['conditions'][] = array('StockOrder.status NOT IN' => array('offer', 'to_invoice', 'invoiced', 'cancelled'));

        if ($mode == 'delivery') {
          $options['conditions'] = array(
            'delivery_date >=' => $start_date,
            'delivery_date <=' => $end_date,
            'StockOrder.status NOT IN' => array('offer', 'to_invoice', 'invoiced', 'cancelled'),
            'StockOrder.type <>' => 'withdrawal'
          );
        } elseif ($mode == 'return') {
          $options['conditions'] = array(
            'return_date >=' => $start_date,
            'return_date <=' => $end_date,
            'StockOrder.status NOT IN' => array('offer', 'to_invoice', 'invoiced', 'cancelled'),
            'StockOrder.type <>' => array('delivery', 'sale')
          );
        } elseif ($mode == 'cancellations') {
          $options['conditions'] = array(
            'OR' => array(
              array(
                'delivery_date >=' => $start_date,
                'delivery_date <=' => $end_date
              ),
              array(
                'return_date >=' => $start_date,
                'return_date <=' => $end_date
              )
            ),
            'StockOrder.status' => array('cancelled'),
            'StockOrder.notify_cancellation' => 1
          );
        }
        $options['contain'] = array('Client');
        $data = $this->StockOrder->find('all', $options);
        $orders = array();
        foreach($data as $k => $order){
          $classes = array();
          $bgColors = array();
          $this->StockOrder->id = $order['StockOrder']['id'];
          $orders[$k]['title'] = $order['StockOrder']['order_number'];
          $orders[$k]['stock_order_id'] = $order['StockOrder']['id'];
          $orders[$k]['stock_order_order_number'] = $order['StockOrder']['order_number'];
          $orders[$k]['portlet_url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'portlet', $order['StockOrder']['id']));

          $orders[$k]['url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'detail', $order['StockOrder']['id']));
          $orders[$k]['depot_detail_url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'detail', $order['StockOrder']['id']));
          $orders[$k]['depot_issue_url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'issue', $order['StockOrder']['id']));
          $orders[$k]['depot_return_url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'return', $order['StockOrder']['id']));

          $orders[$k]['depot_delivery_note_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0&showPrices=0&source=depot'));
          $orders[$k]['depot_pallet_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'pallet', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0&source=depot'));
          $orders[$k]['depot_return_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'return_pdf', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0'));
          $orders[$k]['depot_return_pdf_all_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'return_pdf', $order['StockOrder']['return_date'] . '.pdf', '?' => 'download=0'));

          $packaging = $this->StockOrder->getPackaging();
          $packaging1 = '';
          if(!empty($packaging['pallets'])) $packaging1 .= $packaging['pallets'] . ' palette(s) ';
          if(!empty($packaging['pallets_xl'])) $packaging1 .= $packaging['pallets_xl'] . ' palette(s) XL ';
          if(!empty($packaging['rollis'])) $packaging1 .= $packaging['rollis'] . ' rolli(s) ';
          if(!empty($packaging['rollis_xl'])) $packaging1 .= $packaging['rollis_xl'] . ' rolli(s) XL ';
          $orders[$k]['packaging'] = trim($packaging1);

          $classes[] = 'depot-order';

          if($mode == 'delivery'){
            $orders[$k]['start'] = $order['StockOrder']['delivery_date'];
            if(empty($order['StockOrder']['delivery_weight'])){
              if($order['StockOrder']['delivery_mode'] == 'festiloc'){
                $orders[$k]['id'] = 1000;
              }
              elseif($order['StockOrder']['delivery_mode'] == 'transporter'){
                $orders[$k]['id'] = 2000;
              }
              elseif($order['StockOrder']['delivery_mode'] == 'client'){
                $orders[$k]['id'] = 3000;
              } else {
                $orders[$k]['id'] = 5000;
              }
            } else {
              $orders[$k]['id'] = '0' . $order['StockOrder']['delivery_weight'];
            }
            $orders[$k]['delivery_weight'] = $order['StockOrder']['delivery_weight'];
            $classes[] = $order['StockOrder']['delivery_mode'];
            $orders[$k]['title'] = $order['StockOrder']['order_number'];
            $orders[$k]['description'] = (!empty($order['StockOrder']['delivery_weight'])) ? '<span class="btn btn-xs">'.$order['StockOrder']['delivery_weight'].'</span> ' : '';
            $orders[$k]['description'] .= '<span>' . $order['Client']['name'] . '<br>';
            $orders[$k]['description'] .= $order['StockOrder']['order_number'] . '</span>';
            $orders[$k]['depot_transporter_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'transporter_pdf', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0'));
          }
          if($mode == 'return'){
            $orders[$k]['start'] = $order['StockOrder']['return_date'];
            if(empty($order['StockOrder']['return_weight'])){
              if($order['StockOrder']['return_mode'] == 'festiloc'){
                $orders[$k]['id'] = 1000;
              }
              elseif($order['StockOrder']['return_mode'] == 'transporter'){
                $orders[$k]['id'] = 2000;
              }
              elseif($order['StockOrder']['return_mode'] == 'client'){
                $orders[$k]['id'] = 3000;
              } else {
                $orders[$k]['id'] = 5000;
              }
            } else {
              $orders[$k]['id'] = '0' . $order['StockOrder']['return_weight'];
            }
            $orders[$k]['return_weight'] = $order['StockOrder']['return_weight'];
            $classes[] = $order['StockOrder']['return_mode'];
            $orders[$k]['title'] = $order['StockOrder']['order_number'];
            $orders[$k]['description'] = (!empty($order['StockOrder']['return_weight'])) ? '<span class="btn btn-xs">'.$order['StockOrder']['return_weight'].'</span> ' : '';
            $orders[$k]['description'] .= '<span>' . $order['Client']['name'] . '<br>';
            $orders[$k]['description'] .= $order['StockOrder']['order_number'] . '</span>';
            $orders[$k]['depot_transporter_pdf_url'] = Router::url(array('controller' => 'stock_orders', 'action' => 'transporter_pdf', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0&type=return'));
          }
          if($mode == 'cancellations'){
            $orders[$k]['start'] = $order['StockOrder']['delivery_date'];
            $orders[$k]['id'] = 0;
            $orders[$k]['notify_cancellation'] = 1;
          }
          $classes[] = $mode;
          $orders[$k]['mode'] = $mode;
          $orders[$k]['status'] = $order['StockOrder']['status'];
          switch($order['StockOrder']['status']){
            case 'confirmed':
            $bgColors[] = '#26C281';
            break;
            case 'in_progress':
            $bgColors[] = '#F3C200';
            break;
            case 'delivered':
            $bgColors[] = '#555';
            break;
            case 'processed':
            $bgColors[] = '#3598dc';
            break;
            case 'cancelled':
            $bgColors[] = '#E87E04';
            break;
            default:
            break;
          }
          $classes[] = $order['StockOrder']['status'];

          if($order['StockOrder']['notify_depot'] == 1){
            $classes[] = 'notify';
            $orders[$k]['portlet_modifications_url'] = Router::url(array('controller' => 'festiloc', 'action' => 'depot', 'portlet_modifications', $order['StockOrder']['id']));
          }


          $orders[$k]['className'] = implode(' ', $classes);
          $orders[$k]['backgroundColor'] = implode(' ', $bgColors);
        }

        $this->set('_json', $orders);
      }
      break;

      default:
      break;
    }
  }

  public function revelate(){
    $this->loadModel('Company');
    $this->loadModel('StockOrder');
    $this->StockOrder->contain(array(
      'Client'
    ));
    $stockorders = $this->StockOrder->find('all', array(
      'conditions' => array(
        'export_status' => 'to_export',
      )
    ));
    $this->set(compact('stockorders'));
    $company = $this->Company->findById(3);
    $this->set(compact('company'));

  }

  public function logista( $year = '' ){
    $this->loadModel('StockOrder');
    if(empty($year)){
      $year = date('Y');
    }
    $options = array();
    $options['fields'] = array(
      'id',
      'order_number',
      'delivery_date',
      'delivery_costs',
      'forced_delivery_costs',
      'return_date',
      'Client.name'
    );
    $options['contain'] = array('Client');

    $options['conditions'] = array(
      'order_number <>' => null,
      'delivery_transporter_id' => 2,
      'delivery_date <=' => date(sprintf('%s-12-31', $year)),
      'delivery_date >=' => date(sprintf('%s-01-01', $year)),
      'status' => array('invoiced', 'to_invoice')
    );
    $options['order'] = array('delivery_date');
    $deliveries = $this->StockOrder->find('all', $options);

    $options['conditions'] = array(
      'order_number <>' => null,
      'return_transporter_id' => 2,
      'return_date <=' => date(sprintf('%s-12-31', $year)),
      'return_date >=' => date(sprintf('%s-01-01', $year)),
      'status' => array('invoiced', 'to_invoice')
    );
    $options['order'] = array('return_date');
    $returns = $this->StockOrder->find('all', $options);

    $data = array();
    $data1 = array();
    foreach($deliveries as $order){
      $costs = array();
      $month = date('Y-m-d', strtotime($order['StockOrder']['delivery_date']));
      $k = $month . '-' . $order['StockOrder']['id'];
      $order['StockOrder']['mode'] = 'delivery';
      $data1[$k] = $order;
    }
    foreach($returns as $order){
      $month = date('Y-m-d', strtotime($order['StockOrder']['return_date']));
      $k = $month . '-' . $order['StockOrder']['id'];
      $order['StockOrder']['mode'] = 'return';
      $data1[$k] = $order;
    }
    ksort($data1);
    foreach($data1 as $k => $order){
      $month = date('Y-m-01', strtotime($order['StockOrder']['mode'] == 'return' ? $order['StockOrder']['return_date'] : $order['StockOrder']['delivery_date']));
      $this->StockOrder->id = $order['StockOrder']['id'];
      $packaging = $this->StockOrder->getPackaging();
      if($packaging['total'] == 0){
        $this->StockOrder->id = null;
        continue;
      } else {
        $costs = array();
        $estimations = array();
        if($packaging['pallets'] == 1){
          $costs['pallets'] = 65;
          $estimations['pallets'] = 75;
        } elseif($packaging['pallets'] > 1){
          $costs['pallets'] = 50 * $packaging['pallets'];
          $estimations['pallets'] = 60 * $packaging['pallets'];
        } else {
          $costs['pallets'] = 0;
          $estimations['pallets'] = 0;
        }
        if($packaging['rollis'] == 1){
          $costs['rollis'] = 60;
          $estimations['rollis'] = 70;
        } elseif($packaging['rollis'] > 1){
          $costs['rollis'] = 40 * $packaging['rollis'];
          $estimations['rollis'] = 50 * $packaging['rollis'];
        } else {
          $costs['rollis'] = 0;
          $estimations['rollis'] = 0;
        }
        $costs['xl'] = ($packaging['pallets_xl'] + $packaging['rollis_xl']) * 17;
        $estimations['xl'] = ($packaging['pallets_xl'] + $packaging['rollis_xl']) * 17.5;
        $packaging['xl'] = ($packaging['pallets_xl'] + $packaging['rollis_xl']);
        $costs['total'] = $costs['pallets'] + $costs['rollis'] + $costs['xl'];
        $estimations['total'] = $estimations['pallets'] + $estimations['rollis'] + $estimations['xl'];
      }
      $order['StockOrder']['packaging'] = $packaging;
      $order['StockOrder']['packaging_costs'] = $costs;
      $order['StockOrder']['estimations'] = $estimations;
      $data[$month][] = $order;
      unset($data1[$k]);
    }
    $this->set(compact('data'));
  }

}
