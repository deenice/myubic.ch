<?php

class VehicleCompaniesController extends AppController {

	public $components = [
        'DataTable.DataTable' => [
            'Companies' => [
            	'model' => 'VehicleCompany',
                'columns' => [
                    'name',
                    'address' => array(
                    	'useField' => false
                    ),
                    'contact' => array(
                    	'useField' => false
                    ),
                    'Actions' => null,
                ],
                'fields' => array(
                    'VehicleCompany.id',
                    'VehicleCompany.name',
                    'VehicleCompany.address',
                    'VehicleCompany.zip',
                    'VehicleCompany.city',
                    'VehicleCompany.contact_person_first_name',
                    'VehicleCompany.contact_person_last_name',
                    'VehicleCompany.contact_person_phone',
                    'VehicleCompany.contact_person_email'
                ),
                'autoData' => false
            ]
        ],
    ];

	public function index() {
		$this->DataTable->setViewVar(array('Companies'));
	}

    public function beforeFilter() {
        parent::beforeFilter();
        $this->DataTable->settings['Companies']['columns']['name']['label'] = __('Name');
        $this->DataTable->settings['Companies']['columns']['contact']['label'] = __('Contact person');
        $this->DataTable->settings['Companies']['columns']['address']['label'] = __('Address');
    }

	public function add() {
		if($this->request->is('put') || $this->request->is('post')){
			if($this->VehicleCompany->save($this->request->data)){
				$this->Session->setFlash(__('Company has been saved.'), 'alert', array('type' => 'success'));
                if($this->request->data['destination'] == 'edit'){
                    return $this->redirect(array('action' => 'edit', $this->VehicleCompany->id));
                } else {
                    return $this->redirect(array('action' => 'index'));                    
                }
			} else {
                $this->Session->setFlash(__('Company has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
		}
	}

	public function edit($id = null){

		if($this->request->is('put') || $this->request->is('post')){
			if($this->VehicleCompany->save($this->request->data)){
				$this->Session->setFlash(__('Company has been saved.'), 'alert', array('type' => 'success'));
                if($this->request->data['destination'] == 'edit'){
                    return $this->redirect(array('action' => 'edit', $this->VehicleCompany->id));
                } else {
                    return $this->redirect(array('action' => 'index'));                    
                }
			} else {
                $this->Session->setFlash(__('Company has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
            }
		} else {
			$this->request->data = $this->VehicleCompany->read(null, $id);
		}

	}


}