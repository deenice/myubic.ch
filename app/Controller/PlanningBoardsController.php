<?php

class PlanningBoardsController extends AppController {

  public function add( $model_id = '', $model = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->set(compact('model_id'));
      $this->set(compact('model'));
      $highestWeight = $this->PlanningBoard->find('first', array(
        'conditions' => array(
          'model_id' => $model_id,
          'model' => $model
        ),
        'fields' => array(
          'weight'
        ),
        'order' => 'weight DESC'
      ));
      if(empty($highestWeight)){
        $weight = 0;
      } else {
        $weight = $highestWeight['PlanningBoard']['weight'] + 1;
      }
      $this->set(compact('weight'));
      $this->render('modal');
    }
  }

  public function edit( $id = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->PlanningBoard->save($this->request->data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function delete( $id = '' ){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if($this->PlanningBoard->delete($id)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function modal($id = ''){
    $board = $this->PlanningBoard->findById($id);
    $this->set(compact('board'));
  }

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['boards'])){
				foreach($this->request->data['boards'] as $weight => $board){
					$this->PlanningBoard->id = $board;
					$this->PlanningBoard->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
