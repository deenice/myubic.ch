<?php

class CacheController extends AppController {

	function clear(){

		App::uses('ClearCache', 'ClearCache.Lib');
		$ClearCache = new ClearCache();
		$output = $ClearCache->files();

		if(empty($output['error'])){
			$this->Session->setFlash(__('Cache has been cleared. You should clear your browser cache too.'), 'alert', array('type' => 'success'));
		} else {
			$this->Session->setFlash(__('An error occured. Cache could not be cleared.'), 'alert', array('type' => 'danger'));
		}
		return $this->redirect($this->referer());
	}

}