<?php

class VehicleTourMomentsController extends AppController {

	public function add(){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			$end = $this->VehicleTourMoment->find('first', array(
				'conditions' => array(
					'VehicleTourMoment.vehicle_tour_id' => $this->request->data['VehicleTourMoment']['vehicle_tour_id'],
					'VehicleTourMoment.type' => 'end'
				)
			));
			$weight = $end['VehicleTourMoment']['weight'];
			$this->request->data['VehicleTourMoment']['weight'] = $weight;
			if($this->VehicleTourMoment->save($this->request->data)){
				$this->response->body(json_encode(array('success' => 1, 'id' => $this->VehicleTourMoment->id)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function edit( $id = null ){

		if($this->request->is('ajax')){
			$this->autoRender = false;

			if($this->VehicleTourMoment->save($this->request->data)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function delete( $id = null ){

		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->VehicleTourMoment->delete($this->request->data['id'])){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

}
