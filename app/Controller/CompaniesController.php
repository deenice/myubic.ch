<?php

class CompaniesController extends AppController {

  public $components = [
    'DataTable.DataTable' => [
      'Company' => [
        'columns' => [
          'name',
          'Actions' => null,
        ],
        'fields' => array('Company.class', 'Company.id'),
        'order' => array('Company.name' => 'asc'),
        'autoData' => false
      ]
    ],
  ];

  public function index() {
    $this->DataTable->setViewVar(array('Company'));
  }

  public function beforeFilter() {
    parent::beforeFilter();
    $this->DataTable->settings['Company']['columns']['name']['label'] = __('Name');
  }

  public function edit( $id = null ){

    $this->loadModel('Document');

    $this->Company->id = $id;

    if (!$this->Company->exists()) {
      throw new NotFoundException(__('Invalid Company'));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      $this->Company->create();
      if ($this->Company->save($this->request->data)) {
        if(!empty($this->request->data['Company']['logo']['tmp_name'])){
					$this->Document->saveFile($this->request->data['Company']['logo'], $this->Company->id, array('companies','logo'), 'company_logo', Configure::read('Documents.Images.allowedExtensions'));
				}
        $this->Session->setFlash(__('Company has been saved.'), 'alert', array('type' => 'success'));
        if($this->request->data['destination'] == 'edit'){
          return $this->redirect(array('action' => 'edit', $this->Company->id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
      } else {
        $this->Session->setFlash(__('Company has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
      }
    } else {
      $company = $this->Company->find('first', array(
        'conditions' => array(
          'Company.id' => $id
        ),
        'contain' => array('Logo')
      ));

      $this->set(compact('company'));
      $this->request->data = $company;
    }

  }

}
