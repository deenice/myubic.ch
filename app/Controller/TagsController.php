<?php

class TagsController extends AppController {

	//public $scaffold;

	/*public function index(){

		//debug($this->Tag->getLanguages());

	}*/

	public function languages() {
	    $this->request->onlyAllow('ajax'); // No direct access via browser URL - Note for Cake2.5: allowMethod()
	 
	    $data = array(
	        'content' => $this->Tag->find('list'),
	        'error' => '',
	    );
	    $this->set(compact('data')); // Pass $data to the view
	    $this->set('_serialize', 'data'); // Let the JsonView class know what variable to use
	}

	public function select2(){
		$this->autoRender = false;
        if ($this->request->is('ajax')) {
        	$data = $this->Tag->find('all', array(
        		'conditions' => array(
        			'category' => $_POST['category'],
        			'value LIKE' => '%' . $_POST['term'] . '%'
        		),
                'order' => 'value'
        	));
        	foreach($data as $k => $item){
        		$tags[$k]['id'] = $item['Tag']['id'];
        		$tags[$k]['text'] = $item['Tag']['value'];
        	}
        	if(empty($tags)){
        		$tags[0]['id'] = $_POST['term'];
        		$tags[0]['text'] = $_POST['term']; 
        	}
        	$this->response->body(json_encode($tags));            
        }
	}

	public function getValue(){
        $this->autoRender = false;
        if ($this->request->is('ajax')) {
        	$value = $this->Tag->findById($_POST['id']);
        	$value = $value['Tag']['value'];
        	$this->response->body(json_encode($value));
        }
	}
}