<?php

App::uses('HttpSocket', 'Network/Http');

class WebServicesController extends AppController {

	//public $components = array('PhpExcel.PhpExcel');

	public function excel( $companyId = null ) {
		// header("Content-Type: application/vnd.ms-excel; charset=UTF-8");
		// header("Content-Type: application/force-download");
		// header("Content-Type: application/download");
		// header("Content-Disposition: inline; filename=\"".'test'.".xls\"");

		$this->autoRender = false;
		$this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
		$line1 = array(
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => __('Adresse Générale')),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => __('Adresse Facturation')),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => __('Adresse Livraison')),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => ''),
	    array('label' => '')
		);
		$line2 = array(
	    array('label' => __('Code')), // A
	    array('label' => __('Type')), // B
	    array('label' => __('Nom')), // C
	    array('label' => __('Prénom')), // D
	    array('label' => __('2èmes Prénoms')), // E
	    array('label' => __('Politesse')), // F
	    array('label' => __('Initiales')), // G
	    array('label' => __('Date de Naissance')), // H
	    array('label' => __('Complément Nom')), // I
	    array('label' => __('Code Employeur')), // J
	    array('label' => __('Titre')), // K
	    array('label' => __('Profession')), // L
	    array('label' => __("Domain d'activité")), // M
	    array('label' => __('Est client?')), // N
	    array('label' => __('Est fournisseur?')), // O
	    array('label' => __('Est employé?')), // P
	    array('label' => __('IBAN')), // Q
	    array('label' => __('CCP')), // R
	    array('label' => __('Téléphone Fixe')), // S
	    array('label' => __('Téléphone Mobile')), // T
	    array('label' => __('Fax')), // U
	    array('label' => __('Email')), // V
	    array('label' => __('Site web')), // W
	    array('label' => __('Rue')), // X
	    array('label' => __('Rue / Case postale')), // Y
	    array('label' => __('Code Postal')), // Z
	    array('label' => __('Ville')), // AA
	    array('label' => __('Canton')), // AB
	    array('label' => __('Code Pays')), // AC
	    array('label' => __('Rue')), // AD
	    array('label' => __('Rue / Case postale')), // AE
	    array('label' => __('Code Postal')), // AF
	    array('label' => __('Ville')), // AG
	    array('label' => __('Canton')), // AH
	    array('label' => __('Code Pays')), // AI
	    array('label' => __('Rue')), // AJ
	    array('label' => __('Rue / Case postale')), // AK
	    array('label' => __('Code Postal')), // AL
	    array('label' => __('Ville')), // AM
	    array('label' => __('Canton')), // AN
	    array('label' => __('Code Pays')), // AO
	    array('label' => __('Notes')) // AP
		);
		$this->PhpExcel->addTableHeader($line1, array('name' => 'Cambria', 'bold' => true));
		$this->PhpExcel->addTableHeader($line2, array('name' => 'Cambria', 'bold' => true));

		$civilities = Configure::read('ContactPeople.civilities');
		$this->loadModel('Client');
		$this->loadModel('Company');
		$data = $this->Client->getAll(array($companyId));
		$company = $this->Company->findById($companyId);
		$counter = 3;
		foreach ($data as $k => $d) {
			$clientCode = Inflector::slug(strtolower($d['Client']['name']));
			$this->PhpExcel->getActiveSheet()
				->setCellValue('A' . $counter, $clientCode)
				->setCellValue('B' . $counter, 'PM')
				->setCellValue('C' . $counter, $d['Client']['name'])
				->setCellValue('X' . $counter, $d['Client']['address'])
				->setCellValue('Z' . $counter, $d['Client']['zip'])
				->setCellValue('AA' . $counter, $d['Client']['city']);
			$counter++;
			if(!empty($d['ContactPeople'])){
				foreach($d['ContactPeople'] as $j => $cp){
					$this->PhpExcel->getActiveSheet()
						->setCellValue('A' . $counter, Inflector::slug(strtolower($cp['full_name'])))
						->setCellValue('B' . $counter, 'PP')
						->setCellValue('C' . $counter, $cp['first_name'])
						->setCellValue('D' . $counter, $cp['last_name'])
						->setCellValue('F' . $counter, empty($cp['civility']) ? '' : $civilities[$cp['civility']])
						->setCellValue('F' . $counter, $clientCode)
						->setCellValue('L' . $counter, empty($cp['function']) ? '' : $cp['function'])
						->setCellValue('M' . $counter, empty($cp['department']) ? '' : $cp['department'])
						->setCellValue('S' . $counter, empty($cp['phone']) ? '' : $cp['phone'])
						->setCellValue('V' . $counter, empty($cp['email']) ? '' : $cp['email']);
					$counter++;
				}
			}
		}
		$this->PhpExcel->save(WWW_ROOT . sprintf('files/clients_%s.xlsx', $company['Company']['name']));
	}

	public function export($model = '', $companyId = ''){

		$this->loadModel('Company');
		$this->loadModel('StockOrder');
		$this->loadModel('Client');
		$this->loadModel('Revelate');

		$company = $this->Company->findById($companyId);
		$this->set(compact('company'));

		if($model == 'stockorders' && $companyId == 3){
			$stockorders = $this->StockOrder->find('all', array(
				'conditions' => array(
					'status' => 'to_invoice'
				)
			));
			$this->set(compact('stockorders'));
			if($this->request->is('ajax')){
				$this->autoRender = false;

				$company = $this->Company->findById($this->request->data['companyId']);
				$stockorder = $this->StockOrder->findById($this->request->data['itemId']);
				$this->StockOrder->id = $this->request->data['itemId'];
				$xml = $this->Revelate->xml('stockorder', $stockorder['StockOrder']['id']);
				if(empty($stockorder['StockOrder']['uuid'])){
					// we need to create the client in revelate
					$result = $this->Revelate->post($company['Company']['uuid'], 'salesInvoices', $xml);
					$this->response->body(json_encode($result));
					// if($result->isOk()){
					// 	$result = Xml::toArray(Xml::build($result->body));
					// 	$this->StockOrder->saveField('uuid', $result['Identifier']['Id']);
					// 	$this->response->body(json_encode(array('success' => 1)));
					// } else {
					// 	$this->response->body(json_encode(array('success' => 0)));
					// }
				} else {
					// we update the client
					$this->response->body(json_encode(array('success' => 1)));
					// $result = $this->Revelate->put($company['Company']['uuid'], 'salesInvoices', $stockorder['StockOrder']['uuid'], $xml);
					// if($result->isOk()){
					// 	$this->response->body(json_encode(array('success' => 1)));
					// } else {
					// 	$this->response->body(json_encode(array('success' => 0)));
					// }
				}
			}
		}

		if($model == 'clients'){
			$clients = $this->Client->getAll(array($companyId));
			$this->set(compact('clients'));

			if($this->request->is('ajax')){
				$this->autoRender = false;

				$company = $this->Company->findById($this->request->data['companyId']);
				$client = $this->Client->findById($this->request->data['itemId']);
				$this->Client->id = $this->request->data['itemId'];
				$xml = $this->Revelate->xml('client', $client['Client']['id']);
				if(empty($client['Client']['uuid'])){
					// we need to create the client in revelate
					$result = $this->Revelate->post($company['Company']['uuid'], 'clients', $xml);
					if($result->isOk()){
						$result = Xml::toArray(Xml::build($result->body));
						$this->Client->saveField('uuid', $result['Identifier']['Id']);
						$this->response->body(json_encode(array('success' => 1)));
					} else {
						$this->response->body(json_encode(array('success' => 0)));
					}
				} else {
					// we update the client
					$this->response->body(json_encode(array('success' => 1)));
					// $result = $this->Revelate->put($company['Company']['uuid'], 'clients', $client['Client']['uuid'], $xml);
					// if($result->isOk()){
					// 	$this->response->body(json_encode(array('success' => 1)));
					// } else {
					// 	$this->response->body(json_encode(array('success' => 0)));
					// }
				}
			}
		}
	}

}
