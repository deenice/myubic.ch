<?php

class StockInventoriesController extends AppController {

	public $components = [
    'DataTable.DataTable' => [
			'All' => [
      	'model' => 'StockInventory',
        'columns' => [
          'name',
          'start',
          'end',
          'Actions' => null,
        ],
        'autoData' => false,
				'fields' => array('StockInventory.id')
      ],
			'Empty' => [
      	'model' => 'StockItem',
        'columns' => [
          'code',
          'name',
					'Actions' => null
        ],
        'autoData' => false,
				'joins' => array(
					array(
						'table' => 'stock_inventories_stock_items',
						'alias' => 'SISI',
						'type' => 'LEFT',
						'conditions' => array('SISI.stock_item_id = StockItem.id')
					),
					array(
						'table' => 'stock_inventories',
						'alias' => 'SI',
						'type' => 'LEFT',
						'conditions' => array('SISI.stock_inventory_id = SI.id')
					)
				),
				'contain' => array('StockInventory'),
				'conditions' => array('SI.id IS NULL'),
				'fields' => array('StockItem.id', 'StockItem.code'),
				'order' => array('StockItem.code')
      ]
    ]
  ];

	public function index() {
		$this->DataTable->setViewVar(array('All', 'Empty'));
		$inventories = $this->StockInventory->find('all', array(
			'contain' => 'StockItem',
			'order' => 'start'
		));
		$this->set(compact('inventories'));
	}

  public function beforeFilter() {
    parent::beforeFilter();
    $this->DataTable->settings['All']['columns']['name']['label'] = __('Name');
    $this->DataTable->settings['Empty']['columns']['name']['label'] = __('Stock item');
    $this->DataTable->settings['All']['columns']['start']['label'] = __('Start date');
    $this->DataTable->settings['All']['columns']['end']['label'] = __('End date');
  }

	public function add(){
		if($this->request->is('put') || $this->request->is('post')){
			if(!empty($this->request->data['StockInventory']['start'])){
				$this->request->data['StockInventory']['start'] = date('Y-m-d', strtotime($this->request->data['StockInventory']['start']));
			}
			if(!empty($this->request->data['StockInventory']['end'])){
				$this->request->data['StockInventory']['end'] = date('Y-m-d', strtotime($this->request->data['StockInventory']['end']));
			}
			if($this->StockInventory->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockInventory']['name']), 'alert', array('type' => 'success'));
        if($this->request->data['destination'] == 'edit'){
          return $this->redirect(array('action' => 'edit', $this->StockInventory->id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
			}
		}
	}

	public function edit( $id = null){
		if($this->request->is('put') || $this->request->is('post')){
			if(!empty($this->request->data['StockInventory']['start'])){
				$this->request->data['StockInventory']['start'] = date('Y-m-d', strtotime($this->request->data['StockInventory']['start']));
			}
			if(!empty($this->request->data['StockInventory']['end'])){
				$this->request->data['StockInventory']['end'] = date('Y-m-d', strtotime($this->request->data['StockInventory']['end']));
			}
			if($this->StockInventory->save($this->request->data)){
				$this->Session->setFlash(__('%s has been saved.', $this->request->data['StockInventory']['name']), 'alert', array('type' => 'success'));
        if($this->request->data['destination'] == 'edit'){
          return $this->redirect(array('action' => 'edit', $this->StockInventory->id));
        } else {
          return $this->redirect(array('action' => 'index'));
        }
			}
		} else {
			$inventory = $this->StockInventory->findById($id);
			if(!empty($inventory['StockInventory']['start'])){
				$inventory['StockInventory']['start'] = date('d-m-Y', strtotime($inventory['StockInventory']['start']));
			}
			if(!empty($inventory['StockInventory']['end'])){
				$inventory['StockInventory']['end'] = date('d-m-Y', strtotime($inventory['StockInventory']['end']));
			}
			$this->request->data = $inventory;
		}
	}

}
