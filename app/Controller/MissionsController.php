<?php

class MissionsController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Mission' => [
				'columns' => [
					'name',
					'Actions' => null,
				],
				'conditions' => array(
					'company_id <>' => null
				),
				'order' => array('Mission.confirmed_date' => 'asc'),
				'fields' => array(
					'Mission.id'
				),
				'autoData' => false
			]
		],
	];

	public function index() {
		return $this->redirect(array('controller' => 'events', 'action' => 'index', 'missions'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Mission']['columns']['name']['label'] = __('Name');
	}

	public function add(){
		$companies = $this->Mission->Company->find('list');
		$this->set(compact('companies'));

		$managers = $this->Mission->User->find('list', array(
			'conditions' => array(
				'role' => 'fixed'
			),
			'fields' => array('User.id', 'User.full_name'),
			'order' => array('User.first_name')
		));
		$this->set(compact('managers'));

		if($this->request->is('post') || $this->request->is('put')){
			if (!empty($this->request->data['Mission']['confirmed_date'])) {
				$this->request->data['Mission']['confirmed_date'] = date('Y-m-d', strtotime($this->request->data['Mission']['confirmed_date']));
			}
			$this->Mission->create();
			if ($this->Mission->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Mission has been saved.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Mission->id));
				} elseif ($this->request->data['destination'] == 'work') {
					return $this->redirect(array('controller' => 'events', 'action' => 'work', $this->Mission->id));
				} else {
					return $this->redirect(array('controller' => 'events', 'action' => 'index', '#' => 'missions'));
				}
			} else {
				$this->Session->setFlash(__('Mission has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function edit( $id = null ){

		$managers = $this->Mission->User->find('list', array(
			'conditions' => array(
				'role' => 'fixed'
			),
			'fields' => array('User.id', 'User.full_name'),
			'order' => array('User.first_name')
		));
		$this->set(compact('managers'));

		$companies = $this->Mission->Company->find('list');
		$this->set(compact('companies'));

		if($this->request->is('post') || $this->request->is('put')){
			if (!empty($this->request->data['Mission']['confirmed_date'])) {
				$this->request->data['Mission']['confirmed_date'] = date('Y-m-d', strtotime($this->request->data['Mission']['confirmed_date']));
			}
			$this->Mission->create();
			if ($this->Mission->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Mission has been saved.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Mission->id));
				} elseif ($this->request->data['destination'] == 'work') {
					return $this->redirect(array('controller' => 'events', 'action' => 'work', $this->Mission->id));
				} else {
					return $this->redirect(array('controller' => 'events', 'action' => 'index', '#' => 'missions'));
				}
			} else {
				$this->Session->setFlash(__('Mission has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$mission = $this->Mission->findById($id);
			$this->request->data = $mission;
      if (!empty($mission['Mission']['confirmed_date'])) {
        $this->request->data['Mission']['confirmed_date'] = date('d-m-Y', strtotime($this->request->data['Mission']['confirmed_date']));
      }
			$this->set(compact('mission'));
		}
	}

	public function calendar() {

		$status = empty($this->request->data['status']) ? '' : $this->request->data['status'];

		$view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
		$ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
		$start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
		$end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
		$start_date = date('Y-m-d', $start);
		$end_date = date('Y-m-d', $end);
		$options = array();

		if ($view == 'month') {
			$month = date('m', $ts);
			$days = date('t', $ts);
			$start_date = date(sprintf('Y-%s-01', $month));
			$end_date = date(sprintf('Y-%s-%s', $month, $days));
			$end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
		}
		$options['contain'] = array(
			'Company',
			'PersonInCharge'
		);

		if(!empty($this->request->data['filters'])){
			foreach($this->request->data['filters'] as $filter){
				if($filter['field'] == 'resp_id' && !empty($filter['value'])){
					$options['conditions'][] = array('Mission.resp_id' => $filter['value']);
				}
				if($filter['field'] == 'company_id' && !empty($filter['value'])){
					$options['conditions'][] = array('Mission.company_id' => $filter['value']);
				}
				if($filter['field'] == 'type' && !empty($filter['value'])){
					$options['conditions'][] = array('Mission.type' => $filter['value']);
				}
			}
		}

		//filter events by confirmed date or potential dates
		$options['conditions'][] = array(
			'Mission.confirmed_date <>' => null,
			'Mission.confirmed_date >=' => $start_date,
			'Mission.confirmed_date <=' => $end_date
		);
		$options['group'] = 'Mission.id';

		$data = $this->Mission->find('all', $options);

		$missions = array();
		$view = new View($this);
		$html = $view->loadHelper('Html');
		foreach ($data as $k => $mission) {
			$this->Mission->id = $mission['Mission']['id'];
			$missions[$k] = $this->Mission->generateCalendarEvent();
		}
		$this->set('_json', $missions);
	}

}
