<?php

class MomentTasksController extends AppController {

	public function updateWeights(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if(!empty($this->request->data['tasks'])){
				foreach($this->request->data['tasks'] as $weight => $task){
					$this->MomentTask->id = $task;
					$this->MomentTask->saveField('weight', $weight);
				}
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}
}
