<?php

class StockStorageTypesController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'All' => [
				'model' => 'StockStorageType',
				'columns' => [
					'name',
					'dimensions' => null,
					'Actions' => null
				],
				'fields' => array(
					'StockStorageType.id',
					'StockStorageType.width',
					'StockStorageType.height',
					'StockStorageType.depth',
					'StockStorageType.weight',
					'StockStorageType.volume'
				),
				'autoData' => false
			]
		],
	];

	public function index() {
		$this->DataTable->setViewVar(array('All'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['All']['columns']['name']['label'] = __('Name');
	}

	public function add(){

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->StockStorageType->create();
			if ($this->StockStorageType->save($this->request->data)) {
				$this->Session->setFlash(__('Storage type %s has been saved.', $this->request->data['StockStorageType']['name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->StockStorageType->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Storage type has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}

	}

	public function edit( $id = null ){

		$this->StockStorageType->id = $id;

		if (!$this->StockStorageType->exists()) {
			throw new NotFoundException(__('Invalid StockStorageType'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->StockStorageType->create();
			if ($this->StockStorageType->save($this->request->data)) {
				$this->Session->setFlash(__('Storage type %s has been saved.', $this->request->data['StockStorageType']['name']), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->StockStorageType->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Storage type has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$storageType = $this->StockStorageType->findById($id);
			$this->set(compact('storageType'));
			$this->request->data = $storageType;
		}

	}
}
