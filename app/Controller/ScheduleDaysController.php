<?php

class ScheduleDaysController extends AppController {

  public function test1(){
    $this->ScheduleDay->User->id = 1;
    debug($this->ScheduleDay->User->addUnknownAvailabilities());
    exit;
  }

  public function update(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data['ids'])){
        $days = array();
        foreach($this->request->data['ids'] as $id){
          $days[] = array(
            'ScheduleDay' => array(
              'id' => $id,
              'availability' => $this->request->data['availability']
            )
          );
        }
        if($this->ScheduleDay->saveMany($days)){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }
    }
  }

  public function convert(){
    $users = $this->ScheduleDay->User->find('all', array(

    ));
    $this->set(compact('users'));

    if($this->request->is('ajax')){
			$this->autoRender = false;
			$user = $this->ScheduleDay->User->findById($this->request->data['itemId']);
			$this->ScheduleDay->User->id = $this->request->data['itemId'];
      if($this->ScheduleDay->User->updateScheduleDays()){
        if($this->ScheduleDay->User->addUnknownAvailabilities()){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
		}
  }

  public function test(){
    $this->loadModel('User');
    $this->loadModel('ScheduleDay');
    $this->User->contain('Availabilities');
    //$user = $this->User->findById(1);
    $days = array('monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6, 'sunday' => 7);
    $users = $this->User->find('all', array(
      'offset' => 0,
      'limit' => 10
    ));
    foreach($users as $k => $user){
      if(!empty($user['Availabilities'])){
        foreach($user['Availabilities'] as $item){
          $availability = explode('_', $item['value']);
          if($availability[0] == 'on'){
            continue;
          }
          $day = $days[$availability[0]];
          if($availability[1] == 'morning'){
            $hours = range(4,11);
          }
          if($availability[1] == 'afternoon'){
            $hours = range(12,16);
          }
          if($availability[1] == 'morning'){
            $hours = array_merge(range(17,23), range(0,3));
          }
          if(!empty($hours)){
            foreach($hours as $hour){
              $exists = $this->ScheduleDay->findByDayAndHourAndUserId($day, $hour, $user['User']['id']);
              $s = array('ScheduleDay' => array(
                'id' => empty($exists) ? '' : $exists['ScheduleDay']['id'],
                'user_id' => $user['User']['id'],
                'day' => $day,
                'hour' => $hour,
                'availability' => $availability[2]
              ));
              $this->ScheduleDay->create();
              $this->ScheduleDay->save($s);
            }
          }
        }
      }
    }
    exit;
  }

}
