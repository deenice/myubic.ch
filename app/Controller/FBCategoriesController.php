<?php

class FBCategoriesController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'FBCategory' => array(
				'columns' => array(
					'name',
					'Actions' => null,
				),
				'order' => array('FBCategory.name' => 'asc'),
				'fields' => array('FBCategory.id'),
 				'autoData' => false
			)
		)
	);

	public function index() {
		$this->DataTable->setViewVar(array('FBCategory'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['FBCategory']['columns']['name']['label'] = __('Name');
	}


	public function add() {

		if($this->request->is('post') || $this->request->is('put')){
			if($this->FBCategory->save($this->request->data)){
				$this->Session->setFlash(__(sprintf('F&B category %s has been saved.', $this->request->data['FBCategory']['name'])), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->FBCategory->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('F&B category has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'index'));
			}
		}
	}

	public function view( $id = null ) {
		return $this->redirect(array('action' => 'edit', $id));
	}

	public function edit( $id = null ) {

		if($this->request->is('post') || $this->request->is('put')){
			if($this->FBCategory->save($this->request->data)){
				$this->Session->setFlash(__(sprintf('F&B category %s has been saved.', $this->request->data['FBCategory']['name'])), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->FBCategory->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('F&B category has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
				return $this->redirect(array('action' => 'index'));
			}
		} else {
			$fbCategory = $this->FBCategory->findById($id);
			$this->request->data = $fbCategory;
		}
	}

	public function delete($id = null){
		if(!empty($id) && $this->FBCategory->delete($id)){
			$this->Session->setFlash(__(sprintf('F&B category has been deleted.')), 'alert', array('type' => 'success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('F&B category has not been deleted. Please try again.'), 'alert', array('type' => 'danger'));
			return $this->redirect(array('action' => 'index'));
		}
	}

}
