<?php

class CommunesController extends AppController {

	public function add(){
		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Commune->save($this->request->data)){
				$this->Session->setFlash(__('Commune has been added.'), 'alert', array('type' => 'success'));
				return $this->redirect(array('action' => 'index', 'controller' => 'places'));
			}
		}
	}

	public function getLatLng() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$search = $_POST['zip'] . '+' . $_POST['city'];
			$search = str_replace(' ',  '-', $search);
			$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$search.'&sensor=false';
			$ch = curl_init();
			$timeout = 5;
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			$data = curl_exec($ch);
			curl_close($ch);
			$data = json_decode($data);
			$lat = $data->results[0]->geometry->location->lat;
			$lng = $data->results[0]->geometry->location->lng;
			$output = array('latitude' => $lat, 'longitude' => $lng);
			$this->response->body(json_encode($output));
		}
	}


	public function import() {
		/*$handle = fopen(WWW_ROOT . 'files/communes.csv', "r");
		$header = fgetcsv($handle);
		while (($row = fgetcsv($handle)) !== FALSE) {
			$row = explode(';', $row[0]);
			$data = array(
				'Commune' => array(
					'name' => utf8_encode($row[1]),
					'canton' => $row[2],
					'zip' => $row[0]
					));

			$this->Commune->create();
			$this->Commune->save($data);
		}*/
		$key = 'AIzaSyCS9SNCkw3DbNWEbOvD2ZunXi_qxvtOE0s';
		$communes = $this->Commune->find('all', array(
			'limit' => 300,
			'offset' => 3100
		));
		foreach($communes as $c){
			$search = $c['Commune']['zip'] . '+' . $c['Commune']['name'];
			$search = str_replace(' ',  '-', $search);
			$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$search.'&sensor=false&key=' . $key;
			$json = @file_get_contents($url);
			$data=json_decode($json);
			$lat = $data->results[0]->geometry->location->lat;
			$lng = $data->results[0]->geometry->location->lng;
			$this->Commune->id = $c['Commune']['id'];
			$this->Commune->saveField('latitude', $lat);
			$this->Commune->saveField('longitude', $lng);
		}
		exit;
	}

	public function get() {
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			$output = $this->Commune->find('all', array(
				'conditions' => array(
					'OR' => array(
						'name LIKE' => '%'.$this->request->data['term'].'%',
						'zip LIKE' => '%'.$this->request->data['term'].'%'
					)
				),
				//'group' => array('name'),
				'order' => array('name')
			));
			$this->response->body(json_encode($output));
		}
	}


}
