<?php
App::uses('HttpSocket', 'Network/Http');

class EventsController extends AppController {

  public $helpers = array('Js');

  public $components = array(
    'Paginator'
  );

  public function beforeFilter() {
    parent::beforeFilter();

    if($this->request->is('post') || $this->request->is('put')){
      if(!empty($this->request->data['FilterOffer'])){
        foreach($this->request->data['FilterOffer'] as $filter => $value){
          if(!empty($value) && is_array($value)){
            $this->request->params['named']['FilterOffer'][$filter] = implode(',', $value);
          } elseif(!empty($value)) {
            $this->request->params['named']['FilterOffer'][$filter] = $value;
          } else {
            unset($this->request->params['named']['FilterOffer'][$filter]);
          }
        }
      }
      if(!empty($this->request->data['FilterConfirmed'])){
        foreach($this->request->data['FilterConfirmed'] as $filter => $value){
          if(!empty($value) && is_array($value)){
            $this->request->params['named']['FilterConfirmed'][$filter] = implode(',', $value);
          } elseif(!empty($value)) {
            $this->request->params['named']['FilterConfirmed'][$filter] = $value;
          } else {
            unset($this->request->params['named']['FilterConfirmed'][$filter]);
          }
        }
      }
    }
  }

  public function index( $tab = '', $slug = null ) {

    $companyIds = array();

    if(!empty($slug)){
      $slug1 = explode('-', $slug);
      $company = $this->Event->Company->find('all', array(
        'conditions' => array(
          'class' => $slug1
        )
      ));
      if($company){
        foreach($company as $c){
          $companyIds[] = $c['Company']['id'];
        }
      }
    }
    $this->set(compact('slug'));
    $managers = $this->Event->User->find('list', array(
      'joins' => array(
        array(
          'table' => 'events',
          'alias' => 'Event',
          'conditions' => array('Event.resp_id = User.id')
        )
      ),
      'conditions' => array(
        'role' => 'fixed',
        'Event.resp_id <>' => null
      ),
      'fields' => array('User.id', 'User.first_name'),
      'order' => array('User.first_name')
    ));
    $managers['everyone'] = __('Everyone');
    $this->set(compact('managers'));

    $languages = array(
      'fr' => '<span class="flag-icon flag-icon-fr"></span>',
      'de' => '<span class="flag-icon flag-icon-de"></span>',
      'en' => '<span class="flag-icon flag-icon-gb"></span>'
    );
    $this->set(compact('languages'));

    $eventLanguages = array(
      'fr' => '<span class="flag-icon flag-icon-fr"></span>',
      'de' => '<span class="flag-icon flag-icon-de"></span>',
      'en' => '<span class="flag-icon flag-icon-gb"></span>'
    );
    $this->set(compact('eventLanguages'));

    $crmStatuses = Configure::read('Events.crm_status');
    $this->set(compact('crmStatuses'));

    $categories = $this->Event->EventCategory->find('list', array(
      'fields' => array('EventCategory.id', 'EventCategory.name'),
    ));
    $this->set(compact('categories'));

    $companies = $this->Event->Company->find('list', array(
      'fields' => array('Company.id', 'Company.name'),
    ));
    $this->set(compact('companies'));

    $this->set(compact('companyIds'));

    $needsAnalysis = Configure::read('Events.needs_analysis');
    $this->set(compact('needsAnalysis'));
    $this->set(compact('tab'));
    switch($tab){
      case 'offers':
        if(!empty($companyIds)){
          $conditions[] = array('Event.company_id' => $companyIds);
        } else {
          $conditions[] = array('Event.company_id' => array(2,6));
        }
        if(empty($this->request->params['named']['FilterOffer'])){
          // we will set default crm statuses to filter all events
          $defaultFilters = Configure::read('Events.crm_status_default');
          $this->request->params['named']['FilterOffer']['crm_status'] = implode(',', array_keys($defaultFilters));
          //$this->request->params['named']['FilterOffer']['resp_id'] = AuthComponent::user('id');
          $this->request->params['named']['FilterOffer']['resp_id'] = 'everyone';
        }
        $offerSetStatuses = array();
        if(!empty($this->request->params['named']['FilterOffer'])){
          foreach($this->request->params['named']['FilterOffer'] as $filter => $value){
            if(empty($value)) continue;
            $value = explode(',', $value);
            if($filter == 'resp_id'){
              if(!in_array('everyone', $value)){
                $conditions[] = array('Event.resp_id' => $value);
              }
              $this->request->params['named']['FilterOffer']['resp_id'] = $value;
            }
            if($filter == 'crm_status'){
              $conditions[] = array('Event.crm_status' => $value);
              $offerSetStatuses = $value;
              $this->request->params['named']['FilterOffer']['crm_status'] = $value;
            }
            if($filter == 'language'){
              $conditions[] = array('Event.language' => $value);
              $this->request->params['named']['FilterOffer']['language'] = $value;
            }
            if($filter == 'min_turnover'){
              $conditions[] = array('Event.projected_turnover >=' => $value[0]);
              $this->request->params['named']['FilterOffer']['min_turnover'] = $value;
            }
            if($filter == 'max_turnover'){
              $conditions[] = array('Event.projected_turnover <=' => $value[0]);
              $this->request->params['named']['FilterOffer']['max_turnover'] = $value;
            }
            if($filter == 'min_number_of_persons'){
              $conditions[] = array(
                'OR' => array(
                  array('Event.min_number_of_persons >=' => $value[0]),
                  array('Event.max_number_of_persons >=' => $value[0])
                ),
                'Event.min_number_of_persons <>' => null,
                'Event.max_number_of_persons <>' => null
              );
              $this->request->params['named']['FilterOffer']['min_number_of_persons'] = $value;
            }
            if($filter == 'max_number_of_persons'){
              $conditions[] = array(
                'OR' => array(
                  array('Event.min_number_of_persons <=' => $value[0]),
                  array('Event.max_number_of_persons <=' => $value[0])
                ),
                'Event.min_number_of_persons <>' => null,
                'Event.max_number_of_persons <>' => null
              );
              $this->request->params['named']['FilterOffer']['max_number_of_persons'] = $value;
            }
          }
        }

        $this->Paginator->settings = array(
          'contain' => array(
            'Client' => array('fields' => array('Client.id', 'Client.name')),
            'PersonInCharge' => array('fields' => array('PersonInCharge.first_name')),
            'Company' => array('fields' => array('Company.class')),
            'Date',
            'Note'
          ),
          'conditions' => $conditions,
          //'fields' => array('Event.*', 'Company.class', 'PersonInCharge.first_name', 'Client.id', 'Client.name'),
          'group' => 'Event.id',
          'order' => 'Event.opening_date',
          'limit' => 9999
        );
        $offers = $this->Paginator->paginate('Event');
        $this->set(compact('offers'));
        $this->set(compact('offerSetStatuses'));
      break;
      case 'confirmed':
        $conditions = array(
          'Event.crm_status' => 'confirmed',
          'confirmed_date >=' => date('Y-m-d')
        );
        if(!empty($companyIds)){
          $conditions[] = array('Event.company_id' => $companyIds);
        } else {
          $conditions[] = array('Event.company_id' => array(2,6));
        }
        if(empty($this->request->params['named']['FilterConfirmed'])){
          //$this->request->params['named']['FilterConfirmed']['resp_id'] = AuthComponent::user('id');
          $this->request->params['named']['FilterConfirmed']['resp_id'] = 'everyone';
        }
        if(!empty($this->request->params['named']['FilterConfirmed'])){
          foreach($this->request->params['named']['FilterConfirmed'] as $filter => $value){
            if(empty($value)) continue;
            $value = explode(',', $value);
            if($filter == 'client_id'){
              $conditions[] = array('Event.client_id' => $value);
              $this->request->params['named']['FilterConfirmed']['client_id'] = $value;
            }
            if($filter == 'place_id'){
              $conditions[] = array('m.place_id' => $value);
              $this->request->params['named']['FilterConfirmed']['place_id'] = $value;
            }
            if($filter == 'resp_id'){
              if(!in_array('everyone', $value)){
                $conditions[] = array('Event.resp_id' => $value);
              }
              $this->request->params['named']['FilterConfirmed']['resp_id'] = $value;
            }
            if($filter == 'min_number_of_persons'){
              $conditions[] = array(
                'OR' => array(
                  array('Event.min_number_of_persons >=' => $value[0]),
                  array('Event.max_number_of_persons >=' => $value[0])
                ),
                'Event.min_number_of_persons <>' => null,
                'Event.max_number_of_persons <>' => null
              );
              $this->request->params['named']['FilterConfirmed']['min_number_of_persons'] = $value;
            }
            if($filter == 'max_number_of_persons'){
              $conditions[] = array(
                'OR' => array(
                  array('Event.min_number_of_persons <=' => $value[0]),
                  array('Event.max_number_of_persons <=' => $value[0])
                ),
                'Event.min_number_of_persons <>' => null,
                'Event.max_number_of_persons <>' => null
              );
              $this->request->params['named']['FilterConfirmed']['max_number_of_persons'] = $value;
            }
            if($filter == 'start_date'){
              $date = date('Y-m-d', strtotime($value[0]));
              $conditions[] = array('Event.confirmed_date >=' => $date);
              $this->request->params['named']['FilterConfirmed']['start_date'] = $value;
            }
            if($filter == 'end_date'){
              $date = date('Y-m-d', strtotime($value[0]));
              $conditions[] = array('Event.confirmed_date <=' => $date);
              $this->request->params['named']['FilterConfirmed']['end_date'] = $value;
            }
          }
        }

        $this->Paginator->settings = array(
          'joins' => array(
            array(
              'table' => 'moments',
              'alias' => 'm',
              'type' => 'left',
              'conditions' => array('m.event_id = Event.id')
            ),
            array(
              'table' => 'users',
              'alias' => 'p',
              'conditions' => array('Event.resp_id = p.id')
            )
          ),
          'contain' => array(
            'Client' => array('fields' => array('Client.id', 'Client.name')),
            'PersonInCharge' => array('fields' => 'PersonInCharge.first_name'),
            'Company',
            'Note',
            'SelectedActivity' => array('Activity' => array('fields' => array('Activity.id', 'Activity.slug', 'Activity.name'))),
            'SuggestedActivity' => array('Activity' => array('fields' => array('Activity.id', 'Activity.slug', 'Activity.name'))),
            'SelectedFBModule' => array('FBModule' => array('fields' => array('FBModule.id', 'FBModule.slug', 'FBModule.name'))),
            'SuggestedFBModule' => array('FBModule' => array('fields' => array('FBModule.id', 'FBModule.slug', 'FBModule.name'))),
            'ConfirmedEventPlace' => array('Place' => array('fields' => array('Place.name', 'Place.zip_city')))
          ),
          'conditions' => $conditions,
          'group' => 'Event.id',
          'order' => 'Event.confirmed_date, p.first_name',
          'limit' => 9999
        );
        $confirmed = $this->Paginator->paginate('Event');

        foreach($confirmed as $k => $event){
          $this->Event->id = $event['Event']['id'];
          $confirmed[$k]['Event']['number_of_jobs'] = $this->Event->getNumberOfJobs();
          $confirmed[$k]['Event']['number_of_filled_jobs'] = $this->Event->getNumberOfJobs('filled');
        }
        $this->set(compact('confirmed'));
      break;
      case 'past':
        if(!empty($this->request->params['named']['year'])){
          $year = $this->request->params['named']['year'];
        } else {
          $year = date('Y');
        }
        $this->set(compact('year'));
        $conditions = array(
          // 'confirmed_date <' => date(sprintf('%s-m-d', $year)),
          // 'confirmed_date >=' => date(sprintf('%s-01-01', $year)),
          'OR' => array(
            array(
              array('Event.crm_status' => array('confirmed')),
              array('Event.type' => 'standard'),
              array('Event.code <>' => null),
            ),
            array(
              array('Event.type' => 'mission')
            )
          )
        );
        if($year == date('Y')){
          $conditions[] = array('confirmed_date <' => date(sprintf('%s-m-d', $year)));
          $conditions[] = array('confirmed_date >=' => date(sprintf('%s-01-01', $year)));
        } else {
          $conditions[] = array('confirmed_date <' => date(sprintf('%s-12-31', $year)));
          $conditions[] = array('confirmed_date >=' => date(sprintf('%s-01-01', $year)));
        }

        if(!empty($companyIds)){
          $conditions[] = array('Event.company_id' => $companyIds);
        } else {
          $conditions[] = array('Event.company_id' => array(2,6));
        }

        $this->Paginator->settings = array(
          'joins' => array(
            array(
              'table' => 'users',
              'alias' => 'p',
              'conditions' => array('Event.resp_id = p.id')
            )
          ),
          'contain' => array(
            'Client' => array('fields' => array('Client.id', 'Client.name')),
            'PersonInCharge' => array('fields' => array('PersonInCharge.first_name')),
            'Company' => array('fields' => array('Company.class')),
            'Note'
          ),
          'conditions' => $conditions,
          'group' => 'Event.id',
          'order' => 'Event.confirmed_date DESC, p.first_name',
          'limit' => 9999
        );
        $past = $this->Paginator->paginate('Event');
        foreach($past as $k => $event){
          $this->Event->id = $event['Event']['id'];
          $numberOfJobs = $this->Event->getNumberOfJobs();
          if($event['Event']['type'] == 'standard' || ($event['Event']['type'] == 'mission' && $numberOfJobs)){
            $past[$k]['Event']['number_of_jobs'] = $numberOfJobs;
            $past[$k]['Event']['number_of_filled_jobs'] = $this->Event->getNumberOfJobs('filled');
            $past[$k]['Event']['number_of_validated_jobs'] = $this->Event->getNumberOfJobs('validated');
            $past[$k]['Event']['accounting_match'] = $this->Event->accountingMatch();
            $past[$k]['Event']['checklists_complete'] = $this->Event->areChecklistsComplete();
          } else {
            unset($past[$k]);
          }
        }
        $this->set(compact('past'));
      break;
      case 'missions':
        if(!empty($this->request->params['named']['year'])){
          $year = $this->request->params['named']['year'];
        } else {
          $year = date('Y');
        }
        $this->set(compact('year'));
        $conditions = array(
          // 'confirmed_date <' => date('Y-m-d'),
          // 'confirmed_date >=' => date('Y-01-01'),
          'Event.type' => 'mission'
        );
        if($year == date('Y')){
          $conditions[] = array('confirmed_date <' => date(sprintf('%s-m-d', $year)));
          $conditions[] = array('confirmed_date >=' => date(sprintf('%s-01-01', $year)));
        } else {
          $conditions[] = array('confirmed_date <' => date(sprintf('%s-12-31', $year)));
          $conditions[] = array('confirmed_date >=' => date(sprintf('%s-01-01', $year)));
        }

        if(!empty($companyIds)){
          $conditions[] = array('Event.company_id' => $companyIds);
        }

        $this->Paginator->settings = array(
          'joins' => array(
            array(
              'table' => 'users',
              'alias' => 'p',
              'conditions' => array('Event.resp_id = p.id')
            )
          ),
          'contain' => array(
            'Client' => array('fields' => array('Client.id', 'Client.name')),
            'PersonInCharge' => array('fields' => array('PersonInCharge.first_name')),
            'Company' => array('fields' => array('Company.class')),
            'Note'
          ),
          'conditions' => $conditions,
          'group' => 'Event.id',
          'order' => 'Event.confirmed_date DESC, p.first_name',
          'limit' => 9999
        );
        $missions = $this->Paginator->paginate('Event');
        $this->set(compact('missions'));
      break;
    }

  }

  public function ajaxtest(){
    $this->paginate = array(
      'limit' => 20, // la tu met le nombre d'élements par page
      'order' => array('id' => 'asc'), // ici tu met à la place de champ le nom du "champ" surlequel tu veut faire un tri (ASC ou DESC)
    );
    $this->set('data', $this->paginate());
  }

  public function add( ) {
    // $clients = $this->Event->Client->find('list', array(
    //   'fields' => array('Client.id', 'Client.name_address'),
    // ));
    // $this->set(compact('clients'));

    $companies = $this->Event->Company->find('list');
    $this->set(compact('companies'));

    $regions1 = $this->Event->Region->find('list', array(
      'order' => 'name',
      'fields' => array('Region.id', 'Region.name', 'Region.type')
    ));
    $regionTypes = Configure::read('Regions.types');
    $regions = array();
    foreach($regions1 as $type => $region){
      if(!isset($regionTypes[$type])){ continue; }
      $regions[$regionTypes[$type]][] = $region;
    }
    $this->set(compact('regions'));

    $categories = $this->Event->EventCategory->find('list');
    $this->set(compact('categories'));
    if(!empty($this->request->params['named']['client_id'])){
      $this->Event->Client->contain('ContactPeople', 'Company');
      $client = $this->Event->Client->findById($this->request->params['named']['client_id']);
      $this->set(compact('client'));
      $this->set('contactPeoples', $this->Event->Client->ContactPeople->find('list', array(
        'conditions' => array(
          'client_id' => $this->request->params['named']['client_id']
        ),
        'order' => 'ContactPeople.created DESC',
        'fields' => array('ContactPeople.id', 'ContactPeople.full_name_phone_email')
      )));
    }

    $resps = $this->Event->User->find('list', array(
      'conditions' => array(
      'User.role' => 'fixed',
      ),
      'fields' => array(
      'User.id', 'User.full_name',
      ),
      'order' => 'User.full_name',
    ));
    $this->set(compact('resps'));

    if ($this->request->is('post') || $this->request->is('put')) {

      if (!empty($this->request->data['Event']['new_client'])) {
        $client = array(
          'Client' => array(
          'name' => $this->request->data['Event']['new_client'],
          'company_id' => $this->request->data['Event']['company_id'],
          ),
        );
        $this->Event->Client->save($client);
        $this->request->data['Event']['client_id'] = $this->Event->Client->id;
      }

      if(!empty($this->request->data['Region'])){
        $regionData = $this->request->data['Region'];
        unset($this->request->data['Region']);
      }

      if (!empty($this->request->data['Date'])) {
        foreach ($this->request->data['Date'] as $k => $date) {
          if (empty($date['date'])) {
            unset($this->request->data['Date'][$k]);
          } else {
            $this->request->data['Date'][$k]['date'] = date('Y-m-d', strtotime($date['date']));
          }
        }
      }
      if (!empty($this->request->data['Event']['confirmed_date'])) {
        $this->request->data['Event']['confirmed_date'] = date('Y-m-d', strtotime($this->request->data['Event']['confirmed_date']));
      }
      if (!empty($this->request->data['Event']['period_start'])) {
        $this->request->data['Event']['period_start'] = date('Y-m-d', strtotime($this->request->data['Event']['period_start']));
      }
      if (!empty($this->request->data['Event']['period_end'])) {
        $this->request->data['Event']['period_end'] = date('Y-m-d', strtotime($this->request->data['Event']['period_end']));
      }
      if (!empty($this->request->data['Event']['relaunch_date'])) {
        $this->request->data['Event']['relaunch_date'] = date('Y-m-d', strtotime($this->request->data['Event']['relaunch_date']));
      }
      if (!empty($this->request->data['Event']['opening_date'])) {
        $this->request->data['Event']['opening_date'] = date('Y-m-d', strtotime($this->request->data['Event']['opening_date']));
      }
      if (!empty($this->request->data['Event']['offer_sending_deadline'])) {
        $this->request->data['Event']['offer_sending_deadline'] = date('Y-m-d', strtotime($this->request->data['Event']['offer_sending_deadline']));
      }
      if($this->request->data['Event']['start_hour'] == '0:00'){
        $this->request->data['Event']['start_hour'] = null;
      }
      if($this->request->data['Event']['end_hour'] == '0:00'){
        $this->request->data['Event']['end_hour'] = null;
      }

      if (!empty($this->request->data['Event']['languages'])) {
        $this->request->data['Event']['languages'] = implode(',', $this->request->data['Event']['languages']);
      }

      if (!empty($this->request->data['Event']['confirmed_date'])) {
        $year = date('y', strtotime($this->request->data['Event']['confirmed_date']));
      } else {
        $year = date('y');
      }

      if(!empty($this->request->data['Event']['company_id']) && empty($this->request->data['Event']['code'])){
        $this->request->data['Event']['code'] = $this->Event->getCode($this->request->data);
      }

      $this->Event->create();
      if ($this->Event->saveAssociated($this->request->data)) {
        $this->Session->setFlash(__('Event has been saved.'), 'alert', array('type' => 'success'));
        if(!empty($regionData)){
          foreach($regionData as $region){
            if(empty($region['id'])){continue;}
            $eventRegion = array(
              'EventRegion' => array(
                'event_id' => $this->Event->id,
                'region_id' => $region['id'][0]
              )
            );
            $this->Event->EventRegion->save($eventRegion);
          }
        }
        if ($this->request->data['destination'] == 'edit') {
          return $this->redirect(array('action' => 'edit', $this->Event->id));
        } elseif ($this->request->data['destination'] == 'work') {
          return $this->redirect(array('action' => 'work', $this->Event->id));
        } else {
          $company = $this->Event->Company->findById($this->request->data['Event']['company_id']);
          $status = 'confirmed';
          if(in_array($this->request->data['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default')))){
            $status = 'offers';
          }
          return $this->redirect(array('action' => 'index', $status, $company['Company']['class']));
        }
      } else {
        $this->Session->setFlash(__('Event has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
      }
    }
  }

  public function edit($id = null) {
    $this->Event->contain(
      array(
        'Company',
        'Client',
        'Date',
        'Job',
        'Moment' => array('Option' => array('Place', 'User')),
      )
    );

    if (!$id) {
      throw new NotFoundException(__('Invalid event'));
    }
    $event = $this->Event->find('first', array(
      'conditions' => array(
        'Event.id' => $id,
      ),
    ));

    $clients = $this->Event->Client->find('list', array(
      'joins' => array(
        array(
          'table' => 'clients_companies',
          'alias' => 'cc',
          'conditions' => array(
            'cc.client_id = Client.id'
          )
        )
      ),
      'conditions' => array(
        'cc.company_id' => $event['Event']['company_id']
      ),
      'fields' => array('Client.id', 'Client.name_address'),
    ));
    $this->set(compact('clients'));

    $companies = $this->Event->Company->find('list');
    $this->set(compact('companies'));

    $categories = $this->Event->EventCategory->find('list');
    $this->set(compact('categories'));

    $resps = $this->Event->User->find('list', array(
      'conditions' => array(
        'User.role' => 'fixed',
      ),
      'fields' => array(
        'User.id', 'User.full_name',
      ),
      'order' => 'User.full_name',
    ));
    $this->set(compact('resps'));

    $contactPeoples = $this->Event->ContactPeople->find('list', array(
      'conditions' => array(
        'ContactPeople.client_id' => $event['Client']['id'],
      ),
      'fields' => array('ContactPeople.id', 'ContactPeople.full_name_phone_email'),
    ));
    $this->set(compact('contactPeoples'));

    $places = $this->Event->Moment->Place->find('list', array(
      'fields' => array('Place.id', 'Place.name'),
    ));
    $this->set(compact('places'));

    $possibleWorkers = $this->Event->User->find('list', array(
      'contain' => array(
        'Job',
      ),
      'conditions' => array(

      ),
      'order' => 'User.full_name',
      'fields' => array(
        'User.id',
        'User.full_name',
      ),
    ));
    $this->set(compact('possibleWorkers'));

    $regions1 = $this->Event->Region->find('list', array(
      'order' => 'name',
      'fields' => array('Region.id', 'Region.name', 'Region.type')
    ));
    $regionTypes = Configure::read('Regions.types');
    $regions = array();
    foreach($regions1 as $type => $region){
      if(!isset($regionTypes[$type])){ continue; }
      $regions[$regionTypes[$type]][] = $region;
    }
    $this->set(compact('regions'));

    $eventRegions = $this->Event->EventRegion->find('list', array(
      'conditions' => array(
        'event_id' => $id
      ),
      'contain' => array('Region'),
      'fields' => array('Region.id')
    ));
    $this->set(compact('eventRegions'));

    $this->loadModel('Activity');
    $ubicActivities = $this->Activity->find('list', array('conditions' => array('company_id' => 2)));
    $ugActivities = $this->Activity->find('list', array('conditions' => array('company_id' => 6)));
    $this->set('activities', array('ubic' => $ubicActivities, 'ug' => $ugActivities, 'all' => $ubicActivities + $ugActivities));

    if (!$event) {
      throw new NotFoundException(__('Invalid event'));
    }

    if ($this->request->is('post') || $this->request->is('put')) {
      //debug($this->request->data);exit;
      $this->Event->id = $id;
      if (!empty($this->request->data['Date'])) {
        foreach ($this->request->data['Date'] as $k => $date) {
          $this->Event->Date->delete($date['id']);
          if (empty($date['date'])) {
            unset($this->request->data['Date'][$k]);
          } else {
            $this->request->data['Date'][$k]['date'] = date('Y-m-d', strtotime($date['date']));
          }
        }
      }
      if(!empty($this->request->data['Region'])){
        $this->Event->EventRegion->deleteAll(array(
          'event_id' => $id
        ));
        $this->request->data['EventRegion'] = array();
        foreach($this->request->data['Region']['id'] as $region){
          if(empty($region)){continue;}
          $this->request->data['EventRegion'][] = array(
            'event_id' => $id,
            'region_id' => $region[0]
          );
        }
        unset($this->request->data['Region']);
      }
      if (!empty($this->request->data['Event']['confirmed_date'])) {
        $this->request->data['Event']['confirmed_date'] = date('Y-m-d', strtotime($this->request->data['Event']['confirmed_date']));
      }
      if (!empty($this->request->data['Event']['period_start'])) {
        $this->request->data['Event']['period_start'] = date('Y-m-d', strtotime($this->request->data['Event']['period_start']));
      }
      if (!empty($this->request->data['Event']['period_end'])) {
        $this->request->data['Event']['period_end'] = date('Y-m-d', strtotime($this->request->data['Event']['period_end']));
      }
      if (!empty($this->request->data['Event']['relaunch_date'])) {
        $this->request->data['Event']['relaunch_date'] = date('Y-m-d', strtotime($this->request->data['Event']['relaunch_date']));
      }
      if (!empty($this->request->data['Event']['opening_date'])) {
        $this->request->data['Event']['opening_date'] = date('Y-m-d', strtotime($this->request->data['Event']['opening_date']));
      }
      if (!empty($this->request->data['Event']['offer_sending_deadline'])) {
        $this->request->data['Event']['offer_sending_deadline'] = date('Y-m-d', strtotime($this->request->data['Event']['offer_sending_deadline']));
      }
      if($this->request->data['Event']['start_hour'] == '0:00'){
        $this->request->data['Event']['start_hour'] = null;
      }
      if($this->request->data['Event']['end_hour'] == '0:00'){
        $this->request->data['Event']['end_hour'] = null;
      }

      if (!empty($this->request->data['Event']['languages'])) {
        $this->request->data['Event']['languages'] = implode(',', $this->request->data['Event']['languages']);
      }
      if (!empty($this->request->data['Event']['place_id'])) {
        $this->loadModel('Option');
        $this->Option->update(
          'confirmed',
          $this->request->data['Event']['id'],
          $this->request->data['Event']['place_id'],
          $this->request->data['Event']['confirmed_date'],
          AuthComponent::user('id')
        );
        if ($this->request->data['Event']['status'] == 'not_confirmed') {
          $this->request->data['Event']['status'] = 'confirmed';
        }
      }
      if ($this->Event->saveAssociated($this->request->data)) {
        $this->Session->setFlash(__('Event has been saved.'), 'alert', array('type' => 'success'));
        if ($this->request->data['destination'] == 'edit') {
          return $this->redirect(array('action' => 'edit', $this->Event->id));
        }
        elseif ($this->request->data['destination'] == 'work') {
          return $this->redirect(array('action' => 'work', $this->Event->id));
        } else {
          $company = $this->Event->Company->findById($this->request->data['Event']['company_id']);
          $status = 'confirmed';
          if(in_array($this->request->data['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default')))){
            $status = 'offers';
          }
          return $this->redirect(array('action' => 'index', $status, $company['Company']['class']));
        }
      } else {
        $this->Session->setFlash(__('Event has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
      }
    }
    if (!$this->request->data) {
      if (!empty($event['Event']['confirmed_date'])) {
        $event['Event']['confirmed_date'] = date('d-m-Y', strtotime($event['Event']['confirmed_date']));
      }
      if (!empty($event['Event']['period_start'])) {
        $event['Event']['period_start'] = date('d-m-Y', strtotime($event['Event']['period_start']));
      }
      if (!empty($event['Event']['period_end'])) {
        $event['Event']['period_end'] = date('d-m-Y', strtotime($event['Event']['period_end']));
      }
      if (!empty($event['Event']['relaunch_date'])) {
        $event['Event']['relaunch_date'] = date('d-m-Y', strtotime($event['Event']['relaunch_date']));
      }
      if (!empty($event['Event']['opening_date'])) {
        $event['Event']['opening_date'] = date('d-m-Y', strtotime($event['Event']['opening_date']));
      }
      if (!empty($event['Event']['offer_sending_deadline'])) {
        $event['Event']['offer_sending_deadline'] = date('d-m-Y', strtotime($event['Event']['offer_sending_deadline']));
      }
      if (!empty($event['Date'])) {
        foreach ($event['Date'] as $k => $date) {
          $event['Date'][$k]['date'] = date('d-m-Y', strtotime($date['date']));
        }
      }
      $this->request->data = $event;
    }
  }

  public function work($id = null) {

    if (!$id) {
      throw new NotFoundException(__('Invalid event'));
    }

    $activities = $this->Event->Activity->find('list', array(
      'conditions' => array(
        'Activity.place_id' => null,
      ),
      'contain' => array('Company'),
      'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
    ));
    $this->set(compact('activities'));

    $fb_modules = $this->Event->FBModule->find('list', array(
      'contain' => array('FBCategory'),
      'fields' => array('FBModule.id', 'FBModule.name')
    ));
    $this->set(compact('fb_modules'));

    // for modal job edition
    $fbmodules = $this->Event->FBModule->find('list', array(
      'conditions' => array('FBModule.with_competences' => 1),
      'fields' => array('FBModule.id', 'FBModule.name')
    ));
    $this->set(compact('fbmodules'));

    $this->Event->contain(array(
      'Client',
      'PersonInCharge',
      'EventCategory',
      'Date',
      'Company',
      'Job' => array(
        'User'
      )
    ));
    $event = $this->Event->findById($id);
    $this->set(compact('event'));

    $jobs = array();
    if (!empty($event['Job'])) {
      foreach ($event['Job'] as $k => $job) {
        $job['user_ids'] = array();
        if(!empty($job['User'])){
          $userIds = array();
          foreach($job['User'] as $user){
            $userIds[] = $user['id'];
          }
          $job['user_ids'] = $userIds;
        }
        $jobs[$job['sector']][$k] = $job;
      }
    }
    ksort($jobs);
    $this->set(compact('jobs'));

    $users = $this->Event->Job->User->find('list', array(
      'conditions' => array(
        'User.role' => array('fixed', 'temporary')
      ),
      'order' => 'User.first_name',
      'fields' => array('User.id', 'User.full_name')
    ));
    $this->set(compact('users'));

    $this->loadModel('VehicleCompanyModel');
    $vehicle_models = array();
    $vm = $this->VehicleCompanyModel->find('all', array(
      'contain' => array(
        'VehicleCompany'
      ),
      'order' => 'VehicleCompany.id'
    ));
    if(!empty($vm)){
      foreach($vm as $model){
        $vehicle_models[$model['VehicleCompanyModel']['id']] = sprintf('%s - %s', $model['VehicleCompanyModel']['name'], $model['VehicleCompany']['name']);
      }
    }
    $this->set(compact('vehicle_models'));

    $checklists = array();
    $data = $this->Event->Checklist->find('all', array(
      'contain' => array('Company'),
      'conditions' => array(
        'model' => 'event',
        'model_id' => null,
        'OR' => array(
          array('default' => 1),
          array('user_id' => AuthComponent::user('id'), 'personal' => 1)
        )
      ),
      'fields' => array('Checklist.id', 'Checklist.name', 'Checklist.default', 'Company.class'),
      'order' => 'default DESC'
    ));
    if(!empty($data)){
      foreach($data as $checklist){
        if($checklist['Checklist']['default'] == 1){
          $icon = 'fa-star font-yellow';
        } else {
          $icon = 'fa-user';
        }
        $checklists[] = array(
          'value' => $checklist['Checklist']['id'],
          'name' => $checklist['Checklist']['name'],
          'data-content' => sprintf('<i class="fa %s"></i> %s <span class="btn btn-xs btn-company btn-company-%s pull-right"></span>', $icon, $checklist['Checklist']['name'], $checklist['Company']['class'])
        );
      }
    }
    $this->set(compact('checklists'));

    $orders = $this->Event->Order->find('list', array(
      'conditions' => array(
        'model' => 'event',
        'model_id' => null
      ),
      'fields' => array('Order.id', 'Order.name'),
      'order' => array('Order.name')
    ));
    $this->set(compact('orders'));

    $mission = false;
    if($event['Event']['type'] == 'mission'){
      $mission = true;
    }
    $this->set(compact('mission'));

    $page['title'] = $mission ? __('Missions') : __('Events');
    $page['url'] = $mission ? array('controller' => 'missions', 'action' => 'index') : array('controller' => 'events', 'action' => 'index');
    $this->set(compact('page'));

    $this->Event->id = $id;
    $this->Event->initPlanningBoards();
    $this->Event->convertExistingMoments();
    $this->Event->initPlanningBoardMoments(); // create moments with ActivityEvent and EventFBModule
  }

  public function delete($id) {
    if ($this->request->is('get')) {
      throw new MethodNotAllowedException();
    }
    if ($this->Event->delete($id)) {
      $this->Session->setFlash(__('Event has been correctly deleted.'), 'alert', array('type' => 'success'));

      return $this->redirect(array('action' => 'index'));
    }
  }

  public function view($id = null) {
    $this->Event->id = $id;

    if (!$this->Event->exists()) {
      throw new NotFoundException(__('Invalid Event'));
    }
    $event = $this->Event->find('first', array(
      'contain' => array(
      'Date',
      'Client',
      'ContactPeople',
      'Job' => array('User'),
      'Moment' => array('Option', 'Place'),
      'User' => array('Portrait'),
    ),
    'conditions' => array('Event.id' => $id),
  ));
    $this->set(compact('event'));
  }

  public function updateFeedback(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data['pk'])){
        $this->Event->id = $this->request->data['pk'];
        if($this->Event->saveField('feedback', $this->request->data['value'])){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function searchPlace($event_id = null) {
    $this->Event->contain('Search');
    $event = $this->Event->read(null, $event_id);
    if (!empty($event['Search'][0]['id'])) {
      return $this->redirect(array('controller' => 'search', 'action' => 'places', $event['Search'][0]['id']));
    } else {
      $this->Event->Search->create();
      $data = array(
    'Search' => array(
      'model' => 'place',
      'event_id' => $event_id,
    ),
    );
      $this->Event->Search->save($data);

      return $this->redirect(array('controller' => 'search', 'action' => 'places', $this->Event->Search->id, '?' => array('places' => 0)));
    }
  }

  public function searchUsers($event_id = null) {
    $this->Event->contain('Search');
    $event = $this->Event->read(null, $event_id);
    if (!empty($event['Search'][0]['id'])) {
      return $this->redirect(array('controller' => 'search', 'action' => 'users', $event['Search'][0]['id']));
    } else {
      $this->Event->Search->create();
      $data = array(
    'Search' => array(
      'model' => 'place',
      'event_id' => $event_id,
    ),
    );
      $this->Event->Search->save($data);

      return $this->redirect(array('controller' => 'search', 'action' => 'users', $this->Event->Search->id));
    }
  }

  public function getData() {
    $this->Event->contain('Client', 'Date');
    $this->autoRender = false;
    if ($this->request->is('ajax')) {
      $event = $this->Event->findById($_POST['id']);
      if (!empty($event['Event']['confirmed_date'])) {
        App::uses('CakeTime', 'Utility');
        $event['Event']['confirmed_date'] = CakeTime::format($event['Event']['confirmed_date'], '%A %d %B %Y');
      }
      if (!empty($event['Date'])) {
        App::uses('CakeTime', 'Utility');
        foreach ($event['Date'] as $k => $date) {
          $event['Date'][$k]['date'] = CakeTime::format($date['date'], '%d %B %Y');
        }
      }
      $this->response->body(json_encode($event));
    }
  }

  public function import() {
    $this->autoRender = false;
    if (AuthComponent::user('id') == 1) {
      $counter = 1;
      $data = explode('BEGIN:VEVENT', file_get_contents(WWW_ROOT.'files/events.ics'));
      unset($data[0]);

      foreach ($data as $key => $row) {
        $row = explode("\n", trim($row));
        debug(date('Y-m-d', strtotime('20150319T150000Z')));
        exit;
        debug(trim($row[2]));
        exit;
      }
      exit;
      unset($data[0]);

      $clients = array();
      foreach ($data as $row) {
        $row = explode(',', $row);
        $clients[$row[7]] = $row;
      }

      foreach ($clients as $row) {
        ++$counter;
    //$row = explode(',', $row);
    //debug($row);exit;
    $data = array(
      'Client' => array(
      'name' => $row[1],
      'contact_person_name' => $row[4].' '.$row[5],
      'contact_person_phone' => $row[12],
      'contact_person_email' => $row[7],
      'website' => $row[3],
      'address' => $row[8],
      'zip' => $row[9],
      'city' => $row[10],
      'company_id' => 2,
      'civility' => $row[2],
      'formal' => $row[3],
      ),
    );

        $this->Client->create();
        if ($this->Client->save($data)) {
        } else {
          debug($this->Client->invalidFields());
        }
        exit;
      }
    } else {
      return $this->redirect(array('controller' => 'dashboard', 'action' => 'index'));
    }
  }

  public function calendar() {
    if($this->request->is('ajax')){

    } else {
      return $this->redirect(array('controller' => 'events', 'action' => 'index'));
    }

    $this->loadModel('Activity');

    $managers = $this->Event->User->find('list', array(
      'joins' => array(
        array(
          'table' => 'events',
          'alias' => 'Event',
          'conditions' => array('Event.resp_id = User.id')
        )
      ),
      'conditions' => array(
        'role' => 'fixed',
        'Event.resp_id <>' => null
      ),
      'fields' => array('User.id', 'User.full_name'),
      'order' => array('User.first_name')
    ));
    $this->set(compact('managers'));

    $places = $this->Event->Moment->Place->find('list', array(
      'limit' => 10,
      'fields' => array('Place.id', 'Place.name'),
    ));
    $this->set(compact('places'));

    $categories = $this->Event->EventCategory->find('list', array(
      'fields' => array('EventCategory.id', 'EventCategory.name'),
    ));
    $this->set(compact('categories'));

    $companies = $this->Event->Company->find('list', array(
      'fields' => array('Company.id', 'Company.name'),
    ));
    $this->set(compact('companies'));

    $activities = $this->Activity->find('list', array(
      'contain' => array('Company'),
      'conditions' => array(
        'category' => null,
      ),
      'fields' => array('Activity.id', 'Activity.name', 'Company.name'),
    ));
    $this->set(compact('activities'));

    $status = empty($this->request->data['status']) ? '' : $this->request->data['status'];

    $view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
    $ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
    $start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
    $end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
    $start_date = date('Y-m-d', $start);
    $end_date = date('Y-m-d', $end);
    $options = array();

    if ($view == 'month') {
      $year = date('Y', $ts);
      $month = date('m', $ts);
      $days = date('t', $ts);
      $start_date = date(sprintf('%s-%s-01', $year, $month));
      $end_date = date(sprintf('%s-%s-%s', $year, $month, $days));
      $end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
    }
    $options['contain'] = array(
      'Client' => array('fields' => array('name')),
      'Date' => array('fields' => array('date')),
      'EventCategory' => array('fields' => array('slug')),
      'Company' => array('fields' => array('class')),
      'SelectedActivity' => array('Activity' => array('fields' => array('slug'))),
      'SuggestedActivity' => array('Activity' => array('fields' => array('slug'))),
      'SelectedFBModule' => array('FBModule' => array('fields' => array('slug'))),
      'SuggestedFBModule' => array('FBModule' => array('fields' => array('slug'))),
      'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city')))
    );
    $setDefaultStatus = true;
    if(!empty($this->request->data['filters'])){
      foreach($this->request->data['filters'] as $filter){
        if($filter['field'] == 'resp_id' && !empty($filter['value'])){
          $options['conditions'][] = array('Event.resp_id' => $filter['value']);
        }
        if($filter['field'] == 'crm_status' && !empty($filter['value'])){
          $options['conditions'][] = array('Event.crm_status' => $filter['value']);
          $setDefaultStatus = false;
        }
        if($filter['field'] == 'event_category_id' && !empty($filter['value'])){
          $options['conditions'][] = array('Event.event_category_id' => $filter['value']);
        }
        if($filter['field'] == 'company_id' && !empty($filter['value'])){
          $options['conditions'][] = array('Event.company_id' => $filter['value']);
        }
        // if($filter['field'] == 'type' && !empty($filter['value'])){
        //   $options['conditions'][] = array('Event.type' => $filter['value']);
        // }
      }
    }
    if($setDefaultStatus){
      $options['conditions'][] = array('crm_status NOT' => array('refused', 'null', 'partner'));
    }
    $options['conditions'][] = array('Event.type <>' => 'mission');
    $options['joins'] = array(
      array(
        'table' => 'dates',
        'alias' => 'Date1',
        'type' => 'left',
        'conditions' => array('Date1.event_id = Event.id')
      )
    );
    $options['fields'] = array(
      'Event.name',
      'Event.confirmed_date',
      'Event.confirmed_number_of_persons',
      'Event.code_name',
      'Event.crm_status',
      'Event.languages',
      'Event.start_hour',
      'Event.end_hour',
      'Event.company_id',
      'Event.id'
    );

    //filter events by confirmed date or potential dates
    $options['conditions'][] = array(
      'OR' => array(
        array(
          array('Event.confirmed_date <>' => null),
          array('Event.confirmed_date >=' => $start_date),
          array('Event.confirmed_date <=' => $end_date)
        ),
        array(
          'Date1.date >=' => $start_date,
          'Date1.date <=' => $end_date
        )
      )
    );
    $options['group'] = 'Event.id';

    $data = $this->Event->find('all', $options);

    $events = array();
    $view = new View($this);
    $html = $view->loadHelper('Html');
    $index = 0;
    foreach ($data as $k => $event) {
      $this->Event->id = $event['Event']['id'];
      if (!empty($event['Event']['confirmed_date'])) {
        $events[$index++] = $this->Event->generateEvent($event, $event['Event']['confirmed_date']);
      } else {
        if (!empty($event['Date'])) {
          foreach ($event['Date'] as $kk => $date) {
            $events[$index++] = $this->Event->generateEvent($event, $date['date']);
          }
        }
      }
    }
    $this->set('_json', $events);
  }

  public function tracking_sheet( $id = '', $mode = 'standard' ) {
    App::uses('CakePdf', 'CakePdf.Pdf');

    $this->set('knowledge', Configure::read('Events.ubic_knowledge'));
    $this->set('activityTypes', Configure::read('Events.activity_types'));

    if(!empty($id)){
      $event = $this->Event->find('first', array(
        'contain' => array(
          'Client' => array(
            'Event' => array(
              'conditions' => array(
                'Event.id <>' => $id,
                'Event.confirmed_date <=' => date('d.m.Y')
              ),
              'order' => 'confirmed_date DESC',
              'limit' => 3
            )
          ),
          'ContactPeople',
          'PersonInCharge',
          'Date',
          'SuggestedActivity' => array('Activity')
        ),
        'conditions' => array(
          'Event.id' => $id
        )
      ));

      $this->set('eventLanguages', Configure::read('Jobs.languages'));
      $this->set(compact('event'));
    }

    $this->layout = 'default';
    $this->pdfConfig = array(
      'filename' => 'test.pdf',
      'download' => false,
      'orientation' => $mode != 'standard' ? 'landscape' : 'portrait'
    );
    if($mode != 'standard'){
      $this->render(sprintf('tracking_sheet_%s', $mode));
    } else {
      $this->render('tracking_sheet');
    }
  }

  public function portlet($portlet = '', $id = null) {
    if ($this->request->is('ajax')) {
      $contain = array();

      switch($portlet){
        case 'client':
          $contain = array('Client' => array('Logo'), 'ContactPeople');
          break;
        case 'tasks':
          $contain = array('Checklist' => array('ChecklistBatch' => array('ChecklistTask')));
          break;
        case 'infos':
          $contain = array('Date', 'EventCategory', 'PersonInCharge', 'ConfirmedEventPlace' => array('Place'), 'Note');
          break;
        case 'invoicing':
          $contain = array('Client');
          break;
        case 'closing':
          $contain = array('Document');
          break;
        case 'activities':
          $contain = array(
            'Activity' => array('Game')
          );
          break;
        case 'fb_modules':
          $contain = array('FBModule' => array('FBCategory'));
          break;
        case 'places':
          $contain = array('EventPlace' => array('Place' => array('Note')));
          break;
        case 'recruiting':
        case 'hours':
          $contain = array('Job' => array('Event', 'User' => array('Portrait')));
          break;
        case 'planning':
          $contain = array('PlanningBoard' => array('PlanningBoardMoment' => array('PlanningResource')));
          break;
        case 'orders':
          $contain = array('Order' => array('OrderGroup' => array('OrderItem' => array('StockItem' => array('fields' => array('code_name', 'id', 'quantity'))))));
          break;
        default:
          $contain = array();
          break;
      }
      $this->Event->contain($contain);
      $event = $this->Event->findById($id);

      $this->set(compact('event'));

      $this->set('countries', Configure::read('Countries'));

      $portletNotes = $this->Event->Note->find('count', array(
        'conditions' => array(
          'model' => 'Event',
          'model_id' => $id,
          'category' => $portlet
        )
      ));
      $this->set(compact('portletNotes'));

      $mission = false;
      if($event['Event']['type'] == 'mission'){
        $mission = true;
      }
      $this->set(compact('mission'));

      switch ($portlet) {
        case 'client':
        $this->render('portlet/client');
        break;
        case 'infos':
        $this->render('portlet/infos');
        break;
        case 'vehicles':
        $this->set('families', Configure::read('VehiclesTrailers.families'));
        $openReservations = $this->Event->VehicleReservation->find('all', array(
          'joins' => array(
            array(
              'table' => 'events_vehicle_reservations',
              'alias' => 'evr',
              'conditions' => array(
                'evr.vehicle_reservation_id = VehicleReservation.id'
              )
            )
          ),
          'conditions' => array(
            'evr.event_id' => $id,
            'confirmed' => 0,
            'cancelled' => 0,
            'archived' => 0
          )
        ));
        $this->set(compact('openReservations'));
        $confirmedReservations = $this->Event->VehicleReservation->find('all', array(
          'contain' => array(
            'Vehicle' => array('VehicleCompanyModel' => array('VehicleCompany'))
          ),
          'joins' => array(
            array(
              'table' => 'events_vehicle_reservations',
              'alias' => 'evr',
              'conditions' => array(
                'evr.vehicle_reservation_id = VehicleReservation.id'
              )
            )
          ),
          'conditions' => array(
            'evr.event_id' => $id,
            'confirmed' => 1,
            'cancelled' => 0,
            'archived' => 0
          )
        ));
        $this->set(compact('confirmedReservations'));
        $cancelledReservations = $this->Event->VehicleReservation->find('all', array(
          'contain' => array(
            'Vehicle' => array('VehicleCompanyModel' => array('VehicleCompany'))
          ),
          'joins' => array(
            array(
              'table' => 'events_vehicle_reservations',
              'alias' => 'evr',
              'conditions' => array(
                'evr.vehicle_reservation_id = VehicleReservation.id'
              )
            )
          ),
          'conditions' => array(
            'evr.event_id' => $id,
            'confirmed' => 1,
            'cancelled' => 1
          )
        ));
        $this->set(compact('cancelledReservations'));
        // we need to find all drivers of jobs in current event
        $drivers = $this->Event->Job->JobUser->User->find('list', array(
          'joins' => array(
            array(
              'table' => 'jobs_users',
              'alias' => 'ju',
              'conditions' => array('ju.user_id = User.id')
            ),
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array('j.id = ju.job_id')
            )
          ),
          'conditions' => array(
            'j.event_id' => $id
          ),
          'fields' => array('User.id', 'User.full_name')
        ));
        $this->set(compact('drivers'));
        $this->set('locations', Configure::read('VehicleTours.locations'));
        $this->render('portlet/vehicles');
        break;
        case 'plus':
        $this->render('portlet/plus');
        break;
        case 'closing':
        $this->render('portlet/closing');
        break;
        case 'client_transport':
        $this->render('portlet/client_transport');
        break;
        case 'planning':
        $resources = array();
        $client = $this->Event->Client->read(array('id', 'name'), $event['Event']['client_id']);
        $resources['client'] = array(
          array('id' => $client['Client']['id'], 'label' => $client['Client']['name'], 'model' => 'client', 'icon' => 'fa fa-money')
        );
        $manager = $this->Event->User->read(array('id', 'first_name'), $event['Event']['resp_id']);
        $resources['manager'] = array(
          array('id' => $manager['User']['id'], 'label' => $manager['User']['first_name'], 'model' => 'manager', 'icon' => 'fa fa-graduation-cap')
        );
        $extras = $this->Event->Job->JobUser->find('all', array(
          'contain' => array('User'),
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array(
                'j.id = JobUser.job_id',
                'j.event_id' => $id
              )
            )
          ),
          'fields' => array('User.id', 'User.first_name', 'User.last_name')
        ));
        if(!empty($extras)){
          $resources['extra'] = array();
          foreach($extras as $extra){
            $resources['extra'][] = array(
              'id' => $extra['User']['id'],
              'model' => 'extra',
              'icon' => 'fa fa-user',
              'label' => sprintf('%s %s', $extra['User']['first_name'], $extra['User']['last_name'])
            );
          }
        }
        $places = $this->Event->EventPlace->find('all', array(
          'contain' => array('Place'),
          'conditions' => array(
            'EventPlace.event_id' => $id,
            'EventPlace.option' => array('confirmed', 'managed_by_client')
          ),
          'fields' => array('Place.id', 'Place.name')
        ));
        $defaultPlaces = $this->Event->EventPlace->Place->find('all', array(
          'conditions' => array(
            'planning_default' => 1
          )
        ));
        if(!empty($places)){
          $resources['place'] = array();
          foreach($places as $place){
            $resources['place'][] = array(
              'id' => $place['Place']['id'],
              'model' => 'place',
              'icon' => 'fa fa-map-marker',
              'label' => $place['Place']['name']
            );
          }
        }
        if(!empty($defaultPlaces)){
          foreach($defaultPlaces as $place){
            $resources['place'][] = array(
              'id' => $place['Place']['id'],
              'model' => 'place',
              'icon' => 'fa fa-map-marker',
              'label' => $place['Place']['name']
            );
          }
        }
        $activities = $this->Event->ActivityEvent->find('all', array(
          'contain' => array('Activity'),
          'conditions' => array(
            'ActivityEvent.event_id' => $id,
            'ActivityEvent.status' => 'selected'
          ),
          'fields' => array('Activity.id', 'Activity.name', 'ActivityEvent.name')
        ));
        if(!empty($activities)){
          $resources['activity'] = array();
          foreach($activities as $activity){
            $resources['activity'][] = array(
              'id' => $activity['Activity']['id'],
              'model' => 'activity',
              'icon' => 'fa fa-trophy',
              'label' => !empty($activity['ActivityEvent']['name']) ? $activity['ActivityEvent']['name'] : $activity['Activity']['name']
            );
          }
        }
        $fbmodules = $this->Event->EventFBModule->find('all', array(
          'contain' => array('FBModule'),
          'conditions' => array(
            'EventFBModule.event_id' => $id,
            'EventFBModule.status' => 'selected'
          ),
          'fields' => array('FBModule.id', 'FBModule.name', 'EventFBModule.name')
        ));
        if(!empty($fbmodules)){
          $resources['fb_module'] = array();
          foreach($fbmodules as $fbmodule){
            $resources['fb_module'][] = array(
              'id' => $fbmodule['FBModule']['id'],
              'model' => 'fb_module',
              'icon' => 'fa fa-cutlery',
              'label' => !empty($fbmodule['EventFBModule']['name']) ? $fbmodule['EventFBModule']['name'] : $fbmodule['FBModule']['name']
            );
          }
        }
        $vehicles = $this->Event->VehicleReservation->find('all', array(
          'contain' => array('Vehicle'),
          'joins' => array(
            array(
              'table' => 'events_vehicle_reservations',
              'alias' => 'evr',
              'conditions' => array(
                'evr.vehicle_reservation_id = VehicleReservation.id',
                'evr.event_id' => $id
              )
            )
          ),
          'conditions' => array(
            'VehicleReservation.confirmed' => 1,
            'VehicleReservation.cancelled' => 0,
            'VehicleReservation.archived' => 0
          ),
          'fields' => array('Vehicle.id', 'Vehicle.name', 'Vehicle.matriculation')
        ));
        if(!empty($vehicles)){
          $resources['vehicle'] = array();
          foreach($vehicles as $vehicle){
            $resources['vehicle'][] = array(
              'id' => $vehicle['Vehicle']['id'],
              'model' => 'vehicle',
              'icon' => 'fa fa-truck',
              'label' => sprintf('%s - %s', $vehicle['Vehicle']['name'], $vehicle['Vehicle']['matriculation'])
            );
          }
        }
        $this->set(compact('resources'));
        $activeBoard = 0;
        $this->set(compact('activeBoard'));
        $this->render('portlet/planning');
        break;
        case 'tasks':
        $this->render('portlet/tasks');
        break;
        case 'accounting':
        $total = round($event['Event']['invoice_amount'], 2);
        $this->Event->id = $id;
        $accountingMatch = $this->Event->accountingMatch();
        $noRepartition = false;
        $this->set(compact('noRepartition'));
        $this->set(compact('accountingMatch'));
        $this->render('portlet/accounting');
        break;
        case 'recruiting':
        $jobs = array();
        // sort events by sector
        if (!empty($event['Job'])) {
          foreach ($event['Job'] as $k => $job) {
            $jobs[$job['sector']][$k] = $job;
          }
        }
        ksort($jobs);

        // we check if extra has been added manually, if yes he does not have the competences for the job
        foreach($jobs as $sector => $jobss){
          foreach($jobss as $k => $job){
            if(!empty($job['User'])){
              foreach($job['User'] as $kk => $user){
                $this->Event->Job->User->id = $user['id'];
                $hasCompetences = $this->Event->Job->User->hasCompetences($job['id']);
                $jobs[$sector][$k]['User'][$kk]['has_competences'] = $hasCompetences;
              }
            }
          }
        }

        $this->set('jobs', $jobs);

        $briefings = $this->Event->Job->JobBriefing->find('all', array(
          'contain' => array('Job'),
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array('j.id = JobBriefing.job_id')
            ),
            array(
              'table' => 'events',
              'alias' => 'e',
              'conditions' => array('e.id = j.event_id', 'e.id' => $id)
            )
          )
        ));

        $this->set('briefings', $briefings);

        $this->render('portlet/recruiting');
        break;
        case 'places':
        if(!empty($event['EventPlace'])){
          foreach($event['EventPlace'] as $k => $ep){
            // we need to check if current place has already options
            $allCancelled = true;
            $options = $this->Event->EventPlace->find('all', array(
              'joins' => array(
                array(
                  'table' => 'events',
                  'alias' => 'Event',
                  'conditions' => array(
                    'Event.id = EventPlace.event_id'
                  )
                ),
                array(
                  'table' => 'places',
                  'alias' => 'Place',
                  'conditions' => array(
                    'EventPlace.place_id = Place.id'
                  )
                ),
                array(
                  'table' => 'clients',
                  'alias' => 'Client',
                  'conditions' => array(
                    'Event.client_id = Client.id'
                  )
                ),
                array(
                  'table' => 'companies',
                  'alias' => 'Company',
                  'conditions' => array(
                    'Event.company_id = Company.id'
                  )
                ),
                array(
                  'table' => 'dates',
                  'alias' => 'Date',
                  'type' => 'LEFT',
                  'conditions' => array(
                    'Date.event_id = Event.id',
                    'Event.confirmed_date' => null
                  )
                )
        			),
              'conditions' => array(
                'EventPlace.place_id' => $ep['place_id'],
                'EventPlace.option <>' => array('free', 'cancelled'),
                'EventPlace.option <>' => null,
                'Event.id <>' => $event['Event']['id'],
                'OR' => array(
                  array(
                    array('Event.confirmed_date <>' => null),
                    array('Event.confirmed_date' => $event['Event']['confirmed_date'])
                  ),
                  array(
                    array('Event.confirmed_date' => null),
                    array('Date.date' => $event['Event']['confirmed_date'])
                  ),
                )
              ),
              'fields' => array('EventPlace.option', 'EventPlace.place_id', 'Event.code', 'Event.name', 'Event.id', 'Client.name', 'Client.id', 'Event.confirmed_date', 'Date.date')
            ));
            if(!empty($options)){
              foreach($options as $option){
                if($option['EventPlace']['option'] != 'cancelled' && $allCancelled){
                  $allCancelled = false;
                }
              }
            }
            $event['EventPlace'][$k]['options'] = $options;
            $event['EventPlace'][$k]['all_cancelled'] = $allCancelled;
          }
        }
        $this->set(compact('event'));
        $searches = $this->Event->Search->find('all', array(
          'conditions' => array(
            'model' => 'place',
            'event_id' => $id,
            'user_id' => AuthComponent::user('id')
          ),
          'order' => 'created'
        ));
        $this->set(compact('searches'));
        $this->set('placesOptions', Configure::read('Places.options'));
        $this->render('portlet/places');
        break;
        case 'hours':
        $this->render('portlet/hours');
        break;
        case 'orders':
        $firstGroup = array();
        if(!empty($event['Order']['OrderGroup'])){
          $firstGroup = array($event['Order']['OrderGroup'][0]['id']);
        }
        $this->set(compact('firstGroup'));
        $this->render('portlet/orders');
        break;
        case 'fb':
        $this->render('portlet/fb');
        break;
        case 'activities':
        $this->render('portlet/activities');
        break;
        case 'fb_modules':
        $this->render('portlet/fb_modules');
        break;
        case 'invoicing':
        $this->render('portlet/invoicing');
        break;
        case 'weather':
        //$HttpSocket = new HttpSocket();
        //$json = $HttpSocket->get('http://www.prevision-meteo.ch/services/json/lat=46.2985391lng=7.0464938');
        //$json = json_decode(file_get_contents('http://www.prevision-meteo.ch/services/json/lat=46.8031637lng=7.1422125'));
        $json = array();
        $this->set(compact('json'));
        $this->render('portlet/weather');
        break;
        case 'downloads':
        $downloads = array(
          'confirmation' => array(
            'title' => __('Engagement confirmation'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-check',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'confirmations', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'billboard' => array(
            'title' => __('Billboard'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-map-signs',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'billboard', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'practical' => array(
            'title' => __('Practical info'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-info-circle',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'practical_info', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'briefing' => array(
            'title' => __('Briefing'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-file-o',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'briefing', 'ext' => 'pdf')),
            //'modal' => Router::url(array('controller' => 'events', 'action' => 'briefing', $id)),
            'modal' => 0,
            'callback' => 0
          ),
          'memo' => array(
            'title' => __('Memo'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-clipboard',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'memo', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'feedback' => array(
            'title' => __('Feedback'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-comments-o',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'feedback', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'hours' => array(
            'title' => __('Hours'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-clock-o',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'hours', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          ),
          'evaluation' => array(
            'title' => __('Evaluation'),
            'bg' => 'bg-green',
            'icon' => 'fa fa-thumbs-o-up',
            'href' => Router::url(array('controller' => 'events', 'action' => 'documents', $id, 'evaluation', 'ext' => 'pdf')),
            'modal' => 0,
            'callback' => 0
          )
        );
        $this->set(compact('downloads'));
        $this->render('portlet/downloads');
        break;
        default:
        break;
      }
    }
  }

  public function hours(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $job = $this->Event->Job->findById($this->request->data['job_id']);
      $jobUser = $this->Event->Job->JobUser->findByJobIdAndUserId($this->request->data['job_id'], $this->request->data['user_id']);
      $jobUser['JobUser']['effective_start_hour'] = $this->request->data['start'];
      $jobUser['JobUser']['effective_end_hour'] = $this->request->data['end'];
      $jobUser['JobUser']['effective_break'] = $this->request->data['break'];
      $jobUser['JobUser']['salary'] = $this->request->data['salary'];
      $start = new DateTime($this->request->data['start']);
      $end = new DateTime($this->request->data['end']);
      $break = new DateTime($this->request->data['break']);
      $midnight = new DateTime('00:00:00');

      if($start > $end){
        $numberOfHours = 24*3600 - $start->getTimeStamp() + $end->getTimeStamp();
      } else {
        $numberOfHours = $start->getTimeStamp() - $end->getTimeStamp();
      }
      if($this->request->data['break'] != '0:00'){
        $break = strtotime($this->request->data['break']) - strtotime('00:00:00');
      } else {
        $break = 0;
      }
      $numberOfHours = abs($numberOfHours);
      $hours = ($numberOfHours - $break) / 3600;
      //$salary = !empty($jobUser['JobUser']['salary']) ? $jobUser['JobUser']['salary'] : $job['Job']['salary'];
      $salary = $jobUser['JobUser']['salary'];
      $jobUser['JobUser']['hours'] = $hours;
      $jobUser['JobUser']['total'] = $salary * 0.8175 * $hours;
      if($this->Event->Job->JobUser->save($jobUser, array('callbacks' => false))){
        $ju = $this->Event->Job->JobUser->read();
        $this->response->body(json_encode(array('success' => 1, 'salary' => $salary, 'hours' => gmdate('H:i', floor($ju['JobUser']['hours'] * 3600)), 'total' => sprintf('%s CHF', $ju['JobUser']['total']))));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function validate_hours(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $jobUser = $this->Event->Job->JobUser->findByJobIdAndUserId($this->request->data['job_id'], $this->request->data['user_id']);
      $jobUser['JobUser']['validated'] = $this->request->data['validated'];
      if($this->Event->Job->JobUser->save($jobUser, array('callbacks' => false))){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function updateField(){
    // x-editable
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->Event->id = $this->request->data['pk'];
      if($this->Event->saveField($this->request->data['name'], $this->request->data['value'])){
        $this->Event->updateOrderItems();
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function staff( $slug = null ){

    $view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
    $ts = strtotime(empty($this->request->data['start']) ? date('Y-m-d') : $this->request->data['start']);
    $start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
    $end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
    $start_date = date('Y-m-d', $start);
    $end_date = date('Y-m-d', $end);

    $jobs = Configure::read('Competences.jobs');

    if ($view == 'month') {
      $month = date('m', $ts);
      $days = date('t', $ts);
      $start_date = date(sprintf('Y-%s-01', $month));
      $end_date = date(sprintf('Y-%s-%s', $month, $days));
      $end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
    }

    $companyIds = array();

    if(!empty($slug)){
      $slug1 = explode('-', $slug);
      $company = $this->Event->Company->find('all', array(
        'conditions' => array(
          'class' => $slug1
        )
      ));
      if($company){
        foreach($company as $c){
          $companyIds[] = (int) $c['Company']['id'];
        }
      }
    } elseif(!empty($this->request->data['company'])){
      $companyIds = explode(',', $this->request->data['company']);
    } else {
      $companyIds = array(1,2,3,4,5,6);
    }
    $this->set(compact('companyIds'));

    $data = $this->Event->Job->find('all', array(
      'contain' => array('Event' => array('Client', 'Company'), 'JobUser' => array('User' => array('Portrait'))),
      'conditions' => array(
        'Event.confirmed_date >=' => $start_date,
        'Event.confirmed_date <=' => $end_date,
        'Event.company_id' => $companyIds
      ),
      'order' => 'Job.event_id'
    ));

    $staff = array();
    $check = array();
    $counter = 0;

    if(!empty($data)){
      foreach($data as $k => $item){

        if(empty($item['Event'])){continue;}

        if(empty($check[$item['Event']['confirmed_date']])){
          $check[$item['Event']['confirmed_date']] = array();
        }

        if(!empty($item['JobUser'])){
          foreach($item['JobUser'] as $kk => $ju){
            if(in_array($ju['User']['id'], $check[$item['Event']['confirmed_date']])){
              $notify = true;
            } else {
              $notify = false;
              $check[$item['Event']['confirmed_date']][] = $ju['User']['id'];
            }
            $staff[] = $this->Event->generateExtraEvent( $item, $ju['User'], $notify );
          }
        }

        if($item['Job']['staff_enrolled'] < $item['Job']['staff_needed'] && sizeof($item['JobUser']) < $item['Job']['staff_needed']){
          //CakeLog::write('myubic', json_encode($item['Job']));
          // there are missing staff for this job
          for($i = 0; $i < ($item['Job']['staff_needed'] - $item['Job']['staff_enrolled']); $i++){
            $staff[] = $this->Event->generateExtraEvent( $item );
          }
        }

      }
    }

    //ksort($staff, SORT_NATURAL);
    $this->set('_json', array_values($staff));
  }

  public function filters(){
    // filters for staff calendar
    if($this->request->is('ajax')){

      $view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
      $ts = strtotime(empty($this->request->data['date']) ? date('Y-m-d') : $this->request->data['date']);
      $start = (date('w', $ts) == 1) ? $ts : strtotime('last sunday', $ts);
      $end = (date('w', $ts) == 0) ? $ts : strtotime('next monday', $ts);
      $start_date = date('Y-m-d', $start);
      $end_date = date('Y-m-d', $end);

      if($view == 'basicDay'){
        $start_date = $this->request->data['date'];
        $end_date = $this->request->data['date'];
      }

      $companyIds = array(1,2,3,4,5,6);
      if(!empty($this->request->data['company'])){
        $companyIds = explode(',', $this->request->data['company']);
      }

      $events = $this->Event->find('all', array(
        'conditions' => array(
          'Event.confirmed_date >=' => $start_date,
          'Event.confirmed_date <=' => $end_date,
          'Event.crm_status' => array('confirmed'),
          'Event.company_id' => $companyIds
        ),
        'order' => 'Event.confirmed_date',
        'fields' => array('Event.name', 'Event.id', 'Event.calendar_color', 'Event.type')
      ));
      $this->loadModel('Mission');
      $missions = $this->Mission->find('all', array(
        'conditions' => array(
          'Mission.confirmed_date >=' => $start_date,
          'Mission.confirmed_date <=' => $end_date,
          'Mission.company_id' => $companyIds
        ),
        'order' => 'Mission.confirmed_date',
        'fields' => array('Mission.name', 'Mission.id', 'Mission.calendar_color', 'Mission.type')
      ));

      $this->set(compact('events'));
      $this->set(compact('missions'));
    }
  }

  public function add_activity(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $activity = $this->Event->Activity->findById($this->request->data['activity_id']);
      $ae = $this->Event->ActivityEvent->findByActivityIdAndEventId($this->request->data['activity_id'], $this->request->data['event_id']);
      $data = array(
        'ActivityEvent' => array(
          'id' => (empty($ae) || $activity['Activity']['standard'] == 0) ? '' : $ae['ActivityEvent']['id'],
          'activity_id' => $this->request->data['activity_id'],
          'event_id' => $this->request->data['event_id'],
          'status' => 'suggested'
        )
      );
      if($this->Event->ActivityEvent->save($data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function select_activity(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $ae = $this->Event->ActivityEvent->findById($this->request->data['id']);
      if(!empty($ae)){
        if($this->request->data['status'] == 'selected'){
          $ae['ActivityEvent']['status'] = 'suggested';
          // we need to delete orders corresponding to this activity
          $this->loadModel('Order');
          $this->Order->deleteFromEvent($ae['ActivityEvent']['event_id'], 'activity', $ae['ActivityEvent']['activity_id']);
          // we need to delete moments and resources corresponding to this activity
          $this->loadModel('PlanningBoardMoment');
          $this->PlanningBoardMoment->deleteFromEvent($ae['ActivityEvent']['event_id'], 'activity', $ae['ActivityEvent']['activity_id']);
        }
        if($this->request->data['status'] == 'suggested'){
          $ae['ActivityEvent']['status'] = 'selected';
        }
        if($this->Event->ActivityEvent->save($ae)){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }  else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function delete_activity(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $ae = $this->Event->ActivityEvent->findById($this->request->data['id']);
      if(!empty($ae)){
        $activity_id = $ae['ActivityEvent']['activity_id'];
        $event_id = $ae['ActivityEvent']['event_id'];
        if($this->Event->ActivityEvent->delete($ae['ActivityEvent']['id'])){
          // we need to delete orders corresponding to this activity
          $this->loadModel('Order');
          $this->Order->deleteFromEvent($ae['ActivityEvent']['event_id'], 'activity', $ae['ActivityEvent']['activity_id']);
          // we need to delete moments and resources corresponding to this activity
          $this->loadModel('PlanningBoardMoment');
          $this->PlanningBoardMoment->deleteFromEvent($ae['ActivityEvent']['event_id'], 'activity', $ae['ActivityEvent']['activity_id']);
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }  else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function add_fb_module(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $em = $this->Event->EventFBModule->find('first', array(
        'conditions' => array(
          'fb_module_id' => $this->request->data['fb_module_id'],
          'event_id' => $this->request->data['event_id']
        )
      ));
      $module = $this->Event->FBModule->findById($this->request->data['fb_module_id']);
      $data = array(
        'EventFBModule' => array(
          'id' => (empty($em) || $module['FBModule']['standard'] == 0) ? '' : $em['EventFBModule']['id'],
          'fb_module_id' => $this->request->data['fb_module_id'],
          'event_id' => $this->request->data['event_id'],
          'status' => 'suggested'
        )
      );
      if($this->Event->EventFBModule->save($data)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function select_fb_module(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $em = $this->Event->EventFBModule->findById($this->request->data['id']);
      if(!empty($em)){
        if($this->request->data['status'] == 'selected'){
          $em['EventFBModule']['status'] = 'suggested';
          // we need to delete orders corresponding to this module
          $this->loadModel('Order');
          $this->Order->deleteFromEvent($em['EventFBModule']['event_id'], 'fb_module', $em['EventFBModule']['fb_module_id']);
          // we need to delete moments and resources corresponding to this module
          $this->loadModel('PlanningBoardMoment');
          $this->PlanningBoardMoment->deleteFromEvent($em['EventFBModule']['event_id'], 'fb_module', $em['EventFBModule']['fb_module_id']);
        }
        if($this->request->data['status'] == 'suggested'){
          $em['EventFBModule']['status'] = 'selected';
        }
        if($this->Event->EventFBModule->save($em)){
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }  else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function delete_fb_module(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $em = $this->Event->EventFBModule->findById($this->request->data['id']);
      if(!empty($em)){
        $module_id = $em['EventFBModule']['fb_module_id'];
        $event_id = $em['EventFBModule']['event_id'];
        if($this->Event->EventFBModule->delete($em['EventFBModule']['id'])){
          // we need to delete orders corresponding to this module
          $this->loadModel('Order');
          $this->Order->deleteFromEvent($em['EventFBModule']['event_id'], 'fb_module', $em['EventFBModule']['fb_module_id']);
          // we need to delete moments and resources corresponding to this module
          $this->loadModel('PlanningBoardMoment');
          $this->PlanningBoardMoment->deleteFromEvent($em['EventFBModule']['event_id'], 'fb_module', $em['EventFBModule']['fb_module_id']);
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }  else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function getCRMStatuses(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->response->body(json_encode(Configure::read('Events.crm_status')));
    }
  }

  public function getMissionsStatuses(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $this->response->body(json_encode(Configure::read('Missions.status')));
    }
  }

  public function updateCRMStatus(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data['pk'])){
        $this->Event->id = $this->request->data['pk'];
        if($this->Event->saveField('crm_status', $this->request->data['value'])){
          $this->Event->updateOrderItems();
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function google( $type = null ){
    if(empty($type)){
      return;
    }
    $this->layout = 'default';
    $this->response->type('ics');
    $this->response->disableCache();
    $this->helpers[] = 'Tools.Ical';
    $this->Event->contain(array(
      'Company',
      'PersonInCharge',
      'Client',
      'ConfirmedEventPlace' => array('Place'),
      'SelectedActivity' => array('Activity'),
      'SuggestedActivity' => array('Activity')
    ));
    $conditions = array(
      'Event.name <>' => null,
      'Event.confirmed_date <>' => null
    );
    $joins = array();
    switch($type){
      case 'ubic':
        $conditions[] = array(
          'Event.crm_status' => 'confirmed',
          'Event.company_id' => 2
        );
        break;
      case 'ug':
        $conditions[] = array(
          'Event.crm_status' => 'confirmed',
          'Event.company_id' => 6
        );
        break;
      case 'eg':
        $conditions[] = array(
          'Event.crm_status' => 'confirmed',
          'Event.company_id' => array(4,5)
        );
        break;
      case 'ubic-options':
        $conditions[] = array(
          'Event.crm_status' => array_keys(Configure::read('Events.crm_status_default')),
          'Event.company_id' => 2
        );
        break;
      case 'ug-options':
        $conditions[] = array(
          'Event.crm_status' => array_keys(Configure::read('Events.crm_status_default')),
          'Event.company_id' => 6
        );
        break;
      case 'ubic-ug':
        $conditions[] = array(
          'Event.crm_status' => 'confirmed',
          'Event.company_id' => 2,
          'a.company_id' => 6,
          'ae.status' => 'selected'
        );
        $joins = array(
          array(
            'table' => 'activities_events',
            'alias' => 'ae',
            'conditions' => array('Event.id = ae.event_id')
          ),
          array(
            'table' => 'activities',
            'alias' => 'a',
            'conditions' => 'a.id = ae.activity_id'
          )
        );
        break;
      default:
      break;

    }
    $events = $this->Event->find('all', array(
      'joins' => $joins,
      'conditions' => $conditions
    ));
    $this->set(compact('events'));
  }

  public function add_place(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $ep = array('EventPlace' => array(
        'event_id' => $this->request->data['event_id'],
        'place_id' => $this->request->data['place_id']
      ));
      $this->Event->EventPlace->create();
      if($this->Event->EventPlace->save($ep)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function add_vehicle_family(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $event = $this->Event->findById($this->request->data['event_id']);
      $start = strtotime($event['Event']['confirmed_date']);
      $startDay = date('Y-m-d', $start);
      $endDay = date('Y-m-d', $start);
      if($event['Event']['end_hour'] < $event['Event']['start_hour']){
        $endDay = date('Y-m-d', strtotime($startDay . "+1 day"));
        $start = date('Y-m-d H:i:s', strtotime($startDay . ' ' . $event['Event']['start_hour']));
        $end = date('Y-m-d H:i:s', strtotime($endDay . ' ' . $event['Event']['end_hour']));
      } else {
        $start = date('Y-m-d H:i:s', strtotime($startDay . ' ' . $event['Event']['start_hour']));
        $end = date('Y-m-d H:i:s', strtotime($startDay . ' ' . $event['Event']['end_hour']));
      }

      $reservation = array(
        'VehicleReservation' => array(
          'family' => $this->request->data['family'],
          'start' => $start,
          'end' => $end,
          'confirmed' => 0,
          'start_place' => 'ubic',
          'end_place' => 'ubic'
        ),
        'Event' => array(
          'id' => $this->request->data['event_id']
        )
      );
      $this->Event->VehicleReservation->create();
      if($this->Event->VehicleReservation->save($reservation)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function assign_driver(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      $reservation = $this->Event->VehicleReservation->findById($this->request->data['id']);
      $reservation['VehicleReservation'][$this->request->data['field']] = empty($this->request->data['driver_id']) ? '' : $this->request->data['driver_id'];
      if($this->Event->VehicleReservation->save($reservation)){
        $this->response->body(json_encode(array('success' => 1)));
      } else {
        $this->response->body(json_encode(array('success' => 0)));
      }
    }
  }

  public function orders( $id = null, $mode = null ){
    App::uses('CakePdf', 'CakePdf.Pdf');

    $this->layout = 'default';
    $this->pdfConfig = array(
      'filename' => 'test.pdf',
      'download' => false,
      // 'options' => array(
      //   'footer-center' => '[page]',
      //   'footer-font-size' => 8,
      // ),
    );

    $showAlley = false;

    $this->set(compact('mode'));

    switch($mode){
      case 'locations':
        $showAlley = true;
        $this->loadModel('StockLocation');
        $locations = $this->StockLocation->find('all');
        $locations[] = array(
          'StockLocation' => array(
            'id' => null,
            'name' => __('Not defined')
          )
        );
        foreach($locations as $k => $location){

          $orders = $this->Event->Order->find('all', array(
            'joins' => array(
              array(
                'table' => 'order_groups',
                'alias' => 'og',
                'type' => 'inner',
                'conditions' => array('Order.id = og.order_id')
              ),
              array(
                'table' => 'order_items',
                'alias' => 'oi',
                'type' => 'inner',
                'conditions' => array(
                  'oi.order_group_id = og.id'
                )
              )
            ),
            'contain' => array(
              'OrderGroup' => array(
                'fields' => array('OrderGroup.name'),
                'OrderItem' => array(
                  'fields' => array('OrderItem.name', 'OrderItem.quantity', 'OrderItem.number_of_containers', 'OrderItem.quantity_option', 'OrderItem.quantity_unit', 'OrderItem.remarks'),
                  'StockLocation' => array('fields' => array('StockLocation.name')),
                  'StockItem' => array('fields' => array('StockItem.code_name')),
                  'conditions' => array(
                    'OrderItem.stock_location_id' => $location['StockLocation']['id'],
                    'OrderItem.quantity > 0'
                  )
                )
              )
            ),
            'conditions' => array(
              'Order.model' => 'event',
              'Order.model_id' => $id,
              'oi.stock_location_id' => $location['StockLocation']['id']
            ),
            'group' => 'Order.id',
            'fields' => array('Order.name')
          ));
          $orders = $this->Event->Order->mergeDuplicates($orders);

          if(!empty($orders)){
            foreach($orders as $j => $order){
              foreach($order['OrderGroup'] as $kk => $group){
                if(empty($group['OrderItem'])){
                  unset($orders[$j]['OrderGroup'][$kk]);
                }
                if(sizeof($orders[$j]['OrderGroup']) == 0){
                  unset($orders[$j]);
                }
              }
              if(!empty($orders[$j]['OrderGroup'])) sort($orders[$j]['OrderGroup']);
            }
            $locations[$k]['orders'] = $orders;
          } else {
            unset($locations[$k]);
          }


        }

        $this->set(compact('locations'));
        $this->Event->contain(array(
          'Company' => array('Logo'),
          'Client',
          'PersonInCharge',
          'SelectedActivity' => array('Activity'),
          'ConfirmedEventPlace' => array('Place')
        ));
        $event = $this->Event->findById($id);
        $this->set(compact('event'));
      break;
      case 'suppliers':
        $this->loadModel('Supplier');
        $suppliers = $this->Supplier->find('all', array(
          'conditions' => array(
            'external_supplier' => 1
          )
        ));
        $suppliers[] = array(
          'Supplier' => array(
            'id' => null,
            'name' => __('Not defined')
          )
        );
        foreach($suppliers as $k => $supplier){

          $orders = $this->Event->Order->find('all', array(
            'joins' => array(
              array(
                'table' => 'order_groups',
                'alias' => 'og',
                'type' => 'inner',
                'conditions' => array('Order.id = og.order_id')
              ),
              array(
                'table' => 'order_items',
                'alias' => 'oi',
                'type' => 'inner',
                'conditions' => array(
                  'oi.order_group_id = og.id'
                )
              )
            ),
            'contain' => array(
              'OrderGroup' => array(
                'fields' => array('OrderGroup.name'),
                'OrderItem' => array(
                  'fields' => array('OrderItem.name', 'OrderItem.quantity', 'OrderItem.number_of_containers', 'OrderItem.quantity_option', 'OrderItem.quantity_unit', 'OrderItem.remarks'),
                  'StockLocation' => array('fields' => array('StockLocation.name')),
                  'StockItem' => array('fields' => array('StockItem.code_name')),
                  'conditions' => array(
                    'OrderItem.supplier_id' => $supplier['Supplier']['id'],
                    'OrderItem.quantity > 0'
                  )
                )
              )
            ),
            'conditions' => array(
              'Order.model' => 'event',
              'Order.model_id' => $id,
              'oi.supplier_id' => $supplier['Supplier']['id']
            ),
            'group' => 'Order.id',
            'fields' => array('Order.name')
          ));
          $orders = $this->Event->Order->mergeDuplicates($orders);
          $numberOfItems = 0;
          if(!empty($orders)){

            foreach($orders as $j => $order){
              foreach($order['OrderGroup'] as $kk => $group){
                if(empty($group['OrderItem'])){
                  unset($orders[$j]['OrderGroup'][$kk]);
                }
                if(sizeof($orders[$j]['OrderGroup']) == 0){
                  unset($orders[$j]);
                }
              }
              if(!empty($orders[$j]['OrderGroup'])) sort($orders[$j]['OrderGroup']);
            }

          }
          $suppliers[$k]['orders'] = $orders;
        }
        $this->set(compact('suppliers'));
        $this->Event->contain(array(
          'Company' => array('Logo'),
          'Client',
          'PersonInCharge',
          'SelectedActivity' => array('Activity'),
          'ConfirmedEventPlace' => array('Place')
        ));
        $event = $this->Event->findById($id);
        if($event['Event']['company_id'] == 5){
          $this->Event->Company->contain(array('Logo'));
          $eg = $this->Event->Company->findById(4);
          $event['Event']['company_id'] = 4;
          $event['Company']['Logo'] = $eg['Logo'];
        }
        $this->set(compact('event'));

        $depotArrival = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array('PlanningBoardMoment.planning_board_id = pb.id')
            )
          ),
          'conditions' => array(
            'pb.model' => 'event',
            'pb.model_id' => $id,
            'PlanningBoardMoment.type' => 'loading'
          ),
          'order' => 'PlanningBoardMoment.start'
        ));

        $depotLeaving = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array('PlanningBoardMoment.planning_board_id = pb.id')
            )
          ),
          'conditions' => array(
            'pb.model' => 'event',
            'pb.model_id' => $id,
            'PlanningBoardMoment.type' => 'route'
          ),
          'order' => 'PlanningBoardMoment.start'
        ));

        $emdo = $this->Event->Job->JobUser->find('first', array(
          'contain' => array('User' => array('fields' => array('full_name', 'phone', 'mobile'))),
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array(
                'j.id = JobUser.job_id',
                'j.event_id' => $id
              )
            )
          ),
          'conditions' => array(
            'j.hierarchy' => array(3,4),
            'OR' => array(
              array('j.job' => 'animator'),
              array('j.job' => 'urban_leader')
            )
          ),
          'order' => 'j.hierarchy'
        ));
        $this->set(compact('emdo'));

        $this->set(compact('emdo'));
        $this->set(compact('depotArrival'));
        $this->set(compact('depotLeaving'));

      break;
      default:
        $this->Event->contain(array(
          'Company' => array('Logo'),
          'Client',
          'PersonInCharge',
          'SelectedActivity' => array('Activity'),
          'ConfirmedEventPlace' => array('Place'),
          'Order' => array('OrderGroup' => array('OrderItem' => array('StockItem' => array('StockLocation'), 'Supplier', 'conditions' => array('OrderItem.quantity > 0'))))
        ));
        $event = $this->Event->findById($id);
        $event['Order'] = $this->Event->Order->mergeDuplicates($event['Order']);
        $this->set(compact('event'));
      break;
    }
    $this->set(compact('showAlley'));

  }

  public function add_reservation(){
    if($this->request->is('ajax')){
      $this->autoRender = false;
      if(!empty($this->request->data)){
        if (!empty($this->request->data['Event']['confirmed_date'])) {
          $this->request->data['Event']['confirmed_date'] = date('Y-m-d', strtotime($this->request->data['Event']['confirmed_date']));
        }
        if (!empty($this->request->data['Event']['start_hour'])) {
          $this->request->data['VehicleReservation']['start'] = $this->request->data['Event']['confirmed_date'] . ' ' . $this->request->data['Event']['start_hour'];
        }
        if (!empty($this->request->data['Event']['end_hour'])) {
          $this->request->data['VehicleReservation']['end'] = $this->request->data['Event']['confirmed_date'] . ' ' . $this->request->data['Event']['end_hour'];
        }
        $VehicleReservation = $this->request->data['VehicleReservation'];
        unset($this->request->data['VehicleReservation']);
        //CakeLog::write('myubic', json_encode($this->request->data));
        if($this->Event->save($this->request->data, array('callbacks' => false))){
          $reservation = array(
            'VehicleReservation' => $VehicleReservation,
            'Event' => array(
              'id' => $this->Event->id
            )
          );
          $this->Event->VehicleReservation->save($reservation);
          $jobs = array();
          if(!empty($reservation['VehicleReservation']['driver_id'])){
            // we create a driver job with selected user
            $jobs[] = array(
              'Job' => array(
                'name' => 'Chauffeur',
                'job' => 'driver',
                'sector' => 'logistics',
                'event_id' => $this->Event->id,
                'start_time' => $this->request->data['Event']['start_hour'],
                'end_time' => $this->request->data['Event']['end_hour'],
                'staff_needed' => 1,
                'staff_enrolled' => 1
              ),
              'User' => array(
                'id' => $reservation['VehicleReservation']['driver_id']
              )
            );
          }
          if(!empty($reservation['VehicleReservation']['back_driver_id']) && $reservation['VehicleReservation']['back_driver_id'] != $reservation['VehicleReservation']['driver_id']){
            // we create a driver job with selected user
            $jobs[] = array(
              'Job' => array(
                'name' => 'Chauffeur',
                'job' => 'driver',
                'sector' => 'logistics',
                'event_id' => $this->Event->id,
                'start_time' => $this->request->data['Event']['start_hour'],
                'end_time' => $this->request->data['Event']['end_hour'],
                'staff_needed' => 1,
                'staff_enrolled' => 1
              ),
              'User' => array(
                'id' => $reservation['VehicleReservation']['back_driver_id']
              )
            );
          }
          if(!empty($jobs)) $this->Event->Job->saveMany($jobs);
          $this->response->body(json_encode(array('success' => 1)));
        } else {
          $this->response->body(json_encode(array('success' => 0)));
        }
      }
    }
  }

  public function documents($id = null, $mode = null){

    if (!$id) {
      throw new NotFoundException(__('Invalid event'));
    }

    App::uses('CakePdf', 'CakePdf.Pdf');
    $this->layout = 'default';

    switch ($mode) {
      case 'confirmations':
      case 'hours':
      case 'feedback':
      case 'evaluation':
        $pdfOptions = array(
          'footer-left' => date('d.m.Y H:i'),
          'footer-right' => '[page]',
          'footer-font-size' => 8,
          'margin-top' => '20mm',
          'margin-right' => '20mm',
          'margin-bottom' => '20mm',
          'margin-left' => '20mm'
        );
        $this->Event->contain(array(
          'Client' => array('fields' => array('name', 'zip_city')),
          'ContactPeople' => array('fields' => array('full_name', 'phone')),
          'Company' => array('fields' => array('name', 'class'), 'Logo'),
          'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city'))),
          'PersonInCharge' => array('fields' => array('full_name')),
          'GeneralPlanning' => array(
            'PlanningBoardMoment' => array(
              'Activity' => array('fields' => array('name')),
              'FBModule' => array('fields' => array('name')),
              'Client' => array('fields' => array('name')),
              'Place' => array('fields' => array('name', 'zip_city')),
              'Vehicle' => array('fields' => array('name_number')),
              'Manager' => array('fields' => array('full_name')),
              'Extra' => array('fields' => array('full_name'))
            )
          )
        ));
        $staff = $this->Event->Job->JobUser->find('all', array(
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array(
                'j.id = JobUser.job_id',
                'j.event_id' => $id
              )
            )
          ),
          'contain' => array(
            'User' => array('fields' => array('full_name', 'first_name', 'phone', 'mobile', 'role', 'trainee')),
            'Job' => array(
              'Activity' => array('fields' => array('name')),
              'FBModule' => array('fields' => array('name')),
              'Outfit' => array('OutfitItem'),
              'fields' => array('name', 'job', 'languages', 'engagement_remarks', 'start_time', 'end_time')
            )
          )
        ));
        foreach($staff as $k => $item){
          $moments = $this->Event->PlanningBoard->PlanningBoardMoment->find('all', array(
            'contain' => array('Place'),
            'joins' => array(
              array(
                'table' => 'planning_resources',
                'alias' => 'pr',
                'conditions' => array(
                  'pr.planning_board_moment_id = PlanningBoardMoment.id',
                  'pr.resource_id' => $item['JobUser']['user_id'],
                  'pr.resource_model' => 'extra'
                )
              ),
              array(
                'table' => 'planning_boards',
                'alias' => 'pb',
                'conditions' => array(
                  'pb.id = PlanningBoardMoment.planning_board_id',
                  'pb.model' => 'event',
                  'pb.model_id' => $id
                )
              )
            ),
            'order' => array('ISNULL(start)', 'start', 'weight', 'end')
          ));
          $staff[$k]['moments'] = $moments;
          $convocation = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
            'contain' => array('Place'),
            'joins' => array(
              array(
                'table' => 'planning_resources',
                'alias' => 'pr',
                'conditions' => array(
                  'pr.planning_board_moment_id = PlanningBoardMoment.id',
                  'pr.resource_id' => $item['JobUser']['user_id'],
                  'pr.resource_model' => 'extra'
                )
              ),
              array(
                'table' => 'planning_boards',
                'alias' => 'pb',
                'conditions' => array(
                  'pb.id = PlanningBoardMoment.planning_board_id',
                  'pb.model' => 'event',
                  'pb.model_id' => $id
                )
              )
            ),
            'conditions' => array(
              'PlanningBoardMoment.type' => 'appointment'
            ),
            'order' => array('start', 'weight', 'end')
          ));
          //debug($convocation);
          $mandateStart = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
            'contain' => array('Place'),
            'joins' => array(
              array(
                'table' => 'planning_resources',
                'alias' => 'pr',
                'conditions' => array(
                  'pr.planning_board_moment_id = PlanningBoardMoment.id',
                  'pr.resource_id' => $item['JobUser']['user_id'],
                  'pr.resource_model' => 'extra'
                )
              ),
              array(
                'table' => 'planning_boards',
                'alias' => 'pb',
                'conditions' => array(
                  'pb.id = PlanningBoardMoment.planning_board_id',
                  'pb.model' => 'event',
                  'pb.model_id' => $id
                )
              )
            ),
            'conditions' => array(
              'PlanningBoardMoment.type' => 'mandate_start'
            ),
            'order' => array('start', 'weight', 'end')
          ));
          $mandateEnd = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
            'contain' => array('Place'),
            'joins' => array(
              array(
                'table' => 'planning_resources',
                'alias' => 'pr',
                'conditions' => array(
                  'pr.planning_board_moment_id = PlanningBoardMoment.id',
                  'pr.resource_id' => $item['JobUser']['user_id'],
                  'pr.resource_model' => 'extra'
                )
              ),
              array(
                'table' => 'planning_boards',
                'alias' => 'pb',
                'conditions' => array(
                  'pb.id = PlanningBoardMoment.planning_board_id',
                  'pb.model' => 'event',
                  'pb.model_id' => $id
                )
              )
            ),
            'conditions' => array(
              'PlanningBoardMoment.type' => 'mandate_end'
            ),
            'order' => array('start', 'weight', 'end')
          ));
          $staff[$k]['moments'] = $moments;
          $staff[$k]['convocation'] = $convocation;
          $staff[$k]['mandate_start'] = $mandateStart;
          $staff[$k]['mandate_end'] = $mandateEnd;
        }
        //exit;
        $this->set(compact('staff'));

        $clientPlanning = $this->Event->PlanningBoard->PlanningBoardMoment->find('all', array(
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event'
              )
            ),
            array(
              'table' => 'planning_resources',
              'alias' => 'pr',
              'conditions' => array(
                'pr.resource_model' => 'client',
                'pr.planning_board_moment_id = PlanningBoardMoment.id'
              )
            )
          ),
          'order' => array('PlanningBoardMoment.weight')
        ));

        $clientArrival = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event'
              )
            )
          ),
          'conditions' => array(
            'PlanningBoardMoment.type' => 'client_arrival'
          ),
          'order' => 'start'
        ));

        $this->set(compact('clientPlanning'));
        $this->set(compact('clientArrival'));

        $carSharingStarts = $this->Event->PlanningBoard->PlanningBoardMoment->find('all', array(
          'contain' => array('Place'),
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event',
                'pb.type' => 'general'
              )
            )
          ),
          'conditions' => array(
            'PlanningBoardMoment.type' => 'car_sharing_start'
          ),
          'order' => 'start'
        ));

        $this->set(compact('carSharingStarts'));
        break;
      case 'billboard':
        $this->Event->contain(array(
          'EventCategory',
          'Job',
          'Client' => array('fields' => array('name'), 'Logo'),
          'Company' => array('fields' => array('name'), 'Logo'),
          'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city'))),
          'PersonInCharge' => array('fields' => array('full_name', 'first_name')),
          'BillboardPlanning' => array(
            'PlanningBoardMoment'
          )
        ));
        $pdfOptions = array(
          'margin-top' => '0mm',
          'margin-right' => '10mm',
          'margin-bottom' => '10mm',
          'margin-left' => '10mm'
        );
        break;
      case 'practical_info':
        $client = $this->Event->read(array('client_id', 'company_id'), $id);
        $this->Event->contain(array(
          'EventCategory',
          'Client' => array('fields' => array('name'), 'Logo'),
          'Company' => array('fields' => array('name', 'class'), 'Logo'),
          'SelectedActivity' => array('Activity' => array('OutfitAssociation' => array('Outfit' => array('OutfitItem', 'conditions' => array('Outfit.type' => 'client'))), 'fields' => array('name'))),
          'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city'))),
          'PersonInCharge' => array('fields' => array('full_name', 'first_name', 'business_phone')),
          'Region'
        ));
        $pdfOptions = array(
          'margin-top' => '20mm',
          'margin-right' => '10mm',
          'margin-bottom' => '20mm',
          'margin-left' => '24mm'
        );
        if($client['Event']['company_id'] == 6){
          $pdfOptions['footer-html'] = Router::url(array('controller' => 'events', 'action' => 'pdf_footer', 6, 'full_base' => true));
          $pdfOptions['footer-font-size'] = 8;
          $pdfOptions['footer-center'] = '[page]';
        }

        $clientMoments = $this->Event->PlanningBoard->PlanningBoardMoment->find('all', array(
          'contain' => array('Place' => array('fields' => 'zip_city')),
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'PlanningBoardMoment.planning_board_id = pb.id',
                'pb.model' => 'event',
                'pb.model_id' => $id,
                'pb.type' => 'practical_info'
              )
            )
          ),
          'order' => 'weight'
        ));
        $this->set(compact('clientMoments'));

        $clientArrival = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'contain' => array('Place' => array('fields' => array('name', 'zip_city', 'address'))),
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event',
                'pb.type' => 'practical_info'
              )
            )
          ),
          'conditions' => array(
            'PlanningBoardMoment.type' => 'client_arrival'
          ),
          'order' => 'start'
        ));
        $this->set(compact('clientArrival'));

        $clientDeparture = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'contain' => array('Place' => array('fields' => array('name', 'zip_city', 'address'))),
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event',
                'pb.type' => 'practical_info'
              )
            )
          ),
          'conditions' => array(
            'PlanningBoardMoment.type' => 'client_departure'
          ),
          'order' => array('end DESC', 'start DESC')
        ));
        $this->set(compact('clientDeparture'));

        $emdo = $this->Event->Job->JobUser->find('first', array(
          'contain' => array('User' => array('fields' => array('full_name', 'phone', 'mobile'))),
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array(
                'j.id = JobUser.job_id',
                'j.event_id' => $id
              )
            )
          ),
          'conditions' => array(
            'j.hierarchy' => array(3,4),
            'OR' => array(
              array('j.job' => 'animator'),
              array('j.job' => 'urban_leader')
            )
          ),
          'order' => 'j.hierarchy'
        ));
        $this->set(compact('emdo'));

      break;
      case 'memo':
        $pdfOptions = array(
          'footer-left' => date('d.m.Y H:i'),
          'footer-right' => '[page]',
          'footer-font-size' => 8,
          'margin-top' => '20mm',
          'margin-right' => '20mm',
          'margin-bottom' => '20mm',
          'margin-left' => '20mm'
        );
        $this->Event->contain(array(
          'Company' => array('fields' => array('name', 'class')),
          'Client' => array('fields' => array('name')),
          'ContactPeople' => array('fields' => array('full_name', 'phone')),
          'SelectedActivity' => array('Activity' => array('fields' => array('name'))),
          'SelectedFBModule' => array('FBModule' => array('fields' => array('name'))),
          'ConfirmedEventPlace' => array('Place' => array('fields' => array('name', 'zip_city')))
        ));

        $carSharingStarts = $this->Event->PlanningBoard->PlanningBoardMoment->find('all', array(
          'contain' => array('Place'),
          'joins' => array(
            array(
              'table' => 'planning_boards',
              'alias' => 'pb',
              'conditions' => array(
                'pb.id = PlanningBoardMoment.planning_board_id',
                'pb.model_id' => $id,
                'pb.model' => 'event',
                'pb.type' => 'general'
              )
            )
          ),
          'conditions' => array(
            'PlanningBoardMoment.type' => 'car_sharing_start'
          ),
          'order' => 'start'
        ));
        $this->set(compact('carSharingStarts'));
      break;
      case 'briefing':
        $pdfOptions = array(
          'header-left' => 'Bureau - 026 409 70 00',
          'header-center' => 'Aline Andrey - 077 520 73 92',
          'header-right' => 'Loris Corbaz - 079 654 67 46',
          'header-font-size' => 8,
          'footer-left' => date('d.m.Y H:i'),
          'footer-right' => '[page]',
          'footer-font-size' => 8,
          'margin-top' => '20mm',
          'margin-right' => '20mm',
          'margin-bottom' => '20mm',
          'margin-left' => '20mm'
        );
        $this->Event->contain(array(
          'Company' => array('fields' => array('name', 'class')),
          'Client' => array('fields' => array('name')),
          'Job' => array('JobBriefing')
        ));
      break;
      default:
        $pdfOptions = array();
        break;
    }

    $this->pdfConfig = array(
      'filename' => 'document.pdf',
      'download' => false,
			'options' => $pdfOptions
    );

    $event = $this->Event->findById($id);
    //debug($event);exit;

    $jobs = Configure::read('Competences.jobs');
    $this->set(compact('jobs'));

    $hierarchies = Configure::read('Competences.hierarchies');
    $this->set(compact('hierarchies'));

    $languages = Configure::read('Jobs.languages');
    $this->set(compact('languages'));

    if(!empty($event['Event']['languages'])){
      $languages = explode(',', $event['Event']['languages']);
      $event['Event']['languages'] = $languages;
    }
    $pax = 0;
    if(!empty($event['Event']['confirmed_number_of_persons'])){
      $pax = $event['Event']['confirmed_number_of_persons'];
    } else {
      if(!empty($event['Event']['min_number_of_persons'])){
        $pax = $event['Event']['min_number_of_persons'];
      }
      if(!empty($event['Event']['max_number_of_persons'])){
        $pax .= ' - ' . $event['Event']['max_number_of_persons'];
      }
    }
    $event['Event']['pax'] = $pax;
    $this->set(compact('event'));

    $this->render($mode);
  }

  public function pdf_footer($company_id){
    if($company_id == 6){
      $this->layout = 'pdf/default';
		  return true;
    } else {
      return false;
    }
  }

  public function stimbox( $respId = null ){
    $options = array(
      'contain' => array(
        'PersonInCharge' => array('fields' => array('first_name', 'last_name')),
        'Client' => array('fields' => array('name')),
        'ContactPeople' => array('fields' => array('first_name', 'last_name', 'email')),
        'ActivityEvent' => array('fields' => array(), 'Activity' => array('fields' => array('name'))),
        'Date' => array('fields' => array('date'))
      ),
      'conditions' => array(
        'ContactPeople.email <>' => null
      ),
      'fields' => array('code', 'name', 'confirmed_date', 'projected_turnover', 'crm_status', 'period_start', 'period_end'),
      'order' => array('confirmed_date')
    );
    if(empty($respId)){
      $respId = AuthComponent::user('id');
    }
    $options['conditions'][] = array('Event.resp_id' => $respId);
    $resp = $this->Event->User->read(array('first_name'), $respId);
    $this->response->download(sprintf("mailing_stimbox_%s.csv", strtolower($resp['User']['first_name'])));
    $clients = $this->Event->find('all', $options);
    // $clients = array();
    // if(!empty($data)){
    //   foreach($data as $client){
    //     $clients[$client['ContactPeople']['email']] = $client;
    //   }
    // }
    $this->set('data', $clients);
    $this->set('statuses', Configure::read('Events.crm_status'));
    $this->layout = 'ajax';
    return;
  }

}
