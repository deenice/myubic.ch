<?php

class RegionsController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'Region' => array(
				'columns' => array(
					'name',
					'type',
					'Actions' => null,
				),
				'order' => array('Region.type', 'Region.name'),
				'fields' => array('Region.id'),
				'autoData' => false
			)
		),
	);

	public function index() {
		$this->DataTable->setViewVar(array('Region'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Region']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Region']['columns']['type']['label'] = __('Type');
	}

	public function add(){
		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Region->save($this->request->data)){
				$this->Session->setFlash(__('Region has been added.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Region->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function edit( $id = null ){
		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Region->save($this->request->data)){
				$this->Session->setFlash(__('Region has been added.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Region->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		} else {
			$region = $this->Region->findById($id);
			$this->request->data = $region;
			$this->set(compact('region'));
		}
	}

  public function importCommunes(){
		exit;
    $this->loadModel('Commune');
    $communes = $this->Commune->find('all', array(
      'conditions' => array(
        'latitude <>' => null,
        'longitude <>' => null,
				'zip_name_canton <>' => null
      ),
			'fields' => array('latitude', 'longitude', 'zip_name_canton')
    ));
    foreach($communes as $commune){
			$this->Region->create();
			$this->Region->save(array(
				'Region' => array(
					'name' => $commune['Commune']['zip_name_canton'],
					'type' => 'city',
					'latitude' => $commune['Commune']['latitude'],
					'longitude' => $commune['Commune']['longitude']
				)
			));
		}
    exit;
  }

}
