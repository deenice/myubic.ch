<?php

class ActivitiesController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'Activity' => [
				'columns' => [
					'name',
					'company' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'conditions' => array(
					'company_id <>' => null
				),
				'order' => array('Activity.name' => 'asc'),
				'contain' => array('Company'),
				'fields' => array(
					'Activity.id', 'Company.name'
				),
				'autoData' => false
			]
		],
	];

	public function index() {
		$this->DataTable->setViewVar(array('Activity'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Activity']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Activity']['columns']['company']['label'] = __('Company');
	}

	public function add(){

		$this->set('companies', $this->Activity->Company->find('list'));

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Activity->create();
			if ($this->Activity->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Activity has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Activity->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Activity has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}

	}

	public function edit( $id = null ){

		$this->Activity->id = $id;

		if (!$this->Activity->exists()) {
			throw new NotFoundException(__('Invalid Activity'));
		}

		$this->set('companies', $this->Activity->Company->find('list'));

		// Prepare photos and send them to view
		$photos = $this->Activity->Document->getFiles($id, 'activity_photo');
		$docs = $this->Activity->Document->getFiles($id, 'activity_document');
		$videos = $this->Activity->Document->getFiles($id, 'activity_video');
		$documents = array();
		$documents['documents'] = $docs;
		$documents['photos'] = $photos;
		$documents['videos'] = $videos;

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Activity->create();
			if(!empty($this->request->data['PlanningBoardMoment'])){
				foreach($this->request->data['PlanningBoardMoment'] as $k => $moment){
					if(empty($moment['name'])){
						unset($this->request->data['PlanningBoardMoment'][$k]);
						if(!empty($moment['id'])){
							$this->Activity->PlanningBoardMoment->delete($moment['id']);
						}
					}
				}
			}
			if ($this->Activity->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Activity has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Activity->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Event has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$activity = $this->Activity->find('first', array(
				'conditions' => array(
					'Activity.id' => $id
				),
				'contain' => array(
					'Game' => array('Document'),
					'PlanningBoardMoment'
				)
			));

			if(!empty($activity['Game'])){
				foreach($activity['Game'] as $game){
					$documents['games'][$game['id']]['photos'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_photo');
					$documents['games'][$game['id']]['documents'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_document');
					$documents['games'][$game['id']]['videos'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_video');
				}
			}

			$this->set(compact('activity'));
			$this->set(compact('documents'));
			$this->request->data = $activity;
		}

	}

	public function view($id = null){
		if(is_numeric($id)){
			$this->Activity->id = $id;
			if (!$this->Activity->exists()) {
				throw new NotFoundException(__('Invalid Activity'));
			}
			$activity = $this->Activity->find('first', array(
				'contain' => array('Game', 'Company'),
				'conditions' => array('Activity.id' => $id)
			));
		} elseif(is_string($id)){
			$activity = $this->Activity->find('first', array(
				'contain' => array('Game', 'Company'),
				'conditions' => array('Activity.slug' => $id)
			));
		}
		$this->set(compact('activity'));

		// Prepare photos and send them to view
		$photos = $this->Activity->Document->getFiles($activity['Activity']['id'], 'activity_photo');
		$docs = $this->Activity->Document->getFiles($activity['Activity']['id'], 'activity_document');
		$videos = $this->Activity->Document->getFiles($activity['Activity']['id'], 'activity_video');
		$documents = array();
		$documents['documents'] = $docs;
		$documents['photos'] = $photos;
		$documents['videos'] = $videos;

		if(!empty($activity['Game'])){
			foreach($activity['Game'] as $game){
				$documents['games'][$game['id']]['photos'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_photo');
				$documents['games'][$game['id']]['documents'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_document');
				$documents['games'][$game['id']]['videos'] = $this->Activity->Game->Document->getFiles($game['id'], 'game_video');
			}
		}
		//debug($documents);exit;
		$this->set(compact('documents'));

		$this->loadModel('User');
		$hierarchies = Configure::read('Competences.hierarchies');
		$jobs = Configure::read('Competences.jobs');
		$data = $this->User->find('all', array(
			'joins' => array(
				array(
					'table' => 'competences',
					'alias' => 'Competence',
					'type' => 'INNER',
					'conditions' => array(
						'Competence.user_id = User.id'
					)
				)
			),
			'contain' => array(
				'Portrait'
			),
			'conditions' => array(
				'Competence.activity_id' => $activity['Activity']['id'],
				'User.role' => array('fixed', 'temporary')
			),
			'order' => 'Competence.hierarchy DESC, User.full_name',
			//'group' => 'User.id',
			'fields' => array('User.*', 'Competence.*')
		));
		$staff = array();
		$numberOfStaff = 0;
		if(!empty($data)){
			foreach($data as $user){
				$staff[$user['Competence']['job']][$user['Competence']['hierarchy']][] = $user;
				$numberOfStaff++;
			}
		}
		$this->set(compact('hierarchies'));
		$this->set(compact('jobs'));
		$this->set(compact('staff'));
		$this->set('number_of_staff', $numberOfStaff);
	}

	public function delete($id = null){
		$this->autoRender = false;
		if ($this->request->is('ajax')) {
			if($this->Activity->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		}
	}

	public function pdf($id = null){

		$this->layout = 'default';

		if (!$this->Activity->exists($id)) {
			throw new NotFoundException(__('Invalid Activity'));
		}

		$activity = $this->Activity->find('first', array(
			'conditions' => array(
				'Activity.id' => $id
			),
			'contain' => array(
				'Game' => array('Photo'),
				'Photo'
			)
		));

		$filename = $activity['Activity']['name'] . '.pdf';

		$download = true;
		if(!empty($this->request->query)){
			if(!$this->request->query['download']){
				$download = false;
			}
		}

		$this->pdfConfig = array(
			'filename' => $filename,
			'download' => $download,
			'options' => array(
				'footer-center' => '[page]',
				'footer-right' => date('d.m.Y'),
				'footer-font-size' => 8,
			)
		);

		$this->set(compact('activity'));
		$this->set('photo', (!empty($this->request->params['named']['photos']) && $this->request->params['named']['photos']) ? true : false);

	}

	public function updateNonStandardName(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$ae = $this->Activity->ActivityEvent->findById($this->request->data['activity_event_id']);
			if(!empty($ae)){
				$ae['ActivityEvent']['name'] = $this->request->data['name'];
				if($this->Activity->ActivityEvent->save($ae, array('callbacks' => false))){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

	public function updateInfoField(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$ae = $this->Activity->ActivityEvent->findById($this->request->data['id']);
			if(!empty($ae)){
				$ae['ActivityEvent'][$this->request->data['field']] = $this->request->data['value'];
				if($this->Activity->ActivityEvent->save($ae, array('callbacks' => false))){
					$this->Activity->OrderItem->updateQuantities(array(
						'order_model' => 'event',
						'order_model_id' => $ae['ActivityEvent']['event_id'],
						'item_model' => 'activity',
						'item_model_id' => $ae['ActivityEvent']['activity_id']
					));
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					$this->response->body(json_encode(array('success' => 0)));
				}
			}
		}
	}

}
