<?php

class OutfitsController extends AppController {

	public $components = array(
		'DataTable.DataTable' => array(
			'Outfit' => array(
				'columns' => array(
					'name',
					'type',
					'Actions' => null,
				),
				'order' => array('Outfit.type', 'Outfit.name'),
				'fields' => array('Outfit.id'),
				'autoData' => false
			)
		),
	);

	public function index() {
		$this->DataTable->setViewVar(array('Outfit'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['Outfit']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['Outfit']['columns']['type']['label'] = __('Type');
	}

	public function add(){
		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Outfit->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('Outfit has been added.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Outfit->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		}
	}

	public function edit( $id = null ){

		if (!$this->Outfit->exists($id)) {
			throw new NotFoundException(__('Invalid Outfit'));
		}

		if($this->request->is('post') OR $this->request->is('put')){
			if($this->Outfit->saveAssociated($this->request->data)){
				$this->Session->setFlash(__('Outfit has been added.'), 'alert', array('type' => 'success'));
				if ($this->request->data['destination'] == 'edit') {
					return $this->redirect(array('action' => 'edit', $this->Outfit->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			}
		} else {
			$this->Outfit->contain(array('OutfitItem'));
			$outfit = $this->Outfit->findById($id);
			$this->request->data = $outfit;
			$this->set(compact('outfit'));
		}
	}

	public function associations( $company_id = null ){

		$options = array(
			'contain' => array(
				'Company',
				'Activity' => array('fields' => array('id', 'name', 'slug')),
				'FBModule' => array('fields' => array('id', 'name', 'slug'))
			),
			'conditions' => array(
				'Outfit.name <>' => ''
			),
			'order' => array('company_id', 'Outfit.name')
		);

		if(!empty($company_id)){
			$options['conditions'][] = array('Outfit.company_id' => $company_id);
		}

		$outfits = $this->Outfit->find('all', $options);
		$this->set(compact('outfits'));

		$companies = $this->Outfit->Company->find('all', array(
			'joins' => array(
				array(
					'table' => 'outfits',
					'alias' => 'o',
					'conditions' => array('o.company_id = Company.id')
				)
			),
			'group' => 'Company.id'
		));
		$this->set(compact('companies'));

		$resources = array();
		$this->loadModel('Activity');
		$resources['Activity'] = $this->Activity->find('all', array(
			'conditions' => array(
				'Activity.category' => null,
				'Activity.name <>' => ''
			),
			'order' => 'Activity.name'
		));
		$this->loadModel('FBModule');
		$resources['FBModule'] = $this->FBModule->find('all', array(
			'conditions' => array(
				'FBModule.name <>' => ''
			),
			'order' => 'FBModule.name'
		));
		$this->set(compact('resources'));

		$this->set('types', Configure::read('Outfits.types'));

	}

	public function associations1(){

		$outfits = $this->Outfit->find('all', array(
			'contain' => array('Company'),
			'conditions' => array(
				'type <>' => 'default'
 			)
		));
		$this->set(compact('outfits'));

		$items = array();
		$associations = array();

		$activities = $this->Outfit->OutfitAssociation->Activity->find('all', array(
			'contain' => array('Company'),
			'conditions' => array(
				'company_id <>' => null
			),
			'fields' => array('id', 'Activity.name', 'Company.name')
		));
		foreach($activities as $activity){
			$activity['Activity']['model'] = 'activity';
			$activity['Activity']['outfits'] = $this->Outfit->OutfitAssociation->find('list', array(
				'conditions' => array(
					'model_id' => $activity['Activity']['id'],
					'model' => 'activity'
				),
				'fields' => array('id', 'outfit_id')
			));
			$items[$activity['Company']['name']][] = $activity['Activity'];
		}

		$fb_modules = $this->Outfit->OutfitAssociation->FBModule->find('all', array(
			'fields' => array('id', 'FBModule.name')
		));
		$items['Effet Gourmand'] = array();
		foreach($fb_modules as $module){
			$module['FBModule']['model'] = 'fb_module';
			$module['FBModule']['outfits'] = $this->Outfit->OutfitAssociation->find('list', array(
				'conditions' => array(
					'model_id' => $module['FBModule']['id'],
					'model' => 'fb_module'
				),
				'fields' => array('id', 'outfit_id')
			));
			$items['Effet Gourmand'][] = $module['FBModule'];
		}

		$this->set(compact('items'));


	}

	public function json(){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			$outfits = array();
			if(!empty($this->request->data['model']) && !empty($this->request->data['model_id'])){
				$outfits = $this->Outfit->find('list', array(
					'joins' => array(
						array(
							'table' => 'outfit_associations',
							'alias' => 'oa',
							'conditions' => array(
								'oa.outfit_id = Outfit.id',
								'oa.model' => $this->request->data['model'],
								'oa.model_id' => $this->request->data['model_id']
							)
						)
					),
					'conditions' => array('Outfit.type <>' => 'client'),
					'order' => 'name'
				));
			} else {
				$outfits = $this->Outfit->find('list', array(
					'conditions' => array(
						'type' => 'default'
					),
					'order' => 'name'
				));
			}
			$this->response->body(json_encode($outfits));
		}
	}

}
