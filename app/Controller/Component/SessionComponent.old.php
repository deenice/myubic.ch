<?php

class SessionComponent extends Component {

	public function setFlash($message, $type="message") {
		//If flash messages are already stored in session, fetch array
		if(CakeSession::check('flashMessages'))
			$messages = CakeSession::read('flashMessages');
		//Else, create a new empty array
		else
			$messages = array();
		//If the flash message type is not supported (only "error", "notice", "success" and "warning"), set it to "notice"
		if(!in_array($type = strtolower($type), array("error", "info", "success", "warning")))
			$type = "info";
		//Add the passed flash message to the array
		$messages[$type][] = $message;
		//Store in sessione
		CakeSession::write('flashMessages', $messages);
	}
	
}