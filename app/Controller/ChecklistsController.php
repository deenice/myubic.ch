<?php

class ChecklistsController extends AppController {

	public $components = [
		'DataTable.DataTable' => [
			'All' => [
				'model' => 'Checklist',
				'columns' => [
					'name',
					'model',
					'batches' => array(
						'useField' => false
					),
					'Actions' => null,
				],
				'contain' => array('ChecklistBatch', 'Company'),
				'fields' => array(
					'Checklist.default',
					'Checklist.personal',
					'Checklist.user_id',
					'Company.class'
				),
				'order' => array('default DESC'),
				'autoData' => false
			]
		],
	];

	public function index(){
		$this->DataTable->setViewVar(array('All'));
	}

	public function beforeFilter() {
		parent::beforeFilter();
		$this->DataTable->settings['All']['columns']['name']['label'] = __('Name');
		$this->DataTable->settings['All']['columns']['model']['label'] = __('Ideal for');
		$this->DataTable->settings['All']['columns']['batches']['label'] = __('Groups');

		$this->DataTable->settings['All']['conditions'] = array(
			'Checklist.model_id' => null,
			'OR' => array(
				array('Checklist.default' => 1),
				array('Checklist.user_id' => AuthComponent::user('id'), array('Checklist.personal' => 1))
			)
		);
	}

	public function add(){
		$companies = $this->Checklist->Company->find('list', array(
			'fields' => array(
				'Company.id', 'Company.name'
			)
		));
		$this->set(compact('companies'));
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Checklist->create();
			if ($this->Checklist->save($this->request->data)) {
				$this->Session->setFlash(__('Checklist has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Checklist->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Artist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		}
	}

	public function edit( $id = null ){

		$this->Checklist->id = $id;

		if (!$this->Checklist->exists()) {
			throw new NotFoundException(__('Invalid Checklist'));
		}

		$this->Checklist->contain(array(
			'ChecklistBatch' => array(
				'ChecklistTask'
			)
		));

		$companies = $this->Checklist->Company->find('list', array(
			'fields' => array(
				'Company.id', 'Company.name'
			)
		));
		$this->set(compact('companies'));

		if ($this->request->is('post') || $this->request->is('put')) {
			$this->Checklist->create();
			if ($this->Checklist->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('Checklist has been saved.'), 'alert', array('type' => 'success'));
				if($this->request->data['destination'] == 'edit'){
					return $this->redirect(array('action' => 'edit', $this->Checklist->id));
				} else {
					return $this->redirect(array('action' => 'index'));
				}
			} else {
				$this->Session->setFlash(__('Checklist has not been saved. Please try again.'), 'alert', array('type' => 'danger'));
			}
		} else {
			$checklist = $this->Checklist->findById($id);
			$this->set(compact('checklist'));
			$this->request->data = $checklist;
		}
	}

	public function duplicate( $id = null ){
		$this->Checklist->id = $id;
		if (!$this->Checklist->exists()) {
			throw new NotFoundException(__('Invalid Checklist'));
		}
		if($this->Checklist->assign()){
			$this->Session->setFlash(__('Checklist has been duplicated.'), 'alert', array('type' => 'success'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('Checklist has not been duplicated. Please try again.'), 'alert', array('type' => 'danger'));
			return $this->redirect(array('action' => 'index'));
		}
	}

	public function select( $id = '', $model_id = '' ){

		if($this->request->is('ajax')){
			$this->autoRender = false;
		}

		if(!empty($id)){
			$this->Checklist->id = $id;
			if($this->Checklist->assign($model_id)){
				if($this->request->is('ajax')){
					$this->response->body(json_encode(array('success' => 1)));
				} else {
					return true;
				}
			} else {
				if($this->request->is('ajax')){
					$this->response->body(json_encode(array('success' => 0)));
				} else {
					return false;
				}
			}
		} else {
			if($this->request->is('ajax')){
				$this->response->body(json_encode(array('success' => 0)));
			} else {
				return false;
			}
		}
	}

	public function preview( ){
		$this->Checklist->contain(array(
			'Company',
			'ChecklistBatch' => array(
				'ChecklistTask'
			)
		));
		$checklists = $this->Checklist->find('all', array(
			'conditions' => array(
				'model' => 'event',
				'model_id' => null,
				'OR' => array(
					array('default' => 1),
					array('user_id' => AuthComponent::user('id'), 'personal' => 1)
				)
			),
			'order' => 'default DESC'
		));
		$this->set(compact('checklists'));
	}

	public function batches_tasks( $id = null ){
		if($this->request->is('ajax')){
			$this->Checklist->contain(array(
				'ChecklistBatch' => array(
					'ChecklistTask'
				)
			));
			$checklist = $this->Checklist->findById($id);
			$this->set(compact('checklist'));
			if(!empty($this->request->data['last_open_batch'])){
				$this->set('last_open_batch', preg_replace('/\D/', '', $this->request->data['last_open_batch']));
			} else {
				$this->set('last_open_batch', 0);
			}
		}
	}

	public function calendar(){
		$view = empty($this->request->data['view']) ? '' : $this->request->data['view'];
		$ts = strtotime(empty($this->request->data['date']) ? time() : $this->request->data['date']);
		$start = (date('w', $ts) == 0) ? strtotime('yesterday', $ts) : strtotime('last sunday - 1 day', $ts);
		$end = (date('w', $ts) == 0) ? strtotime('tomorrow', $ts) : strtotime('next monday + 3 days', $ts);
		$start_date = date('Y-m-d', $start);
		$end_date = date('Y-m-d', $end);
		$options = array();

		if ($view == 'month') {
			$month = date('m', $ts);
			$days = date('t', $ts);
			$start_date = date(sprintf('Y-%s-01', $month));
			$end_date = date(sprintf('Y-%s-%s', $month, $days));
			$end_date = date('Y-m-d', strtotime($end_date.'+ 7 days'));
		}

		$data = $this->Checklist->ChecklistBatch->ChecklistTask->find('all', array(
			'contain' => array(
				'ChecklistBatch' => array(
					'Checklist' => array(
						'Event' => array('Client')
					)
				)
			),
			'joins' => array(
				array(
					'table' => 'checklist_batches',
					'alias' => 'cb',
					'conditions' => array(
						'cb.id = ChecklistTask.checklist_batch_id'
					)
				),
				array(
					'table' => 'checklists',
					'alias' => 'c',
					'conditions' => array(
						'c.id = cb.checklist_id'
					)
				)
			),
			'conditions' => array(
				'due_date <>' => null,
				'ChecklistTask.done' => 0,
				'c.user_id' => AuthComponent::user('id'),
				'due_date >=' => $start_date,
				'due_date <=' => $end_date
			),
			'order' => 'cb.weight, ChecklistTask.weight'
		));
		$tasks = array();
		if(!empty($data)){
			foreach($data as $k => $task){
				$html = '';
				$html .= '<strong>'.$task['ChecklistBatch']['name'].'</strong>';
				$html .= '<span><i class="fa fa-square-o"></i> ';
				$html .= '<a href="'.Router::url(array('controller' => 'checklist_tasks', 'action' => 'modal', $task['ChecklistTask']['id'])).'" data-toggle="modal" data-target="#modal-calendar-task">'.$task['ChecklistTask']['name'].'</a></span>';
				$html .= '<br />';
				if(!empty($task['ChecklistBatch']['Checklist']['Event']['code_name'])){
					$smallLinkName = $task['ChecklistBatch']['Checklist']['Event']['code_name'];
				} else {
					$smallLinkName = $task['ChecklistBatch']['Checklist']['Event']['name'];
				}
				$html .= '<small><a href="'.Router::url(array('controller' => 'events', 'action' => 'work', $task['ChecklistBatch']['Checklist']['Event']['id'])).'" target="_blank">'.$smallLinkName.'</a></small>';
				$html .= '<br />';
				$html .= '<small>'.$task['ChecklistBatch']['Checklist']['Event']['Client']['name'].'</small>';
				$tasks[$k] = array(
					'id' => $task['ChecklistTask']['id'],
					'html' => $html,
					'start' => $task['ChecklistTask']['due_date'],
					'style' => 'border-left-color:' . $task['ChecklistBatch']['Checklist']['Event']['calendar_color'],
					'allDay' => true
				);
			}
		}
		$this->set('_json', $tasks);
	}

	public function delete( $id = '' ){
		if($this->request->is('ajax')){
			$this->autoRender = false;
			if($this->Checklist->delete($id)){
				$this->response->body(json_encode(array('success' => 1)));
			} else {
				$this->response->body(json_encode(array('success' => 0)));
			}
		} else {
			if($this->Checklist->delete($id)){
				$this->Session->setFlash(__('Checklist has been deleted.'), 'alert', array('type' => 'success'));
			} else {
				$this->Session->setFlash(__('Checklist has not been deleted.'), 'alert', array('type' => 'warning'));
			}
			return $this->redirect(array('action' => 'index'));
		}
	}

}
