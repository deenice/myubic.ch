<?php 

class AppError extends ErrorHandler {
	public function error404($params, $messages) {
		$this->Session->flash($messages);
		$this->controller->redirect(array('controller'=>'welcome', 'action'=>'index'));
		parent::error404($params);
	}
}