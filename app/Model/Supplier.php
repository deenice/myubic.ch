<?php

class Supplier extends AppModel {

	public $useTable = 'clients';

	public $actsAs = array(
    'Containable',
    'Icing.Versionable' => array(
      'contain' => array('ContactPeople', 'Document')
    )
  );

  public $virtualFields = array(
  	'zip_city' => 'CONCAT(Supplier.zip, " ", Supplier.city)',
		'invoice_zip_city' => 'CONCAT(Supplier.invoice_zip, " ", Supplier.invoice_city)',
  	'name_address' => 'CONCAT(Supplier.name, " - ", Supplier.address, ", ", Supplier.zip, " ", Supplier.city)'
  );

	public $hasMany = array(
		'StockItem',
		'OrderItem',
		'Document' => array(
			'foreignKey' => 'parent_id'
		),
		'ContactPeople' => array(
			'order' => 'ContactPeople.created DESC'
		),
		'Note' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'client'
			)
		)
	);

	public $hasOne = array(
		'Logo' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Logo.category' => 'supplier_logo'
			),
			'order' => 'Logo.modified DESC'
		)
	);

	public $hasAndBelongsToMany = array('Company');

	function beforeFind($query) {
    $query['conditions'][] = array('type' => 'supplier');
    return $query;
  }

}
