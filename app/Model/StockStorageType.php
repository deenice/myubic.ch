<?php

class StockStorageType extends AppModel {

  public $actsAs = array('Containable');

	public $hasMany = array('StockItem');

}
