<?php

class History extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'StockOrder',
		'Event'
	);

}