<?php

class Mission extends AppModel{

	public $useTable = 'events';
	public $actsAs = array('Containable');

  public $belongsTo = array(
    'Company',
    'User',
    'PersonInCharge' => array(
      'className' => 'User',
      'foreignKey' => 'resp_id'
    )
  );

	function beforeFind($query) {
    $query['conditions'][] = array('type' => 'mission');
    return $query;
  }

  public function generateCalendarEvent(){

		App::import('Model', 'Event');
		$Event = new Event();

    $this->contain(array('Company', 'PersonInCharge'));
    $mission = $this->findById($this->id);
    $data = array();
    $data['id'] = $mission['Mission']['id'];
    $data['title'] = $mission['Mission']['name'];
    $data['start'] = date('c', strtotime($mission['Mission']['confirmed_date'] . ' ' . $mission['Mission']['start_hour']));
    $data['end'] = date('c', strtotime($mission['Mission']['confirmed_date'] . ' ' . $mission['Mission']['end_hour']));
    $data['allDay'] = false;
    $data['className'] = 'mission';
    $data['url'] = Router::url(array('controller' => 'events', 'action' => 'work', $mission['Mission']['id']), true);

    $Event->id = $this->id;
    $numberOfJobs = $Event->getNumberOfJobs();
    $numberOfFilledJobs = $Event->getNumberOfJobs('filled');

    $data['html'] = '<p>';
    $data['html'] .= '<i class="fa fa-crosshairs"></i> ';
    $data['html'] .= date('H:i', strtotime($mission['Mission']['start_hour']));
    $data['html'] .= ' - ' . date('H:i', strtotime($mission['Mission']['end_hour']));
    $data['html'] .= '<br /><strong>'.$mission['Mission']['name'].'</strong>';
    $data['html'] .= '<br>';
    $data['html'] .= $numberOfJobs ? __('<i class="fa fa-user"></i> %s/%s', $numberOfFilledJobs, $numberOfJobs) : __('No job defined');
    $data['html'] .= sprintf('<span class="btn btn-xs btn-company btn-company-%s"></span>', $mission['Company']['class']);
    $data['html'] .= '</p>';

    return $data;
  }

}
