<?php 

class WarehouseStoragePoint extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'WarehouseCell',
		'WarehousePassage' => array(
			'foreignKey' => 'adjacency_passage_id'
		),
		'Warehouse'
	);
	public $hasMany = array('StockItem');

}