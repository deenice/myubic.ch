<?php

class PlanningResource extends AppModel {

  public $actsAs = array(
    'Containable'
  );

  public $belongsTo = array(
    'PlanningBoardMoment',
    'Activity' => array('foreignKey' => 'resource_id', 'type' => 'inner'),
    'Client' => array('foreignKey' => 'resource_id'),
    'FBModule' => array('foreignKey' => 'resource_id'),
    'Extra' => array(
      'foreignKey' => 'resource_id'
    ),
  );

  public function beforeSave($options = array()){
    if(empty($this->data['PlanningResource']['slug']) && !empty($this->data['PlanningResource']['resource_id'])){
      $this->data['PlanningResource']['slug'] = $this->getSlug($this->data['PlanningResource']['resource_id'], $this->data['PlanningResource']['resource_model']);
    }
  }

  public function getSlug( $id, $model ){
    switch($model){
      case 'manager':
      case 'extra':
      case 'user':
        App::import('Model', 'User');
    		$User = new User();
        $resource = $User->read(array('full_name'), $id);
        $slug = $this->slug($resource['User']['full_name']);
      break;
      case 'activity':
        App::import('Model', 'Activity');
    		$Activity = new Activity();
        $resource = $Activity->read(array('name', 'slug'), $id);
        $slug = !empty($resource['Activity']['slug']) ? $resource['Activity']['slug'] : $this->slug($resource['Activity']['name']);
      break;
      case 'fb_module':
        App::import('Model', 'FBModule');
    		$FBModule = new FBModule();
        $resource = $FBModule->read(array('name', 'slug'), $id);
        $slug = !empty($resource['FBModule']['slug']) ? $resource['FBModule']['slug'] : $this->slug($resource['FBModule']['name']);
      break;
      case 'vehicle':
        App::import('Model', 'Vehicle');
    		$Vehicle = new Vehicle();
        $resource = $Vehicle->read(array('name'), $id);
        $slug = $this->slug($resource['Vehicle']['name']);
      break;
      case 'place':
        App::import('Model', 'Place');
    		$Place = new Place();
        $resource = $Place->read(array('name'), $id);
        $slug = $this->slug($resource['Place']['name']);
      break;
      case 'client':
        App::import('Model', 'Client');
    		$Client = new Client();
        $resource = $Client->read(array('name'), $id);
        $slug = $this->slug($resource['Client']['name']);
      break;
      default:
        $slug = 'XX';
      break;
    }
    return strtoupper($slug);
  }

  private function slug( $string ){
    $string = Inflector::slug($string, ' ');
    preg_match_all('/\b\w/', iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $string), $match);
    return $slug = implode('', $match[0]);
  }

  public function remove($event = '', $model = '', $modelId = ''){
    $resources = $this->find('all', array(
      'joins' => array(
        array(
          'table' => 'planning_board_moments',
          'alias' => 'pbm',
          'conditions' => array(
            'pbm.id = PlanningResource.planning_board_moment_id'
          )
        ),
        array(
          'table' => 'planning_boards',
          'alias' => 'p',
          'conditions' => array(
            'p.id = pbm.planning_board_id',
            'p.model' => 'event',
            'p.model_id' => $event
          )
        )
      ),
      'conditions' => array(
        'PlanningResource.resource_model' => $model,
        'PlanningResource.resource_id' => $modelId
      ),
      'group' => 'PlanningResource.id'
    ));
    if(!empty($resources)){
      foreach($resources as $resource){
        $this->delete($resource['PlanningResource']['id']);
      }
    }
  }
}
