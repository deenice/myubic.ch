<?php

class Outfit extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'OutfitItem' => array(
			'order' => 'weight',
      'dependent' => true
		),
		'OutfitAssociation',
    'Job'
	);

	public $belongsTo = array('Company');

  public $hasAndBelongsToMany = array(
    'Activity' => array(
      'joinTable' => 'outfit_associations',
      'foreignKey' => 'outfit_id',
      'associationForeignKey' => 'model_id',
      'unique' => true,
      'conditions' => array(
        'model' => 'activity'
      ),
      'order' => 'name'
    ),
    'FBModule' => array(
      'joinTable' => 'outfit_associations',
      'foreignKey' => 'outfit_id',
      'associationForeignKey' => 'model_id',
      'unique' => true,
      'conditions' => array(
        'model' => 'fb_module'
      ),
      'order' => 'name'
    )
  );

	// public function assign( $options = array() ){
  //   $this->contain(array(
  //     'OrderGroup' => array(
  //       'OrderItem'
  //     )
  //   ));
  //   $default = $this->findById($this->id);
  //   $default['Order']['id'] = null;
  //   $default['Order']['name'] = empty($options['order_model_id']) ? $default['Order']['name'] . ' - copy' : $default['Order']['name'];
  //   $default['Order']['model'] = $options['order_model'];
  //   $default['Order']['model_id'] = $options['order_model_id'];
  //   $default['Order']['parent'] = $default['Order']['id']; // we need to keep the original id to retrieve associations
  //   $this->create();
  //   if(!$this->save($default)){
  //     return false;
  //   }
  //   foreach($default['OrderGroup'] as $k => $group){
  //     $default['OrderGroup'][$k]['id'] = null;
  //     $default['OrderGroup'][$k]['order_id'] = $this->id;
  //     $this->OrderGroup->create();
  //     if(!$this->OrderGroup->save($default['OrderGroup'][$k])){
  //       return false;
  //     }
  //     foreach($group['OrderItem'] as $kk => $item){
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['id'] = null;
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['order_group_id'] = $this->OrderGroup->id;
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['model_id'] = !empty($options['item_model_id']) ? $options['item_model_id'] : null;
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['model'] = !empty($options['item_model']) ? $options['item_model'] : null;
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['default_quantity'] = $item['default_quantity'];
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['quantity'] = $item['default_quantity'];
  //       $default['OrderGroup'][$k]['OrderItem'][$kk]['editable'] = 0;
  //       $this->OrderGroup->OrderItem->create();
  //       if(!$this->OrderGroup->OrderItem->save($default['OrderGroup'][$k]['OrderItem'][$kk])){
  //         return false;
  //       }
  //     }
  //   }
  //   return true;
  // }

}
