<?php

class StockLocation extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'OrderItem',
		'StockItem'
	);

}
