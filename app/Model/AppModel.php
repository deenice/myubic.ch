<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    public $recursive = -1;
    //public $useDbConfig = 'prod';


    function find($conditions = null, $fields = array(), $order = null, $recursive = null) {
        $doQuery = true;
        // check if we want the cache
        if (!empty($fields['cache'])) {
            $cacheConfig = null;
            // check if we have specified a custom config
            if (!empty($fields['cacheConfig'])) {
                $cacheConfig = $fields['cacheConfig'];
            }
            $cacheName = $this->name . '-' . $fields['cache'];
            // if so, check if the cache exists
            $data = Cache::read($cacheName, $cacheConfig);
            if ($data == false) {
                $data = parent::find($conditions, $fields,
                    $order, $recursive);
                Cache::write($cacheName, $data, $cacheConfig);
            }
            $doQuery = false;
        }
        if ($doQuery) {
            $data = parent::find($conditions, $fields, $order,
                $recursive);
        }
        return $data;
    }

	public function saveAssociated($data = null, $options = array()) {

        if(!empty($data['Material'])){
            foreach($data['Material'] as $item){
                if($item['id']) $tmp[] = $item['id'];
            }
            if(!empty($tmp))$materials = implode(',',$tmp);
            if(!empty($materials))$data['Place']['materials'] = $materials;
        }

        if(in_array($this->name, array('Place', 'User', 'Configuration', 'Artist', 'Myjeu'))){
            $data = $this->Tag->prepareData($data, 'User', 'languages', 'language', false);
            $data = $this->Tag->prepareData($data, 'User', 'availabilities', 'availability');
            $data = $this->Tag->prepareData($data, 'User', 'talents', 'talent');
            $data = $this->Tag->prepareData($data, 'User', 'section', 'section');
            $data = $this->Tag->prepareData($data, 'Place', 'ideal_for', 'ideal_for', false);
            $data = $this->Tag->prepareData($data, 'Place', 'materials', 'material');
            $data = $this->Tag->prepareData($data, 'Configuration', 'configuration_type', 'configuration_type');
            $data = $this->Tag->prepareData($data, 'Artist', 'tags', 'artist');
            $data = $this->Tag->prepareData($data, 'Myjeu', 'tags', 'myjeu');
        }

        foreach ($data as $alias => $modelData) {
            if (!empty($this->hasAndBelongsToMany[$alias])) {
                $habtm = array();
                $Model = ClassRegistry::init($this->hasAndBelongsToMany[$alias]['className']);
                foreach ($modelData as $modelDatum) {
                    if (empty($modelDatum['id'])) {
                        $Model->create();
                    }
                    $Model->save($modelDatum);
                    $habtm[] = empty($modelDatum['id']) ? $Model->getInsertID() : $modelDatum['id'];
                }
                $data[$alias] = array($alias => $habtm);
            }
        }
        return parent::saveAssociated($data, $options);
    }

    public function nextId() {
        $result = $this->query("SELECT Auto_increment FROM information_schema.tables AS NextId  WHERE table_name='$this->useTable' AND table_schema='$this->schemaName'");
        return $result[0]['NextId']['Auto_increment'];
    }

}
