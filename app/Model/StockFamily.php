<?php

class StockFamily extends AppModel {

  public $useTable = 'stock_families';

	public $actsAs = array('Containable');

	public $belongsTo = array('StockCategory', 'StockSection', 'Company');

	public $hasMany = array('StockItem');

	public $virtualFields = array(
    	'code_name' => 'CONCAT(StockFamily.code, " - ", StockFamily.name)'
    );

	// public $validate = array(
	// 	'name' => array(
	// 		'unique' => array(
	// 			'rule' => 'isUnique',
	// 			'message' => "This family name is already taken."
	// 		)
	// 	)
	// );

}
