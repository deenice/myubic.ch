<?php

class Company extends AppModel {

  public $actsAs = array('Containable');

  public $hasMany = array('Event', 'Activity', 'StockCategory', 'StockFamily', 'StockSection', 'StockItem', 'Checklist');

  public $hasAndBelongsToMany = array('Client', 'User');

  public $hasOne = array(
    'Logo' => array(
      'className' => 'Document',
      'foreignKey' => 'parent_id',
      'conditions' => array(
        'Logo.category' => 'company_logo'
      ),
      'order' => 'Logo.modified DESC'
    )
  );

}
