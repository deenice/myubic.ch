<?php

class Competence extends AppModel {

	public $actsAs = array('Containable');
	public $belongsTo = array(
		'User',
		'Activity',
		'FBModule' => array(
			'foreignKey' => 'fb_module_id'
		),
		'Worker' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);

}
