<?php

class TagUser extends AppModel {

	public $useTable = 'tags_users';
	public $belongsTo = array('Tag', 'User');

}
