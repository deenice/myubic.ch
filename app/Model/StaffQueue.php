<?php

class StaffQueue extends AppModel {

	public $actsAs = array('Containable');

    public $belongsTo = array('User', 'Job');

}