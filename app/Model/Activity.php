<?php

class Activity extends AppModel {

	public $actsAs = array('Containable');
	public $belongsTo = array('Place', 'Company', 'User');
	public $hasMany = array(
		'Competence',
		'Job',
		'Game',
    'Document' => array(
      'foreignKey' => 'parent_id'
    ),
		'Photo' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Photo.category' => 'activity_photo'
			)
		),
		'ActivityEvent',
		'ActivityUnavailability',
		'OrderAssociation' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'activity'
			)
		),
		'OrderItem' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'OrderItem.model' => 'activity'
			)
		),
    'PlanningResource' => array(
      'foreignKey' => 'resource_id',
      'conditions' => array(
        'PlanningResource.resource_model' => 'activity'
      )
    ),
		'OutfitAssociation' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'activity'
			)
		),
		'PlanningBoardMoment' => array(
			'foreignKey' => 'default_id',
			'conditions' => array(
				'PlanningBoardMoment.type' => 'activity'
			),
			'order' => 'weight'
		)
	);
	public $hasAndBelongsToMany = array('Event');

	public function afterSave($created, $options = array()){

		$hierarchies = Configure::read('Competences.hierarchies');

		if($created){
			$users = $this->User->TagUser->find('all', array(
				'conditions' => array(
					'tag_id' => 173
				)
			));
			if($this->data['Activity']['company_id'] == 2){ // UBIC
				$job1 = 'animator';
				$job2 = 'animator_facilitator';
				$name1 = sprintf('Animation Animateur %s %s', $this->data['Activity']['name'], $hierarchies[1]);
				$name2 = sprintf('Animation Aide animateur %s %s', $this->data['Activity']['name'], $hierarchies[1]);
			}
			if($this->data['Activity']['company_id'] == 6){ // UG
				$job1 = 'urban_leader';
				$job2 = 'urban_coach';
				$name1 = sprintf('Animation Urban Leader %s %s', $this->data['Activity']['name'], $hierarchies[1]);
				$name2 = sprintf('Animation Urban Coach %s %s', $this->data['Activity']['name'], $hierarchies[1]);
			}
			foreach($users as $data){

				$competence = array(
					'name' => $name1,
					'job' => $job1,
					'sector' => 'animation',
					'hierarchy' => 1,
					'activity_id' => $this->id,
					'user_id' => $data['TagUser']['user_id']
				);

				$this->User->Competence->create();
				$this->User->Competence->save($competence);
				$competence['name'] = $name2;
				$competence['job'] = $job2;
				$this->User->Competence->create();
				$this->User->Competence->save($competence);
			}
		}

	}

}
