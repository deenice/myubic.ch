<?php

class Game extends AppModel {

	public $actsAs = array('Containable');
	public $belongsTo = array('Activity');
	public $hasMany = array(
		'Document' => array(
			'foreignKey' => 'parent_id'
		),
		'Photo' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Photo.category' => 'game_photo'
			)
		)
	);

}
