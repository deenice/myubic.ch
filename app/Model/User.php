<?php

class User extends AppModel {

	public $actsAs = array(
		'Containable',
		'Icing.Versionable' => array(
			'contain' => array('Tag', 'Availabilities', 'Talents', 'Section', 'Module', 'Document', 'Portrait')
		)
	);

	//public $virtualFields = array('full_name' => sprintf('CONCAT(%s.first_name, " ", %s.last_name)'));
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['full_name'] = sprintf('CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias);
		$this->virtualFields['zip_city'] = sprintf('CONCAT(%s.zip, " ", %s.city)', $this->alias, $this->alias);
	}

	public $hasAndBelongsToMany = array(
		'Tag',
		'Availabilities' => array(
			'className' => 'Tag',
			'conditions' => array(
				'category' => 'availability'
			)
		),
		'Talents' => array(
			'className' => 'Tag',
			'conditions' => array(
				'category' => 'talent'
			)
		),
		'Section' => array(
			'className' => 'Tag',
			'conditions' => array(
				'category' => 'section'
			)
		),
		'Module',
		'Company',
		'Job'
	);
	public $hasMany = array(
		'Document' => array(
			'foreignKey' => 'parent_id'
		),
		'Place',
		'Event',
		'Search',
		'Activity',
		'Competence' => array('order' => 'sector, hierarchy'),
		'ValidatedUser' => array(
			'className' => 'User'
		),
		//'Job',
		'Options',
		'Portrait' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Portrait.category' => 'user_thumbnail'
			),
			'order' => 'Portrait.modified DESC',
			'limit' => 1
		),
		'ModuleUser',
		'StaffQueue' => array(
			'order' => 'StaffQueue.created DESC'
		),
		'Message',
		'TagUser',
		'Sections' => array(
			'className' => 'TagUser',
			'conditions' => 'Sections.tag_id IN (173,174,175)'
		),
		'ScheduleDay' => array(
		  'order' => 'day, weight'
		),
		'MondayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'MondayAvailability.day' => 1
		  )
		),
		'TuesdayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'TuesdayAvailability.day' => 2
		  )
		),
		'WednesdayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'WednesdayAvailability.day' => 3
		  )
		),
		'ThursdayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'ThursdayAvailability.day' => 4
		  )
		),
		'FridayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'FridayAvailability.day' => 5
		  )
		),
		'SaturdayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'SaturdayAvailability.day' => 6
		  )
		),
		'SundayAvailability' => array(
		  'className' => 'ScheduleDay',
		  'order' => 'weight',
		  'conditions' => array(
			'SundayAvailability.day' => 7
		  )
		),
		'UserUnavailability',
		'JobUser' => array(
      'dependent' => true
    ),
		'VehicleReservation',
		'Wish'
	);
	public $belongsTo = array(
		'ValidatedBy' => array(
			'className' => 'User',
			'foreignKey' => 'validated_by'
		),
		//'Group'
	);

	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A username is required.'
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => "This username is already taken."
			)
		),
		'password' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'A password is required.'
			)
		),
		'email' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => "This email has been already stored."
			)
		)
	);

	/*public function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		if (isset($this->data['User']['group_id'])) {
			$groupId = $this->data['User']['group_id'];
		} else {
			$groupId = $this->field('group_id');
		}
		if (!$groupId) {
			return null;
		}
		return array('Group' => array('id' => $groupId));
	}

	public function bindNode($user) {
		return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}*/

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}

	public function afterSave($created = false, $options = array()){
		if($created){
	  	$days = array('monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6, 'sunday' => 7);
	  	$weights = Configure::read('Hours.weights');
	  	$hours = range(0,23);
			foreach($days as $day){
			  foreach($hours as $hour){
				$s = array('ScheduleDay' => array(
					'user_id' => $this->id,
					'day' => $day,
					'hour' => $hour,
					'weight' => $weights[$hour],
					'availability' => 'unknown'
				));
				$this->ScheduleDay->create();
				$this->ScheduleDay->save($s);
			  }
			}
			if($this->data[$this->alias]['role'] == 'fixed'){
				$this->saveField('group', 'editor');
			}
			if($this->data[$this->alias]['role'] == 'temporary'){
				$this->saveField('group', 'user');
			}
			$this->addFBCompetences();
		}
	}

	public function addFBCompetences() {
		$taguser = $this->TagUser->find('first', array(
			'conditions' => array(
				'tag_id' => 175,
				'user_id' => $this->id
			)
		));
		if(!empty($taguser)){
			$modules = $this->Competence->FBModule->find('all', array(
				'conditions' => array(
					'with_competences' => 1
				)
			));
			foreach($modules as $module){
				$competence = array(
					'name' => sprintf('F&B Test %s', $module['FBModule']['name']),
					'job' => 'test',
					'sector' => 'fb',
					'fb_module_id' => $module['FBModule']['id'],
					'user_id' => $this->id
				);
				$this->Competence->create();
				$this->Competence->save($competence);
			}
		}

	}

	public function available($day){

		$availabilitiesTagsByDay = Configure::read('Users.availabilitiesTagsByDay');
		$availabilitiesTags = $availabilitiesTagsByDay[$day];

		$options['joins'] = array(
			array(
				'table' => 'tags_users',
				'alias' => 'TagsUsers',
				'type' => 'INNER',
				'foreignKey' => false,
				'conditions' => array(
					'TagsUsers.user_id = User.id',
				)
			),
			array('table' => 'tags',
				'alias' => 'Availabilities',
				'type' => 'INNER',
				'conditions' => array(
					'Availabilities.id = TagsUsers.tag_id',
					'Availabilities.category = "availability"'
				)
			)
		);
		$options['conditions'][] = array('Availabilities.id IN' => $availabilitiesTags);
		$options['conditions'][] = array('User.role <>' => 'retired');
		$options['contain'] = array(
			'Document.category = "user_thumbnail"',
			'Availabilities',
			'Section'
		);
		$options['group'] = array('User.id');
		$options['order'] = array('User.full_name');
		$options['cache'] = 'availabilities_' . $day . date('W');
		$options['cacheConfig'] = 'short';

		return $this->find('all', $options);
	}

	public function hasRights($controller, $action){
		$everyone = array('everyone');
		$fixed = array('admin', 'editor', 'festiloc', 'mg');
		$top = array('admin', 'editor');
		$mg = array('admin', 'editor', 'mg');

		switch($controller){
			case 'users':
			if($action == 'edit'){
				$groups = $everyone;
			}
			break;
			case 'places':
			break;
			case 'events':
			break;
			case 'activities':
			break;
			case 'clients':
			break;
			case 'search':
			break;
			case 'me':
			$groups = $everyone;
			break;
			case 'stock_orders':
			case 'stock_items':
			break;
			case 'dashboard':
			break;
			case 'cache':
			$groups = $fixed;
			break;
			default:
			break;
		}
		if(!isset($groups)){
			switch($action){
				case 'add':
				case 'edit':
				case 'index':
				case 'view':
				case 'delete':
				$groups = $fixed;
				break;
				default:
				$groups = $everyone;
				break;
			}
		}

		if($this->field('group') == 'superadmin'){
			return true;
		}
		elseif(in_array('everyone', $groups)){
			return true;
		}
		elseif(in_array($this->field('group'), $groups)){
			return true;
		} else {
			return false;
		}
	}

	public function getRole(){
		return $this->field('role');
	}

	public function getGroup(){
		return $this->field('group');
	}



	// public function customSearchUser($column, $searchTerm, $columnSearchTerm, $config) {
	//	 if($columnSearchTerm){
	//		 $config->conditions[] = array(
	//			 array('TagUser.tag_id' => 173)
	//		 );
	//	 }
	//	 return $config;
	// }


	public function getAvailability($jobId){

		$this->contain('UserUnavailability');
		$user = $this->findById($this->id);
	  $job = $this->Job->findById($jobId);
	  $startHour = date('G', strtotime($job['Job']['start_time']));
	  $endHour = date('G', strtotime($job['Job']['end_time']));
	  $event = $this->Job->Event->findById($job['Job']['event_id']);

	  $jobDay = date('N', strtotime($event['Event']['confirmed_date']));
	  $prevJobDay = ($jobDay - 1 < 1) ? 7 : $jobDay - 1;
		$nextJobDay = ($jobDay + 1 > 7) ? 0 : $jobDay + 1;

	  if($startHour <= $endHour){ // same day
			$schedules = $this->ScheduleDay->find('all', array(
			  'conditions' => array(
					'ScheduleDay.day' => $jobDay,
					'ScheduleDay.hour BETWEEN ? AND ?' => array($startHour, $endHour),
					'ScheduleDay.user_id' => $this->id
			  ),
				'order' => 'ScheduleDay.hour'
			));
	  } elseif($startHour > $endHour){ // 2 days
			$schedules = $this->ScheduleDay->find('all', array(
			  'conditions' => array(
					'ScheduleDay.user_id' => $this->id,
					'OR' => array(
						  array('ScheduleDay.day' => $jobDay, 'ScheduleDay.hour >=' => $startHour),
						  array('ScheduleDay.day' => $nextJobDay, 'ScheduleDay.hour <=' => $endHour)
						)
			  ),
				'order' => 'ScheduleDay.hour'
			));
	  }
		if(!empty($schedules)){
			$maxScore = sizeof($schedules) * 2;
			$score = 0;
			foreach($schedules as $schedule){
			  if($schedule['ScheduleDay']['availability'] == 'yes'){
					$score += 2;
			  }
			  if($schedule['ScheduleDay']['availability'] == 'no'){
					$score += 0;
			  }
			  if($schedule['ScheduleDay']['availability'] == 'maybe' || $schedule['ScheduleDay']['availability'] == 'unknown'){
					$score += 1;
			  }
			}
			$percent = $score / $maxScore * 100;
			if($percent >= 80){
			  $output['availability'] = 'yes';
			} elseif($percent > 30 AND $percent < 80){
			  $output['availability'] = 'maybe';
			} else {
			  $output['availability'] = 'no';
			}
	  } else {
			$output['availability'] = 'unknown';
	  }
		$output['User']['full_name'] = $user['User']['full_name'];

		$unavailability = $this->UserUnavailability->find('first', array(
			'conditions' => array(
				'UserUnavailability.user_id' => $this->id,
				'UserUnavailability.date' => $event['Event']['confirmed_date'],
				'UserUnavailability.model_id <>' => $jobId
			),
			'contain' => array(
				'Job' => array('Event' => array('Client'))
			)
		));
		if(!empty($unavailability)){
			$output['data'] = $unavailability;
			if(in_array($unavailability['UserUnavailability']['status'], array('not_available', 'enrolled', 'interested'))){
				$output['availability'] = 'no';
			}
		}

	  return $output;
	}

	public function updateScheduleDays(){
	  $days = array('monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6, 'sunday' => 7);
	  $weights = Configure::read('Hours.weights');
	  $this->contain('Availabilities');
	  $user = $this->findById($this->id);
	  if(!empty($user['Availabilities'])){
		foreach($user['Availabilities'] as $item){
		  $availability = explode('_', $item['value']);
		  if($availability[0] == 'on'){
				continue;
		  }
		  $day = $days[$availability[0]];
		  if($availability[1] == 'morning'){
				$hours = range(4,11);
		  }
		  if($availability[1] == 'afternoon'){
				$hours = range(12,16);
		  }
		  if($availability[1] == 'morning'){
				$hours = array_merge(range(17,23), range(0,3));
		  }
		  if(!empty($hours)){
				foreach($hours as $hour){
				  $exists = $this->ScheduleDay->findByDayAndHourAndUserId($day, $hour, $user['User']['id']);
				  $s = array('ScheduleDay' => array(
					'id' => empty($exists) ? '' : $exists['ScheduleDay']['id'],
					'user_id' => $user['User']['id'],
					'day' => $day,
					'hour' => $hour,
					'availability' => $availability[2],
					'weight' => $weights[$hour]
				  ));
				  $this->ScheduleDay->create();
				  $this->ScheduleDay->save($s);
				}
		  }
		}
		return true;
	  } else {
			// add unknown availabilities
			$user = $this->findById($this->id);
	  	$days = array('monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6, 'sunday' => 7);
	  	$weights = Configure::read('Hours.weights');
	  	$hours = range(0,23);
			foreach($days as $day){
			  foreach($hours as $hour){
				$s = array('ScheduleDay' => array(
					'user_id' => $user['User']['id'],
					'day' => $day,
					'hour' => $hour,
					'weight' => $weights[$hour],
					'availability' => 'unknown'
				));
				$this->ScheduleDay->create();
				$this->ScheduleDay->save($s);
			  }
			}
			return true;
	  }
	}

	public function addUnknownAvailabilities(){
	  $this->contain('ScheduleDay');
	  $user = $this->findById($this->id);
	  $days = array('monday' => 1, 'tuesday' => 2, 'wednesday' => 3, 'thursday' => 4, 'friday' => 5, 'saturday' => 6, 'sunday' => 7);
	  $weights = Configure::read('Hours.weights');
	  $hours = range(0,23);
	  if(!empty($user['ScheduleDay'])){
		foreach($days as $day){
		  foreach($hours as $hour){
				$exists = $this->ScheduleDay->findByDayAndHourAndUserId($day, $hour, $this->id);
				if(empty($exists)){
				  $s = array('ScheduleDay' => array(
						'user_id' => $user['User']['id'],
						'day' => $day,
						'hour' => $hour,
						'weight' => $weights[$hour],
						'availability' => 'unknown'
				  ));
				  $this->ScheduleDay->create();
				  $this->ScheduleDay->save($s);
				}
		  }
		}
		return true;
	  } else {
		return false;
	  }
	}

	public function isEnrolledOnJob($jobId){
		$this->Job->contain('User');
		$job = $this->Job->findById($jobId);
		$isEnrolled = false;
		if(!empty($job['User'])){
			foreach($job['User'] as $user){
				if($user['id'] == $this->id){
					$isEnrolled = true;
				} else {
					continue;
				}
			}
		}
		return $isEnrolled;
	}

	public function addUnavailability( $params = array() ){
		$exists = $this->UserUnavailability->find('first', array(
			'conditions' => array(
				'user_id' => $this->id,
				'model_id' => $params['model_id'],
				'model' => $params['model']
			)
		));
		$unavailability = array(
			'UserUnavailability' => array(
				'id' => empty($exists) ? '' : $exists['UserUnavailability']['id'],
				'user_id' => $this->id,
				'model_id' => $params['model_id'],
				'model' => $params['model'],
				'date' => $params['date'],
				'status' => $params['status']
				)
			);
		$this->UserUnavailability->save($unavailability);
	}

	public function hasCompetences($jobId){
		$job = $this->Job->findById($jobId);

		$conditions = array();
		$conditions[] = array('Competence.job' => $job['Job']['job']);
		$conditions[] = array('Competence.sector' => $job['Job']['sector']);
		$conditions[] = array('Competence.hierarchy >=' => $job['Job']['hierarchy']);
		$conditions[] = array('Competence.user_id' => $this->id);

		if(!empty($job['Job']['activity_id'])){
			$conditions[] = array('Competence.activity_id' => $job['Job']['activity_id']);
		}

		$competences = $this->Competence->find('all', array(
			'conditions' => $conditions
		));

		if(empty($competences)){
			return false;
		} else {
			return true;
		}
	}

	public function getReporting($company = null, $start = null, $end = null){
		// we need to sum all events of current period
		// we need to sum all salaries of current period
		// we need to get distincts hour salaries of current period

		if(empty($start)) $start = date('Y-m-01');
		if(empty($end)) $end = date('Y-m-t');

		$reports = array('events' => null, 'hours' => null, 'salaries' => '');

		$role = $this->field('role');
		$trainee = $this->field('trainee');
		$forbiddenSalaries = array();
		if($role == 'fixed'){
			$forbiddenSalaries = array(35.00, 15.00);
		}

		if($company == 4 || $company == 5){
			$companyIds = array(4,5);
		} else {
			$companyIds = $company;
		}

		$data = array(
			'joins' => array(
				array(
					'table' => 'jobs',
					'alias' => 'j',
					'conditions' => array('j.id = JobUser.job_id')
				),
				array(
					'table' => 'events',
					'alias' => 'e',
					'conditions' => array('e.id = j.event_id')
				)
			),
			'conditions' => array(
				'JobUser.user_id' => $this->id,
				'JobUser.validated' => 1,
				'e.confirmed_date >=' => $start,
				'e.confirmed_date <=' => $end,
				'e.company_id' => $companyIds,
				'JobUser.salary NOT' => $forbiddenSalaries
			)
		);
		$data['group'] = 'e.id';
		$events = $this->JobUser->find('count', $data);
		if(!empty($events)){
			$reports['events'] = $events;
		}
		unset($data['group']);

		$data['fields'] = 'SUM(JobUser.hours) AS hours';
		$hours = $this->JobUser->find('all', $data);
		if(!empty($hours)){
			$reports['hours'] = $hours[0][0]['hours'];
		}
		unset($data['fields']);

		$data['fields'] = 'SUM(JobUser.total) AS salary';
		$salary = $this->JobUser->find('all', $data);
		if(!empty($salary)){
			$reports['salary'] = round($salary[0][0]['salary'] / 5, 2) * 5;
		}
		unset($data['fields']);

		$data['fields'] = array('JobUser.salary', 'j.salary', 'JobUser.user_id');
		$salaries1 = $this->JobUser->find('all', $data);
		$salaries = array();
		if(!empty($salaries1)){
			foreach($salaries1 as $item){

				if(!empty($item['JobUser']['salary'])){
					$salary = $item['JobUser']['salary'];
				} elseif(!empty($item['j']['salary'])) {
					$salary = $item['j']['salary'];
				} else {
					continue;
				}
				if(!in_array($salary * 0.8175, $salaries)){
					if($start >= '2016-09-01'){
						$salaries[] = round($salary * 0.8175, 4);
					} else {
						$salaries[] = round($salary * 0.8175, 4);
					}
				}
			}
			$reports['salaries'] = $salaries;
			sort($reports['salaries']);
		}

		return $reports;
	}

	public function customSearchUser($column, $searchTerm, $columnSearchTerm, $config) {
  	if($searchTerm){
  		$config->conditions[] = array(
  			'OR' => array(
  				array('User.first_name LIKE' => '%' . $searchTerm . '%'),
  				array('User.last_name LIKE' => '%' . $searchTerm . '%'),
  				array('User.email LIKE' => '%' . $searchTerm . '%'),
  				array('t.value LIKE' => '%' . $searchTerm . '%')
  			)
  		);
  	}
  	return $config;
	}
}
