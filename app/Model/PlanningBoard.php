<?php

class PlanningBoard extends AppModel {

  public $actsAs = array(
    'Containable'
  );

  public $belongsTo = array('Event');

  public $hasMany = array(
    'PlanningBoardMoment' => array(
      'order' => 'PlanningBoardMoment.weight',
      'dependent' => true
    )
  );

}
