<?php

class Commune extends AppModel {

	public $actsAs = array('Containable');

	public $virtualFields = array(
		'zip_name' => 'CONCAT(zip, " ", name)',
		'zip_name_canton' => 'CONCAT(zip, " ", name," ", canton)'
	);

}
