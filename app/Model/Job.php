<?php class Job extends AppModel {

  public $actsAs = array('Containable');
  public $belongsTo = array(
    //'User',
    'Event' => array(
      'counterCache' => true
    ),
    'Activity',
    'FBModule' => array(
      'foreignKey' => 'fb_module_id'
    ),
    'Outfit'
  );
  public $hasMany = array(
    'StaffQueue',
    'Round',
    'UserUnavailability' => array(
      'foreignKey' => 'model_id',
      'conditions' => array('UserUnavailability.model' => 'job')
    ),
    'JobUser' => array(
      'dependent' => true
    ),
    'JobBriefing' => array(
      'dependent' => true
    )
  );

  public $hasAndBelongsToMany = array(
    'User'
  );

  public function beforeSave( $options = array() ){
    
    $jobusers = $this->JobUser->find('all', array(
      'conditions' => array(
        'job_id' => $this->id
      )
    ));
    $eventId = $this->field('event_id');
    if(!empty($jobusers)){
      App::import('Model','PlanningResource');
		  $PlanningResource = new PlanningResource();
      foreach($jobusers as $ju){
        if(empty($this->data['Job']['user_ids']) || !in_array($ju['JobUser']['user_id'], $this->data['Job']['user_ids'])){
          $PlanningResource->remove($eventId, 'extra', $ju['JobUser']['user_id']);
        }
      }
    }

  }

  public function afterSave($created, $options = array()){
    // we need to create 3 round steps if they do not exist yet
    $round = $this->Round->findByJobId($this->id);
    if(empty($round)){
      $this->Round->saveAssociated(array(
        'Round' => array(
          'job_id' => $this->id,
          'status' => 'open'
        ),
        'RoundStep' => array(
          array(
            'weight' => 1,
            'date' => date('Y-m-d'),
            'sent' => 0
          ),
          array(
            'weight' => 2,
            'date' => date('Y-m-d', strtotime("+7 days")),
            'sent' => 0
          ),
          array(
            'weight' => 3,
            'date' => date('Y-m-d', strtotime("+14 days")),
            'sent' => 0
          )
        )
      ));
    }

    $this->contain(array('User', 'Event', 'JobUser'));
    $job = $this->findById($this->id);

    // save user unavailabilities
    if(empty($job['User'])){
      // we need to delete all exising unavailabilities with user and job
      $this->User->UserUnavailability->deleteAll(array(
        'UserUnavailability.model_id' => $this->id,
        'UserUnavailability.model' => 'job',
        'UserUnavailability.status' => 'interested'
      ));
      $this->JobUser->deleteAll(array(
        'JobUser.job_id' => $job['Job']['id']
      ));
    } else {
      foreach($job['User'] as $user){
        if(!empty($job['Event']['confirmed_date'])){
          $data = array(
            'model_id' => $this->id,
            'model' => 'job',
            'status' => 'interested',
            'date' => $job['Event']['confirmed_date'],
            'start_hour' => $job['Job']['start_time'],
            'end_hour' => $job['Job']['end_time']
          );
          $this->User->id = $user['id'];
          $this->User->addUnavailability($data);
        }
      }
    }

    // let's save actual staff
    $staffEnrolled = $this->JobUser->find('count', array(
      'joins' => array(
        array(
          'table' => 'users',
          'alias' => 'u',
          'conditions' => array(
            'u.id = JobUser.user_id',
            'u.email IS NOT NULL'
          )
        )
      ),
      'conditions' => array(
        'job_id' => $job['Job']['id'],
        'user_id <>' => null
      )
    ));
    $this->saveField('staff_enrolled', $staffEnrolled, array('callbacks' => false));

    // we need to check if staff is complete, if yes no rounds should be sent
    if( $staffEnrolled >= $job['Job']['staff_needed'] ){
      $round['Round']['status'] = 'closed';
    } else {
      $round['Round']['status'] = 'open';
    }
    $this->Round->save($round);

    // if(empty($job['Job']['salary']) || ($job['Job']['staff_needed'] == 1 && !empty($job['User']))){
    //   $salary = $this->getSalary();
    //   if($job['Job']['staff_needed'] == 1 && !empty($job['User'])){
    //     $salary = $this->getSalary(array('User' => $job['User'][0]));
    //   }
    //   $this->saveField('salary', $salary, array('callbacks' => false));
    // }
  }

  public function getSalary($user = array()){
    $job = $this->findById($this->id);
    $salary = 25.00;

    if(!empty($user)){
      if(!is_null($user['User']['minimal_salary'])){
        return $user['User']['minimal_salary'];
      }
      if($user['User']['trainee']){
        return Configure::read('Users.salary_for_trainee');
      }
      if($user['User']['role'] == 'fixed'){
        return Configure::read('Users.salary_for_fixed');
      }
      if($user['User']['more_than_250_hours'] == 1){
        return Configure::read('Users.minimal_salary');
      }
    }

    if(($job['Job']['sector'] == 'fb' && $job['Job']['job'] == 'event_manager') || ($job['Job']['sector'] == 'fb' && $job['Job']['job'] == 'bar_manager')){
      return 30.00;
    }

    switch($job['Job']['sector']){
      case 'fb':
        if($job['Job']['job'] == 'mg_event_manager') $salary = 30.00;
        elseif($job['Job']['job'] == 'event_manager') $salary = 30.00;
        elseif($job['Job']['job'] == 'mg_service_crew') $salary = 25.00;
        elseif($job['Job']['job'] == 'service_crew') $salary = 25.00;
        elseif($job['Job']['job'] == 'bar_crew') $salary = 25.00;
        elseif($job['Job']['job'] == 'mg_event_assistant') $salary = 25.00;
        elseif($job['Job']['job'] == 'event_assistant') $salary = 25.00;
        elseif($job['Job']['job'] == 'mg_deliverer') $salary = 30.00;
      break;
      case 'logistics':
        if    ($job['Job']['job'] == 'mg_deliveryman')  $salary = 30.00;
        elseif($job['Job']['hierarchy'] == 4)            $salary = 27.00;
        elseif($job['Job']['hierarchy'] == 3)            $salary = 25.00;
        elseif($job['Job']['hierarchy'] == 2)            $salary = 25.00;
      break;
      case 'animation':
        if    ($job['Job']['job'] == 'animator')                 $salary = 27.00;
        elseif($job['Job']['job'] == 'animator_facilitator')     $salary = 25.00;
        elseif($job['Job']['job'] == 'coach')                   $salary = 0.00;
        elseif($job['Job']['job'] == 'urban_leader')             $salary = 27.00;
        elseif($job['Job']['job'] == 'urban_coach')             $salary = 25.00;
      break;
      case 'other':
        if     ($job['Job']['job'] == 'coordinator')             $salary = 27.00;
        elseif($job['Job']['job'] == 'presenter')               $salary = 27.00;
      break;
      default:
        $salary = 25.00;
      break;
    }
    return $salary;
  }

  public function getManpowerCosts(){
    $this->contain(array('JobUser'));
    $job = $this->findById($this->id);
    $hours = 0;
    $salary = 0;
    if(!empty($job['JobUser'])){
      foreach($job['JobUser'] as $item){
        if(empty($item['total']) || empty($item['hours']) || empty($item['validated'])){
          continue;
        }
        $hours += $item['hours'];
        $salary += ($item['hours'] * $item['salary'] * 1.1);
      }
    }
    return array('hours' => $hours, 'salary' => $salary);
  }

}
