<?php

class Partner extends AppModel {

	public $useTable = 'clients';

  public $hasMany = array(
    'ContactPeople',
		'Document' => array(
			'foreignKey' => 'parent_id'
		)
  );
  public $hasAndBelongsToMany = array('Company');

	function beforeFind($query) {
    $query['conditions'][] = array('type' => 'partner');
    return $query;
  }

}
