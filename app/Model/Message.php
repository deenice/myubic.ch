<?php

class Message extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('User');

	public function send(){

		$this->layout = 'default';

		App::uses('CakePdf', 'CakePdf.Pdf');
		App::uses('CakeEmail', 'Network/Email');

		$message = $this->read(null, $this->id);
		$sender = $this->User->findById($message['Message']['user_id']);
		$recipients = $this->recipients();

		$server = Configure::read('Debug.server');
		if($server == 'local'){
			$to = 'denis@une-bonne-idee.ch';
		} elseif($server == 'test'){
			$to = 'dev@myubic.ch';
		} elseif($server == 'prod'){
			$to = $recipients;
		}

		$mail = new CakeEmail();
    $mail->from(array($sender['User']['email'] => $sender['User']['first_name']))
         ->to($to)
				 ->bcc('denis@une-bonne-idee.ch')
         ->subject($message['Message']['subject'])
         ->emailFormat('html')
         ->template('message_body')
         ->viewVars(array('body' => $message['Message']['body']));

    if($mail->send()){
			return true;
		} else {
			return false;
		}

	}

	public function recipients(){
		$message = $this->read(null, $this->id);
		$recipients = explode(',', $message['Message']['recipients']);
		$data = array();
		if(!empty($recipients)){
			foreach($recipients as $recipient){
				$user = $this->User->findById($recipient);
				$data[] = $user['User']['email'];
			}
		}
		return $data;
	}
}
