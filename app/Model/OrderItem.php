<?php

class OrderItem extends AppModel {

  public $actsAs = array('Containable');

  public $belongsTo = array(
    'OrderGroup',
    'StockItem',
    'StockLocation',
    'Supplier',
    'Activity' => array(
      'foreignKey' => 'model_id'
    ),
    'FBModule' => array(
      'foreignKey' => 'model_id'
    )
  );

  public function afterSave($created, $options = array()){

    $groupId = $this->field('order_group_id');
    $quantity = $this->field('quantity');
    $editable = $this->field('editable');

    if($created){
      $itemInSameGroup = $this->find('first', array(
        'conditions' => array(
          'order_group_id' => $groupId
        )
      ));
      if(!empty($itemInSameGroup) && !empty($itemInSameGroup['OrderItem']['model_id'])){
        $this->saveField('model', $itemInSameGroup['OrderItem']['model'], array('callbacks' => false));
        $this->saveField('model_id', $itemInSameGroup['OrderItem']['model_id'], array('callbacks' => false));
      }
    }

    if(empty($quantity) && !$editable){
      $this->updateQuantity();
    }

    $this->updateUnavailability();
  }

  public function updateUnavailability(){
    $item = $this->findById($this->id);
    $group = $this->OrderGroup->findById($item['OrderItem']['order_group_id']);
    $order = $this->OrderGroup->Order->findById($group['OrderGroup']['order_id']);
    if(!empty($order['Order']['model_id']) && $order['Order']['model'] == 'event'){
      $event = $this->OrderGroup->Order->Event->findById($order['Order']['model_id']);
    } else {
      $event = array();
    }
    if(empty($item['OrderItem']['model'])){
      return false;
    }
    if($item['OrderItem']['quantity'] <= 0){
      return false;
    }
    if(!empty($item['OrderItem']['stock_item_id']) && !empty($event)){
      $existingUnavailability = $this->StockItem->StockItemUnavailability->deleteAll(array(
        'stock_item_id' => $item['OrderItem']['stock_item_id'],
        'event_id' => $event['Event']['id']
      ));
      $status = 'potential';
      if($event['Event']['crm_status'] == 'confirmed'){
        $status = 'confirmed';
      }
      if($event['Event']['start_hour'] > $event['Event']['end_hour']){
        $endDate = date('Y-m-d', strtotime($event['Event']['confirmed_date'] . "+1 days"));
      } else {
        $endDate = $event['Event']['confirmed_date'];
      }
      // many items of same stock item can be found in event!
      $totalQuantity = $this->find('all', array(
        'joins' => array(
          array(
            'table' => 'order_groups',
            'alias' => 'og',
            'conditions' => array('og.id = OrderItem.order_group_id')
          ),
          array(
            'table' => 'orders',
            'alias' => 'o',
            'conditions' => array('o.id = og.order_id')
          )
        ),
        'conditions' => array(
          'OrderItem.stock_item_id' => $item['OrderItem']['stock_item_id'],
          'o.model' => 'event',
          'o.model_id' => $event['Event']['id']
        ),
        'fields' => array('SUM(quantity) as total')
      ));
      $start = new DateTime( $event['Event']['confirmed_date'] . ' ' . $event['Event']['start_hour'] );
      $end = new DateTime( $endDate . ' ' . $event['Event']['end_hour'] );
      $unavailabilities = array(
        array(
          'StockItemUnavailability' => array(
            'id' => null,
            'stock_item_id' => $item['OrderItem']['stock_item_id'],
            'quantity' => !empty($item['OrderItem']['number_of_containers']) ? $item['OrderItem']['number_of_containers'] : $totalQuantity[0][0]['total'],
            'event_id' => $event['Event']['id'],
            'start_date' => $start->format('Y-m-d H:i'),
            'end_date' => $end->format('Y-m-d H:i'),
            'status' => $status
          )
        ),
        array(
          'StockItemUnavailability' => array(
            'id' => null,
            'stock_item_id' => $item['OrderItem']['stock_item_id'],
            'quantity' => !empty($item['OrderItem']['number_of_containers']) ? $item['OrderItem']['number_of_containers'] : $totalQuantity[0][0]['total'],
            'event_id' => $event['Event']['id'],
            'start_date' => $end->format('Y-m-d H:i'),
            'end_date' => $end->add(new DateInterval ("PT18H"))->format('Y-m-d H:i'),
            'status' => 'reconditionning'
          )
        )
      );
      $this->StockItem->StockItemUnavailability->create();
      if($this->StockItem->StockItemUnavailability->saveMany($unavailabilities)){
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  public function updateQuantities( $options = array() ){
    $items = $this->find('all', array(
      'joins' => array(
        array(
          'table' => 'order_groups',
          'alias' => 'og',
          'conditions' => array('og.id = OrderItem.order_group_id')
        ),
        array(
          'table' => 'orders',
          'alias' => 'o',
          'conditions' => array('o.id = og.order_id')
        )
      ),
      'conditions' => array(
        'OrderItem.model' => $options['item_model'],
        'OrderItem.model_id' => $options['item_model_id'],
        'OrderItem.locked' => 0,
        'o.model_id' => $options['order_model_id'],
        'o.model' => $options['order_model']
      )
    ));
    if(!empty($items)){
      foreach($items as $item){
        $this->id = $item['OrderItem']['id'];
        $this->updateQuantity();
        $this->updateUnavailability();
      }
    }
  }

  public function updateQuantity(){

    $modelId = $this->field('model_id');
    $model = $this->field('model');
    $groupId = $this->field('order_group_id');
    $group = $this->OrderGroup->findById($groupId);
    $order = $this->OrderGroup->Order->findById($group['OrderGroup']['order_id']);

    $event = $this->OrderGroup->Order->Event->findById($order['Order']['model_id']);

    $option = $this->field('quantity_option');
    $quantity = 0;
    $this->contain(array('StockItem'));
    $item = $this->findById($this->id);

    $locked = $this->field('locked');

    switch($model){
      case 'activity':
        $ae = $this->Activity->ActivityEvent->find('first', array(
          'conditions' => array(
            'activity_id' => $modelId,
            'event_id' => $order['Order']['model_id']
          )
        ));
        $options = $ae['ActivityEvent'];
      break;
      case 'fb_module':
        $efm = $this->FBModule->EventFBModule->find('first', array(
          'conditions' => array(
            'fb_module_id' => $modelId,
            'event_id' => $order['Order']['model_id']
          )
        ));
        $options = $efm['EventFBModule'];
      break;
    }

    switch($option){
      case 'fixed':
        $quantity = $item['OrderItem']['default_quantity'];
      break;
      case 'pieces_per_person':
        if(!empty($options['number_of_persons'])){
          $quantity = $options['number_of_persons'] * $item['OrderItem']['default_quantity'];
        } elseif(empty($options['number_of_persons']) && !empty($event['Event']['confirmed_number_of_persons'])){
          $quantity = $event['Event']['confirmed_number_of_persons'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'quantity_per_person':
        if(!empty($options['number_of_persons'])){
          $quantity = $options['number_of_persons'] * $item['OrderItem']['default_quantity'];
        } elseif(empty($options['number_of_persons']) && !empty($event['Event']['confirmed_number_of_persons'])){
          $quantity = $event['Event']['confirmed_number_of_persons'] * $item['OrderItem']['default_quantity'];
        }
        $numberOfContainers = is_null($item['OrderItem']['quantity_per_container']) ? 0 : ceil($quantity / $item['OrderItem']['quantity_per_container']);
        $this->saveField('number_of_containers', $numberOfContainers, array('callbacks' => false));
      break;
      case 'pieces_per_group':
        if(!empty($options['number_of_groups'])){
          $quantity = $options['number_of_groups'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'pieces_per_team':
        if(!empty($options['number_of_teams'])){
          $quantity = $options['number_of_teams'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'pieces_per_animator':
        if(!empty($options['number_of_animators'])){
          $quantity = $options['number_of_animators'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'interval':
        if(!empty($options['number_of_persons'])){
          $quantity = ceil($options['number_of_persons'] / $item['OrderItem']['default_quantity']);
        } elseif(empty($options['number_of_persons']) && !empty($event['Event']['confirmed_number_of_persons'])){
          $quantity = ceil($event['Event']['confirmed_number_of_persons'] / $item['OrderItem']['default_quantity']);
        }
      break;
      case 'pieces_per_staff':
        $this->Activity->Event->id = $options['event_id'];
        $numberOfStaff = $this->Activity->Event->getNumberOfJobs('filled');
        if(!empty($numberOfStaff)){
          $quantity = $numberOfStaff * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'pieces_per_person_per_team':
        if(!empty($options['number_of_persons_per_team'])){
          $quantity = $options['number_of_persons_per_team'] * $item['OrderItem']['default_quantity'];
        }
      break;
      case 'clothing_size':
        // find all users working on event
        $users = $this->OrderGroup->Order->Event->Job->JobUser->find('all', array(
          'contain' => array('User' => array('fields' => 'shirt_size')),
          'joins' => array(
            array(
              'table' => 'jobs',
              'alias' => 'j',
              'conditions' => array('j.id = JobUser.job_id', 'j.event_id' => $event['Event']['id'])
            )
          )
        ));
        $sizes = array();
        if(!empty($users)){
          foreach($users as $user){
            if($user['User']['shirt_size'] == 'XS'){
              $user['User']['shirt_size'] = 'S';
            }
            $sizes[$user['User']['shirt_size']][] = $user;
          }
          $quantity = !empty($item['StockItem']['size'] && isset($sizes[$item['StockItem']['size']])) ? $item['OrderItem']['default_quantity'] * sizeof($sizes[$item['StockItem']['size']]) : 0;
        } else {
          $quantity = 0;
        }
      break;
      case 'pieces_per_table':
        if(!empty($options['number_of_tables'])){
          $quantity = $options['number_of_tables'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
      case 'pieces_per_buffet':
        if(!empty($options['number_of_buffets'])){
          $quantity = $options['number_of_buffets'] * $item['OrderItem']['default_quantity'];
        }
        $quantity = ceil($quantity);
      break;
    }

    if(!$locked) $this->saveField('quantity', $quantity, array('callbacks' => false));
  }

}
