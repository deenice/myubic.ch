<?php

class Wish extends AppModel {

	public $actsAs = array('Containable');
	public $belongsTo = array('Client', 'ContactPeople', 'User');

	public function send(){

		$sender = $this->User->read(array('email', 'full_name'), AuthComponent::user('id'));
		$message = $this->field('message');
		$cp = $this->field('contact_people_id');
		$contact = $this->ContactPeople->read(array('email'), $cp);

		App::uses('CakeEmail', 'Network/Email');
		$mail = new CakeEmail();
    $mail->config('ubic')
					->from(array('info@une-bonne-idee.ch' => 'Une-Bonne-Idée.ch'))
					->sender(array('info@une-bonne-idee.ch' => 'Une-Bonne-Idée.ch'))
					->replyTo(array($sender['User']['email'] => $sender['User']['full_name']))
					->to($contact['ContactPeople']['email'])
					->emailFormat('html')
					->subject('Voeux 2017')
					->template('wishes')
					->viewVars(array('message' => $message));
		
		if($mail->send()){
			return true;
		} else {
			return false;
		}

	}

}
