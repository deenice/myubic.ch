<?php

class Round extends AppModel {

  public $actsAs = array('Containable');

  public $hasMany = array(
    'RoundStep' => array(
      'order' => 'RoundStep.weight'
    )
  );

  public $hasOne = array(
    'FirstRoundStep' => array(
      'className' => 'RoundStep',
      'conditions' => array(
        'FirstRoundStep.weight' => 1
      )
    ),
    'SecondRoundStep' => array(
      'className' => 'RoundStep',
      'conditions' => array(
        'SecondRoundStep.weight' => 2
      )
    ),
    'ThirdRoundStep' => array(
      'className' => 'RoundStep',
      'conditions' => array(
        'ThirdRoundStep.weight' => 3
      )
    )
  );

  public $belongsTo = array('Job');

  public function getAnswersByUser($userId = ''){

    $this->contain(
      array(
        'FirstRoundStep' => array(
          'RoundAnswer' => array(
            'conditions' => array(
              'RoundAnswer.user_id' => $userId
            )
          )
        ),
        'SecondRoundStep' => array(
          'RoundAnswer' => array(
            'conditions' => array(
              'RoundAnswer.user_id' => $userId
            )
          )
        ),
        'ThirdRoundStep' => array(
          'RoundAnswer' => array(
            'conditions' => array(
              'RoundAnswer.user_id' => $userId
            )
          )
        )
      )
    );
    return $this->findById($this->id);

  }

}
