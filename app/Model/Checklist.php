<?php

class Checklist extends AppModel{

	public $actsAs = array('Containable');

  public $belongsTo = array(
    'Event' => array(
      'foreignKey' => 'model_id'
    ),
    'Company'
  );

  public $hasMany = array(
    'ChecklistBatch' => array(
      'order' => 'weight',
      'dependent' => true
    )
  );

  public function afterSave($created, $options = array()){
    if($created){
      $isDefault = $this->field('default');
      $isPersonal = $this->field('personal');
      if(!$isDefault && !$isPersonal){
        $this->saveField('personal', 1);
      }
    }
  }

  public function assign( $model_id = null ){
    $this->contain(array(
      'ChecklistBatch' => array(
        'ChecklistTask'
      )
    ));
    $default = $this->findById($this->id);
    //debug($default);
    $default['Checklist']['id'] = null;
    $default['Checklist']['default'] = 0;
    $default['Checklist']['personal'] = 1;
    $default['Checklist']['name'] = empty($model_id) ? $default['Checklist']['name'] . ' - copy' : $default['Checklist']['name'];
    $default['Checklist']['model_id'] = $model_id;
    if($default['Checklist']['model'] == 'event' && !empty($model_id)){
      $event = $this->Event->findById($model_id);
      $default['Checklist']['user_id'] = $event['Event']['resp_id'];
    } else {
      $default['Checklist']['user_id'] = AuthComponent::user('id');
    }
    $this->create();
    if(!$this->save($default)){
      return false;
    }
    foreach($default['ChecklistBatch'] as $k => $batch){
      $default['ChecklistBatch'][$k]['id'] = null;
      $default['ChecklistBatch'][$k]['checklist_id'] = $this->id;
      $this->ChecklistBatch->create();
      if(!$this->ChecklistBatch->save($default['ChecklistBatch'][$k])){
        return false;
      }
      foreach($batch['ChecklistTask'] as $kk => $task){
        $default['ChecklistBatch'][$k]['ChecklistTask'][$kk]['id'] = null;
        $default['ChecklistBatch'][$k]['ChecklistTask'][$kk]['checklist_batch_id'] = $this->ChecklistBatch->id;
        $default['ChecklistBatch'][$k]['ChecklistTask'][$kk]['due_date'] = null;
        $this->ChecklistBatch->ChecklistTask->create();
        if(!$this->ChecklistBatch->ChecklistTask->save($default['ChecklistBatch'][$k]['ChecklistTask'][$kk])){
          return false;
        }
      }
    }
    return true;
  }

  public function isComplete(){
    $progressAverage = $this->ChecklistBatch->find('all', array(
      'conditions' => array(
        'checklist_id' => $this->id
      ),
      'fields' => array("AVG(progress) as avg")
    ));
    $avg = round($progressAverage[0][0]['avg'], 0);
    if($avg == 100){
      return true;
    } else {
      return false;
    }
  }
}
