<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class Document extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'User' => array('foreignKey' => 'owner'),
		'Place',
		'Owner' => array(
			'className' => 'User',
			'foreignKey' => 'owner'
		),
		'StockItem',
		'Module',
		'Activity',
		'Game',
		'Event',
		'Myjeu'
	);

	public function saveFile($file, $parent_id, $destination = array(), $category = '', $extensions = ''){
		$allowedExtensions = explode(',',$extensions);
		$pathinfo = pathinfo($file['name']);
		$extension = strtolower($pathinfo['extension']);
		if(in_array($extension, $allowedExtensions)){

			$token = $category == 'stockitem' ? $pathinfo['filename'] : md5(uniqid(mt_rand(), true));
			$filename = $token . '.' . $extension;

			if($category == 'stockitem'){
				unset($destination[1]);
			}

			$relativeDir = 'files' . DS . 'documents' . DS . implode(DS, $destination) . DS . $parent_id . DS;
			$absoluteDir = WWW_ROOT . $relativeDir;
			$folder = new Folder($absoluteDir, true, 0777);

			$absoluteFilename = $absoluteDir . $filename;
			$relativeFilename = $relativeDir . $filename;

			if(move_uploaded_file($file['tmp_name'], $absoluteFilename)){
				$this->create();
				//thumbnail is unique, therefore we check if any exists, if yes => overwrite
				if(in_array('thumbnail', $destination) OR in_array('logo', $destination)){
					$exists = $this->find('first', array(
						'conditions' => array(
							'parent_id' => $parent_id,
							'category IN' => array('user_thumbnail', 'place_thumbnail')
						)
					));
				}
				$document = array(
					'name' => Inflector::slug($pathinfo['filename'],' '),
					'filename' => $filename,
					'url' => $relativeFilename,
					'filesize' => $file['size'],
					'filetype' => $file['type'],
					'parent_id' => $parent_id,
					'owner' => AuthComponent::user('id'),
					'category' => $category,
					'id' => !empty($exists['Document']['id']) ? $exists['Document']['id'] : ''
				);
				//debug($document);exit;
				$this->save(array('Document' => $document));
				return true;
			}
		} else {
			return false;
		}
	}

	public function rename($id, $oldName, $newName, $userId) {
		if (!file_exists($oldPath = $this->getFullPath($pathFolderNames, $oldName))) { // File doesn't exist
			$document = $this->findByUrl($this->getRelativePath($pathFolderNames, $oldName), array('id'));
			if (!empty($document)) {
				$this->delete($document['Document']['id']);
				return array(
					'error' => __d("document_manager", "Ce fichier n'existe plus."),
					'remove' => true,
				);
			}
		} else { // File exists
			if (file_exists($newPath = $this->getFullPath($pathFolderNames, $newName))) {
				// A file with requested new name already exists
				if ($oldName != $newName) { // If same names, no problem
					return array(
						'error' => __d("document_manager", "Un fichier portant ce nom existe déjà."),
						'remove' => false,
					);
				}
			} else {
				$document = $this->findByUrl($this->getRelativePath($pathFolderNames, $newName), array('id'));
				if (!empty($document)) { // A document exists for requested new name, but without a file. This shouldn't happen
					// Prevent database error for duplicate Document URL
					$this->delete($document['Document']['id']);
				}
			}

			if (!($success = $oldName == $newName // If same names, epic win
					|| rename($oldPath, $this->getFullPath($pathFolderNames, $newName)))) {
				return array(
					'error' => __d("document_manager", "Le fichier n'a pu être renommé."),
					'remove' => false,
				);
			}

			$document = $this->findByUrl($this->getRelativePath($pathFolderNames, $oldName), array('id'));
			if (empty($document)) { // No document existed for this file. This should happen only if a file was created in the directory outside of this plugin
				// Create Document for this file
				$session = new CakeSession();
				$this->create();
				$this->save(array(
					'user_id' => $userId,
					'url' => $this->getRelativePath($pathFolderNames, $newName),
				));
			} else {
				$this->id = $document['Document']['id'];
				if ($newName != $oldName) { // File name changed
					// Update Document URL
					$this->saveField('url', $this->getRelativePath($pathFolderNames, $newName));
				}
			}

			// A correct Document exists now for this file: find useful information
			$document = $this->find('first', array(
				'conditions' => array('id' => $this->id),
				'contain' => array('User'),
				'fields' => array('user_id', 'comments'),
					));
			return array(
				'name' => $newName,
				'Document' => empty($document) ? null : $document['Document'],
				'User' => empty($document) ? null : $document['User'],
			);
		}
	}

	public function getFiles( $parent_id, $category = null ) {
		$options = array(
			'conditions' => array(
				'parent_id' => $parent_id,
				'category' => $category
			),
			'contain' => array('User', 'Owner')
		);
		$files = $this->find('all', $options);
		if(!empty($files)){
			return $files;
		} else {
			return false;
		}
	}

	public function getThumbnail(){
		debug($this->User->find('first'));
	}
}
