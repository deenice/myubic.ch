<?php

class VehicleTourMoment extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('StockOrder', 'VehicleTour');

	public function get($vehicleTourId = ''){

		$start = array();
		$end = array();
		$orders = array();
		$others = array();
		$data = array();
		$stockorders = array();

		$moments = $this->find('all', array(
			'conditions' => array(
				'vehicle_tour_id' => $vehicleTourId
			),
			'order' => 'weight ASC'
		));
		if(empty($moments)){
			return $moments;
		}

		foreach($moments as $k => $moment){
			if($moment['VehicleTourMoment']['type'] == 'start'){
				$start = $moment;
			}
			elseif($moment['VehicleTourMoment']['type'] == 'end'){
				$end = $moment;
			}
			elseif($moment['VehicleTourMoment']['type'] == 'delivery' OR $moment['VehicleTourMoment']['type'] == 'return'){
				$orders[$moment['VehicleTourMoment']['weight']] = $moment;
				$stockorders[] = $moment['VehicleTourMoment']['stock_order_id'];
			} elseif($moment['VehicleTourMoment']['type'] == 'route') {
				// we delete the route, it will be added later
				$this->delete($moment['VehicleTourMoment']['id']);
			} else {
				$others[$moment['VehicleTourMoment']['weight']] = $moment;
			}
		}

		// search for orders which tour is the selected one but is not in moments table
		$missingOrders = $this->VehicleTour->StockOrderVehicleTour->find('all', array(
			'conditions' => array(
				'StockOrderVehicleTour.vehicle_tour_id' => $vehicleTourId,
				'StockOrderVehicleTour.stock_order_id NOT' => $stockorders,
			)
		));
		if(!empty($missingOrders)){
			foreach($missingOrders as $k => $order){
				$newMoment = $this->save(array(
					'VehicleTourMoment' => array(
						'type' => $order['StockOrderVehicleTour']['type'],
						'stock_order_id' => $order['StockOrderVehicleTour']['stock_order_id'],
						'vehicle_tour_id' => $vehicleTourId,
						'weight' => 100 + $k
					)
				));
				$orders[$newMoment['VehicleTourMoment']['weight']] = $newMoment;
			}
		}

		$data[0] = $start;
		$counter = 0;
		foreach($orders as $k => $order){
			if($counter == 0){
				$data[++$counter] = array('VehicleTourMoment' => array('type' => 'route', 'weight' => $counter, 'vehicle_tour_id' => $order['VehicleTourMoment']['vehicle_tour_id']));
			}
			$data[++$counter] = $order;
			$data[++$counter] = array('VehicleTourMoment' => array('type' => 'route', 'weight' => $counter, 'vehicle_tour_id' => $order['VehicleTourMoment']['vehicle_tour_id']));
		}
		foreach($others as $position => $other){
			$data = $this->array_insert( $data, $position, array('VehicleTourMoment' => $other) );
		}
		$data[] = $end;

		// set new weights
		foreach($data as $k => $tmp){
			$tmp['VehicleTourMoment']['weight'] = $k;
			$data[$k] = $tmp;
		}

		$this->saveMany($data);

		$newMoments = $this->find('all', array(
			'conditions' => array(
				'vehicle_tour_id' => $vehicleTourId
			),
			'contain' => array('StockOrder'),
			'order' => 'weight'
		));
		return $newMoments;
	}

	private function array_insert($array, $position, $insert){
	    if (is_int($position)) {
	        array_splice($array, $position, 0, $insert);
	    } else {
	        $pos   = array_search($position, array_keys($array));
	        $array = array_merge(
	            array_slice($array, 0, $pos),
	            $insert,
	            array_slice($array, $pos)
	        );
	    }
	    return $array;
	}

	public function clean(){
		$this->deleteAll(array(
			'vehicle_tour_id' => null
		));
		$data = $this->find('all');
		$moments = array();
		foreach ($data as $key => $moment) {
			$moments[$moment['VehicleTourMoment']['vehicle_tour_id']]['delete'] = true;
		}
		foreach ($data as $key => $moment) {
			$moments[$moment['VehicleTourMoment']['vehicle_tour_id']][$key] = $moment;
			if(($moment['VehicleTourMoment']['type'] == 'delivery' OR $moment['VehicleTourMoment']['type'] == 'return') AND $moments[$moment['VehicleTourMoment']['vehicle_tour_id']]['delete']){
				$moments[$moment['VehicleTourMoment']['vehicle_tour_id']]['delete'] = false;
			}
		}
		foreach($moments as $id => $m){
			if($m['delete']){
				unset($m['delete']);
				foreach($m as $moment){
					$this->delete($moment['VehicleTourMoment']['id']);
				}
			}
		}
	}


}
