<?php

class Client extends AppModel {

	public $actsAs = array(
        'Containable',
        'Icing.Versionable' => array(
            'contain' => array('ContactPeople', 'Document')
        )
    );
    public $virtualFields = array(
    	'zip_city' => 'CONCAT(Client.zip, " ", Client.city)',
			'invoice_zip_city' => 'CONCAT(Client.invoice_zip, " ", Client.invoice_city)',
    	'name_address' => 'CONCAT(Client.name, " - ", Client.address, ", ", Client.zip, " ", Client.city)'
    );

	public $hasMany = array(
		'Event',
		'Document' => array(
			'foreignKey' => 'parent_id'
		),
		'ContactPeople' => array(
			'order' => 'ContactPeople.created DESC'
		),
		'StockOrder',
		'Lead',
		'Note' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'client'
			)
		),
    'PlanningResource' => array(
      'foreignKey' => 'resource_id',
      'conditions' => array(
        'PlanningResource.resource_model' => 'client'
      )
    ),
	'Wish'
	);

	public $hasOne = array(
		'Logo' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Logo.category' => 'client_logo'
			),
			'order' => 'Logo.modified DESC'
		)
	);

	public $hasAndBelongsToMany = array('Company');

	function beforeFind($query) {
    $query['conditions'][] = array('type' => 'client');
    return $query;
  }

	public function getAll( $companies = array() ){
		$clients = $this->find('all', array(
            'joins' => array(
                array(
                    'table' => 'clients_companies',
                    'alias' => 'ClientCompany',
                    'conditions' => array(
                        'ClientCompany.client_id = Client.id'
                    )
                ),
                array(
                    'table' => 'companies',
                    'alias' => 'Company',
                    'conditions' => array(
                        'Company.id = ClientCompany.company_id'
                    )
                )
            ),
            'conditions' => array(
                'Company.id' => $companies
            ),
            'contain' => array(
                'ContactPeople'
            ),
            'order' => 'Client.name'
        ));
        return $clients;
	}

	public function getUUID(){
		App::import('Model', 'Revelate');
		$Revelate = new Revelate();
		$this->contain(array('Company'));
		$client = $this->findById($this->id);
		if(!empty($client['Client']['uuid'])){
			$xml = $Revelate->xml( 'client', $client['Client']['id'] );
			$result1 = $Revelate->put($client['Company'][0]['uuid'], 'clients', $xml, $client['Client']['uuid']);
			return $client['Client']['uuid'];
		} else {
			// we need to create the client in Revelate
			$result = $Revelate->post($client['Company'][0]['uuid'], 'clients', $Revelate->xml( 'client', $client['Client']['id'] ));
			if($result->isOk()){
				$result = Xml::toArray(Xml::build($result->body));
				$this->saveField('uuid', $result['Identifier']['Id']);
				return $result['Identifier']['Id'];
			} else {
				return false;
			}
		}
	}

}
