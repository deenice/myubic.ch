<?php

class JobBriefing extends AppModel {

	public $actsAs = array(
		'Containable'
	);

	public $belongsTo = array('Job');

}
