<?php

class Search extends AppModel {

	public $actsAs = array('Containable');
	//public $recursive = 1;
	public $useTable = 'searches';

	public $belongsTo = array('Event', 'User');

	public $virtualFields = array('zip_city' => 'CONCAT(place_zip, " ", place_city)');
	
}