<?php

class VehicleTour extends AppModel {

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array(
		'VehicleReservation',
		'StockOrder' => array(
			'order' => 'StockOrdersVehicleTour.weight'
		)
	);

	public $hasMany = array(
		'VehicleTourMoment' => array('order' => 'weight'),
		'StockOrderVehicleTour' => array(
			'order' => 'weight'
		)
	);

	public $belongsTo = array(
		'Driver' => array(
			'className' => 'User',
			'foreignKey' => 'driver_id'
		)
	);

	public function afterSave($created, $options = array()){
		// we should delete empty tours (with no orders) and remove corresponding reservations
		$tours = $this->find('all', array(
			'contain' => array('StockOrderVehicleTour', 'VehicleReservation')
		));
		foreach ($tours	as $key => $tour) {
			if(empty($tour['StockOrderVehicleTour'])){
				//$this->query(sprintf("DELETE FROM vehicle_tours WHERE id = %d", $tour['VehicleTour']['id']));
				if(!empty($tour['VehicleReservation'])){
					foreach($tour['VehicleReservation'] as $res){
						$this->VehicleReservation->delete($res['id']);
					}
				}
			}
		}
		App::import('Model', 'VehicleReservationVehicleTour');
		$VehicleReservationVehicleTour = new VehicleReservationVehicleTour();
		$restours = $VehicleReservationVehicleTour->find('all', array(
			'conditions' => array(
				'vehicle_tour_id' => $this->id
			)
		));
		$driver_id = $this->field('driver_id');

		if(!empty($restours) && !empty($driver_id)){
			foreach($restours as $item){
				$reservation = $this->VehicleReservation->findById($item['VehicleReservationVehicleTour']['vehicle_reservation_id']);
				$reservation['VehicleReservation']['driver_id'] = $driver_id;
				$this->VehicleReservation->save($reservation, array('callbacks'=>false));
			}
		}
	}

	public function getStartHour(){
		$this->contain(array('VehicleTourMoment'));
		$tour = $this->findById($this->id);
		$startPoint = $this->VehicleTourMoment->find('first', array(
			'conditions' => array(
				'VehicleTourMoment.type' => 'start',
				'VehicleTourMoment.vehicle_tour_id' => $this->id,
			),
			'fields' =>array('weight')
		));
		if($startPoint['VehicleTourMoment']['weight'] != 0){
			$gap = 0;
			foreach($tour['VehicleTourMoment'] as $moment){
				if($moment['VehicleTourMoment']['weight'] < $startPoint['VehicleTourMoment']['weight']){
					$gap += $moment['VehicleTourMoment']['duration'];
				} else {
					continue;
				}
			}
		} else {
			$start_hour = $tour['VehicleTour']['start'];
		}
	}

	public function prepareMoments($moments, $tour, $start_hour){

		App::import('Model', 'MyTools');
		$MyTools = new MyTools;

		foreach($moments as $k => $moment){
			switch($moment['VehicleTourMoment']['type']){
				case 'start':
					$hour = $start_hour;
					$description = empty($tour['VehicleTour']['start_location_custom']) ? $tour['VehicleTour']['start_location'] : $tour['VehicleTour']['start_location_custom'];
					$description = strtoupper($description);
				break;

				case 'end':
					$hour = $start_hour;
					$description = empty($tour['VehicleTour']['end_location_custom']) ? $tour['VehicleTour']['end_location'] : $tour['VehicleTour']['end_location_custom'];
					$description = strtoupper($description);
				break;

				case 'route':
					// we have to find previous order or start address to compute distance and time
					$previous = $this->VehicleTourMoment->find('first', array(
						'contain' => array('StockOrder'),
						'conditions' => array(
							'vehicle_tour_id' => $tour['VehicleTour']['id'],
							'VehicleTourMoment.type' => array('start', 'delivery', 'return'),
							'weight <' => $moment['VehicleTourMoment']['weight']
						),
						'order' => 'VehicleTourMoment.weight DESC'
					));
					// we have to find next order or end address to compute distance and time
					$next = $this->VehicleTourMoment->find('first', array(
						'contain' => array('StockOrder'),
						'conditions' => array(
							'vehicle_tour_id' => $tour['VehicleTour']['id'],
							'VehicleTourMoment.type' => array('end', 'delivery', 'return'),
							'weight >' => $moment['VehicleTourMoment']['weight']
						),
						'order' => 'VehicleTourMoment.weight'
					));
					if($previous['VehicleTourMoment']['type'] == 'start'){
						$origin = $tour['VehicleTour']['start_address'];
					} elseif($previous['VehicleTourMoment']['type'] == 'delivery'){
						$origin = $previous['StockOrder']['delivery_address'] . ' ' . $previous['StockOrder']['delivery_zip_city'];
					} elseif($previous['VehicleTourMoment']['type'] == 'return'){
						$origin = $previous['StockOrder']['return_address'] . ' ' . $previous['StockOrder']['return_zip_city'];
					}
					$origin = trim($origin);
					if($next['VehicleTourMoment']['type'] == 'end'){
						$destination = $tour['VehicleTour']['end_address'];
					} elseif($next['VehicleTourMoment']['type'] == 'delivery'){
						$destination = $next['StockOrder']['delivery_address'] . ' ' . $next['StockOrder']['delivery_zip_city'];
					} elseif($next['VehicleTourMoment']['type'] == 'return'){
						$destination = $next['StockOrder']['return_address'] . ' ' . $next['StockOrder']['return_zip_city'];
					}
					$destination = trim($destination);
					$travel = $MyTools->getTravelTime($origin, $destination);
					$time = $travel['time_text'];
					$description = $travel['distance_text'] . ' - ' . $travel['time_text'];
					$start_hour = $start_hour + $travel['time_value'];
					$hour = '';
				break;

				case 'delivery':
					$description = $moment['StockOrder']['order_number'] . ' ';
					$description .= empty($moment['StockOrder']['delivery_place']) ? '' : $moment['StockOrder']['delivery_place'] . ', ';
					$description .= empty($moment['StockOrder']['delivery_address']) ? '' : $moment['StockOrder']['delivery_address'] . ', ';
					$description .= $moment['StockOrder']['delivery_zip_city'];
					$hour = $start_hour;
				break;

				case 'return':
					$description = $moment['StockOrder']['order_number'] . ' ';
					$description .= empty($moment['StockOrder']['return_place']) ? '' : $moment['StockOrder']['return_place'] . ', ';
					$description .= empty($moment['StockOrder']['return_address']) ? '' : $moment['StockOrder']['return_address'] . ', ';
					$description .= $moment['StockOrder']['return_zip_city'];
					$hour = $start_hour;
				break;

				case 'loading':
				case 'unloading':
				case 'break':
					$hour = $start_hour + ($moment['VehicleTourMoment']['duration'] * 60);
					$start_hour = $hour;
					$description = __('%s minutes', $moment['VehicleTourMoment']['duration']);
				break;

				case 'free':
					$hour = $start_hour + ($moment['VehicleTourMoment']['duration'] * 60);
					$start_hour = $hour;
					$description = $moment['VehicleTourMoment']['description'];
				break;

				default:
					$hour = '';
					$description = '';
				break;
			}
			$moments[$k]['VehicleTourMoment']['hour'] = $hour;
			$moments[$k]['VehicleTourMoment']['description'] = $description;
		}
		$this->updateVehicleReservation(array('first' => $moments[0], 'last' => end($moments)));
		return $moments;
	}

	public function updateVehicleReservation( $moments = array() ){
		if(sizeof($moments) != 2){
			return false;
		}
		$firstMoment = $moments['first'];
		$lastMoment = $moments['last'];
		$this->id = $firstMoment['VehicleTourMoment']['vehicle_tour_id'];
		$start = $this->field('start');
		$date = $this->field('date');
		if(!empty($start)){
			$this->saveField('end', date('H:i:00', $lastMoment['VehicleTourMoment']['hour']));
			$reservations = $this->VehicleReservation->find('all', array(
				'joins' => array(
					array(
						'table' => 'vehicle_reservations_vehicle_tours',
						'alias' => 'vrvt',
						'conditions' => array(
							'VehicleReservation.id = vrvt.vehicle_reservation_id',
							'vrvt.vehicle_tour_id' => $this->id
						)
					)
				),
				'fields' => array('VehicleReservation.id')
			));
			if(!empty($reservations)){
				foreach($reservations as $reservation){
					$start = $date . ' ' . date('H:i:00', $firstMoment['VehicleTourMoment']['hour']);
					$end = $date . ' ' . date('H:i:00', $lastMoment['VehicleTourMoment']['hour']);
					$this->VehicleReservation->id = $reservation['VehicleReservation']['id'];
					$this->VehicleReservation->saveField('start', $start);
					$this->VehicleReservation->saveField('end', $end);
				}
			}
		}

	}

}
