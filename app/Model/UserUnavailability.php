<?php

class UserUnavailability extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'User',
		'Job' => array(
			'foreignKey' => 'model_id'
		)
	);

}
