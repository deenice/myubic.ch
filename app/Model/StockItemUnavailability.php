<?php

class StockItemUnavailability extends AppModel {

	public $useTable = 'stock_item_unavailabilities';

	public $actsAs = array('Containable');

	public $belongsTo = array('StockItem', 'StockOrder');

}