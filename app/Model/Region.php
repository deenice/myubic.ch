<?php

class Region extends AppModel {

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array('Event');
}
