<?php
class ContactPeople extends AppModel {

  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Client',
    'Supplier' => array(
      'foreignKey' => 'client_id'
    ),
    'Partner' => array(
      'foreignKey' => 'client_id'
    ),
    'Transporter'
  );
  public $hasMany = array('Event', 'Wish');

  public function __construct($id = false, $table = null, $ds = null) {
    parent::__construct($id, $table, $ds);
    $this->virtualFields['full_name'] = sprintf('CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias);
    $this->virtualFields['full_name_phone_email'] = sprintf('CONCAT(%s.first_name, " ", %s.last_name, " - ", %s.phone, " - ", %s.email)', $this->alias, $this->alias, $this->alias, $this->alias);
  }

  public function customSearchContactPeople($column, $searchTerm, $columnSearchTerm, $config) {
    if($searchTerm){
      $config->conditions[] = array(
        'OR' => array(
          array('ContactPeople.first_name LIKE' => '%' . $searchTerm . '%'),
          array('ContactPeople.last_name LIKE' => '%' . $searchTerm . '%'),
          array('ContactPeople.name LIKE' => '%' . $searchTerm . '%'),
          array('Client.name LIKE' => '%' . $searchTerm . '%')
        )
      );
    }
    return $config;
  }

}
