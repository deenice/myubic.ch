<?php

class FBModule extends AppModel {

	public $useTable = 'fb_modules';

	public $actsAs = array('Containable');
	public $belongsTo = array(
		'FBCategory' => array(
			'foreignKey' => 'fb_category_id'
		),
		'User'
	);
	public $hasMany = array(
		'Document' => array(
      'foreignKey' => 'parent_id'
    ),
		'EventFBModule' => array(
			'foreignKey' => 'fb_module_id'
		),
		'OrderAssociation' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'fb_module'
			)
		),
		'OrderItem',
    'PlanningResource' => array(
      'foreignKey' => 'resource_id',
      'conditions' => array(
        'PlanningResource.resource_model' => 'fb_module'
      )
    ),
		'OutfitAssociation' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'fb_module'
			)
		),
		'PlanningBoardMoment' => array(
			'foreignKey' => 'default_id',
			'conditions' => array(
				'PlanningBoardMoment.type' => 'fb_module'
			),
			'order' => 'weight'
		)
	);
	public $hasAndBelongsToMany = array('Event');

	public function afterSave($created, $options = array()){
		$hasCompetences = $this->field('with_competences');
		if($created and $hasCompetences){
			$users = $this->User->TagUser->find('all', array(
				'conditions' => array(
					'tag_id' => 175
				)
			));
			$name = sprintf('F&B Test %s', $this->data['FBModule']['name']);
			foreach($users as $data){

				$competence = array(
					'name' => $name,
					'job' => 'test',
					'sector' => 'fb',
					'hierarchy' => '',
					'fb_module_id' => $this->id,
					'user_id' => $data['TagUser']['user_id']
				);

				$this->User->Competence->create();
				$this->User->Competence->save($competence);
			}
		}

	}

}
