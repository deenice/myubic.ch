<?php

class Configuration extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Place' => array(
			'counterCache' => true
		)
	);
	public $hasAndBelongsToMany = array('Tag');
	public $hasMany = array('Event');

}