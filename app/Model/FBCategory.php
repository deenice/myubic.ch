<?php

class FBCategory extends AppModel {

	public $useTable = 'fb_categories';

	public $actsAs = array('Containable');
	public $hasMany = array('FBModule', 'Order');

}
