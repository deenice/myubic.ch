<?php

class Moment extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('Event', 'Place');

	public $hasMany = array(
		'Option',
		'MomentTask' => array(
			'order' => 'MomentTask.weight'
		)
	);

}
