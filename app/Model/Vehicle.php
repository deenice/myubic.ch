<?php

class Vehicle extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('VehicleCompany', 'VehicleCompanyModel');

	public $hasMany = array('VehicleReservation');

    public $virtualFields = array(
    	'name_number' => 'CONCAT(Vehicle.name, " - ", Vehicle.matriculation)'
    );

    public function isAvailable($start = null, $end = null){
    	if(empty($start)){
    		$start = date('Y-m-d H:i:s');
    	}
    	if(empty($end)){
    		$end = date('Y-m-d H:i:s');
    	}
    }


	public function getAvailableVehiclesByDate($date){
		$data = $this->VehicleReservation->find('all', array(
			'conditions' => array(
				//'start <=' => $date,
				//'end >=' => $date,
				'confirmed' => 1
			),
			//'fields' => array('VehicleReservation.vehicle_id')
		));
		$reservations = array();
		foreach($data as $k => $reservation){
			if(date('Y-m-d', strtotime($reservation['VehicleReservation']['start'])) == $date){
				$reservations[$k] = array(
					'warning' => 1,
					'date' => $date,
					'vehicle_id' => $reservation['VehicleReservation']['vehicle_id']
				);
			} else {
				$reservations[$k] = array(
					'warning' => 0,
					'vehicle_id' => $reservation['VehicleReservation']['vehicle_id']
				);
			}
		}
		return $reservations;
	}

}