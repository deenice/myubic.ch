<?php class JobUser extends AppModel {

  public $actsAs = array('Containable');

  public $useTable = 'jobs_users';

  public $belongsTo = array(
    'Job',
    'User'
  );

  public function afterSave($created, $options = array()){
    // save salary in jobs_users
    $user_id = $this->field('user_id');
    $job_id = $this->field('job_id');
    $validated = $this->field('validated');
    $user = $this->User->findByid($user_id);
    $this->Job->id = $job_id;
    $salary = $this->Job->getSalary($user);
    if(!$validated) $this->saveField('salary', $salary, array('callbacks' => false));
  }

  // public function beforeDelete( $cascade = true ){
  //   $jobId = $this->field('job_id');
  //   $userId = $this->field('user_id');
  //   $this->loadModel('PlanningResource');
  //   $job = $this->Job->read(null, $jobId);
  //   CakeLog::write('myubic', json_encode($job));
  //   exit;
  //   //$this->PlanningResource->remove($job['Job']['event_id'], 'extra', $userId);
  // }

}
