<?php

class EventCategory extends AppModel {

    public $actsAs = array(
        'Containable'
    );

	public $hasMany = array(
		'Event'
	);

}