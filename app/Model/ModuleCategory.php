<?php

class ModuleCategory extends AppModel {

	public $actsAs = array('Containable');
	public $useTable = 'modules_categories';
	public $hasMany = array('Module', 'ModuleSubcategory');

}