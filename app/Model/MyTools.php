<?php

class MyTools extends AppModel {

	public function get_data($url = '') {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}

	public function getTravelTime($origin = '', $destination = ''){
		$destination = str_replace(' ', '+', $destination);
		$origin = str_replace(' ', '+', $origin);

		App::import('Model','Direction');
		$Direction = new Direction();

        $data = $Direction->find('first', array(
            'conditions' => array(
                'origin' => $origin,
                'destination' => $destination
            )
        ));

        if(!empty($data)){
            $infos['time_text'] = $data['Direction']['time_text'];
            $infos['time_value'] = $data['Direction']['time_value'];
            $infos['distance_value'] = $data['Direction']['distance_value'];
            $infos['distance_text'] = $data['Direction']['distance_text'];
            $infos['end_address'] = $data['Direction']['end_address'];
        } else {
            $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey');
            $travel = json_decode($this->file_get_contents_curl($url), true);
            if($travel['status'] == 'OK'){
                $infos['distance_text'] = $travel['routes'][0]['legs'][0]['distance']['text'];
                $infos['time_text'] = $travel['routes'][0]['legs'][0]['duration']['text'];
                $infos['distance_value'] = $travel['routes'][0]['legs'][0]['distance']['value'];
                $infos['time_value'] = $travel['routes'][0]['legs'][0]['duration']['value'];
                $infos['end_address'] = $travel['routes'][0]['legs'][0]['end_address'];
                $Direction->create();
                $direction = array(
                    'Direction' => array(
                        'origin' => $origin,
                        'destination' => $destination,
                        'end_address' => $infos['end_address'],
                        'distance_text' => $infos['distance_text'],
                        'distance_value' => $infos['distance_value'],
                        'time_text' => $infos['time_text'],
                        'time_value' => $infos['time_value']
                    )
                );
                $Direction->save($direction);
            } else {
                $infos['time'] = 0;
                $infos['distance'] = 0;
                $infos['travel'] = $travel;
            }
        }
        return $infos;
	}

	private function file_get_contents_curl($url) {
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

	    $data = curl_exec($ch);
	    curl_close($ch);

	    return $data;
	}

}
