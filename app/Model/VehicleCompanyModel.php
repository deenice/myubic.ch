<?php

class VehicleCompanyModel extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('VehicleCompany');

	public $hasMany = array('Vehicle', 'VehicleReservation');

}
