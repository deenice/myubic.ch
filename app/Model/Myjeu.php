<?php

class Myjeu extends AppModel {

	public $actsAs = array('Containable');

	public $useTable = 'myjeu';

	public $hasMany = array(
		'Image' => array(
			'className' => 'Document',
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Document.category' => 'myjeu_image'
			)
		),
		'Document' => array(
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Document.category' => 'myjeu_document'
			)
		)
	);

	public $hasAndBelongsToMany = array(
		'Tag' => array(
			'Tag.category' => 'myjeu'
		)
	);
}
