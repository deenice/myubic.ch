<?php

class EventRegion extends AppModel {

	public $useTable = 'events_regions';
	public $actsAs = array('Containable');
	public $belongsTo = array('Region', 'Event');


}
