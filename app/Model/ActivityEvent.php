<?php

class ActivityEvent extends AppModel {

  public $useTable = 'activities_events';
  public $actsAs = array('Containable');
  public $belongsTo = array('Activity', 'Event');

  public function afterSave($created, $options = array()){
    // we need to assign orders of selected activity for the event
    $activityId = $this->field('activity_id');
    $eventId = $this->field('event_id');
    $status = $this->field('status');
    $event = $this->Event->findById($eventId);
    if($created){
      if(!empty($event['Event']['confirmed_number_of_persons'])){
        $this->saveField('number_of_persons', $event['Event']['confirmed_number_of_persons'], array('callbacks' => false));
      }
    }
    $activity = $this->Activity->find('first', array(
      'contain' => array(
        'OrderAssociation',
        'PlanningBoardMoment'
      ),
      'conditions' => array(
        'id' => $activityId
      )
    ));
    if($status == 'selected'){
      if(!empty($activity['OrderAssociation'])){
        foreach($activity['OrderAssociation'] as $oa){
          $this->Event->Order->id = $oa['order_id'];
          $this->Event->Order->assign(array('order_model' => 'event', 'order_model_id' => $eventId, 'item_model' => 'activity', 'item_model_id' => $activityId));
        }
      }
    } elseif( $status == 'suggested' ){
      $this->saveField('number_of_groups', 0, array('callbacks' => false));
      $this->saveField('number_of_teams', 0, array('callbacks' => false));
      $this->saveField('number_of_animators', 0, array('callbacks' => false));
      $this->saveField('number_of_persons_per_team', 0, array('callbacks' => false));
    }
    $boards = $this->Event->PlanningBoard->find('all', array(
      'conditions' => array(
        'model' => 'event',
        'model_id' => $eventId,
        'type' => array('general', 'animation', 'billboard', 'practical_info')
      )
    ));
    if(!empty($boards) && $status == 'selected'){
      foreach($boards as $board){
        $this->Event->PlanningBoard->PlanningBoardMoment->create();
        $existingMoment = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'joins' => array(
            array(
              'table' => 'planning_resources',
              'alias' => 'pr',
              'conditions' => array(
                'pr.planning_board_moment_id = PlanningBoardMoment.id',
                'pr.resource_id' => $activityId,
                'pr.resource_model' => 'activity'
              )
            )
          ),
          'conditions' => array(
            'planning_board_id' => $board['PlanningBoard']['id']
          )
        ));
        $moment = array(
          'PlanningBoardMoment' => array(
            'id' => empty($existingMoment) ? '' : $existingMoment['PlanningBoardMoment']['id'],
            'name' => $activity['Activity']['name'],
            'planning_board_id' => $board['PlanningBoard']['id'],
            'start' => $event['Event']['start_hour'],
            'end' => $event['Event']['end_hour'],
            'type' => 'activity',
            'weight' => 100
          )
        );
        $this->Event->PlanningBoard->PlanningBoardMoment->save($moment);
        $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->create();
        $existingResource = $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->find('first', array(
          'conditions' => array(
            'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
            'resource_model' => 'activity',
            'resource_id' => $activityId,
          )
        ));
        $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->save(array(
          'PlanningResource' => array(
            'id' => empty($existingResource) ? '' : $existingResource['PlanningResource']['id'],
            'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
            'resource_model' => 'activity',
            'resource_id' => $activityId,
            'slug' => $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->getSlug($activityId, 'activity')
          )
        ));
      }
    }
    if(!empty($activity['PlanningBoardMoment']) && $status == 'selected' && !empty($boards)){
      foreach($activity['PlanningBoardMoment'] as $moment){
        foreach($boards as $board){
          $m = array(
            'PlanningBoardMoment' => array(
              'name' => $moment['name'],
              'remarks' => $moment['remarks'],
              'planning_board_id' => $board['PlanningBoard']['id'],
              'type' => $moment['type'],
              'weight' => 110 + $moment['weight'],
              'copied_from' => $moment['id']
            )
          );
          $this->Event->PlanningBoard->PlanningBoardMoment->create();
          $this->Event->PlanningBoard->PlanningBoardMoment->save($m);
          $r = array(
            'PlanningResource' => array(
              'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
              'resource_id' => $moment['default_id'],
              'resource_model' => $moment['type']
            )
          );
          $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->create();
          $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->save($r);
        }
      }
    }
  }

  public function beforeDelete( $cascade = true ){
    $eventId = $this->field('event_id');
    $activityId = $this->field('activity_id');
    $orders = $this->Activity->OrderItem->find('all', array(
      'joins' => array(
        array(
          'table' => 'order_groups',
          'alias' => 'og',
          'conditions' => array('og.id = OrderItem.order_group_id')
        ),
        array(
          'table' => 'orders',
          'alias' => 'o',
          'conditions' => array('o.id = og.order_id')
        )
      ),
      'conditions' => array(
        'OrderItem.model' => 'activity',
        'OrderItem.model_id' => $activityId,
        'o.model' => 'event',
        'o.model_id' => $eventId
       ),
      'fields' => array('o.id'),
      'group' => array('o.id')
    ));
    if(!empty($orders)){
      foreach($orders as $order){
        $this->Activity->OrderItem->OrderGroup->Order->delete($order['o']['id']);
      }
    }
  }
}
