<?php

class StockCategory extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array('StockFamily', 'StockSection');

	public $belongsTo = array('Company');

	public $virtualFields = array(
  	'code_name' => 'CONCAT(StockCategory.code, " - ", StockCategory.name)'
  );

	public $validate = array(
		'code' => array(
			'unique' => array(
				'rule' => 'isUnique',
				'message' => "This code is already taken."
			)
		)
	);

}
