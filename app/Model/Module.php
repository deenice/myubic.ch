<?php

class Module extends AppModel {

	public $actsAs = array('Containable');
	public $belongsTo = array('ModuleCategory', 'ModuleSubcategory');
	public $hasAndBelongsToMany = array(
		'User' => array(
            'order' => 'User.full_name',
            'unique' => true
        )
	);
    public $hasMany = array(
        'ModuleUser' => array(
            'group' => 'ModuleUser.user_id'
        ), 
        'Document' => array(
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'category' => 'module'
            )
        )
    );


	public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required.'
            )
        ),
        'module_category_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A category is required.'
            )
        )
    );
}