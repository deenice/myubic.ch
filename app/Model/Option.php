<?php

class Option extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('Moment', 'Place', 'User');

	public function update($value, $event_id, $place_id, $date, $user_id, $morning = 0, $evening = 0){

		// first we get the correponding options
		$eventOptions = $this->find('all', array(
			'conditions' => array(
				'event_id' => $event_id,
				'place_id <>' => $place_id
			)
		));
		$placeOptions = $this->find('all', array(
			'conditions' => array(
				'event_id <>' => $event_id,
				'place_id' => $place_id
			)
		));
		$eventPlaceOption = $this->find('first', array(
			'conditions' => array(
				'place_id' => $place_id,
				'event_id' => $event_id
			),
			'fields' => 'Option.id'
		));

		// if an option is already present for place and event, we update it otherwise we create a new one
		if(!empty($eventPlaceOption['Option']['id'])){
			$this->id = $eventPlaceOption['Option']['id'];
		} else {
			$this->create();
		}
		$this->set('value', $value);
		$this->set('place_id', $place_id);
		$this->set('event_id', $event_id);
		$this->set('user_id',  $user_id);
		$this->set('date',  $date);
		$this->set('morning',  $morning);
		$this->set('evening',  $evening);
		$this->save();
		$this->id = null;

		// second we cancel all options for the event
		if(!empty($eventOptions) && $value == 'confirmed'){
			foreach($eventOptions as $option){
				$this->id = $option['Option']['id'];
				$this->set('value', 'cancelled');
				$this->save();
				$this->id = null;
			}
		}

		// third we set all options of the place to "occupied"
		if(!empty($placeOptions) && $value == 'confirmed'){
			foreach($placeOptions as $option){
				$this->id = $option['Option']['id'];
				$this->set('value', 'occupied');
				$this->save();
				$this->id = null;
			}
		}
	}

}