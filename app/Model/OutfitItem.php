<?php

class OutfitItem extends AppModel {

  public $actsAs = array('Containable');

  public $belongsTo = array(
    'Outfit'
  );

  public function afterSave($created, $options = array()){
    $name = $this->field('name');
    if(empty($name)){
      $this->delete($this->id);
    }
  }

}
