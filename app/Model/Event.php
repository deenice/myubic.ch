<?php

class Event extends AppModel {

  public $virtualFields = array(
    'code_name' => 'CONCAT(Event.code, " ", Event.name)',
    'modal_title' => 'CONCAT(Event.code, " ", Event.name)',
		'invoice_zip_city' => 'CONCAT(Event.invoice_zip, " ", Event.invoice_city)',
		'region_zip_city' => 'CONCAT(Event.region_zip, " ", Event.region_city)'
		//'turnover' => 'SUM(projected_turnover)'
  );

  public $actsAs = array(
    'Containable',
    'Icing.Versionable' => array(
      'contain' => array('Client', 'ContactPeople', 'Company', 'PersonInCharge', 'Date', 'Job', 'Moment')
    )
  );

	public $belongsTo = array(
		'Client',
		'ContactPeople' => array(
			'order' => 'ContactPeople.created DESC',
			'limit' => 1
		),
		'Company',
		'User',
		'PersonInCharge' => array(
			'className' => 'User',
			'foreignKey' => 'resp_id'
		),
		'EventCategory'
	);
	public $hasMany = array(
		'Search',
		'Option',
		'Date' => array(
			'order' => 'date ASC',
			'dependent' => true
		),
		'Job' => array(
			'order' => 'Job.hierarchy DESC',
			'dependent' => true
		),
		'EmptyJob' => array(
			'className' => 'Job',
			'conditions' => array(
				'EmptyJob.user_id' => null
			),
			'order' => 'EmptyJob.name'
		),
		'FilledJob' => array(
			'className' => 'Job',
			'conditions' => array(
				'FilledJob.user_id <>' => null
			),
			'order' => 'FilledJob.name'
		),
		'Moment' => array(
			'order' => 'weight, start_hour',
			'dependent' => true
		),
		'BeforeMoment' => array(
      'className' => 'Moment',
			'order' => 'start_hour ASC',
      'conditions' => array(
        'BeforeMoment.type' => 'before'
      )
		),
		'AfterMoment' => array(
      'className' => 'Moment',
			'order' => 'start_hour ASC',
      'conditions' => array(
        'AfterMoment.type' => 'after'
      )
		),
		'ActivityEvent' => array(
			'dependent' => true
		),
		'EventPlace' => array(
			'dependent' => true
		),
		'EventRegion' => array(
			'dependent' => true
		),
		'ConfirmedEventPlace' => array(
			'className' => 'EventPlace',
			'conditions' => array(
				'ConfirmedEventPlace.option' => array('confirmed', 'managed_by_client')
			)
		),
		'Document' => array(
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Document.category' => 'event_document'
			)
		),
		'Note' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'event'
			),
			'order' => array('Note.created DESC'),
			'dependent' => true
		),
		'SelectedActivity' => array(
			'className' => 'ActivityEvent',
			'conditions' => array(
				'SelectedActivity.status' => 'selected'
			)
		),
		'SuggestedActivity' => array(
			'className' => 'ActivityEvent',
			'conditions' => array(
				'SuggestedActivity.status' => 'suggested'
			)
		),
		'SelectedFBModule' => array(
			'className' => 'EventFBModule',
			'conditions' => array(
				'SelectedFBModule.status' => 'selected'
			)
		),
		'SuggestedFBModule' => array(
			'className' => 'EventFBModule',
			'conditions' => array(
				'SuggestedFBModule.status' => 'suggested'
			)
		),
		'EventFBModule' => array(
			'foreignKey' => 'fb_module_id'
		),
		'StartMoment' => array(
			'className' => 'Moment',
			'conditions' => array(
				'StartMoment.type' => 'start'
			)
		),
		'EndMoment' => array(
			'className' => 'Moment',
			'conditions' => array(
				'EndMoment.type' => 'end'
			)
		),
		'Checklist' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'Checklist.model' => 'event'
			),
			'dependent' => true
		),
		'Order' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'Order.model' => 'event'
			),
			'dependent' => true
		),
		'Children' => array(
			'className' => 'Event',
			'foreignKey' => 'parent_id'
		),
    'PlanningBoard' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'PlanningBoard.model' => 'event'
			),
			'order' => 'PlanningBoard.weight'
		)
	);

	public $hasOne = array(
		'FirstJob' => array(
			'className' => 'Job',
			'order' => 'FirstJob.start_time ASC',
		),
		'LastJob' => array(
			'className' => 'Job',
			'order' => 'LastJob.end_time DESC',
		),
		'GeneralPlanning' => array(
			'className' => 'PlanningBoard',
			'foreignKey' => 'model_id',
			'conditions' => array(
				'GeneralPlanning.model' => 'event',
				'GeneralPlanning.type' => 'general'
			),
		),
		'BillboardPlanning' => array(
			'className' => 'PlanningBoard',
			'foreignKey' => 'model_id',
			'conditions' => array(
				'BillboardPlanning.model' => 'event',
				'BillboardPlanning.type' => 'billboard'
			),
		),
		'ClientPlanning' => array(
			'className' => 'PlanningBoard',
			'foreignKey' => 'model_id',
			'conditions' => array(
				'ClientPlanning.model' => 'event',
				'ClientPlanning.type' => 'client'
			),
		)
	);

	public $hasAndBelongsToMany = array('Activity', 'FBModule', 'VehicleReservation', 'Region');

	public $validate = array(
		'name' => array(
      'required' => array(
        'rule' => array('notEmpty'),
        'message' => 'A name is required.'
      )
    )
	);

  public function afterSave($created, $options = array()){
    $colors = Configure::read('Events.colors');
    $calendarColor = $this->field('calendar_color');
    if(empty($calendarColor)){
      $this->saveField('calendar_color', $colors[rand(0, sizeof($colors)-1)]);
    }

		// reaffect owner of event checklists
		$this->Checklist->updateAll(array(
			'Checklist.user_id' => $this->field('resp_id')
		), array(
			'Checklist.model' => 'event',
			'Checklist.model_id' => $this->id
		));
		$this->updateOrderItems(); // in case date has changed or start/end hours
  }

	public function customSearchEvent($column, $searchTerm, $columnSearchTerm, $config) {
    	if($searchTerm){
    		$config->conditions[] = array(
    			'OR' => array(
    				array('Event.name LIKE' => '%' . $searchTerm . '%'),
    				array('Event.code LIKE' => '%' . $searchTerm . '%'),
    				array('Client.name LIKE' => '%' . $searchTerm . '%')
    			)
    		);
    	}
    	return $config;
	}

  public function getStartEndHours(){
    $firstJob = $this->Job->find('first', array(
      'conditions' => array(
        'Job.event_id' => $this->id
      ),
      'order' => 'Job.start_time ASC'
    ));
    $lastJob = $this->Job->find('first', array(
      'conditions' => array(
        'Job.event_id' => $this->id
      ),
      'order' => 'Job.end_time DESC'
    ));
    return array('start' => $firstJob['Job']['start_time'], 'end' => $lastJob['Job']['end_time']);
  }

	public function generateEvent($event, $start, $allDay = true) {

		$data = array();
		$classes = array();
		$data['id'] = $this->id;
		$data['title'] = $event['Event']['code_name'] . ' - ' . $event['Client']['name'];
		$data['start'] = $start;
		if (!empty($event['Event']['start_hour']) && !empty($event['Event']['end_hour'])) {
			$hour1 = $event['Event']['start_hour'];
			$hour2 = $event['Event']['end_hour'];
			$seconds1 = strtotime("1970-01-01 $hour1 UTC");
			$seconds2 = strtotime("1970-01-01 $hour2 UTC");
			$start1 = date('c', strtotime($start) + $seconds1);
			$data['start'] = $start1;
			if($hour2 < $hour1){
				$end = date('c', strtotime($start . "+1 day") + $seconds2);
			} else {
				$end = date('c', strtotime($start) + $seconds2);
			}
			$data['end'] = $end;
			$allDay = false;
		}
		$data['allDay'] = $allDay;
		// if ($event['Event']['status'] == 'confirmed') {
		// 	$classes[] = 'bg-blue';
		// } elseif ($event['Event']['status'] == 'elaboration') {
		// 	$classes[] = 'bg-yellow-crusta';
		// }
		// if ($event['Event']['company_id'] == 6) {
		// 	//$classes[] = 'ug';
		// }
		$classes[] = $event['EventCategory']['slug'];
		$classes[] = in_array($event['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default'))) ? 'offer' : '';
		$data['className'] = implode(' ', $classes);
		$data['html'] = '';
		// temporary
		$rand1 = rand(0, 1);
		$rand2 = rand(0, 1);
		if ($rand1 && $rand2) {
			$h = '60%';
		}
		if ($rand1 && !$rand2) {
			$h = '80%';
		}
		if (!$rand1 && $rand2) {
			$h = '80%';
		}
		if (!$rand1 && !$rand2) {
			$h = '100%';
		}
		//$data['html'] .= $rand1 ? '<div class="before"></div>' : '';
		//$data['html'] .= '<div class="event" style="height:'.$h.'"><p>';

		$flags = array();
		if(!empty($event['Event']['languages'])){
			$languages = explode(',', $event['Event']['languages']);
			foreach($languages as $lang){
				$flags[] = $lang == 'en' ? 'gb' : $lang;
			}
		}

		$data['html'] .= '<div class="event"><p>';
		if(!empty($event['Event']['start_hour'])){
			$data['html'] .= date('H:i', strtotime($event['Event']['start_hour']));
			if(!empty($event['Event']['end_hour'])){
				$data['html'] .= ' - ' . date('H:i', strtotime($event['Event']['end_hour']));
			}
			$data['html'] .= '<br>';
		}
		if(in_array($event['Event']['company_id'], array(4,5))){
			$data['html'] .= '<b>'.$event['Event']['name'].'</b>'.'<br>';
		} else {
			$data['html'] .= '<b>'.$event['Client']['name'].'</b>'.'<br>';
		}
		if(!empty($event['SelectedActivity']) || !empty($event['SuggestedActivity'])){
			if(!empty($event['SelectedActivity'])){
				foreach($event['SelectedActivity'] as $activity){
					$data['html'] .= '<i class="fa fa-trophy"></i> ' . $activity['Activity']['slug'] . ' ';
				}
			}
			if(empty($event['SelectedActivity']) && !empty($event['SuggestedActivity']) && in_array($event['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default')))){
				foreach($event['SuggestedActivity'] as $activity){
					if(!empty($activity['Activity']['slug'])) $data['html'] .= '<i class="fa fa-trophy"></i> ' . $activity['Activity']['slug'] . ' ';
				}
			}
			$data['html'] .= '<br />';
		}
		if(!empty($event['SelectedFBModule']) || !empty($event['SuggestedFBModule'])){
			if(!empty($event['SelectedFBModule'])){
				foreach($event['SelectedFBModule'] as $module){
					if(!empty($module['FBModule']['slug'])) $data['html'] .= '<i class="fa fa-cutlery"></i> ' . $module['FBModule']['slug'] . ' ';
				}
			}
			if(empty($event['SelectedFBModule']) && !empty($event['SuggestedFBModule']) && in_array($event['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default')))){
				foreach($event['SuggestedFBModule'] as $module){
					if(!empty($module['FBModule']['slug'])) $data['html'] .= '<i class="fa fa-cutlery"></i> ' . $module['FBModule']['slug'] . ' ';
				}
			}
			$data['html'] .= '<br />';
		}
		if(!empty($event['ConfirmedEventPlace'])){
			foreach($event['ConfirmedEventPlace'] as $cep){
				$data['html'] .= '<i class="fa fa-map-marker"></i> ' . $cep['Place']['name'] . ' ';
			}
			$data['html'] .= '<br />';
		}

		//$data['html'] .= 'BTS'.' - '.$event['Event']['confirmed_number_of_persons'].' PAX'.'<br>';
		if(!empty($flags)){
			foreach($flags as $flag){
				$data['html'] .= '<span class="flag-icon flag-icon-'.$flag.'"></span> ';
			}
		}
		$data['html'] .= !empty($event['Event']['confirmed_number_of_persons']) ? $event['Event']['confirmed_number_of_persons'] . ' PAX ' : '';
		$data['html'] .= ($this->getNumberOfJobs()) ? __('<i class="fa fa-user"></i> %s/%s', $this->getNumberOfJobs('filled'), $this->getNumberOfJobs()) : '<br>' . __('No job defined');
		$data['html'] .= '</p>';
		$data['html'] .= sprintf('<span class="btn btn-xs btn-company btn-company-%s"></span>', $event['Company']['class']);
		$data['html'] .= '</div>';
		//$data['html'] .= $rand2 ? '<div class="after"></div>' : '';
		//$data['html'] .= '<span class="percent" style="width:'.rand(10, 100).'%"></span>';

		$data['url'] = Router::url(array('controller' => 'events', 'action' => 'work', $event['Event']['id']), true);
		return $data;
	}

  public function generateExtraEvent( $data, $user = array(), $notify = false ){

		$View = new View();
    $Html = $View->loadHelper('Html');
    $Time = $View->loadHelper('Time');
    $ImageSize = $View->loadHelper('ImageSize');

    $output = array();
		$output['start'] = $data['Event']['confirmed_date'];
		$output['id'] = $data['Event']['id'] * 100000 + $data['Job']['id'];
		$output['title'] = empty($user) ? '' : $user['full_name'];
		$output['url'] = Router::url(array('controller' => 'events', 'action' => 'work', $data['Event']['id']));
		$output['backgroundColor'] = $data['Event']['calendar_color'];
		$output['allDay'] = true;
		$output['eventId'] = $data['Event']['id'];
		$classes[] = empty($user) ? 'empty' : '';
		$classes[] = $notify ? 'notify' : '';
		if($data['Event']['type'] == 'mission'){
			$classes[] = 'mission';
		}

		$output['className'] = implode(' ', $classes);

		if(empty($user)){
			$portrait = $Html->tag('i', '', array('class' => 'fa fa-user-times fa-2x'));
		} else {
			$portrait = !empty($user['Portrait']) ? $Html->link($Html->image($ImageSize->crop($user['Portrait'][0]['url'], 80, 80)), array('controller' => 'users', 'action' => 'modal', $user['id']), array('data-toggle' => 'modal', 'data-target' => '#modal-user-ajax', 'escape' => false)) : '';
		}

		$description = empty($user) ? '' : $Html->tag('strong', $user['full_name'], array('class' => 'user-name'));
		$description .= $Html->tag('span', $data['Job']['name'], array('class' => 'job-name'));
		if(isset($data['Event']['Client']['name'])) $description .= $Html->tag('span', $data['Event']['Client']['name'], array('class' => 'client-name'));
		$description .= $Html->tag('span', $data['Event']['name'], array('class' => 'event-name'));
		$description .= $Html->tag('span', sprintf('%s - %s', $Time->format('H:i', $data['Job']['start_time']), $Time->format('H:i', $data['Job']['end_time'])), array('class' => 'hours'));
		$company = $Html->tag('a', '', array('class' => 'btn btn-xs btn-company btn-company-' . $data['Event']['Company']['class'], 'style' => 'margin-top: 2px'));
		$output['html'] = sprintf('<table class="fc-event-staff"><tr><td>%s</td><td class="portrait">%s%s</td></tr></table>', $description, $portrait, $company );

    return $output;
  }

  public function getNumberOfJobs( $status = null ){
		$this->Job->contain(array('JobUser'));
    $jobs = $this->Job->find('all', array(
			'conditions' => array(
				'Job.event_id' => $this->id
			)
		));
		$total = 0;
		if(!empty($jobs)){
			foreach($jobs as $job){
				$enrolled = sizeof($job['JobUser']);
				// update staff enrolled value
				if($job['Job']['staff_enrolled'] == 0 && $enrolled > 0){
					$job['Job']['staff_enrolled'] = $enrolled;
					$this->Job->save($job, array('callbacks' => false));
				}
				if($status == 'filled'){
					$total = $total + $job['Job']['staff_enrolled'];
				} elseif($status == 'validated'){
					for($i=0; $i<$job['Job']['staff_enrolled']; $i++){
						if(isset($job['JobUser'][$i])) $total = $total + $job['JobUser'][$i]['validated'];
					}
				} else {
					$total = $total + $job['Job']['staff_needed'];
				}
			}
		}
    return $total;
  }

	public function getManpowerCosts(){
		$this->contain(array('Job' => array('JobUser')));
		$event = $this->findById($this->id);
		$hours = 0;
		$salary = 0;
		if(!empty($event['Job'])){
			foreach($event['Job'] as $job){
				$this->Job->id = $job['id'];
				$tmp = $this->Job->getManpowerCosts();
				$hours += $tmp['hours'];
				$salary += $tmp['salary'];
			}
		}
		return array('hours' => $hours, 'salary' => $salary);
	}

	public function sales($company = null, $year = null){

		App::import('Model','ReportingOption');
		$ReportingOption = new ReportingOption();

		$sales['month'] = array(
			'january' => array(),
			'february' => array(),
			'march' => array(),
			'april' => array(),
			'may' => array(),
			'june' => array(),
			'july' => array(),
			'august' => array(),
			'september' => array(),
			'october' => array(),
			'november' => array(),
			'december' => array()
		);

		foreach( $sales['month'] as $month => $data ){
			$start = date(sprintf('%s-m-01', $year), strtotime($month . ' 15'));
			$end = date(sprintf('%s-m-t', $year), strtotime($month . ' 15'));
			$start = new DateTime( sprintf('%s-%s-15', $year, $month) );
			$end = new DateTime( sprintf('%s-%s-15', $year, $month) );
			// using datetime to be sure to get 29th of february...
			$start->modify( 'first day of this month' );
			$end->modify( 'last day of this month' );
			$start =  $start->format('Y-m-d');
			$end =  $end->format('Y-m-d');
			$sales['month'][$month]['total'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'opening_date <=' => $end,
					'opening_date >=' => $start,
          'type' => 'standard'
				)
			));
			$sales['month'][$month]['null'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'opening_date <=' => $end,
					'opening_date >=' => $start,
					'crm_status' => 'null',
          'type' => 'standard'
				)
			));
			$sales['month'][$month]['accepted'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'opening_date <=' => $end,
					'opening_date >=' => $start,
					'crm_status' => array('confirmed', 'done'),
          'type' => 'standard'
				)
			));
			$sales['month'][$month]['refused'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'opening_date <=' => $end,
					'opening_date >=' => $start,
					'crm_status' => 'refused',
          'type' => 'standard'
				)
			));
			$sales['month'][$month]['in_progress'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'opening_date <=' => $end,
					'opening_date >=' => $start,
					'crm_status <>' => array('refused', 'null', 'confirmed', 'done', 'partner'),
          'type' => 'standard'
				)
			));

			$sales['month'][$month]['sent'] = $sales['month'][$month]['total'] - $sales['month'][$month]['null'];

			$sales['month'][$month]['effective_sell_rate'] = ($sales['month'][$month]['sent'] == 0) ? 0 : ($sales['month'][$month]['accepted'] / $sales['month'][$month]['sent'] * 100);

			if(in_array($company, array(4,5))){
				$sellRate = Configure::read('Reporting.balanced_sell_rate1');
			} else {
				$sellRate = Configure::read('Reporting.balanced_sell_rate');
			}
			$sales['month'][$month]['projected_sell_rate'] = ($sales['month'][$month]['sent'] == 0) ? 0 : (($sales['month'][$month]['accepted']+$sales['month'][$month]['in_progress']*$sellRate) / $sales['month'][$month]['sent'] * 100);
			if(empty($sales['month'][$month]['projected_sell_rate'])){
				$sales['month'][$month]['projected_sell_rate'] = $sellRate * 100;
			}
			$projected_turnover = 0;
			$balanced_turnover = 0;
			$effective_turnover = 0;
			$effective_turnover_margin = 0;

			$acceptedEventsInMonth = $this->find('all', array(
				'conditions' => array(
					'company_id' => $company,
					'confirmed_date <=' => $end,
					'confirmed_date >=' => $start,
					'crm_status' => array('done', 'confirmed'),
          'type' => 'standard'
				)
			));
			$numberOfAcceptedEventsInMonth = sizeof($acceptedEventsInMonth);

			if(!empty($acceptedEventsInMonth)){
				foreach($acceptedEventsInMonth as $event){
					switch($event['Event']['company_id']){
						case 2: //UBIC
						if(!empty($event['Event']['repartition_corporate_outing']) || !empty($event['Event']['repartition_team_building']) || !empty($event['Event']['repartition_events_creation']) || !empty($event['Event']['repartition_urbangaming']) || !empty($event['Event']['repartition_misc'])){
							$effective_turnover += !empty($event['Event']['repartition_corporate_outing']) ? $event['Event']['repartition_corporate_outing'] : 0;
							$effective_turnover_margin += !empty($event['Event']['repartition_corporate_outing']) ? $event['Event']['repartition_corporate_outing'] : 0;
							$effective_turnover += !empty($event['Event']['repartition_team_building']) ? $event['Event']['repartition_team_building'] : 0;
							$effective_turnover_margin += !empty($event['Event']['repartition_team_building']) ? $event['Event']['repartition_team_building'] : 0;
							$effective_turnover += !empty($event['Event']['repartition_events_creation']) ? $event['Event']['repartition_events_creation'] : 0;
							$effective_turnover_margin += !empty($event['Event']['repartition_events_creation']) ? $event['Event']['repartition_events_creation'] : 0;
							$effective_turnover += !empty($event['Event']['repartition_urbangaming']) ? $event['Event']['repartition_urbangaming'] : 0;
							$effective_turnover_margin += !empty($event['Event']['repartition_urbangaming']) ? $event['Event']['repartition_urbangaming'] : 0;
							$effective_turnover += !empty($event['Event']['repartition_misc']) ? $event['Event']['repartition_misc'] : 0;
						} else {
							if (!empty($event['Event']['invoice_amount'])) {
								$effective_turnover += $event['Event']['invoice_amount'];
							}
						}
						break;
						default:
						if (!empty($event['Event']['invoice_amount'])) {
							$effective_turnover += $event['Event']['invoice_amount'];
						}
						break;
					}
					if(!empty($event['Event']['invoice_amount'])){
						$projected_turnover += $event['Event']['invoice_amount'];
						if(!empty($event['Event']['repartition_effet_gourmand'])){
							$projected_turnover -= $event['Event']['repartition_effet_gourmand'];
						}
						if(!empty($event['Event']['repartition_maiergrill'])){
							$projected_turnover -= $event['Event']['repartition_maiergrill'];
						}
						if(!empty($event['Event']['repartition_misc'])){
							$projected_turnover -= $event['Event']['repartition_misc'];
						}
					} elseif(!empty($event['Event']['projected_turnover']) && empty($event['Event']['invoice_amount'])){
						$projected_turnover += $event['Event']['projected_turnover'];
					}
				}
			}

			$inProgressEventsInMonth = $this->find('all', array(
				'conditions' => array(
					'company_id' => $company,
					'confirmed_date <=' => $end,
					'confirmed_date >=' => $start,
					'crm_status <>' => array('done', 'confirmed', 'refused', 'null', 'partner'),
          'type' => 'standard'
				)
			));
			$numberOfInProgressEventsInMonth = sizeof($inProgressEventsInMonth);
			if(!empty($inProgressEventsInMonth)){
				foreach($inProgressEventsInMonth as $event){
					if(!empty($event['Event']['projected_turnover']) && empty($event['Event']['invoice_amount'])){
						$projected_turnover += $event['Event']['projected_turnover'] * $sales['month'][$month]['projected_sell_rate'] / 100;
					}
				}
			}

			$yearly_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'type' => 'yearly_budget'
				),
				'fields' => array('value')
			));

			$monthly_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'month' => date('n', strtotime($month)),
					'type' => 'monthly_budget'
				),
				'fields' => array('value')
			));

			$minimal_offers = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'month' => date('n', strtotime($month)),
					'type' => 'minimal_offers'
				),
				'fields' => array('value')
			));

			if(!empty($monthly_budget)){
				$sales['month'][$month]['monthly_budget'] = $monthly_budget['ReportingOption']['value'];
			} elseif(empty($monthly_budget) && !empty($yearly_budget)) {
				$sales['month'][$month]['monthly_budget'] = $yearly_budget['ReportingOption']['value'] / 12;
			} elseif(empty($yearly_budget)){
				$sales['month'][$month]['monthly_budget'] = 0;
			}

			$balanced_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
          'month' => date('n', strtotime(sprintf('%s-%s-15', $year, $month))),
					'type' => 'balanced_budget'
				),
				'fields' => array('value')
			));

			if(!empty($balanced_budget)){
				$sales['month'][$month]['balanced_budget'] = $balanced_budget['ReportingOption']['value'];
			} else {
				$sales['month'][$month]['balanced_budget'] = 0;
			}

			$sales['month'][$month]['minimal_offers'] = !empty($minimal_offers) ? $minimal_offers['ReportingOption']['value'] : 0;
			$sales['month'][$month]['effective_turnover'] = $effective_turnover;
			$sales['month'][$month]['effective_turnover_margin'] = $effective_turnover_margin;
			$sales['month'][$month]['projected_turnover'] = $projected_turnover;
			$sales['month'][$month]['averaged_turnover'] = $numberOfAcceptedEventsInMonth == 0 ? 0 : $projected_turnover / $numberOfAcceptedEventsInMonth;
			$sales['month'][$month]['accepted_events'] = $numberOfAcceptedEventsInMonth;
			$sales['month'][$month]['in_progress_events'] = $numberOfInProgressEventsInMonth;
			$sales['month'][$month]['projected_result'] = $projected_turnover - $sales['month'][$month]['balanced_budget'];
			$sales['month'][$month]['effective_result'] = (!empty($effective_turnover_margin) ? $effective_turnover_margin : $effective_turnover) - $sales['month'][$month]['balanced_budget'];
		}

		$managers = $this->User->find('list', array(
			'joins' => array(
				array(
					'table' => 'events',
					'alias' => 'Event',
					'conditions' => array('Event.resp_id = User.id')
				)
			),
			'conditions' => array(
				'role' => 'fixed',
				'Event.resp_id <>' => null,
				'Event.opening_date >=' => date(sprintf('%s-01-01', $year)),
				'Event.company_id' => $company,
				'Event.type <>' => 'mission'
			),
			'fields' => array('User.id', 'User.first_name'),
			'order' => array('User.first_name')
		));

		foreach($managers as $id => $manager){
			$sales['person'][$manager]['total'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'resp_id' => $id,
					'opening_date >=' => date(sprintf('%s-01-01', $year))
				)
			));
			$sales['person'][$manager]['realized'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'resp_id' => $id,
					'confirmed_date <=' => date(sprintf('%s-m-d', $year)),
					'confirmed_date >=' => date(sprintf('%s-01-01', $year)),
					'crm_status' => 'confirmed'
				)
			));
			$sales['person'][$manager]['null'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'resp_id' => $id,
					'opening_date >=' => date(sprintf('%s-01-01', $year)),
					'crm_status' => 'null'
				)
			));
			$sales['person'][$manager]['refused'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'resp_id' => $id,
					'opening_date >=' => date(sprintf('%s-01-01', $year)),
					'crm_status' => 'refused'
				)
			));
			$sales['person'][$manager]['accepted'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'resp_id' => $id,
					'opening_date >=' => date(sprintf('%s-01-01', $year)),
					'crm_status' => array('done', 'confirmed')
				)
			));
			$sales['person'][$manager]['conversion'] = $sales['person'][$manager]['total'] == 0 ? 0 : 100 * $sales['person'][$manager]['accepted'] / ($sales['person'][$manager]['total'] - $sales['person'][$manager]['null']);
		}

		return $sales;
	}

	public function accountingMatch(){
		$total = $this->field('invoice_amount');
		if(empty($total)){
			return false;
		}
		$companyId = $this->field('company_id');
		if($companyId == 2 || $companyId == 6){
			$check = 	$this->field('repartition_corporate_outing') +
							$this->field('repartition_team_building') +
							$this->field('repartition_digital_games') +
							$this->field('repartition_serious_game') +
							$this->field('repartition_events_creation') +
							$this->field('repartition_effet_gourmand') +
							$this->field('repartition_maiergrill') +
							$this->field('repartition_misc');
		} elseif($companyId == 4){
			$check = 	$this->field('repartition_eg_direct') +
							$this->field('repartition_eg_ubic') +
							$this->field('repartition_misc');
		} elseif($companyId == 5){
			$check = 	$this->field('repartition_mg_direct') +
							$this->field('repartition_mg_ubic') +
							$this->field('repartition_misc');
		}
		$check = round($check, 2);
		$accountingMatch = true;
		if($total != '0.00' && $check != 0 && $total != $check){
			$accountingMatch = false;
		}
		return $accountingMatch;
	}

	public function areChecklistsComplete(){
		$this->contain(array('Checklist'));
		$event = $this->findById($this->id);
		$complete = 1;
		if(!empty($event['Checklist'])){
			foreach($event['Checklist'] as $list){
				$this->Checklist->id = $list['id'];
				if($this->Checklist->isComplete()){
					$complete = $complete * 1;
				} else {
					$complete = $complete * 0;
				}
			}
		}
		return $complete;
	}

	public function updateChildren(){
		$this->contain(array('Children'));
		$event = $this->findById($this->id);
		if(!empty($event['Chidlren'])){
			foreach($event['Children'] as $child){
				//TODO: ...
			}
		}
	}

	public function updateOrderItems(){
		$items = $this->Order->OrderGroup->OrderItem->find('all', array(
			'joins' => array(
				array(
					'table' => 'order_groups',
					'alias' => 'og',
					'conditions' => array('OrderItem.order_group_id = og.id')
				),
				array(
					'table' => 'orders',
					'alias' => 'o',
					'conditions' => array('og.order_id = o.id')
				)
			),
			'conditions' => array(
				'o.model' => 'event',
				'o.model_id' => $this->id
			)
		));
		if(!empty($items)){
			foreach($items as $item){
				$this->Order->OrderGroup->OrderItem->id = $item['OrderItem']['id'];
				$this->Order->OrderGroup->OrderItem->updateUnavailability();
			}
		}
	}

  public function initPlanningBoards(){
    $types = array_values(Configure::read('PlanningBoards.types'));
    $keys = array_keys(Configure::read('PlanningBoards.types'));
		$boards = $this->PlanningBoard->find('all', array(
			'conditions' => array(
				'model_id' => $this->id,
				'model' => 'event',
				'type' => $types
			)
		));
		if(empty($boards)){
			foreach($types as $k => $type){
				if($keys[$k] == 'custom'){ continue; }
				$this->PlanningBoard->create();
				$this->PlanningBoard->save(array(
					'PlanningBoard' => array(
						'name' => $type,
						'model' => 'event',
						'model_id' => $this->id,
						'type' => $keys[$k],
						'weight' => $k
					)
				));
	    }
		}
  }

	public function convertExistingMoments(){
		$moments = $this->Moment->find('all', array(
			'conditions' => array(
				'event_id' => $this->id,
				'converted' => 0
			)
		));
		if(!empty($moments)){
			foreach($moments as $moment){
				$options = array(
					'conditions' => array(
						'model_id' => $this->id,
						'model' => 'event'
					)
				);
				$startHour = $this->field('start_hour');
				$endHour = $this->field('end_hour');
				$type = $moment['Moment']['type'];
				switch($moment['Moment']['type']){
					case 'activity':
					$options['conditions'][] = array(
						'type' => array('general', 'animation')
					);
					break;
					case 'fb_module':
					$options['conditions'][] = array(
						'type' => array('general', 'fb')
					);
					$type = 'fb';
					break;
					case 'start':
					$endHour = '';
					case 'end':
					$startHour = '';
					default:
					$options['conditions'][] = array(
						'type' => 'general'
					);
					break;
				}
				$boards = $this->PlanningBoard->find('all', $options);
				foreach($boards as $board){
					$planningMoment = array(
						'PlanningBoardMoment' => array(
							'planning_board_id' => $board['PlanningBoard']['id'],
							'name' => $moment['Moment']['name'],
							'type' => $type,
							'start' => empty($moment['Moment']['start_hour']) ? $startHour : $moment['Moment']['start_hour'],
							'end' => empty($moment['Moment']['end_hour']) ? $endHour : $moment['Moment']['end_hour'],
							'weight' => $moment['Moment']['weight'],
							'remarks' => $moment['Moment']['remarks'],
						)
					);
					$this->PlanningBoard->PlanningBoardMoment->create();
					if($this->PlanningBoard->PlanningBoardMoment->save($planningMoment)){
						$this->Moment->id = $moment['Moment']['id'];
						$this->Moment->saveField('converted', 1);
						if(!empty($moment['Moment']['activity_id'])){
							$resource = array(
								'PlanningResource' => array(
									'resource_id' => $moment['Moment']['activity_id'],
									'resource_model' => 'activity',
									'planning_board_moment_id' => $this->PlanningBoard->PlanningBoardMoment->id
								)
							);
						}
						if(!empty($moment['Moment']['fb_module_id'])){
							$resource = array(
								'PlanningResource' => array(
									'resource_id' => $moment['Moment']['fb_module_id'],
									'resource_model' => 'fb_module',
									'planning_board_moment_id' => $this->PlanningBoard->PlanningBoardMoment->id
								)
							);
						}
						$this->PlanningBoard->PlanningBoardMoment->PlanningResource->create();
						if(isset($resource)) $this->PlanningBoard->PlanningBoardMoment->PlanningResource->save($resource);
					}
				}
			}
		}
	}

  public function initPlanningBoardMoments(){
    // we need to find all ActivityEvent and EventFBModule and eventually create moments (if not existing)
    $aes = $this->ActivityEvent->find('all', array(
      'conditions' => array(
        'event_id' => $this->id
      )
    ));
		if(!empty($aes)){
			foreach($aes as $ae){
				// search for an existing moment with this activity
				$moments = $this->PlanningBoard->PlanningBoardMoment->find('all', array(
					'joins' => array(
						array(
							'table' => 'planning_boards',
							'alias' => 'pb',
							'conditions' => array(
								'PlanningBoardMoment.planning_board_id = pb.id',
								'pb.model' => 'event',
								'pb.model_id' => $this->id
							)
						)
					)
				));
			}
		}
    //debug($aes);exit;
  }

  public function getCode( $event = array(), $step = 1 ){
    if(!in_array($event['Event']['company_id'], array(4,5))){
      return '';
    }
    if (!empty($event['Event']['confirmed_date'])) {
      $year = date('y', strtotime($event['Event']['confirmed_date']));
    } else {
      $year = date('y');
    }
    $firstPart = 0;
    if($event['Event']['company_id'] == 4) $firstPart = 7;
    if($event['Event']['company_id'] == 5) $firstPart = 8;
    $last = $this->find('first', array(
      'conditions' => array(
        'company_id' => $event['Event']['company_id'],
        'code LIKE' => '%-' . $year
      ),
      'order' => 'code DESC',
      'fields' => array('code')
    ));
    if(!empty($last)){
      $code = explode('-', $last['Event']['code']);
      $partialCode = $code[1]+$step;
      if($partialCode < 10){
        $part = '00' . $partialCode;
      } elseif($partialCode < 100 AND $partialCode >= 10){
        $part = '0' . $partialCode;
      } else {
        $part = $partialCode;
      }
      $code = sprintf('%s-%s-%s', $firstPart, $part, $year);
    } else {
      $code = sprintf('%s-%s-%s', $firstPart, '001', $year);
    }
    $loop = true;
    while($loop){
      $existing = $this->find('first', array(
        'conditions' => array(
          'code' => $code
        )
      ));
      if(!$existing){
        $loop = false;
        return $code;
      } else {
        $code = $this->getCode($event, $step+1);
      }
    }
  }

}
