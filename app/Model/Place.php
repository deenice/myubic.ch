<?php
App::uses('Direction', 'Model');

class Place extends AppModel {

    public $actsAs = array(
        'Containable',
        'Icing.Versionable' => array(
            'contain' => array('Configuration', 'Tag', 'Material', 'IdealFor', 'Activity', 'Document', 'Thumbnail')
        )
    );

    public $virtualFields = array('zip_city' => 'CONCAT(Place.zip, " ", Place.city)');
    public $hasAndBelongsToMany = array(
        'Tag',
        'Material' => array(
            'className' => 'Tag',
            'conditions' => array(
                'category' => 'material'
            ),
            'with' => 'PlacesTags'
        ),
        'IdealFor' => array(
            'className' => 'Tag',
            'conditions' => array(
                'category' => 'ideal_for'
            ),
            'with' => 'PlacesTags'
        )
    );
    public $hasMany = array(
        'Activity',
        'Document' => array(
            'foreignKey' => 'parent_id'
        ),
        'Option' => array(
            'order' => 'date ASC'
        ),
        'Event',
        'EventPlace',
        'Configuration',
        'Moment',
        'Thumbnail' => array(
            'className' => 'Document',
            'foreignKey' => 'parent_id',
            'conditions' => array(
                'Thumbnail.category' => 'place_thumbnail'
            ),
            'limit' => 1
        ),
        'Note' => array(
          'foreignKey' => 'model_id',
          'conditions' => array(
            'model' => 'place'
          )
        )
    );
    public $belongsTo = array(
        'User',
        'Owner' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Referer' => array(
            'className' => 'User',
            'foreignKey' => 'referer'
        ),
        'LastModifiedBy' => array(
            'className' => 'User',
            'foreignKey' => 'last_modified_by'
        )
    );

	public $validate = array(
        'name' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A name is required.'
            )
        ),
        'latitude' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A latitude is required.'
            )
        ),
        'longitude' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A longitude is required.'
            )
        )
    );

    public function getTravelTime($origin, $destination){

        $Direction = new Direction();

        $origin = str_replace(' ', '+', $origin);
        $originUBIC = 'Route+du+Petit-Moncor+1c+1752+Villars-sur-Glane';
        $destination = str_replace(' ', '+', $destination);

        $data = $Direction->find('first', array(
            'conditions' => array(
                'origin' => $origin,
                'destination' => $destination
            )
        ));
        $dataUbic = $Direction->find('first', array(
            'conditions' => array(
                'origin' => $originUBIC,
                'destination' => $destination
            )
        ));
        if(!empty($data)){
            $infos['time'] = $data['Direction']['time'];
            $infos['distance'] = $data['Direction']['distance'];
        } else {
            $url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$origin.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey');
            $travel = json_decode(file_get_contents($url), true);
            if($travel['status'] == 'OK'){
                $infos['distance'] = $travel['routes'][0]['legs'][0]['distance']['text'];
                $infos['time'] = $travel['routes'][0]['legs'][0]['duration']['text'];
                $Direction->create();
                $direction = array(
                    'Direction' => array(
                        'origin' => $origin,
                        'destination' => $destination,
                        'time' => $infos['time'],
                        'distance' => $infos['distance']
                    )
                );
                $Direction->save($direction);
            } else {
                $infos['time'] = 0;
                $infos['distance'] = 0;
            }
        }
        if(!empty($dataUbic)){
            $infos['timeUBIC'] = $dataUbic['Direction']['time'];
            $infos['distanceUBIC'] = $dataUbic['Direction']['distance'];
        } else {
            $travel2 = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$originUBIC.'&destination='.$destination.'&language=fr&key=' . Configure::read('GoogleMapsAPIKey')), true);
            if($travel2['status'] == 'OK'){
                $infos['distanceUBIC'] = $travel2['routes'][0]['legs'][0]['distance']['text'];
                $infos['timeUBIC'] = $travel2['routes'][0]['legs'][0]['duration']['text'];
                $Direction->create();
                $direction = array(
                    'Direction' => array(
                        'origin' => $originUBIC,
                        'destination' => $destination,
                        'time' => $infos['timeUBIC'],
                        'distance' => $infos['distanceUBIC']
                    )
                );
                $Direction->save($direction);
            } else {
                $infos['timeUBIC'] = 0;
                $infos['distanceUBIC'] = 0;
            }
        }

        return $infos;
    }

  public function getFillingPercentage( $percent = false ){
    $place = $this->findById($this->id);
    $numberOfFields = sizeof($place['Place']);
    $counter = 0;
    foreach($place['Place'] as $field => $value){
      if(!is_null($value)){
        $counter++;
      }
    }
    if($percent){
      return round(100 * $counter / $numberOfFields, 2);
    }
    return $counter / $numberOfFields;
  }

  public function generateCalendarHtml($option, $date){
    $html = '';
    $html .= '<strong>'.$option['Place']['name'].'</strong>';
    $html .= $option['Place']['zip'] . ' ' . $option['Place']['city'];
    if(!empty($option['Event']) && !empty($option['Client'])){
      $html .= '<br>' . $option['Client']['name'];
      $html .= '<br>' . $option['Event']['code'] . ' ' . $option['Event']['name'];
      $html .= sprintf('<span class="btn btn-xs btn-company btn-company-%s"></span>', $option['Company']['class']);
    }
    $html .= sprintf('<div class="progress"><div class="progress-bar" style="width:%s%%"></div></div>', $this->getFillingPercentage(true));
    return $html;
  }
}
