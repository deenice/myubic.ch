<?php

class Artist extends AppModel {

    public $actsAs = array(
        'Containable'
    );

	
	public $hasAndBelongsToMany = array(
		'Tag',
		'ArtistTags' => array(
    		'className' => 'Tag',
    		'conditions' => array(
    			'ArtistTags.category = "artist"'
    		)
    	)
    );

    public function customSearchArtist($column, $searchTerm, $columnSearchTerm, $config) {
        if($searchTerm){
            $config->conditions[] = array(
                'OR' => array(
                    array('Artist.name LIKE' => '%' . $searchTerm . '%'),
                    array('Artist.contact_person_first_name LIKE' => '%' . $searchTerm . '%'),
                    array('Artist.contact_person_last_name LIKE' => '%' . $searchTerm . '%'),
                    array('Artist.contact_person_phone LIKE' => '%' . $searchTerm . '%'),
                    array('Artist.contact_person_email LIKE' => '%' . $searchTerm . '%')
                )
            );
        }
        return $config;
    }

}