<?php

class StockItem extends AppModel {

  public $useTable = 'stock_items';

  public $virtualFields = array(
    'code_name' => 'CONCAT(StockItem.code, " ", StockItem.name)'
  );

  public $actsAs = array(
    'Containable',
    'Icing.Versionable' => array(
      'contain' => array('StockCategory', 'StockSection', 'StockFamily', 'Document', 'StockItemUnavailability', 'StockZone')
    )
  );

  public $belongsTo = array('Supplier', 'StockCategory', 'StockSection', 'StockFamily', 'StockZone', 'StockLocation', 'WarehouseStoragePoint', 'StockStorageType', 'Company');

  public $hasMany = array(
    'StockOrderBatch',
    'StockItemUnavailability' => array(
      'order' => 'StockItemUnavailability.start_date DESC'
    ),
    'Document' => array(
      'foreignKey' => 'parent_id',
      'conditions' => array(
        'Document.category' => 'stockitem'
      )
    ),
    'OrderItem'
  );
  public $hasAndBelongsToMany = array('StockInventory');

  // public $validate = array(
  //   'name' => array(
  //     'notEmpty' => array(
  //       'rule' => 'notEmpty',
  //       'message' => 'Name cannot be empty.'
  //     )
  //   ),
  //   'stock_category_id' => array(
  //     'notEmpty' => array(
  //       'rule' => 'notEmpty',
  //       'message' => 'Category cannot be empty.'
  //     )
  //   ),
  //   'stock_section_id' => array(
  //     'notEmpty' => array(
  //       'rule' => 'notEmpty',
  //       'message' => 'Section cannot be empty.'
  //     )
  //   ),
  //   'stock_family_id' => array(
  //     'notEmpty' => array(
  //       'rule' => 'notEmpty',
  //       'message' => 'Family cannot be empty.'
  //     )
  //   )
  // );

  public function afterSave($created, $options = array()){
    $archived = $this->field('archived');
    if($archived){
      $this->saveField('online', 0, array('callbacks' => false));
    }
  }

  public function customSearchStockItem($column, $searchTerm, $columnSearchTerm, $config) {
    if($searchTerm){
      $config->conditions[] = array(
        'OR' => array(
          array('StockItem.name LIKE' => '%' . $searchTerm . '%'),
          array('StockCategory.name LIKE' => '%' . $searchTerm . '%'),
          array('StockSection.name LIKE' => '%' . $searchTerm . '%'),
          array('StockFamily.name LIKE' => '%' . $searchTerm . '%')
        )
      );
    }
    return $config;
  }

  public function getActualAvailibility(){
    return $this->getAvailibility(date('Y-m-d'));
  }

  public function getAvailibility($date, $orderId = null, $eventId = null){

    if(empty($orderId)){
      $orderId = 0;
    }
    if(empty($eventId)){
      $eventId = 0;
    }

    $db = $this->getDataSource();
    $data = $db->fetchAll(
      'SELECT (si.quantity - sum(siu.quantity)) AS availability FROM stock_item_unavailabilities AS siu
      LEFT JOIN stock_orders AS so ON so.id = siu.stock_order_id
      LEFT JOIN events AS e ON e.id = siu.event_id
      INNER JOIN stock_items AS si ON si.id = siu.stock_item_id
      WHERE
      DATE_FORMAT(siu.start_date, "%Y-%m-%d") <= ? AND
      DATE_FORMAT(siu.end_date, "%Y-%m-%d") >= ? AND
      siu.stock_item_id = ? AND
      ((siu.event_id <> ? AND e.crm_status NOT IN ("null", "refused", "partner")) OR (siu.stock_order_id <> ? AND so.status <> "cancelled"))
      ',
      array($date, $date, $this->id, $eventId, $orderId)
    );

    if(!$eventId){
      $data = $db->fetchAll(
        'SELECT (si.quantity - sum(siu.quantity)) AS availability FROM stock_item_unavailabilities AS siu
        INNER JOIN stock_orders AS so ON so.id = siu.stock_order_id
        INNER JOIN stock_items AS si ON si.id = siu.stock_item_id
        WHERE
        DATE_FORMAT(siu.start_date, "%Y-%m-%d") <= ? AND
        DATE_FORMAT(siu.end_date, "%Y-%m-%d") >= ? AND
        siu.stock_item_id = ? AND
        siu.stock_order_id IS NOT NULL AND
        siu.event_id IS NULL AND
        ((siu.stock_order_id <> ? AND so.status <> "cancelled"))
        ',
        array($date, $date, $this->id, $orderId)
      );
    }

    if(!is_null($data[0][0]['availability'])){
      return $data[0][0]['availability'];
    } else {
      $total = $this->field('quantity');
      return $total;
    }
  }

  public function getUnavailibility($date, $status){
    App::import('Model', 'StockItemUnavailability');
    $siu = new StockItemUnavailability;
    $end = date('Y-m-d', strtotime($date . "+1 days"));
    $data = $siu->find('first', array(
      'conditions' => array(
        'stock_item_id' => $this->id,
        'status' => $status,
        'DATE_FORMAT(start_date, "%Y-%m-%d") <=' => $date,
        'DATE_FORMAT(end_date, "%Y-%m-%d") >=' => $date
      ),
      'fields' => array(
        'sum(quantity) as totalQuantity'
      )
    ));
    if(empty($data)){
      $availability = 0;
    } else {
      $availability = $data[0]['totalQuantity'];
    }
    return $availability;
  }

  public function checkAvailability($duration = 15, $amount = 0, $startDate = '', $stock_order_id = null, $event_id = null){

    if(!empty($startDate)){
      $start = date('Y-m-d', strtotime($startDate));
    } else {
      $start = date('Y-m-d');
    }
    $end = date('Y-m-d', strtotime($start . "+" . $duration . " days"));

    $availabilities = array();
    $missing = array();
    $dates = array();
    $sufficientStock = true;
    $emptyStock = false;
    for($i=0; $i<=$duration;$i++){
      $date = date('Y-m-d', strtotime($start . "+" . $i . " days"));
      $availability = $this->getAvailibility($date, $stock_order_id, $event_id);
      $availabilities[] = $availability;
      if($availability - $amount < 0 && $sufficientStock){
        $sufficientStock = false;
      }
      if($availability - $amount == 0 && !$emptyStock){
        $emptyStock = true;
      }
      $dates[$date] = $date;
    }
    if(!empty($availabilities)) $data['available'] = min($availabilities);
    $data['sufficientStock'] = $sufficientStock;
    $data['emptyStock'] = $emptyStock;
    $data['dates'] = $dates;
    return $data;
  }

  public function checkAvailabilityByHour($amount = 0, $start = '', $end = '', $model = '', $model_id = null){
    /*
     0: no stock available
     1: stock available
     2: empty stock
    */
    $quantity = $this->field('quantity');
    if($amount > $quantity){
      return 0;
    }
    $unavailabilities = $this->StockItemUnavailability->find('all', array(
      'conditions' => array(
        'event_id <>' => $model_id,
        'stock_item_id' => $this->id,
        'OR' => array(
          array('start_date BETWEEN ? AND ?' => array($start, $end)),
          array('? BETWEEN start_date AND end_date' => $start)
        )
      ),
      'group' => 'event_id',
      //'fields' => array('SUM(quantity) as ordered_quantity')
    ));
    $orderedQuantity = 0;
    if(!empty($unavailabilities)){
      foreach($unavailabilities as $tmp){
        $orderedQuantity += $tmp['StockItemUnavailability']['quantity'];
      }
    }
    if(is_null($orderedQuantity)){
      if($quantity < $amount){
        return 0;
      } elseif($quantity == $amount){
        return 2;
      } else {
        return 1;
      }
    } else {
      if($orderedQuantity > $quantity){
        return 0;
      } elseif($orderedQuantity == $quantity){
        return 2;
      } else {
        return 1;
      }
    }

  }

  public function checkAvailability1($duration = 15, $amount = 0, $startDate = ''){
    $availabilities = array('conflicts' => array('dates' => array(), 'orders' => array()));
    if(!empty($startDate)){
      $start = date('Y-m-d', strtotime($startDate));
    } else {
      $start = date('Y-m-d');
    }
    $end = date('Y-m-d', strtotime($start . "+" . $duration . " days"));
    $counter = 0;
    for($i=0; $i<$duration;$i++){
      $date = date('Y-m-d', strtotime($start . "+" . $i . " days"));
      $displayedDate = date('d.m.Y', strtotime($date));
      $availability = $this->getAvailibility($date);
      $availabilities[$date][$counter]['available'] = $availability;
      $availabilities[$date][$counter]['total'] = $this->field('quantity');
      $availabilities[$date][$counter]['potential'] = $availability - $amount;
      $availabilities['availabilities'][$displayedDate]['available'] = $availability;
      $availabilities['availabilities'][$displayedDate]['total'] = $this->field('quantity');
      $availabilities['availabilities'][$displayedDate]['potential'] = $availability - $amount;
      if($availability - $amount <= 0){
        $availabilities['conflicts']['dates'][] = $displayedDate;
        // search for potential orders
        $data = $this->StockOrderBatch->find('first', array(
          'conditions' => array(
            'StockOrderBatch.stock_item_id' => $this->id,
            'StockOrder.delivery_date <=' => $date,
            'StockOrder.return_date >=' => $date
          ),
          'contain' => array('StockOrder')
        ));
        if(!empty($data['StockOrder'])){
          $data['StockOrder']['date'] = $displayedDate;
          $data['StockOrder']['needed'] = abs($availability - $amount);
          $availabilities['conflicts']['orders'][$data['StockOrder']['id']][] = $data['StockOrder'];
        }
      }
      $counter++;
    }

    return $availabilities;
  }

}
