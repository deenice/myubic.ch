<?php

class StockOrder extends AppModel {

  public $virtualFields = array(
  	'delivery_zip_city' => 'CONCAT(StockOrder.delivery_zip, " ", StockOrder.delivery_city)',
  	'return_zip_city' => 'CONCAT(StockOrder.return_zip, " ", StockOrder.return_city)',
  	'invoice_zip_city' => 'CONCAT(StockOrder.invoice_zip, " ", StockOrder.invoice_city)',
  	'order_number' => 'CONCAT(StockOrder.id, " / ", StockOrder.client_id)'
  );

	public $actsAs = array('Containable', 'Icing.Versionable' => array(
	    'contain'          => array('History', 'StockOrderBatch', 'Client', 'ContactPeopleClient', 'ContactPeopleDelivery', 'ContactPeopleReturn', 'Company'), //contains for relative model to be included in the version.
	    'versions'         => '20',           //how many version to save at any given time (false by default unlimited)
	    'minor_timeframe'  => '10',          //Mark all previous versions if saved within 10 seconds of current version.  Easily cleanup minor_versions
	    'bind'             => false,         //if true, attach IcingVersionable as HasMany relationship for you onFind and if contained
	    'check_identical'  => true,         //if true, version is marked as minor, if the data is identical to last version
	    'ignore_identical' => true         //if true, no version is created, if the data is identical to last version
	));

	public $hasMany = array(
		'StockOrderBatch' => array(
			'order' => array('weight'),
      'dependent' => true
		),
		'StockItemUnavailability' => array(
      'dependent' => true
    ),
		'History' => array(
			'foreignKey' => 'parent_id',
			'order' => 'date DESC'
		),
		'OfferSendHistory' => array(
			'className' => 'History',
			'foreignKey' => 'parent_id',
			'order' => 'date DESC',
			'conditions' => array(
				'OfferSendHistory.reason = "offer_sent"'
			)
		),
		'OfferReviveHistory' => array(
			'className' => 'History',
			'foreignKey' => 'parent_id',
			'order' => 'date DESC',
			'conditions' => array(
				'OfferReviveHistory.reason = "revive_offer"'
			)
		),
		'DeliveryTransporterHistory' => array(
			'className' => 'History',
			'foreignKey' => 'parent_id',
			'order' => 'date DESC',
			'conditions' => array(
				'DeliveryTransporterHistory.reason = "delivery_transporter_notify"'
			)
		),
		'ReturnTransporterHistory' => array(
			'className' => 'History',
			'foreignKey' => 'parent_id',
			'order' => 'date DESC',
			'conditions' => array(
				'ReturnTransporterHistory.reason = "return_transporter_notify"'
			)
		),
		'Document' => array(
			'foreignKey' => 'parent_id',
			'conditions' => array(
				'Document.category' => 'stockorder'
			)
		),
		'IcingVersion' => array(
			'order' => 'IcingVersion.created DESC',
			'foreignKey' => 'model_id'
		),
		'StockOrderExtraHour',
		'StockOrderVehicleTour' => array(
			'order' => 'weight'
		),
	);

	public $belongsTo = array(
		'Client',
		'Company',
		'ContactPeopleClient' => array(
			'className' => 'ContactPeople',
			'foreignKey' => 'contact_people_id'
		),
		'ContactPeopleDelivery' => array(
			'className' => 'ContactPeople',
			'foreignKey' => 'delivery_contact_people_id'
		),
		'ContactPeopleReturn' => array(
			'className' => 'ContactPeople',
			'foreignKey' => 'return_contact_people_id'
		),
		'Responsable' => array(
			'className' => 'User',
			'foreignKey' => 'resp_id'
		),
		'ControlledBy' => array(
			'className' => 'User',
			'foreignKey' => 'controlled_by'
		),
		'Transporter',
		'DeliveryTransporter' => array(
			'className' => 'Transporter',
			'foreignKey' => 'delivery_transporter_id',
			'limit' => 1
		),
		'ReturnTransporter' => array(
			'className' => 'Transporter',
			'foreignKey' => 'return_transporter_id',
			'limit' => 1
		),
		'User'
	);

	public $hasAndBelongsToMany = array(
		'VehicleTour',
		'DeliveryTour' => array(
			'className' => 'VehicleTour',
			'conditions' => array(
				'type' => 'delivery'
			)
		),
		'ReturnTour' => array(
			'className' => 'VehicleTour',
			'conditions' => array(
				'type' => 'return'
			)
		)
	);

	public function afterSave($created, $options = array()){

		$orderStatus = $this->field('status');
		$data = $this->data;
		$request = Router::getRequest();
		App::import('Model', 'Export');
		$Export = new Export();

		if(!empty($data['StockOrder']['forced_net_total'])){
			if($data['StockOrder']['forced_net_total'] != $data['StockOrder']['net_total']){
				// we need to update TVA
				$difference = $data['StockOrder']['net_total'] - $data['StockOrder']['forced_net_total'];
				$newTva = $data['StockOrder']['forced_net_total'] - ($data['StockOrder']['forced_net_total'] / 1.08);
				if($newTva > 0) $this->saveField('tva', $newTva, array('callbacks' => false));
			}
		} elseif(!empty($data['StockOrder']['net_total'])) {
			$netTotal = $data['StockOrder']['net_total'];
			$newTva = $netTotal - ($netTotal / 1.08);
			if($newTva > 0) $this->saveField('tva', $newTva, array('callbacks' => false));
		}

		// if(empty($data['StockOrder']['replacement_costs'])){
		// 	$batches = $this->StockOrderBatch->find('all', array(
		// 		'conditions' => array(
		// 			'StockOrderBatch.stock_order_id' => $this->id
		// 		)
		// 	));
		// 	if(!empty($batches)){
		// 		foreach($batches as $b){
		// 			$this->StockOrderBatch->id = $b['StockOrderBatch']['id'];
		// 			$this->StockOrderBatch->saveField('invoice_replacement', 0, array('callbacks' => false));
		// 		}
		// 	}
		// }

		// if(empty($data['StockOrder']['extrahours_costs'])){
		// 	$extrahours = $this->StockOrderExtraHour->find('all', array(
		// 		'conditions' => array(
		// 			'StockOrderExtraHour.stock_order_id' => $this->id
		// 		)
		// 	));
		// 	if(!empty($extrahours)){
		// 		foreach($extrahours as $h){
		// 			$this->StockOrderExtraHour->id = $h['StockOrderExtraHour']['id'];
		// 			$this->StockOrderExtraHour->saveField('invoice', 0, array('callbacks' => false));
		// 		}
		// 	}
		// }

		if(!in_array($orderStatus, array('invoiced'))){
			App::import('Model', 'StockItemUnavailability');
			$siu = new StockItemUnavailability();

			// first we delete all stock items unavailabilities
			if(!empty($data['StockOrderBatch']) && !$request->is('ajax')){
				$siu->deleteAll(
					array('stock_order_id' => $data['StockOrder']['id']), false
				);
			}

			// then we create new stock items unavailabilities for each product
			if(!empty($data['StockOrderBatch'])){
				if(!empty($data['StockOrder']['status'])){
					if($orderStatus == 'offer'){
						$status = 'potential';
					} elseif(in_array($orderStatus, array('confirmed', 'processed', 'delivered', 'to_process', 'in_progress'))){
						$status = 'confirmed';
					} else {
						$status = '';
					}
				} else {
					$status = '';
				}

				$ids = array();
				foreach($data['StockOrderBatch'] as $batch){
					$obj = array();
					if(!empty($batch['StockOrderBatch'])){
						$obj = $batch['StockOrderBatch'];
					} else {
						$obj = $batch;
					}
					if(empty($obj['stock_order_id']) OR $obj['stock_item_id'] == -1){
						continue;
					}
					$siu->create();
					$data1 = array('StockItemUnavailability' => array(
						'stock_item_id' => $obj['stock_item_id'],
						'stock_order_id' => $obj['stock_order_id'],
						'quantity' => $obj['quantity'],
						'start_date' => date('Y-m-d', strtotime($data['StockOrder']['delivery_date'])),
						'end_date' => date('Y-m-d', strtotime($data['StockOrder']['return_date'])),
						'status' => $status
					));

					if(!empty($status) AND $data['StockOrder']['type'] != 'sale' AND $data['StockOrder']['delivery_date'] != '1970-01-01' AND $data['StockOrder']['return_date'] != '1970-01-01' && !$request->is('ajax')){
						$siu->save($data1);
					}

					$ids[] = $obj['stock_item_id'];
				}

				if(in_array($orderStatus, array('confirmed', 'in_progress', 'processed', 'delivered')) && !$request->is('ajax')){
					$this->addReconditionningUnavailability();
				}

			}

			if(!empty($data['StockOrder']['status']) AND $data['StockOrder']['status'] == 'cancelled'){
				$this->deleteStockItemUnavailabilities();
				$debug = Configure::read('debug');
				if($debug == 0 && !$request->is('ajax') && $request->params['action'] == 'cancel') $this->notify('cancellation');
			}
		}

		if(!empty($data['StockOrder']['status']) AND in_array($data['StockOrder']['status'], array('to_invoice', 'invoiced')) ){
			$this->deleteStockItemUnavailabilities();
		}
    if(in_array($orderStatus, array('confirmed', 'processed', 'delivered', 'to_process', 'in_progress')) && !$request->is('ajax')){
      $temp = array('StockOrder' => array('id' => $this->id, 'notify_depot' => 0));
      $notify = $this->getModifications();
      if($notify){
        $temp['StockOrder']['notify_depot'] = 1;
        $this->save($temp, array('callbacks' => false));
      } else {
        $this->save($temp, array('callbacks' => false));
      }
    }


	}

	public function deleteStockItemUnavailabilities(){
		App::import('Model', 'StockItemUnavailability');
		$siu = new StockItemUnavailability();
		$data = $siu->find('all', array(
			'conditions' => array(
				'stock_order_id' => $this->id,
				'status' => array('potential', 'confirmed')
			)
		));
		foreach($data as $d){
			$siu->delete($d['StockItemUnavailability']['id']);
		}
	}

	public function removeDuplicatedBatches($stock_item_ids){
		foreach($stock_item_ids as $id){
			$batches = $this->StockOrderBatch->find('all', array(
				'conditions' => array(
					'StockOrderBatch.stock_item_id' => $id,
					'StockOrderBatch.stock_order_id' => $this->id
				)
			));
			if(sizeof($batches) > 1){
				unset($batches[0]);
				foreach($batches as $batch){
					if($this->StockOrderBatch->delete($batch['StockOrderBatch']['id'])){

					}
				}
			}
		}
	}

	public function isProcessed() {
		if($this->field('status') == 'processed'){
			return true;
		} else {
			App::import('Model', 'StockOrderBatch');
			$sob = new StockOrderBatch();
			$batches = $sob->find('all', array(
				'conditions' => array(
					'stock_order_id' => $this->id
				)
			));
			if(!empty($batches)){
				$processed = 0;
				foreach($batches as $batch){
					if($batch['StockOrderBatch']['processed']){
						$processed++;
					}
				}
				if($processed == sizeof($batches)){
					$this->saveField('status', 'processed');
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}

	public function send( $params = array() ){

		$this->layout = 'default';

		App::uses('CakePdf', 'CakePdf.Pdf');
		App::uses('CakeEmail', 'Network/Email');

		$this->History->create();
		$this->Document->create();

		$stockorder = $this->find('first', array(
        	'conditions' => array(
        		'StockOrder.id' => $this->id
        	),
        	'contain' => array(
        		'StockOrderBatch' => array(
        			'StockItem'
        		),
        		'Client',
        		'Company',
        		'ContactPeopleClient',
        		'ContactPeopleDelivery',
        		'ContactPeopleReturn'
        	)
        ));
        $status = $stockorder['StockOrder']['status'];

        $history = array(
        	'History' => array(
        		'parent_id' => $this->id,
        		'reason' => $params['reason'],
        		'date' => date('Y-m-d H:i:s')
        	)
        );
        $this->History->save($history);

        $relativeUrl = 'files' . DS . 'documents' . DS . 'stockorders' . DS . $this->id . DS . $this->History->id . DS . $params['pdfFilename'];
        $absoluteUrl = WWW_ROOT . $relativeUrl;

        $CakePdf = new CakePdf(array(
          'orientation' => 'portrait',
    			'filename' => $params['pdfFilename'],
          'options' => array(
    				'footer-font-size' => 8,
    				'footer-html' => Router::url(array('controller' => 'stock_orders', 'action' => 'pdf_footer', 'full_base' => true))
    			),
    			'margin' => array(
    				'bottom' => 30,
    				'left' => 10,
    				'right' => 10,
    				'top' => 15,
    			)
        ));
        $CakePdf->template($params['pdfTemplate'],'default');
    		$CakePdf->viewVars(array(
    			'stockorder' => $stockorder,
    			'data' => $params['emailVars']
    		));
    		$CakePdf->write($absoluteUrl);

        $document = array(
        	'Document' => array(
        		'owner' => AuthComponent::user('id'),
        		'name' => basename($params['pdfFilename']),
        		'filename' => $params['pdfFilename'],
        		'url' => $relativeUrl,
        		'filesize' => filesize($relativeUrl),
        		'filetype' => 'application/pdf',
        		'category' => 'stockorder',
        		'parent_id' => $this->id,
        		'group' => $params['reason']
        	)
        );
        $this->Document->save($document);
        $this->History->saveField('document_id', $this->Document->id);

				$server = Configure::read('Debug.server');
				if($server == 'local'){
					$to = 'denis@une-bonne-idee.ch';
				} elseif($server == 'test'){
					$to = 'dev@myubic.ch';
				} elseif($server == 'prod'){
					$to = $params['emailVars']['to'];
				}

        $mail = new CakeEmail();
        $mail->config('festiloc');
        $mail->sender(array('info@festiloc.ch' => 'Info Festiloc'))
        	 ->from(array('info@festiloc.ch' => 'Info Festiloc'))
        	 ->replyTo('info@festiloc.ch')
             ->to($to)
             ->cc($params['emailVars']['cc'])
             ->subject($params['emailVars']['subject'])
             ->emailFormat('html')
             ->template($params['mailTemplate'])
             ->viewVars($params['emailVars']);
        if(isset($params['emailVars']['attachment'])){
        	if($params['emailVars']['attachment'] == 1){
        		$mail->attachments(
								array(
									$this->Document->field('filename') => array(
										'file' => $document['Document']['url'],
										'mimetype' => 'application/pdf'
									),
									'festiloc_cgv.pdf' => array(
										'file' => 'files/documents/festiloc_cgv.pdf',
										'mimetype' => 'application/pdf'
									)
								)
             	);
        	}
        } else {
        	$mail->attachments(
             	array(
             		$this->Document->field('filename') => array(
             			'file' => $document['Document']['url'],
             			'mimetype' => 'application/pdf'
             		),
								'festiloc_cgv.pdf' => array(
									'file' => 'files/documents/festiloc_cgv.pdf',
									'mimetype' => 'application/pdf'
								)
             	)
         	);
        }
        try {
        	if($mail->send()){
        		return true;
        	} else {
        		return false;
        	}
        } catch (Exception $e) {
        	echo 'Exception reçue : ',  $e->getMessage(), "\n";exit;
        }
	}

	public function verifyEmail($toemail, $fromemail, $getdetails = false){
		$details = '';
		$email_arr = explode("@", $toemail);
		$domain = array_slice($email_arr, -1);
		$domain = $domain[0];
		// Trim [ and ] from beginning and end of domain string, respectively
		$domain = ltrim($domain, "[");
		$domain = rtrim($domain, "]");
		if( "IPv6:" == substr($domain, 0, strlen("IPv6:")) ) {
			$domain = substr($domain, strlen("IPv6") + 1);
		}
		$mxhosts = array();
		if( filter_var($domain, FILTER_VALIDATE_IP) )
			$mx_ip = $domain;
		else
			getmxrr($domain, $mxhosts, $mxweight);
		if(!empty($mxhosts) )
			$mx_ip = $mxhosts[array_search(min($mxweight), $mxhosts)];
		else {
			if( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ) {
				$record_a = dns_get_record($domain, DNS_A);
			}
			elseif( filter_var($domain, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) ) {
				$record_a = dns_get_record($domain, DNS_AAAA);
			}
			if( !empty($record_a) )
				$mx_ip = $record_a[0]['ip'];
			else {
				$result   = "invalid";
				$details .= "No suitable MX records found.";
				return ( (true == $getdetails) ? array($result, $details) : $result );
			}
		}

		$connect = @fsockopen($mx_ip, 25);
		if($connect){
			if(preg_match("/^220/i", $out = fgets($connect, 1024))){
				fputs ($connect , "HELO $mx_ip\r\n");
				$out = fgets ($connect, 1024);
				$details .= $out."\n";

				fputs ($connect , "MAIL FROM: <$fromemail>\r\n");
				$from = fgets ($connect, 1024);
				$details .= $from."\n";
				fputs ($connect , "RCPT TO: <$toemail>\r\n");
				$to = fgets ($connect, 1024);
				$details .= $to."\n";
				fputs ($connect , "QUIT");
				fclose($connect);
				if(!preg_match("/^250/i", $from) || !preg_match("/^250/i", $to)){
					$result = "invalid";
				}
				else{
					$result = "valid";
				}
			}
		}
		else{
			$result = "invalid";
			$details .= "Could not connect to server";
		}
		if($getdetails){
			return array($result, $details);
		}
		else{
			return $result;
		}
	}

  public function customSearchOrder($column, $searchTerm, $columnSearchTerm, $config) {
  	if($searchTerm){
  		$config->conditions[] = array(
  			'OR' => array(
  				array('StockOrder.name LIKE' => '%' . $searchTerm . '%'),
  				array('StockOrder.id LIKE' => '%' . $searchTerm . '%'),
  				array('StockOrder.client_id LIKE' => '%' . $searchTerm . '%'),
  				array('Client.name LIKE' => '%' . $searchTerm . '%'),
  				array('ContactPeopleClient.name LIKE' => '%' . $searchTerm . '%')
  			)
  		);
  	}
  	return $config;
	}

    public function customSortOrder($column, $searchTerm, $columnSearchTerm, $config) {
    	if($searchTerm){
    		$config->conditions[] = array(
    			'OR' => array(
    				array('StockOrder.name LIKE' => '%' . $searchTerm . '%'),
    				array('StockOrder.id LIKE' => '%' . $searchTerm . '%'),
    				array('StockOrder.client_id LIKE' => '%' . $searchTerm . '%'),
    				array('Client.name LIKE' => '%' . $searchTerm . '%'),
    				array('ContactPeopleClient.name LIKE' => '%' . $searchTerm . '%')
    			)
    		);
    	}
    	return $config;
	}

	public function notify($reason = ''){

		App::uses('CakeEmail', 'Network/Email');

		$this->contain(array(
			'ContactPeopleClient'
		));
		$stockorder = $this->read(null, $this->id);
		$clientName = $stockorder['ContactPeopleClient']['full_name'];
		$clientEmail = $stockorder['ContactPeopleClient']['email'];

		if($reason == 'cancellation'){
			$subject = 'Annulation de votre commande';
			$mailTemplate = 'stockorder_cancel';
		} else {
			$subject = 'Modification de votre commande';
			$mailTemplate = 'stockorder_modification';
		}

		$subject .= ' no ' . $stockorder['StockOrder']['order_number'];

		$this->History->create();
        $history = array(
        	'History' => array(
        		'parent_id' => $this->id,
        		'reason' => $reason,
        		'date' => date('Y-m-d H:i:s')
        	)
        );
        $this->History->save($history);

		$server = Configure::read('Debug.server');
		if($server == 'local'){
			$to = 'denis@une-bonne-idee.ch';
		} elseif($server == 'test'){
			$to = 'dev@myubic.ch';
		} elseif($server == 'prod'){
			$to = $clientEmail;
		}

		$mail = new CakeEmail();
        $mail->sender(array('info@festiloc.ch' => 'Info Festiloc'))
        	 ->from(array('info@festiloc.ch' => 'Info Festiloc'))
        	 ->replyTo('info@festiloc.ch')
             ->to($to)
             ->cc('info@festiloc.ch')
             ->subject($subject)
             ->emailFormat('html')
             ->template($mailTemplate)
             ->viewVars(
             	array(
             		'stockorder' => $stockorder,
             		'civilities' => Configure::read('ContactPeople.civilities')
             	)
             );
        if(!empty($clientEmail) AND $mail->send()){
    		return true;
    	} else {
    		return false;
    	}

	}

	public function getPackaging(){
		$data = array();

		$numberOfPallets = $this->field('number_of_pallets');
		$numberOfRollis = $this->field('number_of_rollis');
		$numberOfPallets1 = $this->field('number_of_pallets1');
		$numberOfRollis1 = $this->field('number_of_rollis1');

		$numberOfPalletsXL = $this->field('number_of_pallets_xl');
		$numberOfRollisXL = $this->field('number_of_rollis_xl');
		$numberOfPalletsXL1 = $this->field('number_of_pallets_xl1');
		$numberOfRollisXL1 = $this->field('number_of_rollis_xl1');

		$data['pallets'] = is_null($numberOfPallets1) ? is_null($numberOfPallets) ? 0 : $numberOfPallets : $numberOfPallets1;
		$data['rollis'] = is_null($numberOfRollis1) ? is_null($numberOfRollis) ? 0 : $numberOfRollis : $numberOfRollis1;
		$data['pallets_xl'] = is_null($numberOfPalletsXL1) ? is_null($numberOfPalletsXL) ? 0 : $numberOfPalletsXL : $numberOfPalletsXL1;
		$data['rollis_xl'] = is_null($numberOfRollisXL1) ? is_null($numberOfRollisXL) ? 0 : $numberOfRollisXL : $numberOfRollisXL1;

		$counter = 0;
		foreach($data as $type => $d){
			if($d > 0){
				for($i=0; $i<$d;$i++){
					$data['distribution'][$counter] = $type;
					$counter++;
				}
			}
		}

		$data['total'] = $data['pallets'] + $data['pallets_xl'] + $data['rollis'] + $data['rollis_xl'];

		return $data;
	}

	public function computeTotal(){
		$stockorder = $this->findById($this->id);
		$total = $stockorder['StockOrder']['total_ht'];
		$total -= $stockorder['StockOrder']['fidelity_discount_amount'];
		$total -= $stockorder['StockOrder']['quantity_discount'];
		$total += $stockorder['StockOrder']['transportation_costs'];
		$total += !is_null($stockorder['StockOrder']['forced_delivery_costs']) ? $stockorder['StockOrder']['forced_delivery_costs'] : $stockorder['StockOrder']['delivery_costs'];
		$tva = $total * 8 / 100;
		$total = $total + $tva;
		$total = round($total * 20) / 20;
		if($stockorder['StockOrder']['deposit'] > 0){
			$invoicedTotal = $total - $stockorder['StockOrder']['deposit'];
		} else {
			$invoicedTotal = $total;
		}
		$one = $this->saveField('net_total', $total, array('callbacks' => false));
		$two = $this->saveField('forced_net_total', $total, array('callbacks' => false));
		$three = $this->saveField('invoiced_total', $invoicedTotal, array('callbacks' => false));
		$four = $this->saveField('tva', $tva, array('callbacks' => false));
		if($one AND $two AND $three AND $four){
			return true;
		} else {
			return false;
		}
	}

  public function addReconditionningUnavailability(){
		$this->StockItemUnavailability->create();
		$this->contain(array('StockOrderBatch'));
		$order = $this->findById($this->id);
		if(in_array($order['StockOrder']['status'], array('confirmed', 'processed', 'in_progress', 'delivered'))){
			$return_date = $order['StockOrder']['return_date'];
			$day = strtolower(date('N', strtotime($return_date)));
			$offset1 = 1;
			$offset2 = 4;
			switch($day){
				case 1:
					$offset2 = 2;
					break;
				case 2:
					$offset2 = 1;
					break;
				case 3:
					$offset2 = 4;
					break;
				case 4:
					$offset2 = 3;
					break;
				case 5:
				case 6:
				case 7:
					$offset2 = 2;
					break;
				default:
				break;
			}
			$start_date = date('Y-m-d', strtotime($return_date . sprintf(' +%d weekdays', $offset1)));
			$end_date = date('Y-m-d', strtotime($return_date . sprintf(' +%d weekdays', $offset2)));
      $data = array();
      foreach($order['StockOrderBatch'] as $batch){
				// get default duration of reconditionning
				if($batch['stock_item_id'] > 0){
					$stockitem = $this->StockOrderBatch->StockItem->find('first', array(
						'conditions' => array(
							'StockItem.id' => $batch['stock_item_id']
						),
						'fields' => array('default_reconditionning_duration')
					));
					if(!empty($stockitem['StockItem']['default_reconditionning_duration'])){
						$duration = $stockitem['StockItem']['default_reconditionning_duration'];
						$end_date = date('Y-m-d', strtotime($return_date . sprintf(' +%d weekdays', $duration == 1 ? 1 : $duration - 1)));
					}
				}

      	$tmp = array('StockItemUnavailability' => array(
            'stock_item_id' => $batch['stock_item_id'],
            'stock_order_id' => $batch['stock_order_id'],
            'quantity' => is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity'],
            'start_date' => $start_date,
            'end_date' => $end_date,
            'status' => 'reconditionning'
        ));
        array_push($data, $tmp);
      }
      if($this->StockItemUnavailability->saveMany($data)){
      	return true;
      } else {
      	return false;
      }
		} else {
			return false;
		}
	}

  public function getModifications( $returnData = false ){

    $this->contain(array(
        'Client',
        'ContactPeopleClient',
        'ContactPeopleDelivery',
        'ContactPeopleReturn',
        'StockOrderBatch' => array('StockItem' => array('StockCategory')),
        'IcingVersion',
        'DeliveryTransporter',
    ));
    $order = $this->findById($this->id);
    $notify = 0;
    $v = array();
    $output = array();
    $ignoredFields = Configure::read('StockOrders.ignored_fields');

    if(empty($order['StockOrder']['version_id_read'])){
      $version = $this->getLastVersion();
    } else {
      $version = $this->IcingVersion->findById($order['StockOrder']['version_id_read']);
      if(empty($version)){
        $version = $this->getLastVersion();
      }
    }

    //if (!empty($lastVersion) and !empty($v)) {
    if (!empty($version)) {
      $difference = $this->diffVersion($version['IcingVersion']['id']);
      $oldOrder = json_decode($version['IcingVersion']['json'], true);
      $actualOrder = $order;
      $output['products'] = array();
      $output['versions'] = array();

      if ($version['IcingVersion']['id'] != $order['StockOrder']['version_id_read'] || 1==1) {
        foreach ($difference as $model => $data) {
          $order[$model]['old'] = array();
          $order[$model]['actual'] = array();
          if (!empty($data)) {
            if ($model == 'StockOrderBatch') {
              $notify = 1;
              if($returnData){
                $output['oldProducts'] = array();
                $output['actualProducts'] = array();
                foreach ($oldOrder['StockOrderBatch'] as $batch) {
                  if ($batch['stock_item_id'] == -1) {
                    $key = '9999'.$batch['id'];
                    $output['products'][$key] = array('StockItem' => array('name' => $batch['name'], 'code' => '#'));
                  } else {
                    $key = $batch['stock_item_id'].$batch['id'];
                    $output['products'][$key] = $this->StockOrderBatch->StockItem->findById($batch['stock_item_id']);
                  }
                  $output['oldProducts'][$key] = $batch;
                }
                foreach ($actualOrder['StockOrderBatch'] as $batch) {
                  if ($batch['stock_item_id'] == -1) {
                    $key = '9999'.$batch['id'];
                    $output['products'][$key] = array('StockItem' => array('name' => $batch['name'], 'code' => '#'));
                  } else {
                    $key = $batch['stock_item_id'].$batch['id'];
                    $output['products'][$key] = $this->StockOrderBatch->StockItem->findById($batch['stock_item_id']);
                  }
                  $output['actualProducts'][$key] = $batch;
                }
                $order[$model]['old'] = $output['oldProducts'];
                $order[$model]['actual'] = $output['actualProducts'];
              }
            } elseif ($model != 'History') {
              foreach ($ignoredFields as $key) {
                unset($data[$key]);
              }
              if (!empty($data)) {
                $notify = 1;
                if($returnData){
                  foreach ($data as $key => $value) {
                    $output['versions'][$order['StockOrder']['id']][$model][$key]['old'] = empty($oldOrder[$model][$key]) ? '' : $oldOrder[$model][$key];
                    $output['versions'][$order['StockOrder']['id']][$model][$key]['new'] = empty($actualOrder[$model][$key]) ? '' : $actualOrder[$model][$key];
                  }
                }
              }
            }
          }
        }
      }
    }
    if($returnData){
      unset($order['IcingVersion']);
      $output['order'] = $order;
      return $output;
    } else {
      if($order['StockOrder']['status'] == 'cancelled'){
        return 0;
      } else {
        return $notify;
      }
    }
  }

  public function getLastVersion(){
    $lastVersion = $this->IcingVersion->find('first', array(
      'joins' => array(
        array(
          'table' => 'stock_orders',
          'alias' => 'StockOrder',
          'conditions' => array(
            'IcingVersion.model_id = StockOrder.id',
          ),
        ),
      ),
      'conditions' => array(
        'IcingVersion.model_id' => $this->id,
        'StockOrder.status' => array('in_progress', 'processed', 'delivered'),
      ),
      'order' => 'created ASC',
    ));
    return $lastVersion;
  }

	public function sales($company = null, $year = null){

		App::import('Model','ReportingOption');
		$ReportingOption = new ReportingOption();

		$sales['month'] = array(
			'january' => array(),
			'february' => array(),
			'march' => array(),
			'april' => array(),
			'may' => array(),
			'june' => array(),
			'july' => array(),
			'august' => array(),
			'september' => array(),
			'october' => array(),
			'november' => array(),
			'december' => array()
		);

		foreach( $sales['month'] as $month => $data ){
			$start = date(sprintf('%s-m-01', $year), strtotime($month . ' 15'));
			$end = date(sprintf('%s-m-t', $year), strtotime($month . ' 15'));
			$start = new DateTime( sprintf('%s-%s-15', $year, $month) );
			$end = new DateTime( sprintf('%s-%s-15', $year, $month) );
			// using datetime to be sure to get 29th of february...
			$start->modify( 'first day of this month' );
			$end->modify( 'last day of this month' );
			$start =  $start->format('Y-m-d');
			$end =  $end->format('Y-m-d');
			$sales['month'][$month]['total'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start
				)
			));
			$sales['month'][$month]['accepted'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress')
				)
			));
			$sales['month'][$month]['refused'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => 'cancelled'
				)
			));
			$sales['month'][$month]['in_progress'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => 'offer'
				)
			));
			$sales['month'][$month]['sent'] = $this->History->find('count', array(
				'joins' => array(
					array(
						'table' => 'stock_orders',
						'alias' => 'so',
						'conditions' => array(
							'so.id = History.parent_id'
						)
					)
				),
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'so.return_date >=' => $start,
					'so.return_date <=' => $end,
					'reason' => 'offer_sent'
				),
				'group' => array('parent_id')
			));
			$sales['month'][$month]['sell_rate'] = $sales['month'][$month]['accepted'] == 0 ? 0 : ($sales['month'][$month]['accepted'] / $sales['month'][$month]['total'] * 100);
			$data = $this->find('all', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress', 'processed')
				)
			));
			$turnover = 0;
			if(!empty($data)){
				foreach($data as $order){
					$total = $order['StockOrder']['total_ht'];
          if(!empty($order['StockOrder']['fidelity_discount_amount'])){
            $total -= $order['StockOrder']['fidelity_discount_amount'];
          }
          if(!empty($order['StockOrder']['quantity_discount'])){
            $total -= $order['StockOrder']['quantity_discount'];
          }
          if($total < 0){
            $total = 0;
          }
          $turnover += $total;
				}
			}
			$sales['month'][$month]['turnover'] = $turnover;
			$sales['month'][$month]['averaged_turnover'] = $sales['month'][$month]['accepted'] == 0 ? 0 : $turnover / $sales['month'][$month]['accepted'];
			$sales['month'][$month]['effective_sell_rate'] = ($sales['month'][$month]['sent'] == 0) ? 0 : ($sales['month'][$month]['accepted'] / $sales['month'][$month]['sent'] * 100);
			$sales['month'][$month]['projected_sell_rate'] = ($sales['month'][$month]['sent'] == 0) ? 0 : (($sales['month'][$month]['accepted']+$sales['month'][$month]['in_progress']*Configure::read('Reporting.balanced_sell_rate_festiloc')) / $sales['month'][$month]['sent'] * 100);

			$projected_turnover = 0;
			$balanced_turnover = 0;
			$effective_turnover = 0;
			$effective_turnover_margin = 0;

			$numberOfAcceptedOrdersInMonth = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress')
				)
			));
      $numberOfInProgressOrdersInMonth = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => 'offer'
				)
			));

      // compute projected turnover
      $ordersInMonth = $this->find('all', array(
				'conditions' => array(
					'company_id' => $company,
          'client_id <>' => null,
					'return_date <=' => $end,
					'return_date >=' => $start,
					'status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress', 'offer')
				),
        'fields' => array('invoiced_total', 'tva', 'total_deposit', 'total_ht', 'quantity_discount', 'fidelity_discount_amount', 'status')
			));

			if(!empty($ordersInMonth)){
				foreach($ordersInMonth as $order){
          if($order['StockOrder']['status'] == 'invoiced'){
            $tmp = $order['StockOrder']['invoiced_total'] - $order['StockOrder']['tva'] + $order['StockOrder']['total_deposit'];
            $effective_turnover += $tmp;
  					$projected_turnover += $tmp;
          } else {
            $tmp = $order['StockOrder']['total_ht'] - $order['StockOrder']['fidelity_discount_amount'] - $order['StockOrder']['quantity_discount'];
            if($order['StockOrder']['status'] == 'offer'){
              $tmp = $tmp * Configure::read('Reporting.balanced_sell_rate_festiloc');
            }
            $projected_turnover += $tmp;
          }
				}
			}

			$yearly_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'type' => 'yearly_budget'
				),
				'fields' => array('value')
			));

			$monthly_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'month' => date('n', strtotime($month)),
					'type' => 'monthly_budget'
				),
				'fields' => array('value')
			));

			$minimal_offers = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'month' => date('n', strtotime($month)),
					'type' => 'minimal_offers'
				),
				'fields' => array('value')
			));

			if(!empty($monthly_budget)){
				$sales['month'][$month]['monthly_budget'] = $monthly_budget['ReportingOption']['value'];
			} elseif(empty($monthly_budget) && !empty($yearly_budget)) {
				$sales['month'][$month]['monthly_budget'] = $yearly_budget['ReportingOption']['value'] / 12;
			} elseif(empty($yearly_budget)){
				$sales['month'][$month]['monthly_budget'] = 0;
			}

			$balanced_budget = $ReportingOption->find('first', array(
				'conditions' => array(
					'company_id' => $company,
					'year' => $year,
					'month' => date('n', strtotime(sprintf('%s-%s-15', $year, $month))),
					'type' => 'balanced_budget'
				),
				'fields' => array('value')
			));

			if(!empty($balanced_budget)){
				$sales['month'][$month]['balanced_budget'] = $balanced_budget['ReportingOption']['value'];
			} else {
				$sales['month'][$month]['balanced_budget'] = 0;
			}

			$sales['month'][$month]['minimal_offers'] = !empty($minimal_offers) ? $minimal_offers['ReportingOption']['value'] : 0;
			$sales['month'][$month]['effective_turnover'] = $effective_turnover;
			$sales['month'][$month]['effective_turnover_margin'] = $effective_turnover_margin;
			$sales['month'][$month]['projected_turnover'] = $projected_turnover;
			$sales['month'][$month]['averaged_turnover'] = $numberOfAcceptedOrdersInMonth == 0 ? 0 : $effective_turnover / $numberOfAcceptedOrdersInMonth;
			$sales['month'][$month]['accepted_events'] = $numberOfAcceptedOrdersInMonth;
			$sales['month'][$month]['in_progress_events'] = $numberOfInProgressOrdersInMonth;
			$sales['month'][$month]['projected_result'] = $projected_turnover - $sales['month'][$month]['balanced_budget'];
			$sales['month'][$month]['effective_result'] = (!empty($effective_turnover_margin) ? $effective_turnover_margin : $effective_turnover) - $sales['month'][$month]['balanced_budget'];

		}

		$managers = $this->User->find('list', array(
			'joins' => array(
				array(
					'table' => 'stock_orders',
					'alias' => 'so',
					'conditions' => array('so.user_id = User.id')
				)
			),
			'conditions' => array(
				'company_id' => $company,
				'so.created >=' => date(sprintf('%s-01-01', $year))
			),
			'fields' => array('User.id', 'User.first_name'),
			'order' => array('User.first_name')
		));
		if(empty($managers)){
			$managers = array(null => 'Festiloc');
		}
		foreach($managers as $id => $manager){
			$sales['person'][$manager]['total'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'user_id' => $id,
					'created >=' => date(sprintf('%s-01-01', $year))
				)
			));
			$sales['person'][$manager]['realized'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'user_id' => $id,
					'created >=' => date(sprintf('%s-01-01', $year)),
					'status' => 'confirmed'
				)
			));
			$sales['person'][$manager]['refused'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'user_id' => $id,
					'created >=' => date(sprintf('%s-01-01', $year)),
					'status' => 'cancelled'
				)
			));
			$sales['person'][$manager]['accepted'] = $this->find('count', array(
				'conditions' => array(
					'company_id' => $company,
					'user_id' => $id,
					'created >=' => date(sprintf('%s-01-01', $year)),
					'status' => array('confirmed', 'delivered', 'to_invoice', 'invoiced', 'in_progress')
				)
			));
			if($sales['person'][$manager]['total'] == 0){
				$sales['person'][$manager]['conversion'] = 0;
			} else {
				$sales['person'][$manager]['conversion'] = $sales['person'][$manager]['accepted'] / $sales['person'][$manager]['total'] * 100;
			}

		}


		return $sales;

	}

	public function getNetTotal(){

		$net_total = $this->field('net_total');
		$invoiced_total = $this->field('invoiced_total');
		$deposit = $this->field('deposit');

		$total = $net_total;
		if(!empty($invoiced_total)){
			$total = $invoiced_total;
			if($deposit != 0){
				$total = $total + $deposit;
			}
		}
		return $total;

	}

	public function saveTotals( $updateInvoicedTotal = false ){
		// used in edit_invoice
		$this->contain(array(
			'StockOrderBatch' => array(
				'conditions' => array(
					'StockOrderBatch.invoice_replacement' => 1
				),
				'StockItem' => array('fields' => array('price', 'replacement_price'))
			),
			'StockOrderExtraHour'
		));
		$order = $this->findById($this->id);
		$replacementCosts = 0;
		$extraHoursCosts = 0;

		// compute replacement costs
		if(!empty($order['StockOrderBatch'])){
			foreach($order['StockOrderBatch'] as $batch){
				$price = (!empty($batch['StockItem']['replacement_price'])) ? $batch['StockItem']['replacement_price'] : $batch['StockItem']['price'] * 5;
				if(!empty($batch['delivered_quantity'])){
					$difference = $batch['delivered_quantity'] - $batch['returned_quantity'];
				} else {
					$difference = $batch['quantity'] - $batch['returned_quantity'];
				}
				$replacementCosts += $price * $difference;
			}
		}
		$this->saveField('replacement_costs', $replacementCosts, array('callbacks' => false));

		$status = $this->field('status');
		if(!in_array($status, array('to_invoice', 'invoiced'))){
			$this->saveField('invoiced_total', 0, array('callbacks' => false));
		}

		// compute extra hours costs
		if(!empty($order['StockOrderExtraHour'])){
			foreach($order['StockOrderExtraHour'] as $hour){
				if($hour['invoice']) $extraHoursCosts += $hour['duration'];
			}
		}
		$this->saveField('extrahours_costs', $extraHoursCosts, array('callbacks' => false));

		// compute subtotal ht (used in edit_invoice)
		$subtotal = $order['StockOrder']['total_ht'];
		$subtotal += empty($order['StockOrder']['forced_delivery_costs']) ? $order['StockOrder']['delivery_costs'] : $order['StockOrder']['forced_delivery_costs'];
		$subtotal += $extraHoursCosts;
		$subtotal += $replacementCosts;
		$subtotal -= $order['StockOrder']['fidelity_discount_amount'];
		$subtotal -= $order['StockOrder']['quantity_discount'];
		$this->saveField('subtotal_ht', $subtotal, array('callbacks' => false));

		// compute TVA
		$tva = $subtotal * 8 / 100;
		$this->saveField('tva', $tva, array('callbacks' => false));

		// compute net total
		$net_total = $subtotal + $tva;
		$net_total = round($net_total / 0.05) * 0.05;
		$this->saveField('net_total', $net_total, array('callbacks' => false));
		$this->saveField('forced_net_total', $net_total, array('callbacks' => false));

		// compute net total with deposit and total deposit
		$deposit = $order['StockOrder']['deposit'] + $order['StockOrder']['on_site_payment'];;
		if(!empty($deposit)){
			$net_total_with_deposit = $net_total - $deposit;
			$this->saveField('net_total_with_deposit', $net_total_with_deposit, array('callbacks' => false));
			if($net_total_with_deposit < 0) $this->saveField('invoiced_total', $net_total_with_deposit, array('callbacks' => false));
		} else {
			$net_total_with_deposit = $net_total;
			$this->saveField('net_total_with_deposit', $net_total, array('callbacks' => false));
		}
		$this->saveField('total_deposit', $deposit, array('callbacks' => false));

		// update invoiced total if true or if is null (when field is emptied)
    $invoiced_total = $this->field('net_total');
		if($updateInvoicedTotal OR is_null($order['StockOrder']['invoiced_total'])){
			$invoiced_total = $net_total;
			if($order['StockOrder']['deposit']){
				$invoiced_total -= $order['StockOrder']['deposit'];
			}
			if($order['StockOrder']['on_site_payment']){
				$invoiced_total -= $order['StockOrder']['on_site_payment'];
			}
			$this->saveField('invoiced_total', $invoiced_total, array('callbacks' => false));
		}

		// compute arrondi
    $invoiced_total = $this->field('invoiced_total');
    $total_deposit = $this->field('total_deposit');
		if(!empty(floatval($invoiced_total)) || $invoiced_total == '0.00'){
      $invoiced_total = floatval($invoiced_total);
			$arrondi = !empty($invoiced_total) ? ($invoiced_total - $net_total_with_deposit) / 1.08 : 0;
			$tva = ($invoiced_total + $total_deposit) / 1.08 * 0.08;
      // we need to save tva = 0 only if it's an invoice
			if(in_array($order['StockOrder']['status'], array('invoiced', 'to_invoice'))) $this->saveField('tva', $tva, array('callbacks' => false));
		} else {
			$arrondi = 0;
		}
		$this->saveField('arrondi', $arrondi, array('callbacks' => false));

		// compute solde
		$solde = $this->field('net_total_with_deposit');
    if(isset($_GET['temp']) && $_GET['temp'] == 1){
      $this->saveField('solde', $solde, array('callbacks' => false));
    } elseif($invoiced_total < 0 && !isset($_GET['temp'])){
      $this->saveField('solde', $invoiced_total, array('callbacks' => false));
    } else {
			$this->saveField('solde', $solde, array('callbacks' => false));
		}

		return true;

	}

	public function fixTotalHt(){
		$this->contain(array(
			'StockOrderBatch'
		));
		$total = 0;
		$order = $this->findById($this->id);
		foreach($order['StockOrderBatch'] as $batch){
			$total += $batch['quantity'] * $batch['price'] * $batch['coefficient'] * abs($batch['discount'] - 100) / 100;
		}
		$this->saveField('total_ht', $total, array('callbacks' => false));
	}


}
