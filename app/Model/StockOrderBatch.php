<?php

class StockOrderBatch extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'StockItem' => array(
			'order' => 'StockItem.code'
		), 
		'StockOrder'
	);

}