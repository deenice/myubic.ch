<?php

class ActivityUnavailability extends AppModel {

	public $useTable = 'activity_unavailabilities';
	public $actsAs = array('Containable');
	public $belongsTo = array('Activity', 'Event');

}
