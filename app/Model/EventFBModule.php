<?php

class EventFBModule extends AppModel {

	public $useTable = 'events_fb_modules';
	public $actsAs = array('Containable');
	public $belongsTo = array(
		'FBModule' => array(
			'foreignKey' => 'fb_module_id'
		),
		'Event'
	);

	public function afterSave($created, $options = array()){
		// we need to assign orders of selected fb module for the event
		$fbModuleId = $this->field('fb_module_id');
		$eventId = $this->field('event_id');
		$status = $this->field('status');
    $event = $this->Event->findById($eventId);
		if($created){
			$event = $this->Event->findById($eventId);
			if(!empty($event['Event']['confirmed_number_of_persons'])){
				$this->saveField('number_of_persons', $event['Event']['confirmed_number_of_persons'], array('callbacks' => false));
			}
			if(!empty($event['Event']['offer_number_of_persons']) && empty($event['Event']['confirmed_number_of_persons'])){
				$this->saveField('number_of_persons', $event['Event']['offer_number_of_persons'], array('callbacks' => false));
			}
		}
		$fbModule = $this->FBModule->find('first', array(
			'contain' => array(
				'OrderAssociation',
        'PlanningBoardMoment'
			),
			'conditions' => array(
				'id' => $fbModuleId
			)
		));
		if($status == 'selected'){
			if(!empty($fbModule['OrderAssociation'])){
				foreach($fbModule['OrderAssociation'] as $oa){
					$this->Event->Order->id = $oa['order_id'];
					$this->Event->Order->assign(array('order_model' => 'event', 'order_model_id' => $eventId, 'item_model' => 'fb_module', 'item_model_id' => $fbModuleId));
				}
			}
		} elseif( $status == 'suggested' ){
      $this->saveField('number_of_staff', 0, array('callbacks' => false));
      $this->saveField('number_of_tables', 0, array('callbacks' => false));
      $this->saveField('number_of_persons_per_table', 0, array('callbacks' => false));
      $this->saveField('number_of_buffets', 0, array('callbacks' => false));
      $this->saveField('number_of_teams', 0, array('callbacks' => false));
      $this->saveField('number_of_persons_per_team', 0, array('callbacks' => false));
    }
		$boards = $this->Event->PlanningBoard->find('all', array(
      'conditions' => array(
        'model' => 'event',
        'model_id' => $eventId,
        'type' => array('general', 'fb')
      )
    ));
    if(!empty($boards) && $status == 'selected'){
      foreach($boards as $board){
        $this->Event->PlanningBoard->PlanningBoardMoment->create();
        $existingMoment = $this->Event->PlanningBoard->PlanningBoardMoment->find('first', array(
          'joins' => array(
            array(
              'table' => 'planning_resources',
              'alias' => 'pr',
              'conditions' => array(
                'pr.planning_board_moment_id = PlanningBoardMoment.id',
                'pr.resource_id' => $fbModuleId,
                'pr.resource_model' => 'fb_module'
              )
            )
          ),
          'conditions' => array(
            'planning_board_id' => $board['PlanningBoard']['id']
          )
        ));
        $moment = array(
          'PlanningBoardMoment' => array(
            'id' => empty($existingMoment) ? '' : $existingMoment['PlanningBoardMoment']['id'],
            'name' => $fbModule['FBModule']['name'],
            'planning_board_id' => $board['PlanningBoard']['id'],
            'start' => $event['Event']['start_hour'],
            'end' => $event['Event']['end_hour'],
            'type' => 'fb_module',
            'weight' => 100
          )
        );
        $this->Event->PlanningBoard->PlanningBoardMoment->save($moment);
        $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->create();
        $existingResource = $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->find('first', array(
          'conditions' => array(
            'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
            'resource_model' => 'fb_module',
            'resource_id' => $fbModuleId,
          )
        ));
        $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->save(array(
          'PlanningResource' => array(
            'id' => empty($existingResource) ? '' : $existingResource['PlanningResource']['id'],
            'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
            'resource_model' => 'fb_module',
            'resource_id' => $fbModuleId,
            'slug' => $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->getSlug($fbModuleId, 'fb_module')
          )
        ));
      }
    }
    if(!empty($fbModule['PlanningBoardMoment']) && $status == 'selected' && !empty($boards)){
      foreach($fbModule['PlanningBoardMoment'] as $moment){
        foreach($boards as $board){
          $m = array(
            'PlanningBoardMoment' => array(
              'name' => $moment['name'],
              'remarks' => $moment['remarks'],
              'planning_board_id' => $board['PlanningBoard']['id'],
              'type' => $moment['type'],
              'weight' => 110 + $moment['weight'],
              'copied_from' => $moment['id']
            )
          );
          $this->Event->PlanningBoard->PlanningBoardMoment->create();
          $this->Event->PlanningBoard->PlanningBoardMoment->save($m);
          $r = array(
            'PlanningResource' => array(
              'planning_board_moment_id' => $this->Event->PlanningBoard->PlanningBoardMoment->id,
              'resource_id' => $moment['default_id'],
              'resource_model' => $moment['type']
            )
          );
          $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->create();
          $this->Event->PlanningBoard->PlanningBoardMoment->PlanningResource->save($r);
        }
      }
    }
	}

}
