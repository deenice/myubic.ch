<?php

class ChecklistBatch extends AppModel{

	public $actsAs = array('Containable');

  public $belongsTo = array('Checklist');

  public $hasMany = array(
    'ChecklistTask' => array(
      'order' => 'weight',
      'dependent' => true
    )
  );

  public function afterSave($created, $options = array()){

    if($created){
      $numberOfBatches = $this->find('count', array(
        'conditions' => array(
          'checklist_id' => $this->field('checklist_id')
        )
      ));
      $this->saveField('weight', $numberOfBatches);
    }

    $id = $this->id;

    // reset weights, get phases order by weight and reassign in loop
    $batches = $this->find('all', array(
      'conditions' => array(
        'checklist_id' => $this->field('checklist_id')
      ),
      'order' => 'weight'
    ));

    if(!empty($batches)){
      foreach($batches as $weight => $phase){
        $phase['ChecklistBatch']['weight'] = $weight;
        $phase['ChecklistBatch']['position'] = 1;
        if($weight != sizeof($batches) - 1){
          $phase['ChecklistBatch']['position'] = 1;
          $phase['ChecklistBatch']['neighbor'] = isset($batches[$weight+1]) ? $batches[$weight+1]['ChecklistBatch']['id'] : null;
        } else {
          $phase['ChecklistBatch']['position'] = 0;
          $phase['ChecklistBatch']['neighbor'] = isset($batches[$weight-1]) ? $batches[$weight-1]['ChecklistBatch']['id'] : null;
        }
        //$this->id = $phase['ChecklistBatch']['id'];
        $this->save($phase, array('callbacks' => false));
      }
    }
    $this->id = $id;

  }

}
