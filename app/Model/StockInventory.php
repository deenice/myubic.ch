<?php

class StockInventory extends AppModel {

  public $useTable = 'stock_inventories';

	public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array(
    'StockItem' => array(
      'order' => 'code ASC'
    )
  );

  public $virtualFields = array(
    'name_dates' => 'CONCAT(StockInventory.name, " (", DATE_FORMAT(StockInventory.start, "%d.%m.%Y"), "-", DATE_FORMAT(StockInventory.end, "%d.%m.%Y"), ")")'
  );

}
