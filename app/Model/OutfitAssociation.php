<?php

class OutfitAssociation extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Outfit',
		'Activity' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'activity'
			)
		),
		'FBModule' => array(
			'foreignKey' => 'model_id',
			'conditions' => array(
				'model' => 'fb_module'
			)
		)
	);

}
