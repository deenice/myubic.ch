<?php

class Export extends AppModel {

  public function cresus($company = null, $type = null){
    $date = date('d.m.Y');
    App::import('Model','Client');
    App::import('Model','StockOrder');

    $Client = new Client();
    $StockOrder = new StockOrder();

    $output = '';
		$separator = ";";
		$extension = "csv";

		$civilities = Configure::read('ContactPeople.civilities1');

    switch($company){
			case 'festiloc':
				if($type == 'clients'){
					$output = '';
					$clients = $Client->find('all', array(
						'contain' => array('ContactPeople'),
						'joins' => array(
							array(
								'table' => 'clients_companies',
								'alias' => 'ClientCompany',
								'conditions' => array(
									'ClientCompany.client_id = Client.id',
									'ClientCompany.company_id = 3'
								)
							)
						)
					));
					foreach($clients as $client){
						if(empty($client['ContactPeople'][0])){
							$cp = array();
							$civility = '';
						} else {
							$cp = $client['ContactPeople'][0];
							$civility = empty($cp['civility']) ? '' : !is_numeric($cp['civility']) ? $civilities[$cp['civility']] : '';
						}
						$output .= $client['Client']['id'];
						$output .= $separator;
						$output .= sprintf('"%s"',$client['Client']['name']);
						$output .= $separator;
						$output .= $civility;
						$output .= $separator;
						$output .= empty($cp['last_name']) ? '' : sprintf('"%s"',$cp['last_name']);
						$output .= $separator;
						$output .= empty($cp['first_name']) ? '' : sprintf('"%s"',$cp['first_name']);
						$output .= $separator;
						$output .= empty($cp['email']) ? '' : sprintf('"%s"', $cp['email']);
						$output .= $separator;
						$output .= empty($cp['phone']) ? '' : sprintf('"%s"', $cp['phone']);
						$output .= $separator;
						$output .= $client['Client']['zip'];
						$output .= $separator;
						$output .= sprintf('"%s"',$client['Client']['city']);
						$output .= $separator;
						$output .= sprintf('"%s"',$client['Client']['address']);
						$output .= "\r\n";
					}
					if(file_put_contents(WWW_ROOT . sprintf('files/export/export_festiloc_clients.%s', $extension), $output)){
            return true;
          } else {
            return false;
          }
				}
				if($type == 'factures'){
					$orders = $StockOrder->find('all', array(
            'contain' => array(
              'Client',
              'ContactPeopleClient'
            ),
						'conditions' => array(
							'export_status' => array('to_export'),
							//'id' => array('2784'),
						)
					));
          if(AuthComponent::user('id') == 1 && 1==2){
            $orders = $StockOrder->find('all', array(
  						'conditions' => array(
  							'status' => array('invoiced'),
  						)
  					));
          }
					$output = '';
					foreach($orders as $k => $order){
            $address = '';
						$StockOrder->id = $order['StockOrder']['id'];
						$output .= $order['StockOrder']['id'] + 9000000;
						$output .= $separator;
						$output .= $order['StockOrder']['client_id'];
						$output .= $separator;
						$concern = $order['StockOrder']['order_number'];
						//if(!empty($order['StockOrder']['name'])) $concern .= ' - ' . $order['StockOrder']['name'];
						$output .= sprintf('"%s"',$concern);
						$output .= $separator;
						$output .= 100; // id article
						$output .= $separator;
						$output .= 1; // quantity
						$output .= $separator;
            if(!is_null($order['StockOrder']['invoiced_total'])){
              $total = $order['StockOrder']['invoiced_total'];
            } elseif(!is_null($order['StockOrder']['forced_net_total']) AND is_null($order['StockOrder']['invoiced_total'])){
              $total = $order['StockOrder']['forced_net_total'];
            } else {
              $total = $order['StockOrder']['net_total'];
            }
						$output .= $total;
						$output .= $separator;
						$output .= empty($order['StockOrder']['invoice_date']) ? date('d.m.y', strtotime($order['StockOrder']['return_date'])) : date('d.m.y', strtotime($order['StockOrder']['invoice_date']));
						$output .= $separator;
            if(!empty($order['StockOrder']['invoice_address'])){
              if(!empty($order['StockOrder']['invoice_client'])){
                $address .= $order['StockOrder']['invoice_client'] . "|";
              } else {
                $address .= $order['Client']['name'] . "|";
              }
              if(!empty($order['StockOrder']['invoice_contact_person'])){
                $address .= $order['StockOrder']['invoice_contact_person'] . "|";
              } elseif($order['ContactPeopleClient']['full_name'] != $order['Client']['name']) {
                $address .= $order['ContactPeopleClient']['full_name'] . "|";
              }
              $address .= $order['StockOrder']['invoice_address'];
              $address .= "|" . $order['StockOrder']['invoice_zip'] . ' ' . $order['StockOrder']['invoice_city'];
            } else {
              $address = '';
            }
            $output .= $address;
						$output .= $separator;
						$output .= "$";
						$output .= "\r\n";
            $StockOrder->saveField('export_status', 'exported', array('callbacks' => false));
            $StockOrder->saveField('status', 'invoiced', array('callbacks' => false));
					}
					if(file_put_contents(WWW_ROOT . sprintf('files/export/export_festiloc_factures.%s', $extension), $output)){
            return true;
          } else {
            return false;
          }
				}
			break;

			case 'ubic':
			break;

			default:
			break;
		}


  }

}
