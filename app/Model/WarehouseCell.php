<?php 

class WarehouseCell extends AppModel {

	public $hasTable = array('warehouse_cells');

	public $actsAs = array('Containable');

	public $belongsTo = array('Warehouse');

	public $hasMany = array('WarehouseNode');

}