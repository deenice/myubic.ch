<?php 

class Warehouse extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array('WarehouseCell', 'WarehousePassage', 'WarehouseNode', 'WarehouseStoragePoint', 'WarehouseNodeDistance', 'WarehouseBestRoute');

	public function getShortestRoute($batch, $warehouseId){

		$population = array();
		$bestRoute = array();
		$spids = array();
		$stockitemIds = array();

		$numberOfGenerations = 50;
		$populationSize = 20;
		$start = '44';
		$shortestDistance = 0;

		foreach($batch as $b){
			$spids[] = $b['StockItem']['warehouse_storage_point_id'];
			$stockitemIds[] = $b['StockItem']['id'];
		}
		for($i=0;$i<$populationSize;$i++){
			shuffle($spids);
			$population[] = array_merge(array(0 => $start), $spids);
		}
		$training = $population;


		sort($training[0]);
		$existingBestRoute = $this->WarehouseBestRoute->find('first', array(
			'conditions' => array(
				'WarehouseBestRoute.warehouse_id' => $warehouseId,
				'WarehouseBestRoute.warehouse_storage_point_ids_sorted' => implode(',', $training[0])
			),
			'order' => array('distance'),
			// 'cache' => implode(',', $training[0]),
			// 'cacheConfig' => 'short'
		));

		if(!empty($existingBestRoute)){
			$bestRoute = explode(',', $existingBestRoute['WarehouseBestRoute']['warehouse_storage_point_ids']);
		} else {
			for($generationStep = 0; $generationStep < $numberOfGenerations; $generationStep++){
				// compute distance (score) of generated chromosome, i.e. between each storage point

				foreach($training as $key => $chromosome){
					$distance = 0;
					for($i=0;$i<sizeof($training);$i++){
						if(isset($chromosome[$i]) AND isset($chromosome[$i+1])){
							$distance = $distance + $this->computeDistance($chromosome[$i], $chromosome[$i+1]);
						}
						if($i == (sizeof($training)-1)){
							$training[$key]['score'] = $distance;
						}
					}
				}
				// we need to sort the chromosomes according to the route's distance
				$tmp = array();
				foreach($training as $k => $p){
					$tmp[$k] = $p['score'];
				}
				array_multisort($tmp, SORT_ASC, $training);
				// let's compute fitness degree for each chromosome
				foreach($training as $j => $chromosome){
					$fitness = $j / sizeof($training);
					$training[$j]['fitness'] = $fitness;
				}

				// re-populate training set with new chromosomes based on fitness degree
				$best = $training[0];
				unset($best['score']);
				unset($best['fitness']);
				if($generationStep == $numberOfGenerations - 1){
					$bestRoute = $training[0];
				} else {
					foreach($training as $k => $chromosome){
						if($generationStep < $numberOfGenerations){
							unset($chromosome['score']);
							unset($chromosome['fitness']);
						}
						if($k != 0){
							$start = $best[0];
							//$first = $best[1];
							$rest = array_slice($best, 1);
							shuffle($rest);
							$newSet = array_merge(array(0 => $start), $rest);
							$training[$k] = $newSet;
						} else {
							$training[$k] = $chromosome;
						}
					}
				}
				
			}

			$distance = $bestRoute['score'];
			unset($bestRoute['score']);
			unset($bestRoute['fitness']);
			$bestRouteSorted = $bestRoute;
			sort($bestRouteSorted);

			if(!empty($existingBestRoute)){
				$bestRoute = explode(',', $existingBestRoute['WarehouseBestRoute']['warehouse_storage_point_ids']);
			} else {
				$this->WarehouseBestRoute->save(array(
					'WarehouseBestRoute' => array(
						'warehouse_id' => $warehouseId,
						'warehouse_storage_point_ids' => implode(',',$bestRoute),
						'warehouse_storage_point_ids_sorted' => implode(',',$bestRouteSorted),
						'distance' => $distance
					)
				));
			}
		}
		unset($bestRoute[0]);
		return $bestRoute;
	}

	public function computeDistance($storagePointId1, $storagePointId2){

		$sp1 = $this->WarehouseStoragePoint->find('first', array(
			'conditions' => array(
				'WarehouseStoragePoint.id' => $storagePointId1
			),
			'cache' => $storagePointId1,
			'cacheConfig' => 'short'
		));

		$sp2 = $this->WarehouseStoragePoint->find('first', array(
			'conditions' => array(
				'WarehouseStoragePoint.id' => $storagePointId2
			),
			'cache' => $storagePointId2,
			'cacheConfig' => 'short'
		));

		if( 
			$sp1['WarehouseStoragePoint']['adjacency_passage_id'] == $sp2['WarehouseStoragePoint']['adjacency_passage_id']
		){
			$shortestDistance = max( abs($sp1['WarehouseStoragePoint']['x'] - $sp2['WarehouseStoragePoint']['x']) , abs($sp1['WarehouseStoragePoint']['y'] - $sp2['WarehouseStoragePoint']['y']));
		} else {
			$d11 = $sp1['WarehouseStoragePoint']['distance_node1'];
			$d12 = $sp1['WarehouseStoragePoint']['distance_node2'];
			$d21 = $sp2['WarehouseStoragePoint']['distance_node1'];
			$d22 = $sp2['WarehouseStoragePoint']['distance_node2'];

			$node11 = $this->WarehouseNode->find('first', array(
				'conditions' => array(
					'WarehouseNode.id' => $sp1['WarehouseStoragePoint']['adjacency_node1_id']
				),
				'fields' => array('WarehouseNode.id'),
				'cache' => $sp1['WarehouseStoragePoint']['adjacency_node1_id'],
				'cacheConfig' => 'short'
			));
			$node12 = $this->WarehouseNode->find('first', array(
				'conditions' => array(
					'WarehouseNode.id' => $sp1['WarehouseStoragePoint']['adjacency_node2_id']
				),
				'fields' => array('WarehouseNode.id'),
				'cache' => $sp1['WarehouseStoragePoint']['adjacency_node2_id'],
				'cacheConfig' => 'short'
			));
			$node21 = $this->WarehouseNode->find('first', array(
				'conditions' => array(
					'WarehouseNode.id' => $sp2['WarehouseStoragePoint']['adjacency_node1_id']
				),
				'fields' => array('WarehouseNode.id'),
				'cache' => $sp2['WarehouseStoragePoint']['adjacency_node1_id'],
				'cacheConfig' => 'short'
			));
			$node22 = $this->WarehouseNode->find('first', array(
				'conditions' => array(
					'WarehouseNode.id' => $sp2['WarehouseStoragePoint']['adjacency_node2_id']
				),
				'fields' => array('WarehouseNode.id'),
				'cache' => $sp2['WarehouseStoragePoint']['adjacency_node2_id'],
				'cacheConfig' => 'short'
			));

			$d1 = $this->getDistanceBetweenNodes($node11['WarehouseNode']['id'], $node21['WarehouseNode']['id']);
			$d2 = $this->getDistanceBetweenNodes($node11['WarehouseNode']['id'], $node22['WarehouseNode']['id']);
			$d3 = $this->getDistanceBetweenNodes($node12['WarehouseNode']['id'], $node21['WarehouseNode']['id']);
			$d4 = $this->getDistanceBetweenNodes($node21['WarehouseNode']['id'], $node12['WarehouseNode']['id']);
			$shortestDistance = min( ($d11 + $d1 + $d21), ($d11 + $d2 + $d22), ($d12 + $d3 + $d21), ($d12 + $d4 + $d22) );
		}
		return $shortestDistance;
	}

	public function getDistanceBetweenNodes($nodeId1, $nodeId2){
		$distance = $this->WarehouseNodeDistance->find('first', array(
			'conditions' => array(
				'OR' => array(
					array(
						'WarehouseNodeDistance.warehouse_node1_id' => $nodeId1,
						'WarehouseNodeDistance.warehouse_node2_id' => $nodeId2
					),
					array(
						'WarehouseNodeDistance.warehouse_node1_id' => $nodeId2,
						'WarehouseNodeDistance.warehouse_node2_id' => $nodeId1
					)
				)
			),
			'fields' => array('distance'),
			'cache' => $nodeId1.$nodeId2,
			'cacheConfig' => 'short'
		));
		return !empty($distance['WarehouseNodeDistance']) ? $distance['WarehouseNodeDistance']['distance'] : 0;
	}

}