<?php

class VehicleCompany extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array('Vehicle', 'VehicleCompanyModel');

}