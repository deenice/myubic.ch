<?php

class VehicleReservation extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Vehicle',
		'VehicleCompanyModel' => array(
			'foreignKey' => 'vehicle_model_id'
		),
		'Driver' => array(
			'className' => 'User',
			'foreignKey' => 'driver_id'
		),
		'BackDriver' => array(
			'className' => 'User',
			'foreignKey' => 'back_driver_id'
		),
	);

	public $hasAndBelongsToMany = array('VehicleTour', 'Event');

	public function searchForConflicts(){
		$reservation = $this->findById($this->id);
		$data = array();
		if(!empty($reservation['VehicleReservation']['vehicle_id'])){
			$data = $this->find('all', array(
				'conditions' => array(
					'start <' => $reservation['VehicleReservation']['end'],
					'end >' => $reservation['VehicleReservation']['start'],
					'id <>' => $this->id,
					'vehicle_id' => $reservation['VehicleReservation']['vehicle_id'],
					'cancelled' => 0,
					'archived' => 0
				)
			));
		}
		return $data;
	}

}
