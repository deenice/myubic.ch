<?php

class Beacon extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('StockZone');
}