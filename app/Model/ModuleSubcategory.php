<?php

class ModuleSubcategory extends AppModel {

	public $actsAs = array('Containable');
	public $useTable = 'modules_subcategories';
	public $belongsTo = array('ModuleCategory');
	public $hasMany = array('Module');

}