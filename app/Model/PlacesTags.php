<?php

class PlacesTags extends AppModel {

	public $actsAs = array('Containable');


	public $belongsTo = array(
		'Tag',
		'Place'
	);

}