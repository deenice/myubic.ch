<?php

class Order extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'OrderGroup' => array(
			'order' => 'weight',
      'dependent' => true
		),
		'OrderAssociation'
	);

	public $belongsTo = array('Company', 'Event', 'FBCategory');

  public $hasAndBelongsToMany = array(
    'Activity' => array(
      'joinTable' => 'order_associations',
      'foreignKey' => 'order_id',
      'associationForeignKey' => 'model_id',
      'unique' => true,
      'conditions' => array(
        'model' => 'activity'
      ),
      'order' => 'name'
    ),
    'FBModule' => array(
      'joinTable' => 'order_associations',
      'foreignKey' => 'order_id',
      'associationForeignKey' => 'model_id',
      'unique' => true,
      'conditions' => array(
        'model' => 'fb_module'
      ),
      'order' => 'name'
    )
  );

	public function assign( $options = array() ){
    // $options = array(
    //   'order_model' => null,
    //   'order_model_id' => null,
    //   'item_model' => null,
    //   'item_model_id' => null
    // );
    $this->contain(array(
      'OrderGroup' => array(
        'OrderItem'
      )
    ));
    $default = $this->findById($this->id);
    $default['Order']['id'] = null;
    $default['Order']['name'] = empty($options['order_model_id']) ? $default['Order']['name'] . ' - copy' : $default['Order']['name'];
    $default['Order']['model'] = $options['order_model'];
    $default['Order']['model_id'] = $options['order_model_id'];
    $default['Order']['parent'] = $default['Order']['id']; // we need to keep the original id to retrieve associations
    $this->create();
    if(!$this->save($default)){
      return false;
    }
    foreach($default['OrderGroup'] as $k => $group){
      $default['OrderGroup'][$k]['id'] = null;
      $default['OrderGroup'][$k]['order_id'] = $this->id;
      $this->OrderGroup->create();
      if(!$this->OrderGroup->save($default['OrderGroup'][$k])){
        return false;
      }
      foreach($group['OrderItem'] as $kk => $item){
        $default['OrderGroup'][$k]['OrderItem'][$kk]['id'] = null;
        $default['OrderGroup'][$k]['OrderItem'][$kk]['order_group_id'] = $this->OrderGroup->id;
        $default['OrderGroup'][$k]['OrderItem'][$kk]['model_id'] = !empty($options['item_model_id']) ? $options['item_model_id'] : null;
        $default['OrderGroup'][$k]['OrderItem'][$kk]['model'] = !empty($options['item_model']) ? $options['item_model'] : null;
        $default['OrderGroup'][$k]['OrderItem'][$kk]['default_quantity'] = $item['default_quantity'];
        $default['OrderGroup'][$k]['OrderItem'][$kk]['quantity'] = $item['default_quantity'];
        $default['OrderGroup'][$k]['OrderItem'][$kk]['editable'] = 0;
        $this->OrderGroup->OrderItem->create();
        if(!$this->OrderGroup->OrderItem->save($default['OrderGroup'][$k]['OrderItem'][$kk])){
          return false;
        }
      }
    }
    return true;
  }

  public function mergeDuplicates( $orders ){
    $items = array();
    foreach($orders as $j => $order){
      foreach($order['OrderGroup'] as $k => $group){
        foreach($group['OrderItem'] as $kk => $item){
          if(empty($item['stock_item_id'])){ continue; }
          $items[$item['stock_item_id']][] = array(
            'quantity' => $item['quantity'],
            'number_of_containers' => $item['number_of_containers'],
            'quantity_option' => $item['quantity_option'],
            'j' => $j,
            'k' => $k,
            'kk' => $kk
          );
        }
      }
    }
    foreach($items as $k => $item){
      if(sizeof($item) == 1){
        unset($items[$k]);
        continue;
      }
    }
    // if quantity options are different and one of them is "quantity per person", we don't want them to be merged!
    if(!empty($items)){
      foreach($items as $id => $products){
        foreach($products as $kk => $product){
          if(isset($products[$kk+1])){
            if($product['quantity_option'] != $products[$kk+1]['quantity_option']){
              if($product['quantity_option'] == 'quantity_per_person' || $products[$kk+1]['quantity_option'] == 'quantity_per_person'){
                unset($items[$id]);
                continue;
              }
            }
          }
        }
      }
    }
    if(!empty($items)){
      foreach($items as $stockItemId => $data){
        foreach($data as $i => $item){
          if($i>0){
            $orders[$data[0]['j']]['OrderGroup'][$data[0]['k']]['OrderItem'][$data[0]['kk']]['quantity'] += $item['quantity'];
            $orders[$data[0]['j']]['OrderGroup'][$data[0]['k']]['OrderItem'][$data[0]['kk']]['number_of_containers'] += $item['number_of_containers'];
            unset($orders[$item['j']]['OrderGroup'][$item['k']]['OrderItem'][$item['kk']]);
          }
        }
      }
    }
    return $orders;
  }

  public function deleteFromEvent($id = '', $model = '', $modelId = ''){
    $orders = $this->find('all', array(
      'joins' => array(
        array(
          'table' => 'order_groups',
          'alias' => 'og',
          'conditions' => array('og.order_id = Order.id')
        ),
        array(
          'table' => 'order_items',
          'alias' => 'oi',
          'conditions' => array(
            'oi.order_group_id = og.id',
            'oi.model' => $model,
            'oi.model_id' => $modelId
          )
        )
      ),
      'conditions' => array(
        'Order.model_id' => $id,
        'Order.model' => 'event'
      ),
      'group' => 'Order.id'
    ));
    if(!empty($orders)){
      foreach($orders as $order){
        $this->delete($order['Order']['id']);
      }
    }
  }

}
