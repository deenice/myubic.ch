<?php

class RoundStep extends AppModel {

  public $actsAs = array('Containable');
  public $belongsTo = array('Round');
  public $hasMany = array('RoundAnswer');

  public function send(){
    $step = $this->findById($this->id);
    $round = $this->Round->findById($step['RoundStep']['round_id']);
    if($round['Round']['status'] == 'closed'){
      return false;
    }

    $answers = $this->RoundAnswer->find('all', array(
      'conditions' => array(
        'RoundAnswer.round_step_id' => $step['RoundStep']['id'],
        'RoundAnswer.answer' => 'waiting'
      )
    ));
    foreach($answers as $answer){
      $this->RoundAnswer->id = $answer['RoundAnswer']['id'];
      $this->RoundAnswer->notify();
    }
    if( $this->saveField('sent', 1) && $this->saveField('sent_date', date('Y-m-d H:i:s'))){
      return true;
    } else {
      return false;
    }
  }

}
