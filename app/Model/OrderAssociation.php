<?php

class OrderAssociation extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'Order',
		'Activity' => array(
			'foreignKey' => 'model_id'
		),
		'FBModule' => array(
			'foreignKey' => 'model_id'
		)
	);


}
