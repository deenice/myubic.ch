<?php

class OrderGroup extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array(
		'OrderItem' => array(
			'order' => 'weight',
			'dependent' => true
		),
		'EditableOrderItem' => array(
			'order' => 'weight',
			'className' => 'OrderItem',
			'conditions' => array(
				'EditableOrderItem.editable' => 1
			)
		),
		'UnEditableOrderItem' => array(
			'order' => 'weight',
			'className' => 'OrderItem',
			'conditions' => array(
				'UnEditableOrderItem.editable' => 0
			)
		)
	);

	public $belongsTo = array('Order');


}
