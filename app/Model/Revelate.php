<?php

App::uses('HttpSocket', 'Network/Http');

class Revelate extends AppModel {

  public $server, $auth, $http;

  public function __construct($id = false, $table = null, $ds = null) {

    parent::__construct($id, $table, $ds);

    $this->server = (Configure::read('debug')) ? 'https://test.wealthings.net/services/organizations/' : 'https://www.wealthings.net/services/organizations/';
    // $this->server = 'https://www.wealthings.net/services/organizations/';

    $this->auth['user'] = 'webservices@une-bonne-idee-ch';
    $this->auth['pass'] = '5sBNGTwD';

    $this->http = new HttpSocket(array('ssl_verify_peer' => false));
    $this->http->configAuth('Basic', $this->auth['user'], $this->auth['pass']);
  }

  public function get( $organization = null,  $service = null, $id = null ){
    $url = $this->server . $organization . '/' . $service . '/' . $id;
    $result = $this->http->get( $url );
    return $result;
  }

  public function post( $organization = null,  $service = null, $xml = null ){
    $url = $this->server . $organization . '/' . $service;
    $result = $this->http->post( $url, $xml );
    return $result;
  }

  public function put( $organization = null,  $service = null, $xml = null, $id = null ){
    $url = $this->server . $organization . '/' . $service . '/' . $id;
    $result = $this->http->put( $url, $xml );
    return $result;
  }

  // public function put( $organization = null,  $service = null, $xml = null ){
  //   $url = $this->server . $organization . '/' . $service;
  //   $result = $this->http->put( $url, $xml );
  //   return $result;
  // }

  public function xml( $model = '', $id = '', $params = array() ){
    $common = 'http://www.revelate.com/Services/Common.xsd';
    switch($model){
      case 'client':
        App::import('Model', 'Client');
  			$Client = new Client();
        $client = $Client->findById($id);
        if($client['Client']['country'] == 'Suisse'){
          $country = 'CH';
        } elseif($client['Client']['country'] == 'France'){
          $country = 'F';
        } else {
          $country = $client['Client']['country'];
        }

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Client></Client>');
        $xml->addAttribute('version', '1.0');
        $xml->addAttribute('xmlns', 'http://www.revelate.com/Services/Client.xsd');
        $xml->addAttribute('xmlns:xmlns:rvcm', $common);
        $contact = $xml->addChild('Contact');
        $entity = $contact->addChild('Entity', '', $common);
        $organization = $entity->addChild('Organization', '', $common);
        $name = $organization->addChild('Name', $client['Client']['name'], $common);
        $addresses = $contact->addChild('Addresses', '', $common);
        $general = $addresses->addChild('General', '', $common);
        $line1 = $general->addChild('Line1', $client['Client']['address'], $common);
        $zip = $general->addChild('PostalCode', $client['Client']['zip'], $common);
        $city = $general->addChild('City', $client['Client']['city'], $common);
        $country = $general->addChild('CountryCode', $country, $common);
        $string = $xml->asXML();
        file_put_contents('files/documents/revelate/clients/' . $client['Client']['id'] . '__' . date('Y-m-d') . '.xml' , $string);
        return $string;
      break;

      case 'stockorder':
        $types =  Configure::read('StockOrders.types');
        App::import('Model', 'StockOrder');
  			$StockOrder = new StockOrder();
        $StockOrder->contain(
          array(
            'Client',
            'ContactPeopleClient'
          )
        );
        $order = $StockOrder->findById($id);
        $StockOrder->Client->id = $order['Client']['id'];
        $clientUUID = $StockOrder->Client->getUUID();

        if(!empty($order['StockOrder']['invoice_client'])){
          $client = $order['StockOrder']['invoice_client'];
          $cp = $order['StockOrder']['invoice_contact_person'];
        } else {
          if($order['ContactPeopleClient']['full_name'] != $order['Client']['name']) {
            $client = $order['Client']['name'];
            $cp = $order['ContactPeopleClient']['full_name'];
          } else {
            $client = $order['Client']['name'];
            $cp = null;
          }
        }

        if(!empty($order['StockOrder']['invoiced_total'])){
          $total = $order['StockOrder']['invoiced_total'];
        } else {
          $total = $order['StockOrder']['net_total'];
        }
        $tva = $order['StockOrder']['tva'];
        if(!empty($params['deposit'])){
          $total = $order['StockOrder']['deposit'];
          $tva = round($total - ($total / 1.08), 2);
        }
        $totalExclTax = $total / 1.08;


        if(empty($order['StockOrder']['invoice_date'])){
          $date = new DateTime($order['StockOrder']['return_date']);
        } else {
          $date = new DateTime($order['StockOrder']['invoice_date']);
        }
        if(!empty($params['deposit'])){
          $date = new DateTime(date('Y-m-d', time()));
        }
        $date = $date->format('Y-m-d\T00:00:00');

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><SalesInvoice></SalesInvoice>');
        $xml->addAttribute('version', '1.0');
        $xml->addAttribute('xmlns', 'http://www.revelate.com/Services/SalesInvoice.xsd');
        $xml->addAttribute('xmlns:xmlns:rvcm', $common);
        // if($params['deposit'] && !empty($order['StockOrder']['deposit_uuid'])){
        //   $identifier = $xml->addChild('Identifier');
        //   $identifier->addChild('Id', $order['StockOrder']['deposit_uuid'], $common);
        // }
        // if(empty($params) && !empty($order['StockOrder']['uuid'])){
        //   $identifier = $xml->addChild('Identifier');
        //   $identifier->addChild('Id', $order['StockOrder']['uuid'], $common);
        // }
        $date = $xml->addChild('Date', $date);
        $number = $xml->addChild('Number', $order['StockOrder']['id']);
        $clientId = $xml->addChild('ClientId');
        $Id = $clientId->addChild('Id', $clientUUID, $common);
        $recipient = $xml->addChild('Recipient');
        $fullname = $recipient->addChild('FullName', $client);
        $address = $recipient->addChild('Address');
        $line1 = $address->addChild('Line1', $order['StockOrder']['invoice_address'], $common);
        $zip = $address->addChild('PostalCode', $order['StockOrder']['invoice_zip'], $common);
        $city = $address->addChild('City', $order['StockOrder']['invoice_city'], $common);
        $country = $address->addChild('CountryCode', 'CH', $common);
        $attention = $address->addChild('AttentionOf', $cp, $common);
        if(!empty($params['deposit'])){
          $title = $xml->addChild('Title', sprintf('ACOMPTE : %s - %s', date('d.m.Y', strtotime($order['StockOrder']['service_date_begin'])), date('d.m.Y', strtotime($order['StockOrder']['service_date_end']))));
        } else {
          $title = $xml->addChild('Title', sprintf('%s - %s', date('d.m.Y', strtotime($order['StockOrder']['service_date_begin'])), date('d.m.Y', strtotime($order['StockOrder']['service_date_end']))));
        }
        $clientsRef = $xml->addChild('ClientsReference', $order['StockOrder']['name']);
        $sales = $xml->addChild('SalesRepresentativeId');
        $paymentTerms = $xml->addChild('PaymentTerms', 0);
        $currency = $xml->addChild('CurrencyCode', 'CHF');
        $lines = $xml->addChild('InvoiceLines');

        $line = $lines->addChild('InvoiceLine');
        $quantity = $line->addChild('Quantity', 1);
        $description = $line->addChild('Description');
        if(!empty($params['deposit'])){
          $short = $description->addChild('Short', sprintf('ACOMPTE : %s %s', $types[$order['StockOrder']['type']], __('material')));
        } else {
          $short = $description->addChild('Short', sprintf('%s %s', $types[$order['StockOrder']['type']], __('material')));
        }
        $long = $description->addChild('Long');
        $unitPrice = $line->addChild('UnitPriceExclTax', $totalExclTax);
        $taxRate = $line->addChild('TaxRate', '.08');
        $unitTaxAmount = $line->addChild('UnitTaxAmount', $tva);
        $unitPriceInclTax = $line->addChild('UnitPriceInclTax', $total);
        $totalPriceExclTax = $line->addChild('TotalPriceExclTax', $totalExclTax);
        $TotalTaxAmount1 = $line->addChild('TotalTaxAmount', $tva);
        $totalPriceInclTax = $line->addChild('TotalPriceInclTax', $total);
        if(!empty($params['deposit'])){
          $accountingCode = $line->addChild('AccountingCode', 2030);
        } else {
          $accountingCode = $line->addChild('AccountingCode', 3000);
        }
        $product = $line->addChild('ProductId');
        $BusinessUnitId = $line->addChild('BusinessUnitId');
        $BusinessUnitIdId = $BusinessUnitId->addChild('Id', '00000000-0000-0000-0000-000000000000', $common);
        $productId = $product->addChild('Id', '00000000-0000-0000-0000-000000000000', $common);
        $productExternalId = $product->addChild('ExternalId', $order['StockOrder']['id'], $common);

        $TotalAmountExclTax = $xml->addChild('TotalAmountExclTax', $totalExclTax);
        $TaxCalculationType = $xml->addChild('TaxCalculationType', 0);
        //$TaxRegime = $xml->addChild('TaxRegime', 0);
        $TaxRate = $xml->addChild('TaxRate', '.08');
        $TotalTaxAmount = $xml->addChild('TotalTaxAmount', $tva);
        $TotalAmountInclTax = $xml->addChild('TotalAmountInclTax', $total);
        $ExternalId = $xml->addChild('ExternalId', $order['StockOrder']['id']);

        $string = $xml->asXML();
        file_put_contents('files/documents/revelate/orders/' . $order['StockOrder']['id'] . '__' . date('Y-m-d') . '.xml' , $string);
        return $string;
      break;
      default:
      break;
    }
  }


}
