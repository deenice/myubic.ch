<?php

class ModuleUser extends AppModel{

	public $useTable = 'modules_users';
	public $actsAs = array('Containable');
	public $belongsTo = array('Module', 'User');

}