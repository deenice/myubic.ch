<?php

class Manager extends AppModel {

  public $useTable = 'users';

	public $actsAs = array(
		'Containable'
	);

	//public $virtualFields = array('full_name' => sprintf('CONCAT(%s.first_name, " ", %s.last_name)'));
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->virtualFields['full_name'] = sprintf('CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias);
		$this->virtualFields['zip_city'] = sprintf('CONCAT(%s.zip, " ", %s.city)', $this->alias, $this->alias);
	}

	public $hasMany = array(
    'PlanningResource' => array(
      'foreignKey' => 'resource_id',
      'conditions' => array(
        'PlanningResource.resource_model' => 'manager'
      )
    )
	);

}
