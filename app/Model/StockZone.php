<?php

class StockZone extends AppModel {

	public $actsAs = array('Containable');

	public $hasMany = array('StockItem', 'Beacon');

}