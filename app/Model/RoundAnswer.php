<?php

App::uses('CakeEmail', 'Network/Email');

class RoundAnswer extends AppModel {

  public $actsAs = array('Containable');
  public $belongsTo = array('Round', 'User', 'RoundStep');

  public function afterSave($created, $options = array()){
    if($created){
      return false;
    } else {
      $this->contain(array(
        'RoundStep' => array('Round' => array('Job' => array('Event')))
      ));
      $answer = $this->findById($this->id);

      $unavailability = array(
        'model_id' => $answer['RoundStep']['Round']['Job']['id'],
        'model' => 'job',
        'status' => $answer['RoundAnswer']['answer'],
        'start_hour' => '',
        'end_hour' => '',
        'date' => $answer['RoundStep']['Round']['Job']['Event']['confirmed_date']
      );
      $this->User->id = $answer['RoundAnswer']['user_id'];
      if(!empty($answer['RoundAnswer']['answer'])){
        $this->User->addUnavailability($unavailability);
      }

      if(in_array($answer['RoundAnswer']['answer'], array('not_available', 'not_interested'))){
        // we need to disable next mails for this user
        $answers = $this->find('all', array(
          'joins' => array(
            array(
              'table' => 'round_steps',
              'alias' => 'rs',
              'conditions' => array('rs.id = RoundAnswer.round_step_id')
            )
          ),
          'conditions' => array(
            'user_id' => $answer['RoundAnswer']['user_id'],
            'rs.round_id' => $answer['RoundStep']['round_id'],
            'RoundAnswer.id <>' => $answer['RoundAnswer']['id']
          )
        ));
        if(!empty($answers)){
          foreach($answers as $a){
            $a['RoundAnswer']['answer'] = $answer['RoundAnswer']['answer'];
            $this->save($a, array('callbacks' => false));
          }
        }
      }
    }
  }

  public function notify(){

    App::import('Model', 'Message');
    $Message = new Message();
    $View = new View();
    $Time = $View->loadHelper('Time');

    $answer = $this->read(null, $this->id);
    $step = $this->RoundStep->findById($answer['RoundAnswer']['round_step_id']);
    $round = $this->RoundStep->Round->findById($step['RoundStep']['round_id']);
    $this->RoundStep->Round->Job->contain(array('Activity'));
    $job = $this->RoundStep->Round->Job->findById($round['Round']['job_id']);
    $this->RoundStep->Round->Job->Event->contain(array('Client', 'ConfirmedEventPlace' => array('Place')));
    $event = $this->RoundStep->Round->Job->Event->findById($job['Job']['event_id']);
    if($event['Event']['confirmed_date'] < date('Y-m-d')){
      return false;
    }
    $user = $this->User->findById($answer['RoundAnswer']['user_id']);
    $sender = $this->User->findById($event['Event']['resp_id']);
    if(!empty($event['ConfirmedEventPlace'])){
      $place = $event['ConfirmedEventPlace'][0]['Place'];
      $placeName = $place['name'];
      if(!empty($place['address'])){
        $placeName .= ', ' . $place['address'];
      }
      if(!empty($place['zip_city'])){
        $placeName .= ', ' . $place['zip_city'];
      }
    } else {
      $placeName = '';
    }

    $competencesJobs = Configure::read('Competences.jobs');

    $this->RoundStep->Round->Job->id = $job['Job']['id'];
    $salary = $this->RoundStep->Round->Job->getSalary($user);

    $confirmLink = $this->url($user, 'interested');
    $notInterestedLink = $this->url($user, 'not_interested');
    $notAvailableLink = $this->url($user, 'not_available');

    $pax = 0;
    if(!empty($event['Event']['confirmed_number_of_persons'])){
      $pax = $event['Event']['confirmed_number_of_persons'];
    } elseif(!empty($event['Event']['min_number_of_persons']) && empty($event['Event']['max_number_of_persons'])){
      $pax = $event['Event']['min_number_of_persons'];
    } elseif(empty($event['Event']['min_number_of_persons']) && !empty($event['Event']['max_number_of_persons'])){
      $pax = $event['Event']['max_number_of_persons'];
    } elseif(!empty($event['Event']['min_number_of_persons']) && !empty($event['Event']['max_number_of_persons'])){
      $pax = $event['Event']['min_number_of_persons'] . ' - ' . $event['Event']['max_number_of_persons'];
    } elseif(!empty($event['Event']['offer_number_of_persons'])){
      $pax = $event['Event']['offer_number_of_persons'];
    }

    $languages = array();
    $jobsLanguages = Configure::read('Jobs.languages');
    if(!empty($event['Event']['languages'])){
      $langs = explode(',',$event['Event']['languages']);
      foreach($langs as $lang){
        $languages[] = $jobsLanguages[$lang];
      }
    }
    // set variables for email
    $subject = sprintf("Proposition d'extra - %s", date('d.m.Y', strtotime($event['Event']['confirmed_date'])));
    $body = $View->element('Messages/round_mail', array(
      'senderFirstName' => $sender['User']['first_name'],
      'userFirstName' => $user['User']['first_name'],
      'eventDate' => $Time->format($event['Event']['confirmed_date'], '%A %d.%m.%Y'),
      'eventName' => $event['Event']['name'],
      'clientName' => $event['Client']['name'],
      'eventPax' => $pax,
      'placeName' => $placeName,
      'jobStartTime' => date('H:i', strtotime($job['Job']['start_time'])),
      'jobEndTime' => date('H:i', strtotime($job['Job']['end_time'])),
      'jobJob' => $competencesJobs[$job['Job']['job']],
      'job' => $job['Job']['job'],
      'jobActivityName' => (!empty($job['Activity']['name'])) ? $job['Activity']['name'] : '',
      'jobSalary' => $salary,
      'message' => '',
      'remarks' => empty($job['Job']['remarks']) ? '' : $job['Job']['remarks'],
      'languages' => $languages,
      'confirmLink' => $confirmLink,
      'notInterestedLink' => $notInterestedLink,
      'notAvailableLink' => $notAvailableLink
    ));

    $message = array(
      'Message' => array(
        'subject' => $subject,
        'body' => $body,
        'recipients' => $user['User']['id'],
        'type' => 'mail',
        'user_id' => $event['Event']['resp_id'],
        'group' => $step['RoundStep']['id']
      )
    );
    $Message->create();
    $Message->save($message);
    if($Message->send()){
      //$this->saveField('answer', 'waiting');
      CakeLog::write('myubic_round_answers', json_encode($message));
      return true;
    } else {
      return false;
    }
  }

  public function url( $user, $status = '' ){
    $url = '';
    $token = md5($user['User']['email'] . time());
    $this->saveField('token', $token, array('callbacks' => false));
    switch($status){
      case 'interested':
        $url = Router::url(array('controller' => 'round_answers', 'action' => 'process', $token, 'interested', 'full_base' => true));
      break;
      case 'not_interested':
        $url = Router::url(array('controller' => 'round_answers', 'action' => 'process', $token, 'not_interested', 'full_base' => true));
      break;
      case 'not_available':
        $url = Router::url(array('controller' => 'round_answers', 'action' => 'process', $token, 'not_available', 'full_base' => true));
      break;
      default:
      break;
    }
    return $url;
  }

}
