<?php

class Transporter extends AppModel {

	public $actsAs = array('Containable');
	public $hasMany = array(
    'ContactPeople',
    'StockOrder'
  );

}
