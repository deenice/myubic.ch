<?php

class EventPlace extends AppModel {

	public $useTable = 'events_places';
	public $actsAs = array('Containable');
	public $belongsTo = array('Place', 'Event');

}
