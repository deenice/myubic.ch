<?php

class Tag extends AppModel {

    public $actsAs = array('Containable');

	public $hasAndBelongsToMany = array('User', 'Place', 'Configuration', 'Artist');
    public $hasMany = array('TagUser');

	public function getLanguages($option) {
		return $this->find($option, array('conditions' => array('category' => 'language')));
	}

	public function getTalents($option) {
		return $this->find($option, array('conditions' => array('category' => 'talent')));
	}
    
    public function prepareData($data, $model, $field, $category, $saveNewIds = true){
        if(!empty($data[$model][$field])){
            if(empty($data['Tag'])) $data['Tag'] = array();
            if(is_array($data[$model][$field])){
                $data[$model][$field] = implode(',', $data[$model][$field]);
            }
            $terms = explode(',', $data[$model][$field]);
            foreach ($terms as $key => $term) {
                if(is_numeric($term)){
                    array_push($data['Tag'], array(
                        'category' => $category,
                        'id' => $term
                    ));
                } elseif($saveNewIds) {
                    array_push($data['Tag'], array(
                        'category' => $category,
                        'value' => $term
                    ));
                }                    
            }
            return $data;
        } else {
            return $data;
        }
    }
}