<?php

class ChecklistTask extends AppModel{

	public $actsAs = array('Containable');

  public $belongsTo = array(
    'ChecklistBatch' => array(
      'counterCache' => array(
        'total_tasks' => array('ChecklistTask.done' => array(0,1)),
        'closed_tasks' => array('ChecklistTask.done' => 1)
      )
    )
  );

  public function afterSave($created, $options = array()){

    if($created){
      $numberOfTasks = $this->find('count', array(
        'conditions' => array(
          'checklist_batch_id' => $this->field('checklist_batch_id')
        )
      ));
      $this->saveField('weight', $numberOfTasks);
    }

    $task = $this->read(null, $this->id);
    $batch = $this->ChecklistBatch->findById($task['ChecklistTask']['checklist_batch_id']);
    $progress = round($batch['ChecklistBatch']['closed_tasks'] / $batch['ChecklistBatch']['total_tasks'] * 100, 0);
    $batch['ChecklistBatch']['progress'] = $progress;
    $this->ChecklistBatch->create();

    if($this->ChecklistBatch->save($batch)){
      return true;
    } else {
      return false;
    }

  }

  public function beforeDelete( $cascade = true ){

    $task = $this->read(null, $this->id);
    $batch = $this->ChecklistBatch->findById($task['ChecklistTask']['checklist_batch_id']);
    $progress = round($batch['ChecklistBatch']['closed_tasks'] / ($batch['ChecklistBatch']['total_tasks'] - 1) * 100, 0);
    $batch['ChecklistBatch']['progress'] = $progress;
    $this->ChecklistBatch->create();

    if($this->ChecklistBatch->save($batch)){
      return true;
    } else {
      return false;
    }

  }

}
