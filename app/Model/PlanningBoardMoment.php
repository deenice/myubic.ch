<?php

class PlanningBoardMoment extends AppModel {

  public $actsAs = array(
    'Containable'
  );

  public $belongsTo = array('PlanningBoard', 'Activity', 'FBModule');

  public $hasMany = array(
    'PlanningResource' => array(
      'dependent' => true
    )
  );

  public $hasAndBelongsToMany = array(
    'Activity' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'activity'
      ),
      'order' => 'name'
    ),
    'FBModule' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'fb_module'
      ),
      'order' => 'name'
    ),
    'Extra' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'extra'
      ),
      'order' => 'first_name'
    ),
    'Manager' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'manager'
      ),
      'order' => 'first_name'
    ),
    'Client' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'client'
      )
    ),
    'Place' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'place'
      ),
      'order' => 'name'
    ),
    'Vehicle' => array(
      'joinTable' => 'planning_resources',
      'foreignKey' => 'planning_board_moment_id',
      'associationForeignKey' => 'resource_id',
      'unique' => true,
      'conditions' => array(
        'resource_model' => 'vehicle'
      ),
      'order' => 'name'
    )
  );

  public function afterSave($created, $options = array()){

    if($created && !empty($this->data['PlanningBoardMoment']['add_in_general'])){
      $boardId = $this->field('planning_board_id');
      $moment = $this->findById($this->id);
      $board = $this->PlanningBoard->findById($boardId);
      if($board['PlanningBoard']['type'] != 'general'){
        // we add this moment in general board
        $generalBoard = $this->PlanningBoard->find('first', array(
          'conditions' => array(
            'model' => $board['PlanningBoard']['model'],
            'model_id' => $board['PlanningBoard']['model_id'],
            'type' => 'general'
          )
        ));
        $moment['PlanningBoardMoment']['copied_from'] = $this->id;
        $moment['PlanningBoardMoment']['id'] = null;
        $moment['PlanningBoardMoment']['weight'] = 100;
        $moment['PlanningBoardMoment']['planning_board_id'] = $generalBoard['PlanningBoard']['id'];
        $this->create();
        $this->save($moment);
      }
    }

  }

  public function deleteFromEvent($id = '', $model = '', $modelId = ''){
    $moments = $this->find('all', array(
      'joins' => array(
        array(
          'table' => 'planning_resources',
          'alias' => 'pr',
          'conditions' => array(
            'pr.planning_board_moment_id = PlanningBoardMoment.id',
            'pr.resource_id' => $modelId,
            'pr.resource_model' => $model
          )
        ),
        array(
          'table' => 'planning_boards',
          'alias' => 'p',
          'conditions' => array(
            'p.id = PlanningBoardMoment.planning_board_id',
            'p.model' => 'event',
            'p.model_id' => $id
          )
        )
      ),
      'group' => 'PlanningBoardMoment.id'
    ));
    if(!empty($moments)){
      foreach($moments as $moment){
        $this->delete($moment['PlanningBoardMoment']['id']);
      }
    }
  }
}
