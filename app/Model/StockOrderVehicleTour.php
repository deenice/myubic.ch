<?php

class StockOrderVehicleTour extends AppModel {

	public $actsAs = array('Containable');

	public $useTable = 'stock_orders_vehicle_tours';

	public $belongsTo = array('StockOrder', 'VehicleTour');

}
