<?php

class StockSection extends AppModel {

	public $actsAs = array('Containable');

	public $belongsTo = array('StockCategory', 'Company');
	public $hasMany = array('StockFamily');

	public $virtualFields = array(
    	'code_name' => 'CONCAT(StockSection.code, " - ", StockSection.name)'
    );

}
