<?php 

class WarehouseNode extends AppModel {

	public $hasTable = array('warehouse_nodes');

	public $actsAs = array('Containable');

	public $belongsTo = array(
		'WarehouseCell',
		'HorizontalPassage' => array(
			'className' => 'WarehousePassage',
			'limit' => 1,
			'foreignKey' => 'horizontal_passage_id',
			'conditions' => array(
				'HorizontalPassage.type' => 'horizontal'
			)
		),
		'VerticalPassage' => array(
			'className' => 'WarehousePassage',
			'limit' => 1,
			'foreignKey' => 'vertical_passage_id',
			'conditions' => array(
				'VerticalPassage.type' => 'vertical'
			)
		)
	);

}