/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    var basePath = "";

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }

    var handleDatePickers = function () {

        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                autoclose: true,
                weekStart:1,
                language: 'fr-FR',
                format: 'dd-mm-yyyy'
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
        if(jQuery().timepicker){
            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });
        }

    }

    var handleBootstrapSelect = function() {
        $('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            size: 5
        });
    }

    var handleRemoveConfiguration = function() {
        $('body').on('click', 'a.remove-configuration', function(event){
            event.preventDefault();
            var id = $(this).data('id');
            var config = $(this).parents('.configuration-form');
            
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: basePath + '/configurations/delete/' + id,
                success: function(data){
                    toastr.success('Configuration has been removed.')
                    config.fadeOut();
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            })
        })
    }

    var handleRemoveActivity = function() {
        $('body').on('click', 'a.remove-activity', function(event){
            event.preventDefault();
            var id = $(this).data('id');
            var activity = $(this).parents('.activity-form');
            
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: basePath + '/activities/delete/' + id,
                success: function(data){
                    toastr.success('Activity has been removed.');
                    activity.fadeOut();
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            })
        })
    }

    var handleConfigurations = function() {
        $('body').on('click', 'a.add-configuration', function(event){
            event.preventDefault();

            var configDiv = $('.configuration-form:first');
            var clone = configDiv.clone();
            clone.find('.caption').text('New configuration');
            clone.hide();
            clone.find('.select2-container').remove();
            clone.find('input').each(function(i,e){
                $(e).val('');
                var index = $('.configuration-form').get().length;
                if($(e).attr('name') !== undefined) var name = $(e).attr('name').replace('[0]', '['+index+']');
                var id = $(e).attr('id').replace('0', index);
                $(e).attr('name', name);
                $(e).attr('id', id);
            })
            $('#configurations .form-actions').before(clone);
            clone.fadeIn();
            handleTagsSelection();
        });
    }

    var handlePlacesOptions = function(){

        $('body').on('click', '.save-option', function(event){
            event.preventDefault();
            var rel = $(this).attr('rel');
            var div = $('#' + rel);
            var activeInput = div.find('.options label.active input');
            var option = activeInput.data('value');
            var id = div.find('input.option_id').val();
            var date = activeInput.data('date');
            var place_id = activeInput.data('place-id');
            var moment_id = activeInput.data('moment-id');
            var event_id = activeInput.data('event-id');
            var user_id = activeInput.data('user-id');
            var remarks = div.find('input.remarks').val();
            var valid_until = div.find('input.date-picker').val();

            if(typeof(option) !== 'undefined'){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: basePath + '/options/save',
                    data: {
                        id: id,
                        option: option,
                        date: date,
                        place_id: place_id,
                        moment_id: moment_id,
                        event_id: event_id,
                        user_id: user_id,
                        remarks: remarks,
                        valid_until: valid_until
                    },
                    success: function(data){
                        if(data.success == 1){
                            $('#' + rel).find('input.option_id').val(data.id);
                            toastr.success('Option has been saved.');
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }                    
                })
            } else {
                bootbox.alert("No option has been selected! Please select one.");
            }
        })

        $('.btn-group.options').each(function(i,e){
            var div = $(e);
            var place_id = $(e).find('input:first').data('place-id');
            var moment_id = $(e).find('input:first').data('moment-id');
            var date = $(e).find('input:first').data('date');

            if(date && place_id){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: basePath + '/options/getInfos',
                    data: {
                        place_id: place_id,
                        date: date,
                        moment_id: moment_id
                    },
                    success: function(data){
                        if(data.overlap==1 && data.empty == 0){
                            div.find('label.'+data.Option.value).addClass('active');
                        }
                        if(data.empty == 0 && data.overlap == 0){
                            div.find('label.'+data.Option.value).addClass('active');
                            div.find('input.option_id').val(data.Option.id);
                            div.find('p.infos').show();
                            div.find('p.infos .user').text(data.User.name);
                            div.find('p.infos .event').text(data.Event.name);
                            div.find('p.infos .moment').text(data.Moment.name);
                            div.find('p.infos .start_hour').text(data.Moment.start_hour);
                            div.find('p.infos .end_hour').text(data.Moment.end_hour);
                        } else if(data.empty == 1){
                            div.find('label.free').addClass('active');
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                })
            }

        });

        return;
        $('.places-options').each(function(i,e){
            var div = $(e);
            var place_id = $(e).find('input:first').data('place-id');
            var date = $(e).find('input:first').data('date');
            var moment_id = $(e).find('input:first').data('moment-id');

            if(date && place){
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: basePath + '/places/getOption/' + place + '/' + date + '/' + morning + '/' + evening,
                    success: function(data){
                        div.find('label.'+data.option).addClass('active');
                        if( typeof(data.full_name) != 'undefined' && data.full_name != ''){
                            div.find('span.'+data.option).removeClass('hidden').html("<small><i class='fa fa-user'></i> " + data.full_name + "</small>");
                            if(data.valid_until.length > 0) div.find('input.valid_until').val(data.valid_until);
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                })
            }

        });

        $('body').on('click', '.places-options label', function(event){

            var div = $(this).parents('.places-options');
            var place_id = $(this).find('input').data('place');
            var date = $(this).find('input').data('date');
            var value = $(this).find('input').data('value');
            var morning = $(this).find('input:first').data('event-morning');
            var evening = $(this).find('input:first').data('event-evening');
            var event_id = $(this).find('input:first').data('event-id');
            var user_id = $(this).find('input:first').data('user-id');
            var valid_until = $(this).parents('.places-options').find('input.valid_until').val();

            if(date && place_id){
                $('#loading').fadeIn(100);
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: basePath + '/places/setOption',
                    data: {
                        place_id: place_id,
                        date: date,
                        value: value,
                        morning: morning,
                        evening: evening,
                        event_id: event_id,
                        user_id: user_id,
                        valid_until: valid_until
                    },
                    success: function(data){
                        $('#loading').fadeOut(100);
                        div.find('span:not(.input-group-btn)').addClass('hidden').html('');
                        toastr.success('Option has been saved.');
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log(request.responseText);
                    }
                })
            }
        })


    }

    var handleEditDocument = function() {
        $('body').on('click', '.edit-document', function(event){
            event.preventDefault();
            var modal = $(this).parents('.modal');
            var id = modal.find('input:first').data('id');
            var newName = modal.find('input#newName'+id).val();
            var group = modal.find('select#group'+id).val();

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: basePath + '/documents/edit/' + id + '/' + newName+ '/' + group,
                success: function(data){
                    toastr.success('The document has been correctly updated. Changes will be visible after refreshing the page.', 'Success');
                    modal.modal('hide');
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            })
        })
    }

    var handleDeleteDocument = function() {
        $('body').on('click', '.delete-document', function(event){
            event.preventDefault();
            var documentId = $(this).data('document-id');
            var div = $(this).parents('.mix');

            bootbox.confirm("Are you sure you want to delete this document?", function(result) {
                if(result){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: basePath + '/documents/delete',
                        data: {
                            documentId: documentId
                        },
                        success: function(data){
                            toastr.success('Document has been correctly deleted!', 'Success');
                            div.fadeOut();
                        },
                        error: function(request, errorType, errorText) {
                            toastr.error('Something wrong happened. Please try again.');
                            console.log([
                                'Error Type: ' + errorType,
                                'Error Text: ' + errorText
                            ].join('\n'));
                        }
                    })
                }
            });
        })
    }

    var handleSelectCommune = function() {
        $("#communes, #PlaceZipCity, #SearchZipCity, #StockOrderDeliveryZipCity, #StockOrderReturnZipCity, #StockOrderInvoiceZipCity").select2({
            minimumInputLength: 2,
            communes: [],
            ajax: {
                url: basePath + "/communes/get",
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function (term) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.Commune.zip_name,
                                id: item.Commune.id,
                                name: item.Commune.name,
                                zip: item.Commune.zip
                            }
                        })
                    };
                }
            },
            initSelection: function(element, callback){
                var data = {text: element.data('default')};
                if(data.text === undefined){data.text = ''}
                callback(data);
            }
        });

        $("#PlaceZipCity, .zip-city, #StockOrderDeliveryZipCity, #StockOrderReturnZipCity").on('select2-selecting', function(val, choice){
            if($(this).attr('id') == 'StockOrderDeliveryZipCity'){
                $('#StockOrderDeliveryZip').val(val.object.zip);
                $('#StockOrderDeliveryCity').val(val.object.name);
            }
            else if($(this).attr('id') == 'StockOrderReturnZipCity'){
                $('#StockOrderReturnZip').val(val.object.zip);
                $('#StockOrderReturnCity').val(val.object.name);
            } else {
                $('#PlaceZip, .zip').val(val.object.zip);
                $('#PlaceCity, .city').val(val.object.name);
            }

            var address = $('#PlaceAddress').val();
            var zip = $('#PlaceZip').val();
            var city = $('#PlaceCity').val();

            $('#PlaceLatitude, #PlaceLongitude').addClass('spinner');

            $.ajax({
                url: basePath + '/places/getLatLng',
                data: {
                    address: address,
                    city: city,
                    zip: zip
                },
                type: 'POST',
                dataType: 'json',
                success: function(data){
                    $('#PlaceLatitude').val(data.latitude);
                    $('#PlaceLongitude').val(data.longitude);
                    $('#PlaceLatitude, #PlaceLongitude').removeClass('spinner');
                }
            })
        });
        $('body').on('blur', '#PlaceAddress', function(event){

            var address = $('#PlaceAddress').val();
            var zip = $('#PlaceZip').val();
            var city = $('#PlaceCity').val();

            if(zip.length > 0 && city.length > 0){
                $('#PlaceLatitude, #PlaceLongitude').val('').addClass('spinner');
                $.ajax({
                    url: basePath + '/places/getLatLng',
                    data: {
                        address: address,
                        city: city,
                        zip: zip
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(data){
                        $('#PlaceLatitude').val(data.latitude);
                        $('#PlaceLongitude').val(data.longitude);
                        $('#PlaceLatitude, #PlaceLongitude').removeClass('spinner');
                    }
                })
            }
        })
    }

    var handleTagsSelection = function(){
        if($(".select2").length > 0){
            $(".select2").select2({
                multiple: true,
                closeOnSelect: false,
                ajax: {
                    url: basePath + '/tags/select2',
                    dataType: 'json',
                    type: 'POST',
                    data: function(term, page) {
                        return {
                            term: term,
                            category: $(this).data('category')
                        };
                    },
                    results: function(data, page) {
                        return {
                            results: data
                        };                    
                    }
                },
                initSelection: function(element, callback){
                    var data = [];
                    $(element.val().split(",")).each(function (i,e) {
                        $.ajax({
                            url: basePath + '/tags/getValue',
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                id: e
                            },
                            success: function(value){
                                data.push({id: e, text: value});
                                callback(data);
                            }   
                        })
                    });                
                }
            });
        }

    }

    var portletDraggable = function(){
        $("#sortable_portlets").sortable({
            connectWith: ".portlet",
            handle: ".portlet-title",
            items: ".portlet", 
            opacity: 0.8,
            coneHelperSize: true,
            placeholder: 'portlet-sortable-placeholder',
            forcePlaceholderSize: true,
            tolerance: "pointer",
            helper: "clone",
            cancel: ".portlet-sortable-empty",
            revert: 250, // animation in milliseconds
            update: function(b, c) {
                if (c.item.prev().hasClass("portlet-sortable-empty")) {
                    c.item.prev().before(c.item);
                }
                $('#sortable_portlets .portlet').each(function(i,e){
                    $(e).find('.weight').attr('value',i);
                })             
            }
        });
    }

    var handleLoadEvent = function(){
        if($('#SearchEventId').length){
            $('#SearchEventId').bind('change', function(){
                var eventId = $(this).find(':selected').val();
                $.ajax({
                    url: basePath + '/events/getData',
                    data: {
                        id: eventId
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(data){
                        $('#EventClientId').find('option[value="'+data.Client.id+'"]').attr('selected', 'selected');
                        $('#EventConfirmedDate').val(data.Event.confirmed_date);
                        var potentialDates = '';
                        data.Date.forEach(function(e){
                            potentialDates += e.date+'  ';
                        })
                        $('#EventPotentialDates').val(potentialDates);
                    }
                })
            })
        }
    }

    var handleAvailabilities = function() {
        $('body').on('click', 'div.btn-group label.btn', function(event){
            event.preventDefault();
            $(this).parents('.btn-group').find('input').removeAttr('checked');
            $(this).parents('.btn-group').find('label').removeClass('active');
            $(this).find('input').attr('checked', 'checked');
        })
    }

    var handleCheckAll = function(){
        $('#EmailUserCheckAll').change(function(){
            if($(this).is(':checked')){
                $('input[type="checkbox"]').attr('checked', 'checked');
                $('input[type="checkbox"]').parents('span').addClass('checked');
            } else {
                $('input[type="checkbox"]').removeAttr('checked');
                $('input[type="checkbox"]').parents('span').removeClass('checked');
            }
        })
    }

    var mapMarker = function () {
        var div = $('#gmap_marker');
        var lat = div.data('lat');
        var lng = div.data('lng');
        var title = div.data('title');
        var map = new GMaps({
            div: '#gmap_marker',
           lat: lat,
                lng: lng,
        });
        map.addMarker({
           lat: lat,
                lng: lng,
            title: 'Lima'
        });
        map.setZoom(16);
    }
    
    var handleDatatable = function(table, aTargets, mColumns, sort, defaultSort) {

        if(table.length > 0){
            if(typeof(sort) == 'undefined') sort = true;

            TableTools.DEFAULTS.aButtons = [ "pdf" ];
            TableTools.DEFAULTS.sSwfPath = "../TableTools-2.0.0/media/swf/copy_cvs_xls_pdf.swf";

            var id = table.attr('id');

            /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

            /* Set tabletools buttons and button container */

            $.extend(true, $.fn.DataTable.TableTools.classes, {
                "container": "btn-group tabletools-dropdown-on-portlet",
                "buttons": {
                    "normal": "btn default",
                    "disabled": "btn btn-sm default disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
                }
            });

            var oTable = table.DataTable({

                // Internationalisation. For more info refer to http://datatables.net/manual/i18n
                "language": {
                    "aria": {
                        "sortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sortDescending": ": activer pour trier la colonne par ordre décroissant"
                    },
                    "emptyTable": "Aucune donnée disponible dans le tableau",
                    "info": "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "infoEmpty": "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    "infoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "lengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "search": "Rechercher&nbsp;:",
                    "zeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher"
                },
                
                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],

                "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': aTargets }
                ],

                // set the initial value
                "pageLength": 20,

                "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "tableTools": {
                    "sSwfPath": basePath + "/metronic/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
                    "aButtons": [{
                        "sExtends": "pdf",
                        "sButtonText": "Exporter en PDF",
                        "mColumns": mColumns
                    }]
                },

                bSort: sort,
                order: defaultSort
            });

            if(table.attr('id') == 'users1'){
                table.dataTable().columnFilter({
                    sPlaceHolder : 'head:before',
                    aoColumns: [ null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 { type: "select", values: [ 'Animation', 'F&B', 'Logistique']},
                                 null
                               ] 
                });
            }

            if(table.attr('id') == 'users2'){
                table.dataTable().columnFilter({
                    sPlaceHolder : 'head:before',
                    aoColumns: [ null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 { type: "select", values: [ 'Animation', 'F&B', 'Logistique']},
                                 null
                               ] 
                });
            }

            var tableWrapper = $('#'+id+'_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        }

    }

    var initDatatables = function(tabsDiv){
        tabsDiv.tabs({
            activate: function(event, ui) {
                ttInstances = TableTools.fnGetMasters();
                for (i in ttInstances) {
                    if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
                }
            }
        });
    }

    var handleCompetencesJobs = function(controller){
        $('body').on('change', 'select.sector', function(){
            var portlet = $(this).parents('.portlet.element');
            var selectedValue = $(this).val();
            var selectedOption = $(this).find('option:selected').text();

            portlet.find('select:not(.sector)').addClass('hidden');
            portlet.find('select:not(.sector)').val('');
            portlet.find('.caption span').text('');
            portlet.find('span.sector').text(selectedOption);
            portlet.find('.col-md-3.activities').addClass('hidden');

            if(selectedValue == 'animation') portlet.find('.animation_jobs').removeClass('hidden');
            if(selectedValue == 'logistics') portlet.find('.logistics_jobs').removeClass('hidden');
            if(selectedValue == 'fb') portlet.find('.fb_jobs').removeClass('hidden');
            
            saveElement(portlet, controller);
        });
        $('body').on('change', 'select.jobs', function(){
            var portlet = $(this).parents('.portlet.element');
            var selectedValue = $(this).val();
            var selectedOption = $(this).find('option:selected').text();
            var selectedSector = portlet.find('select.sector').val();

            portlet.find('span.job').text(selectedOption);

            if(selectedSector == 'animation'){
                portlet.find('.col-md-3.activities, .col-md-2.activities').removeClass('hidden');
                if(selectedValue == 'urban_leader' || selectedValue == 'urban_coach'){
                    portlet.find('.ubicActivities').addClass('hidden').val('');
                    portlet.find('.ugActivities').removeClass('hidden');
                } else if(selectedValue == 'animator' || selectedValue == 'animator_facilitator'){
                    portlet.find('.ubicActivities').removeClass('hidden');
                    portlet.find('.ugActivities').addClass('hidden').val('');
                } else {
                    portlet.find('.col-md-3.activities').addClass('hidden');
                    portlet.find('.ubicActivities').addClass('hidden').val('');
                    portlet.find('.ugcActivities').addClass('hidden').val('');
                    portlet.find('.hierarchies').removeClass('hidden');
                }
            } else {
                portlet.find('.hierarchies').removeClass('hidden');
            }

            saveElement(portlet, controller);
        });
        $('body').on('change', '.ugActivities, .ubicActivities', function(){
            var portlet = $(this).parents('.portlet.element');
            var selectedValue = $(this).val();
            var selectedOption = $(this).find('option:selected').text();

            portlet.find('span.activity').text(selectedOption);
            portlet.find('.hierarchies').removeClass('hidden');

            saveElement(portlet, controller);
        });
        $('body').on('change', '.hierarchies', function(){
            var portlet = $(this).parents('.portlet.element');
            var selectedValue = $(this).val();
            var selectedOption = $(this).find('option:selected').text();

            portlet.find('span.hierarchy').text(selectedOption);

            saveElement(portlet, controller);
            if(controller == 'jobs'){
                portlet.find('.salaries').removeClass('hidden');
            }
        });
        $('body').on('change', 'input.start_time, input.end_time, input.remarks, input.salaries', function(){
            var portlet = $(this).parents('.portlet.element');
            saveElement(portlet, controller);
        });
        $('body').on('change', '.user_id', function(){
            var portlet = $(this).parents('.portlet.element');
            portlet.find('.salaries').val('');
            saveElement(portlet, controller);
        });
        $('body').on('click', '.add-element', function(event){
            event.preventDefault();
            var portletClone = $('.portlet.element:last').clone();
            portletClone.find('select:not(.sector)').addClass('hidden');
            portletClone.find('select').val('');
            portletClone.find('select option').removeAttr('selected');
            portletClone.find('input.id').val('');
            portletClone.find('input.salaries').val('').addClass('hidden');
            portletClone.find('span.salaries').addClass('hidden');
            portletClone.find('.caption span').text('');
            portletClone.appendTo('#elements');
        });
        $('body').on('click', '.remove-element', function(event){
            event.preventDefault();
            var portlet = $(this).parents('.portlet.element');
            var id = portlet.find('input.id').val();
            bootbox.confirm("Are you sure you want to delete this element?", function(result) {
                if(result){
                    removeElement(id, portlet, controller);
                }
            });
        });

        var removeElement = function(id, portlet, controller){
            
            portlet.find('.fa-spin').removeClass('hidden');
            
            $.ajax({
                url: basePath + '/'+controller+'/delete',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(data){
                    if(data.success != 0){
                        toastr.success('Element has been correctly removed.');
                        portlet.fadeOut();
                    } else {
                        toastr.error('Something wrong happened. Please try again.');
                    }
                    portlet.find('.fa-spin').addClass('hidden');
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            });
        }

        var saveElement = function(portlet, controller){
            if( portlet.find('select.hierarchies option:selected').val() ){

                var id = portlet.find('input.id').val();
                var user_id = portlet.find('input.user_id').val();
                var event_id = portlet.find('input.event_id').val();
                var sector = portlet.find('select.sector option:selected').val();
                var job = portlet.find('select.jobs option[value!=""]:selected').val();
                var hierarchy = portlet.find('select.hierarchies option[value!=""]:selected').val();
                var activity = portlet.find('.activities select option[value!=""]:selected').val();
                var name = portlet.find('.caption span.sector').text() + ' ';
                name += portlet.find('.caption span.job').text() + ' ';
                name += portlet.find('.caption span.activity').text() + ' ';
                name += portlet.find('.caption span.hierarchy').text();
                var model = portlet.find('input.model').val();
                var amount = portlet.find('input.amount').val();
                var start_time = portlet.find('input.start_time').val();
                var salary = portlet.find('input.salaries').val();
                var end_time = portlet.find('input.end_time').val();
                var remarks = portlet.find('input.remarks').val();
                var worker_id = portlet.find('select.user_id option[value!=""]:selected').val();

                portlet.find('.fa-spin').removeClass('hidden');

                /*console.log(id);
                console.log(user_id);
                console.log(event_id);
                console.log(sector);
                console.log(job);
                console.log(hierarchy);
                console.log(activity);
                console.log(name);
                console.log(model);
                console.log(controller);
                console.log(amount);
                console.log(start_time);
                console.log(end_time);
                console.log(worker_id);
                return;*/

                $.ajax({
                    url: basePath + '/'+controller+'/save',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: id,
                        user_id: user_id,
                        event_id: event_id,
                        sector: sector,
                        job: job,
                        hierarchy: hierarchy,
                        activity: activity,
                        name: name,
                        amount: amount,
                        start_time: start_time,
                        end_time: end_time,
                        remarks: remarks,
                        worker_id: worker_id,
                        salary: salary
                    },
                    success: function(data){
                        console.log(data);
                        if(data.success != 0){
                            if(controller == 'competences'){
                                toastr.success('Competence has been saved.');
                                portlet.find('input.id').val(data.id);
                            }
                            if(controller == 'jobs'){
                                toastr.success('Job has been saved.');
                                portlet.find('input.id').val(data.id);
                                portlet.find('input.salaries').val(data.salary);
                            }
                            
                        } else {
                            toastr.error('Something wrong happened. Please try again.');
                        }
                        portlet.find('.fa-spin').addClass('hidden');
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                });
            } else {
                return false;
            }
            
        }

    }

    var handleMoments = function(controller){
        $('body').on('change', 'input.name', function(){
            var portlet = $(this).parents('.portlet.moment');
            var selectedValue = $(this).val();
            portlet.find('span.name').text(selectedValue);
            saveMoment(portlet, controller);
        });
        $('body').on('change', 'input.start_hour', function(){
            toastr.clear();
            var portlet = $(this).parents('.portlet.moment');
            var selectedValue = $(this).val();
            portlet.find('span.start_hour').text(selectedValue);
            saveMoment(portlet, controller);
        });
        $('body').on('change', 'input.end_hour', function(){
            toastr.clear();
            var portlet = $(this).parents('.portlet.moment');
            var selectedValue = $(this).val();
            portlet.find('span.end_hour').text(selectedValue);
            saveMoment(portlet, controller);
        });
        $('body').on('change', 'input.remarks', function(){
            toastr.clear();
            var portlet = $(this).parents('.portlet.moment');
            var selectedValue = $(this).val();
            saveMoment(portlet, controller);
        });
        $("#MomentPlaces").on("select2-selecting", function(e, choice) {
            var portlet = $(this).parents('.portlet.moment');
            portlet.find('.place_id').val(e.val);
            saveMoment(portlet, controller);
        })
        $('body').on('click', '.add-moment', function(event){
            event.preventDefault();
            var portletClone = $('.portlet.moment.empty').clone();
            portletClone.find('select:not(.sector)').addClass('hidden');
            portletClone.find('select').val('');
            portletClone.find('select option').removeAttr('selected');
            portletClone.find('input.id, input.name, input.remarks').val('');
            portletClone.find('.caption span').text('');
            portletClone.appendTo('#moments1');
        });
        $('body').on('click', '.remove-moment', function(event){
            event.preventDefault();
            var portlet = $(this).parents('.portlet.moment');
            var id = portlet.find('input.id').val();
            bootbox.confirm("Are you sure you want to delete this moment?", function(result) {
                if(result){
                    removeMoment(id, portlet, controller);
                }
            });
        });

        var removeMoment = function(id, portlet, controller){
            
            portlet.find('.fa-spin').removeClass('hidden');
            
            $.ajax({
                url: basePath + '/'+controller+'/delete',
                type: 'post',
                dataType: 'json',
                data: {
                    id: id
                },
                success: function(data){
                    if(data.success != 0){
                        toastr.success('Element has been correctly removed.');
                        portlet.fadeOut();
                    } else {
                        toastr.error('Something wrong happened. Please try again.');
                    }
                    portlet.find('.fa-spin').addClass('hidden');
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            });
        }

        var saveMoment = function(portlet, controller){
            if( portlet.find('input.name').val().length > 0 ){

                var id = portlet.find('input.id').val();
                var event_id = portlet.find('input.event_id').val();
                var place_id = portlet.find('input.place_id').val();
                var name = portlet.find('input.name').val();
                var start_hour = portlet.find('input.start_hour').val();
                var end_hour = portlet.find('input.end_hour').val();
                var remarks = portlet.find('input.remarks').val();

                portlet.find('.fa-spin').removeClass('hidden');

                $.ajax({
                    url: basePath + '/'+controller+'/save',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        id: id,
                        event_id: event_id,
                        place_id: place_id,
                        name: name,
                        start_hour: start_hour,
                        end_hour: end_hour,
                        remarks: remarks
                    },
                    success: function(data){
                        if(data.success != 0){
                            toastr.options = {
                                "preventDuplicates" : true,
                                //"closeButton": true
                            }
                            toastr.success('Moment has been saved.');
                            portlet.find('input.id').val(data.id);
                            
                        } else {
                            toastr.error('Something wrong happened. Please try again.');
                        }
                        portlet.find('.fa-spin').addClass('hidden');
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                });
            } else {
                return false;
            }
            
        }

    }

    var handleSpinners = function(){
        $('#spinner1').spinner();
    }

    var handleRecruit = function(){
        $('body').on('click', '.recruit', function(event){
            event.preventDefault();

            var searchId = $(this).data('search-id');
            var fullName = $(this).data('full-name');
            var userId = $(this).data('user-id');
            var jobId = $(this).data('job-id');

            bootbox.confirm("Are you sure you want to recruit "+fullName+"?", function(result) {
                $('#loading').fadeIn();
                if(result){
                    $.ajax({
                        url: basePath + '/users/recruit',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            search_id: searchId,
                            user_id: userId,
                            job_id: jobId
                        },
                        success: function(data){
                            $('#loading').fadeOut();
                            if(data.success){
                                toastr.success('Mail has been sent!');
                            } else {
                                toastr.error('Something wrong happened. Please try again.');
                            }
                        },
                        error: function(request, errorType, errorText) {
                            $('#loading').fadeOut();
                            toastr.error('Something wrong happened. Please try again.');
                            console.log([
                                'Error Type: ' + errorType,
                                'Error Text: ' + errorText
                            ].join('\n'));
                        }
                    });
                }
            });
        })
    }

    var handleTravelTime = function(){
        $('span.travel').each(function(i,e){
            var origin = $(e).data('origin');
            var destination = $(e).data('destination');
            $.ajax({
                url: basePath + '/search/getTravelTime',
                type: 'post',
                dataType: 'json',
                data: {
                    origin: origin,
                    destination: destination
                },
                success: function(data){
                    $(e).html(data);
                    /*if(data.success){
                        toastr.success('Mail has been sent!');
                    } else {
                        toastr.error('Something wrong happened. Please try again.');
                    }*/
                },
                error: function(request, errorType, errorText) {
                    //toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            })
        })
    }

    var handleRiseUser = function() {
        $('body').on('click', '.riseUser', function(event){
            event.preventDefault();
            var url = $(this).attr('href');
            var row = $(this).parents('tr');
            //$('#loading').fadeIn(100);
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                success: function(data){
                    if(data.success){
                        //$('#loading').fadeOut(100);
                        toastr.success('Competence has been upgraded!');
                        row.css('opacity', '0.6').find('a.riseUser').hide();
                    } else {
                        toastr.error('Something wrong happened. Please try again.');
                    }
                },
                error: function(request, errorType, errorText) {
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + errorText
                    ].join('\n'));
                }
            })
        })
    }

    var mapMarkerPlaces = function () {
        
    }

    var handleShowMapList = function(){

        

    }

    var handleAddStockItem = function(){

        if($("#StockOrderAddStockItemQuantity").length > 0){
            $("#StockOrderAddStockItemQuantity").inputmask({
                "mask": "9",
                "repeat": 10,
                "greedy": false
            });
        }
        
        $("#StockOrderAddStockItem").select2({
            placeholder: "Search for an item",
            minimumInputLength: 1,
            ajax: {
                url: basePath + '/stock_items/json',
                dataType: 'json',
                type: 'POST',
                data: function(term, page) {
                    // get quantity of wanted stock item
                    if($(this).parents('.form-group').find('#StockOrderAddStockItemQuantity').val().length > 0){
                        var quantity = $(this).parents('.form-group').find('#StockOrderAddStockItemQuantity').val();
                    } else {
                        var quantity = 1;
                    }

                    // get delivery date
                    var delivery_date = $('#StockOrderDeliveryDate').val();
                    // get return date
                    var return_date = $('#StockOrderReturnDate').val();

                    return {
                        term: term,
                        delivery_date: delivery_date,
                        return_date: return_date,
                        quantity: quantity
                    };
                },
                results: function(data, page) {
                //console.log(data);    
                    return {
                        results: data
                    };       
                }
            },
            formatResult: format

        });
        function format(product){
            if(product.availability == 0){
                return '<span class="text-muted" style="text-decoration: line-through">'+product.text+'</span>'
            } else {
                return product.text;
            }
        }
        $("#StockOrderAddStockItem").on("select2-selecting", function(e) {

            var deliveryDate = $('#StockOrderServiceDateBegin').val().split('-');
            var returnDate = $('#StockOrderServiceDateEnd').val().split('-');

            var date1 = new Date( deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2] );
            var date2 = new Date( returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2] );
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            var coefficient = 1;

            var numberOfRows = $('#stockitemsTable tbody tr:not(.empty)').get().length;

            if(diffDays > 0 && diffDays <= 2){
                coefficient = 1;
            } else if(diffDays > 2 && diffDays <= 9){
                coefficient = 1.3;
            } else if(diffDays > 9 && diffDays <= 16){
                coefficient = 1.6;
            } else if(diffDays > 16 && diffDays <= 23){
                coefficient = 1.9;
            } else if(diffDays > 23){
                coefficient = 2.2;
            }

            var line = $('#stockitemsTable tr.empty').clone().removeClass('hidden').removeClass('empty');
            line.find('td.quantity span').text(e.choice.quantity);
            line.find('td.code').text(e.choice.code);
            line.find('td.stockitem').html(e.choice.name + '<br>' + e.choice.section);
            line.find('td.price').text(e.choice.price);
            line.find('td.priceWithDisount').text(e.choice.price);
            line.find('td.coefficient input').val(coefficient);
            line.find('input.quantity').val(e.choice.quantity);
            line.find('td.quantity span.help-text').html(' / ' + e.choice.totalQuantity)
            line.find('input.price').val(e.choice.price);
            line.find('input.weight').val(numberOfRows);
            line.find('input.stock_item_id').val(e.choice.id);
            line.insertBefore($('#stockitemsTable tbody tr.empty'));

            checkAvailability(e.choice.id, e.choice.quantity, date1, diffDays, line);

        }).on('select2-close', function(e){
            computeTotal();
            $("#StockOrderAddStockItem").select2('val', '');
            setTimeout(function() {
                $('.select2-container-active').removeClass('select2-container-active');
                $(':focus').blur();
                $('#StockOrderAddStockItemQuantity').focus().select();
            }, 1);
        });

        $('body').on('click', '.remove-stock-item', function(event){
            event.preventDefault();
            var $this = $(this);
            var line = $this.parents('tr');
            bootbox.confirm("Are you sure you want to delete this item?", function(result) {
                if(result){
                    line.fadeOut(function(){
                        line.remove();
                    });
                    setTimeout(function(){
                        computeTotal();
                    }, 500);
                }
            })
        });

        $('body').on('change', '.coefficient, .discount, .quantity', function(event){
            computeTotal();
        });

        $('body').on('change', 'input.quantity', function(event){
            var line = $(this).parents('tr');
            var stockItemId = line.find('.stock_item_id').val();
            var amount = line.find('input.quantity').val();
            var deliveryDate = $('#StockOrderServiceDateBegin').val().split('-');
            var returnDate = $('#StockOrderServiceDateEnd').val().split('-');

            var date1 = new Date( deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2] );
            var date2 = new Date( returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2] );
            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

            checkAvailability(stockItemId, amount, date1, diffDays, line);
        });

        var computeTotal = function(){
            var total = 0;
            var prices = $('#stockitemsTable tbody tr:not(.empty)').each(function(i,e){
                var price = parseFloat($(e).find('input.price').val());
                var quantity = parseFloat($(e).find('input.quantity').val());
                var coefficient = parseFloat($(e).find('input.coefficient').val());
                var discount = $(e).find('input.discount').val().length > 0 ? parseFloat($(e).find('input.discount').val()) : 0;
                var discountAmount = quantity * price * coefficient * discount / 100;
                var netHt = parseFloat(quantity * price * coefficient).toFixed(2);
                var totalTtc = parseFloat(netHt - (netHt * discount / 100)).toFixed(2);
                $(e).find('td.net_ht').text(netHt);
                $(e).find('td.total_ttc').text(totalTtc);
                if(!isNaN(price)) total += quantity * price * coefficient - discountAmount;
            });
            var stockOrderTotal = (Math.ceil(total*20)/20).toFixed(2);
            $('#StockOrderTotalHt').val(stockOrderTotal).trigger('change');
            $('.total_ht span').text(stockOrderTotal);
        }

        if(document.location.href.indexOf('stock_orders/edit/') > 0){
           computeTotal();
        }
        

        var checkAvailability = function(stockItemId, amount, date, duration, line){

            line.find('.feedback ul').html('');
            line.find('.feedback .loader').removeClass('hidden');
            line.find('.feedback span.text-success, .feedback span.text-danger').addClass('hidden');

            $.ajax({
                url: basePath + '/stock_items/checkAvailability',
                type: 'post',
                dataType: 'json',
                data: {
                    stock_item_id: stockItemId,
                    duration: duration,
                    amount: amount,
                    startDate: date
                },
                success: function(data){
                    if(data.conflicts.dates.length == 0){
                        line.find('.loader').addClass('hidden');
                        line.find('.text-success').removeClass('hidden');
                    }
                    if(data.conflicts.dates.length > 0){
                        var showDanger = false;
                        var showWarning = false;
                        line.find('.loader').addClass('hidden');
                        $.each(data.availabilities, function(date,values){
                            if(values.potential < 0){
                                showDanger = true;
                                line.find('.feedback ul').append('<li class="text-danger">'+date+' : '+Math.abs(values.potential)+' manquants</li>');
                            }
                            else if(values.potential == 0){
                                showWarning = true;
                                line.find('.feedback ul').append('<li class="text-warning">'+date+' : 0 disponible</li>');
                            } else {
                                showWarning = true;
                                showDanger = false;
                            }                    
                        })
                        if(showDanger){
                            line.find('.text-danger').removeClass('hidden');
                        }
                        if(showWarning){
                            line.find('.text-warning').removeClass('hidden');
                        }
                    }
                }
            })
        }

    }

    var handleGetLatLngCommune = function(){
        $('#CommuneZip').blur(function(){
            if($('#CommuneZip').val() && $('#CommuneName').val()){
                $('#CommuneLatitude, #CommuneLongitude').addClass('spinner');
                $.ajax({
                    url: basePath + '/communes/getLatLng',
                    data: {
                        city: $('#CommuneName').val(),
                        zip: $('#CommuneZip').val()
                    },
                    type: 'POST',
                    dataType: 'json',
                    success: function(data){
                        $('#CommuneLatitude').val(data.latitude);
                        $('#CommuneLongitude').val(data.longitude);
                        $('#CommuneLatitude, #CommuneLongitude').removeClass('spinner');
                    }
                })
            }
        })
        /**/
    }

    var handleDocumentsActions = function(){
        $('body').on('click', '#internalPhotosActions li a, #clientPhotosActions li a, #documentsActions li a', function(event){
            event.preventDefault();
            var group = $(this).data('group');
            var docs = $('input.selectme:checked').get();

            if(docs.length > 0){
                $.each(docs, function(i,e){
                    var id = $(e).data('id');
                    var div = $(e).parents('.panel').find('small.group');
                    $.ajax({
                        url: basePath + '/documents/editGroup/' + id + '/' + group,
                        type: 'POST',
                        dataType: 'json',
                        success: function(data){
                            $('input.selectme').attr('checked', false);
                            $('input.selectme').parents('span').removeClass('checked');
                            div.text(data.group);
                            toastr.success('Document has been updated.');
                        }
                    })
                })
            } else {
                toastr.error('No document has been selected!');
            }
        })
    }

    var handleSelectCategory = function(){
        $('body').on('change', '#PriceListCategoryId', function(){
            event.preventDefault();
            var categoryId = $(this).val();
            $('#PriceListSectionId option').remove();

            if(categoryId.length > 0){
                $.ajax({
                    url: basePath + '/stock_items/getSections',
                    type: 'POST',
                    data: {
                        stock_category_id: categoryId
                    },
                    dataType: 'json',
                    success: function(data){
                        $('#PriceListSectionId').append('<option value="">Select an option</option>');
                        $.each(data, function(i,e){
                            $('#PriceListSectionId').append('<option value="'+i+'">'+e+'</option>');
                        })
                    }
                })
            } else {
                
            }
        })
    }

    var handleStockItemsImages = function(){
        $('.product .image').each(function(i,e){
            var stockItemId = $(e).data('article-id');
            var div = $(e);
            $.ajax({
                url: basePath + '/stock_items/getImage/' + stockItemId,
                type: 'post',
                dataType: 'json',
                success: function(data){
                    if(data.success == 1){
                        div.html(data.image);
                    } else {
                        div.html('<i class="fa fa-times"></i>');
                    }
                }
            })
        })
    }

    var handleSectionsFamilies = function(){
        $('body').on('change', '#StockItemStockCategoryId', function(){
            event.preventDefault();
            var categoryId = $(this).val();
            $('#StockItemStockSectionId option').remove();
            $('#StockItemStockFamilyId option').remove();
            getSections(categoryId, '#StockItemStockSectionId');
        })
        $('body').on('change', '#StockItemStockSectionId', function(){
            event.preventDefault();
            var categoryId = $('#StockItemStockCategoryId').val();
            var sectionId = $(this).val();
            $('#StockItemStockFamilyId option').remove();
            getFamilies(categoryId, sectionId, '#StockItemStockFamilyId');
        })
    }

    var getSections = function(category, selectId){
        $.ajax({
            url: basePath + '/stock_items/getSections',
            data: {
                stock_category_id: category
            },
            type: 'post',
            dataType: 'json',
            success: function(data){
                if(data){
                    $.each(data, function(i,e){
                        $(selectId).append('<option value="'+i+'">'+e+'</option>')
                    })
                }
            }
        })
    }
    var getFamilies = function(category, section, selectId){
        $.ajax({
            url: basePath + '/stock_items/getFamilies',
            data: {
                stock_category_id: category,
                stock_section_id: section
            },
            type: 'post',
            dataType: 'json',
            success: function(data){
                if(data){
                    $.each(data, function(i,e){
                        $(selectId).append('<option value="'+i+'">'+e+'</option>')
                    })
                }
            }
        })
    }

    var handleSelectStockItem = function(select) {
        select.select2({
            minimumInputLength: 2,
            stockitems: [],
            ajax: {
                url: basePath + "/stock_items/get",
                dataType: 'json',
                type: "POST",
                quietMillis: 50,
                data: function (term) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.StockItem.code + ' ' + item.StockItem.name,
                                id: item.StockItem.id
                                /*name: item.Commune.name,
                                zip: item.Commune.zip*/
                            }
                        })
                    };
                }
            },
            initSelection: function(element, callback){
                var data = {text: element.data('default')};
                if(data.text === undefined){data.text = ''}
                callback(data);
            }
        });

        $("#selectStockItem").on('select2-selecting', function(val, choice){
            window.location = basePath + '/stock_items/view/' + val.val;
        });
    }

    var handleDeleteStockItem = function(){

        $('body').on('click', '.deleteStockItem', function(){
            event.preventDefault();
            var redirect = $(this).attr('href');
            bootbox.confirm("Are you sure you want to delete this item?", function(result) {
                if(result){
                    window.location = redirect;
                }
            })
        })

        
    }

    var handleRadioButtons = function(){
        $('input.toggle[type="radio"]').each(function(i,e){
            if($(e).is(':checked')){
                $(e).parents('.btn').addClass('active')
            }
        })
    }

    var handleContactPeople = function(clientInput, contactPeopleInput){
        $('body').on('change', clientInput, function(){
            $(contactPeopleInput).find('option').remove();
            var client_id = $(this).val();
            $.ajax({
                url: basePath + '/clients/getContactPeoples',
                data: {
                    client_id: client_id
                },
                type: 'post',
                dataType: 'json',
                success: function(data){
                    if(data){
                        $.each(data, function(i,e){
                            $(contactPeopleInput).append('<option value="'+e.id+'">'+e.text+'</option>');
                        })
                        $(contactPeopleInput).selectpicker('refresh');
                    }
                    if(clientInput == '#StockOrderClientId'){
                        $.ajax({
                            url: basePath + '/clients/get',
                            data: {
                                client_id: client_id
                            },
                            type: 'post',
                            dataType: 'json',
                            success: function(data){
                                $('#StockOrderInvoiceAddress').val(data.Client.address);
                                $('#StockOrderInvoiceZip').val(data.Client.zip);
                                $('#StockOrderInvoiceCity').val(data.Client.city);
                                $('#StockOrderInvoiceZipCity').select2('data', {text: data.Client.zip_city});
                            }
                        });
                    }        
                }
            })
        })
    }

    var handlePlacesResults = function(){

        $('button.date, button.moment').click(function(event){
            event.preventDefault();
            if($(this).hasClass('date')){
                $('button.date').removeClass('active');
            }
            if($(this).hasClass('moment')){
                $('button.moment').removeClass('active');
            }
            $(this).addClass('active');
            $('.portlet.place').each(function(i,e){
                checkOptions($(e));
            })
        });

        var numberOfPlaces = $('.portlet.place:not(.origin)').get().length;

        $('.portlet.place:not(.origin)').each(function(i,e){
            Metronic.blockUI({
                target: $(e).find('.portlet-body'),
                animate: true,
                overlayColor: 'none'
            });
            $.ajax({
                url: basePath + '/search/place/' + $(e).data('place-id'),
                success: function(data){
                    Metronic.unblockUI($(e).find('.portlet-body'));
                    $(e).find('.portlet-body').append(data);
                    if($(e).data('complex-search') == 1){
                        checkOptions($(e));
                        computeTravelTime($(e));
                    } else {
                        $(e).find('span.distance').parents('li').hide();
                        $(e).find('span.distanceUBIC').parents('li').hide();
                    }
                    if(typeof($('#SearchOrigin').val()) !== 'undefined' && $(e).data('complex-search') == 0){
                        computeTravelTime($(e));
                        $(e).find('span.distanceUBIC').parents('li').show();
                    }
                    if(i == (numberOfPlaces-1)){
                        $('.scroller').slimScroll({
                            height: '300px',
                            railVisible: false,
                            railOpacity: 0.7,
                            alwaysVisible: false,
                            size: '5px'
                        });
                    }
                }
            })
        })

        if($('#map').length > 0){
            window.map = new google.maps.Map(document.getElementById('gmap_places'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var bounds = new google.maps.LatLngBounds();
            var originLat = $('.portlet.origin').data('lat');
            var originLng = $('.portlet.origin').data('lng');

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(originLat, originLng),
                map: map,
                icon: basePath + '/img/icons/icon_marker_green.png',
                zIndex: 0
            });
            bounds.extend(marker.position);
            var latLng = marker.getPosition(); // returns LatLng object
            map.setCenter(latLng); // setCenter takes a LatLng object
            //map.fitBounds(bounds);

            $('#map').css('height', 0);
            $('body').on('click', '.actions .btn', function(event){
                $('.actions .btn').removeClass('active');
                $(this).addClass('active');
                event.preventDefault();
                var rel = $(this).attr('rel');
                if(rel == 'list'){
                    $('#map').css('height', 0).hide();
                    $('#list').show();
                }
                if(rel == 'map'){
                    $('#map').css('height', 'auto').show();
                    $('#list').hide();
                }
            })

            var listener = google.maps.event.addListener(map, "idle", function() { 
                map.setZoom(10); 
                google.maps.event.removeListener(listener);
                var infowindow = new google.maps.InfoWindow();
                $('.portlet.place:not(.origin)').each(function(i,e){
                    var lat = $(e).data('lat');
                    var lng = $(e).data('lng');
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, lng),
                        map: map,
                        title: $(e).data('place-id').toString()
                    });
                    google.maps.event.addListener(marker, 'click', function() {
                        infowindow.close();
                        var content = $('.portlet.place[data-place-id="'+marker.title+'"] .infowindow').html();
                        infowindow.setContent(content);
                        infowindow.open(map, marker);
                    });
                    bounds.extend(marker.position);
                })
                map.fitBounds(bounds);
            });
        }

        if($('#map').length > 0 && 1==2){
            window.map = new google.maps.Map(document.getElementById('gmap_places'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();
            var bounds = new google.maps.LatLngBounds();
            var originLat = $('.portlet.origin').data('lat');
            var originLng = $('.portlet.origin').data('lng');

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(originLat, originLng),
                map: map,
                icon: basePath + '/img/icons/icon_marker_green.png',
                zIndex: 0
            });
            bounds.extend(marker.position);

            $('#map').css('height', 0);
            $('body').on('click', '.actions .btn', function(event){
                $('.actions .btn').removeClass('active');
                $(this).addClass('active');
                event.preventDefault();
                var rel = $(this).attr('rel');
                if(rel == 'list'){
                    $('#map').css('height', 0).hide();
                    $('#list').show();
                }
                if(rel == 'map'){
                    map.setZoom(3);
                    map.fitBounds(bounds);
                    $('#map').css('height', 'auto').show();
                    $('#list').hide();
                }
            })
        }

        computeDistance = function(div){
            var latitude = div.find('input.latitude').val();
            var longitude = div.find('input.longitude').val();
            var communeLatitude = $('#SearchLatitude').val();
            var communeLongitude = $('#SearchLongitude').val();
            $.ajax({
                url: basePath + '/search/getDistance',
                dataType: 'json',
                type: 'post',
                data: {
                    latA: latitude,
                    lngA: longitude,
                    latB: communeLatitude,
                    lngB: communeLongitude
                },
                success: function(distance){
                    div.find('span.distance').text(distance);
                }
            })
        }

        computeTravelTime = function(div){
            var destination = div.find('input.destination').val();
            var origin = $('#SearchOrigin').val();
            if(destination !== '++'){  
                $.ajax({
                    url: basePath + '/search/getTravelTime',
                    dataType: 'json',
                    type: 'post',
                    data: {
                        destination: destination,
                        origin: origin
                    },
                    success: function(infos){
                        if(infos.time == 0 && infos.distance == 0){
                            div.find('span.time').text('non disponible');
                            div.find('span.distance').hide();
                        } else {
                            div.find('span.time').text(' - ' + infos.time);
                            div.find('span.distance').text(infos.distance);
                        }
                        if(infos.timeUBIC == 0 && infos.distanceUBIC == 0){
                            div.find('span.timeUBIC').text('non disponible');
                            div.find('span.distanceUBIC').hide();
                        } else {
                            div.find('span.timeUBIC').text(' - ' + infos.timeUBIC);
                            div.find('span.distanceUBIC').text(infos.distanceUBIC);
                        }
                    },
                    error: function(request, errorType, errorText) {
                        //toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                })
            } else {
                div.find('span.distance').text('Aucune adresse spécifiée!');
            }
        }

        removeEmptyTab = function(div){
            if(typeof(div.find('.tab-pane.remarks .scroller').html()) !== "undefined"){
                if(div.find('.tab-pane.remarks .scroller').html().trim().length == 0){
                    div.find('ul.nav li.remarks').hide();
                }
            }
        }

        addMapMarker = function(div, map){
            var latitude = div.find('input.latitude').val();
            var longitude = div.find('input.longitude').val();
            var title = div.find('strong.title').text();
            var id = div.data('place-id');
            var info = $('.infowindow[rel="'+id+'"]').html();
            
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                map: map,
                title: title
            });

            bounds.extend(marker.position);
            google.maps.event.addListener(marker, 'click', (function (marker, id) {
                return function () {
                    infowindow.setContent(info);
                    infowindow.open(map, marker);
                }
            })(marker, id));
        }

        checkOptions = function(div){

            div.find('.options label').removeClass('active');
            div.find('input.option_id').val('');

            var info = div.find('p.text-primary');
            var warning = div.find('p.text-warning');
            info.hide();
            warning.hide();

            var selectedPlace = div.data('place-id');
            var selectedDate = $('button.date.active').data('date');
            var selectedMoment = $('button.moment.active').data('moment-id');
            var selectedEvent = $('select#SearchEventId option:selected').val();

            if(selectedDate && selectedPlace){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: basePath + '/options/getInfos',
                    data: {
                        place_id: selectedPlace,
                        date: selectedDate,
                        moment_id: selectedMoment
                    },
                    success: function(data){
                        if(typeof(data.Moment) == 'undefined'){
                            data.Moment = {};
                            data.Moment.id = 0;
                        }
                        if(selectedMoment == data.Moment.id){
                            div.find('label.'+data.Option.value).addClass('active');
                            div.find('input.option_id').val(data.Option.id);
                            info.show();
                            info.find('.user').text(data.User.name);
                            info.find('.event').text(data.Event.name);
                            info.find('.client').text(data.Client.name);
                            info.find('.moment').text(data.Moment.name);
                            info.find('.start_hour').text(data.Moment.start_hour);
                            info.find('.end_hour').text(data.Moment.end_hour);
                        }
                        else if( data.overlap == 1 && data.empty == 0 && selectedMoment != data.Moment.id ){
                            div.find('label.'+data.Option.value).addClass('active');
                            div.find('input.option_id').val(data.Option.id);
                            warning.show();
                            warning.find('.user').text(data.User.name);
                            warning.find('.event').text(data.Event.name);
                            warning.find('.client').text(data.Client.name);
                            warning.find('.moment').text(data.Moment.name);
                            warning.find('.start_hour').text(data.Moment.start_hour);
                            warning.find('.end_hour').text(data.Moment.end_hour);
                        }
                        else if( data.overlap == 0 && data.empty == 0 && selectedMoment != data.Moment.id ){
                            div.find('label.free').addClass('active');
                            div.find('input.option_id').val(data.Option.id);
                            warning.show();
                            warning.find('.user').text(data.User.name);
                            warning.find('.event').text(data.Event.name);
                            warning.find('.client').text(data.Client.name);
                            warning.find('.moment').text(data.Moment.name);
                            warning.find('.start_hour').text(data.Moment.start_hour);
                            warning.find('.end_hour').text(data.Moment.end_hour);
                        } else if(data.empty == 1){
                            div.find('label.free').addClass('active');
                        }
                        if(data.Option.value == 'not_interested'){
                            var place = div.parents('.col-md-6').detach();
                            place.removeClass('col-md-6').addClass('col-md-4');
                            place.find('.portlet').css('height', 'auto');
                            place.find('.portlet-body').remove();
                            place.find('.portlet-title ul').remove();
                            place.appendTo('#notInterestingPlaces');
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }
                })
            }

        }

        $('body').on('click', '.save-option', function(event){
            event.preventDefault();
            var rel = $(this).attr('rel');
            var div = $('#' + rel);
            var activeInput = div.find('.options label.active input');
            var option = activeInput.data('value');
            var user_id = activeInput.data('user-id');
            var id = div.find('input.option_id').val();
            var place_id = div.data('place-id');
            var remarks = div.find('input.remarks').val();
            var valid_until = div.find('input.date-picker').val();
            var date = $('button.date.active').data('date');
            var moment_id = $('button.moment.active').data('moment-id');
            var event_id = $('select#SearchEventId option:selected').val();

            if(typeof(option) !== 'undefined'){
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: basePath + '/options/save',
                    data: {
                        id: id,
                        option: option,
                        date: date,
                        place_id: place_id,
                        moment_id: moment_id,
                        event_id: event_id,
                        user_id: user_id,
                        remarks: remarks,
                        valid_until: valid_until
                    },
                    success: function(data){
                        if(data.success == 1){
                            $('#' + rel).find('input.option_id').val(data.id);
                            toastr.success('Option has been saved.');
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log([
                            'Error Type: ' + errorType,
                            'Error Text: ' + errorText
                        ].join('\n'));
                    }                    
                })
            } else {
                bootbox.alert("No option has been selected! Please select one.");
            }
        })

    }

    var handleSelectModuleCategory = function(){
        $('body').on('change', '#ModuleModuleCategoryId', function(){
            event.preventDefault();
            var categoryId = $(this).val();
            $('#ModuleModuleSubcategoryId option').remove();

            if(categoryId.length > 0){
                $.ajax({
                    url: basePath + '/modules/getSubcategories',
                    type: 'POST',
                    data: {
                        module_category_id: categoryId
                    },
                    dataType: 'json',
                    success: function(data){
                        //$('#ModuleModuleSubcategoryId').append('<option value="">Select an option</option>');
                        $.each(data, function(i,e){
                            $('#ModuleModuleSubcategoryId').append('<option value="'+i+'">'+e+'</option>');
                        });
                        $('#ModuleModuleSubcategoryId').selectpicker('refresh');
                    }
                })
            } else {
                
            }
        })
    }

    var handleUserModules = function(){

        if($('input.module.followed').length > 0){
            $('input.module.followed')
                .bootstrapSwitch()
                .on('switchChange.bootstrapSwitch', function(event, state){
                    var line = $(this).parents('td');
                    var module_id = $(this).data('module-id');
                    var user_id = $(this).data('user-id');
                    var module_user_id = $(this).parents('td').find('input.module-user-id-followed').val();
                    $.ajax({
                        url: basePath + '/modules/changeStatus/followed',
                        type: 'POST',
                        data: {
                            user_id: user_id,
                            module_id: module_id,
                            status: state,
                            module_user_id: module_user_id
                        },
                        dataType: 'json',
                        success: function(data){
                            line.find('input.module-user-id-followed').val(data.ModuleUserId);
                            toastr.success('Changes have been saved.')
                        }
                    })
                })
                .each(function(i,e){
                    var module_id = $(e).data('module-id');
                    var user_id = $(e).data('user-id');
                    $.ajax({
                        url: basePath + '/modules/getStatus/followed',
                        type: 'POST',
                        data: {
                            user_id: user_id,
                            module_id: module_id
                        },
                        dataType: 'json',
                        success: function(data){
                            if(data.checked === true){
                                $(e).bootstrapSwitch('state', true, true);
                                $(e).parents('td').find('input.module-user-id-followed').val(data.ModuleUserId);
                            }
                        }
                    })
                })
        }

        $('.suitable-modules').each(function(i,e){
            var module_id = $(e).data('module-id');
            var user_id = $(e).data('user-id');
            $.ajax({
                url: basePath + '/modules/getStatus/suitable',
                type: 'POST',
                data: {
                    user_id: user_id,
                    module_id: module_id
                },
                dataType: 'json',
                success: function(data){
                    if(data.checked === true){
                        if(data.value == 'suitable'){
                            $(e).find('.yes input').attr('checked', 'checked').parents('.btn').addClass('active');
                        }
                        if(data.value == 'not_suitable'){
                            $(e).find('.no input').attr('checked', 'checked').parents('.btn').addClass('active');
                        }
                        if(data.value == 'maybe_suitable'){
                            $(e).find('.maybe input').attr('checked', 'checked').parents('.btn').addClass('active');
                        }
                        $(e).find('.module-user-id-suitable').val(data.ModuleUserId);
                    } else {
                        $(e).find('.maybe input').attr('checked', 'checked').parents('.btn').addClass('active');
                    }
                }
            })
        })
        $('.suitable-modules label').click(function(event){
            var user_id = $(this).parents('.suitable-modules').data('user-id');
            var module_id = $(this).parents('.suitable-modules').data('module-id');
            var module_user_id = $(this).parents('.suitable-modules').find('.module-user-id-suitable').val();
            var value = $(this).find('input.module-user-id-suitable-input').val();
            var line = $(this).parents('.suitable-modules');

            //alert(module_user_id);return;

            $.ajax({
                url: basePath + '/modules/changeStatus/' + value,
                type: 'POST',
                data: {
                    user_id: user_id,
                    module_id: module_id,
                    status: true,
                    module_user_id: module_user_id
                },
                dataType: 'json',
                success: function(data){
                    line.find('input.module-user-id-suitable').val(data.ModuleUserId);
                    toastr.success('Changes have been saved.')
                }
            })
        })
    }

    var handleUpdateOption = function(){
        $('body').on('change', '.update-option', function(){
            var selectedOption = $(this).val();
            var optionId = $(this).parents('tr').data('option-id');
            $.ajax({
                url: basePath + '/options/update',
                type: 'POST',
                dataType: 'json',
                data: {
                    value: selectedOption,
                    id: optionId
                },
                success: function(data){
                    if(data.success == 1){
                        toastr.success('The option for this moment has been saved.');
                    } else {
                        toastr.error('Something wrong happened. Please try again.');
                    }
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log([
                        'Error Type: ' + errorType,
                        'Error Text: ' + request.responseText
                    ].join('\n'));
                }
            })
        })
    }

    var initCalendar = function(){
        var h = {};
        h = {
            left: 'title',
            center: '',
            right: 'prev,next,today,month,agendaWeek,agendaDay'
        };
        $('#calendar').fullCalendar('destroy'); // destroy the calendar
        $('#calendar').fullCalendar({
            lang: 'fr',
            header: h,
            firstDay: 1,
            minTime: "06:00:00",
            defaultView: 'agendaWeek',
            eventSources: [
                {
                    url: basePath + '/events/index.json?status=confirmed',
                    textColor: '#fff', 
                    color: '#3598dc'
                },
                {
                    url: basePath + '/events/index.json?status=elaboration',
                    color: '#f3c200',  
                    textColor: '#fff',
                },
                {
                    url: basePath + '/options/index.json?',
                    color: '#E87E04',  
                    textColor: '#fff',
                }
            ]
        })
        $('#tabs').tabs({
            activate: function(event, ui) {
                if(ui.newPanel.attr('id') == 'calendar1'){
                    $('#calendar').fullCalendar('render');
                }                
            }
        });
         $('#calendar').on( 'click', '.fc-event', function(e){
            e.preventDefault();
            window.open( $(this).attr('href'), '_blank' );
        });
    }

    var initPlaceAgenda = function(){
        var h = {};
        h = {
            left: 'title',
            center: '',
            right: 'prev,next,today,month,agendaWeek,agendaDay'
        };
        $('#placeAgenda').fullCalendar('destroy');
        $('#placeAgenda').fullCalendar({
            lang: 'fr',
            header: h,
            firstDay: 1,
            minTime: "06:00:00",
            defaultView: 'agendaWeek',
            eventSources: [
                {
                    url: basePath + '/places/agenda.json?type=options',
                    color: '#26a69a',
                    textColor: '#fff',
                    backgroundColor: Metronic.getBrandColor('green-jungle')
                }
            ]
        })
        $('#tabs').tabs({
            activate: function(event, ui) {
                if(ui.newPanel.attr('id') == 'agenda'){
                    $('#placeAgenda').fullCalendar('render');
                }                
            }
        });
    }

    var handleDepotPlan = function(){
        $('body').on('click', 'table.depot td', function(){
            
            var cell = $(this);
            var class1 = cell.attr('class');
            cell.removeAttr('class');
            
            toastr.clear();
            
            if(class1 == 'bg-red'){
                cell.addClass('bg-green');
                saveCell(cell, 2);
            }
            else if(class1 == 'bg-green'){
                cell.addClass('bg-grey');
                saveCell(cell, 3);
            }
            else if(class1 == 'bg-grey'){
                cell.addClass('bg-yellow');
                saveCell(cell, 4);
            }
            else if(class1 == 'bg-yellow'){
                saveCell(cell, 0);
            } else if(typeof class1 == 'undefined') {
                cell.addClass('bg-red');
                saveCell(cell, 1);
            }
        });

        $('body').on('click', 'button.initWarehouse', function(){
            var $this = $(this);
            $('#loading').fadeIn();
            $.ajax({
                url: basePath + '/warehouses/init/' + $this.data('warehouse-id'),
                type: 'post',
                dataType: 'json',
                success: function(data){
                    $('#loading').fadeOut();
                    if(data.success == 1){
                        toastr.success('Les informations ont été générées et enregistrées.')
                    } else {
                        toastr.error('Une erreur s\'est produite. Merci de réessayer.');
                    }
                }
            })
        })

        //getNodes($('table.depot').data('warehouse-id'));

        function getNodes(warehouse_id){
            $('table.depot td').each(function(i,e){
                var cell = $(e);
                var x = cell.data('x');
                var y = cell.data('y');
                $.ajax({
                    url: basePath + '/warehouses/getNodes/' + warehouse_id,
                    type: 'post',
                    dataType: 'json'
                })
            })
        }
        
        function saveCell(cell, flag){
            
            var x = cell.data('x');
            var y = cell.data('y');
            var cell_id = cell.data('id');
            
            $.ajax({
                url: basePath + '/warehouses/saveCell',
                type: 'post',
                dataType: 'json',
                data:{
                    x: x,
                    y: y,
                    flag: flag,
                    cell_id: cell_id
                },
                success: function(data){
                    cell.data('id', data.id);
                    toastr.success('Cell has been saved.');
                }
            })
        }
    }

    var initSendMessage = function(){
        $("#sendRecipients").select2({
            placeholder: "Search for a user",
            minimumInputLength: 2,
            tags: true,
            ajax: {
                url: basePath + '/users/json',
                dataType: 'json',
                type: 'POST',
                data: function(term, page) {
                    return {
                        term: term
                    };
                },
                results: function(data, page) {
                    return {
                        results: data
                    };       
                }
            }

        });
        $('input.make-switch').on('switchChange.bootstrapSwitch', function(event, state) {
            if(state){
                $('.body.mail').parents('.input').removeClass('hidden');
                $('.body.sms').addClass('hidden');
            } else {
                $('.body.mail').parents('.input').addClass('hidden');
                $('.body.sms').removeClass('hidden');
            }
        });

        CKEDITOR.replace( 'sendMailBody', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["list"]}
            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        } );
    }

    var handleStockOrderMoments = function(){
        $('body').on('change', '#StockOrderDeliveryMoment', function(){
            var moment = $(this).val();
            if(moment == 'hour'){
                $('#StockOrderDeliveryMomentHour').removeClass('hidden');
                $('.delivery_moment_hour_invoice').removeClass('hidden');
            } else {
                $('#StockOrderDeliveryMomentHour').addClass('hidden');
                $('.delivery_moment_hour_invoice').addClass('hidden');
            }
        })
        $('body').on('change', '#StockOrderReturnMoment', function(){
            var moment = $(this).val();
            if(moment == 'hour'){
                $('#StockOrderReturnMomentHour').removeClass('hidden');
                $('.return_moment_hour_invoice').removeClass('hidden');
            } else {
                $('#StockOrderReturnMomentHour').addClass('hidden');
                $('.return_moment_hour_invoice').addClass('hidden');
            }
        })
    }

    var handleStockOrderDistanceCovered = function(){
        $('body').on('change', '.computeDistanceCovered', function(){
            computeDistanceCovered();
        })

        $('body').on('change', '#StockOrderDeliveryDistance, #StockOrderReturnDistance', function(){
            updateTotalDistanceCovered();
        })

        $("#StockOrderDeliveryZipCity, #StockOrderReturnZipCity").on('select2-selecting', function(val, choice){
            computeDistanceCovered();
        })

        $('input#StockOrderDelivery, input#StockOrderReturn').on('switchChange.bootstrapSwitch', function(event, state) {
            computeDistanceCovered();
        });

        $('body').on('click', '.searchGooglePlaces', function(event){
            event.preventDefault();
        })

        $(".place").select2({
            tags: true,
            milliseconds: 400,
            minimumInputLength: 1,
            maximumSelectionSize: 1,
            ajax: {
                url: basePath + '/stock_orders/getPlaces',
                dataType: 'json',
                type: 'post',
                delay: 800,
                data: function (term) {
                  return {
                    q: term
                  };
                },
                results: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.text,
                                id: item.text,
                                address: item.address,
                                zip: item.zip,
                                city: item.city,
                                zip_city: item.zip_city
                            }
                        })
                    };
                    
                }
            },
            //Allow manually entered text in drop down.
            createSearchChoice:function(term, results) {
                if ($(results).filter( function() {
                    return term.localeCompare(this.text)===0; 
                }).length===0) {
                    return {id:term, text: term, address: '', zip: '', city: ''};
                }
            },
            initSelection: function(element, callback){
                var data = {text: element.val()};
                if(element.val().length < 1){data.text = ''}
                callback(data);
            }
        }).on("select2-selecting", function(e) {
            var rel = $(this).attr('rel');
            console.log(rel);
            $('.place[rel="'+rel+'"]').val(e.val);
            $('.address[rel="'+rel+'"]').val(e.choice.address);
            $('.zip[rel="'+rel+'"]').val(e.choice.zip);
            $('.city[rel="'+rel+'"]').val(e.choice.city);
            $('.zip_city[rel="'+rel+'"]').select2('data', {text: e.choice.zip_city});
            computeDistanceCovered();
        }).on("select2-removed", function(e) {
            var rel = $(this).attr('rel');
            $('.place[rel="'+rel+'"]').val('');
            $('.address[rel="'+rel+'"]').val('');
            $('.zip[rel="'+rel+'"]').val('');
            $('.city[rel="'+rel+'"]').val('');
            $('.zip_city[rel="'+rel+'"]').select2('val', '');
        });

        updateTotalDistanceCovered = function(){
            if( $('#StockOrderDeliveryDistance').val().length !== 0){
                var distance1 = parseFloat($('#StockOrderDeliveryDistance').val());
            } else {
                distance1 = 0;
            }
            if( $('#StockOrderReturnDistance').val().length !== 0){
                var distance2 = parseFloat($('#StockOrderReturnDistance').val());
            } else {
                distance2 = 0;
            }
            var totalDistance = distance1 + distance2;
            $('#StockOrderDistanceCovered').val(totalDistance).trigger('change');
        }

        computeDistanceCovered = function(){

            var deliveryMode = $('#StockOrderDelivery').is(':checked');
            var returnMode = $('#StockOrderReturn').is(':checked');
           
            var deliveryAddress = $('#StockOrderDeliveryAddress').val();
            var deliveryZip = $('#StockOrderDeliveryZip').val();
            var deliveryCity = $('#StockOrderDeliveryCity').val();
           
            var returnAddress = $('#StockOrderReturnAddress').val();
            var returnZip = $('#StockOrderReturnZip').val();
            var returnCity = $('#StockOrderReturnCity').val();

            var distance = 0;

            if(deliveryMode){
                $('#StockOrderDeliveryDistance').addClass('spinner');
                $('.delivery_end_address').addClass('hidden');
                if(deliveryCity && deliveryZip){
                    $.ajax({
                        url: basePath + '/stock_orders/computeDistance',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            toAddress: deliveryAddress,
                            toZip: deliveryZip,
                            toCity: deliveryCity
                        },
                        success: function(data){
                            $('#StockOrderDeliveryDistance').val( data.distance * 2 ).trigger('change');
                            $('#StockOrderDeliveryDistance').removeClass('spinner');
                            $('.delivery_end_address').removeClass('hidden').find('strong').text(data.end_address);
                        },
                        error: function(request, errorType, errorText) {
                            toastr.error('Something wrong happened. Please try again.');
                            console.log(request.responseText);
                        }
                    })
                }
            }

            if(returnMode){
                $('#StockOrderReturnDistance').addClass('spinner');
                $('.return_end_address').addClass('hidden');
                if(returnCity && returnZip){
                    $.ajax({
                        url: basePath + '/stock_orders/computeDistance',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            toAddress: returnAddress,
                            toZip: returnZip,
                            toCity: returnCity
                        },
                        success: function(data){
                            $('#StockOrderReturnDistance').val( data.distance * 2 ).trigger('change');
                            $('#StockOrderReturnDistance').removeClass('spinner');
                            $('.return_end_address').removeClass('hidden').find('strong').text(data.end_address);
                        },
                        error: function(request, errorType, errorText) {
                            toastr.error('Something wrong happened. Please try again.');
                            console.log(request.responseText);
                        }
                    })
                }
            }
        }

        //computeDistanceCovered();

    }

    var handleSelectStockOrderPlace = function(){
        $("#StockOrderDeliveryPlaceId, #StockOrderReturnPlaceId").on('select2-selecting', function(e, choice){

            var placeId = e.val;
            var inputId = $(e.target).attr('id');

            $.ajax({
                url: basePath + '/places/get',
                type: 'post',
                dataType: 'json',
                data: {
                    id: placeId
                },
                success: function(data){
                    if(inputId == 'StockOrderDeliveryPlaceId'){
                        $('#StockOrderDeliveryAddress').val(data.Place.address).trigger('change');
                        $('#StockOrderDeliveryZip').val(data.Place.zip).trigger('change');
                        $('#StockOrderDeliveryCity').val(data.Place.city).trigger('change');
                        $('#StockOrderDeliveryZipCity').addClass('hidden');
                        $('#StockOrderDeliveryZipCity1').removeClass('hidden').val(data.Place.zip_city);
                    }
                    if(inputId == 'StockOrderReturnPlaceId'){
                        $('#StockOrderReturnAddress').val(data.Place.address).trigger('change');
                        $('#StockOrderReturnZip').val(data.Place.zip).trigger('change');
                        $('#StockOrderReturnCity').val(data.Place.city).trigger('change');
                        $('#StockOrderReturnZipCity').addClass('hidden');
                        $('#StockOrderReturnZipCity1').removeClass('hidden').val(data.Place.zip_city);
                    }

                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log(request.responseText);
                }
            })
        })

        $("#StockOrderDeliveryPlaceId, #StockOrderReturnPlaceId").on('select2-removed', function(e, choice){
            $('#StockOrderDeliveryZipCity').removeClass('hidden');
            $('#StockOrderDeliveryZipCity1').addClass('hidden').val('');
            $('#StockOrderDeliveryAddress').val('');
            $('#StockOrderDeliveryDistance').val('');
        })
    }

    var handleComputeTotal = function(){

        $('body').on('change', '.computeTotal', function(){
            computeTotal();
        });
        $('input#StockOrderFidelityDiscount, input#StockOrderDelivery, input#StockOrderReturn').on('switchChange.bootstrapSwitch', function(event, state) {
            var id = $(this).attr('id');
            if(id == 'StockOrderDelivery'){
                $('input.delivery_mode').removeAttr('checked').parents('span').removeClass('checked');
                $('.delivery_mode').each(function(i,e){
                    if(state){
                        if($(e).val() == 'festiloc'){
                            $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
                        }
                    } else {
                        if($(e).val() == 'client'){
                            $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
                        }
                    }
                    
                })   
            }
            if(id == 'StockOrderReturn'){
                $('input.return_mode').removeAttr('checked').parents('span').removeClass('checked');
                $('.return_mode').each(function(i,e){
                    if(state){
                        if($(e).val() == 'festiloc'){
                            $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
                        }
                    } else {
                        if($(e).val() == 'client'){
                            $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
                        }
                    }
                    
                })   
            }
            if(id == 'StockOrderFidelityDiscount'){
                if(state){
                    $('input#StockOrderFidelityDiscount').attr('checked', 'checked').val(1)
                } else {
                    $('input#StockOrderFidelityDiscount').attr('checked', '').val(0)
                }
            }
                     
            computeTotal();
        });
        $('body').on('change', 'input.discount', function(){
            var line = $(this).parents('tr');
            var discount = $(this).val();
            updateUnitPrice(discount, line);
        })

        var updateUnitPrice = function(discount, line){
            var unitPriceWithDiscount = parseFloat(line.find('.price').text()) * (100-discount) / 100;
            line.find('td.priceWithDisount').text(unitPriceWithDiscount.toFixed(2));
        }


        if($('#stockitemsTable').length > 0){
            $('#stockitemsTable tbody tr').each(function(i,e){
                var discount = $(e).find('input.discount').val();
                updateUnitPrice(discount, $(e));
            })
        }

        var computeTotal = function(){
            var numberOfPaletts = $('#StockOrderNumberOfPallets').val().length > 0 ? parseInt($('#StockOrderNumberOfPallets').val()) : 0;
            var numberOfRollis = $('#StockOrderNumberOfRollis').val().length > 0 ? parseInt($('#StockOrderNumberOfRollis').val()) : 0;
            var xlSurcharge = $('#StockOrderXlSurcharge').val().length > 0 ? parseInt($('#StockOrderXlSurcharge').val()) : 0;
            var distanceCovered = $('#StockOrderDistanceCovered').val().length > 0 ? parseInt($('#StockOrderDistanceCovered').val()) : 0;
            var deliveryMoment = $('#StockOrderDeliveryMoment').val();
            var deliveryMode = $('#StockOrderDeliveryMode:checked').val();
            var returnMode = $('#StockOrderReturnMode:checked').val();
            var deliveryMomentInvoice = $('#StockOrderDeliveryMomentHourInvoice').is(':checked');
            var returnMoment = $('#StockOrderReturnMoment').val();
            var returnMomentInvoice = $('#StockOrderReturnMomentHourInvoice').is(':checked');
            var stockOrderTotal = $('#StockOrderTotalHt').val();
            var fidelityDiscountChecked = $('#StockOrderFidelityDiscount').is(':checked');
            var fidelityDiscountPercentage = $('#StockOrderFidelityDiscountPercentage').val().length ? parseInt($('#StockOrderFidelityDiscountPercentage').val()) : 3;
            var withDelivery = $('#StockOrderDelivery').is(':checked');
            var withReturn = $('#StockOrderReturn').is(':checked');
            var deliveryDistanceCovered = $('#StockOrderDeliveryDistance').val().length > 0 ? parseFloat($('#StockOrderDeliveryDistance').val()) : 0;
            var returnDistanceCovered = $('#StockOrderReturnDistance').val().length > 0 ? parseFloat($('#StockOrderReturnDistance').val()) : 0;
            var deliveryCosts = 0;

            // compute number of trips and packaging costs
            var palettPrice = 75;
            var rolliPrice = 70;

            if(numberOfPaletts > 1 || (numberOfPaletts >= 1 && numberOfRollis >= 1)){
                palettPrice = 60;
            }
            if(numberOfRollis > 1 || (numberOfPaletts >= 1 && numberOfRollis >= 1)){
                rolliPrice = 50;
            }
            var palettsCosts = 0;
            var rollisCosts = 0;
            var packagingCosts = 0;
            var deliveryPackagingCosts = 0;
            var returnPackagingCosts = 0;

            deliveryPackagingCosts = (palettPrice * numberOfPaletts) + (rolliPrice * numberOfRollis);
            returnPackagingCosts = (palettPrice * numberOfPaletts) + (rolliPrice * numberOfRollis);
            packagingCosts = deliveryPackagingCosts + returnPackagingCosts + (xlSurcharge * 35);

            $('.packaging_costs span').html(parseFloat(packagingCosts).toFixed(2));
            $('.delivery_packaging_costs span').html(parseFloat(deliveryPackagingCosts).toFixed(2));
            $('.return_packaging_costs span').html(parseFloat(returnPackagingCosts).toFixed(2));

            // compute transportation costs
            var deliveryTransportationCosts = 0;
            if( (numberOfPaletts + numberOfRollis) <= 6){
                var deliveryTransportationCosts = deliveryDistanceCovered * 1.5;
            } else if((numberOfPaletts + numberOfRollis) > 6 && (numberOfPaletts + numberOfRollis) <= 12){
                var deliveryTransportationCosts = deliveryDistanceCovered * 2;                
            } else if( (numberOfPaletts + numberOfRollis) > 12 ){
                var deliveryTransportationCosts = deliveryDistanceCovered * 2.5;                
            }

            var returnTransportationCosts = 0;
            if((numberOfPaletts + numberOfRollis) <= 6){
                var returnTransportationCosts = returnDistanceCovered * 1.5;
            } else if((numberOfPaletts + numberOfRollis) > 6 && (numberOfPaletts + numberOfRollis) <= 12){
                var returnTransportationCosts = returnDistanceCovered * 2;                
            } else if( (numberOfPaletts + numberOfRollis) > 12 ){
                var returnTransportationCosts = returnDistanceCovered * 2.5;                
            }
            
            if(deliveryMoment == 'hour' && deliveryMomentInvoice){
                deliveryTransportationCosts = deliveryTransportationCosts + 50;
            }
            if(returnMoment == 'hour' && returnMomentInvoice){
                returnTransportationCosts = returnTransportationCosts + 50;
            }
            var deliveryTransportationCosts1 = deliveryTransportationCosts;
            var returnTransportationCosts1 = returnTransportationCosts;
            var transportationCostsInfo = deliveryTransportationCosts1 + returnTransportationCosts1;
            if(deliveryTransportationCosts < 50){
                deliveryTransportationCosts = 50;
            }
            if(returnTransportationCosts < 50){
                returnTransportationCosts = 50;
            }
            if(transportationCostsInfo < 100){
                transportationCostsInfo = 100;
            }
            
            $('.delivery_transportation_costs span').html(parseFloat(deliveryTransportationCosts).toFixed(2));
            $('.return_transportation_costs span').html(parseFloat(returnTransportationCosts).toFixed(2));
            $('.transportation_costs_info span').html(parseFloat(transportationCostsInfo).toFixed(2));
            
            var transportationCosts = 0;
            if(deliveryMode == 'festiloc' && returnMode == 'festiloc'){
                transportationCosts = deliveryTransportationCosts + returnTransportationCosts;
            } else if(deliveryMode == 'festiloc' && returnMode != 'festiloc'){
                transportationCosts = deliveryTransportationCosts;
            } else if(deliveryMode != 'festiloc' && returnMode == 'festiloc'){
                transportationCosts = returnTransportationCosts;
            }
            // if(transportationCosts < 100){
            //     transportationCosts = 100;
            // }
            if(deliveryMode != 'festiloc' && returnMode != 'festiloc'){
                transportationCosts = 0;
            }

            $('#StockOrderTransportationCosts').val(parseFloat(transportationCosts).toFixed(2));
            $('#StockOrderDeliveryTransportationCosts').val(parseFloat(deliveryTransportationCosts).toFixed(2));
            $('#StockOrderReturnTransportationCosts').val(parseFloat(returnTransportationCosts).toFixed(2));

            $('#StockOrderPackagingCosts').val(parseFloat(packagingCosts).toFixed(2));
            $('#StockOrderDeliveryPackagingCosts').val(parseFloat(deliveryPackagingCosts).toFixed(2));
            $('#StockOrderReturnPackagingCosts').val(parseFloat(deliveryPackagingCosts).toFixed(2));

            if(deliveryMode == 'client' && returnMode == 'client'){
                deliveryCosts = 0;
            }
            if(deliveryMode == 'client' && returnMode == 'festiloc'){
                deliveryCosts = returnTransportationCosts;
            }
            if(deliveryMode == 'client' && returnMode == 'transporter'){
                deliveryCosts = returnPackagingCosts;
            }
            if(deliveryMode == 'festiloc' && returnMode == 'client'){
                deliveryCosts = deliveryTransportationCosts;
            }
            if(deliveryMode == 'festiloc' && returnMode == 'festiloc'){
                deliveryCosts = transportationCosts;
            }
            if(deliveryMode == 'festiloc' && returnMode == 'transporter'){
                deliveryCosts = deliveryTransportationCosts + returnPackagingCosts;
            }
            if(deliveryMode == 'transporter' && returnMode == 'client'){
                deliveryCosts = deliveryPackagingCosts;
            }
            if(deliveryMode == 'transporter' && returnMode == 'festiloc'){
                deliveryCosts = deliveryPackagingCosts + returnTransportationCosts;
            }
            if(deliveryMode == 'transporter' && returnMode == 'transporter'){
                deliveryCosts = packagingCosts;
            }
            var deliveryCosts1 = (Math.ceil( deliveryCosts * 20 - 0.5)/20).toFixed(2);
            $('.delivery_costs span').html(parseFloat(deliveryCosts1).toFixed(2));
            $('#StockOrderDeliveryCosts').val(parseFloat(deliveryCosts1).toFixed(2));

            //var costsDifference = Math.abs(transportationCosts - packagingCosts);
            //$('.costs_difference span').html(parseFloat(costsDifference).toFixed(2));

            // compute discount according to total HT
            var discountPercentage = $('#StockOrderQuantityDiscountPercentage').val().length > 0 ? parseFloat($('#StockOrderQuantityDiscountPercentage').val()) : Math.floor(stockOrderTotal / 2000) * 2;
            var discount = stockOrderTotal * discountPercentage / 100;
            $('#StockOrderQuantityDiscount').val(parseFloat(discount).toFixed(2));
            $('.quantity_discount span:first').html(parseFloat(discount).toFixed(2));
            $('span.discount em').text(discountPercentage);
            $('#StockOrderQuantityDiscountPercentage').val(discountPercentage);
            $('.quantity_discount em').html(discountPercentage + '%');
            var stockOrderTotalWithDiscount = parseFloat(stockOrderTotal - discount).toFixed(2);

            // compute fidelity discount
            var fidelityDiscount = 0.00;
            if(fidelityDiscountChecked){
                fidelityDiscount = parseFloat(stockOrderTotal * fidelityDiscountPercentage / 100).toFixed(2);
            }
            stockOrderTotalWithDiscount = stockOrderTotalWithDiscount - fidelityDiscount;
            $('#StockOrderFidelityDiscountAmount').val(fidelityDiscount);
            $('#StockOrderFidelityDiscountPercentage').val(fidelityDiscountPercentage);
            $('.fidelity_discount_amount span').html(fidelityDiscount);

            // compute tva
            var intermediateTotal = stockOrderTotalWithDiscount + deliveryCosts;

            var stockOrderTva = (Math.ceil(( (intermediateTotal)*0.08)*20 - 0.5)/20).toFixed(2);
            $('#StockOrderTva').val(stockOrderTva);
            $('.tva span').html(stockOrderTva);

            var stockOrderNetTotal = (parseFloat(intermediateTotal) + parseFloat(stockOrderTva)).toFixed(2);
            $('.net_total span').html(stockOrderNetTotal);
            $('#StockOrderNetTotal').val(stockOrderNetTotal);
        }

        computeTotal();

    }



    var handleFestilocLive = function(){

        var refreshStockOrders = function(){

            $('.modal').modal('hide');

            var from = $('#from').val();
            var to = $('#to').val();
            var statuses = $('#status').val();
            var dateType = $('#dateType').val();

            $('#loading').fadeIn();
            $.ajax({
                url: basePath + '/stock_orders/live_list',
                type: 'post',
                dataType: 'html',
                data: {
                    fromDate: from,
                    toDate: to,
                    statuses: statuses,
                    dateType: dateType
                },
                success: function(data){
                    $('#stockorders').html(data);
                    $('#stockorders .portlet').each(function(i,e){
                        var label = $(e).find('span.label');
                        var status = label.data('status');
                        var select = $(e).find('.bs-select');
                        select.find('option[value="'+status+'"]').attr('selected', 'selected');
                        if(status == 'confirmed'){
                            select.attr('data-style', 'green');
                        }
                        if(status == 'in_progress'){
                            select.attr('data-style', 'yellow-crusta');
                        }
                        if(status == 'processed' || status == 'delivered'){
                            select.attr('data-style', 'grey-gallery');
                        }
                    })
                    handleBootstrapSelect();
                    $('.row.title h3').show().find('span').text($('#stockorders .portlet').get().length);
                    $('#loading').fadeOut();
                    $('.pulsate').pulsate({
                        color: "#D91E18"
                    });
                    $('body').on('click', 'button.confirmModifications', function(event){
                        var modalId = $(this).parents('.modal').attr('id');
                        var stockOrderId = $(this).data('stock-order-id');
                        $.ajax({
                            url: basePath + '/stock_orders/readModifications',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                stock_order_id:  stockOrderId
                            },
                            success: function(data){
                                if(data.success){
                                    $('#' + modalId).modal('toggle');
                                    refreshStockOrders();
                                }
                            },
                            error: function(request, errorType, errorText) {
                                toastr.error('Something wrong happened. Please try again.');
                                console.log(request.responseText);
                            }
                        })                        
                    })
                }
            })
        }

        //init first refresh
        if($('.refresh').length > 0){
            refreshStockOrders();
        }

        $('body').on('click', '.refresh', function(event){
            event.preventDefault();
            refreshStockOrders();  
        })

        $('body').on('change', '.refreshStockOrders', function(){
            refreshStockOrders();
        })

        $('body').on('click', '.clear', function(event){
            event.preventDefault();
            $('.refreshStockOrders').val('');
            $('select.refreshStockOrders option').prop("selected", false).trigger('change');
            refreshStockOrders();
        })

        $('body').on('click', '.delivery_note', function(event){
            var stock_order_id = $(this).parents('.portlet').data('stock-order-id');
            setStatus(stock_order_id, 'in_progress');
        })

        $('body').on('change', '.bs-select', function(){
            var stock_order_id = $(this).parents('.portlet').data('stock-order-id');
            setStatus(stock_order_id, $(this).val());
        })

        $('body').on('change', 'input[name="type"]', function(){
            var type = $('input[name="type"]:checked').val();
            var fromDate = $('#from').data('default');
            var toDate = $('#to').data('default');

            $('#from').datepicker('update', fromDate);
            $('#to').datepicker('update', toDate);

            $('#status option').removeAttr('selected');

            if(type == 'delivery'){
                $('#status option[value="confirmed"]').attr('selected', 'selected');
                $('#status option[value="in_progress"]').attr('selected', 'selected');
                $('#dateType').val('delivery');
            }
            if(type == 'return'){
                $('#status option[value="delivered"]').attr('selected', 'selected');
                $('#status option[value="processed"]').attr('selected', 'selected');
                $('#dateType').val('return');
            }

            $('#status').trigger('change').selectpicker('render');
        })

        var setStatus = function(id, status){
            $.ajax({
                url: basePath + '/stock_orders/setStatus',
                type: 'post',
                dataType: 'json',
                data:{
                    status: status,
                    stock_order_id: id
                },
                success: function(data){
                    if(data.success){
                        refreshStockOrders();
                    }
                }
            })
        }

        setInterval(function(){
            if($('.refresh').length > 0){
                if($('.modal.in').length < 1){
                    refreshStockOrders();
                }                
            }
        }, 300000);

        $('body').on('click', 'a.add-line', function(event){
            event.preventDefault();
            var clone = $('#extra tr.empty').clone();
            clone.find('input').val('');
            clone.appendTo('#extra tbody');
        })

        $('body').on('click', '.save-invoice', function(event){
            event.preventDefault();
            bootbox.confirm("Êtes-vous sûr d'avoir bien contrôlé toute la marchandise? La commande va être mise en statut \"à facturer\" et ne sera plus visible.", function(result){
                if(result){
                    $('form').submit();
                } else {
                    return false;
                }
            });
        })

        $('body').on('click', '.save-issue', function(event){
            event.preventDefault();
            bootbox.confirm("Êtes-vous sûr d'avoir bien préparé toute la marchandise?", function(result){
                if(result){
                    $('form').submit();
                } else {
                    return false;
                }
            });
        })

        var checkAvailability = function(input){
            var line = input.parents('tr');
            var id = line.attr('id').replace('#', '');
            var reprocessingDuration = line.find('.reprocessing_duration').val();
            var returnedQuantity = line.find('.returned_quantity').val();
            var stockItemId = line.find('.stockItemId').val();
            input.addClass('spinner');
            line.removeClass().addClass('batch');
            $('table#batches tbody tr:not(.batch)').addClass('hidden');
            $('table#batches tbody tr:not(.batch) ul').empty();
            $('table#batches tbody tr:not(.batch) .infoLine').addClass('hidden');
            $.ajax({
                url: basePath + '/stock_items/checkAvailability',
                type: 'post',
                dataType: 'json',
                data: {
                    duration: reprocessingDuration,
                    stock_item_id: stockItemId,
                    amount: returnedQuantity
                },
                success: function(data){
                    input.removeClass('spinner');
                    var infoLine = $('tr[rel="'+id+'"]');
                    if(data.conflicts.dates.length > 0 && data.conflicts.orders.length < 1){
                        infoLine.removeClass('hidden');
                        line.addClass('warning');
                        infoLine.find('.text-warning').removeClass('hidden');
                        infoLine.find('span.start-date').text(data.conflicts.dates[0]);
                        infoLine.find('span.end-date').text(data.conflicts.dates[data.conflicts.dates.length - 1]);
                    }
                    if(data.conflicts.orders.length > 0){
                        infoLine.removeClass('hidden');
                        line.addClass('danger');
                        infoLine.find('.text-danger').removeClass('hidden');
                        var item = '<li>'+data.conflicts.orders[0].date + ' ' + data.conflicts.orders[0].name + ' : ' + data.conflicts.orders[0].needed + ' pièces manquantes</li>';
                        infoLine.find('ul').html(item);
                    }
                    
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log(request.responseText);
                }
            })
        }

        $('body').on('change', 'input.checkAvailability', function(){
            checkAvailability($(this));
        })

        if($('table#batches').length > 0){
            $('table#batches tbody tr.batch').each(function(i,e){
                var input = $(e).find('.checkAvailability');
                checkAvailability(input);
            })
        }
    }

    var handleWeekPreparation = function(){
        if($('#calendar').length > 0){
            var h = {};
            h = {
                left: 'title',
                center: '',
                right: 'prev,next,today,month,basicWeek'
            };
            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({
                lang: 'fr',
                header: h,
                firstDay: 1,
                minTime: "06:00:00",
                defaultView: 'basicWeek',
                editable: true,
                droppable: true,
                eventSources: [
                    {
                        url: basePath + '/stock_orders/prepareWeek.json',
                        type: 'POST',
                        data: {
                            delivery_mode: 'festiloc'
                        },
                        error: function() {
                            alert('there was an error while fetching events!');
                        }
                    }
                ],
                eventRender: function(event, element) {
                    element.attr('data-toggle', 'context');
                    element.attr('data-target', '#context');
                    element.css('background-color', event.bgColor);
                    element.html(event.description);
                    element.on('contextmenu', function(){
                        $('#context').find('li a').attr('data-stock-order-id', event.stock_order_id);
                        $('#context').find('li a').on('click', function(e){
                            var $this = $(this);
                            if(event.mode == 'delivery'){
                                var field = 'delivery_transporter_id';
                                var data = {id: event.stock_order_id, delivery_transporter_id: $this.attr('data-transporter-id')}
                            }
                            if(event.mode == 'return'){
                                var field = 'return_transporter_id';
                                var data = {id: event.stock_order_id, return_transporter_id: $this.attr('data-transporter-id')}
                            }
                            $.ajax({
                                url: basePath + '/stock_orders/update/' + field,
                                type: 'POST',
                                dataType: 'json',
                                data: data,
                                success: function(data){
                                    if(data.success == 1){
                                        toastr.success('La commande a été correctement modifiée.');
                                    } else {
                                        toastr.error('La commande n\'a pas été modifiée.');
                                    }
                                    $('#calendar').fullCalendar('refetchEvents');
                                },
                                error: function(request, errorType, errorText) {
                                    toastr.error('Something wrong happened. Please try again.');
                                    console.log(request.responseText);
                                }
                            })
                            $('#context').find('li a').unbind('click');
                        })
                    })
                    $('#context').on('hide.bs.context', function (e) {
                        $('#context').find('li a').unbind('click');
                    });
                },
                eventDrop: function(event, delta, revertFunc) {
                    bootbox.confirm("Êtes-vous sûr de vouloir modifier la date de livraison/retour de la commande?", function(confirm){
                        if(confirm){
                            if(event.mode == 'delivery'){
                            var field = 'delivery_date';
                            var data = {id: event.stock_order_id, delivery_date: event.start.format()}
                        }
                        if(event.mode == 'return'){
                            var field = 'return_date';
                            var data = {id: event.stock_order_id, return_date: event.start.format()}
                        }
                        $.ajax({
                            url: basePath + '/stock_orders/update/' + field,
                            type: 'POST',
                            dataType: 'json',
                            data: data,
                            success: function(data){
                                if(data.success == 1){
                                    toastr.success('La commande a été correctement modifiée.');
                                } else {
                                    toastr.error('La commande n\'a pas été modifiée.');
                                }
                            },
                            error: function(request, errorType, errorText) {
                                toastr.error('Something wrong happened. Please try again.');
                                console.log(request.responseText);
                            }
                        })
                        } else {
                            $('#calendar').fullCalendar('refetchEvents');
                        }
                    })                    
                }
            })
            $('#tabs').tabs({
                activate: function(event, ui) {
                    if(ui.newPanel.attr('id') == 'calendar1'){
                        $('#calendar').fullCalendar('render');
                    }                
                }
            });
            $('body').on('click', '.notifyTransporters', function(event){
                event.preventDefault();
                $('#loading').fadeIn();
                var start = $('#calendar').fullCalendar('getDate').startOf('week').format();
                $.ajax({
                    url: basePath + '/stock_orders/notify_transporters',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        date: start
                    },
                    success: function(data){
                        $('#loading').fadeOut();
                        $('#calendar').fullCalendar('refetchEvents');
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log(request.responseText);
                    }
                })
            })
        }
        

        return;

        if($('#gmap_places1').length){
            window.map1 = new google.maps.Map(document.getElementById('gmap_places1'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }
        if($('#gmap_places2').length){
            window.map2 = new google.maps.Map(document.getElementById('gmap_places2'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
        }

        var initMap = function(map1, map2){

            if(typeof(map1) == 'undefined' && typeof(map2) == 'undefined'){
                return;
            }
            
            var geocoder = new google.maps.Geocoder();

            var originLat = $('.origin').data('lat');
            var originLng = $('.origin').data('lng');
            var bounds1 = new google.maps.LatLngBounds();
            var bounds2 = new google.maps.LatLngBounds();

            var origin1 = new google.maps.Marker({
                position: new google.maps.LatLng(originLat, originLng),
                map: map1,
                icon: basePath + '/img/icons/marker-origin.png',
                zIndex: 0,
                title: 'Festiloc'
            });

            var origin2 = new google.maps.Marker({
                position: new google.maps.LatLng(originLat, originLng),
                map: map2,
                icon: basePath + '/img/icons/marker-origin.png',
                zIndex: 0,
                title: 'Festiloc'
            });
            var latlng = [];
            bounds1.extend(origin1.position);
            bounds2.extend(origin2.position);

            var listener = google.maps.event.addListener(map1, "idle", function() { 
                google.maps.event.removeListener(listener);
                
                var infowindow = new google.maps.InfoWindow();
                $('table.deliveries tbody tr.stockorder').each(function(i,e){

                    var address = $(e).find('input.address').val();
                    var deliveryMode = $(e).find('input.delivery_mode').val();
                    var temp = [];
                    var line = $(e);
                    var infowindowContent = line.find('.infowindow').html();

                    geocoder.geocode( {'address': address}, function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                        temp = new google.maps.Marker({
                            map: map1,
                            position: results[0].geometry.location,
                            icon: basePath + '/img/icons/marker-'+deliveryMode+'.png',
                        });
                        latlng.push(temp.position);
                        bounds1.extend(temp.position);
                        google.maps.event.addListener(temp, 'click', function() {
                            infowindow.close();
                            infowindow.setContent(infowindowContent);
                            infowindow.open(map1, temp);
                        });
                      } else {
                        //console.log('Geocode was not successful for the following reason: ' + status);
                      }
                    });
                    
                })
                map1.setZoom(9);
                map1.setCenter(bounds1.getCenter());
            });
            var listener2 = google.maps.event.addListener(map2, "idle", function() { 
                google.maps.event.removeListener(listener2);
                
                var infowindow = new google.maps.InfoWindow();
                $('table.returns tbody tr.stockorder').each(function(i,e){

                    var address = $(e).find('input.address').val();
                    var returnMode = $(e).find('input.return_mode').val();
                    var temp = [];
                    var line = $(e);
                    var infowindowContent = line.find('.infowindow').html();

                    geocoder.geocode( {'address': address}, function(results, status) {
                      if (status == google.maps.GeocoderStatus.OK) {
                        temp = new google.maps.Marker({
                            map: map2,
                            position: results[0].geometry.location,
                            icon: basePath + '/img/icons/marker-'+returnMode+'.png',
                        });
                        latlng.push(temp.position);
                        bounds2.extend(temp.position);
                        google.maps.event.addListener(temp, 'click', function() {
                            infowindow.close();
                            infowindow.setContent(infowindowContent);
                            infowindow.open(map2, temp);
                        });
                      } else {
                        //console.log('Geocode was not successful for the following reason: ' + status);
                      }
                    });
                    
                })
                map2.setZoom(9);
                map2.setCenter(bounds2.getCenter());
            });

            $('#tabs').tabs({
                activate: function(event, ui) {
                    if(ui.newPanel.attr('id') == 'deliveries'){
                        google.maps.event.trigger(map1, 'resize');
                        initMap(window.map1, window.map2);
                    }
                    if(ui.newPanel.attr('id') == 'returns'){
                        google.maps.event.trigger(map2, 'resize');
                        initMap(window.map1, window.map2);
                    }                
                }
            });
            
        }

        var changeDeliveryMode = function(){
            $('body').on('change', 'select.delivery_mode', function(){
                var deliveryMode = $(this).val();
                var stockOrderId = $(this).data('stock-order-id');
                var input = $(this).parents('tr.stockorder').find('input.delivery_mode');
                $.ajax({
                    url: basePath + '/stock_orders/changeDeliveryMode',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        delivery_mode: deliveryMode,
                        stock_order_id: stockOrderId
                    },
                    success: function(data){
                        if(data.success){
                            input.val(deliveryMode);
                            toastr.success('Le mode de livraison a correctement été mis à jour.');
                            initMap(window.map1, window.map2);
                            google.maps.event.trigger(window.map1, 'idle');
                        }
                    }
                })
            })
        }
        var changeReturnMode = function(){
            $('body').on('change', 'select.return_mode', function(){
                var returnMode = $(this).val();
                var stockOrderId = $(this).data('stock-order-id');
                var input = $(this).parents('tr.stockorder').find('input.return_mode');
                $.ajax({
                    url: basePath + '/stock_orders/changeReturnMode',
                    type: 'post',
                    dataType: 'json',
                    data: {
                        return_mode: returnMode,
                        stock_order_id: stockOrderId
                    },
                    success: function(data){
                        if(data.success){
                            input.val(returnMode);
                            toastr.success('Le mode de livraison a correctement été mis à jour.');
                            initMap(window.map1, window.map2);
                            google.maps.event.trigger(window.map2, 'idle');
                        }
                    }
                })
            })
        }


        initMap(window.map1, window.map2);
        changeDeliveryMode();
        changeReturnMode();

        $('body').on('click', '.notifyTransporters', function(event){
            event.preventDefault();
            var lines;
            var type = $(this).attr('rel');
            if(type == 'delivery'){
                lines = $('table.deliveries tbody tr.stockorder');
            }
            if(type == 'return'){
                lines = $('table.returns tbody tr.stockorder');
            }
            $.each(lines, function(i,e){
                var line = $(e);
                var mode = line.find('.'+type+'_mode').val();
                var stockOrderId = line.find('input.stockOrderId').val();
                if( (type == 'delivery' && mode != 'client' && mode != 'festiloc') || (type == 'return' && mode != 'client' && mode != 'festiloc')){
                    line.find('.loading').fadeIn();
                    $.ajax({
                        url: basePath + '/stock_orders/notifyTransporter',
                        type: 'post',
                        dataType: 'json',
                        data: {
                            stock_order_id: stockOrderId,
                            mode: mode,
                            type: type
                        },
                        success: function(data){
                            line.find('.loading').fadeOut();
                            if(data.success){
                                line.find('.ok').fadeIn();
                            } else {
                                line.find('.not-ok').fadeIn();
                            }
                            setTimeout(function(){
                                line.find('span.ok, span.not-ok').fadeOut();
                            }, 10000);
                        }
                    })
                }
            })
        })

        $('#map1, #map2').css('height', 0);
        $('body').on('click', '.actions .btn', function(event){
            $('.actions .btn').removeClass('active');
            $(this).addClass('active');
            event.preventDefault();
            var rel = $(this).attr('rel');
            if(rel == 'list1'){
                $('#map1').css('height', 0).hide();
                $('#list1').show();
            }
            if(rel == 'map1'){
                $('#map1').css('height', 'auto').show();
                $('#list1').hide();
            }
            if(rel == 'list2'){
                $('#map2').css('height', 0).hide();
                $('#list2').show();
            }
            if(rel == 'map2'){
                $('#map2').css('height', 'auto').show();
                $('#list2').hide();
            }
        })
    }

    var handleConflicts = function(){
        if($('#calendar').length){
            $('#calendar').fullCalendar({
                lang: 'fr',
                firstDay: 1,
                defaultView: 'basicWeek',
                height: 'auto',
                columnFormat: 'dddd DD.MM',
                eventSources: [
                    {
                        url: basePath + '/stock_orders/conflicts.json'
                    }
                ]
            })
        }        
    }

    var handleInvoice = function(){
        $('body').on('change', 'input.invoice_replacement', function(){

            var line = $(this).parents('tr');
            var checked = $(this).is(':checked');
            var cost = parseFloat(line.find('.replacement_cost').val());
            var netTotal = parseFloat($('#StockOrderNetTotal').val());
            var tva = parseFloat($('#StockOrderTva').val());
            var replacementCosts = parseFloat($('#StockOrderReplacementCosts').val());
            var batchId = line.find('input.batch_id').val();
            var stockOrderId = $('#StockOrderId').val();

            var totalWithoutTva = netTotal - tva;

            if(checked){
                var newReplacementCosts = replacementCosts + cost;
                var newTva = tva + (cost * 8 / 100);
                var newNetTotal = totalWithoutTva + newTva + cost;
                var invoice_replacement = 1;
            } else {
                var newReplacementCosts = replacementCosts - cost;
                var newTva = tva - (cost * 8 / 100);
                var newNetTotal = totalWithoutTva + newTva - cost;
                var invoice_replacement = 0;
            }
            var newTva1 = (Math.ceil(( (newTva))*20 - 0.5)/20).toFixed(2);
            
            $('#loading').fadeIn();

            $.ajax({
                url: basePath + '/stock_orders/updateCosts',
                type: 'post',
                dataType: 'json',
                data: {
                    stock_order_batch_id: batchId,
                    stock_order_id: stockOrderId,
                    net_total: newNetTotal,
                    tva: newTva,
                    replacement_costs: newReplacementCosts,
                    invoice_replacement: invoice_replacement
                },
                success: function(data){
                    $('#loading').fadeOut();
                    $('#StockOrderNetTotal').val(newNetTotal.toFixed(2));
                    $('#StockOrderTva').val(newTva1);
                    $('#StockOrderReplacementCosts').val(newReplacementCosts.toFixed(2));
                    //$('td.net_total span').text(newNetTotal.toFixed(2));
                    $('td.net_total input').val(newNetTotal.toFixed(2));
                    $('td.tva span').text(newTva1);
                    $('td.replacement_costs span').text(newReplacementCosts.toFixed(2));
                    toastr.success('Les modifications ont été sauvegardées.');
                }
            })

        })

        $('body').on('change', 'input.invoice_extrahour', function(){

            var line = $(this).parents('tr');
            var checked = $(this).is(':checked');
            var cost = parseFloat(line.find('.extrahour_cost').val());
            var netTotal = parseFloat($('#StockOrderNetTotal').val());
            var tva = parseFloat($('#StockOrderTva').val());
            var extrahoursCosts = $('#StockOrderExtrahoursCosts').val().length ? parseFloat($('#StockOrderExtrahoursCosts').val()) : 0;
            var extraHourId = line.find('input.extrahour_id').val();
            var stockOrderId = $('#StockOrderId').val();

            var totalWithoutTva = netTotal - tva;

            if(checked){
                var newExtrahoursCosts = extrahoursCosts + cost;
                var newTva = tva + (cost * 8 / 100);
                var newNetTotal = totalWithoutTva + newTva + cost;
                var invoice_extrahour = 1;
            } else {
                var newExtrahoursCosts = extrahoursCosts - cost;
                var newTva = tva - (cost * 8 / 100);
                var newNetTotal = totalWithoutTva + newTva - cost;
                var invoice_extrahour = 0;
            }
            var newTva1 = (Math.ceil(( (newTva))*20 - 0.5)/20).toFixed(2);

            $('#loading').fadeIn();

            $.ajax({
                url: basePath + '/stock_orders/updateCosts',
                type: 'post',
                dataType: 'json',
                data: {
                    stock_order_id: stockOrderId,
                    net_total: newNetTotal,
                    tva: newTva1,
                    extrahours_costs: newExtrahoursCosts,
                    invoice_extrahour: invoice_extrahour,
                    extrahour_id: extraHourId
                },
                success: function(data){
                    $('#loading').fadeOut();
                    $('#StockOrderNetTotal').val(newNetTotal.toFixed(2));
                    $('#StockOrderTva').val(newTva1);
                    $('#StockOrderExtrahoursCosts').val(newExtrahoursCosts.toFixed(2));
                    //$('td.net_total span').text(newNetTotal.toFixed(2));
                    $('td.net_total input').val(newNetTotal.toFixed(2));
                    $('td.tva span').text(newTva1);
                    $('td.extrahours_costs span').text(newExtrahoursCosts.toFixed(2));
                    toastr.success('Les modifications ont été sauvegardées.');
                }
            })

        })
    }

    var handleSearchGooglePlaces = function(){
        $("#StockOrderAddressSearch").select2({
            minimumInputLength: 5,
            delay: 1000,
            ajax: {
                url: basePath + '/stock_orders/searchGooglePlaces',
                dataType: 'json',
                type: 'POST',
                data: function(term, page) {
                    return {
                        term: term
                    };
                },
                results: function(data, page) {
                    return {
                        results: data
                    };       
                }
            },
            formatResult: formatPlace
        });
        function formatPlace (place) {
          if (!place.id) { return place.text; }
          var div = '';
          div += '<div class="row">';
          div += '<div class="col-md-2">';
          div += '<img src="'+place.icon+'" class="img-responsive"/>';
          div += '</div>';
          div += '<div class="col-md-12">';
          div += '<h4>'+place.text+'</h4>';
          div += '<p>' + place.address + '<br>' + place.zip_city + '</p>';
          div += '</div>';
          div += '</div>';

          var $place = $(div);
          return $place;
        };
        $("#StockOrderAddressSearch").on("select2-selecting", function(e, choice) {
            $('#StockOrderDeliveryAddress').val(e.choice.address);
            $('#StockOrderDeliveryZipCity1').removeClass('hidden').val(e.choice.zip_city);
            $('#StockOrderDeliveryZip').val(e.choice.zip);
            $('#StockOrderDeliveryCity').val(e.choice.city);
            $('#StockOrderDeliveryZipCity, #s2id_StockOrderDeliveryZipCity').addClass('hidden');
        })
    }

    var handleModalClient = function(){
        $('body').on('submit', '#ClientAddForm', function(){
            var data = $(this).serialize();
            $.ajax({
                url: basePath + '/clients/add',
                data: data,
                dataType: 'json',
                success: function(data){
                    if(data.success == 1){
                        toastr.success('Client has been added.');
                        $('.modal.in').modal('hide');
                        $('select.client').append('<option value="'+data.clientId+'">'+data.clientName+'</option>')
                        $('.client').val(data.clientId).change();
                        $('.contact-people').val(data.contactPeopleId);
                    } else {
                        toast.error('An error occured. Please try again.');
                    }
                },
                error: function(request, errorType, errorText) {
                    toastr.error('Something wrong happened. Please try again.');
                    console.log(request.responseText);
                }
            })
            return false;
        })

        $('#new-contact-people').on('shown.bs.modal', function(){
            $('#ContactPeopleClientId').val($('select.client').val());
        })
        $('body').on('submit', '#ContactPeopleAddForm', function(){
            var data = $(this).serialize();
            $.ajax({
                url: basePath + '/contact_peoples/add',
                data: data,
                dataType: 'json',
                success: function(data){
                    if(data.success == 1){
                        toastr.success('Contact people has been added.');
                        $('.modal.in').modal('hide');
                        $('.client').change();
                        setTimeout(function(){
                            $('.contact-people').selectpicker('val', data.contactPeopleId);
                            $('.contact-people').selectpicker('refresh');
                            $('.contact-people').val(data.contactPeopleId).change();
                        }, 500);
                    } else {
                        toast.error('An error occured. Please try again.');
                    }
                }
            })
            return false;
        })
    }

    var handleGetContactPeople = function(){
        $('#StockOrderClientId').on('select2-close', function(){
            setTimeout(function(){
                var contactPeopleId = $('#StockOrderContactPeopleId').val();
                getContactPeople(contactPeopleId);
            }, 700)
        });

        var getContactPeople = function(id){
            $.ajax({
                url: basePath + '/contact_peoples/get',
                type: 'post',
                dataType: 'json',
                data: {
                    contact_people_id: id
                },
                success: function(data){
                    $('#StockOrderInvoicePhone').val(data.ContactPeople.phone);
                    $('#StockOrderInvoiceEmail').val(data.ContactPeople.email);
                }
            })
        }

        setInterval(function(){
            var contactPeopleId = $('#StockOrderContactPeopleId').val();
            if(contactPeopleId){
                getContactPeople(contactPeopleId);
            }            
        }, 2000)
    }

    var handleAutomaticSave = function(){

        var save = function(){
            return;
            setTimeout(function(){
                var data = $('#StockOrderEditForm').serialize();
                var id = $('#StockOrderId').val();
                $.ajax({
                    url: basePath + '/stock_orders/edit/' + id,
                    data: data,
                    type: 'post',
                    dataType: 'json',
                    success: function(data){
                        toastr.clear();
                        toastr.info('La comamnde a été sauvegardée.');
                        if($('#StockOrderName').val().length > 0)$('span.caption-subject').text($('#StockOrderName').val());
                        if(data){
                            $('#stockitemsTable tbody tr:not(.empty)').each(function(i,e){
                                if($(e).find('input.id').val().length < 1) $(e).find('input.id').val(data[i].id);
                            })
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log(request.responseText);
                    }
                })
            }, 100)            
        }
        if(document.location.href.indexOf('stock_orders/edit/') > 0){
            setInterval(function(){
                save();            
            }, 10000);
        }

        // $('body').on('blur', '#StockOrderEditForm input:not(.date-picker), #StockOrderEditForm textarea', function(){
        //     save();
        // })
        // $('body').on('change', '.date-picker', function(){
        //     save();
        // })
        // $('body').on('click', '.btn-group', function(){
        //     save();
        // })
        // $('body').on('change', 'select', function(){
        //     save();
        // })
    }

    var handleBootbox = function(){
        $('body').on('click', '.confirm-offer', function(event){
            event.preventDefault();
            var href = $(this).attr('href');
            bootbox.confirm("Êtes-vous sûr de vouloir confirmer l'offre?", function(result) {
                if(result){
                    window.location.href = href;
                } else {
                    return;
                }
            });
        })
    }

    var initCkeditor = function(){
        CKEDITOR.replaceAll( 'ckeditor', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["list"]}
            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        } );
    }

    var handleDraggableProducts = function(){

        $('.sortable').on('click', function(event){
            event.preventDefault();
        })

        $("#stockitemsTable tbody").sortable({
            revert: true,
            placeholder: "empty-row",
            stop: function(event, ui) {
                //var rows = [];
                $("#stockitemsTable tbody tr").each(function(i,e){
                    $(e).find('.weight').val(i);
                })
                // $.ajax({
                //     url: basePath + '/stock_order_batches/updateWeights',
                //     type: 'post',
                //     dataType: 'json',
                //     data: {
                //         rows: rows
                //     },
                //     success: function(data){
                //         if(data.success){
                //             toastr.success("L'ordre des produits a bien été sauvegardé.")
                //         }
                //     },
                //     error: function(request, errorType, errorText) {
                //         toastr.error('Something wrong happened. Please try again.');
                //         console.log(request.responseText);
                //     }
                // })
            }
        });
        //$( "#stockitemsTable tbody" ).disableSelection();

    }

    var handleScrollSpy = function(){
        $('body').scrollspy({ target: '#profile-nav', offset: 90 });

        $('#profile-nav a').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 90)
            }, 1000, 'easeInOutExpo');
            event.preventDefault();
        });
        $(window).scroll(function(){
            scrollProfileMenu();
        });
        $(window).load(function(){
            scrollProfileMenu();
        });
        var scrollProfileMenu = function(){
            if ($(window).scrollTop() > 50){
                $("#profile-menu").css("top", $(window).scrollTop() - 50 + "px");
            } else {
                $("#profile-menu").css("top", "0px");
            }
        }
    }

    var handleHideAlert = function(){
        if($('.alert').length > 0){
            setTimeout(function(){
                $('.alert').fadeOut();
            }, 5000);
        }
    }

    // public functions
    return {

        //main function
        init: function () {

            //initialize here something.
            handleDatePickers();
            handleBootstrapSelect();
            handleRemoveConfiguration();
            handleRemoveActivity();
            handleConfigurations();
            handleEditDocument();
            handleDeleteDocument();
            handleSelectCommune();
            handleTagsSelection();
            portletDraggable();
            handleLoadEvent();
            $('[data-toggle="tooltip"]').tooltip();
            //handleUploadForm();
            //handleGetLatLng();
            /*if(typeof(Dropzone) !== undefined){
                Dropzone.options.internalPhotosForm1 = {
                    init: function() {
                        this.on("complete", function(file) {
                            console.log('Photo has been saved.')
                        });
                        this.on("completemultiple", function(file) {
                            console.log('Photos have been saved.')
                        });
                    }
                }
            }*/
        },

        user: function() {
            handleBootstrapSelect();
            handleTagsSelection();
            handleDatePickers();
            handleAvailabilities();
            initDatatables($('#tabs'));
            // handleDatatable($('#users'), [0,4], [1,2,3], true, [1, "asc"]);
            // handleDatatable($('#users1'), [0,5], [1,2,3,4], true, [1, "asc"]);
            // handleDatatable($('#users2'), [0,4], [1,2,3], true, [1, "asc"]);
            // handleDatatable($('#users3'), [0], [1,2], true, [1, "asc"]);
            // handleDatatable($('#users4'), [1], [0], true, [0, "asc"]);
            // handleDatatable($('#users5'), [1], [0], true, [0, "asc"]);
            handleRiseUser();
            if($('#users').length){
                $('#users').dataTable({
                    "ajax": {
                        "url": basePath + '/users/index_json.json',
                        'dataSrc': 'data.fixed'
                    },
                    'columns': [
                        {data: "User.portrait"},
                        {data: "User.full_name"},
                        {data: "User.email"},
                        {data: "actions[&nbsp;]"}
                    ],
                    "pageLength": 20,
                    "lengthMenu": [
                        [10, 20, 100, -1],
                        [10, 20, 100, "All"] // change per page values here
                    ],
                    "aaSorting": [[1, 'asc']]
                });
            }
            
            if($('#users1').length){
                $('#users1').dataTable({
                    "ajax": {
                        "url": basePath + '/users/index_json.json',
                        'dataSrc': 'data.temporary'
                    },
                    'columns': [
                        {data: "User.portrait"},
                        {data: "User.full_name"},
                        {data: "User.email"},
                        {data: "User.phone"},
                        {data: "Section[, ]"},
                        {data: "actions[&nbsp;]"}
                    ],
                    "pageLength": 20,
                    "lengthMenu": [
                        [10, 20, 100, -1],
                        [10, 20, 100, "All"] // change per page values here
                    ],
                    "aaSorting": [[1, 'asc']]
                }).columnFilter({
                    sPlaceHolder : 'head:before',
                    aoColumns: [ null,
                                 null,
                                 null,
                                 null,
                                 { type: "select", values: [ 'Animation', 'F&B', 'Logistique']},
                                 null
                               ] 
                });
            }
            
            if($('#users2').length){
                $('#users2').dataTable({
                    "ajax": {
                        "url": basePath + '/users/index_json.json',
                        'dataSrc': 'data.candidate'
                    },
                    'columns': [
                        {data: "User.portrait"},
                        {data: "User.full_name"},
                        {data: "User.email"},
                        {data: "User.phone"},
                        {data: "User.remarks"},
                        {data: "Section[, ]"},
                        {data: "actions[&nbsp;]"}
                    ],
                    "pageLength": 20,
                    "lengthMenu": [
                        [10, 20, 100, -1],
                        [10, 20, 100, "All"] // change per page values here
                    ],
                    "aaSorting": [[1, 'asc']]
                }).columnFilter({
                    sPlaceHolder : 'head:before',
                    aoColumns: [ null,
                                 null,
                                 null,
                                 null,
                                 null,
                                 { type: "select", values: [ 'Animation', 'F&B', 'Logistique']},
                                 null
                               ] 
                });
            }
            
            if($('#users3').length){
                $('#users3').dataTable({
                    "ajax": {
                        "url": basePath + '/users/index_json.json',
                        'dataSrc': 'data.to_evaluate'
                    },
                    'columns': [
                        {data: "User.portrait"},
                        {data: "User.full_name"},
                        {data: "Job.name"},
                        {data: "Event.name"},
                        {data: "Event.confirmed_date"},
                        {data: "Client.name"},
                        {data: "actions[&nbsp;]"}
                    ],
                    "pageLength": 20,
                    "lengthMenu": [
                        [10, 20, 100, -1],
                        [10, 20, 100, "All"] // change per page values here
                    ],
                    "aaSorting": [[1, 'asc']]
                });
            }
            

            $('#users4').dataTable({
                "ajax": {
                    "url": basePath + '/users/index_json.json',
                    'dataSrc': 'data.talents'
                },
                'columns': [
                    {data: "User.portrait"},
                    {data: "Talent.name"},
                    {data: "User.full_name"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ],
                "aaSorting": [[2, 'asc']]
            });

            $('#users5').dataTable({
                "ajax": {
                    "url": basePath + '/users/index_json.json',
                    'dataSrc': 'data.unsuitable'
                },
                'columns': [
                    {data: "User.full_name"},
                    {data: "actions[&nbsp;]"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ],
                "aaSorting": [[0, 'asc']]
            });

            $('#users6').dataTable({
                "ajax": {
                    "url": basePath + '/users/index_json.json',
                    'dataSrc': 'data.retired'
                },
                'columns': [
                    {data: "User.full_name"},
                    {data: "actions[&nbsp;]"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ],
                "aaSorting": [[0, 'asc']]
            });
        },

        search: function() {
            handleCheckAll();
            handleLoadEvent();
            handleRecruit();
            handleDatePickers();
            mapMarkerPlaces();
            handleShowMapList();
            handlePlacesResults();
            handleDatePickers();
        },

        gmap: function() {
            mapMarker();
        },

        places: function() {
            initDatatables($('#tabs'));
            //handleDatatable($('#places'), [0,5], [1,2,3,4], true, [1, "asc"]);
            //handleDatatable($('#places1'), [0,5], [1,2,3,4], true, [1, "asc"]);
            //handleDatatable($('#places2'), [0,5], [1,2,3,4], true, [1, "asc"]);
            handleDocumentsActions();
            $('#places').dataTable({
                "ajax": {
                    "url": basePath + '/places/index_json.json',
                    'dataSrc': 'data.all'
                },
                'columns': [
                    {"data": "Place.checked"},
                    {"data": "Place.name"},
                    {"data": "Place.city"},
                    {"data": "Place.type"},
                    {"data": "Place.modified[ par ]"},
                    {"data": "actions[&nbsp;]"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ]
            });
            $('#places1').dataTable({
                "ajax": {
                    "url": basePath + '/places/index_json.json',
                    'dataSrc': 'data.incomplete'
                },
                'columns': [
                    {"data": "Place.checked"},
                    {"data": "Place.name"},
                    {"data": "Place.city"},
                    {"data": "Place.type"},
                    {"data": "Place.modified[ par ]"},
                    {"data": "actions[&nbsp;]"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ]
            });
            $('#places2').dataTable({
                "ajax": {
                    "url": basePath + '/places/index_json.json',
                    'dataSrc': 'data.to_check'
                },
                'columns': [
                    {"data": "Place.checked"},
                    {"data": "Place.name"},
                    {"data": "Place.city"},
                    {"data": "Place.type"},
                    {"data": "Place.modified[ par ]"},
                    {"data": "actions[&nbsp;]"}
                ],
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ]
            });
        },

        place: function(){
            initPlaceAgenda();
        },

        activities: function() {
            handleDatatable($('#activities'), [1], [0], false, [0, "asc"]);
            // initCkeditor('ActivityDescription');
            // initCkeditor('ActivityRules');
            // initCkeditor('ActivityHints');
            // initCkeditor('GameDescription');
            //initCkeditor();
        },

        clients: function() {
            //handleDatatable($('#clients'), [1], [0], false, [0, "asc"]);
            $('#clients').dataTable({
                "ajax": {
                    "url": basePath + '/clients/index_json.json'
                },
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ],
                'columns': [
                    {"data": "Client.name"},
                    {"data": "ContactPeople.name[<br />]"},
                    {"data": "Client.address[<br />]"},
                    {"data": "actions[&nbsp;]"}
                ]
            });
            $('#festilocClients').dataTable({
                "ajax": {
                    "url": basePath + '/festiloc/clients_json.json'
                },
                "pageLength": 20,
                "lengthMenu": [
                    [10, 20, 100, -1],
                    [10, 20, 100, "All"] // change per page values here
                ],
                'columns': [
                    {"data": "Client.name"},
                    {"data": "ContactPeople.name[<br />]"},
                    {"data": "Client.address[<br />]"},
                    {"data": "actions[&nbsp;]"}
                ]
            });
        },

        stockitems: function() {
            handleSelectStockItem($("#selectStockItem"));
            handleDeleteStockItem();
            //handleDatatable($('#stockitems0'), [5], [0,1,2,3,4], true, [0, "asc"]);
        },

        stockitem: function() {
            handleSectionsFamilies();
            handleDeleteStockItem();
        },

        stockitemChart: function() {
            var initChartSample1 = function() {
                var dataProvider = [];
                $('#data input').each(function(i,e){
                    dataProvider.push(
                        {
                            'day': $(e).data('day'), 
                            'quantity': $(e).data('actualQuantity'),
                            'reconditionning': $(e).data('reconditionning'),
                            'reparation': $(e).data('reparation'),
                            'potential': $(e).data('potential'),
                            'confirmed': $(e).data('confirmed'),
                            'totalQuantity': $(e).data('quantity'),
                        }
                    )
                })
                var chart = AmCharts.makeChart("chart_1", {
                    "type": "serial",
                    "theme": "light",
                    "pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",
                    "autoMargins": false,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,

                    "fontFamily": 'Open Sans',            
                    "color":    '#888',
                    
                    "dataProvider": dataProvider,
                    "valueAxes": [{
                        "stackType": "regular",
                        "axisAlpha": 0,
                        "position": "left",
                        "integersOnly": true
                    }],
                    "startDuration": 1,
                    "legend": {
                        "horizontalGap": 10,
                        "maxColumns": 1,
                        "position": "bottom",
                        "useGraphSettings": true,
                        "markerSize": 10
                    },
                    "graphs": [{
                        "balloonText": "<span style='font-size:13px;'>[[title]] : <b>[[value]]</b> [[additional]]</span>",
                        "bullet": "round",
                        "dashLengthField": "dashLengthLine",
                        "lineThickness": 3,
                        "bulletSize": 7,
                        "bulletBorderAlpha": 1,
                        "bulletColor": "#FFFFFF",
                        "useLineColorForBulletBorder": true,
                        "bulletBorderThickness": 3,
                        "fillAlphas": 0,
                        "lineAlpha": 1,
                        "title": "Quantité totale",
                        "valueField": "totalQuantity",
                        "lineColor": "#ccc"
                    },{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Quantité restante",
                        "type": "column",
                        "valueField": "quantity",
                        "labelText": "[[value]]",
                        "lineColor": "#8775a7",
                        "color": "#fff"
                    },{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Quantité confirmée",
                        "type": "column",
                        "valueField": "confirmed",
                        "labelText": "[[value]]",
                        "lineColor": "#26a69a",
                        "color": "#fff"
                    },{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Quantité en offre",
                        "type": "column",
                        "valueField": "potential",
                        "labelText": "[[value]]",
                        "lineColor": "#3598dc",
                        "color": "#fff"
                    }, {
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Quantité en recondionnement",
                        "type": "column",
                        "valueField": "reconditionning",
                        "labelText": "[[value]]",
                        "lineColor": "#f3c200",
                        "color": "#fff"
                    },{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Quantité en réparation",
                        "type": "column",
                        "valueField": "reparation",
                        "labelText": "[[value]]",
                        "lineColor": "#E08283",
                        "color": "#fff"
                    }],
                    "categoryField": "day",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    },
                    "export": {
                        "enabled": true
                     }
                });

                $('#chart_1').closest('.portlet').find('.fullscreen').click(function() {
                    chart.invalidateSize();
                });

                chart.addListener('clickGraphItem', function(event){
                    console.log(event.item.category);
                    console.log(event.item.values);
                })
            }
            initChartSample1();
        },

        prepare: function(){
            if(document.location.hostname == 'localhost' || document.location.hostname == '192.168.1.126'){
                basePath = '/myubic';
            }
            if($('#debug-kit-toolbar').length > 0){
                $('#debug-kit-toolbar').addClass('hidden-print');
            }
            handleRadioButtons();
            $('li.has-children ul').addClass('sub-menu');
            $('ul.page-sidebar-menu').attr('data-auto-scroll', true).attr('data-slide-speed', 200);
        },

        competences: function(){
            handleCompetencesJobs('competences');
        },

        events: function(){
            if($('#tabs').length > 0){
                initDatatables($('#tabs'));
                handleDatatable($('#events'), [4], [0,1,2,3], false, [0, "asc"]);
                handleDatatable($('#events1'), [4], [0,1,2,3], false, [0, "asc"]);
                handleDatatable($('#options1'), [], [0,1,2,3,4,5,6], false, [0, "asc"]);
            }
            handleCompetencesJobs('jobs');
            handleMoments('moments');
            handleContactPeople('#EventClientId', '#EventContactPeopleId');
            handleUpdateOption();
            if($('#feeling').length > 0){
                $('#feeling').noUiSlider({
                    start: $('#EventFeeling').val(),
                    step: 5,
                    connect: "lower",
                    range: {
                        'min': 0,
                        'max': 100
                    }
                });
                $("#feeling").Link('lower').to($('#EventFeeling'));
                $("#feeling").Link('lower').to($('#EventFeelingSpan'));
            }
            if($('#calendar').length > 0){
                initCalendar();
            }
            handleModalClient();
        },

        stockorders: function(){
            handleAddStockItem();
            handleDatatable($('#stockorders'), [4], [0,1,2,3], false, [0, "asc"]);
            handleContactPeople('#StockOrderClientId', '#StockOrderContactPeopleId, #StockOrderDeliveryContactPeopleId, #StockOrderReturnContactPeopleId');
            handleStockOrderMoments();
            handleStockOrderDistanceCovered();
            handleSelectStockOrderPlace();
            if($('.computeTotal').length)handleComputeTotal();
            handleWeekPreparation();
            handleInvoice();
            handleSearchGooglePlaces();
            handleModalClient();
            handleGetContactPeople();
            handleAutomaticSave();
            handleBootbox();
            handleDraggableProducts();
        },

        communes: function(){
            handleGetLatLngCommune();
        },

        pricelist: function(){
            //handleStockItemsImages();
            handleSelectCategory();
        },

        modules: function(){
            handleSelectModuleCategory();
            handleUserModules();
        },

        warehouse: function(){
            handleDepotPlan();
        },

        messages: function(){
            initSendMessage();
        },

        festiloc: function(){
            handleFestilocLive();
            handleConflicts();
            $('body').on('change', '.updatePackaging', function(){
                var stockOrderId = $('#StockOrderId').val();
                var numberOfPallets = $('#StockOrderNumberOfPallets1').val().length ? $('#StockOrderNumberOfPallets1').val() : '';
                var numberOfRollis = $('#StockOrderNumberOfRollis1').val().length ? $('#StockOrderNumberOfRollis1').val() : '';
                var xlSurcharge = $('#StockOrderXlSurcharge1').val().length ? $('#StockOrderXlSurcharge1').val() : '';
                $.ajax({
                    url: basePath + '/stock_orders/updatePackaging',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        id: stockOrderId,
                        numberOfPallets: numberOfPallets,
                        numberOfRollis: numberOfRollis,
                        xlSurcharge: xlSurcharge
                    },
                    success: function(data){
                        if(data.success == 1){
                            toastr.success('La commande a été sauvegardée.');
                        }
                    },
                    error: function(request, errorType, errorText) {
                        toastr.error('Something wrong happened. Please try again.');
                        console.log(request.responseText);
                    }
                })
            })
        },

        stockitemunavailabilities: function(){
            handleSelectStockItem($("#StockItemUnavailabilityStockItemId"));
        },

        me: function(){
            handleScrollSpy();
            handleHideAlert();
            handleAvailabilities();
        }

    };

}();