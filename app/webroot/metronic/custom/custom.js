/**
Custom module for you to write your own javascript functions
**/
var Custom = function() {

  var basePath = "";
  var googleMapsDelay = 750;

  var handleSidebarAndContentHeight = function() {
    var content = $('.page-content');
    var sidebar = $('.page-sidebar');
    var body = $('body');
    var height;

    if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
      var available_height = App.getViewPort().height - $('.page-footer').outerHeight() - $('.page-header').outerHeight();
      if (content.height() < available_height) {
        content.attr('style', 'min-height:' + available_height + 'px');
      }
    } else {
      if (body.hasClass('page-sidebar-fixed')) {
        height = _calculateFixedSidebarViewportHeight();
        if (body.hasClass('page-footer-fixed') === false) {
          height = height - $('.page-footer').outerHeight();
        }
      } else {
        var headerHeight = $('.page-header').outerHeight();
        var footerHeight = $('.page-footer').outerHeight();

        if (App.getViewPort().width < resBreakpointMd) {
          height = App.getViewPort().height - headerHeight - footerHeight;
        } else {
          height = sidebar.outerHeight() + 10;
        }

        if ((height + headerHeight + footerHeight) <= App.getViewPort().height) {
          height = App.getViewPort().height - headerHeight - footerHeight;
        }
      }
      content.attr('style', 'min-height:' + height + 'px');
    }
  };

  var log_error = function(request, errorType, errorText) {
    if (errorType == 'abort') {
      return;
    }
    toastr.error('Something wrong happened. Please try again.');
    console.log([
      'Error Type: ' + errorType,
      'Error Text: ' + request.responseText
    ].join('\n'));
  };

  var roundAmount = function(amount) {
    return (Math.ceil(amount * 20 - 0.5) / 20).toFixed(2);
  };

  var roundToTwo = function(num) {
    return +(Math.round(num + "e+2") + "e-2");
  };

  var convertDate = function(inputFormat) {
    function pad(s) {
      return (s < 10) ? '0' + s : s;
    }
    var d = new Date(inputFormat);
    return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
  };

  var ajaxInProgress = false;
  var ajaxStockOrderSave = '';

  var stockOrderSave = function(callback) {
    //if(ajaxInProgress) return;
    //ajaxInProgress = true;
    setTimeout(function() {
      var data = $('#StockOrderEditForm').serialize();
      //$('.form-actions button, .form-actions a').addClass('disabled').attr('disabled', 'disabled');
      ajaxStockOrderSave = $.ajax({
        url: document.location.href,
        data: data,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          toastr.options = {
            preventDuplicates: true,
            hideDuration: 1
          }
          toastr.clear();
          toastr.info('La commande a été sauvegardée.');
          if ($('#StockOrderName').val().length > 0) $('span.caption-subject').text($('#StockOrderName').val());
          if (data) {
            $('#stockitemsTable tbody tr:not(.empty, .free)').each(function(i, e) {
              $(e).find('.text-not-saved').addClass('hidden');
              if ($(e).find('input.id').val().length < 1) {
                var temp = data[i];
                if (typeof temp !== 'undefined') {
                  $(e).find('input.id').val(temp.id);
                } else {
                  $(e).find('.text-not-saved').removeClass('hidden');
                }
              }
            });
          }
          if (callback) {
            callback();
          }
          //ajaxInProgress = false;
          //$('.form-actions button, .form-actions a').removeClass('disabled').removeAttr('disabled');
          //$('.view-offer').removeClass('disabled');
        },
        error: log_error,
        complete: function() {
          //ajaxInProgress = false;
          //$('.form-actions button, .form-actions a').removeClass('disabled').removeAttr('disabled');
        }
      });
    }, 20);
  }

  var handleInputMasks = function() {
    // $.extend($.inputmask.defaults, {
    //     'autounmask': true
    // });

    // $(".date-picker").inputmask("dd-mm-yyyy", {
    //    autoUnmask: true
    // });

    $('.date-picker').attr('placeholder', 'dd-mm-yyyy');
  }

  var handleDatePickers = function() {

    if (jQuery().datepicker) {
      $('.date-picker:not(.months-only)').datepicker({
        orientation: "left",
        autoclose: true,
        weekStart: 1,
        language: 'fr-FR',
        format: 'dd-mm-yyyy'
      });
      $('.date-picker.months-only').datepicker({
        orientation: "left",
        autoclose: true,
        weekStart: 1,
        language: 'fr-FR',
        format: 'MM yyyy'
      });
      $(".datetime-picker").datetimepicker({
        autoclose: true,
        weekStart: 1,
        format: "dd-mm-yyyy - hh:ii",
        language: 'fr-FR'
      });
      $('.due-date-picker').datepicker({
        orientation: "left",
        autoclose: true,
        clearBtn: true,
        weekStart: 1,
        language: 'fr-FR',
        format: 'dd-mm-yyyy'
      });
      $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }
    $('.timepicker-24').timepicker({
      autoclose: true,
      minuteStep: 5,
      showSeconds: false,
      showMeridian: false,
      defaultTime: false
    });
    $('body').on('keydown', '.timepicker-24', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode == 9) {
        $(this).timepicker('hideWidget');
      }
    });
  };

  var handleBootstrapSelect = function() {
    $('.bs-select').selectpicker({
      iconBase: 'fa',
      tickIcon: 'fa-check',
      size: 5
    });
  };

  var handleRemoveConfiguration = function() {
    $('body').on('click', 'a.remove-configuration', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var config = $(this).parents('.configuration-form');

      $.ajax({
        type: 'GET',
        dataType: 'json',
        url: basePath + '/configurations/delete/' + id,
        success: function(data) {
          toastr.success('Configuration has been removed.')
          config.fadeOut();
        },
        error: log_error
      });
    });
  };

  var handleRemoveActivity = function() {

    $('body').on('click', 'a.add-activity', function(event) {
      event.preventDefault();

      var div = $('.activity-form:first');
      var clone = div.clone();
      clone.find('.caption').text('New activity');
      clone.hide();
      clone.find('.select2-container').remove();
      clone.find('input, textarea').each(function(i, e) {
        $(e).val('');
        var index = $('.activity-form').get().length;
        if ($(e).attr('name') !== undefined) var name = $(e).attr('name').replace('[0]', '[' + index + ']');
        var id = $(e).attr('id').replace('0', index);
        $(e).attr('name', name);
        $(e).attr('id', id);
      });
      $('#activities .form-actions').before(clone);
      clone.fadeIn();
    });

    $('body').on('click', 'a.remove-activity', function(event) {
      event.preventDefault();
      var id = $(this).data('id');
      var activity = $(this).parents('.activity-form');

      $.ajax({
        type: 'GET',
        dataType: 'json',
        url: basePath + '/activities/delete/' + id,
        success: function(data) {
          toastr.success('Activity has been removed.');
          activity.fadeOut();
        },
        error: log_error
      });
    });
  };

  var handleConfigurations = function() {
    $('body').on('click', 'a.add-configuration', function(event) {
      event.preventDefault();

      var configDiv = $('.configuration-form:first');
      var clone = configDiv.clone();
      clone.find('.caption').text('New configuration');
      clone.hide();
      clone.find('.select2-container').remove();
      clone.find('input').each(function(i, e) {
        $(e).val('');
        var index = $('.configuration-form').get().length;
        if ($(e).attr('name') !== undefined) var name = $(e).attr('name').replace('[0]', '[' + index + ']');
        var id = $(e).attr('id').replace('0', index);
        $(e).attr('name', name);
        $(e).attr('id', id);
      });
      $('#configurations .form-actions').before(clone);
      clone.fadeIn();
      handleTagsSelection();
    });
  };

  var handlePlacesOptions = function() {

    $('body').on('click', '.save-option', function(event) {
      event.preventDefault();
      var rel = $(this).attr('rel');
      var div = $('#' + rel);
      var activeInput = div.find('.options label.active input');
      var option = activeInput.data('value');
      var id = div.find('input.option_id').val();
      var date = activeInput.data('date');
      var place_id = activeInput.data('place-id');
      var moment_id = activeInput.data('moment-id');
      var event_id = activeInput.data('event-id');
      var user_id = activeInput.data('user-id');
      var remarks = div.find('input.remarks').val();
      var valid_until = div.find('input.date-picker').val();

      if (typeof(option) !== 'undefined') {
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: basePath + '/options/save',
          data: {
            id: id,
            option: option,
            date: date,
            place_id: place_id,
            moment_id: moment_id,
            event_id: event_id,
            user_id: user_id,
            remarks: remarks,
            valid_until: valid_until
          },
          success: function(data) {
            if (data.success == 1) {
              $('#' + rel).find('input.option_id').val(data.id);
              toastr.success('Option has been saved.');
            }
          },
          error: log_error
        })
      } else {
        bootbox.alert("No option has been selected! Please select one.");
      }
    });

    $('.btn-group.options').each(function(i, e) {
      var div = $(e);
      var place_id = $(e).find('input:first').data('place-id');
      var moment_id = $(e).find('input:first').data('moment-id');
      var date = $(e).find('input:first').data('date');

      if (date && place_id) {
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: basePath + '/options/getInfos',
          data: {
            place_id: place_id,
            date: date,
            moment_id: moment_id
          },
          success: function(data) {
            if (data.overlap == 1 && data.empty == 0) {
              div.find('label.' + data.Option.value).addClass('active');
            }
            if (data.empty == 0 && data.overlap == 0) {
              div.find('label.' + data.Option.value).addClass('active');
              div.find('input.option_id').val(data.Option.id);
              div.find('p.infos').show();
              div.find('p.infos .user').text(data.User.name);
              div.find('p.infos .event').text(data.Event.name);
              div.find('p.infos .moment').text(data.Moment.name);
              div.find('p.infos .start_hour').text(data.Moment.start_hour);
              div.find('p.infos .end_hour').text(data.Moment.end_hour);
            } else if (data.empty == 1) {
              div.find('label.free').addClass('active');
            }
          },
          error: log_error
        });
      }

    });
  };

  var handleEditDocument = function() {
    $('body').on('click', '.edit-document', function(event) {
      event.preventDefault();
      var modal = $(this).parents('.modal');
      var id = modal.find('input:first').data('id');
      var newName = modal.find('input#newName' + id).val();
      var group = modal.find('select#group' + id).val();

      $.ajax({
        type: 'POST',
        dataType: 'json',
        url: basePath + '/documents/edit/' + id + '/' + newName + '/' + group,
        success: function(data) {
          toastr.success('The document has been correctly updated. Changes will be visible after refreshing the page.', 'Success');
          modal.modal('hide');
          if (data.refresh) {
            location.reload();
          }
        },
        error: log_error
      });
    });
  };

  var handleDeleteDocument = function() {
    $('body').on('click', '.mix .delete-document', function(event) {
      event.preventDefault();
      event.stopPropagation();
      var documentId = $(this).data('document-id');
      var div = $(this).parents('.mix');

      bootbox.confirm("Are you sure you want to delete this document?", function(result) {
        if (result) {
          $.ajax({
            type: 'POST',
            dataType: 'json',
            url: basePath + '/documents/delete',
            data: {
              documentId: documentId
            },
            success: function(data) {
              toastr.success('Document has been correctly deleted!', 'Success');
              div.fadeOut();
            },
            error: log_error
          });
        }
      });
    });
  };

  var handleSelectCommune = function() {

    if ($('[data-commune-select2]').length) {

      $('[data-commune-select2]').each(function(i, e) {
        var $this = $(e);
        var $zipCityInput = $this.find('[data-name="zipcity"]');
        var $zipInput = $this.find('input[data-name="zip"]');
        var $cityInput = $this.find('input[data-name="city"]');

        $zipCityInput.select2({
          width: "off",
          allowClear: true,
          placeholder: {
            id: "",
            placeholder: "Sélectionnez une localité..."
          },
          minimumInputLength: 2,
          communes: [],
          ajax: {
            url: basePath + "/communes/get",
            dataType: 'json',
            type: "POST",
            quietMillis: 50,
            data: function(term) {
              return term;
            },
            processResults: function(data) {
              return {
                results: $.map(data, function(item) {
                  return {
                    text: item.Commune.zip_name_canton,
                    id: item.Commune.id,
                    name: item.Commune.name,
                    zip: item.Commune.zip
                  }
                })
              };
            },
            cache: true
          }
        }).on('select2:select', function(event) {
          $zipInput.val(event.params.data.zip);
          $cityInput.val(event.params.data.name);

          $('#PlaceAddress').blur();
          $('#StockOrderDeliveryAddress').blur();

        }).on('select2:unselect', function(event) {
          console.log(event);
          $zipInput.val('');
          $cityInput.val('');
        });
      });

    }

    $('body').on('blur', '#PlaceAddress', function(event) {
      var address = $('#PlaceAddress').val();
      var zip = $('#PlaceZip').val();
      var city = $('#PlaceCity').val();

      if (zip.length > 0 && city.length > 0) {
        $('#PlaceLatitude, #PlaceLongitude').val('').addClass('spinner');
        $.ajax({
          url: basePath + '/places/getLatLng',
          data: {
            address: address,
            city: city,
            zip: zip
          },
          type: 'POST',
          dataType: 'json',
          success: function(data) {
            $('#PlaceLatitude').val(data.latitude);
            $('#PlaceLongitude').val(data.longitude);
            $('#PlaceLatitude, #PlaceLongitude').removeClass('spinner');
          }
        });
      }
    });
  };

  var handleSelect2Tags = function() {
    $('[data-select2-tags]').select2({
      tags: true
    });
  }

  var handleTagsSelection = function() {
    return;
    if ($(".select2").length > 0) {
      $('.select2').each(function(i, field) {
        $(field).select2({
          multiple: true,
          closeOnSelect: false,
          ajax: {
            url: basePath + '/tags/select2',
            dataType: 'json',
            type: 'POST',
            data: function(term, page) {
              return {
                term: term,
                category: $(this).data('category')
              };
            },
            results: function(data, page) {
              return {
                results: data
              };
            }
          },
          initSelection: function(element, callback) {
            var tags = [];
            $(element.val().split(",")).each(function(i, e) {
              $.ajax({
                url: basePath + '/tags/getValue',
                dataType: 'json',
                type: 'POST',
                data: {
                  id: e
                },
                success: function(value) {
                  tags.push({
                    id: e,
                    text: value
                  });
                  callback(tags);
                }
              });
            });
          }
        });
      });
    }
  };

  var portletDraggable = function() {
    $("#sortable_portlets").sortable({
      connectWith: ".portlet",
      handle: ".portlet-title",
      items: ".portlet",
      opacity: 0.8,
      coneHelperSize: true,
      placeholder: 'portlet-sortable-placeholder',
      forcePlaceholderSize: true,
      tolerance: "pointer",
      helper: "clone",
      cancel: ".portlet-sortable-empty",
      revert: 250, // animation in milliseconds
      update: function(b, c) {
        if (c.item.prev().hasClass("portlet-sortable-empty")) {
          c.item.prev().before(c.item);
        }
        $('#sortable_portlets .portlet').each(function(i, e) {
          $(e).find('.weight').attr('value', i);
        });
      }
    });
  };

  var handleLoadEvent = function() {
    if ($('#SearchEventId').length) {
      $('#SearchEventId').bind('change', function() {
        var eventId = $(this).find(':selected').val();
        $.ajax({
          url: basePath + '/events/getData',
          data: {
            id: eventId
          },
          type: 'POST',
          dataType: 'json',
          success: function(data) {
            $('#EventClientId').find('option[value="' + data.Client.id + '"]').attr('selected', 'selected');
            $('#EventConfirmedDate').val(data.Event.confirmed_date);
            var potentialDates = '';
            data.Date.forEach(function(e) {
              potentialDates += e.date + '  ';
            })
            $('#EventPotentialDates').val(potentialDates);
          }
        });
      });
    }
  };

  var handleAvailabilities = function() {
    $('body').on('click', 'div.btn-group label.btn', function(event) {
      event.preventDefault();
      $(this).parents('.btn-group').find('input').removeAttr('checked');
      $(this).parents('.btn-group').find('label').removeClass('active');
      $(this).find('input').attr('checked', 'checked');
    });

    if ($('.table-availabilities').length) {
      var table = $('.table-availabilities');
      $('body').on('click', '.availability-cell', function() {
        var $this = $(this);
        var day = $this.parents('tr').data('day');
        $this.toggleClass('selected');
        if ($this.hasClass('selected')) {
          updateCellColor($this, 'dark');
        } else {
          updateCellColor($this, 'light');
        }
        showList(day);
      });
      $('body').on('click', '.options button', function() {
        var $this = $(this);
        var option = $this.data('availability');
        var day = $this.data('day');
        var rows = table.find('tr[data-day="' + day + '"]');
        var list = table.find('tr[data-day="' + day + '"].actions .options');
        var ids = [];
        if ($this.hasClass('whole-day')) {
          $('tr[data-day="' + day + '"] .availability-cell').css('background-color', '').removeClass('selected').click();
          $this.addClass('active whole-day-cancel');
          $this.removeClass('whole-day');
        } else if ($this.hasClass('whole-day-cancel')) {
          $('tr[data-day="' + day + '"] .availability-cell.selected').click();
          $this.addClass('whole-day');
          $this.removeClass('active whole-day-cancel');
          list.addClass('hidden');
        } else {
          rows.each(function(i, row) {
            var hours = getSelectedHours(row);
            if (hours.length) {
              $.each(hours, function(i, e) {
                ids.push(e);
              });
            }
          });
          saveAvailabilities(ids, option, function() {
            list.addClass('hidden');
            updateCells(ids, option);
          });
        }

      });
      showList = function(day) {
        var show = false;
        var rows = table.find('tr[data-day="' + day + '"]');
        var row = table.find('tr[data-day="' + day + '"]:first');
        var list = table.find('tr[data-day="' + day + '"].actions .options');
        var ids = [];
        rows.each(function(i, row) {
          var hours = getSelectedHours(row);
          if (hours.length && !show) {
            show = true;
          }
        });
        if (show) {
          list.removeClass('hidden');
        } else {
          list.addClass('hidden');
        }
      }
      getSelectedHours = function(row) {
        var hours = [];
        var selected = $(row).find('.selected');
        if (typeof selected !== 'undefined') {
          $.each(selected, function(i, e) {
            hours.push($(e).data('id'));
          });
        }
        return hours;
      }
      saveAvailabilities = function(ids, option, callback) {
        $.ajax({
          url: basePath + '/schedule_days/update',
          type: 'post',
          dataType: 'json',
          data: {
            ids: ids,
            availability: option
          },
          success: function(data) {
            toastr.success('Les disponibilités ont été correctement sauvegardés.');
          },
          error: log_error
        }).then(function() {
          if (callback) {
            callback();
          }
        });
      }
      updateCells = function(ids, availability) {
        $.each(ids, function(i, e) {
          var cell = $('.table-availabilities tr td.availability-cell[data-id="' + e + '"]');
          cell.removeClass('selected');
          cell.removeClass('success warning danger bg-grey');
          if (availability == 'yes') cell.addClass('success');
          if (availability == 'maybe') cell.addClass('warning');
          if (availability == 'no') cell.addClass('danger');
          if (availability == 'unknown') cell.addClass('bg-grey');
        });
      }
      updateCellColor = function(cell, mode) {
        return;
        var rgbString = cell.css('background-color'); // get this in whatever way.
        var parts = rgbString.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
        delete(parts[0]);
        for (var i = 1; i <= 3; ++i) {
          parts[i] = parseInt(parts[i]).toString(16);
          if (parts[i].length == 1) parts[i] = '0' + parts[i];
        }
        var color = '#' + parts.join('').toUpperCase();
        if (mode == 'dark') cell.css('background-color', shadeColor(color, -15));
        if (mode == 'light') cell.css('background-color', '');
      }
      shadeColor = function(color, percent) {

        var R = parseInt(color.substring(1, 3), 16);
        var G = parseInt(color.substring(3, 5), 16);
        var B = parseInt(color.substring(5, 7), 16);

        R = parseInt(R * (100 + percent) / 100);
        G = parseInt(G * (100 + percent) / 100);
        B = parseInt(B * (100 + percent) / 100);

        R = (R < 255) ? R : 255;
        G = (G < 255) ? G : 255;
        B = (B < 255) ? B : 255;

        var RR = ((R.toString(16).length == 1) ? "0" + R.toString(16) : R.toString(16));
        var GG = ((G.toString(16).length == 1) ? "0" + G.toString(16) : G.toString(16));
        var BB = ((B.toString(16).length == 1) ? "0" + B.toString(16) : B.toString(16));

        return "#" + RR + GG + BB;
      }
    }
  };

  var handleCheckAll = function() {
    $('#EmailUserCheckAll').change(function() {
      if ($(this).is(':checked')) {
        $('input[type="checkbox"]').attr('checked', 'checked');
        $('input[type="checkbox"]').parents('span').addClass('checked');
      } else {
        $('input[type="checkbox"]').removeAttr('checked');
        $('input[type="checkbox"]').parents('span').removeClass('checked');
      }
    });
  };

  var mapMarker = function() {
    var div = $('#gmap_marker');
    var lat = div.data('lat');
    var lng = div.data('lng');
    var title = div.data('title');
    var map = new GMaps({
      div: '#gmap_marker',
      lat: lat,
      lng: lng,
    });
    map.addMarker({
      lat: lat,
      lng: lng,
      title: 'Lima'
    });
    map.setZoom(16);
  };

  var handleDatatable = function(table, aTargets, mColumns, sort, defaultSort) {

    if (table.length > 0) {
      if (typeof(sort) == 'undefined') sort = true;

      TableTools.DEFAULTS.aButtons = ["pdf"];
      TableTools.DEFAULTS.sSwfPath = "../TableTools-2.0.0/media/swf/copy_cvs_xls_pdf.swf";

      var id = table.attr('id');
      $.extend(true, $.fn.DataTable.TableTools.classes, {
        "container": "btn-group tabletools-dropdown-on-portlet",
        "buttons": {
          "normal": "btn default",
          "disabled": "btn btn-sm default disabled"
        },
        "collection": {
          "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
        }
      });

      var oTable = table.DataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
          "aria": {
            "sortAscending": ": activer pour trier la colonne par ordre croissant",
            "sortDescending": ": activer pour trier la colonne par ordre décroissant"
          },
          "emptyTable": "Aucune donnée disponible dans le tableau",
          "info": "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
          "infoEmpty": "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
          "infoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
          "lengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
          "search": "Rechercher&nbsp;:",
          "zeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher"
        },
        "lengthMenu": [
          [5, 15, 20, -1],
          [5, 15, 20, "All"] // change per page values here
        ],
        "aoColumnDefs": [{
          'bSortable': false,
          'aTargets': aTargets
        }],
        // set the initial value
        "pageLength": 20,
        "dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        "tableTools": {
          "sSwfPath": basePath + "/App/theme/assets/global/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf",
          "aButtons": [{
            "sExtends": "pdf",
            "sButtonText": "Exporter en PDF",
            "mColumns": mColumns
          }]
        },
        bSort: sort,
        order: defaultSort
      });

      var tableWrapper = $('#' + id + '_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

      tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
    }

  };

  var initDatatables = function(tabsDiv) {
    tabsDiv.tabs({
      activate: function(event, ui) {
        ttInstances = TableTools.fnGetMasters();
        for (i in ttInstances) {
          if (ttInstances[i].fnResizeRequired()) ttInstances[i].fnResizeButtons();
        }
      }
    });
  };

  var handleCompetencesJobs = function(controller) {
    $('body').on('change', 'select.sector', function() {
      var portlet = $(this).parents('.portlet.element');
      var selectedValue = $(this).val();
      var selectedOption = $(this).find('option:selected').text();

      portlet.find('select:not(.sector)').addClass('hidden');
      portlet.find('select:not(.sector)').val('');
      portlet.find('.caption span').text('');
      portlet.find('span.sector').text(selectedOption);
      portlet.find('.col-md-3.activities').addClass('hidden');

      if (selectedValue == 'animation') portlet.find('.animation_jobs').removeClass('hidden');
      if (selectedValue == 'logistics') portlet.find('.logistics_jobs').removeClass('hidden');
      if (selectedValue == 'fb') portlet.find('.fb_jobs').removeClass('hidden');

      saveElement(portlet, controller);
    });
    $('body').on('change', 'select.jobs', function() {
      var portlet = $(this).parents('.portlet.element');
      var selectedValue = $(this).val();
      var selectedOption = $(this).find('option:selected').text();
      var selectedSector = portlet.find('select.sector').val();

      portlet.find('span.job').text(selectedOption);

      if (selectedSector == 'animation') {
        portlet.find('.col-md-3.activities, .col-md-2.activities').removeClass('hidden');
        if (selectedValue == 'urban_leader' || selectedValue == 'urban_coach') {
          portlet.find('.ubicActivities').addClass('hidden').val('');
          portlet.find('.ugActivities').removeClass('hidden');
        } else if (selectedValue == 'animator' || selectedValue == 'animator_facilitator') {
          portlet.find('.ubicActivities').removeClass('hidden');
          portlet.find('.ugActivities').addClass('hidden').val('');
        } else {
          portlet.find('.col-md-3.activities').addClass('hidden');
          portlet.find('.ubicActivities').addClass('hidden').val('');
          portlet.find('.ugcActivities').addClass('hidden').val('');
          portlet.find('.hierarchies').removeClass('hidden');
        }
      } else {
        portlet.find('.hierarchies').removeClass('hidden');
      }
      saveElement(portlet, controller);
    });
    $('body').on('change', '.ugActivities, .ubicActivities', function() {
      var portlet = $(this).parents('.portlet.element');
      var selectedValue = $(this).val();
      var selectedOption = $(this).find('option:selected').text();

      portlet.find('span.activity').text(selectedOption);
      portlet.find('.hierarchies').removeClass('hidden');

      saveElement(portlet, controller);
    });
    $('body').on('change', '.hierarchies', function() {
      var portlet = $(this).parents('.portlet.element');
      var selectedValue = $(this).val();
      var selectedOption = $(this).find('option:selected').text();

      portlet.find('span.hierarchy').text(selectedOption);

      saveElement(portlet, controller);
      if (controller == 'jobs') {
        portlet.find('.salaries').removeClass('hidden');
      }
    });
    $('body').on('change', 'input.start_time, input.end_time, input.remarks, input.salaries, select.languages', function() {
      var portlet = $(this).parents('.portlet.element');
      saveElement(portlet, controller);
    });
    $('body').on('change', '.user_id', function() {
      var portlet = $(this).parents('.portlet.element');
      portlet.find('.salaries').val('');
      saveElement(portlet, controller);
    });
    $('body').on('click', '.add-element', function(event) {
      event.preventDefault();
      var portletClone = $('.portlet.element:last').clone();
      portletClone.find('select:not(.sector)').addClass('hidden');
      portletClone.find('select').val('');
      portletClone.find('select option').removeAttr('selected');
      portletClone.find('input.id').val('');
      portletClone.find('input.salaries').val('').addClass('hidden');
      portletClone.find('span.salaries').addClass('hidden');
      portletClone.find('.caption span').text('');
      portletClone.appendTo('#elements');
    });
    $('body').on('click', '.remove-element', function(event) {
      event.preventDefault();
      var portlet = $(this).parents('.portlet.element');
      var id = portlet.find('input.id').val();
      bootbox.confirm("Are you sure you want to delete this element?", function(result) {
        if (result) {
          removeElement(id, portlet, controller);
        }
      });
    });

    var removeElement = function(id, portlet, controller) {
      portlet.find('.fa-spin').removeClass('hidden');

      $.ajax({
        url: basePath + '/' + controller + '/delete',
        type: 'post',
        dataType: 'json',
        data: {
          id: id
        },
        success: function(data) {
          if (data.success != 0) {
            toastr.success('Element has been correctly removed.');
            portlet.fadeOut();
          } else {
            toastr.error('Something wrong happened. Please try again.');
          }
          portlet.find('.fa-spin').addClass('hidden');
        },
        error: log_error
      });
    };

    var saveElement = function(portlet, controller) {
      if (portlet.find('select.hierarchies option:selected').val()) {

        var id = portlet.find('input.id').val();
        var user_id = portlet.find('input.user_id').val();
        var event_id = portlet.find('input.event_id').val();
        var sector = portlet.find('select.sector option:selected').val();
        var job = portlet.find('select.jobs option[value!=""]:selected').val();
        var hierarchy = portlet.find('select.hierarchies option[value!=""]:selected').val();
        var activity = portlet.find('.activities select option[value!=""]:selected').val();
        var name = portlet.find('.caption span.sector').text() + ' ';
        name += portlet.find('.caption span.job').text() + ' ';
        name += portlet.find('.caption span.activity').text() + ' ';
        name += portlet.find('.caption span.hierarchy').text();
        var model = portlet.find('input.model').val();
        var amount = portlet.find('input.amount').val();
        var start_time = portlet.find('input.start_time').val();
        var salary = portlet.find('input.salaries').val();
        var end_time = portlet.find('input.end_time').val();
        var remarks = portlet.find('input.remarks').val();
        var worker_id = portlet.find('select.user_id option[value!=""]:selected').val();
        var languages = [];
        portlet.find('select.languages option').each(function(i, e) {
          if ($(e).is(':selected')) {
            languages.push($(e).val());
          }
        })

        portlet.find('.fa-spin').removeClass('hidden');

        $.ajax({
          url: basePath + '/' + controller + '/save',
          type: 'post',
          dataType: 'json',
          data: {
            id: id,
            user_id: user_id,
            event_id: event_id,
            sector: sector,
            job: job,
            hierarchy: hierarchy,
            activity: activity,
            name: name,
            amount: amount,
            start_time: start_time,
            end_time: end_time,
            remarks: remarks,
            worker_id: worker_id,
            salary: salary,
            languages: languages,
          },
          success: function(data) {
            if (data.success != 0) {
              if (controller == 'competences') {
                toastr.success('Competence has been saved.');
                portlet.find('input.id').val(data.id);
              }
              if (controller == 'jobs') {
                toastr.success('Job has been saved.');
                portlet.find('input.id').val(data.id);
                portlet.find('input.salaries').val(data.salary);
              }
            } else {
              toastr.error('Something wrong happened. Please try again.');
            }
            portlet.find('.fa-spin').addClass('hidden');
          },
          error: log_error
        });
      } else {
        return false;
      }
    }
  };

  var handleMoments = function(controller) {
    $('body').on('change', 'input.name', function() {
      var portlet = $(this).parents('.portlet.moment');
      var selectedValue = $(this).val();
      portlet.find('span.name').text(selectedValue);
      saveMoment(portlet, controller);
    });
    $('body').on('change', 'input.start_hour', function() {
      toastr.clear();
      var portlet = $(this).parents('.portlet.moment');
      var selectedValue = $(this).val();
      portlet.find('span.start_hour').text(selectedValue);
      saveMoment(portlet, controller);
    });
    $('body').on('change', 'input.end_hour', function() {
      toastr.clear();
      var portlet = $(this).parents('.portlet.moment');
      var selectedValue = $(this).val();
      portlet.find('span.end_hour').text(selectedValue);
      saveMoment(portlet, controller);
    });
    $('body').on('change', 'input.remarks', function() {
      toastr.clear();
      var portlet = $(this).parents('.portlet.moment');
      var selectedValue = $(this).val();
      saveMoment(portlet, controller);
    });
    $("#MomentPlaces").on("select2-selecting", function(e, choice) {
      var portlet = $(this).parents('.portlet.moment');
      portlet.find('.place_id').val(e.val);
      saveMoment(portlet, controller);
    })
    $('body').on('click', '.add-moment', function(event) {
      event.preventDefault();
      var portletClone = $('.portlet.moment.empty').clone();
      portletClone.find('select:not(.sector)').addClass('hidden');
      portletClone.find('select').val('');
      portletClone.find('select option').removeAttr('selected');
      portletClone.find('input.id, input.name, input.remarks').val('');
      portletClone.find('.caption span').text('');
      portletClone.appendTo('#moments1');
    });
    $('body').on('click', '.remove-moment', function(event) {
      event.preventDefault();
      var portlet = $(this).parents('.portlet.moment');
      var id = portlet.find('input.id').val();
      bootbox.confirm("Are you sure you want to delete this moment?", function(result) {
        if (result) {
          removeMoment(id, portlet, controller);
        }
      });
    });

    var removeMoment = function(id, portlet, controller) {
      portlet.find('.fa-spin').removeClass('hidden');

      $.ajax({
        url: basePath + '/' + controller + '/delete',
        type: 'post',
        dataType: 'json',
        data: {
          id: id
        },
        success: function(data) {
          if (data.success != 0) {
            toastr.success('Element has been correctly removed.');
            portlet.fadeOut();
          } else {
            toastr.error('Something wrong happened. Please try again.');
          }
          portlet.find('.fa-spin').addClass('hidden');
        },
        error: log_error
      });
    };

    var saveMoment = function(portlet, controller) {
      if (portlet.find('input.name').val().length > 0) {

        var id = portlet.find('input.id').val();
        var event_id = portlet.find('input.event_id').val();
        var place_id = portlet.find('input.place_id').val();
        var name = portlet.find('input.name').val();
        var start_hour = portlet.find('input.start_hour').val();
        var end_hour = portlet.find('input.end_hour').val();
        var remarks = portlet.find('input.remarks').val();

        portlet.find('.fa-spin').removeClass('hidden');

        $.ajax({
          url: basePath + '/' + controller + '/save',
          type: 'post',
          dataType: 'json',
          data: {
            id: id,
            event_id: event_id,
            place_id: place_id,
            name: name,
            start_hour: start_hour,
            end_hour: end_hour,
            remarks: remarks
          },
          success: function(data) {
            if (data.success != 0) {
              toastr.options = {
                "preventDuplicates": true,
              }
              toastr.success('Moment has been saved.');
              portlet.find('input.id').val(data.id);
            } else {
              toastr.error('Something wrong happened. Please try again.');
            }
            portlet.find('.fa-spin').addClass('hidden');
          },
          error: log_error
        });
      } else {
        return false;
      }
    }

  };

  var handleSpinners = function() {
    $('#spinner1').spinner();
  };

  var handleRecruit = function() {
    $('body').on('click', '.recruit, .relaunch', function(event) {
      event.preventDefault();

      var searchId = $(this).data('search-id');
      var fullName = $(this).data('full-name');
      var userId = $(this).data('user-id');
      var jobId = $(this).data('job-id');
      var relaunch = $(this).data('relaunch');
      var title;
      var href = $(this).attr('href');

      if (relaunch) {
        title = "Êtes-vous sûr de vouloir relancer " + fullName + "?";
      } else {
        title = "Êtes-vous sûr de vouloir recruter " + fullName + "?";
      }

      bootbox.dialog({
        title: title,
        message: '<div class="row">  ' +
          '<div class="col-md-12"> ' +
          '<form class="form-horizontal"> ' +
          '<div class="form-group"> ' +
          '<div class="col-md-12"> ' +
          '<textarea id="message" name="message" placeholder="Vous pouvez encore spécifier un message personnalisé." class="form-control"></textarea> ' +
          '</div>' +
          '</div>' +
          '</form> </div>  </div>',
        buttons: {
          cancel: {
            label: "Annuler",
            className: "default"
          },
          success: {
            label: "Confirmer",
            className: "btn-success",
            callback: function() {
              var message = $('#message').val();
              $('#loading').fadeIn();
              $.ajax({
                url: href,
                type: 'post',
                dataType: 'json',
                data: {
                  search_id: searchId,
                  user_id: userId,
                  job_id: jobId,
                  message: message
                },
                success: function(data) {
                  $('#loading').fadeOut();
                  if (data.success) {
                    toastr.success('Mail has been sent!');
                  } else {
                    toastr.error('Something wrong happened. Please try again.');
                  }
                  setTimeout(function() {
                    document.location.href = document.location.href;
                  }, 400)
                },
                error: function(request, errorType, errorText) {
                  $('#loading').fadeOut();
                  toastr.error('Something wrong happened. Please try again.');
                  console.log([
                    'Error Type: ' + errorType,
                    'Error Text: ' + errorText
                  ].join('\n'));
                }
              });
            }
          }
        }
      });
    });
  };

  var handleTravelTime = function() {
    $('span.travel').each(function(i, e) {
      var origin = $(e).data('origin');
      var destination = $(e).data('destination');
      $.ajax({
        url: basePath + '/search/getTravelTime',
        type: 'post',
        dataType: 'json',
        data: {
          origin: origin,
          destination: destination
        },
        success: function(data) {
          $(e).html(data);
        },
        error: log_error
      });
    });
  };

  var handleRiseUser = function() {
    $('body').on('click', '.riseUser', function(event) {
      event.preventDefault();
      var url = $(this).attr('href');
      var row = $(this).parents('tr');
      //$('#loading').fadeIn(100);
      $.ajax({
        url: url,
        dataType: 'json',
        type: 'post',
        success: function(data) {
          if (data.success) {
            //$('#loading').fadeOut(100);
            toastr.success('Competence has been upgraded!');
            row.css('opacity', '0.6').find('a.riseUser').hide();
          } else {
            toastr.error('Something wrong happened. Please try again.');
          }
        },
        error: log_error
      })
    });
  };

  var handleAddStockItem = function() {

    $('body').on('keydown', '#StockOrderAddStockItemQuantity', function(e) {
      if (e.which == 9) {
        e.preventDefault();
        $("#StockOrderAddStockItem").select2('open');
      }
    });
    $("#StockOrderAddStockItem").select2({
      placeholder: "Rechercher un produit",
      minimumInputLength: 1,
      ajax: {
        url: basePath + '/stock_items/json',
        dataType: 'json',
        type: 'POST',
        cache: true,
        data: function(params) {
          // get quantity of wanted stock item
          if ($(this).parents('.form-group').find('#StockOrderAddStockItemQuantity').val().length > 0) {
            var quantity = $(this).parents('.form-group').find('#StockOrderAddStockItemQuantity').val();
          } else {
            var quantity = 1;
          }

          // get delivery date
          var delivery_date = $('#StockOrderDeliveryDate').val();
          // get return date
          var return_date = $('#StockOrderReturnDate').val();

          return {
            term: params.term,
            delivery_date: delivery_date,
            return_date: return_date,
            quantity: quantity,
            show_off: false,
            company_id: 3
          };
        },
        processResults: function(data, page) {
          return {
            results: data
          };
        }
      },
      formatResult: format
    }).on("select2:selecting", function(e) {
      var product = e.params.args.data;
      var deliveryDate = $('#StockOrderDeliveryDate').val().split('-');
      var returnDate = $('#StockOrderReturnDate').val().split('-');

      var date1 = new Date(deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2]);
      var date2 = new Date(returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2]);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      var serviceBeginDate = $('#StockOrderServiceDateBegin').val().split('-');
      var serviceEndDate = $('#StockOrderServiceDateEnd').val().split('-');

      var date3 = new Date(serviceBeginDate[1] + '/' + serviceBeginDate[0] + '/' + serviceBeginDate[2]);
      var date4 = new Date(serviceEndDate[1] + '/' + serviceEndDate[0] + '/' + serviceEndDate[2]);
      var timeDiff1 = Math.abs(date4.getTime() - date3.getTime());
      var diffDays1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24));

      var coefficient = 1;

      var numberOfRows = $('#stockitemsTable tbody tr:not(.empty)').get().length;

      if (diffDays1 > 0 && diffDays1 <= 2) {
        coefficient = 1;
      } else if (diffDays1 > 2 && diffDays1 <= 9) {
        coefficient = 1.3;
      } else if (diffDays1 > 9 && diffDays1 <= 16) {
        coefficient = 1.6;
      } else if (diffDays1 > 16 && diffDays1 <= 23) {
        coefficient = 1.9;
      } else if (diffDays1 > 23) {
        coefficient = 2.2;
      }

      var line = $('#stockitemsTable tr.empty').clone().removeClass('hidden').removeClass('empty');
      line.find('td.quantity span').text(product.quantity);
      line.find('td.code').text(product.code);
      line.find('td.stockitem').html(product.name + '<br>' + product.section);
      line.find('td.price').text(product.price);
      line.find('td.priceWithDisount').text(product.price);
      line.find('td.coefficient input').val(coefficient);
      line.find('input.quantity').val(product.quantity);
      line.find('td.quantity span.help-text').html(' / ' + product.totalQuantity)
      line.find('input.price').val(product.price);
      line.find('input.subject_to_discount').val(product.subject_to_discount);
      line.find('input.weight').val(numberOfRows);
      line.find('.actions span').text(numberOfRows + 1);
      line.find('input.stock_item_id').val(product.id);
      line.insertBefore($('#stockitemsTable tbody tr.empty'));

      checkAvailability(product.id, product.quantity, date1, diffDays, line, null);

    }).on('select2:close', function(e) {
      $("#StockOrderAddStockItem").select2('val', '');
      setTimeout(function() {
        $('.select2-container-active').removeClass('select2-container-active');
        $(':focus').blur();
        $('#StockOrderAddStockItemQuantity').focus().select();
        computeTotal(true);
      }, 10);
      searchForDuplicates();
    });

    function format(product) {
      return product.text;
    }



    $('body').on('click', '.remove-stock-item', function(event) {
      event.preventDefault();
      var $this = $(this);
      var line = $this.parents('tr');
      var batchId = line.find('#StockOrderBatchId').val().length ? line.find('#StockOrderBatchId').val() : 0;

      bootbox.confirm("Are you sure you want to delete this item?", function(result) {
        if (result) {
          line.fadeOut(function() {
            line.remove();
          });
          if (batchId) {
            var batchesToDelete = $('#StockOrderBatchesToDelete').val().length ? $('#StockOrderBatchesToDelete').val().split(',') : [];
            if (batchesToDelete) {
              batchesToDelete.push(batchId);
              $('#StockOrderBatchesToDelete').val(batchesToDelete.join());
            }
          }
          setTimeout(function() {
            $('#stockitemsTable tbody tr').each(function(i, e) {
              $(e).find('input.weight').val(i);
              $(e).find('.actions span').text(i + 1);
            });
            computeTotal(true);
            searchForDuplicates();
          }, 400);
        }
      });

    });

    $('body').on('change', '.coefficient, .discount, .quantity, .price', function(event) {
      computeTotal(true);
    });

    $('body').on('change', 'input.quantity', function(event) {
      var line = $(this).parents('tr');
      var stockItemId = line.find('.stock_item_id').val();
      var amount = line.find('input.quantity').val();

      var deliveryDate = $('#StockOrderDeliveryDate').val().split('-');
      var returnDate = $('#StockOrderReturnDate').val().split('-');

      var date1 = new Date(deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2]);
      var date2 = new Date(returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2]);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      checkAvailability(stockItemId, amount, date1, diffDays, line, null);
    });

    var computeTotal = function(updateRepartition) {
      if ($('#StockOrderEditForm').length) {
        var total = 0;
        var prices = $('#stockitemsTable tbody tr:not(.empty):not(.free)').each(function(i, e) {
          var price = $(e).find('input.price').val().length > 0 ? parseFloat($(e).find('input.price').val()) : 0;
          var quantity = $(e).find('input.quantity').val().length > 0 ? parseFloat($(e).find('input.quantity').val()) : 1;
          var coefficient = $(e).find('input.coefficient').val().length > 0 ? parseFloat($(e).find('input.coefficient').val()) : 0;
          var discount = $(e).find('input.discount').val().length > 0 ? parseFloat($(e).find('input.discount').val()) : 0;
          var discountAmount = quantity * price * coefficient * discount / 100;
          var netHt = parseFloat(quantity * price * coefficient).toFixed(2);
          var totalTtc = parseFloat(netHt - (netHt * discount / 100)).toFixed(2);
          $(e).find('td.net_ht').text(netHt);
          $(e).find('td.total_ttc').text(totalTtc);
          if (!isNaN(price)) total += quantity * price * coefficient - discountAmount;
        });
        var stockOrderTotal = roundAmount(total);
        $('#StockOrderTotalHt').val(stockOrderTotal).trigger('change');
        $('.total_ht span').text(stockOrderTotal);
        var stockOrderType = $('#StockOrderType').val();
        if (updateRepartition) {
          if (stockOrderType == 'sale') {
            $('#StockOrderRepartitionOther').val(stockOrderTotal);
          } else {
            $('#StockOrderRepartitionRental').val(stockOrderTotal);
          }
        }

      }
    };

    if (document.location.href.indexOf('stock_orders/edit/') > 0) {
      computeTotal();
    }

    $('.form-actions button[type="submit"]').on('click', function(e) {
      var $this = $(this);
      var destination = $this.attr('value');
      e.preventDefault();
      computeTotal();
      $('#StockOrderDestination').val(destination);
      setTimeout(function() {
        $('#StockOrderEditForm, #StockOrderEditInvoiceForm').submit();
      }, 100);
    });

    var checkAvailability = function(stockItemId, amount, date, duration, line, stockOrderId) {

      line.find('.feedback ul').html('');
      line.find('.feedback .loader').removeClass('hidden');
      line.find('.feedback .text-success, .feedback .text-danger, .feedback .text-warning, .feedback .text-info').addClass('hidden');

      var formattedDate = convertDate(date);

      $.ajax({
        url: basePath + '/stock_items/checkAvailability',
        type: 'post',
        dataType: 'json',
        data: {
          stock_item_id: stockItemId,
          stock_order_id: stockOrderId,
          duration: duration,
          amount: amount,
          startDate: formattedDate
        },
        success: function(data) {
          line.find('.loader').addClass('hidden');
          if (data.no_quantity == 1) {
            line.find('.text-info').removeClass('hidden');
          } else if (!data.sufficientStock) {
            line.find('.text-danger:not(.text-archived)').removeClass('hidden');
            if (data.available < 0) {
              data.available = 0;
            }
            line.find('.text-danger span').text(' (disponible: ' + data.available + ')');
            var href = line.find('.text-danger').attr('href');
            line.find('.text-danger').attr('href', href + '/' + stockItemId + '/' + amount + '/' + formattedDate + '/' + duration);
          } else if (data.emptyStock && data.sufficientStock) {
            line.find('.text-warning').removeClass('hidden');
            var href1 = line.find('.text-warning').attr('href');
            line.find('.text-warning').attr('href', href1 + '/' + stockItemId + '/' + amount + '/' + formattedDate + '/' + duration);
          } else {
            line.find('.text-success').removeClass('hidden');
          }

          if (line.find('.archived').val() == 1) {
            line.addClass('danger');
            line.find('.text-success').addClass('hidden');
            line.find('.text-danger').addClass('hidden');
            line.find('.text-warning').addClass('hidden');
            line.find('.text-archived').removeClass('hidden');
          }
        }
      });
    };

    var searchForDuplicates = function() {
      var stock_item_ids = [];
      var warning = false;
      $('#stockitemsTable tbody tr:not(.empty)').each(function(i, e) {
        $(e).removeClass('warning');
        var stockItemId = $(e).find('#StockOrderBatchStockItemId').val();
        if (stock_item_ids.indexOf(stockItemId) > -1) {
          if ($(e).find('td.code').text() != '') {
            warning = true;
            $(e).addClass('warning');
            $('#stockitemsTable tbody tr:not(.empty) .stock_item_id[value="' + stockItemId + '"]').parents('tr').addClass('warning');
          }
        } else {
          stock_item_ids.push(stockItemId);
        }
      })
      if (warning) {
        toastr.warning('Des produits sont à double dans la commande!');
      }
    }

    var searchForEmptyCoefficients = function() {
      var warning = false;
      $('#stockitemsTable tbody tr:not(.empty, .free)').each(function(i, e) {
        $(e).removeClass('danger');
        var coefficient = $(e).find('input.coefficient').val();
        if (coefficient == 0) {
          warning = true;
          $(e).addClass('danger');
        }
      })
      if (warning) {
        toastr.error('Des produits ont un coefficient à 0!');
      }
    };

    $('#stockitemsTable tbody tr:not(.empty)').each(function(i, e) {
      var stockItemId = $(e).find('#StockOrderBatchStockItemId').val();
      var stockOrderId = $('#StockOrderId').val();
      var amount = $(e).find('input.quantity').val();
      var date = $(e).find('input.quantity').val();

      var deliveryDate = $('#StockOrderDeliveryDate').val().split('-');
      var returnDate = $('#StockOrderReturnDate').val().split('-');

      var date1 = new Date(deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2]);
      var date2 = new Date(returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2]);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      checkAvailability(stockItemId, amount, date1, diffDays, $(e), stockOrderId);
    });

    searchForDuplicates();
    searchForEmptyCoefficients();

    $('body').on('click', '.add-line', function(event) {
      event.preventDefault();

      var deliveryDate = $('#StockOrderDeliveryDate').val().split('-');
      var returnDate = $('#StockOrderReturnDate').val().split('-');

      var date1 = new Date(deliveryDate[1] + '/' + deliveryDate[0] + '/' + deliveryDate[2]);
      var date2 = new Date(returnDate[1] + '/' + returnDate[0] + '/' + returnDate[2]);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

      var serviceBeginDate = $('#StockOrderServiceDateBegin').val().split('-');
      var serviceEndDate = $('#StockOrderServiceDateEnd').val().split('-');

      var date3 = new Date(serviceBeginDate[1] + '/' + serviceBeginDate[0] + '/' + serviceBeginDate[2]);
      var date4 = new Date(serviceEndDate[1] + '/' + serviceEndDate[0] + '/' + serviceEndDate[2]);
      var timeDiff1 = Math.abs(date4.getTime() - date3.getTime());
      var diffDays1 = Math.ceil(timeDiff1 / (1000 * 3600 * 24));

      var coefficient = 1;

      var weight = parseInt($('#stockitemsTable tbody tr:not(.empty, .free)').get().length);
      var numberOfRows = parseInt($('#stockitemsTable tbody tr:not(.empty, .free)').get().length) + 1;

      if (diffDays1 > 0 && diffDays1 <= 2) {
        coefficient = 1;
      } else if (diffDays1 > 2 && diffDays1 <= 9) {
        coefficient = 1.3;
      } else if (diffDays1 > 9 && diffDays1 <= 16) {
        coefficient = 1.6;
      } else if (diffDays1 > 16 && diffDays1 <= 23) {
        coefficient = 1.9;
      } else if (diffDays1 > 23) {
        coefficient = 2.2;
      }

      var line = $('#stockitemsTable tr.free').clone().removeClass('hidden').removeClass('free');
      line.find('input.id').val('');
      line.find('td.coefficient input').val(coefficient);
      line.find('td.stockitem input').val('Ligne ' + numberOfRows);
      line.find('input.weight').val(weight);
      line.find('.actions span').text(numberOfRows);
      line.insertBefore($('#stockitemsTable tbody tr.empty'));
      // setTimeout(function(){
      //   ajaxStockOrderSave.abort();
      //   stockOrderSave();
      // }, 20);
    });

  };

  var handleGetLatLngCommune = function() {
    $('#CommuneZip').blur(function() {
      if ($('#CommuneZip').val() && $('#CommuneName').val()) {
        $('#CommuneLatitude, #CommuneLongitude').addClass('spinner');
        $.ajax({
          url: basePath + '/communes/getLatLng',
          data: {
            city: $('#CommuneName').val(),
            zip: $('#CommuneZip').val()
          },
          type: 'POST',
          dataType: 'json',
          success: function(data) {
            $('#CommuneLatitude').val(data.latitude);
            $('#CommuneLongitude').val(data.longitude);
            $('#CommuneLatitude, #CommuneLongitude').removeClass('spinner');
          }
        });
      }
    });
  };

  var handleDocumentsActions = function() {
    $('body').on('click', '#internalPhotosActions li a, #clientPhotosActions li a, #documentsActions li a', function(event) {
      event.preventDefault();
      var group = $(this).data('group');
      var docs = $('input.selectme:checked').get();

      if (docs.length > 0) {
        $.each(docs, function(i, e) {
          var id = $(e).data('id');
          var div = $(e).parents('.panel').find('small.group');
          $.ajax({
            url: basePath + '/documents/editGroup/' + id + '/' + group,
            type: 'POST',
            dataType: 'json',
            success: function(data) {
              $('input.selectme').attr('checked', false);
              $('input.selectme').parents('span').removeClass('checked');
              div.text(data.group);
              toastr.success('Document has been updated.');
            }
          });
        });
      } else {
        toastr.error('No document has been selected!');
      }
    })
  };

  var handleSelectCategory = function() {
    $('body').on('change', '#PriceListCategoryId', function() {
      var categoryId = $(this).val();
      $('#PriceListSectionId option').remove();

      if (categoryId.length > 0) {
        $.ajax({
          url: basePath + '/stock_items/getSections',
          type: 'POST',
          data: {
            stock_category_id: categoryId
          },
          dataType: 'json',
          success: function(data) {
            $('#PriceListSectionId').append('<option value="">Select an option</option>');
            $.each(data, function(i, e) {
              $('#PriceListSectionId').append('<option value="' + i + '">' + e + '</option>');
            });
          }
        });
      }
    });
  };

  var handleStockItemsImages = function() {
    $('.product .image').each(function(i, e) {
      var stockItemId = $(e).data('article-id');
      var div = $(e);
      $.ajax({
        url: basePath + '/stock_items/getImage/' + stockItemId,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            div.html(data.image);
          } else {
            div.html('<i class="fa fa-times"></i>');
          }
        }
      });
    });
  };

  var handleSectionsFamilies = function() {
    $('body').on('change', '#StockItemStockCategoryId', function() {
      var categoryId = $(this).val();
      $('#StockItemStockSectionId option').remove();
      $('#StockItemStockFamilyId option').remove();
      getSections(categoryId, '#StockItemStockSectionId');
    })
    $('body').on('change', '#StockItemStockSectionId', function() {
      var categoryId = $('#StockItemStockCategoryId').val();
      var sectionId = $(this).val();
      $('#StockItemStockFamilyId option').remove();
      getFamilies(categoryId, sectionId, '#StockItemStockFamilyId');
    })
  };

  var getSections = function(category, selectId) {
    return;
    $.ajax({
      url: basePath + '/stock_items/getSections',
      data: {
        stock_category_id: category
      },
      type: 'post',
      dataType: 'json',
      success: function(data) {
        if (data) {
          $.each(data, function(i, e) {
            $(selectId).append('<option value="' + i + '">' + e + '</option>');
          });
        }
      }
    });
  };
  var getFamilies = function(category, section, selectId) {
    return;
    $.ajax({
      url: basePath + '/stock_items/getFamilies',
      data: {
        stock_category_id: category,
        stock_section_id: section
      },
      type: 'post',
      dataType: 'json',
      success: function(data) {
        if (data) {
          $.each(data, function(i, e) {
            $(selectId).append('<option value="' + i + '">' + e + '</option>')
          });
        }
      }
    });
  };

  var handleAddStock = function() {

    $('body').on('change', '[data-company]', function() {
      var companyId = $(this).val();
      getCategories(companyId);
    });

    $('body').on('change', '[data-stock-category]', function() {
      var categoryId = $(this).val();
      getSections(categoryId);
    });

    $('body').on('change', '[data-stock-section]', function() {
      var sectionId = $(this).val();
      getFamilies(sectionId);
    });

    if (typeof $('[data-stock-section]').data('value') === 'undefined') {
      $('[data-company]').change();
    }

    function getCategories(companyId) {
      if ($('[data-stock-category]').length) {
        $('[data-stock-category] option').remove();
        $.ajax({
          url: basePath + '/stock_categories/json',
          data: {
            company_id: companyId
          },
          type: 'post',
          dataType: 'json',
          success: function(data) {
            if (data) {
              $.each(data, function(i, e) {
                $('[data-stock-category]').append('<option value="' + e.StockCategory.id + '">' + e.StockCategory.code_name + '</option>');
              });
            }
          }
        }).then(function() {
          $('[data-stock-category]').selectpicker('refresh').trigger('change');
        });
      } else {
        return;
      }
    }

    function getSections(categoryId) {
      if ($('[data-stock-section]').length) {
        $('[data-stock-section] option').remove();
        $.ajax({
          url: basePath + '/stock_sections/json',
          data: {
            stock_category_id: categoryId
          },
          type: 'post',
          dataType: 'json',
          success: function(data) {
            if (data) {
              $.each(data, function(i, e) {
                $('[data-stock-section]').append('<option value="' + e.StockSection.id + '">' + e.StockSection.code_name + '</option>');
              });
            }
          }
        }).then(function() {
          $('[data-stock-section]').selectpicker('refresh').trigger('change');
        });
      } else {
        return;
      }
    }

    function getFamilies(sectionId) {
      if ($('[data-stock-family]').length) {
        $('[data-stock-family] option').remove();
        $.ajax({
          url: basePath + '/stock_families/json',
          data: {
            stock_section_id: sectionId
          },
          type: 'post',
          dataType: 'json',
          success: function(data) {
            if (data) {
              $.each(data, function(i, e) {
                $('[data-stock-family]').append('<option value="' + e.StockFamily.id + '">' + e.StockFamily.code_name + '</option>');
              });
            }
          }
        }).then(function() {
          $('[data-stock-family]').selectpicker('refresh');
        });
      } else {
        return;
      }
    }

  }

  var handleSelectStockItem = function(select, company_id) {
    select.select2({
      minimumInputLength: 2,
      placeholder: 'Rechercher un produit par code / nom',
      ajax: {
        url: basePath + "/stock_items/get",
        dataType: 'json',
        type: "POST",
        quietMillis: 50,
        data: function(params) {
          return {
            company_id: company_id,
            term: params.term
          };
        },
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.StockItem.code + ' ' + item.StockItem.name,
                id: item.StockItem.id
              }
            })
          };
        }
      }
    }).on('select2:selecting', function(e) {
      window.location = basePath + '/stock_items/view/' + e.params.args.data.id;
    });
  };

  var handleDeleteStockItem = function() {

    $('body').on('click', '.deleteStockItem', function() {
      event.preventDefault();
      var redirect = $(this).attr('href');
      bootbox.confirm("Are you sure you want to delete this item?", function(result) {
        if (result) {
          window.location = redirect;
        }
      })
    });

  };

  var handleRadioButtons = function() {
    $('input.toggle[type="radio"]').each(function(i, e) {
      if ($(e).is(':checked')) {
        $(e).parents('.btn').addClass('active')
      }
    });
  };

  var handleContactPeople = function(clientInput, contactPeopleInput) {
    $('body').on('change', clientInput, function() {
      $(contactPeopleInput).find('option').remove();
      var client_id = $(this).val();
      $.ajax({
        url: basePath + '/clients/getContactPeoples',
        data: {
          client_id: client_id
        },
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data) {
            $.each(data, function(i, e) {
              $(contactPeopleInput).append('<option value="' + e.id + '">' + e.text + '</option>');
            })
            $(contactPeopleInput).selectpicker('refresh');
          }
          if (clientInput == '#StockOrderClientId') {
            $.ajax({
              url: basePath + '/clients/get',
              data: {
                client_id: client_id
              },
              type: 'post',
              dataType: 'json',
              success: function(data) {
                setTimeout(function() {
                  $('#StockOrderInvoiceAddress').val(data.Client.address);
                  $('#StockOrderInvoiceZip').val(data.Client.zip);
                  $('#StockOrderInvoiceCity').val(data.Client.city);
                  $('select#StockOrderInvoiceZipCity option').remove();
                  $('select#StockOrderInvoiceZipCity').append('<option value="9999">' + data.Client.zip_city + '</option>')
                  $('select#StockOrderInvoiceZipCity').val("9999").trigger('change');
                  if (data.Client.festiloc_fidelity_discount > 0) {
                    $('#StockOrderFidelityDiscount').val(1);
                    $('#StockOrderFidelityDiscount').attr('checked', 'checked');
                    $('#StockOrderFidelityDiscount').bootstrapSwitch('state', true, true);
                    $('#StockOrderFidelityDiscountPercentage').val(data.Client.festiloc_fidelity_discount).change();
                  }
                }, 50);
              }
            });
            $.ajax({
              url: basePath + '/stock_orders/getContactPeoples',
              type: 'post',
              dataType: 'json',
              data: {
                stock_order_id: $('#StockOrderId').val(),
                client_id: client_id
              },
              success: function(data) {
                if (data.update == 1) {
                  setTimeout(function() {
                    $('#StockOrderContactPeopleId option[value="' + data.contacts[0] + '"]').attr('selected', true);
                    $('#StockOrderDeliveryContactPeopleId option[value="' + data.contacts[1] + '"]').attr('selected', true);
                    $('#StockOrderReturnContactPeopleId option[value="' + data.contacts[2] + '"]').attr('selected', true);
                    $('.contact-people').selectpicker('refresh');
                  }, 300);
                }
              },
              error: log_error
            });
          }
        }
      });
    });
  };

  var handlePlacesResults = function() {

    $('button.date, button.moment').click(function(event) {
      event.preventDefault();
      if ($(this).hasClass('date')) {
        $('button.date').removeClass('active');
      }
      if ($(this).hasClass('moment')) {
        $('button.moment').removeClass('active');
      }
      $(this).addClass('active');
      $('.portlet.place').each(function(i, e) {
        checkOptions($(e));
      });
    });

    var numberOfPlaces = $('.portlet.place:not(.origin)').get().length;

    $('.portlet.place:not(.origin)').each(function(i, e) {
      App.blockUI({
        target: $(e).find('.portlet-body'),
        animate: true,
        overlayColor: 'none'
      });
      $.ajax({
        url: basePath + '/search/place/' + $(e).data('place-id'),
        success: function(data) {
          App.unblockUI($(e).find('.portlet-body'));
          $(e).find('.portlet-body').append(data);
          if ($(e).data('complex-search') == 1) {
            checkOptions($(e));
            computeTravelTime($(e));
          } else {
            $(e).find('span.distance').parents('li').hide();
            $(e).find('span.distanceUBIC').parents('li').hide();
          }
          if (typeof($('#SearchOrigin').val()) !== 'undefined' && $(e).data('complex-search') == 0) {
            computeTravelTime($(e));
            $(e).find('span.distanceUBIC').parents('li').show();
          }
          if (i == (numberOfPlaces - 1)) {
            $('.scroller').slimScroll({
              height: '300px',
              railVisible: false,
              railOpacity: 0.7,
              alwaysVisible: false,
              size: '5px'
            });
          }
        }
      })
    });

    if ($('#map').length > 0) {
      window.map = new google.maps.Map(document.getElementById('gmap_places'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      var bounds = new google.maps.LatLngBounds();
      var originLat = $('.portlet.origin').data('lat');
      var originLng = $('.portlet.origin').data('lng');

      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(originLat, originLng),
        map: map,
        icon: basePath + '/img/icons/icon_marker_green.png',
        zIndex: 0
      });
      bounds.extend(marker.position);
      var latLng = marker.getPosition(); // returns LatLng object
      map.setCenter(latLng); // setCenter takes a LatLng object
      //map.fitBounds(bounds);

      $('#map').css('height', 0);
      $('body').on('click', '.actions .btn', function(event) {
        $('.actions .btn').removeClass('active');
        $(this).addClass('active');
        event.preventDefault();
        var rel = $(this).attr('rel');
        if (rel == 'list') {
          $('#map').css('height', 0).hide();
          $('#list').show();
        }
        if (rel == 'map') {
          $('#map').css('height', 'auto').show();
          $('#list').hide();
        }
      })

      var listener = google.maps.event.addListener(map, "idle", function() {
        map.setZoom(10);
        google.maps.event.removeListener(listener);
        var infowindow = new google.maps.InfoWindow();
        $('.portlet.place:not(.origin)').each(function(i, e) {
          var lat = $(e).data('lat');
          var lng = $(e).data('lng');
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            title: $(e).data('place-id').toString()
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.close();
            var content = $('.portlet.place[data-place-id="' + marker.title + '"] .infowindow').html();
            infowindow.setContent(content);
            infowindow.open(map, marker);
          });
          bounds.extend(marker.position);
        })
        map.fitBounds(bounds);
      });
    }

    computeDistance = function(div) {
      var latitude = div.find('input.latitude').val();
      var longitude = div.find('input.longitude').val();
      var communeLatitude = $('#SearchLatitude').val();
      var communeLongitude = $('#SearchLongitude').val();
      $.ajax({
        url: basePath + '/search/getDistance',
        dataType: 'json',
        type: 'post',
        data: {
          latA: latitude,
          lngA: longitude,
          latB: communeLatitude,
          lngB: communeLongitude
        },
        success: function(distance) {
          div.find('span.distance').text(distance);
        }
      });
    }

    computeTravelTime = function(div) {
      var destination = div.find('input.destination').val();
      var origin = $('#SearchOrigin').val();
      if (destination !== '++') {
        $.ajax({
          url: basePath + '/search/getTravelTime',
          dataType: 'json',
          type: 'post',
          data: {
            destination: destination,
            origin: origin
          },
          success: function(infos) {
            if (infos.time == 0 && infos.distance == 0) {
              div.find('span.time').text('non disponible');
              div.find('span.distance').hide();
            } else {
              div.find('span.time').text(' - ' + infos.time);
              div.find('span.distance').text(infos.distance);
            }
            if (infos.timeUBIC == 0 && infos.distanceUBIC == 0) {
              div.find('span.timeUBIC').text('non disponible');
              div.find('span.distanceUBIC').hide();
            } else {
              div.find('span.timeUBIC').text(' - ' + infos.timeUBIC);
              div.find('span.distanceUBIC').text(infos.distanceUBIC);
            }
          },
          error: log_error
        });
      } else {
        div.find('span.distance').text('Aucune adresse spécifiée!');
      }
    }

    removeEmptyTab = function(div) {
      if (typeof(div.find('.tab-pane.remarks .scroller').html()) !== "undefined") {
        if (div.find('.tab-pane.remarks .scroller').html().trim().length == 0) {
          div.find('ul.nav li.remarks').hide();
        }
      }
    }

    addMapMarker = function(div, map) {
      var latitude = div.find('input.latitude').val();
      var longitude = div.find('input.longitude').val();
      var title = div.find('strong.title').text();
      var id = div.data('place-id');
      var info = $('.infowindow[rel="' + id + '"]').html();

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map,
        title: title
      });

      bounds.extend(marker.position);
      google.maps.event.addListener(marker, 'click', (function(marker, id) {
        return function() {
          infowindow.setContent(info);
          infowindow.open(map, marker);
        }
      })(marker, id));
    }

    checkOptions = function(div) {

      div.find('.options label').removeClass('active');
      div.find('input.option_id').val('');

      var info = div.find('p.text-primary');
      var warning = div.find('p.text-warning');
      info.hide();
      warning.hide();

      var selectedPlace = div.data('place-id');
      var selectedDate = $('button.date.active').data('date');
      var selectedMoment = $('button.moment.active').data('moment-id');
      var selectedEvent = $('select#SearchEventId option:selected').val();

      if (selectedDate && selectedPlace) {
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: basePath + '/options/getInfos',
          data: {
            place_id: selectedPlace,
            date: selectedDate,
            moment_id: selectedMoment
          },
          success: function(data) {
            if (typeof(data.Moment) == 'undefined') {
              data.Moment = {};
              data.Moment.id = 0;
            }
            if (selectedMoment == data.Moment.id) {
              div.find('label.' + data.Option.value).addClass('active');
              div.find('input.option_id').val(data.Option.id);
              info.show();
              info.find('.user').text(data.User.name);
              info.find('.event').text(data.Event.name);
              info.find('.client').text(data.Client.name);
              info.find('.moment').text(data.Moment.name);
              info.find('.start_hour').text(data.Moment.start_hour);
              info.find('.end_hour').text(data.Moment.end_hour);
            } else if (data.overlap == 1 && data.empty == 0 && selectedMoment != data.Moment.id) {
              div.find('label.' + data.Option.value).addClass('active');
              div.find('input.option_id').val(data.Option.id);
              warning.show();
              warning.find('.user').text(data.User.name);
              warning.find('.event').text(data.Event.name);
              warning.find('.client').text(data.Client.name);
              warning.find('.moment').text(data.Moment.name);
              warning.find('.start_hour').text(data.Moment.start_hour);
              warning.find('.end_hour').text(data.Moment.end_hour);
            } else if (data.overlap == 0 && data.empty == 0 && selectedMoment != data.Moment.id) {
              div.find('label.free').addClass('active');
              div.find('input.option_id').val(data.Option.id);
              warning.show();
              warning.find('.user').text(data.User.name);
              warning.find('.event').text(data.Event.name);
              warning.find('.client').text(data.Client.name);
              warning.find('.moment').text(data.Moment.name);
              warning.find('.start_hour').text(data.Moment.start_hour);
              warning.find('.end_hour').text(data.Moment.end_hour);
            } else if (data.empty == 1) {
              div.find('label.free').addClass('active');
            }
            if (data.Option.value == 'not_interested') {
              var place = div.parents('.col-md-6').detach();
              place.removeClass('col-md-6').addClass('col-md-4');
              place.find('.portlet').css('height', 'auto');
              place.find('.portlet-body').remove();
              place.find('.portlet-title ul').remove();
              place.appendTo('#notInterestingPlaces');
            }
          },
          error: log_error
        });
      }

    }

    $('body').on('click', '.save-option', function(event) {
      event.preventDefault();
      var rel = $(this).attr('rel');
      var div = $('#' + rel);
      var activeInput = div.find('.options label.active input');
      var option = activeInput.data('value');
      var user_id = activeInput.data('user-id');
      var id = div.find('input.option_id').val();
      var place_id = div.data('place-id');
      var remarks = div.find('input.remarks').val();
      var valid_until = div.find('input.date-picker').val();
      var date = $('button.date.active').data('date');
      var moment_id = $('button.moment.active').data('moment-id');
      var event_id = $('select#SearchEventId option:selected').val();

      if (typeof(option) !== 'undefined') {
        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: basePath + '/options/save',
          data: {
            id: id,
            option: option,
            date: date,
            place_id: place_id,
            moment_id: moment_id,
            event_id: event_id,
            user_id: user_id,
            remarks: remarks,
            valid_until: valid_until
          },
          success: function(data) {
            if (data.success == 1) {
              $('#' + rel).find('input.option_id').val(data.id);
              toastr.success('Option has been saved.');
            }
          },
          error: log_error
        });
      } else {
        bootbox.alert("No option has been selected! Please select one.");
      }
    });

  };

  var handleSelectModuleCategory = function() {
    $('body').on('change', '#ModuleModuleCategoryId', function() {
      event.preventDefault();
      var categoryId = $(this).val();
      $('#ModuleModuleSubcategoryId option').remove();

      if (categoryId.length > 0) {
        $.ajax({
          url: basePath + '/modules/getSubcategories',
          type: 'POST',
          data: {
            module_category_id: categoryId
          },
          dataType: 'json',
          success: function(data) {
            $.each(data, function(i, e) {
              $('#ModuleModuleSubcategoryId').append('<option value="' + i + '">' + e + '</option>');
            });
            $('#ModuleModuleSubcategoryId').selectpicker('refresh');
          }
        });
      }
    });
  };

  var handleUserModules = function() {

    if ($('input.module.followed').length > 0) {
      $('input.module.followed')
        .bootstrapSwitch()
        .on('switchChange.bootstrapSwitch', function(event, state) {
          var line = $(this).parents('td');
          var module_id = $(this).data('module-id');
          var user_id = $(this).data('user-id');
          var module_user_id = $(this).parents('td').find('input.module-user-id-followed').val();
          $.ajax({
            url: basePath + '/modules/changeStatus/followed',
            type: 'POST',
            data: {
              user_id: user_id,
              module_id: module_id,
              status: state,
              module_user_id: module_user_id
            },
            dataType: 'json',
            success: function(data) {
              line.find('input.module-user-id-followed').val(data.ModuleUserId);
              toastr.success('Changes have been saved.')
            }
          });
        })
        .each(function(i, e) {
          var module_id = $(e).data('module-id');
          var user_id = $(e).data('user-id');
          $.ajax({
            url: basePath + '/modules/getStatus/followed',
            type: 'POST',
            data: {
              user_id: user_id,
              module_id: module_id
            },
            dataType: 'json',
            success: function(data) {
              if (data.checked === true) {
                $(e).bootstrapSwitch('state', true, true);
                $(e).parents('td').find('input.module-user-id-followed').val(data.ModuleUserId);
              }
            }
          });
        });
    }

    $('.suitable-modules').each(function(i, e) {
      var module_id = $(e).data('module-id');
      var user_id = $(e).data('user-id');
      $.ajax({
        url: basePath + '/modules/getStatus/suitable',
        type: 'POST',
        data: {
          user_id: user_id,
          module_id: module_id
        },
        dataType: 'json',
        success: function(data) {
          if (data.checked === true) {
            if (data.value == 'suitable') {
              $(e).find('.yes input').attr('checked', 'checked').parents('.btn').addClass('active');
            }
            if (data.value == 'not_suitable') {
              $(e).find('.no input').attr('checked', 'checked').parents('.btn').addClass('active');
            }
            if (data.value == 'maybe_suitable') {
              $(e).find('.maybe input').attr('checked', 'checked').parents('.btn').addClass('active');
            }
            $(e).find('.module-user-id-suitable').val(data.ModuleUserId);
          } else {
            $(e).find('.maybe input').attr('checked', 'checked').parents('.btn').addClass('active');
          }
        }
      });
    });
    $('.suitable-modules label').click(function(event) {
      var user_id = $(this).parents('.suitable-modules').data('user-id');
      var module_id = $(this).parents('.suitable-modules').data('module-id');
      var module_user_id = $(this).parents('.suitable-modules').find('.module-user-id-suitable').val();
      var value = $(this).find('input.module-user-id-suitable-input').val();
      var line = $(this).parents('.suitable-modules');

      $.ajax({
        url: basePath + '/modules/changeStatus/' + value,
        type: 'POST',
        data: {
          user_id: user_id,
          module_id: module_id,
          status: true,
          module_user_id: module_user_id
        },
        dataType: 'json',
        success: function(data) {
          line.find('input.module-user-id-suitable').val(data.ModuleUserId);
          toastr.success('Changes have been saved.')
        }
      });
    });
  };

  var handleUpdateOption = function() {
    $('body').on('change', '.update-option', function() {
      var selectedOption = $(this).val();
      var optionId = $(this).parents('tr').data('option-id');
      $.ajax({
        url: basePath + '/options/update',
        type: 'POST',
        dataType: 'json',
        data: {
          value: selectedOption,
          id: optionId
        },
        success: function(data) {
          if (data.success == 1) {
            toastr.success('The option for this moment has been saved.');
          } else {
            toastr.error('Something wrong happened. Please try again.');
          }
        },
        error: log_error
      });
    });
  };

  var initEventsCalendar = function() {

    var renderEventsCalendar = function(defaultView, defaultDate, callback) {

      var filters = [];
      // get all selected values of filters
      $('.filters select.bs-select').each(function(i, e) {
        var value = $(e).val();
        var field = $(e).data('field');
        filters.push({
          'field': field,
          'value': value
        });
      });

      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,agendaDay,agendaWeek,month'
      };

      $('#eventsCalendar').fullCalendar('destroy');
      $('#eventsCalendar').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        //scrollTime: "08:00:00",
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        eventSources: [{
          url: basePath + '/events/calendar.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('eventsCalendarDate'),
              view: localStorage.getItem('eventsCalendarView'),
              filters: filters
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          },
          success: function(data) {

          }
        }, {
          url: basePath + '/missions/calendar.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('eventsCalendarDate'),
              view: localStorage.getItem('eventsCalendarView'),
              filters: filters
            }
          },
          error: function() {
            alert('there was an error while fetching missions!');
          },
          success: function(data) {

          }
        }],
        eventRender: function(event, element) {
          element.attr('data-event-id', event.event_id);
          // element.attr('data-tooltip', 'tooltip');
          // element.attr('data-original-title', event.id);
          // element.attr('data-container', 'body');
          // element.attr('data-placement', 'top');
          element.attr('data-title', event.title);

          element.html(event.html);

          element.on('mouseenter', function() {
            var $this = $(this);
            var left = $this.css('left');
            var right = $this.css('right');
            var zindex = $this.css('z-index');
            var mright = $this.css('margin-right');

            $this.css('left', '0%');
            $this.css('right', '0%');
            $this.css('z-index', 1000);
            $this.css('margin-right', 0);

            $this.attr('left', left);
            $this.attr('right', right);
            $this.attr('z-index', zindex);
            $this.attr('margin-right', mright);
          });
          element.on('mouseleave', function() {
            var $this = $(this);
            $this.css('left', $this.attr('left'));
            $this.css('right', $this.attr('right'));
            $this.css('z-index', $this.attr('z-index'));
            $this.css('margin-right', $this.attr('margin-right'));
          });

          // element.attr('data-toggle', 'context');
          // element.attr('data-target', '#context');

        },
        eventClick: function(event) {
          if (event.url) {
            window.open(event.url);
            return false;
          }
        },
        loading: function(isLoading, view) {
          if (isLoading) {
            $('#loading').fadeIn();
          } else {
            $('#loading').fadeOut();
          }
        },
        eventAfterAllRender: function(view) {
          $('#loading').fadeOut();
          $("[data-tooltip='tooltip']").tooltip();
        },
        viewRender: function(view) {
          var formerView = localStorage.getItem('eventsCalendarView');
          localStorage.setItem('eventsCalendarView', view.type);
          localStorage.setItem('eventsCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderEventsCalendar('month', view.intervalStart.format('YYYY-MM-DD'));
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('eventsCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderEventsCalendar('basicWeek', localStorage.getItem('eventsCalendarDate'));
          }
        }
      });

    };

    $('#events_tabs a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      var href = $(e.target).attr('href');
      if (href == '#calendar') {
        $('#eventsCalendar').fullCalendar('render');
      }
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
      $('a[data-toggle="tab"]').removeClass('active');
      $(this).addClass('active');
    })

    if ($('#eventsCalendar').length) {
      var view = localStorage.getItem('eventsCalendarView');
      if (!view) view = 'agendaWeek';
      renderEventsCalendar(view, localStorage.getItem('eventsCalendarDate'));
    }

    $('body').on('hidden.bs.select', '.filters .bs-select', function(e) {
      renderEventsCalendar(localStorage.getItem('eventsCalendarView'), localStorage.getItem('eventsCalendarDate'));
    });

  };

  var initEventsStaff = function() {

    if ($('#eventsStaff').length) {
      var view = localStorage.getItem('eventsStaffView');
      if (!view) view = 'basicWeek';
      renderEventsStaff(view, localStorage.getItem('eventsStaffDate'));
    }

    function renderEventsStaff(defaultView, defaultDate) {

      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,basicDay,basicWeek'
      };

      if (!defaultView) defaultView = 'basicWeek';

      $('#eventsStaff').fullCalendar('destroy');
      $('#eventsStaff').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        eventSources: [{
          url: basePath + '/events/staff.json',
          type: 'POST',
          data: function() {
            return {
              date: defaultDate,
              view: defaultView,
              company: $('#eventsStaff').data('company-ids')
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }],
        eventOrder: 'id',
        eventRender: function(event, element) {
          element.html(event.html);
          element.attr('data-event-id', event.eventId);
        },
        eventClick: function(event) {
          if (event.url) {
            window.open(event.url);
            return false;
          }
        },
        loading: function(isLoading, view) {
          if (isLoading) {
            $('#loading').fadeIn();
          } else {
            $('#loading').fadeOut();
          }
        },
        eventAfterAllRender: function(view) {
          $('#loading').fadeOut();
          $("[data-tooltip='tooltip']").tooltip();
        },
        viewRender: function(view) {
          if (view == 'basicDay') {
            localStorage.setItem('eventsStaffDate', view.start.format('YYYY-MM-DD'));
          } else {
            localStorage.setItem('eventsStaffDate', view.intervalStart.format('YYYY-MM-DD'));
          }
          localStorage.setItem('eventsStaffView', view.type);
          getEvents(localStorage.getItem('eventsStaffView'), localStorage.getItem('eventsStaffDate'));
        }
      });

    }

    function getEvents(view, date) {
      $.ajax({
        url: basePath + '/events/filters',
        type: 'post',
        dataType: 'html',
        data: {
          view: view,
          date: date,
          company: $('#eventsStaff').data('company-ids')
        },
        success: function(data) {
          $('.filter-events ul').html(data);
          $('.filter-events ul li input[type="checkbox"]').uniform();
        },
        error: log_error
      });
    }

    $('body').on('change', '.filter-events input', function(e) {
      $('.fc-event').css('opacity', 1);
      $('.filter-events input').each(function(i, e) {
        var checked = $(e).is(':checked');
        var eventId = $(e).val();
        if (!checked) {
          $('.fc-event[data-event-id="' + eventId + '"]').css('opacity', '0.1');
        }
      });
    });

  };

  var initPlaceAgenda = function() {
    return;
    var h = {};
    h = {
      left: 'title',
      center: '',
      right: 'prev,next,today,month,agendaWeek,agendaDay'
    };
    $('#placeAgenda').fullCalendar('destroy');
    $('#placeAgenda').fullCalendar({
      locale: 'fr',
      header: h,
      firstDay: 1,
      minTime: "06:00:00",
      defaultView: 'agendaWeek',
      eventSources: [{
        url: basePath + '/places/agenda.json?type=options',
        color: '#26a69a',
        textColor: '#fff',
        backgroundColor: App.getBrandColor('green-jungle')
      }]
    })
    $('#tabs').tabs({
      activate: function(event, ui) {
        if (ui.newPanel.attr('id') == 'agenda') {
          $('#placeAgenda').fullCalendar('render');
        }
      }
    });
  };

  var handleDepotPlan = function() {
    $('body').on('click', 'table.depot td', function() {
      var cell = $(this);
      var class1 = cell.attr('class');
      cell.removeAttr('class');

      toastr.clear();

      if (class1 == 'bg-red') {
        cell.addClass('bg-green');
        saveCell(cell, 2);
      } else if (class1 == 'bg-green') {
        cell.addClass('bg-grey');
        saveCell(cell, 3);
      } else if (class1 == 'bg-grey') {
        cell.addClass('bg-yellow');
        saveCell(cell, 4);
      } else if (class1 == 'bg-yellow') {
        saveCell(cell, 0);
      } else if (typeof class1 == 'undefined') {
        cell.addClass('bg-red');
        saveCell(cell, 1);
      }
    });

    $('body').on('click', 'button.initWarehouse', function() {
      var $this = $(this);
      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/warehouses/init/' + $this.data('warehouse-id'),
        type: 'post',
        dataType: 'json',
        success: function(data) {
          $('#loading').fadeOut();
          if (data.success == 1) {
            toastr.success('Les informations ont été générées et enregistrées.')
          } else {
            toastr.error('Une erreur s\'est produite. Merci de réessayer.');
          }
        }
      });
    });


    function getNodes(warehouse_id) {
      $('table.depot td').each(function(i, e) {
        var cell = $(e);
        var x = cell.data('x');
        var y = cell.data('y');
        $.ajax({
          url: basePath + '/warehouses/getNodes/' + warehouse_id,
          type: 'post',
          dataType: 'json'
        });
      });
    }

    function saveCell(cell, flag) {

      var x = cell.data('x');
      var y = cell.data('y');
      var cell_id = cell.data('id');

      $.ajax({
        url: basePath + '/warehouses/saveCell',
        type: 'post',
        dataType: 'json',
        data: {
          x: x,
          y: y,
          flag: flag,
          cell_id: cell_id
        },
        success: function(data) {
          cell.data('id', data.id);
          toastr.success('Cell has been saved.');
        }
      });
    }
  };

  var initSendMessage = function() {
    $("#sendRecipients").select2({
      placeholder: "Search for a user",
      minimumInputLength: 2,
      tags: true,
      ajax: {
        url: basePath + '/users/json',
        dataType: 'json',
        type: 'POST',
        data: function(term, page) {
          return {
            term: term
          };
        },
        results: function(data, page) {
          return {
            results: data
          };
        }
      }
    });
    $('input.make-switch').on('switchChange.bootstrapSwitch', function(event, state) {
      if (state) {
        $('.body.mail').parents('.input').removeClass('hidden');
        $('.body.sms').addClass('hidden');
      } else {
        $('.body.mail').parents('.input').addClass('hidden');
        $('.body.sms').removeClass('hidden');
      }
    });

    CKEDITOR.replace('sendMailBody', {
      // Define the toolbar groups as it is a more accessible solution.
      toolbarGroups: [{
        "name": "basicstyles",
        "groups": ["basicstyles"]
      }, {
        "name": "links",
        "groups": ["links"]
      }, {
        "name": "paragraph",
        "groups": ["list"]
      }],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    });
  };

  var handleStockOrderMoments = function() {
    $('body').on('change', '#StockOrderDeliveryMoment', function() {
      var moment = $(this).val();
      if (moment == 'hour') {
        $('#StockOrderDeliveryMomentHour').removeClass('hidden');
        $('.delivery_moment_hour_invoice').removeClass('hidden');
      } else {
        $('#StockOrderDeliveryMomentHour').addClass('hidden');
        $('.delivery_moment_hour_invoice').addClass('hidden');
      }
    });
    $('body').on('change', '#StockOrderReturnMoment', function() {
      var moment = $(this).val();
      if (moment == 'hour') {
        $('#StockOrderReturnMomentHour').removeClass('hidden');
        $('.return_moment_hour_invoice').removeClass('hidden');
      } else {
        $('#StockOrderReturnMomentHour').addClass('hidden');
        $('.return_moment_hour_invoice').addClass('hidden');
      }
    });
  };

  var handleStockOrderDistanceCovered = function() {
    $('body').on('change', '.computeDistanceCovered', function() {
      computeDistanceCovered();
    });

    $('body').on('change', '#StockOrderDeliveryDistance, #StockOrderReturnDistance', function() {
      updateTotalDistanceCovered();
    });

    $("#StockOrderDeliveryZipCity, #StockOrderReturnZipCity").on('select2:select', function(val, choice) {
      computeDistanceCovered();
    });

    $('input#StockOrderDelivery, input#StockOrderReturn').on('switchChange.bootstrapSwitch', function(event, state) {
      computeDistanceCovered();
    });

    $('body').on('click', '.searchGooglePlaces', function(event) {
      event.preventDefault();
    });

    $(".place").select2({
      tags: true,
      milliseconds: 400,
      minimumInputLength: 1,
      maximumSelectionSize: 1,
      allowClear: true,
      placeholder: {
        id: "",
        placeholder: "Sélectionnez un lieu enregistré..."
      },
      ajax: {
        url: basePath + '/stock_orders/getPlaces',
        dataType: 'json',
        type: 'post',
        delay: 800,
        data: function(params) {
          return {
            q: params.term
          };
        },
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.text,
                id: item.text,
                address: item.address,
                zip: item.zip,
                city: item.city,
                zip_city: item.zip_city
              }
            })
          };

        }
      },
      //Allow manually entered text in drop down.
      createSearchChoice: function(term, results) {
        if ($(results).filter(function() {
            return term.localeCompare(this.text) === 0;
          }).length === 0) {
          return {
            id: term,
            text: term,
            address: '',
            zip: '',
            city: ''
          };
        }
      }
    }).on("select2:select", function(e) {
      var rel = $(this).attr('rel');
      $('.place[rel="' + rel + '"]').val(e.params.data.text);
      $('.address[rel="' + rel + '"]').val(e.params.data.address);
      $('.zip[rel="' + rel + '"]').val(e.params.data.zip);
      $('.city[rel="' + rel + '"]').val(e.params.data.city);
      $('.zip-city[rel="' + rel + '"] option').remove();
      $('.zip-city[rel="' + rel + '"]').append('<option value="9999">' + e.params.data.zip_city + '</option>');
      $('.zip-city[rel="' + rel + '"]').val(9999).trigger('change');
      computeDistanceCovered();
    }).on("select2:unselect", function(e) {
      var rel = $(this).attr('rel');
      $('.place[rel="' + rel + '"]').val('').trigger('change');
      $('.place[rel="' + rel + '"] option[selected="selected"]').val('');
      $('.address[rel="' + rel + '"]').val('');
      $('.distance[rel="' + rel + '"]').val('');
      $('.zip[rel="' + rel + '"]').val('');
      $('.city[rel="' + rel + '"]').val('');
      $('.zip-city[rel="' + rel + '"]').val('').trigger('change');
      $('.zip-city[rel="' + rel + '"] option').remove().trigger('change');
    });

    updateTotalDistanceCovered = function() {
      var stockOrderType = $('#stockOrderType').val();
      if ($('#StockOrderDeliveryDistance').val().length !== 0) {
        var distance1 = parseFloat($('#StockOrderDeliveryDistance').val());
      } else {
        distance1 = 0;
      }
      if ($('#StockOrderReturnDistance').val().length !== 0) {
        var distance2 = parseFloat($('#StockOrderReturnDistance').val());
      } else {
        distance2 = 0;
      }
      var totalDistance = 0;
      if (stockOrderType == 'delivery' ||  stockOrderType == 'sale') {
        totalDistance = distance1;
      } else if (stockOrderType == 'withdrawal') {
        totalDistance = distance2;
      } else {
        totalDistance = distance1 + distance2;
      }
      $('#StockOrderDistanceCovered').val(totalDistance).trigger('change');
    }

    computeDistanceCovered = function() {

      var deliveryMode = $('#StockOrderDelivery').is(':checked');
      var returnMode = $('#StockOrderReturn').is(':checked');

      var deliveryAddress = $('#StockOrderDeliveryAddress').val();
      var deliveryZip = $('#StockOrderDeliveryZip').val();
      var deliveryCity = $('#StockOrderDeliveryCity').val();

      var returnAddress = $('#StockOrderReturnAddress').val();
      var returnZip = $('#StockOrderReturnZip').val();
      var returnCity = $('#StockOrderReturnCity').val();

      var distance = 0;

      if (deliveryMode) {
        $('#StockOrderDeliveryDistance').addClass('spinner');
        $('.delivery_end_address').addClass('hidden');
        if (deliveryCity && deliveryZip) {
          $.ajax({
            url: basePath + '/stock_orders/computeDistance',
            type: 'post',
            dataType: 'json',
            data: {
              toAddress: deliveryAddress,
              toZip: deliveryZip,
              toCity: deliveryCity
            },
            success: function(data) {
              $('#StockOrderDeliveryDistance').val(data.distance_value / 500).trigger('change');
              $('#StockOrderDeliveryDistance').removeClass('spinner');
              $('.delivery_end_address').removeClass('hidden').find('strong').text(data.end_address);
            },
            error: log_error
          });
        }
      }

      if (returnMode) {
        $('#StockOrderReturnDistance').addClass('spinner');
        $('.return_end_address').addClass('hidden');
        if (returnCity && returnZip) {
          $.ajax({
            url: basePath + '/stock_orders/computeDistance',
            type: 'post',
            dataType: 'json',
            data: {
              toAddress: returnAddress,
              toZip: returnZip,
              toCity: returnCity
            },
            success: function(data) {
              $('#StockOrderReturnDistance').val(data.distance_value / 500).trigger('change');
              $('#StockOrderReturnDistance').removeClass('spinner');
              $('.return_end_address').removeClass('hidden').find('strong').text(data.end_address);
            },
            error: log_error
          });
        }
      }
    }
  };

  var handleSelectStockOrderPlace = function() {
    $("#StockOrderDeliveryPlaceId, #StockOrderReturnPlaceId").on('select2-selecting', function(e, choice) {

      var placeId = e.val;
      var inputId = $(e.target).attr('id');

      $.ajax({
        url: basePath + '/places/get',
        type: 'post',
        dataType: 'json',
        data: {
          id: placeId
        },
        success: function(data) {
          if (inputId == 'StockOrderDeliveryPlaceId') {
            $('#StockOrderDeliveryAddress').val(data.Place.address).trigger('change');
            $('#StockOrderDeliveryZip').val(data.Place.zip).trigger('change');
            $('#StockOrderDeliveryCity').val(data.Place.city).trigger('change');
            $('#StockOrderDeliveryZipCity').addClass('hidden');
            $('#StockOrderDeliveryZipCity1').removeClass('hidden').val(data.Place.zip_city);
          }
          if (inputId == 'StockOrderReturnPlaceId') {
            $('#StockOrderReturnAddress').val(data.Place.address).trigger('change');
            $('#StockOrderReturnZip').val(data.Place.zip).trigger('change');
            $('#StockOrderReturnCity').val(data.Place.city).trigger('change');
            $('#StockOrderReturnZipCity').addClass('hidden');
            $('#StockOrderReturnZipCity1').removeClass('hidden').val(data.Place.zip_city);
          }
        },
        error: log_error
      });
    })

    // $("#StockOrderDeliveryPlaceId, #StockOrderReturnPlaceId").on('select2-removed', function(e, choice){
    //   $('#StockOrderDeliveryZipCity').removeClass('hidden');
    //   $('#StockOrderDeliveryZipCity1').addClass('hidden').val('');
    //   $('#StockOrderDeliveryAddress').val('');
    //   $('#StockOrderDeliveryDistance').val('');
    // });
  };

  var handleComputeTotal = function() {

    $('body').on('change', '.computeTotal', function() {
      computeTotal(false);
    });
    $('input#StockOrderFidelityDiscount, input#StockOrderDelivery, input#StockOrderReturn').on('switchChange.bootstrapSwitch', function(event, state) {
      var id = $(this).attr('id');
      if (id == 'StockOrderDelivery') {
        $('input.delivery_mode').removeAttr('checked').parents('span').removeClass('checked');
        $('.delivery_mode').each(function(i, e) {
          if (state) {
            if ($(e).val() == 'festiloc') {
              $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
            }
          } else {
            if ($(e).val() == 'client') {
              $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
            }
          }
        });
      }
      if (id == 'StockOrderReturn') {
        $('input.return_mode').removeAttr('checked').parents('span').removeClass('checked');
        $('.return_mode').each(function(i, e) {
          if (state) {
            if ($(e).val() == 'festiloc') {
              $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
            }
          } else {
            if ($(e).val() == 'client') {
              $(e).attr('checked', 'checked').change().parents('span').addClass('checked');
            }
          }
        })
      }
      if (id == 'StockOrderFidelityDiscount') {
        if (state) {
          $('input#StockOrderFidelityDiscount').attr('checked', 'checked').val(1)
        } else {
          $('input#StockOrderFidelityDiscount').attr('checked', '').val(0)
        }
      }

      computeTotal();
    });
    $('body').on('change', 'input.discount, input.price', function() {
      var line = $(this).parents('tr');
      var discount = line.find('input.discount').val();
      updateUnitPrice(discount, line);
    });

    var updateUnitPrice = function(discount, line) {
      var unitPriceWithDiscount = parseFloat(line.find('input.price').val()) * (100 - discount) / 100;
      line.find('td.priceWithDisount').text(unitPriceWithDiscount.toFixed(2));
    };


    if ($('#stockitemsTable').length > 0) {
      $('#stockitemsTable tbody tr').each(function(i, e) {
        var discount = $(e).find('input.discount').val();
        updateUnitPrice(discount, $(e));
      });
    }

    $('#StockOrderEditForm').keypress(function(event) {
      if (event.keyCode == 10 || event.keyCode == 13) {
        computeTotal();
      }
    });

    var computeTotal = function(init) {
      setTimeout(function() {
        var numberOfPaletts = $('#StockOrderNumberOfPallets').val().length > 0 ? parseInt($('#StockOrderNumberOfPallets').val()) : 0;
        var numberOfRollis = $('#StockOrderNumberOfRollis').val().length > 0 ? parseInt($('#StockOrderNumberOfRollis').val()) : 0;
        var numberOfPalettsXL = $('#StockOrderNumberOfPalletsXl').val().length > 0 ? parseInt($('#StockOrderNumberOfPalletsXl').val()) : 0;
        var numberOfRollisXL = $('#StockOrderNumberOfRollisXl').val().length > 0 ? parseInt($('#StockOrderNumberOfRollisXl').val()) : 0;
        var xlSurcharge = numberOfPalettsXL + numberOfRollisXL;
        var distanceCovered = $('#StockOrderDistanceCovered').val().length > 0 ? parseInt($('#StockOrderDistanceCovered').val()) : 0;
        var stockOrderType = $('#StockOrderType').val();
        var deliveryMoment = $('#StockOrderDeliveryMoment').val();
        var deliveryMode = $('#StockOrderDeliveryMode:checked').val();
        var returnMode = $('#StockOrderReturnMode:checked').val();
        var deliveryMomentInvoice = $('#StockOrderDeliveryMomentHourInvoice').is(':checked');
        var returnMoment = $('#StockOrderReturnMoment').val();
        var returnMomentInvoice = $('#StockOrderReturnMomentHourInvoice').is(':checked');
        var stockOrderTotal = $('#StockOrderTotalHt').val();
        var fidelityDiscountChecked = $('#StockOrderFidelityDiscount').bootstrapSwitch('state');
        var fidelityDiscountPercentage = $('#StockOrderFidelityDiscountPercentage').val().length ? parseInt($('#StockOrderFidelityDiscountPercentage').val()) : 3;
        var withDelivery = $('#StockOrderDelivery').bootstrapSwitch('state');
        var withReturn = $('#StockOrderReturn').bootstrapSwitch('state');
        var deliveryDistanceCovered = $('#StockOrderDeliveryDistance').val().length > 0 ? parseFloat($('#StockOrderDeliveryDistance').val()) : 0;
        var returnDistanceCovered = $('#StockOrderReturnDistance').val().length > 0 ? parseFloat($('#StockOrderReturnDistance').val()) : 0;
        var deliveryCosts = 0;
        var forcedDeliveryCosts = $('#StockOrderForcedDeliveryCosts').val().length > 0 ? parseFloat($('#StockOrderForcedDeliveryCosts').val()) : false;

        // compute number of trips and packaging costs
        var palettPrice = 75;
        var rolliPrice = 70;

        if (numberOfPaletts > 1 || (numberOfPaletts >= 1 && numberOfRollis >= 1)) {
          palettPrice = 60;
        }
        if (numberOfRollis > 1 || (numberOfPaletts >= 1 && numberOfRollis >= 1)) {
          rolliPrice = 50;
        }
        var palettsCosts = 0;
        var rollisCosts = 0;
        var packagingCosts = 0;
        var deliveryPackagingCosts = 0;
        var returnPackagingCosts = 0;

        deliveryPackagingCosts = (palettPrice * numberOfPaletts) + (rolliPrice * numberOfRollis) + (xlSurcharge * 17.5);
        returnPackagingCosts = (palettPrice * numberOfPaletts) + (rolliPrice * numberOfRollis) + (xlSurcharge * 17.5);

        if (stockOrderType == 'delivery' || stockOrderType == 'sale') {
          packagingCosts = deliveryPackagingCosts;
        } else if (stockOrderType == 'withdrawal') {
          packagingCosts = returnPackagingCosts;
        } else {
          packagingCosts = deliveryPackagingCosts + returnPackagingCosts;
        }

        $('.packaging_costs span').html(parseFloat(packagingCosts).toFixed(2));
        $('.delivery_packaging_costs span').html(parseFloat(deliveryPackagingCosts).toFixed(2));
        $('.return_packaging_costs span').html(parseFloat(returnPackagingCosts).toFixed(2));

        // compute transportation costs
        var deliveryTransportationCosts = 0;
        if ((numberOfPaletts + numberOfRollis) <= 6) {
          var deliveryTransportationCosts = deliveryDistanceCovered * 1.5;
        } else if ((numberOfPaletts + numberOfRollis) > 6 && (numberOfPaletts + numberOfRollis) <= 12) {
          var deliveryTransportationCosts = deliveryDistanceCovered * 2;
        } else if ((numberOfPaletts + numberOfRollis) > 12) {
          var deliveryTransportationCosts = deliveryDistanceCovered * 2.5;
        }

        var returnTransportationCosts = 0;
        if ((numberOfPaletts + numberOfRollis) <= 6) {
          var returnTransportationCosts = returnDistanceCovered * 1.5;
        } else if ((numberOfPaletts + numberOfRollis) > 6 && (numberOfPaletts + numberOfRollis) <= 12) {
          var returnTransportationCosts = returnDistanceCovered * 2;
        } else if ((numberOfPaletts + numberOfRollis) > 12) {
          var returnTransportationCosts = returnDistanceCovered * 2.5;
        }

        var deliveryTransportationCosts1 = deliveryTransportationCosts;
        var returnTransportationCosts1 = returnTransportationCosts;
        var transportationCostsInfo = 0;
        if (stockOrderType == 'delivery' || stockOrderType == 'sale') {
          transportationCostsInfo = deliveryTransportationCosts1;
        } else if (stockOrderType == 'withdrawal') {
          transportationCostsInfo = returnTransportationCosts1;
        } else {
          transportationCostsInfo = deliveryTransportationCosts1 + returnTransportationCosts1;
        }
        if (deliveryTransportationCosts < 50) {
          deliveryTransportationCosts = 50;
        }
        if (returnTransportationCosts < 50) {
          returnTransportationCosts = 50;
        }
        if (transportationCostsInfo < 100) {
          transportationCostsInfo = 100;
        }

        if (deliveryMoment == 'hour' && deliveryMomentInvoice) {
          deliveryTransportationCosts = deliveryTransportationCosts + 50;
        }
        if (returnMoment == 'hour' && returnMomentInvoice) {
          returnTransportationCosts = returnTransportationCosts + 50;
        }

        $('.delivery_transportation_costs span').html(parseFloat(deliveryTransportationCosts).toFixed(2));
        $('.return_transportation_costs span').html(parseFloat(returnTransportationCosts).toFixed(2));
        $('.transportation_costs_info span').html(parseFloat(transportationCostsInfo).toFixed(2));

        var transportationCosts = 0;
        if (deliveryMode == 'festiloc' && returnMode == 'festiloc') {
          transportationCosts = deliveryTransportationCosts + returnTransportationCosts;
        } else if (deliveryMode == 'festiloc' && returnMode != 'festiloc') {
          transportationCosts = deliveryTransportationCosts;
        } else if (deliveryMode != 'festiloc' && returnMode == 'festiloc') {
          transportationCosts = returnTransportationCosts;
        }

        if (deliveryMode != 'festiloc' && returnMode != 'festiloc') {
          transportationCosts = 0;
        }

        $('#StockOrderTransportationCosts').val(parseFloat(transportationCosts).toFixed(2));
        $('#StockOrderDeliveryTransportationCosts').val(parseFloat(deliveryTransportationCosts).toFixed(2));
        $('#StockOrderReturnTransportationCosts').val(parseFloat(returnTransportationCosts).toFixed(2));

        $('#StockOrderPackagingCosts').val(parseFloat(packagingCosts).toFixed(2));
        $('#StockOrderDeliveryPackagingCosts').val(parseFloat(deliveryPackagingCosts).toFixed(2));
        $('#StockOrderReturnPackagingCosts').val(parseFloat(deliveryPackagingCosts).toFixed(2));

        if (deliveryMode == 'client' && returnMode == 'client') {
          deliveryCosts = 0;
        }
        if (deliveryMode == 'client' && returnMode == 'festiloc') {
          deliveryCosts = returnTransportationCosts;
        }
        if (deliveryMode == 'client' && returnMode == 'transporter') {
          deliveryCosts = returnPackagingCosts;
        }
        if (deliveryMode == 'festiloc' && returnMode == 'client') {
          deliveryCosts = deliveryTransportationCosts;
        }
        if (deliveryMode == 'festiloc' && returnMode == 'festiloc') {
          deliveryCosts = transportationCosts;
        }
        if (deliveryMode == 'festiloc' && returnMode == 'transporter') {
          deliveryCosts = deliveryTransportationCosts + returnPackagingCosts;
        }
        if (deliveryMode == 'transporter' && returnMode == 'client') {
          deliveryCosts = deliveryPackagingCosts;
        }
        if (deliveryMode == 'transporter' && returnMode == 'festiloc') {
          deliveryCosts = deliveryPackagingCosts + returnTransportationCosts;
        }
        if (deliveryMode == 'transporter' && returnMode == 'transporter') {
          deliveryCosts = packagingCosts;
        }
        var deliveryCosts1 = roundAmount(deliveryCosts);
        $('.delivery_costs span').html(deliveryCosts1);
        $('#StockOrderDeliveryCosts').val(deliveryCosts1);

        //var costsDifference = Math.abs(transportationCosts - packagingCosts);
        //$('.costs_difference span').html(parseFloat(costsDifference).toFixed(2));

        // compute discount according to total HT
        // first we need to get total according to items which are subject to discount!
        var totalForDiscount = 0;
        $('#stockitemsTable tbody tr[data-subject-to-discount]').each(function(i, e) {
          var subjectToDiscount = $(e).find('input.subject_to_discount').val();
          if (subjectToDiscount) {
            totalForDiscount = totalForDiscount + parseFloat($(e).find('td.total_ttc').text());
          }
        })
        totalForDiscount = roundAmount(totalForDiscount);

        var actualDiscountPercentage = $('#StockOrderQuantityDiscountPercentage').val().length > 0 ? parseFloat($('#StockOrderQuantityDiscountPercentage').val()) : Math.floor(stockOrderTotal / 2000) * 2;
        var manualDiscountPercentage = $('#StockOrderManualQuantityDiscountPercentage').is(':checked');
        if (totalForDiscount >= 10000) {
          discountPercentage = 10.0;
        } else if (totalForDiscount >= 8000) {
          discountPercentage = 8.0;
        } else if (totalForDiscount >= 6000) {
          discountPercentage = 6.0;
        } else if (totalForDiscount >= 4000) {
          discountPercentage = 4.0;
        } else if (totalForDiscount >= 2000) {
          discountPercentage = 2.0;
        } else {
          discountPercentage = 0;
        }
        var discount = 0;
        if (manualDiscountPercentage) {
          discount = roundAmount(totalForDiscount * actualDiscountPercentage / 100);
          discountPercentage = actualDiscountPercentage;
        } else {
          discount = roundAmount(totalForDiscount * discountPercentage / 100);
        }
        discount = parseFloat(discount).toFixed(2);
        if (!manualDiscountPercentage ||  1 == 1) {
          $('#StockOrderQuantityDiscount').val(discount);
          $('.quantity_discount span:first').html(discount);
        }
        $('#StockOrderQuantityDiscountPercentage').val(discountPercentage);
        $('.quantity_discount em').html(discountPercentage + '%');

        // compute fidelity discount
        var fidelityDiscount = 0.00;
        if (fidelityDiscountChecked) {
          fidelityDiscount = parseFloat(totalForDiscount * fidelityDiscountPercentage / 100).toFixed(2);
        }
        stockOrderTotalWithDiscount = stockOrderTotal - fidelityDiscount - discount;
        $('#StockOrderFidelityDiscountAmount').val(fidelityDiscount);
        $('#StockOrderFidelityDiscountPercentage').val(fidelityDiscountPercentage);
        $('.fidelity_discount_amount span').html(fidelityDiscount);

        // compute tva
        if (forcedDeliveryCosts !== false) {
          deliveryCosts = forcedDeliveryCosts;
        }
        var intermediateTotal = stockOrderTotalWithDiscount + deliveryCosts;
        var stockOrderTva = roundAmount((intermediateTotal * 0.08).toFixed(3));
        var stockOrderNetTotal = roundAmount(parseFloat(intermediateTotal) + parseFloat(stockOrderTva));

        $('#StockOrderTva').val(stockOrderTva);
        $('.tva span').html(stockOrderTva);
        $('.net_total span').html(stockOrderNetTotal);
        $('#StockOrderNetTotal').val(stockOrderNetTotal);
        $('#StockOrderForcedNetTotal').val(stockOrderNetTotal);

        $('.total_ht_with_discount span').text(roundAmount(stockOrderTotal - fidelityDiscount - discount));

      }, 100);
    }

    $('body').on('blur', '#StockOrderForcedDeliveryCosts', function() {
      var deliveryCosts = $(this).val().length > 0 ? parseFloat($(this).val()) : parseFloat($('#StockOrderDeliveryCosts').val());
      var stockOrderTotal = parseFloat($('#StockOrderTotalHt').val());
      var fidelityDiscount = parseFloat($('#StockOrderFidelityDiscountAmount').val());
      var quantityDiscount = parseFloat($('#StockOrderQuantityDiscount').val());
      var subtotal = parseFloat(deliveryCosts + stockOrderTotal + fidelityDiscount + quantityDiscount);
      var tva = roundAmount((subtotal * 0.08).toFixed(2));
      $('#StockOrderTva').val(tva);
      $('.tva span').html(tva);
      var netTotal = roundAmount(parseFloat(subtotal) + parseFloat(tva));
      $('#StockOrderNetTotal').val(netTotal);
      $('#StockOrderForcedNetTotal').val(netTotal);
      $('.net_total span').text(netTotal);
    })

    computeTotal(true);

    $('body').on('click', '.roundNetTotal', function(event) {
      event.preventDefault();
      var total = $('#StockOrderNetTotal').val();
      total = roundAmount(total);
      $('#StockOrderNetTotal').val(total);
      $('.net_total span').text(total);
    });

    if ($('#StockOrderType').length > 0) {
      $('body').on('change', '#StockOrderType', function() {
        handleDeliveryReturnContainer(false);
        var val = $(this).val();
        var status = $('#StockOrderStatus').val();
        if ((val == 'delivery' || val == 'withdrawal') && status == 'offer') {
          $('#StockOrderStatus').selectpicker('val', 'confirmed');
        }
        var htTotal = parseFloat($('.total_ht span').text());
        if (val == 'sale') {
          $('#StockOrderRepartitionOther').val(htTotal);
          $('#StockOrderRepartitionRental').val('').attr('readonly', true);
        }
      });
      handleDeliveryReturnContainer(true);
    }

    function handleDeliveryReturnContainer(init) {
      var type = $('#StockOrderType').val();
      $('#deliveryContainer').show();
      $('#returnContainer').show();
      if (!init) {
        $('input.delivery_mode').removeAttr('checked');
        $('input.delivery_mode').parents('span').removeClass('checked');
        $('input.return_mode').removeAttr('checked');
        $('input.return_mode').parents('span').removeClass('checked');
      }

      var hasDelivery = $('#StockOrderDelivery').is(':checked');
      var hasReturn = $('#StockOrderReturn').is(':checked');

      switch (type) {
        case 'rental':
          if (!init) {
            if (hasDelivery) {
              $('#StockOrderDeliveryMode[value="festiloc"]').attr('checked', 'checked');
              $('#StockOrderDeliveryMode[value="festiloc"]').parents('span').addClass('checked');
            } else {
              $('#StockOrderReturnMode[value="client"]').attr('checked', 'checked');
              $('#StockOrderReturnMode[value="client"]').parents('span').addClass('checked');
            }
            if (hasReturn) {
              $('#StockOrderReturnMode[value="festiloc"]').attr('checked', 'checked');
              $('#StockOrderReturnMode[value="festiloc"]').parents('span').addClass('checked');
            } else {
              $('#StockOrderReturnMode[value="client"]').attr('checked', 'checked');
              $('#StockOrderReturnMode[value="client"]').parents('span').addClass('checked');
            }
          }
          break;
        case 'sale':
          $('#returnContainer').hide();
          if (!init) {
            $('#StockOrderDeliveryMode[value="client"]').attr('checked', 'checked');
            $('#StockOrderDeliveryMode[value="client"]').parents('span').addClass('checked');
            $('#StockOrderReturnMode[value="client"]').attr('checked', 'checked');
            $('#StockOrderReturnMode[value="client"]').parents('span').addClass('checked');
          }
          break;
        case 'delivery':
          $('#returnContainer').hide();
          if (!init) {
            $('#StockOrderDeliveryMode[value="festiloc"]').attr('checked', 'checked');
            $('#StockOrderDeliveryMode[value="festiloc"]').parents('span').addClass('checked');
            $('#StockOrderReturnMode[value="client"]').attr('checked', 'checked');
            $('#StockOrderReturnMode[value="client"]').parents('span').addClass('checked');
          }
          break;
        case 'withdrawal':
          $('#deliveryContainer').hide();
          if (!init) {
            $('#StockOrderDeliveryMode[value="client"]').attr('checked', 'checked');
            $('#StockOrderDeliveryMode[value="client"]').parents('span').addClass('checked');
            $('#StockOrderReturnMode[value="festiloc"]').attr('checked', 'checked');
            $('#StockOrderReturnMode[value="festiloc"]').parents('span').addClass('checked');
          }
          break;
        default:
          break;
      }
      computeTotal();
    }
  };

  var handleFestilocLive = function() {

    var refreshStockOrders = function() {
      $('.modal').modal('hide');
      var from = $('#from').val();
      var to = $('#to').val();
      var statuses = $('#status').val();
      var dateType = $('#dateType').val();

      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/stock_orders/live_list',
        type: 'post',
        dataType: 'html',
        data: {
          fromDate: from,
          toDate: to,
          statuses: statuses,
          dateType: dateType
        },
        success: function(data) {
          $('#stockorders').html(data);
          $('#stockorders .portlet').each(function(i, e) {
            var label = $(e).find('span.label');
            var status = label.data('status');
            var select = $(e).find('.bs-select');
            select.find('option[value="' + status + '"]').attr('selected', 'selected');
            if (status == 'confirmed') {
              select.attr('data-style', 'green');
            }
            if (status == 'in_progress') {
              select.attr('data-style', 'yellow-crusta');
            }
            if (status == 'processed' ||  status == 'delivered') {
              select.attr('data-style', 'grey-gallery');
            }
          });
          handleBootstrapSelect();
          $('.row.title h3').show().find('span').text($('#stockorders .portlet').get().length);
          $('#loading').fadeOut();
          $('.pulsate').pulsate({
            color: "#D91E18"
          });
          $('body').on('click', 'button.confirmModifications', function(event) {
            var modalId = $(this).parents('.modal').attr('id');
            var stockOrderId = $(this).data('stock-order-id');
            $.ajax({
              url: basePath + '/stock_orders/readModifications',
              type: 'post',
              dataType: 'json',
              data: {
                stock_order_id: stockOrderId
              },
              success: function(data) {
                if (data.success) {
                  $('#' + modalId).modal('hide');
                  refreshStockOrders();
                }
              },
              error: log_error
            });
          });
        }
      })
    };

    //init first refresh
    if ($('.refresh').length > 0) {
      refreshStockOrders();
    }

    $('body').on('click', '.refresh', function(event) {
      event.preventDefault();
      refreshStockOrders();
    })

    $('body').on('click', '.print-pallet', function(event) {
      event.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      var stock_order_id = $(this).parents('.portlet').data('stock-order-id');
      setStatus(stock_order_id, 'processed', function() {
        window.open(href, '_blank');
      });
    })

    $('body').on('change', '.refreshStockOrders', function() {
      refreshStockOrders();
    })

    $('body').on('click', '.clear', function(event) {
      event.preventDefault();
      $('.refreshStockOrders').val('');
      $('select.refreshStockOrders option').prop("selected", false).trigger('change');
      refreshStockOrders();
    });

    $('body').on('click', '.delivery_note', function(event) {
      var stock_order_id = $(this).parents('.portlet').attr('data-stock-order-id');
      setStatus(stock_order_id, 'in_progress', null);
    });

    $('body').on('change', 'input[name="type"]:not(.filter)', function() {
      var type = $('input[name="type"]:checked').val();
      var fromDate = $('#from').data('default');
      var toDate = $('#to').data('default');

      $('#from').datepicker('update', fromDate);
      $('#to').datepicker('update', toDate);

      $('#status option').removeAttr('selected');

      if (type == 'delivery') {
        $('#status option[value="confirmed"]').attr('selected', 'selected');
        $('#status option[value="in_progress"]').attr('selected', 'selected');
        $('#dateType').val('delivery');
      }
      if (type == 'return') {
        $('#status option[value="delivered"]').attr('selected', 'selected');
        $('#status option[value="processed"]').attr('selected', 'selected');
        $('#dateType').val('return');
      }

      $('#status').trigger('change').selectpicker('render');
    });

    var setStatus = function(id, status, callback) {
      $.ajax({
        url: basePath + '/stock_orders/setStatus',
        type: 'post',
        dataType: 'json',
        data: {
          status: status,
          stock_order_id: id
        },
        success: function(data) {
          if (data.success) {
            refreshStockOrders();
            if (callback) {
              callback();
            }
          }
        }
      });
    }

    $('body').on('click', 'a.add-line', function(event) {
      event.preventDefault();
      var clone = $('#extra tr.empty').clone();
      clone.find('input').val('');
      clone.appendTo('#extra tbody');
    })

    $('body').on('click', '.save-invoice', function(event) {
      event.preventDefault();
      bootbox.confirm("Êtes-vous sûr d'avoir bien contrôlé toute la marchandise? La commande va être mise en statut \"à facturer\" et ne sera plus visible.", function(result) {
        if (result) {
          $('form').append('<input type="hidden" value="invoice" name="data[destination]">').submit();
        } else {
          return false;
        }
      });
    })

    $('body').on('click', '.save-issue', function(event) {
      event.preventDefault();
      bootbox.confirm("Êtes-vous sûr d'avoir bien préparé toute la marchandise?", function(result) {
        if (result) {
          $('form').submit();
        } else {
          return false;
        }
      });
    });

    var checkAvailability = function(input) {
      var line = input.parents('tr');
      var id = line.attr('id').replace('#', '');
      var reprocessingDuration = line.find('.reprocessing_duration').val();
      var returnedQuantity = line.find('.returned_quantity').val();
      var stockItemId = line.find('.stockItemId').val();
      input.addClass('spinner');
      line.removeClass().addClass('batch');
      $('table#batches tbody tr:not(.batch)').addClass('hidden');
      $('table#batches tbody tr:not(.batch) ul').empty();
      $('table#batches tbody tr:not(.batch) .infoLine').addClass('hidden');
      $.ajax({
        url: basePath + '/stock_items/checkAvailability',
        type: 'post',
        dataType: 'json',
        data: {
          duration: reprocessingDuration,
          stock_item_id: stockItemId,
          amount: returnedQuantity
        },
        success: function(data) {
          input.removeClass('spinner');
          var infoLine = $('tr[rel="' + id + '"]');
          if (data.conflicts.dates.length > 0 && data.conflicts.orders.length < 1) {
            infoLine.removeClass('hidden');
            line.addClass('warning');
            infoLine.find('.text-warning').removeClass('hidden');
            infoLine.find('span.start-date').text(data.conflicts.dates[0]);
            infoLine.find('span.end-date').text(data.conflicts.dates[data.conflicts.dates.length - 1]);
          }
          if (data.conflicts.orders.length > 0) {
            infoLine.removeClass('hidden');
            line.addClass('danger');
            infoLine.find('.text-danger').removeClass('hidden');
            var item = '<li>' + data.conflicts.orders[0].date + ' ' + data.conflicts.orders[0].name + ' : ' + data.conflicts.orders[0].needed + ' pièces manquantes</li>';
            infoLine.find('ul').html(item);
          }
        },
        error: log_error
      });
    }

    $('body').on('change', 'input.checkAvailability', function() {
      checkAvailability($(this));
    })

    if ($('table#batches').length > 0) {
      $('table#batches tbody tr.batch').each(function(i, e) {
        var input = $(e).find('.checkAvailability');
        checkAvailability(input);
      });
    }

    $('body').on('keydown', 'input.filter', function() {
      var $this = $(this);
      setTimeout(function() {
        $('#orders .order').show();
        var term = $this.val();
        $('#orders .order').each(function(i, e) {
          var string = '';
          $(e).find('.title').each(function(j, f) {
            string += $(f).text().trim().toLowerCase();
          })
          if (string.indexOf(term) < 0) {
            $(e).hide();
          } else {
            $(e).show();
          }
        })
      }, 50);
    })

    function renderDepotCalendar(defaultView, defaultDate) {

      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,basicDay,basicWeek,month'
      };

      var displayDeliveries = $('#display_deliveries').is(':checked');
      var displayReturns = $('#display_returns').is(':checked');

      $('#depotCalendar').fullCalendar('destroy');
      $('#depotCalendar').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        eventSources: [{
          url: basePath + '/festiloc/depot/json.json',
          type: 'POST',
          cache: false,
          data: function() {
            return {
              show: displayDeliveries,
              date: $('#depotCalendar').fullCalendar('getDate').format('YYYY-MM-DD'),
              mode: 'delivery',
              view: defaultView
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }, {
          url: basePath + '/festiloc/depot/json.json',
          type: 'POST',
          cache: false,
          data: function() {
            return {
              show: displayReturns,
              date: $('#depotCalendar').fullCalendar('getDate').format('YYYY-MM-DD'),
              mode: 'return',
              view: defaultView
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }, {
          url: basePath + '/festiloc/depot/json.json',
          type: 'POST',
          cache: false,
          data: function() {
            return {
              show: true,
              date: $('#depotCalendar').fullCalendar('getDate').format('YYYY-MM-DD'),
              mode: 'cancellations',
              view: defaultView
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }],
        eventOrder: ['id', 'title'],
        eventRender: function(event, element) {
          element.attr('data-stock-order-id', event.stock_order_id);
          element.attr('data-tooltip', 'tooltip');
          element.attr('data-original-title', event.packaging);
          element.attr('data-container', 'body');
          element.attr('data-placement', 'top');
          element.attr('data-title', event.title);
          element.attr('data-toggle', 'context');
          element.attr('data-target', '#context');
          element.html(event.description);
          element.on('contextmenu', function() {
            var $context = $('#context');
            var $modal = $('#modal');
            var $weights = $('#weights');
            var $status = $('#status');
            $context.find('.depot_detail').attr('href', event.depot_detail_url);
            $context.find('.depot_issue').attr('href', event.depot_issue_url);
            $context.find('.depot_return').attr('href', event.depot_return_url);

            $context.find('.depot_delivery_note_pdf').attr('href', event.depot_delivery_note_pdf_url);
            $context.find('.depot_pallet_pdf').attr('href', event.depot_pallet_pdf_url);
            $context.find('.depot_transporter_pdf').attr('href', event.depot_transporter_pdf_url);
            $context.find('.depot_return_pdf').attr('href', event.depot_return_pdf_url);
            $context.find('.depot_return_pdf_all').attr('href', event.depot_return_pdf_all_url);

            $context.find('.order_number span').text(event.stock_order_order_number);

            $context.find('.pdf').attr('data-stock-order-id', event.stock_order_id);
            $context.find('.pdf').attr('data-status', event.status);

            if (event.portlet_modifications_url) {
              $context.find('.notifications').show().attr('href', event.portlet_modifications_url);
              $modal.find('.reload').attr('data-url', event.portlet_modifications_url);
              $modal.find('.confirmModifications').attr('data-stock-order-id', event.stock_order_id);
              $context.find('.cancellations').hide();
            } else if (event.notify_cancellation) {
              $context.find('.cancellations a').attr('data-stock-order-id', event.stock_order_id);
              $context.find('.cancellations').show();
              $context.find('.notifications').hide();
            } else {
              $context.find('.notifications').hide();
              $context.find('.cancellations').hide();
            }

            $('body').on('click', '#context li.notifications a', function() {
              $modal.find('.reload').trigger('click');
              $modal.modal('show');
            })

            $('body').on('click', '#context a.action_change_weight', function() {
              $weights.find('#StockOrderId').val(event.stock_order_id);
              $weights.find('#StockOrderMode').val(event.mode);
              if (event.mode == 'delivery') {
                $weights.find('.delivery_weight').show();
                $weights.find('.delivery_weight').val(event.delivery_weight);
                $weights.find('.return_weight').hide();
              }
              if (event.mode == 'return') {
                $weights.find('.return_weight').show();
                $weights.find('.return_weight').val(event.return_weight);
                $weights.find('.delivery_weight').hide();
              }
              $weights.modal('show');
            });

            $('body').on('click', '#context a.action_change_status', function() {
              $status.find('#StockOrderId').val(event.stock_order_id);
              $status.find('#StockOrderStatus').val(event.status);
              $status.find('#StockOrderStatus').selectpicker('refresh');
              $status.modal('show');
            });

            $('body').on('click', '#context a.link', function(ev) {
              document.location.href = $(this).attr('href');
              $('body').off('click', '#context a.link');
            });

            $('body').on('click', '#context a.pdf', function(ev) {
              window.open($(this).attr('href'));
              $('body').off('click', '#context a.pdf');
            });

          });
          var id = document.location.hash.replace('#', '')
          if (event.stock_order_id == id) {
            element.toggleClass('last_modified');
            setTimeout(function() {
              element.toggleClass('faded');
              document.location.hash = '';
            }, 2000);
          }
        },
        loading: function(isLoading, view) {
          if (isLoading) {
            $('#loading').fadeIn();
          } else {
            $('#loading').fadeOut();
          }
        },
        eventAfterAllRender: function(view) {
          $('#loading').fadeOut();
          //var view = $('#depotCalendar').fullCalendar('getView');
          // localStorage.setItem('depotCalendarView', view.type);
          // localStorage.setItem('depotCalendarDate', view.start.format('YYYY-MM-DD'));
          $("[data-tooltip='tooltip']").tooltip();
        },
        viewRender: function(view) {
          var formerView = localStorage.getItem('depotCalendarView');
          localStorage.setItem('depotCalendarView', view.type);
          localStorage.setItem('depotCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderDepotCalendar('month', view.intervalStart.format('YYYY-MM-DD'));
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('depotCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderDepotCalendar('basicWeek', localStorage.getItem('depotCalendarDate'));
          }
        }
      });

    }

    $('body').on('keydown', 'input.filter1', function() {
      var $this = $(this);
      setTimeout(function() {
        $('#depotCalendar .fc-event-container').show();
        var term = $this.val();
        $('#depotCalendar .fc-event-container').each(function(i, e) {
          var string = $(e).find('.fc-event').attr('data-title');
          if (string.indexOf(term) < 0) {
            $(e).css('opacity', '0.3');
          } else {
            $(e).css('opacity', 1);
          }
        });
      }, 50);
    });

    $('body').on('click', 'button.confirmModifications', function(event) {
      var modalId = $(this).parents('.modal').attr('id');
      var stockOrderId = $(this).data('stock-order-id');
      $.ajax({
        url: basePath + '/stock_orders/readModifications',
        type: 'post',
        dataType: 'json',
        data: {
          stock_order_id: stockOrderId
        },
        success: function(data) {
          if (data.success) {
            $('#' + modalId).modal('hide');
            document.location.href = basePath + '/festiloc/depot/detail/' + stockOrderId;
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '#context .notify-cancellation', function(event) {
      event.preventDefault();
      var $this = $(this);
      var stockOrderId = $this.attr('data-stock-order-id');
      bootbox.confirm("Avez-vous bien pris connaissance de l'annulation?", function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/stock_orders/cancel',
            type: 'post',
            dataType: 'json',
            data: {
              stock_order_id: stockOrderId,
              depot: true
            },
            success: function(data) {
              if (data.success) {
                renderDepotCalendar(localStorage.getItem('depotCalendarView'), localStorage.getItem('depotCalendarDate'));
              } else {

              }
            },
            error: log_error
          })
        } else {
          return false;
        }
      });
    });

    $('body').on('change', ':checkbox', function() {
      renderDepotCalendar(localStorage.getItem('depotCalendarView'), localStorage.getItem('depotCalendarDate'));
    });

    $('body').on('click', '.depot_delivery_note_pdf', function(event) {
      var stock_order_id = $(this).attr('data-stock-order-id');
      var status = $(this).attr('data-status');
      if (status == 'confirmed') {
        setStatus(stock_order_id, 'in_progress', null);
      }
    });

    $('body').on('click', '.depot_pallet_pdf', function(event) {
      var stock_order_id = $(this).attr('data-stock-order-id');
      var status = $(this).attr('data-status');
      if (status == 'in_progress') {
        setStatus(stock_order_id, 'processed', null);
      }
    });

    if ($('#depotCalendar').length) {
      var view = localStorage.getItem('depotCalendarView');
      if (!view) view = 'basicWeek';
      renderDepotCalendar(view, localStorage.getItem('depotCalendarDate'));
    }

    $('body').on('click', '.save-weight', function(event) {
      event.preventDefault();
      var form = $('#StockOrderDepotForm');
      var weightInput = form.find('input:visible');
      var weight = weightInput.val();
      var id = form.find('#StockOrderId').val();
      var mode = form.find('#StockOrderMode').val();
      if (mode == 'delivery') {
        var field = 'delivery_weight';
        var data = {
          id: id,
          delivery_weight: weight
        }
      }
      if (mode == 'return') {
        var field = 'return_weight';
        var data = {
          id: id,
          return_weight: weight
        }
      }
      var $modal = $('#weights');

      $.ajax({
        url: basePath + '/stock_orders/update/' + field,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function(data) {
          if (data.success == 1) {
            toastr.success('La commande a été correctement modifiée.');
          } else {
            toastr.error('La commande n\'a pas été modifiée.');
          }
          document.location.hash = id;
          $modal.modal('toggle');
          renderDepotCalendar(localStorage.getItem('depotCalendarView'), localStorage.getItem('depotCalendarDate'));
        },
        error: log_error
      });
    });

    $('body').on('click', '.save-status', function(event) {
      event.preventDefault();
      var form = $('#StockOrderDepotFormStatus');
      var id = form.find('#StockOrderId').val();
      var status = form.find('#StockOrderStatus option:selected').val();
      var $modal = $('#status');

      setStatus(id, status, function() {
        $modal.modal('toggle');
        document.location.hash = id;
      });
    });

    $('#StockOrderDepotForm').keypress(function(event) {
      if (event.keyCode == 10 || event.keyCode == 13) {
        $('#weights').find('.save-weight').trigger('click');
      }
    });

    var setStatus = function(id, status, callback) {
      $.ajax({
        url: basePath + '/stock_orders/setStatus',
        type: 'post',
        dataType: 'json',
        data: {
          status: status,
          stock_order_id: id
        },
        success: function(data) {
          if (data.success) {
            if (callback) {
              callback();
            }
            toastr.success('La commande a été correctement modifiée.');
            var view = localStorage.getItem('depotCalendarView');
            var date = localStorage.getItem('depotCalendarDate');
            renderDepotCalendar(view, date);

          } else {
            toastr.error('La commande n\'a pas été modifiée.');
          }
        }
      })
    }
  };

  var handleWeekPreparation = function() {

    var markers = [];
    //var map;
    var displayClients = 0;

    $('body').on('change', '#displayClients', function(event) {
      event.preventDefault();
      if ($(this).prop('checked')) {
        displayClients = 1;
        $('.client-badge').removeClass('hidden');
      } else {
        displayClients = 0;
        $('.client-badge').addClass('hidden');
      }
      renderCalendar(localStorage.getItem('prepareWeekCalendarView'), localStorage.getItem('prepareWeekCalendarDate'), displayClients);
    });

    if ($('#calendar').length > 0) {
      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,basicDay,basicWeek, month'
      };

      $('body').on('click', '.fc-day-header', function(ev) {
        ev.preventDefault();
        var $this = $(this);
        var start = $('#calendar').fullCalendar('getDate').startOf('week').format('YYYY-MM-DD');
        var daysClasses = ['fc-mon', 'fc-tue', 'fc-wed', 'fc-thu', 'fc-fri', 'fc-sat', 'fc-sun'];
        var classList = $this.attr('class').split(/\s+/).reverse();
        var index = daysClasses.indexOf(classList[0]);
        var href = basePath + '/vehicle_tours/day/' + start + '/' + index;
        window.open(href, '_blank');
      });
      $('body').on('click', '.notifyTransporters', function(event) {
        event.preventDefault();
        $('#loading').fadeIn();
        var start = $('#calendar').fullCalendar('getDate').startOf('week').format();
        $.ajax({
          url: basePath + '/stock_orders/notify_transporters',
          type: 'post',
          dataType: 'json',
          data: {
            date: start
          },
          success: function(data) {
            $('#loading').fadeOut();
            $('#calendar').fullCalendar('refetchEvents');
          },
          error: log_error
        });
      });
      $('body').on('click', '.handleVehicles', function(event) {
        event.preventDefault();
        var start = $('#calendar').fullCalendar('getDate').startOf('week').format('YYYY-MM-DD');
        var href = basePath + '/vehicles/plan/3/' + start;
        window.open(href, '_blank');
      });
    }

    if ($('#gmap1').length > 0) {
      window.map = new google.maps.Map(document.getElementById('gmap1'), {
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      initMap(window.map);
    }

    function renderCalendar(defaultView, defaultDate, displayClients) {
      $('#calendar').fullCalendar('destroy'); // destroy the calendar
      $('#calendar').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        editable: true,
        droppable: true,
        eventOrder: 'id',
        eventSources: [{
          url: basePath + '/stock_orders/prepareWeek.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('prepareWeekCalendarDate'),
              view: localStorage.getItem('prepareWeekCalendarView'),
              mode: 'delivery',
              displayClients: displayClients
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }, {
          url: basePath + '/stock_orders/prepareWeek.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('prepareWeekCalendarDate'),
              view: localStorage.getItem('prepareWeekCalendarView'),
              mode: 'return',
              displayClients: displayClients
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          }
        }],
        eventRender: function(event, element) {
          var $context = $('#context');
          element.attr('data-toggle', 'context');
          element.attr('data-target', '#context');
          element.css('background-color', event.bgColor);
          element.html(event.description);
          element.on('contextmenu', function() {
            $context.find('li a').attr('data-stock-order-id', event.stock_order_id);
            $context.find('li a.transporter_pdf').attr('href', event.transporter_pdf_url);
            $context.find('li.tour').hide();

            $context.find('li.transporter a').on('click', function(e) {
              var $this = $(this);
              if (event.mode == 'delivery') {
                var field = 'delivery_transporter_id';
                var data = {
                  id: event.stock_order_id,
                  delivery_transporter_id: $this.attr('data-transporter-id')
                }
              }
              if (event.mode == 'return') {
                var field = 'return_transporter_id';
                var data = {
                  id: event.stock_order_id,
                  return_transporter_id: $this.attr('data-transporter-id')
                }
              }
              $.ajax({
                url: basePath + '/stock_orders/update/' + field,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(data) {
                  if (data.success == 1) {
                    toastr.success('La commande a été correctement modifiée.');
                  } else {
                    toastr.error('La commande n\'a pas été modifiée.');
                  }
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: log_error
              })
              $context.find('li a').unbind('click');
            });

            $context.find('li.tour a').on('click', function(e) {
              var $this = $(this);
              var date = convertDate(event.start.format());
              var tour_id = $this.attr('data-tour-id');

              if (event.vehicle_tour_id.length > 0) {
                var action = 'edit/' + event.vehicle_tour_id;
              } else {
                var action = 'add';
              }

              $.ajax({
                url: basePath + '/vehicle_tours/' + action,
                type: 'POST',
                dataType: 'json',
                data: {
                  id: event.vehicle_tour_id,
                  stock_order_id: event.stock_order_id,
                  code: tour_id,
                  date: date,
                  type: event.mode
                },
                success: function(data) {
                  if (data.success == 1) {
                    toastr.success('La commande a été correctement modifiée.');
                  } else {
                    toastr.error('La commande n\'a pas été modifiée.');
                  }
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: log_error
              })
              $context.find('li a').unbind('click');
            });

            $context.find('li a.transporter_manually_notify').on('click', function(e) {
              var $this = $(this);
              var date = convertDate(event.start.format());
              $.ajax({
                url: basePath + '/stock_orders/manually_notify_transporter',
                type: 'POST',
                dataType: 'json',
                data: {
                  id: event.vehicle_tour_id,
                  stock_order_id: event.stock_order_id,
                  type: event.mode,
                  date: date,
                },
                success: function(data) {
                  if (data.success == 1) {
                    toastr.success('La commande a été correctement modifiée.');
                  } else {
                    toastr.error('La commande n\'a pas été modifiée.');
                  }
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: log_error
              })
              $context.find('li a').unbind('click');
            });

            $context.find('li a.transporter_notify').on('click', function(e) {
              var $this = $(this);
              $('#loading').fadeIn();
              $.ajax({
                url: basePath + '/stock_orders/notify_transporter',
                type: 'POST',
                dataType: 'json',
                data: {
                  id: event.vehicle_tour_id,
                  stock_order_id: event.stock_order_id,
                  type: event.mode
                },
                success: function(data) {
                  $('#loading').fadeOut();
                  if (data.success == 1) {
                    toastr.success('Le transporteur a été correctement notifié.');
                  } else {
                    toastr.error('Le transporteur n\'a pas été notifié.');
                  }
                  $('#calendar').fullCalendar('refetchEvents');
                },
                error: log_error
              })
              $context.find('li a').unbind('click');
            });

            $context.find('li a.transporter_pdf').on('click', function() {
              window.open($(this).attr('href'));
            })

            if (element.hasClass('festiloc')) {
              $context.find('li.tour').show();
              $context.find('li.model a').on('click', function(e) {
                var $this = $(this);

                $('#hours').modal('show');
                $('#hours .save-reservation').on('click', function(e) {
                  e.preventDefault();

                  var start_date = event.start.format();
                  var start_hour = $('#hours input:eq(0)').val();
                  var start = start_date + ' ' + start_hour + ':00';

                  var end_date = event.start.format();
                  var end_hour = $('#hours input:eq(1)').val();
                  var end = end_date + ' ' + end_hour + ':00';

                  var start_place = $('#hours #start_place').val();
                  var end_place = $('#hours #end_place').val();

                  $.ajax({
                    url: basePath + '/vehicles/add_reservation/',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                      model_id: event.stock_order_id,
                      model: 'stock_order',
                      vehicle_model_id: $this.data('vehicle-model-id'),
                      type: event.mode,
                      start: start,
                      end: end,
                      start_place: start_place,
                      end_place: end_place
                    },
                    success: function(data) {
                      if (data.success == 1) {
                        toastr.success('La commande a été correctement modifiée.');
                      } else {
                        toastr.error('La commande n\'a pas été modifiée.');
                      }
                      $('#calendar').fullCalendar('refetchEvents');
                    },
                    error: log_error
                  });
                  $('#hours .save-reservation').unbind('click');
                  $('#hours').modal('hide');
                });
                $context.find('li a').unbind('click');
              });
            }

            if (element.hasClass('transporter')) {
              $context.find('li a.transporter').show();
            } else {
              $context.find('li a.transporter').hide();
            }


          });
          $context.on('hide.bs.context', function(e) {
            $context.find('li a').unbind('click');
          });
        },
        eventClick: function(event) {
          if (event.url) {
            window.open(event.url);
            return false;
          }
        },
        eventDrop: function(event, delta, revertFunc) {
          bootbox.confirm("Êtes-vous sûr de vouloir modifier la date de livraison/retour de la commande?", function(confirm) {
            if (confirm) {
              if (event.mode == 'delivery') {
                var field = 'delivery_date';
                var data = {
                  id: event.stock_order_id,
                  delivery_date: event.start.format()
                }
              }
              if (event.mode == 'return') {
                var field = 'return_date';
                var data = {
                  id: event.stock_order_id,
                  return_date: event.start.format()
                }
              }
              $.ajax({
                url: basePath + '/stock_orders/update/' + field,
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(data) {
                  if (data.success == 1) {
                    toastr.success('La commande a été correctement modifiée.');
                    $('#calendar').fullCalendar('refetchEvents');
                  } else {
                    toastr.error('La commande n\'a pas été modifiée.');
                  }
                },
                error: log_error
              })
            } else {
              $('#calendar').fullCalendar('refetchEvents');
            }
          })
        },
        dayClick: function(date, jsEvent, view) {
          console.log(date);
        },
        viewRender: function(view) {
          var formerView = localStorage.getItem('prepareWeekCalendarView');
          localStorage.setItem('prepareWeekCalendarView', view.type);
          localStorage.setItem('prepareWeekCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderCalendar('month', view.intervalStart.format('YYYY-MM-DD'), displayClients);
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('prepareWeekCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderCalendar('basicWeek', localStorage.getItem('prepareWeekCalendarDate'), displayClients);
          }
        }
      });
    }

    if ($('#calendar').length) {
      var view = localStorage.getItem('prepareWeekCalendarView');
      if (!view) view = 'basicWeek';
      renderCalendar(view, localStorage.getItem('prepareWeekCalendarDate'), displayClients);
    }

    function handlePortletActions(infowindow) {
      $('.portlet select').bind('change', function() {
        return;
        var $this = $(this);
        var portlet = $this.parents('.portlet');
        var tour_id = $this.find('option:selected').val();
        var stock_order_id = portlet.find('#stock_order_id').val();
        var mode = portlet.find('#mode').val();

        if (mode == 'delivery') {
          var field = 'delivery_tour_id';
          var data = {
            id: stock_order_id,
            delivery_tour_id: tour_id
          }
        }
        if (mode == 'return') {
          var field = 'return_tour_id';
          var data = {
            id: stock_order_id,
            return_tour_id: tour_id
          }
        }
        $.ajax({
          url: basePath + '/stock_orders/update/' + field,
          type: 'POST',
          dataType: 'json',
          data: data,
          success: function(data) {
            if (data.success == 1) {
              toastr.success('La commande a été correctement modifiée.');
              infowindow.close();
            } else {
              toastr.error('La commande n\'a pas été modifiée.');
            }
            //$('#calendar').fullCalendar('refetchEvents');
          },
          error: log_error
        })

        $this.unbind('change');
        $this.blur();
      });
    }

    $('.fullcalendar').on('shown.bs.tab', function() {
      $('#calendar').fullCalendar('render');
      $('#calendar').fullCalendar('refetchEvents');
    })

    $('.gmap').on('shown.bs.tab', function() {
      google.maps.event.trigger(map, 'resize');
      initMap(window.map);
    })

    function initMap(map) {

      deleteMarkers();

      var geocoder = new google.maps.Geocoder();

      var originLat = $('.origin').data('lat');
      var originLng = $('.origin').data('lng');
      var bounds = new google.maps.LatLngBounds();

      //var origin = addMarker(map, new google.maps.LatLng(originLat, originLng), 'Festiloc', basePath + '/img/icons/marker-origin.png', 0);

      var origin = new google.maps.Marker({
        position: new google.maps.LatLng(originLat, originLng),
        map: map,
        icon: basePath + '/img/icons/marker-origin.png',
        zIndex: 0,
        title: 'Festiloc'
      });


      // var calendarView = $('#calendar').fullCalendar('getView');
      // var calendarViewTitle = calendarView.title;
      // var calendarViewName = calendarView.name;
      // var calendarViewStart = calendarView.start.format();
      //
      // $('#map .caption span').html(calendarViewTitle);

      var events = $('#calendar').fullCalendar('clientEvents', function(event) {
        if (calendarViewName == 'basicWeek') {
          if (calendarViewStart == event.start.format()) {
            return true;
          } else {
            var start = new Date(calendarViewStart);
            var date = new Date(event.start.format());
            var diff = (date - start) / (1000 * 3600 * 24);
            if (diff > 0 && diff < 7) {
              return true;
            } else {
              return false;
            }
          }
        } else {
          if (calendarViewStart == event.start.format()) {
            return true;
          } else {
            return false;
          }
        }
      });

      var geocoder = new google.maps.Geocoder();
      var bounds = new google.maps.LatLngBounds();
      var infowindow = new google.maps.InfoWindow();

      $.each(events, function(i, ev) {
        var address = ev.address;
        var portlet = ev.portlet;
        setTimeout(function() {
          geocoder.geocode({
            'address': address
          }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              var temp = addMarker(map, results[0].geometry.location, ev.title, ev.transporter, i);
              bounds.extend(temp.position);
              google.maps.event.addListener(temp, 'click', function() {
                infowindow.close();
                $.ajax({
                  url: basePath + '/stock_orders/portlet/' + ev.stock_order_id,
                  type: 'post',
                  dataType: 'html',
                  data: {
                    mode: ev.mode
                  },
                  success: function(data) {
                    infowindow.setContent(data);
                    infowindow.open(map, temp);
                    handlePortletActions(infowindow);
                    handleBootstrapSelect();
                  },
                  error: log_error
                });
              });
            } else {
              console.log('Geocode was not successful for the following reason: ' + status);
            }
          });
        }, i * googleMapsDelay)
      });
      var latLng = origin.getPosition(); // returns LatLng object
      map.setCenter(latLng); // setCenter takes a LatLng object
      map.setZoom(9);
    }

    // Adds a marker to the map and push to the array.
    function addMarker(map, location, title, transporter, zIndex) {
      var icon = basePath + '/img/icons/marker-' + transporter + '.png';
      var marker = new google.maps.Marker({
        position: location,
        map: map,
        zIndex: zIndex,
        title: title,
        icon: icon
      });
      markers.push(marker);
      return marker;
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
      for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
      }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
      setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
      setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
      clearMarkers();
      markers = [];
    }
  };

  var handleConflicts = function() {
    if ($('#calendar').length) {
      $('#calendar').fullCalendar({
        locale: 'fr',
        firstDay: 1,
        defaultView: 'basicWeek',
        height: 'auto',
        columnFormat: 'dddd DD.MM',
        eventSources: [{
          url: basePath + '/stock_orders/conflicts.json'
        }]
      });
    }
  };

  var handleInvoice = function() {

    $('[data-festiloc-invoice-totals]').each(function(i, e) {
      var $div = $(e);
      var id = $div.data('stockorder-id');
      invoiceTotals(id, $div);
    });

    function invoiceTotals(id, $div, updateInvoicedTotal) {
      if(updateInvoicedTotal){
        var updateTotal = 1;
      } else {
        var updateTotal = 0;
      }
      $.ajax({
        url: basePath + '/stock_orders/invoice_totals',
        type: 'post',
        data: {
          id: id,
          updateInvoicedTotal: updateTotal
        },
        success: function(html) {
          $div.html(html);
        },
        error: log_error
      })
    }

    $('body').on('blur', '[data-festiloc-invoice-totals] input', function() {

      var $this = $(this);
      var value = $this.val();
      var field = $this.data('field');
      var id = $this.data('id');

      $.ajax({
        url: basePath + '/stock_orders/updateField',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: value
        },
        success: function(data) {
          if (data.success == 1) {
            toastr.success('Le champ a été correctement sauvegardé.');
          }
        },
        error: log_error
      }).then(function() {
        var $div = $('[data-festiloc-invoice-totals]');
        var id = $div.data('stockorder-id');
        invoiceTotals(id, $div);
      });

    });

    $('body').on('change', '#replacementCostsTable [type="checkbox"], #extraHoursCostsTable [type="checkbox"]', function() {
      var $this = $(this);
      var $line = $(this).parents('tr');
      var id = $this.data('id');
      var checked = $this.is(':checked');
      var field = $this.data('field');
      var controller = $this.data('controller');
      if (checked) {
        var value = 1;
      } else {
        var value = 0;
      }
      $.ajax({
        url: basePath + '/' + controller + '/updateField',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: value
        },
        success: function(data) {
          if (data.success == 1) {
            toastr.success('Le champ a été correctement sauvegardé.');
          }
        },
        error: log_error
      }).then(function() {
        var $div = $('[data-festiloc-invoice-totals]');
        var id = $div.data('stockorder-id');
        invoiceTotals(id, $div, true);
      });
    });

    return;

    $('body').on('change', 'input.invoice_replacement', function() {

      var line = $(this).parents('tr');
      var checked = $(this).is(':checked');
      var cost = parseFloat(line.find('.replacement_cost').val());
      var netTotal = parseFloat($('#StockOrderNetTotal').val());
      var tva = parseFloat($('#StockOrderTva').val());
      var replacementCosts = parseFloat($('#StockOrderReplacementCosts').val());
      var batchId = line.find('input.batch_id').val();
      var stockOrderId = $('#StockOrderId').val();

      var totalWithoutTva = netTotal - tva;

      if (checked) {
        var newReplacementCosts = replacementCosts + cost;
        var newTva = tva + (cost * 8 / 100);
        var newNetTotal = roundAmount(totalWithoutTva + newTva + cost);
        var invoice_replacement = 1;
      } else {
        var newReplacementCosts = replacementCosts - cost;
        var newTva = tva - (cost * 8 / 100);
        var newNetTotal = roundAmount(totalWithoutTva + newTva - cost);
        var invoice_replacement = 0;
      }
      //var newTva1 = roundAmount(newTva);
      var newTva1 = roundAmount((newTva).toFixed(3));
      //var newNetTotal1 = (Math.ceil(( (newNetTotal))*20 - 0.5)/20).toFixed(2);

      $('#loading').fadeIn();

      $.ajax({
        url: basePath + '/stock_orders/updateCosts',
        type: 'post',
        dataType: 'json',
        data: {
          stock_order_batch_id: batchId,
          stock_order_id: stockOrderId,
          net_total: newNetTotal,
          forced_net_total: newNetTotal,
          invoiced_total: newNetTotal,
          tva: newTva,
          replacement_costs: newReplacementCosts,
          invoice_replacement: invoice_replacement
        },
        success: function(data) {
          $('#loading').fadeOut();
          $('#StockOrderNetTotal').val(newNetTotal);
          $('#StockOrderInvoicedTotal').val(newNetTotal);
          $('#StockOrderTva').val(newTva1);
          $('#StockOrderReplacementCosts').val(newReplacementCosts.toFixed(2));
          //$('td.net_total span').text(newNetTotal.toFixed(2));
          $('td.net_total input').val(newNetTotal);
          $('td.tva span').text(newTva1);
          $('td.replacement_costs span').text(newReplacementCosts.toFixed(2));
          toastr.success('Les modifications ont été sauvegardées.');
        }
      })

    });

    $('body').on('change', 'input.invoice_extrahour', function() {

      var line = $(this).parents('tr');
      var checked = $(this).is(':checked');
      var cost = parseFloat(line.find('.extrahour_cost').val());
      var netTotal = parseFloat($('#StockOrderNetTotal').val());
      var tva = parseFloat($('#StockOrderTva').val());
      var extrahoursCosts = $('#StockOrderExtrahoursCosts').val().length ? parseFloat($('#StockOrderExtrahoursCosts').val()) : 0;
      var extraHourId = line.find('input.extrahour_id').val();
      var stockOrderId = $('#StockOrderId').val();

      var totalWithoutTva = netTotal - tva;

      if (checked) {
        var newExtrahoursCosts = extrahoursCosts + cost;
        var newTva = tva + (cost * 8 / 100);
        var newNetTotal = roundAmount(totalWithoutTva + newTva + cost);
        var invoice_extrahour = 1;
      } else {
        var newExtrahoursCosts = extrahoursCosts - cost;
        var newTva = tva - (cost * 8 / 100);
        var newNetTotal = roundAmount(totalWithoutTva + newTva - cost);
        var invoice_extrahour = 0;
      }
      //var newTva1 = roundAmount(newTva);
      var newTva1 = roundAmount((newTva).toFixed(3));
      //var newNetTotal1 = (Math.ceil(( (newNetTotal))*20 - 0.5)/20).toFixed(2);

      $('#loading').fadeIn();

      $.ajax({
        url: basePath + '/stock_orders/updateCosts',
        type: 'post',
        dataType: 'json',
        data: {
          stock_order_id: stockOrderId,
          net_total: newNetTotal,
          forced_net_total: newNetTotal,
          invoiced_total: newNetTotal,
          tva: newTva1,
          extrahours_costs: newExtrahoursCosts,
          invoice_extrahour: invoice_extrahour,
          extrahour_id: extraHourId
        },
        success: function(data) {
          $('#loading').fadeOut();
          $('#StockOrderNetTotal').val(newNetTotal);
          $('#StockOrderInvoicedTotal').val(newNetTotal);
          $('#StockOrderTva').val(newTva1);
          $('#StockOrderExtrahoursCosts').val(newExtrahoursCosts.toFixed(2));
          //$('td.net_total span').text(newNetTotal.toFixed(2));
          $('td.net_total input').val(newNetTotal);
          $('td.tva span').text(newTva1);
          $('td.extrahours_costs span').text(newExtrahoursCosts.toFixed(2));
          toastr.success('Les modifications ont été sauvegardées.');
        }
      });

    });

    $('body').on('change', 'input.deposit', function() {
      var deposit = parseFloat($(this).val());
      var defaultDeposit = parseFloat($(this).data('default'));
      var netTotal = parseFloat($('#StockOrderNetTotal').val());
      var stockOrderId = $('#StockOrderId').val();

      $.ajax({
        url: basePath + '/stock_orders/updateCosts',
        type: 'post',
        dataType: 'json',
        data: {
          stock_order_id: stockOrderId,
          deposit: deposit
        },
        success: function(data) {
          $('#loading').fadeOut();
          $('#StockOrderInvoicedTotal').val(roundAmount(netTotal - deposit));
          $('#StockOrderInvoicedTotal').attr('data-default', roundAmount(netTotal - deposit));
          setTimeout(function() {
            $('#StockOrderInvoicedTotal').change();
          }, 200);
          toastr.success('Les modifications ont été sauvegardées.');
        }
      });
    });

    $('body').on('change', 'input.invoiced_total', function() {

      var netTotal = parseFloat($('#StockOrderNetTotal').val());
      var invoiced_total = $(this).val().length > 0 ? roundAmount(parseFloat($(this).val())) : netTotal;
      var defaultInvoicedTotal = $(this).attr('data-default').length > 0 ? parseFloat($(this).attr('data-default')) : invoiced_total;
      var tva = parseFloat($('#StockOrderTva').val());
      var stockOrderId = $('#StockOrderId').val();
      var deposit = $('#StockOrderDeposit').val().length > 0 ? parseFloat($('#StockOrderDeposit').val()) : 0;

      //var newTva = roundAmount( tva + (netTotal - (defaultInvoicedTotal - invoiced_total) - netTotal) / 1.08);
      if (deposit > 0) {
        var newTva = tva;
      } else {
        var newTva = roundAmount((invoiced_total + deposit) - ((invoiced_total + deposit) / 1.08));
      }
      if (newTva < 0) newTva = 0;
      var forcedNetTotal = netTotal - (defaultInvoicedTotal - invoiced_total);

      $.ajax({
        url: basePath + '/stock_orders/updateCosts',
        type: 'post',
        dataType: 'json',
        data: {
          stock_order_id: stockOrderId,
          invoiced_total: invoiced_total,
          tva: newTva,
          forced_net_total: forcedNetTotal
        },
        success: function(data) {
          $('#loading').fadeOut();
          $('#StockOrderTva').val(newTva);
          $('td.tva span').text(newTva);
          toastr.success('Les modifications ont été sauvegardées.');
        }
      });
    });
  };

  var handleSearchGooglePlaces = function() {
    $("#StockOrderAddressSearch").select2({
      minimumInputLength: 5,
      delay: 1000,
      ajax: {
        url: basePath + '/stock_orders/searchGooglePlaces',
        dataType: 'json',
        type: 'POST',
        data: function(term, page) {
          return {
            term: term
          };
        },
        results: function(data, page) {
          return {
            results: data
          };
        }
      },
      formatResult: formatPlace
    });

    function formatPlace(place) {
      if (!place.id) {
        return place.text;
      }
      var div = '';
      div += '<div class="row">';
      div += '<div class="col-md-2">';
      div += '<img src="' + place.icon + '" class="img-responsive"/>';
      div += '</div>';
      div += '<div class="col-md-12">';
      div += '<h4>' + place.text + '</h4>';
      div += '<p>' + place.address + '<br>' + place.zip_city + '</p>';
      div += '</div>';
      div += '</div>';

      var $place = $(div);
      return $place;
    };
    $("#StockOrderAddressSearch").on("select2-selecting", function(e, choice) {
      $('#StockOrderDeliveryAddress').val(e.choice.address);
      $('#StockOrderDeliveryZipCity1').removeClass('hidden').val(e.choice.zip_city);
      $('#StockOrderDeliveryZip').val(e.choice.zip);
      $('#StockOrderDeliveryCity').val(e.choice.city);
      $('#StockOrderDeliveryZipCity, #s2id_StockOrderDeliveryZipCity').addClass('hidden');
    });
  };

  var handleModalClient = function() {
    $('body').on('submit', '#ClientAddForm', function() {
      var company = $(this).find('#ClientCompanies option:selected').val();
      if (company) {
        var data = $(this).serialize();
        $.ajax({
          url: basePath + '/clients/add',
          data: data,
          dataType: 'json',
          success: function(data) {
            if (data.success == 1) {
              toastr.success('Client has been added.');
              $('.modal.in').modal('hide');
              $('select.client').append('<option value="' + data.clientId + '">' + data.clientName + '</option>')
              $('.client').val(data.clientId).change();
              $('.contact-people').val(data.contactPeopleId);
            } else {
              toast.error('An error occured. Please try again.');
            }
            $('#EventCompanyId').selectpicker('val', company);
          },
          error: log_error
        });
      } else {
        bootbox.alert('Aucune entreprise sélectionnée!');
      }
      return false;
    });

    $('#new-contact-people').on('shown.bs.modal', function(e) {
      if ($('[data-target="#new-contact-people"]').data('client-id')) {
        $('#ContactPeopleClientId').val($('[data-target="#new-contact-people"]').data('client-id'));
      } else {
        $('#ContactPeopleClientId').val($('select.client').val());
      }
    })
    $('body').on('submit', '#ContactPeopleAddForm', function() {
      var data = $(this).serialize();
      $.ajax({
        url: basePath + '/contact_peoples/add',
        data: data,
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            toastr.success('Contact people has been added.');
            $('.modal.in').modal('hide');
            $('.client').change();
            setTimeout(function() {
              $('.contact-people').selectpicker('val', data.contactPeopleId);
              $('.contact-people').selectpicker('refresh');
              $('.contact-people').val(data.contactPeopleId).change();
            }, 500);
          } else {
            toast.error('An error occured. Please try again.');
          }
        }
      });
      return false;
    });

    $('#edit-client').on('shown.bs.modal', function() {
      var form = $('#ClientEditForm');
      var clientId = $('#StockOrderClientId').val();
      $.ajax({
        url: basePath + '/clients/get',
        type: 'post',
        dataType: 'json',
        data: {
          client_id: clientId
        },
        success: function(data) {
          form.attr('action', basePath + '/clients/edit/' + data.Client.id);
          form.find('#ClientName').val(data.Client.name);
          form.find('#ClientAddress').val(data.Client.address);
          form.find('#ClientZip').val(data.Client.zip);
          form.find('#ClientCity').val(data.Client.city);
          form.find('#ClientId').val(data.Client.id);
          $.each(data.Company, function(i, e) {
            form.find('#ClientCompanies option[value="' + e.id + '"]').attr('selected', true);
          })
          form.find('#ClientCompanies').selectpicker('refresh');
        }
      });
    });
    $('body').on('submit', '#ClientEditForm', function() {
      var data = $(this).serialize();
      var form = $(this);
      var company = form.find('#ClientCompanies option:selected').val();

      if (company) {
        $.ajax({
          url: form.attr('action'),
          data: data,
          dataType: 'json',
          success: function(data) {
            if (data.success == 1) {
              toastr.success('Client has been saved.');
              $('.modal.in').modal('hide');
              $('#StockOrderClientId option[value="' + data.clientId + '"]').text(data.clientName);
              $('#StockOrderClientId').select2('destroy');
              $('#StockOrderClientId').select2().trigger('change');
            } else {
              toast.error('An error occured. Please try again.');
            }
          },
          error: log_error
        });
      } else {
        bootbox.alert('Aucune entreprise sélectionnée!');
      }

      return false;
    });

    $('#edit-contact-people').on('shown.bs.modal', function() {
      var form = $('#ContactPeopleEditForm');
      var contactPeopleId = $('#StockOrderContactPeopleId').val();

      $.ajax({
        url: basePath + '/contact_peoples/get',
        type: 'post',
        dataType: 'json',
        data: {
          contact_people_id: contactPeopleId
        },
        success: function(data) {
          form.attr('action', basePath + '/contact_peoples/edit/' + data.ContactPeople.id);
          form.find('#ContactPeopleFirstName').val(data.ContactPeople.first_name);
          form.find('#ContactPeopleLastName').val(data.ContactPeople.last_name);
          form.find('#ContactPeopleEmail').val(data.ContactPeople.email);
          form.find('#ContactPeoplePhone').val(data.ContactPeople.phone);
          form.find('#ContactPeopleFunction').val(data.ContactPeople.function);
          form.find('#ContactPeopleDepartment').val(data.ContactPeople.department);
          form.find('#ContactPeopleLanguage option[value="' + data.ContactPeople.language + '"]').attr('selected', true);
          form.find('#ContactPeopleCivility option[value="' + data.ContactPeople.civility + '"]').attr('selected', true);
          form.find('#ContactPeopleId').val(data.ContactPeople.id);
          form.find('#ContactPeopleClientId').val(data.ContactPeople.client_id);
          form.find('#ContactPeopleCivility').selectpicker('refresh');
          form.find('#ContactPeopleLanguage').selectpicker('refresh');
        }
      });
    });
    $('body').on('submit', '#ContactPeopleEditForm', function() {
      var data = $(this).serialize();
      var form = $(this);
      $.ajax({
        url: form.attr('action'),
        data: data,
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            toastr.success('Contact person has been saved.');
            $('.modal.in').modal('hide');
            $('#StockOrderClientId').change();
          } else {
            toast.error('An error occured. Please try again.');
          }
        },
        error: log_error
      })
      return false;
    });
  };

  var handleGetContactPeople = function() {
    $('#StockOrderClientId').on('select2:close', function() {
      setTimeout(function() {
        var contactPeopleId = $('#StockOrderContactPeopleId').val();
        getContactPeople(contactPeopleId);
      }, 320)
    });

    var getContactPeople = function(id) {
      $.ajax({
        url: basePath + '/contact_peoples/get',
        type: 'post',
        dataType: 'json',
        data: {
          contact_people_id: id
        },
        success: function(data) {
          $('#StockOrderInvoicePhone').val(data.ContactPeople.phone);
          $('#StockOrderInvoiceEmail').val(data.ContactPeople.email);
        }
      })
    };

    $('body').on('change', '#StockOrderContactPeopleId', function() {
      getContactPeople($(this).val());
    })
  };

  var handleAutomaticSave = function() {

    // var save = function(callback){
    // 	setTimeout(function(){
    // 		var data = $('#StockOrderEditForm').serialize();
    // 		$.ajax({
    // 			url: document.location.href,
    // 			data: data,
    // 			type: 'post',
    // 			dataType: 'json',
    // 			success: function(data){
    // 				toastr.clear();
    // 				toastr.info('La comamnde a été sauvegardée.');
    // 				if($('#StockOrderName').val().length > 0)$('span.caption-subject').text($('#StockOrderName').val());
    // 				if(data){
    // 					$('#stockitemsTable tbody tr:not(.empty, .free)').each(function(i,e){
    // 						if($(e).find('input.id').val().length < 1){
    // 							var temp = data[i];
    // 							$(e).find('input.id').val(temp.id);
    // 						}
    // 					})
    // 				}
    // 				if(callback){
    // 					callback();
    // 				}
    // 			},
    // 			error: log_error
    // 		})
    // 	}, 100)
    // }

    if ($('[data-automatic-save]').length > 0) {

      // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      //   stockOrderSave();
      // });
      // $('body').on('blur', '#StockOrderEditForm input:not(.date-picker, .select2-focusser, .no-save), #StockOrderEditForm textarea', function(){
      //   stockOrderSave(function(){
      //     $('body').off('blur');
      //   });
      // });
      // $('body').on('change', '.date-picker', function(){
      //   stockOrderSave();
      // });
      // $('body').on('click', '.btn-group', function(){
      //   stockOrderSave();
      // });
      // $('body').on('change', 'select', function(){
      //   stockOrderSave();
      // });

      $('body').on('click', '.view-offer', function(event) {
        event.preventDefault();
        var $this = $(this);
        var href = $this.attr('href');
        if (!ajaxInProgress) {
          $('#loading').fadeIn();
          var $this = $(this);
          var href = $this.attr('href');
          stockOrderSave(function() {
            $('#loading').fadeOut();
            window.open(href, '_blank');
          });
        }
      });
      var hook = true;
      $('button.check-order').click(function() {
        hook = false;
      });
      if ($('body').hasClass('admin')) {
        hook = false;
      }
      window.onbeforeunload = function() {
        if (hook) {
          return "Des modifications apportées à votre commande peuvent être perdues."
        }
      }

      setInterval(function() {
        stockOrderSave();
      }, 120000);

      // $('body').on('click', '.form-actions button, .form-actions a', function(e){
      //   ajaxStockOrderSave.abort();
      // });
    }
  };

  var handleBootbox = function() {
    $('body').on('click', '.confirm-offer', function(event) {
      event.preventDefault();
      var href = $(this).attr('href');
      bootbox.confirm("Êtes-vous sûr de vouloir confirmer l'offre?", function(result) {
        if (result) {
          window.location.href = href;
        } else {
          return;
        }
      });
    });
  };

  var initCkeditor = function() {
    CKEDITOR.replaceAll('ckeditor', {
      // Define the toolbar groups as it is a more accessible solution.
      toolbarGroups: [{
        "name": "basicstyles",
        "groups": ["basicstyles"]
      }, {
        "name": "links",
        "groups": ["links"]
      }, {
        "name": "paragraph",
        "groups": ["list"]
      }],
      // Remove the redundant buttons from toolbar groups defined above.
      removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
    });
  };

  var handleDraggableProducts = function() {
    $('.sortable').on('click', function(event) {
      event.preventDefault();
    });

    $("#stockitemsTable tbody").sortable({
      revert: true,
      placeholder: "empty-row",
      stop: function(event, ui) {
        //var rows = [];
        $("#stockitemsTable tbody tr").each(function(i, e) {
          $(e).find('.weight').val(i);
          $(e).find('.actions span').text(i + 1);
        })
      }
    });
  };

  var handleScrollSpy = function() {
    $('body').scrollspy({
      target: '#profile-nav',
      offset: 90
    });

    $('#profile-nav a').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 90)
      }, 1000, 'easeInOutExpo');
      event.preventDefault();
    });
    $(window).scroll(function() {
      scrollProfileMenu();
    });
    $(window).load(function() {
      scrollProfileMenu();
    });
    var scrollProfileMenu = function() {
      if ($(window).scrollTop() > 130) {
        $("#profile-menu").css("top", $(window).scrollTop() - 130 + "px");
      } else {
        $("#profile-menu").css("top", "0px");
      }
    }
  };

  var handleHideAlert = function() {
    if ($('.alert').length > 0) {
      setTimeout(function() {
        $('.alert').fadeOut();
      }, 5000);
    }
  };

  var generateDataTables = function() {

    if ($('[data-dataTable]').length) {
      $('[data-dataTable]').each(function(i, e) {
          generateDataTable($(e));
        })
        //generateDataTable($('[data-dataTable]:visible'));
    }

    function generateDataTable(table) {
      var temp = table.data('columns').split('|');
      var columns = [];
      temp.forEach(function(entry) {
        columns.push({
          data: entry
        });
      });

      var dataSrc = table.data('data-src') ? table.data('data-src') : 'data';
      var pageLength = table.data('page-length') ? table.data('page-length') : 20;
      var columnSort = table.data('column-sort') ? table.data('column-sort') : 0;
      var sortOrder = table.data('sort-order') ? table.data('sort-order') : 'asc';
      var bSort = table.data('bsort') === false ? false : true;
      var delay = table.data('ajax-reload') ? table.data('ajax-reload') : 30000;

      var columnFilter1 = table.data('column-filter') ? table.data('column-filter') : '';
      var columnFilterType = table.data('column-filter-type') ? table.data('column-filter-type') : 'select';
      var columnFilterValues = table.data('column-filter-values') ? table.data('column-filter-values') : [];

      var table1 = table.DataTable({
        ajax: {
          url: table.data('url'),
          dataSrc: dataSrc,
          cache: true
        },
        stateSave: true,
        responsive: true,
        columns: columns,
        pageLength: pageLength,
        lengthMenu: [
          [10, 20, 100, -1],
          [10, 20, 100, "All"] // change per page values here
        ],
        aaSorting: [
          [columnSort, sortOrder]
        ],
        bSort: bSort,
        initComplete: function(settings, json) {
          $('.dataTables_length select').select2();
        },
        language: {
          aria: {
            "sortAscending": ": activer pour trier la colonne par ordre croissant",
            "sortDescending": ": activer pour trier la colonne par ordre décroissant"
          },
          emptyTable: "Aucune donnée disponible dans le tableau",
          info: "Affichage des &eacute;lements _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
          infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
          infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
          lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
          search: "Rechercher&nbsp;:",
          zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
          zeroRecords: "Chargement des donn&eacute;es..."
        },
      });
    }
  };

  var handleLastTab = function() {
    // return;
    $('a[data-toggle="tab"]:not(.gmap, .no-save)').on('shown.bs.tab', function() {
      localStorage.setItem(document.location.pathname + '_lastTab', $(this).attr('href'));
    });

    var hash = document.location.hash;
    if (hash) {
      $('a[data-toggle="tab"][href="' + hash + '"]').tab('show');
    } else {
      var lastTab = localStorage.getItem(document.location.pathname + '_lastTab');
      if (lastTab) {
        $('a[href=' + lastTab + ']').tab('show');
      } else {
        // Set the first tab if cookie do not exist
        $('a[data-toggle="tab"]:first').tab('show');
      }
    }


  };

  var handleAddContactPeople = function() {
    $('body').on('click', '.add-contact-person', function(event) {
      event.preventDefault();
      var portletClone = $('.portlet.contact-person:last').clone();
      portletClone.find('select').val('');
      portletClone.find('select option').removeAttr('selected');
      portletClone.find('input').val('');
      portletClone.insertAfter('.portlet:last');
    });
  };

  var handleCheckEmail = function() {
    $('input[type="email"], .checkEmail').on('blur', function() {
      var input = $(this);
      var email = input.val();
      var parent = input.parents('.input');

      if (email.length) {
        input.addClass('spinner');
        if (validateEmail(email)) {
          parent.removeClass('has-error');

          $.ajax({
            url: basePath + '/my_tools/verify_email/' + email + '/info@une-bonne-idee.ch/0',
            type: 'post',
            dataType: 'json',
            success: function(msg) {
              input.removeClass('spinner');
              if (msg == 'valid') {
                parent.removeClass('has-error');
              } else {
                toastr.error("L'adresse e-mail ne semble pas exister. Veuillez contrôler.");
                parent.addClass('has-error');
              }
            }
          });

        } else {
          toastr.error("L'adresse e-mail n'est pas correctement formatée. Veuillez contrôler.");
          parent.addClass('has-error');
          input.removeClass('spinner');
        }
      }
    });

    function validateEmail(email) {
      var re = "/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i";
      return re.test(email);
    }
  };

  var handleCancelStockOrder = function() {
    $('body').on('click', '.cancel-stock-order', function(event) {
      event.preventDefault();
      $this = $(this);
      $href = $this.attr('href');
      bootbox.confirm('Êtes-vous sûr de vouloir annuler cette commande?', function(result) {
        if (result) {
          document.location.href = $href;
        }
      });
    })
  };

  var handleShowSubmitButtons = function() {

    showButtons();

    $('body').on('change', '.computeTotal, #StockOrderType', function() {
      showButtons();
    })

    function showButtons() {
      var $links = $('.check-order');
      $links.hide();
      var delivery1 = $('#StockOrderDelivery').is(':checked');
      var return1 = $('#StockOrderReturn').is(':checked');
      var rental = $('#StockOrderType').is(':checked');
      if (delivery1 ||  return1) {
        var distanceCovered = $('#StockOrderDistanceCovered').val().length > 0 ? $('#StockOrderDistanceCovered').val() : 0;
        var numberOfPallets = $('#StockOrderNumberOfPallets').val().length > 0 ? $('#StockOrderNumberOfPallets').val() : 0;
        var numberOfRollis = $('#StockOrderNumberOfRollis').val().length > 0 ? $('#StockOrderNumberOfRollis').val() : 0;
        var deliveryMode = $('#StockOrderDeliveryMode').val();
        var returnMode = $('#StockOrderReturnMode').val();

        if ((!delivery1 && deliveryMode != 'client') || (!return1 && returnMode != 'client')) {
          $links.hide();
        }
        if (distanceCovered && (numberOfPallets || numberOfRollis)) {
          $links.show();
        }
      } else {
        $links.show();
      }
      if (!rental) {
        $links.show();
      }
    }
  };

  var handleSelectVehicleCompany = function(companyList, companyModelList) {
    $('body').on('change', companyList, function() {
      var $this = $(this);
      var companyId = $this.find('option:selected').val();
      var models = $(companyModelList);

      models.find('option').remove();
      models.selectpicker('refresh');

      $.ajax({
        url: basePath + '/vehicle_company_models/get',
        type: 'post',
        dataType: 'json',
        data: {
          vehicle_company_id: companyId
        },
        success: function(data) {
          models.find('option').remove();
          models.selectpicker('refresh');
          $.each(data, function(i, e) {
            if (models.data('default') == e.VehicleCompanyModel.id) {
              models.append('<option selected="selected" value="' + e.VehicleCompanyModel.id + '">' + e.VehicleCompanyModel.name + '</option>')
            } else {
              models.append('<option value="' + e.VehicleCompanyModel.id + '">' + e.VehicleCompanyModel.name + '</option>')
            }
            models.selectpicker('refresh');
          });
          models.change();
        },
        error: log_error
      }).then(function() {
        //$('body').off('change', companyList);
      });
    });
  };

  var handleSelectVehicleCompanyModel = function() {
    $('body').on('change', '.models', function() {
      var $this = $(this);
      var model = $this.find('option:selected').val();
      var vehiclesList = $this.data('vehicles-list');
      var vehicles = $('#' + vehiclesList);

      if (!model) {
        return;
      }
      vehicles.find('option:not(:first)').remove();
      getVehicles(model, vehicles, 'model');
    });

    $('body').on('change', '.families', function() {
      var $this = $(this);
      var family = $this.find('option:selected').val();
      var vehiclesList = $this.data('vehicles-list');
      var vehicles = $('#' + vehiclesList);

      if (!family) {
        return;
      }
      vehicles.find('option:not(:first)').remove();
      getVehicles(family, vehicles, 'family');
    });

    $('.models option').each(function(i, e) {
      if ($(e).is(':selected') && $(e).val()) {
        var $this = $(e).parents('.models');
        var model = $(e).val();
        var vehiclesList = $this.data('vehicles-list');
        var vehicles = $('#' + vehiclesList);
        getVehicles($(e).val(), vehicles, 'model');
      }
    })

    function getVehicles(id, list, field) {
      $.ajax({
        url: basePath + '/vehicles/get',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field
        },
        success: function(data) {
          list.find('option').remove();
          list.append('<option value="">A définir</option>');
          $.each(data, function(i, e) {
            if (list.data('default') == e.Vehicle.id) {
              list.append('<option selected="selected" value="' + e.Vehicle.id + '">' + e.Vehicle.name_number + '</option>')
            } else {
              list.append('<option value="' + e.Vehicle.id + '">' + e.Vehicle.name_number + '</option>')
            }
          })
          list.selectpicker('refresh');
        },
        error: log_error
      });
    }
  };

  var handleVehicleReservations = function() {

    $('body').on('click', '.save-reservation', function(event) {
      event.preventDefault();

      var $this = $(this);
      var portlet = $this.parents('.portlet.tour');
      var tour = portlet.data('tour-id');
      var confirmed;

      portlet.find('.reservation').each(function(i, e) {
        var div = $(e);
        var type = div.data('model-type');
        if (type == 'vehicle') {
          var vehicle_model_id = div.find('.models option:selected').val();
          var vehicle_id = div.find('.vehicles option:selected').val();
        }
        if (type == 'trailer') {
          var vehicle_model_id = div.find('.models option:selected').val();
          var vehicle_id = div.find('.trailers option:selected').val();
        }
        var reservationId = div.attr('data-reservation-id');
        var start = div.find('.start').val();
        var end = div.find('.end').val();
        var start_place = div.find('.start_place').val();
        var end_place = div.find('.end_place').val();
        var confirmed = div.find('#confirmed').val();

        if (!vehicle_model_id ||  !start ||  !end) {
          if (type == 'vehicle') {
            toastr.warning("Vous n'avez pas rempli tous les champs nécessaires pour la réservation du véhicule!");
          }
          if (type == 'trailer') {
            toastr.warning("Vous n'avez pas rempli tous les champs nécessaires pour la réservation de la remorque!");
          }
        } else {
          $.ajax({
            url: basePath + '/vehicle_reservations/save',
            type: 'post',
            dataType: 'json',
            data: {
              reservationId: reservationId,
              tour: tour,
              vehicle_model_id: vehicle_model_id,
              vehicle_id: vehicle_id,
              start: start,
              start_place: start_place,
              end: end,
              end_place: end_place,
              confirmed: confirmed
            },
            success: function(data) {
              if (data.success) {
                div.attr('data-reservation-id', data.id);
                if (type == 'vehicle') {
                  toastr.success('La réservation du véhicule a été effectuée. Vous devrez encore la confirmer.');
                } else if (type == 'trailer') {
                  toastr.success('La réservation de la remorque a été effectuée. Vous devrez encore la confirmer.');
                }
                $this.blur();
              } else {
                toastr.error('Une erreur est intervenue.');
              }
            },
            error: log_error
          });
        }
      });
    });

    var renderReservationsCalendar = function(defaultView, defaultDate, callback) {
      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today, agendaDay,agendaWeek,listWeek'
      };
      $('#reservations').fullCalendar('destroy'); // destroy the calendar
      $('#reservations').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        scrollTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        editable: false,
        eventSources: [{
          url: basePath + '/vehicles/reservations.json?type=reservations',
          type: 'post',
          textColor: '#fff',
          color: '#3598dc',
          data: function() {
            return {
              date: localStorage.getItem('vehicleReservationsCalendarDate'),
              view: localStorage.getItem('vehicleReservationsCalendarView')
            }
          }
        }, {
          url: basePath + '/vehicles/reservations.json?type=unavailabilities',
          type: 'post',
          textColor: '#fff',
          color: '#DE8384',
          data: function() {
            return {
              date: localStorage.getItem('vehicleReservationsCalendarDate'),
              view: localStorage.getItem('vehicleReservationsCalendarView')
            }
          }
        }],
        loading: function(isLoading, view) {
          if (isLoading) {
            $('#loading').fadeIn();
          } else {
            $('#loading').fadeOut();
          }
        },
        eventRender: function(event, element) {
          element.find('.fc-content').html(event.description);
          element.find('.fc-bg').html('<span class="btn btn-xs btn-list-item btn-company btn-company-' + event.company + '"></span>');
          element.find('.fc-list-item-marker').prepend('<span class="btn btn-xs btn-list-item btn-company btn-company-' + event.company + '"></span>');
          element.find('.fc-list-item-title').after('<td class="fc-list-item-infos fc-widget-content"></td>');

          if (event.available) {
            element.find('.fc-list-item-title a').attr('data-target', '#modal').attr('data-toggle', 'modal');
            element.find('.fc-list-item-infos').html(event.infos);
          } else {
            element.find('.fc-list-item-title a').attr('data-target', '#modal-unavailability').attr('data-toggle', 'modal');
          }

          $('#modal, #modal-unavailability').on('shown.bs.modal', function(e) {
            handleBootstrapSelect();
            handleDatePickers();
          });
          element.on('mouseenter', function() {
            var $this = $(this);
            var left = $this.css('left');
            var right = $this.css('right');
            var zindex = $this.css('z-index');
            var mright = $this.css('margin-right');

            if ($this.parents('.fc-view').hasClass('fc-agendaWeek-view')) {
              $this.css('left', '0%');
              $this.css('right', '0%');
              $this.css('margin-right', 0);

              $this.attr('left', left);
              $this.attr('right', right);
              $this.attr('margin-right', mright);
            }
            $this.css('z-index', 1000);
            $this.attr('z-index', zindex);


          });
          element.on('mouseleave', function() {
            var $this = $(this);
            $this.css('left', $this.attr('left'));
            $this.css('right', $this.attr('right'));
            $this.css('z-index', $this.attr('z-index'));
            $this.css('margin-right', $this.attr('margin-right'));
          });
        },
        viewRender: function(view) {
          var formerView = localStorage.getItem('vehicleReservationsCalendarView');
          localStorage.setItem('vehicleReservationsCalendarView', view.type);
          localStorage.setItem('vehicleReservationsCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderReservationsCalendar('month', view.intervalStart.format('YYYY-MM-DD'));
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('vehicleReservationsCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderReservationsCalendar('basicWeek', localStorage.getItem('vehicleReservationsCalendarDate'));
          }
        },
        eventAfterAllRender: function() {
          $('#reservations .fc-list-heading .fc-widget-header').attr('colspan', 4);
          if ($('#reservations .cancelled').get().length) {
            toastr.warning('Des réservations ont été annulées! Merci de les traiter.');
          }
          $('#reservations .fc-has-url').on('click', function(e) {
            if ($(e.target).attr('href') === undefined) {
              e.preventDefault();
            }
          });
          if (localStorage.getItem('vehicleReservationsCalendarView') != 'list') {
            $('#reservations .fc-event:not(.unavailable)').attr('data-toggle', 'modal').attr('data-target', '#modal');
            $('#reservations .fc-event.unavailable').attr('data-toggle', 'modal').attr('data-target', '#modal-unavailability');
          }
        }
      });
    }

    $('body').on('submit', '#modal-ask-vehicle form', function() {
      var $this = $(this);
      var $modal = $this.parents('.modal.in');
      $.ajax({
        url: $this.attr('action'),
        type: 'post',
        dataType: 'json',
        data: $this.serialize(),
        success: function(data) {},
        error: log_error
      }).then(function() {
        $modal.modal('hide');
        renderReservationsCalendar(localStorage.getItem('vehicleReservationsCalendarView'), localStorage.getItem('vehicleReservationsCalendarDate'));
      });
      return false;
    });

    if ($('#reservations').length) {
      var view = localStorage.getItem('vehicleReservationsCalendarView');
      if (!view) view = 'agendaWeek';
      renderReservationsCalendar(view, localStorage.getItem('vehicleReservationsCalendarDate'));
    }

    $('body').on('click', '#modal.modal button[type="submit"], #modal-unavailability.modal button[type="submit"]', function(e) {
      e.preventDefault();
      var $this = $(this);
      var mode = $this.data('mode');
      var modal = $this.parents('.modal.in');
      var confirmed = modal.find('#confirmed').val();
      var archived = modal.find('#archived').val();

      if (mode == 'confirm') {
        confirmed = 1;
      }
      if (mode == 'archive') {
        archived = 1;
      }

      var reservationId = modal.find('.reservation').data('reservation-id');
      var vehicle_id = modal.find('#vehicles option:selected').val();
      var vehicle_model_id = modal.find('#models option:selected').val();
      var family = modal.find('#families option:selected').val();
      var start = modal.find('#start').val();
      var end = modal.find('#end').val();
      var start_place = modal.find('#start_place').val();
      var end_place = modal.find('#end_place').val();
      var tour = modal.find('#tour').val();

      if (mode == 'confirm' && !vehicle_id) {
        toastr.warning('Vous devez sélectionner un véhicule avant de confirmer!');
        return;
      }

      if (mode == 'delete') {
        bootbox.confirm("Êtes-vous sûr de vouloir supprimer la réservation?", function(result) {
          if (result) {
            $.ajax({
              url: basePath + '/vehicle_reservations/delete',
              type: 'post',
              dataType: 'json',
              data: {
                reservationId: reservationId
              },
              success: function(data) {
                if (data.success) {
                  if (confirmed == 1) toastr.success('La réservation a été supprimée.');
                  $this.blur();
                } else {
                  toastr.error('Une erreur est intervenue.');
                }
                modal.modal('hide');
                $('#reservations').fullCalendar('refetchEvents');
              },
              error: log_error
            })
          }
        })
        return;
      }

      $.ajax({
        url: basePath + '/vehicle_reservations/save',
        type: 'post',
        dataType: 'json',
        data: {
          reservationId: reservationId,
          vehicle_id: vehicle_id,
          vehicle_model_id: vehicle_model_id,
          family: family,
          start: start,
          start_place: start_place,
          end: end,
          end_place: end_place,
          confirmed: confirmed,
          archived: archived,
          tour: tour
        },
        success: function(data) {
          if (data.success) {
            if (confirmed == 1 && archived == 0) toastr.success('La réservation a été effectuée.');
            if (confirmed == 0 && archived == 0) toastr.success('La réservation a été effectuée. Vous devrez encore la confirmer.');
            if (archived == 1) toastr.success('La réservation a été archivée et masquée.');
            $this.blur();
          } else {
            toastr.error('Une erreur est intervenue.');
          }
          modal.modal('hide');
          $('#reservations').fullCalendar('refetchEvents');
        },
        error: log_error
      });

      $this.unbind('click');
    });
  };

  var handleStockOrderType = function() {
    if ($('#StockOrderType').length > 0) {
      $('body').on('change', '#StockOrderType', function() {
        handleDeliveryReturnContainer();
      });
      handleDeliveryReturnContainer();
    }

    function handleDeliveryReturnContainer() {
      var type = $('#StockOrderType').val();
      $('#deliveryContainer').show();
      $('#returnContainer').show();
      switch (type) {
        case 'rental':
          break;
        case 'sale':
          $('#returnContainer').hide();
          $('input.return_mode').removeAttr('checked');
          $('input.return_mode').parents('span').removeClass('checked');
          break;
        case 'delivery':
          $('#returnContainer').hide();
          $('input.return_mode').removeAttr('checked');
          $('input.return_mode').parents('span').removeClass('checked');
          break;
        case 'withdrawal':
          $('#deliveryContainer').hide();
          $('input.delivery_mode').removeAttr('checked');
          $('input.delivery_mode').parents('span').removeClass('checked');
          break;
        default:
          break;
      }
    }
  };

  var handleBootboxConfirmation = function() {
    $('body').on('click', 'a.bootbox', function(event) {
      event.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      bootbox.confirm("Êtes-vous sûr de vouloir effectuer cette action?", function(result) {
        if (result) {
          $('#loading').show();
          document.location.href = href;
        } else {
          return;
        }
      });
    });
  };

  var handleDraggableOrders = function() {

    $(".tour-orders tbody").sortable({
      revert: true,
      placeholder: "empty-row",
      stop: function(event, ui) {
        var rows = [];
        $(this).find("tr").each(function(i, e) {
          $(e).attr('data-weight', i);
          rows.push({
            id: $(e).data('stockorder-vehicletour-id'),
            weight: i
          });
        })
        $.ajax({
          url: basePath + '/vehicle_tours/updateWeights',
          type: 'post',
          dataType: 'json',
          data: {
            rows: rows
          },
          success: function(data) {
            if (data.success) {
              toastr.success("L'ordre des commandes a bien été sauvegardé.")
            }
          },
          error: log_error
        });
      }
    });
  };

  var handleReturnAddress = function() {
    $('body').on('blur', 'input[rel="delivery"]', function(event) {
      var $this = $(this);
      var id = $this.attr('id');
      var newId = id.replace('Delivery', 'Return');
      var value = $this.val();
      var same = $('#StockOrderDeliveryReturnSame').bootstrapSwitch('state');
      if (same) {
        $('#' + newId).val(value);
        setTimeout(function() {
          copyDeliveryAddress();
        }, 50);
      }
    })
    $('#StockOrderDeliveryReturnSame').on('switchChange.bootstrapSwitch', function(event, state) {
      if (state) {
        copyDeliveryAddress();
        $('#return_coords input').attr('readonly', true);
      } else {
        // disable readonly
        $('#return_coords input').removeAttr('readonly');
      }
    });
    $('#StockOrderDeliveryPlace').on('select2:select', function(event) {
      $('#StockOrderDeliveryAddress').blur();
    });

    function copyDeliveryAddress() {
      var deliveryAddress = $('#StockOrderDeliveryAddress').val();
      var deliveryDistance = $('#StockOrderDeliveryDistance').val();
      var deliveryZip = $('#StockOrderDeliveryZip').val();
      var deliveryCity = $('#StockOrderDeliveryCity').val();
      var deliveryPlace = $('#StockOrderDeliveryPlace').val();
      if (deliveryPlace) deliveryPlace = deliveryPlace.split(',');

      var tempId = Date.now();

      $('#StockOrderReturnAddress').val(deliveryAddress);
      $('#StockOrderReturnDistance').val(deliveryDistance);
      $('#StockOrderReturnZip').val(deliveryZip);
      $('#StockOrderReturnCity').val(deliveryCity);
      $('#StockOrderReturnZipCity').append('<option value="' + tempId + '">' + deliveryZip + ' ' + deliveryCity + '</option>');
      $('#StockOrderReturnZipCity').val(tempId).trigger('change');
      if (typeof deliveryPlace == 'object') {
        $('#StockOrderReturnPlace').append('<option value="' + deliveryPlace[0] + '">' + deliveryPlace[0] + '</option>');
        $('#StockOrderReturnPlace').val(deliveryPlace[0]).trigger('change');
      }
    }
  };

  var handleTourPlanification = function() {

    $('body').on('click', '.start-point, .end-point', function(event) {
      event.preventDefault();
      var $this = $(this);
      var tourId = $this.parents('.feeds').data('tour-id');
      var type = $this.parents('li').data('type');
      var value = $this.parents('li').data('value').split('|');
      var $portlet = $this.parents('.portlet-body');

      if (type == 'start') {
        var $addressId = '#VehicleTourStartAddress';
        var $addressLocation = '#VehicleTourStartLocation';
        var $addressLocationCustom = '#VehicleTourStartLocationCustom';
        var $modal = $('#startLocation');
        var $tourRemarks = $portlet.find('div.tour-remarks').text().trim();
        $modal.find('#VehicleTourRemarks').val($tourRemarks);
      }

      if (type == 'end') {
        var $addressId = '#VehicleTourEndAddress';
        var $addressLocation = '#VehicleTourEndLocation';
        var $addressLocationCustom = '#VehicleTourEndLocationCustom';
        var $modal = $('#endLocation');
      }
      $modal.modal('show');
      $modal.find('.vehicle_tour_id').val(tourId);
      $modal.find('.other').addClass('hidden');

      $modal.on('shown.bs.modal', function() {
        $modal.find($addressLocation).val(value[0]).selectpicker('refresh');
        if (value[0] == 'other') {
          $modal.find($addressLocationCustom).val(value[1]);
          $modal.find($addressLocation).val(value[2]);
          $modal.find('#VehicleTourStart').val(value[3]);
          $('.other').removeClass('hidden');
        } else {
          $modal.find('.other').addClass('hidden');
          $modal.find($addressLocationCustom).val('');
        }

        $modal.find($addressLocation).change(function() {
          var $this = $(this);
          var $address = $modal.find($addressId);
          $modal.find('.other').addClass('hidden');
          $modal.find('.other input').val('');
          if ($this.val() == 'festiloc') {
            $address.val('Route du Tir Fédéral 10, 1762 Givisiez');
          } else if ($this.val() == 'ubic') {
            $address.val('Route du Petit-Moncor 1c, 1752 Villars-sur-Glâne');
          } else {
            $('.other').removeClass('hidden');
          }
        });
      });
    });

    $('body').on('submit', '#startLocation form, #endLocation form', function(e) {
      e.preventDefault();
      var data1 = $(this).serialize();
      var tour = $('#VehicleTourId').val();

      $.ajax({
        url: basePath + '/vehicle_tours/update',
        data: data1,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            $('.modal.in').modal('hide');
            updateTour($('#feeds' + tour), function() {
              $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
            });
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });
      $('#VehicleTourMomentVehicleTourId').val('');
      return false;
    });

    $('body').on('change', '.remarks', function(e) {
      var remarks = $(this).val();
      var tour = $(this).attr('data-tour-id');

      var data1 = {
        'VehicleTour': {
          'id': tour,
          'remarks': remarks
        }
      };

      $.ajax({
        url: basePath + '/vehicle_tours/update',
        data: data1,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            updateTour($('#feeds' + tour), function() {
              $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
            });
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.add-line', function(ev) {
      ev.preventDefault();
      var $this = $(this);
      var type = $this.data('type');
      var tour = $this.data('tour');
      var $modal = $('#addVehicleTourMoment');
      if (type == 'free') {
        $modal.find('.moment-description').show();
      } else {
        $modal.find('.moment-description').hide();
      }
      $modal.modal('show');
      $modal.find('#VehicleTourMomentType').val(type).selectpicker('refresh');
      $modal.find('#VehicleTourMomentVehicleTourId').val(tour);
    });

    $('body').on('submit', '#VehicleTourMomentAddForm', function(e) {
      e.preventDefault();
      var data1 = $(this).serialize();
      var type = $('#VehicleTourMomentType').val();
      var tour = $('#VehicleTourMomentVehicleTourId').val();

      $.ajax({
        url: basePath + '/vehicle_tour_moments/add',
        data: data1,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            $('.modal.in').modal('hide');
            updateTour($('#feeds' + tour), function() {
              $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
            });
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });
      $('#VehicleTourMomentVehicleTourId').val('');
      return false;
    });

    $('body').on('click', '.editable', function(ev) {
      ev.preventDefault();
      var $this = $(this);
      var id = $this.data('vehicle-tour-moment-id');
      var duration = $this.data('value');
      var description = $this.data('description');
      var type = $this.data('type');
      var tour = $this.parents('.feeds').data('tour-id');

      var $modal = $('#editVehicleTourMoment');
      $modal.find('#VehicleTourMomentId').val(id);
      $modal.find('.delete-moment').attr('data-vehicle-tour-moment-id', id);
      $modal.find('.delete-moment').attr('data-vehicle-tour-id', tour);
      $modal.find('#VehicleTourMomentDuration').val(duration);
      $modal.find('#VehicleTourMomentDescription').val(description);
      $modal.find('#VehicleTourMomentType').val(type).selectpicker('refresh');
      $modal.find('#VehicleTourMomentVehicleTourId1').val(tour);
      if (type == 'free') {
        $modal.find('.moment-description').show();
      } else {
        $modal.find('.moment-description').hide();
      }
      $modal.modal('show');
    });

    $('body').on('submit', '#VehicleTourMomentEditForm', function(e) {
      e.preventDefault();
      var data1 = $(this).serialize();
      var tour = $('#VehicleTourMomentVehicleTourId1').val();

      $.ajax({
        url: basePath + '/vehicle_tour_moments/edit',
        data: data1,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            $('.modal.in').modal('hide');
            updateTour($('#feeds' + tour), function() {
              $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
            });
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });
      $('#VehicleTourMomentVehicleTourId').val('');
      return false;
    });

    $('body').on('click', '.notify_driver', function(ev) {
      ev.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      $('#loading').fadeIn();
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          $('#loading').fadeOut();
          if (data.success == 1) {
            toastr.success('Le chauffeur a été notifié.');
          } else {
            toastr.error('Something wrong happened. Please try again.');
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.delete-moment', function(ev) {
      ev.preventDefault();
      var momentId = $(this).attr('data-vehicle-tour-moment-id');
      var tour = $(this).parents('form').find('#VehicleTourMomentVehicleTourId1').val();

      $.ajax({
        url: basePath + '/vehicle_tour_moments/delete',
        type: 'post',
        dataType: 'json',
        data: {
          id: momentId
        },
        success: function(data) {
          if (data.success == 1) {
            $('li[data-vehicle-tour-moment-id="' + momentId + '"]').remove();
            $('.modal.in').modal('hide');
            setTimeout(function() {
              updateTour($('#feeds' + tour), function() {
                $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
              });
            }, 500);
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });

    });

    $('body').on('click', '.remove-vehicle-reservation', function(event) {
      event.preventDefault();
      var $this = $(this);
      var tour = $this.attr('data-vehicle-tour-id');
      var reservation_id = $this.attr('data-vehicle-reservation-id');

      bootbox.confirm("Êtes-vous sûr de vouloir annuler cette réservation?", function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/vehicle_reservations/cancel',
            data: {
              reservationId: reservation_id
            },
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success == 1) {
                $('.modal.in').modal('hide');
                updateTour($('#feeds' + tour), function() {
                  $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
                });
              } else {
                toastr.error('An error occured. Please try again.');
              }
            },
            error: log_error
          });
        }
      });
    });

    $(document).once().ajaxStop(function() {
      $("ul.feeds").sortable({
        revert: true,
        items: ".sortable",
        placeholder: "empty-feed",
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height());
        },
        stop: function(event, ui) {
          var type = ui.item.data('type');
          var prevIndex = ui.item.index() - 1;
          var nextIndex = ui.item.index() + 1;
          var prev = $(this).find('li:eq(' + prevIndex + ')');
          var next = $(this).find('li:eq(' + nextIndex + ')');

          if ((type == prev.data('type') || type == next.data('type')) && 1 == 2) {
            toastr.warning('Impossible de mettre l\'élément désiré à cet emplacement.');
            return false;
          } else {
            var $this = $(this);
            updateTour($this, function() {
              $this.parents('.portlet').find('.reload').trigger('click');
            });
          }
        }
      });

      $('.assign_driver.modal, .assign_vehicle.modal, .assign_trailer.modal').on('shown.bs.modal', function() {
        handleBootstrapSelect();
        handleSelectVehicleCompany('#' + $(this).find('.companies').attr('id'), '#' + $(this).find('.models').attr('id'));
        handleSelectVehicleCompanyModel();
        handleDatePickers();
      });
      $('.assign_vehicle.modal, .assign_trailer.modal').on('shown.bs.modal', function() {
        $(this).find('.companies').trigger('change');
        handleVehicleReservationForm($(this).find('form'), $(this).data('vehicle-tour-id'));
      });
    });

    function updateTour(feeds, callback) {
      var portlet = feeds.parents('.portlet');
      var tour = feeds.data('tour-id');
      var items = [];
      feeds.find('li').each(function(i, e) {
        var type = $(e).data('type');
        items.push({
          weight: i,
          id: $(e).data('vehicle-tour-moment-id'),
          tour: tour,
          type: type,
          stock_order_id: $(e).data('stockorder-id')
        });
      });

      $.ajax({
        url: basePath + '/vehicle_tours/update',
        type: 'post',
        dataType: 'json',
        data: {
          items: items
        },
        success: function(data) {
          if (data.success == 1) {
            toastr.success('La tournée a été correctement sauvegardée.');
          } else {
            toastr.error('La tournée n\'a pas été sauvegardée.');
          }
          if (callback) {
            callback();
          }
        },
        error: log_error
      });
    }

    $('body').on('click', '.display-columns', function(event) {
      event.preventDefault();
      var numberOfColums = $(this).data('columns');
      var tmp = 12 / numberOfColums;
      $('.display-columns').removeClass('active');
      $(this).addClass('active');
      $('.tour').attr('class', 'tour col-md-' + tmp);
    });

    $("#tours").sortable({
      revert: true,
      items: ".tour",
      placeholder: "empty-col-md-6",
      forcePlaceholderSize: false
    });

    $('body').on('submit', '#VehicleTourAssignDriverForm', function(e) {
      e.preventDefault();
      var data1 = $(this).serialize();
      var tour = $('#VehicleTourAssignDriverForm #VehicleTourId').val();

      $.ajax({
        url: basePath + '/vehicle_tours/assign_driver',
        data: data1,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success == 1) {
            $('.modal.in').modal('hide');
            updateTour($('#feeds' + tour), function() {
              $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
            });
          } else {
            toastr.error('An error occured. Please try again.');
          }
        },
        error: log_error
      });
      return false;
    });

    function handleVehicleReservationForm(form, tour) {

      form.submit(function() {
        var vehicle_model_id = form.find('.models option:selected').val();
        var vehicle_id = form.find('.vehicles option:selected').val();
        var reservation_id = form.find('.vehicle_reservation_id').val();
        var start = form.find('.start').val();
        var end = form.find('.end').val();
        var start_place = form.find('.start_place').val(); // use vehicle tour data
        var end_place = form.find('.end_place').val(); // use vehicle tour data
        var confirmed = form.find('.confirmed').val();

        $.ajax({
          url: basePath + '/vehicle_reservations/save',
          data: {
            reservationId: reservation_id,
            tour: tour,
            vehicle_model_id: vehicle_model_id,
            vehicle_id: vehicle_id,
            start: start,
            start_place: start_place,
            end: end,
            end_place: end_place,
            confirmed: confirmed
          },
          type: 'post',
          dataType: 'json',
          success: function(data) {
            if (data.success == 1) {
              $('.modal.in').modal('hide');
              updateTour($('#feeds' + tour), function() {
                $('#feeds' + tour).parents('.portlet').find('.reload').trigger('click');
              });
            } else {
              toastr.error('An error occured. Please try again.');
            }
          },
          error: log_error
        });
        return false;
      });
    }
  };

  var cresus = function() {
    $('body').on('change', 'input.cresus', function() {
      var $this = $(this);
      var cresusId = $this.val();
      var myubicId = $this.data('id');
      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/clients/cresus',
        data: {
          import_id: cresusId,
          id: myubicId
        },
        type: 'post',
        dataType: 'json',
        success: function(data) {
          $('#loading').fadeOut();
          if (data.success = 1) {
            $this.attr('readonly', 'readonly');
            toastr.success("Le client a été correctement mis à jour.");
          } else {
            toastr.danger("Le client n'a pas été mis à jour.");
          }
        }
      });
    });
  };

  var handleSteps = function() {
    $('body').on('click', '.mt-step-col a', function() {
      $('.mt-step-col').removeClass('active');
      var $this = $(this);
      $this.parents('.mt-step-col:not(.done)').toggleClass('active');
    });
  };

  var handleTrackingSheets = function() {
    $('body').on('click', '#tracking-sheets li a', function(event) {
      event.preventDefault();
      var $this = $(this);
      var ts = $this.attr('href').replace('#', '');
      $('.tracking-sheet').hide();
      $('.tracking-sheet[id="' + ts + '"]').show();
    });
  };

  var handleXEditable = function() {
    //$.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.inputclass = 'form-control';
    // $('.editable:not(.sortable)').editable({
    //   disabled: true
    // });
    $('#enable').click(function(e) {
      e.stopPropagation();
      e.preventDefault();
      $('.editable:not(.sortable)').editable('toggleDisabled');
      $(this).blur();
    });

    $('table.accounting .editable').editable({
      url: basePath + '/events/updateField',
      disabled: false,
      //mode: 'inline',
      emptytext: 'vide',
      ajaxOptions: {
        dataType: 'json',
        type: 'post'
      },
      success: function(data, newValue) {
        if (data.success) {
          toastr.success("L'évènement a été correctement sauvegardé.");
        }
      }
    });
  };

  var handleTimeline = function() {
    $('body').on('click', '.timeline-filters a', function(ev) {
      ev.preventDefault();
      var $this = $(this);
      var type = $this.data('type');
      $('.timeline-item').hide();
      if (type == 'all') {
        $('.timeline-item').show();
      } else {
        $('.timeline-item[data-type="' + type + '"]').show();
      }
    });
  };

  var handleNoteModal = function() {
    $('#new-note').on('show.bs.modal', function(e) {
      var related = $(e.relatedTarget);
      var $this = $(this);
      var $form = $this.find('form');
      var sticky = $form.find('#NoteSticky').bootstrapSwitch('state');
      var model = related.data('model');
      var modelId = related.data('model-id');
      var noteId = related.data('note-id');
      var category = related.data('category');

      $form.find('#NoteModel').val(model);
      $form.find('#NoteModelId').val(modelId);
      $form.find('#NoteId').val('');
      $form.find('#NoteCategory').val('');
      $form.find('#NoteTitle').val('');
      $form.find('#NoteMessage').val('');
      $form.find('#NoteLevel').val('').selectpicker('refresh');
      $this.find('h4.new').show();
      $this.find('h4.edit').hide();

      if (sticky) {
        $form.find('#NoteSticky').bootstrapSwitch('toggleState');
      }
      if (noteId) {
        $form.find('#NoteId').val(noteId);
        $this.find('h4.new').hide();
        $this.find('h4.edit').show();
        $.ajax({
          url: basePath + '/notes/modal/modal',
          type: 'post',
          dataType: 'json',
          data: {
            id: noteId
          },
          success: function(note) {
            $form.find('#NoteTitle').val(note.Note.title);
            $form.find('#NoteMessage').val(note.Note.message);
            $form.find('#NoteModel').val(note.Note.model);
            $form.find('#NoteModelId').val(note.Note.model_id);
            $form.find('#NoteUserId').val(note.Note.user_id);
            $form.find('#NoteLevel').val(note.Note.level).selectpicker('refresh');
            if (note.Note.sticky) {
              $form.find('#NoteSticky').bootstrapSwitch('toggleState', true, true);
            }
          },
          error: log_error,
        });
      }
      if (category) {
        $form.find('#NoteCategory').val(category);
      }

      $form.once().submit(function() {
        $.ajax({
          url: basePath + '/notes/add',
          type: 'post',
          dataType: 'json',
          data: $form.serialize(),
          success: function(data) {
            if (data.success) {
              $this.modal('hide');
              if (noteId) {
                toastr.success('La note a été modifiée.');
              } else {
                toastr.success('La note a été crée.');
              }
            }
          },
          error: log_error
        }).then(function() {
          if ($('[data-portlet-notes]').length) {
            $('[data-portlet-notes]').find('.reload').click();
          } else {
            window.location.reload();
          }
        });
        return false;
      });
    });

    $('body').on('click', '.edit-note', function(e) {
      e.preventDefault();
      $('#modal-note-list').modal('hide');
    });

    $('body').on('click', '.delete-note', function(e) {
      e.preventDefault();
      var $this = $(this);
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette note?', function(result) {
        if (result) {
          var noteId = $this.data('note-id');
          $.ajax({
            url: basePath + '/notes/delete',
            type: 'post',
            dataType: 'json',
            data: {
              id: noteId
            },
            success: function(note) {
              toastr.success('La note a été supprimée.');
              $('#modal-note-list').modal('hide');
              if ($('[data-portlet-notes]').length) {
                $('[data-portlet-notes]').find('.reload').click();
              } else {
                window.location.reload();
              }
            },
            error: log_error,
          });

        }
      });

    });

  };

  var handleEventWork = function() {

    handleOrderItems();

    function updatePortlet(portlet, callback) {
      if (portlet == 'orders') {
        var d = new Date();
        var n = d.getTime();
        $('#portlet-orders').removeClass('loaded').attr('data-time', n);
        return;
      }
      $('#portlet-' + portlet).find('.reload').trigger('click');
      if (callback) {
        callback();
      }
    }

    function computeSalary(line, update) {
      var jobId = line.data('job-id');
      var userId = line.data('user-id');
      var start = line.find('input#start').val();
      var end = line.find('input#end').val();
      var break_ = line.find('input#break').val();
      var salary = line.find('input#salary').val();
      var hours = line.find('td.hours').text();
      if (hours == '00:00' || update || !hours) {
        $.ajax({
          url: basePath + '/events/hours',
          type: 'post',
          dataType: 'json',
          data: {
            user_id: userId,
            job_id: jobId,
            start: start,
            break: break_,
            salary: salary,
            end: end
          },
          success: function(data) {
            if (data.success) {
              line.find('.hours').text(data.hours);
              line.find('.total').text(data.total);
              line.find('.salary').text(data.salary);
            }
          },
          error: log_error
        })
      }
    }

    $('#event-navigation a').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 80)
      }, 1000, 'easeInOutExpo', function() {
        $($anchor.attr('href')).addClass('focused');
      });
      event.preventDefault();
    });

    $(window).on('scroll', function() {
      if ($(window).scrollTop() > 350) {
        $('#portlet-downloads').addClass('portlet--side');
      } else {
        $('#portlet-downloads').removeClass('portlet--side');
      }
    });
    $(window).scroll();

    $('body').on('show.bs.collapse', '#portlet-recruiting [data-jobs-accordion] .panel-collapse', function() {

      var $this = $(this);
      var jobId = $this.data('job-id');
      var eventId = $this.data('event-id');

      if ($this.hasClass('loaded')) {
        return;
      }

      $.ajax({
        url: basePath + '/jobs/getStaff',
        type: 'post',
        dataType: 'html',
        data: {
          jobId: jobId,
          eventId: eventId
        },
        success: function(data) {
          $this.find('.panel-body').html(data);
          handleDatePickers();
        },
        error: log_error
      }).then(function() {
        $this.addClass('loaded');
      });

    });

    $('body').on('click', '.set-waiting', function(e) {
      e.preventDefault();
      var $this = $(this);
      var cell = $(this).parents('td');
      var roundAnswerId = cell.find('#RoundAnswerId').val();
      var answer = $this.attr('data-answer');
      if (roundAnswerId) {
        $.ajax({
          url: basePath + '/round_answers/set_to_waiting',
          type: 'post',
          dataType: 'json',
          data: {
            id: roundAnswerId,
            answer: answer
          },
          success: function(data) {
            if (data.success) {
              if (answer == 'waiting') {
                $this.find('i').removeClass('fa-minus').addClass('fa-clock-o');
                $this.attr('data-answer', 'null');
              } else {
                $this.find('i').removeClass('fa-clock-o').addClass('fa-minus');
                $this.attr('data-answer', 'waiting');
              }
              toastr.success('Sauvegardé');
            }
          },
          error: log_error
        });
      }
    });

    $('body').on('loaded.bs.modal', '.modal-job', function(e) {
      var modal = $(this);

      handleBootstrapSelect();

      setTimeout(function() {
        if (modal.data('edit')) {
          modal.find('.input-sector').trigger('change');
          modal.find('.input-jobs').trigger('change');
        } else {
          modal.find('.hide-on-add').hide();
        }
      }, 500);

      $('body').on('change', '#JobSector', function() {
        var $this = $(this);
        var sector = $this.find('option:selected').val();
        modal.find('.group-jobs').show();
        modal.find('.group-activities').hide();
        modal.find('.group-fb-modules').hide();
        modal.find('.input-jobs').hide();

        if (sector == 'animation') {
          modal.find('.input-animation-jobs').show();
          modal.find('.input-logistics-jobs option').removeAttr('selected');
          modal.find('.input-fb-jobs option').removeAttr('selected');
          modal.find('.input-other-jobs option').removeAttr('selected');

          $('body').on('change', '.input-animation-jobs', function() {
            modal.find('.group-activities').show();
          });
        }
        if (sector == 'logistics') {
          modal.find('.input-logistics-jobs').show();
          modal.find('.input-activities option').removeAttr('selected');
          modal.find('.input-fb-jobs option').removeAttr('selected');
          modal.find('.input-animation-jobs option').removeAttr('selected');
          modal.find('.input-other-jobs option').removeAttr('selected');
        }
        if (sector == 'other') {
          modal.find('.input-other-jobs').show();
          modal.find('.input-activities option').removeAttr('selected');
          modal.find('.input-fb-jobs option').removeAttr('selected');
          modal.find('.input-animation-jobs option').removeAttr('selected');
        }
        if (sector == 'fb') {
          modal.find('.input-fb-jobs').show();
          modal.find('.input-activities option').removeAttr('selected');
          modal.find('.input-logistics-jobs option').removeAttr('selected');
          modal.find('.input-animation-jobs option').removeAttr('selected');
          modal.find('.input-other-jobs option').removeAttr('selected');
          modal.find('.group-hierarchies').hide();
          $('body').on('change', '.input-fb-jobs', function() {
            modal.find('.group-fb-modules').show();
          });
        }
        $('.input-jobs').selectpicker('refresh');
        $('.input-activities').selectpicker('refresh');
        $('.input-fb-modules').selectpicker('refresh');

      });

    });

    var getOutfits = function() {
      var $modal = $('.modal.in');
      var $sector = $modal.find('#JobSector');
      var sector = $sector.find('option:selected').val();
      var $outfits = $modal.find('#JobOutfitId');
      $outfits.find('option').remove();

      if (sector == 'animation') {
        var modelId = $modal.find('#JobActivityId option:selected').val();
        if (!modelId) {
          $outfits.find('option').remove();
          return;
        }
        var data = {
          model_id: modelId,
          model: 'activity'
        };
      } else if (sector == 'fb') {
        var modelId = $modal.find('#JobFbModuleId option:selected').val();
        if (!modelId) {
          $outfits.find('option').remove();
          return;
        }
        var data = {
          model_id: modelId,
          model: 'fb_module'
        };
      } else {
        var data = {};
      }
      $.ajax({
        url: basePath + '/outfits/json',
        dataType: 'json',
        type: 'post',
        data: data,
        success: function(data) {
          var count = Object.keys(data).length;
          var $default = $outfits.data('default');
          if (count) {
            $.each(data, function(id, name) {
              if (id == $default) {
                $outfits.append('<option value="' + id + '" selected="selected">' + name + '</option>');
              } else {
                $outfits.append('<option value="' + id + '">' + name + '</option>');
              }

            })
          }
        },
        error: log_error
      }).then(function() {
        $outfits.selectpicker('refresh');
      });
    }

    $('body').on('change', '#JobSector, #JobActivityId, #JobFbModuleId', function() {
      getOutfits();
    });

    $('body').on('click', '.edit-job', function(e) {
      $('.panel-collapse').collapse('hide');
      // var target = $(this).data('target');
      // $('.modal' + target).modal('show');
      e.stopPropagation();
      e.preventDefault();
    });

    $('body').on('submit', '.form-job', function() {
      var form = $(this);
      var modal = form.parents('.modal.in');
      $.ajax({
        url: form.attr('action'),
        type: 'post',
        dataType: 'json',
        data: form.serialize(),
        success: function(data) {
          if (data.success) {
            //modal.find('select').val('').change();
            //window.location.reload();
            modal.modal('hide');
            updatePortlet('recruiting');
            updatePortlet('hours');
            updatePortlet('planning');
          }
        },
        error: log_error
      }).then(function() {
        //modal.find('form').trigger('reset');
      });
      return false;
    });

    $('body').on('hidden.bs.select', '.form-job #JobUserIds', function(e) {
      var $this = $(this);
      var ids = $this.val();
      var jobId = $this.parents('form').find('#JobId').val();
      var $feedback = $this.parents('.modal.in').find('#feedback');

      if (ids && jobId) {
        $.ajax({
          url: basePath + '/users/getAvailabilities',
          type: 'post',
          data: {
            ids: ids,
            job_id: jobId
          },
          success: function(data) {
            if (typeof data !== 'undefined') {
              $feedback.html(data);
            }
          },
          error: log_error
        });
      }
    });

    $('body').on('click', '.delete-job', function(e) {
      var $this = $(this);
      var modal = $this.parents('.modal.in');
      var jobId = $this.data('job-id');

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce poste?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/jobs/delete',
            type: 'post',
            dataType: 'json',
            data: {
              id: jobId
            },
            success: function(data) {
              if (data.success) {
                modal.modal('hide');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('recruiting');
            updatePortlet('hours');
            updatePortlet('planning');
          });
        }
      });
    });

    $('body').on('click', '.save-rounds', function(e) {
      e.preventDefault();
      var $this = $(this);
      var panel = $this.parents('.panel-collapse');
      $.ajax({
        url: basePath + '/round_answers/save',
        type: 'post',
        dataType: 'json',
        data: panel.find('form').serialize(),
        success: function(data) {
          if (data.success) {
            toastr.success('Les envois ont été sauvegardés.');
            updatePortlet('recruiting');
          } else {

          }
        },
        error: log_error
      });
    });

    $('body').on('change', '.round_step_checkbox', function(e) {
      var $this = $(this);
      var checked = $this.is(':checked');
      var cell = $this.parents('td');
      var row = $this.parents('tr');

      row.find('input[type="checkbox"]').removeAttr('checked');
      row.find('.checked').val(0);

      $this.attr('checked', checked);
      if ($this.is(':checked')) {
        cell.find('input.checked').val(1);
      } else {
        cell.find('input.checked').val(0);
      }
    });

    $('body').on('shown.bs.modal', '.modal-round-step', function() {
      $('.form-round-step').submit(function() {
        var form = $(this);
        var modal = form.parents('.modal.in');
        $.ajax({
          url: form.attr('action'),
          type: 'post',
          dataType: 'json',
          data: form.serialize(),
          success: function(data) {
            if (data.success) {
              modal.modal('hide');
              updatePortlet('recruiting');
              toastr.success('La salve a été sauvegardée.');
            }
          },
          error: log_error
        });
        return false;
      });
    });

    $('body').on('change', '#activities', function() {
      var activityId = $(this).val();
      var eventId = $('#EventId').val();
      $.ajax({
        url: basePath + '/events/add_activity',
        type: 'post',
        dataType: 'json',
        data: {
          activity_id: activityId,
          event_id: eventId
        },
        success: function(data) {
          if (data.success) {
            updatePortlet('activities');
            updatePortlet('planning');
            toastr.success("L'activité a été correctement ajoutée.");
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.select-activity', function(e) {
      e.preventDefault();
      var id = $(this).data('activity-event-id');
      var oldStatus = $(this).data('status');

      bootbox.confirm('Êtes-vous sûr de vouloir (dé)sélectionner l\'activité?', function(result) {
        if (result) {
          $('#loading').fadeIn();
          $.ajax({
            url: basePath + '/events/select_activity',
            type: 'post',
            dataType: 'json',
            data: {
              id: id,
              status: oldStatus
            },
            success: function(data) {
              if (data.success) {
                updatePortlet('activities');
                updatePortlet('orders');
                updatePortlet('planning');
                if (oldStatus == 'suggested') {
                  toastr.success("L'activité a été sélectionnée.");
                } else {
                  toastr.success("L'activité n'est plus sélectionnée.");
                }
              }
            },
            error: log_error
          }).then(function() {
            $('#loading').fadeOut();
          });
        }
      });

    });

    $('body').on('click', '.delete-activity', function(e) {
      e.preventDefault();
      var id = $(this).data('activity-event-id');

      if (!id) {
        return;
      }

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette activité?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/events/delete_activity',
            type: 'post',
            dataType: 'json',
            data: {
              id: id
            },
            success: function(data) {
              if (data.success) {
                updatePortlet('activities');
                updatePortlet('moments');
                updatePortlet('orders');
                updatePortlet('planning');
                toastr.success("L'activité a été supprimée.");
              }
            },
            error: log_error
          });
        }
      });

    });

    $('body').on('change', '.table-activity-infos input', function() {
      var $this = $(this);
      var id = $this.parents('tr').data('activity-event-id');
      var field = $this.data('field');
      var value = $this.val();

      if (!id) {
        return;
      }

      $.ajax({
        url: basePath + '/activities/updateInfoField',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: value
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La valeur a été correctement sauvegardée.')
          }
        },
        error: log_error
      }).then(function() {
        // updatePortlet('orders');
        // $this.parents('tr').find('.update-orders').click();
      })
    });

    $('body').on('change', '.table-fb-module-infos input', function() {
      var $this = $(this);
      var id = $this.parents('tr').data('event-fb-module-id');
      var field = $this.data('field');
      var value = $this.val();

      if (!id) {
        return;
      }

      $.ajax({
        url: basePath + '/f_b_modules/updateInfoField',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: value
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La valeur a été correctement sauvegardée.')
          }
        },
        error: log_error
      }).then(function() {
        // updatePortlet('orders');
      })
    });

    // $('body').on('click', '#portlet-activities .update-orders', function(e) {
    //   e.preventDefault();
    //   var $this = $(this);
    //   var $row = $this.parents('tr');
    //   $row.find('.table-activity-infos input:first').trigger('change');
    // });
    //
    // $('body').on('click', '#portlet-fb-modules .update-orders', function(e) {
    //   e.preventDefault();
    //   var $this = $(this);
    //   var $row = $this.parents('tr');
    //   $row.find('.table-fb-module-infos input:first').trigger('change');
    // });

    $('body').on('change', '#fb_modules', function() {
      var fbModuleId = $(this).val();
      var eventId = $('#EventId').val();
      $.ajax({
        url: basePath + '/events/add_fb_module',
        type: 'post',
        dataType: 'json',
        data: {
          fb_module_id: fbModuleId,
          event_id: eventId
        },
        success: function(data) {
          if (data.success) {
            updatePortlet('fb-modules');
            updatePortlet('moments');
            toastr.success("Le module F&B a été correctement ajoutée.");
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.select-fb-module', function(e) {
      e.preventDefault();
      var id = $(this).data('event-fb-module-id');
      var oldStatus = $(this).data('status');

      bootbox.confirm('Êtes-vous sûr de vouloir (dé)sélectionner le module F&B?', function(result) {
        $('#loading').fadeIn();
        $.ajax({
          url: basePath + '/events/select_fb_module',
          type: 'post',
          dataType: 'json',
          data: {
            id: id,
            status: oldStatus
          },
          success: function(data) {
            if (data.success) {
              updatePortlet('fb-modules');
              updatePortlet('orders');
              updatePortlet('planning');
              if (oldStatus == 'suggested') {
                toastr.success("Le module F&B a été sélectionnée.");
              } else {
                toastr.success("Le module F&B n'est plus sélectionnée.");
              }
            }
          },
          error: log_error
        }).then(function() {
          $('#loading').fadeOut();
        });
      });
    });

    $('body').on('click', '.delete-fb-module', function(e) {
      e.preventDefault();
      var id = $(this).data('event-fb-module-id');

      $.ajax({
        url: basePath + '/events/delete_fb_module',
        type: 'post',
        dataType: 'json',
        data: {
          id: id
        },
        success: function(data) {
          if (data.success) {
            updatePortlet('fb-modules');
            updatePortlet('moments');
            updatePortlet('orders');
            updatePortlet('planning');
            toastr.success("Le module F&B a été supprimée.");
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.insert-hours', function(e) {
      e.preventDefault();
      $('#portlet-hours').toggleClass('hidden');
      if ($('#portlet-hours').is(':visible')) {
        updatePortlet('hours');
      }
    });

    $('body').on('click', '.delete-moment', function(e) {
      e.preventDefault();
      var momentId = $(this).data('moment-id');
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce moment?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/moments/delete',
            type: 'post',
            dataType: 'json',
            data: {
              id: momentId
            },
            success: function(data) {
              if (data.success) {
                toastr.success('Le moment a été correctement supprimé.')
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('moments');
          })
        }
      });
    });

    $('body').on('change', '#vehicle_families', function() {
      var family = $(this).val();
      var eventId = $('#EventId').val();
      $.ajax({
        url: basePath + '/events/add_vehicle_family',
        type: 'post',
        dataType: 'json',
        data: {
          family: family,
          event_id: eventId
        },
        success: function(data) {
          if (data.success) {
            updatePortlet('vehicles');
            updatePortlet('planning');
            toastr.success("La demande a été enregistrée.");
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.delete-vehicle-reservation', function(e) {
      e.preventDefault();
      var reservationId = $(this).data('vehicle-reservation-id');
      var eventId = $('#EventId').val();

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette réservation?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/vehicle_reservations/delete',
            type: 'post',
            dataType: 'json',
            data: {
              reservationId: reservationId,
              eventId: eventId
            },
            success: function(data) {
              if (data.success) {
                updatePortlet('vehicles');
                toastr.success("La réservation a été supprimée.");
              }
            },
            error: log_error
          });
        }
      });
    });

    $('body').on('click', '.cancel-vehicle-reservation', function(e) {
      e.preventDefault();
      var reservationId = $(this).data('vehicle-reservation-id');
      var eventId = $('#EventId').val();

      bootbox.confirm('Êtes-vous sûr de vouloir annuler cette réservation?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/vehicle_reservations/cancel',
            type: 'post',
            dataType: 'json',
            data: {
              reservationId: reservationId,
              eventId: eventId
            },
            success: function(data) {
              if (data.success) {
                updatePortlet('vehicles');
                updatePortlet('planning');
                toastr.success("La réservation a été annulée.");
              }
            },
            error: log_error
          });
        }
      });
    });

    $('body').on('change', '#portlet-vehicles .assign-driver', function() {
      var $this = $(this);
      var driverId = $this.val();
      var reservationId = $this.parents('tr').data('vehicle-reservation-id');
      var field = $this.data('field');
      $.ajax({
        url: basePath + '/events/assign_driver',
        type: 'post',
        dataType: 'json',
        data: {
          driver_id: driverId,
          id: reservationId,
          field: field
        },
        success: function(data) {
          if (data.success) {
            updatePortlet('vehicles');
            if (driverId) {
              toastr.success('Le chauffeur a été attribué.');
            } else {
              toastr.success('Le chauffeur a été supprimé.');
            }
          }
        },
        error: log_error
      });
    });

    $('body').on('change', '.vehicle-reservation-schedule', function() {
      var $this = $(this);
      var date = $this.val();
      var field = $this.data('field');
      var id = $this.parents('tr').data('vehicle-reservation-id');
      $.ajax({
        url: basePath + '/vehicle_reservations/update_schedule',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          date: date
        },
        success: function(data) {
          if (data.success) {
            toastr.success("L'horaire de la réservation a été modifié.");
          }
        },
        error: log_error
      });
    });

    $('body').on('change', '.vehicle-reservation-place', function() {
      var $this = $(this);
      var place = $this.val();
      var field = $this.data('field');
      var id = $this.parents('tr').data('vehicle-reservation-id');
      $.ajax({
        url: basePath + '/vehicle_reservations/update_place',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          place: place
        },
        success: function(data) {
          if (data.success) {
            toastr.success("Le lieu de prise/remise de la réservation a été modifié.");
          }
        },
        error: log_error
      });
    });

    $('body').on('mouseenter', '#portlet-planning .table .badge', function() {
      var $this = $(this);
      var model = $this.data('model');
      var model_id = $this.data('model-id');
      $('.planning-resources .tag[data-model="' + model + '"][data-model-id="' + model_id + '"]').focus();
    }).on('mouseleave', '#portlet-planning .table .badge', function() {
      var $this = $(this);
      var model = $this.data('model');
      var model_id = $this.data('model-id');
      $('.planning-resources .tag[data-model="' + model + '"][data-model-id="' + model_id + '"]').blur();
    });

    $('body').on('click', '.planning-resources .tag', function(e) {
      var $this = $(this);
      var model = $this.data('model');
      var modelId = $this.data('model-id');
      var $tab = $this.parents('.tab-pane.active');
      if ($this.hasClass('tag--active')) {
        $tab.find('.table-planning tbody tr').show();
        $this.removeClass('tag--active');
        return;
      }
      $('.planning-resources .tag').removeClass('tag--active');
      $this.addClass('tag--active');
      $tab.find('.table-planning tbody tr').each(function(i, e) {
        var $row = $(e);
        var resources = $row.find('.who .badge[data-model="' + model + '"][data-model-id="' + modelId + '"]').get();
        if (resources.length) {
          $row.show();
        } else {
          $row.hide();
        }
      });
    });

    $('body').on('loaded.bs.modal', '#modal-planning-moment', function() {
      handleDatePickers();
      var $modal = $(this);

      $modal.find('[type="radio"]').uniform();
      $modal.find('[type="checkbox"]').bootstrapSwitch();

      $modal.on('click', '[type="radio"]', function() {
        var $this = $(this);
        var $name = $modal.find('#PlanningBoardMomentName');
        if (!$name.val()) $name.val($this.parents('label').text().trim());
      });

      $modal.find('form').submit(function() {
        var $form = $(this);
        $.ajax({
          url: $form.attr('action'),
          type: 'post',
          dataType: 'json',
          data: $form.serialize(),
          success: function(data) {
            if (data.success) {
              toastr.success('Le moment a été correctement sauvegardé.');
            }
          },
          error: log_error
        }).then(function() {
          $modal.modal('hide');
          updatePortlet('planning', function() {
            setActiveBoardTab();
          });
        });
        return false;
      });

    });

    var setActiveBoardTab = function() {
      $(document).ajaxStop(function() {
        var activeBoardId = $('#portlet-planning #planning_active_board').val();
        if (activeBoardId) {
          $('#portlet-planning .nav-tabs li').removeClass('active');
          $('#portlet-planning .tab-pane').removeClass('active');
          $('#portlet-planning [data-board="' + activeBoardId + '"]').parents('li').addClass('active');
          $('#portlet-planning #board' + activeBoardId + '').addClass('active');
        }
      });
    }

    $('body').on('loaded.bs.modal', '#modal-planning-board', function() {
      var $modal = $(this);

      $modal.find('[type="radio"]').uniform();

      $modal.find('form').submit(function() {
        var $form = $(this);
        $.ajax({
          url: $form.attr('action'),
          type: 'post',
          dataType: 'json',
          data: $form.serialize(),
          success: function(data) {
            if (data.success) {
              toastr.success('La planche a été correctement sauvegardée.');
            }
          },
          error: log_error
        }).then(function() {
          $modal.modal('hide');
          updatePortlet('planning', function() {
            setActiveBoardTab();
          });
        });
        return false;
      });

    });

    $('body').on('click', '#portlet-closing .delete-document', function(e) {
      e.preventDefault();
      var documentId = $(this).data('document-id');
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce document?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/documents/delete',
            type: 'post',
            dataType: 'json',
            data: {
              documentId: documentId
            },
            success: function(data) {
              if (data.success) {
                toastr.success('Le document a été correctement supprimé.')
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('closing');
          })
        }
      });
    });

    $('body').on('click', '#portlet-closing .rename-document', function(e) {
      e.preventDefault();
      var documentId = $(this).data('document-id');
      var value = $(this).data('document-name');
      bootbox.prompt({
        title: 'Veullez indiquer le nouveau nom de fichier.',
        value: value,
        callback: function(result) {
          if (result) {
            $.ajax({
              url: basePath + '/documents/edit/' + documentId + '/' + result,
              type: 'post',
              dataType: 'json',
              success: function(data) {
                if (data.success) {
                  toastr.success('Le document a été correctement renommé.')
                }
              },
              error: log_error
            }).then(function() {
              updatePortlet('closing');
            })
          }
        }
      });
    });

    var updateChecklistProgress = function(id, heading, key) {
      if (!key) {
        key = 'task';
      }
      $.ajax({
        url: basePath + '/checklist_batches/progress',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          key: key
        },
        success: function(data) {
          if (data.success) {
            heading.removeClass(function(index, css) {
              return (css.match(/\bpanel-heading-width-\S+/g) || []).join(' ');
            });
            heading.addClass('panel-heading-width-' + data.progress);
            heading.find('.closed_tasks').text(data.closed_tasks);
            if (data.progress == 100) {
              heading.find('.complete-batch').removeClass('complete-batch--hidden').addClass('complete-batch--visible');
            } else {
              heading.find('.complete-batch').removeClass('complete-batch--visible').addClass('complete-batch--hidden');
            }
          }
        }
      });
    }

    $('body').on('change', '#portlet-tasks .task-list li input[type="checkbox"]', function() {
      var $this = $(this);
      var $row = $this.parents('li');
      var checked = $this.is(':checked');
      var id = $row.data('checklist-task-id');
      var $heading = $row.parents('.panel').find('.panel-heading');
      if (!id) {
        return;
      }
      $this.parents('li').find('.task-title-sp').toggleClass('task-done');
      $.ajax({
        url: basePath + '/checklist_tasks/done',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          done: checked ? 1 : 0
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La tâche a été sauvegardée.');
          }
        },
        error: log_error
      }).then(function() {
        if ($heading.length) updateChecklistProgress(id, $heading, 'task');
      });
    });

    $('body').on('change', 'select.insert-checklist', function(e) {
      e.preventDefault();
      var $this = $(this);
      var id = $this.val();
      var event_id = $('#EventId').val();

      if (!id) {
        return;
      }
      $this.selectpicker('deselectAll');
      selectChecklist(id, event_id);
    });

    $('body').on('click', 'a.insert-checklist', function(e) {
      e.preventDefault();
      var $this = $(this);
      var id = $this.data('checklist-id');
      var event_id = $('#EventId').val();
      if (!id) {
        return;
      }
      var $modal = $this.parents('.modal.in');
      $modal.modal('hide');
      selectChecklist(id, event_id);
    });

    function selectChecklist(id, event_id) {
      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/checklists/select/' + id + '/' + event_id,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success('La liste de tâches a été correctement attribuée');
          }
        },
        error: log_error
      }).then(function() {
        $('#loading').fadeOut();
        updatePortlet('tasks');
      });
    }

    $('body').on('blur', '#portlet-tasks .new-task-input input', function() {
      var $this = $(this);
      var name = $this.val();
      var checklist_batch_id = $this.data('checklist-batch-id');
      var panel = $this.parents('.panel');
      var weight = panel.find('.task-list li:not(.empty, .new-task)').get().length;
      if (!name) {
        return;
      }
      $.ajax({
        url: basePath + '/checklist_tasks/add',
        type: 'post',
        dataType: 'json',
        data: {
          name: name,
          checklist_batch_id: checklist_batch_id,
          weight: weight
        },
        success: function(data) {
          if (data.success) {
            updatePhase(panel, checklist_batch_id);
          }
        }
      });
    });

    $('body').on('keypress', '#portlet-tasks .new-task-input input, #portlet-tasks .new-batch', function(event) {
      if (event.keyCode == 10 || event.keyCode == 13) {
        $(this).trigger('blur');
      }
    });

    $('body').on('blur', '#portlet-tasks .new-batch', function() {
      var $this = $(this);
      var name = $this.val();
      var href = $this.data('href');
      if (!name || !href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le groupe de tâches a été correctement ajouté.');
          }
        },
        error: log_error
      }).then(function() {
        updatePortlet('tasks');
      });
    });

    var updatePhase = function(panel, batch_id) {
      $.ajax({
        url: basePath + '/checklist_batches/html',
        type: 'post',
        data: {
          id: batch_id
        },
        success: function(data) {
          panel.before(data);
          panel.remove();
          $('#portlet-tasks [type="checkbox"]').uniform('refresh');
        }
      });
    }

    $('body').on('click', '.delete-checklist', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');

      if (!href) {
        return;
      }

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette checklist?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('La checklist a bien été effacée.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('tasks');
          });
        }
      });
    });

    $('body').on('click', '#portlet-tasks .delete-batch', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');

      if (!href) {
        return;
      }

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce groupe de tâches?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('Le groupe de tâches a bien été effacé.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('tasks');
          });
        }
      });
    });

    $('body').on('click', '.delete-task', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      var row = $this.parents('li');
      var panel = $this.parents('.panel');
      var id = $this.data('checklist-batch-id');
      var $heading = $this.parents('.panel').find('.panel-heading');
      if (!href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            row.fadeOut();
          }
        },
        error: log_error
      }).then(function() {
        var phase = panel.find('.task-list li:first').data('checklist-batch-id');
        updatePhase(panel, phase);
        if ($heading.length) updateChecklistProgress(id, $heading, 'batch');
      });
    });

    $('body').on('changeDate', '#portlet-tasks .checklist-batch-context .due-date-picker', function(e) {
      var $this = $(this);
      var panel = $this.parents('.panel');
      var batch = $this.data('checklist-batch-id');
      $.ajax({
        url: basePath + '/checklist_batches/setTasksDueDate',
        type: 'post',
        dataType: 'json',
        data: {
          date: e.format('yyyy-mm-dd'),
          id: batch
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La date a été correctement attribué aux tâches du groupe.');
          }
        },
        error: log_error
      }).then(function() {
        updatePhase(panel, batch);
      });
    });

    $('body').on('changeDate', '#portlet-tasks .due-date-picker', function(e) {
      var $this = $(this);
      var panel = $this.parents('.panel');
      var task = $this.data('checklist-task-id');
      var batch = $this.data('checklist-batch-id');
      updateDueDate(task, e.format('yyyy-mm-dd'), panel, batch);
    });

    $('body').on('clear', '#portlet-tasks .due-date-picker', function(e) {
      var $this = $(this);
      var panel = $this.parents('.panel');
      var task = $this.data('checklist-task-id');
      var batch = $this.data('checklist-batch-id');
      updateDueDate(task, '', panel, batch);
    });

    // avoid context menu close on month switch
    $('body').on('click', '#portlet-tasks .due-date-picker .prev, #portlet-tasks .due-date-picker .next', function(e) {
      e.preventDefault();
      e.stopPropagation();
    })

    function updateDueDate(task, date, panel, batch) {
      if (!task) {
        return;
      }
      $.ajax({
        url: basePath + '/checklist_tasks/updateField',
        type: 'post',
        dataType: 'json',
        data: {
          name: 'due_date',
          value: date,
          pk: task
        },
        success: function(data) {

        },
        error: log_error
      }).then(function() {
        updatePhase(panel, batch);
      });
    }

    $('body').on('change', 'select.insert-order', function(e) {
      e.preventDefault();
      var $this = $(this);
      var id = $this.val();
      var event_id = $('#EventId').val();

      if (!id) {
        return;
      }
      selectOrder(id, event_id);
    });

    function selectOrder(id, event_id) {
      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/orders/select/' + id + '/' + event_id,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success('La commande a été correctement attribuée');
          }
        },
        error: log_error
      }).then(function() {
        $('#loading').fadeOut();
        updatePortlet('orders');
      });
    }

    $('body').on('change', '#portlet-orders .table-order-items--portlet input', function() {
      return;
      var $this = $(this);
      var val = $this.val();
      var id = $this.data('order-item-id');
      var field = $this.data('field');
      var href = $this.data('href');

      if (!val || !href) {
        return;
      }

      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: val
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La valeur a été correctement modifiée.');
          }
        },
        error: log_error
      }).then(function() {
        //updateUnavailability(id, val);
      });

    });

    function updateUnavailability(itemId, quantity) {
      $.ajax({
        url: basePath + '/order_items/update_unavailability',
        type: 'post',
        dataType: 'json',
        data: {
          id: itemId
        },
        success: function(data) {
          console.log(data);
        },
        error: log_error
      })
    }

    $('body').on('change', '.non-standard-module', function() {
      var $this = $(this);
      var name = $this.val();
      var id = $this.data('event-fb-module-id');

      if (!name) {
        return;
      }
      $.ajax({
        url: basePath + '/f_b_modules/updateNonStandardName',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le module F&B a été sauvegardé.');
          }
        },
        error: log_error
      }).then(function() {
        updatePortlet('fb-modules');
        updatePortlet('moments');
      });
    });

    $('body').on('change', '.non-standard-activity', function() {
      var $this = $(this);
      var name = $this.val();
      var activity_event_id = $this.data('activity-event-id');

      if (!name) {
        return;
      }
      $.ajax({
        url: basePath + '/activities/updateNonStandardName',
        type: 'post',
        dataType: 'json',
        data: {
          activity_event_id: activity_event_id,
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success("L'activité a été sauvegardée.");
          }
        },
        error: log_error
      }).then(function() {
        updatePortlet('activities');
        updatePortlet('moments');
      });
    });

    $('body').on('click', '.delete-planning-board', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.data('href') ? $this.data('href') : $this.attr('href');

      if (!href) {
        return;
      }

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette planche?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('La planche a bien été supprimée.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('planning', function() {
              setActiveBoardTab();
            });
          });
        }
      });
    });

    $('body').on('click', '.delete-planning-moment', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.data('href') ? $this.data('href') : $this.attr('href');
      $
      var $modal = $('.modal.in');

      if (!href) {
        return;
      }

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce moment?', function(result) {
        $modal.modal('hide');
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('Le moment a bien été supprimé.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('planning', function() {
              setActiveBoardTab();
            });
          });
        }
      });
    });

    $('body').on('change', '[data-hours] input[type="checkbox"]', function() {
      var line = $(this).parents('tr');
      var jobId = line.data('job-id');
      var userId = line.data('user-id');
      var checked = $(this).find('span:first').hasClass('checked');
      var checked = $(this).is(':checked');
      if (checked) {
        var validated = 1;
      } else {
        var validated = 0;
      }
      $.ajax({
        url: basePath + '/events/validate_hours',
        type: 'post',
        dataType: 'json',
        data: {
          user_id: userId,
          job_id: jobId,
          validated: validated
        },
        beforeSend: function() {
          if (validated) computeSalary(line, true);
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Les heures sont sauvegardées.');
          }
        },
        error: log_error
      }).then(function() {
        updatePortlet('hours');
      });
    });

    $('body').on('hide.timepicker', '#portlet-hours [data-hours] .timepicker-24', function() {
      computeSalary($(this).parents('tr'), true);
    });
    $('body').on('blur', '#portlet-hours [data-hours] .salary', function() {
      computeSalary($(this).parents('tr'), true);
    });
    $('body').on('keypress', '#portlet-hours .timepicker-24', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode == 9) {
        $(this).timepicker('hideWidget');
      }
    });

    var saveRoute = function(id, field, value) {
      if (!id) {
        return;
      }
      $.ajax({
        url: basePath + '/jobs/save_route',
        type: 'post',
        dataType: 'json',
        data: {
          id: id,
          field: field,
          value: value
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le trajet a été sauvegardé.');
          }
        },
        error: log_error
      });
    }

    $('body').on('change', '#portlet-recruiting [data-routes] input[type="radio"]', function() {
      var $this = $(this);
      var $row = $this.parents('tr');
      var id = $row.data('job-user-id');
      var field = 'route';
      var value = $this.val();
      saveRoute(id, field, value);
    });

    $('body').on('hide.timepicker', '#portlet-recruiting [data-routes] .timepicker-24', function() {
      var $row = $(this).parents('tr');
      var id = $row.data('job-user-id');
      var field = 'route_duration';
      var value = $(this).val();
      saveRoute(id, field, value);
    });

    $('body').on('click', '[data-download-item]', function(e) {
      e.preventDefault();
      $this = $(this);
      var toggle = $this.data('toggle');
      var callback = $this.data('callback');
      var href = $this.data('href');

      if (toggle == 'modal') {

      } else {
        window.open(href, '_blank');
      }

    });

    $('body').on('loaded.bs.modal', '[data-download-modal]', function() {
      var $modal = $(this);
      var $form = $modal.find('form');
      $form.once().submit(function() {
        console.log($form.serialize());
        return false;
      });
    });

    $('body').on('loaded.bs.modal', '#modal-add-job-briefing', function() {
      var $modal = $(this);
      CKEDITOR.config.height = 500;
      CKEDITOR.replace('ckeditor');

      var $form = $modal.find('form');
      $form.once().submit(function() {
        var content = CKEDITOR.instances.ckeditor.getData();
        $form.find('textarea').val(content);
        $.ajax({
          url: $form.attr('action'),
          type: 'post',
          data: $form.serialize(),
          dataType: 'json',
          success: function(data) {
            if (data.success) {
              toastr.success('Le briefing a bien été sauvegardé.');
              updatePortlet('recruiting');
            }
          }
        }).then(function() {
          $modal.modal('hide');
        });
        return false;
      });
    });

    $('body').on('click', '.delete-job-briefing', function(e) {
      var $this = $(this);
      var modal = $this.parents('.modal.in');
      var id = $this.data('job-briefing-id');

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce briefing?', function(result) {
        if (result) {
          $.ajax({
            url: basePath + '/job_briefings/delete',
            type: 'post',
            dataType: 'json',
            data: {
              id: id
            },
            success: function(data) {
              if (data.success) {
                modal.modal('hide');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('recruiting');
          });
        }
      });
    });


    $(document).ajaxStop(function() {
      handleDatePickers();
      handleBootstrapSelect();
      handleCRMEditable();

      var myDropzone;
      if ($('.dropzone:not(.dz-clickable)').length) {
        myDropzone = new Dropzone(".dropzone");
        myDropzone.on('queuecomplete', function() {
          updatePortlet('closing', function() {
            myDropzone.destroy();
          });
        });
      }

      if ($('#portlet-orders:not(.loaded)').length) {
        // should be loaded only once
        $('body').off('click', '.delete-order-item');
        $('body').off('click', '.delete-order-group');
        $('body').off('click', '.delete-order');
        $('body').off('click', '.update-orders');
        $('body').off('click', '.add-simple-product');
        $('body').off('click', '.set-editable-order-item');
        $('body').off('click', '.lock-order-item');
        $('body').off('blur', '.rename-order-group');
        $('body').off('blur', '.order-group-name input');
        // handleOrderItems();
      }

      $('body').on('blur', '.modal-job input:not(.timepicker-24)', function() {
        $('.modal-job .timepicker-24').timepicker('hideWidget');
      });

      $("#portlet-planning ul.task-list").sortable({
        revert: true,
        items: ".sortable",
        placeholder: "empty-feed",
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height() + 2);
        },
        stop: function(event, ui) {
          var moments = [];
          $('ul.task-list>li').each(function(i, e) {
            moments.push($(e).data('moment-id'));
          });

          $.ajax({
            url: basePath + '/moments/updateWeights',
            type: 'post',
            dataType: 'json',
            data: {
              moments: moments
            },
            success: function(data) {
              updatePortlet('moments');
            },
            error: log_error
          });
        }
      });
      $("#portlet-planning ul.tasks").sortable({
        revert: true,
        items: ".task",
        placeholder: "empty-task",
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height() + 2);
        },
        stop: function(event, ui) {
          var tasks = [];
          $('ul.tasks>li').each(function(i, e) {
            tasks.push($(e).data('moment-task-id'));
          });

          $.ajax({
            url: basePath + '/moment_tasks/updateWeights',
            type: 'post',
            dataType: 'json',
            data: {
              tasks: tasks
            },
            success: function(data) {
              updatePortlet('moments');
            },
            error: log_error
          });
        }
      });

      $("#portlet-tasks ul.task-list").sortable({
        revert: true,
        items: ".sortable",
        placeholder: "empty-feed",
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height() + 2);
        },
        stop: function(event, ui) {
          var tasks = [];
          var panel = ui.item.parents('.panel');
          var batch_id = ui.item.data('checklist-batch-id');

          ui.item.parents('ul').find('li.sortable').each(function(i, e) {
            tasks.push($(e).data('checklist-task-id'));
          });
          $.ajax({
            url: basePath + '/checklist_tasks/updateWeights',
            type: 'post',
            dataType: 'json',
            data: {
              tasks: tasks
            },
            success: function(data) {
              updatePhase(panel, batch_id);
            },
            error: log_error
          });
        }
      });

      $('body').on('contextmenu', '[data-target="#momentContext"]', function(e) {
        e.preventDefault();
        $('#momentContext li a').attr('data-moment-id', $(this).data('moment-id'));
      });

      // $('body').on('click', '[data-target="#edit-moment"]', function(e){
      // 	e.stopPropagation();
      // 	var target = $(this).data('target');
      // 	$('.modal' + target).modal('show');
      // });

      $('body').on('click', '[data-target="#edit-moment"]', function(e) {
        var target = $(this).attr("href");
        target = target + '/' + $(this).data('moment-id');
        $("#edit-moment .modal-body").load(target, function() {
          $("#edit-moment").modal("show");
        });
      });

      $('body').on('shown.bs.modal', '#edit-moment', function(e) {
        //App.initComponents();
      });

      // $('.form-job').submit(function(){
      // 	var form = $(this);
      // 	var modal = form.parents('.modal.in');
      // 	$.ajax({
      // 	  url: form.attr('action'),
      // 	  type: 'post',
      // 	  dataType: 'json',
      // 	  data: form.serialize(),
      // 	  success: function(data){
      // 			if(data.success){
      // 			  modal.modal('hide');
      // 			  updatePortlet('recruiting');
      // 			}
      // 	  },
      // 	  error: log_error
      // 	});
      // 	return false;
      // });

      $('#portlet-closing #feedback1').editable({
        disabled: false,
        url: basePath + '/events/updateFeedback',
        title: 'Modifier le feedback',
        rows: 6,
        mode: 'inline',
        emptytext: 'Vide',
        ajaxOptions: {
          dataType: 'json',
          type: 'post'
        },
        success: function(data, newValue) {
          if (data.success) {
            updatePortlet('closing');
          }
        }
      });

      $('#portlet-accounting .editable[data-name="project_in_wealthings"]').editable({
        source: [{
          value: 0,
          text: 'Non'
        }, {
          value: 1,
          text: 'Oui'
        }],
        url: basePath + '/events/updateField',
        //mode: 'inline',
        emptytext: 'vide',
        ajaxOptions: {
          dataType: 'json',
          type: 'post'
        },
        success: function(data, newValue) {
          if (data.success) {
            updatePortlet('accounting');
          }
        }
      });
      $('[data-portlet-editable]').each(function(i, e) {
        var $portlet = $(e);
        var portletName = $portlet.data('portlet-name');
        $portlet.find('.editable').editable({
          url: basePath + '/events/updateField',
          //mode: 'inline',
          emptytext: 'vide',
          ajaxOptions: {
            dataType: 'json',
            type: 'post',
            success: function() {
              $('#portlet-activities .update-orders:first').click();
            }
          },
          success: function(data, newValue) {
            if (data.success) {
              updatePortlet(portletName);
            }
          }
        });
      });
      $('#portlet-tasks .editable').editable({
        url: basePath + '/checklist_tasks/updateField',
        mode: 'inline',
        showbuttons: false,
        ajaxOptions: {
          dataType: 'json',
          type: 'post'
        },
        success: function(data, newValue) {
          if (data.success) {
            toastr.success('La tâche a été sauvegardée.');
          }
        },
        error: log_error
      });

      $('#portlet-planning .planning-resources:not(.planning-resources--legend) li').draggable({
        revert: true
      });

      $('#portlet-planning .table-planning .badge').draggable({
        revert: 'invalid',
        zIndex: 100
      });

      $('#portlet-planning .table-planning tbody tr').droppable({
        accept: "#portlet-planning .planning-resources li",
        hoverClass: "ui-state-active",
        drop: function(event, ui) {
          var $resource = $(ui.draggable);
          var $resourceLink = $resource.find('a');
          var $row = $(this);

          $.ajax({
            url: basePath + '/planning_resources/add',
            type: 'post',
            dataType: 'json',
            data: {
              resource_model: $resourceLink.data('model'),
              resource_id: $resourceLink.data('model-id'),
              planning_board_moment_id: $row.data('planning-board-moment-id')
            },
            success: function(data) {
              if (data.success) {
                toastr.success('La resource a été correctement ajoutée.');
              }
              if (data.existing) {
                toastr.warning('La resource est déjà présente sur ce moment.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('planning', function() {
              setActiveBoardTab();
            });
          });
        }
      });

      $('#portlet-planning .planning-resources').droppable({
        accept: "#portlet-planning .table-planning .badge",
        tolerance: 'intersect',
        over: function(event, ui) {
          $('.ui-draggable-dragging').addClass('deletable');
        },
        out: function(event, ui) {
          $('.ui-draggable-dragging').removeClass('deletable');
        },
        drop: function(event, ui) {
          var $resource = $(ui.draggable);
          $resource.addClass('dropped');
          $.ajax({
            url: basePath + '/planning_resources/delete',
            type: 'post',
            dataType: 'json',
            data: {
              resource_id: $resource.data('resource-id')
            },
            success: function(data) {
              console.log(data);
            },
            error: log_error
          }).then(function() {
            updatePortlet('planning', function() {
              setActiveBoardTab();
            });
          });
        }
      });

      $("#portlet-planning .table-planning").each(function(i, e) {
        $(e).sortable({
          revert: true,
          items: ".sortable",
          placeholder: "empty-row",
          forceHelperSize: true,
          start: function(e, ui) {
            ui.placeholder.height(ui.item.height());
            ui.placeholder.width(ui.item.width());
          },
          stop: function(event, ui) {
            var moments = [];

            ui.item.parents('tbody').find('.sortable').each(function(i, e) {
              moments.push($(e).data('planning-board-moment-id'));
            });
            $.ajax({
              url: basePath + '/planning_board_moments/updateWeights',
              type: 'post',
              dataType: 'json',
              data: {
                moments: moments
              },
              success: function(data) {
                if (data.success) {
                  toastr.success("L'ordre des moments a été sauvegardé.");
                }
              },
              error: log_error
            });
          }
        });
      });

      $("#portlet-planning .nav").sortable({
        revert: true,
        items: "li",
        placeholder: "empty-tab",
        forceHelperSize: true,
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height());
          ui.placeholder.width(ui.item.width() + 2);
        },
        stop: function(event, ui) {
          var boards = [];

          ui.item.parents('.nav').find('li > a').each(function(i, e) {
            boards.push($(e).data('board'));
          });

          $.ajax({
            url: basePath + '/planning_boards/updateWeights',
            type: 'post',
            dataType: 'json',
            data: {
              boards: boards
            },
            success: function(data) {
              toastr.success("L'ordre des planches a été sauvegardé.");
            },
            error: log_error
          });
        }
      });

      $('#portlet-planning a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $('#portlet-planning #planning_active_board').val($(this).data('board'));
      })
    });

    $('body').on('click', '.force-round-send', function(e) {
      e.preventDefault();
      var form = $(this);
      var modal = form.parents('.modal.in');
      var id = modal.find('#RoundStepId').val();
      $.ajax({
        url: basePath + '/round_steps/send/' + id,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            modal.modal('hide');
            updatePortlet('recruiting');
            toastr.success('La salve a été envoyée.');
          } else {
            modal.modal('hide');
            updatePortlet('recruiting');
            toastr.warning('La salve n\'a pas été envoyée.');
          }
        },
        error: log_error
      });
    });

    $('body').on('click', '.force-round-send-1', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.data('href');
      if (!href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            updatePortlet('recruiting');
            toastr.success('La salve a été envoyée.');
          } else {
            updatePortlet('recruiting');
            toastr.warning('La salve n\'a pas été envoyée.');
          }
        },
        error: log_error
      });
    });

    $('#portlet-places select#place').select2({
      ajax: {
        url: basePath + '/places/json',
        type: 'post',
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term, // search term
          };
        },
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.text,
                name: item.name,
                id: item.id
              }
            })
          };
        },
        cache: true
      },
      minimumInputLength: 2
    }).on('select2:select', function(event) {
      var placeId = event.params.data.id;
      var eventId = $('#EventId').val();
      $.ajax({
        url: basePath + '/events/add_place',
        type: 'post',
        dataType: 'json',
        data: {
          place_id: placeId,
          event_id: eventId
        },
        success: function(data) {
          toastr.success('Le lieu a été ajouté.');
        },
        error: log_error
      }).then(function() {
        updatePortlet('places');
        updatePortlet('planning');
      });
    });

    $('body').on('change', '#portlet-places [name="data[option]"]', function() {
      var $this = $(this);
      var row = $this.parents('tr');
      var epId = row.data('event-place-id');
      var option = $this.val();

      $.ajax({
        url: basePath + '/event_places/update',
        type: 'post',
        dataType: 'json',
        data: {
          id: epId,
          option: option
        },
        success: function(data) {
          if (data.success) {
            toastr.success("L'option a été sauvegardée.");
          } else {
            toastr.error("Une erreur est survenue.");
          }
        },
        error: log_error
      }).then(function() {
        updatePortlet('places');
        updatePortlet('infos');
        updatePortlet('planning');
      });

    });

    $('body').on('click', '#portlet-places .delete-option', function(e) {
      e.preventDefault();
      var $this = $(this);
      var row = $this.parents('tr');
      var epId = row.data('event-place-id');

      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce lieu?', function(result) {
        $.ajax({
          url: basePath + '/event_places/delete',
          type: 'post',
          dataType: 'json',
          data: {
            id: epId
          },
          success: function(data) {
            if (data.success) {
              toastr.success("Le lieu a été supprimé de l'évènement.");
            } else {
              toastr.error("Une erreur est survenue.");
            }
          },
          error: log_error
        }).then(function() {
          updatePortlet('places');
          updatePortlet('infos');
          updatePortlet('planning');
        });
      });

    });

    $('body').on('click', '#portlet-orders .delete-order', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette commande?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('La commande a été correctement supprimée.');
              }
            },
            error: log_error
          }).then(function() {
            updatePortlet('orders');
          });
        }
      });

    });

  };

  var handleDeleteUnavailability = function() {

  };

  var handleUserModal = function() {
    $('body').on('show.bs.modal', '#modal-user-ajax', function(e) {
      $(this).find('.modal-body').html('<div class="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>');
    });
    $('body').on('loaded.bs.modal', '#modal-user-ajax', function(e) {
      var modal = $(this);
      var rows = modal.find('.table-modal--jobs tbody tr').get();

      rows.sort(function(a, b) {
        var A = $(a).find('td:first').text();
        var B = $(b).find('td:first').text();
        if (A < B) {
          return 1;
        }
        if (A >= B) {
          return -1;
        }
        return 0;
      });
      $.each(rows, function(index, row) {
        modal.find('.table-modal--jobs tbody').append(row);
      });
    });
  };

  var handleSidebarNav = function() {
    $('body').scrollspy({
      target: '#activity-nav',
      offset: 90
    });

    $('#activity-nav a').bind('click', function(event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top - 90)
      }, 1000, 'easeInOutExpo');
      event.preventDefault();
    });
    $(window).scroll(function() {
      scrollProfileMenu();
    });
    $(window).load(function() {
      scrollProfileMenu();
    });
    var scrollProfileMenu = function() {
      if ($(window).scrollTop() > 130) {
        $("#activity-menu").css("top", $(window).scrollTop() - 130 + "px");
      } else {
        $("#activity-menu").css("top", "0px");
      }
    }
    $('#activity-nav').on('activate.bs.scrollspy', function(e) {
      $('.activity .portlet').removeClass('active');
      var href = $(e.target).find('a').attr('href').replace('#', '');
      $('.activity div[id="' + href + '"]').find('.portlet').addClass('active');
    })
  };

  var handleActivityPortfolio = function() {

    var initCubePortfolio = function(element) {
      element.cubeportfolio({
        layoutMode: 'grid',
        mediaQueries: [{
          width: 320,
          cols: 3
        }],
        caption: ''
      });
    };

    if ($('.activity-portfolio, .game-portfolio').is(':visible')) {
      initCubePortfolio($('.activity-portfolio:visible, .game-portfolio:visible'));
    }

    $('body').on('shown.bs.tab', '.activity [data-toggle="tab"]', function() {
      var id = $(this).attr('href');
      initCubePortfolio($(id).find('.activity-portfolio, .game-portfolio'));
    });
  }

  var handleCRMEditable = function() {
    $('[data-crm-status-editable]').editable({
      source: basePath + '/events/getCRMStatuses',
      sourceOptions: {
        dataType: 'json'
      },
      emptytext: 'Non défini',
      url: basePath + '/events/updateCRMStatus',
      ajaxOptions: {
        dataType: 'json',
        type: 'post',
        success: function() {
          if ($('#portlet-activities').length) {
            $('#portlet-activities .update-orders:first').click();
          }
        }
      },
      success: function(data, newValue) {
        if (data.success) {
          toastr.success("Le statut a été correctement mis à jour.");
        }
      },
      error: log_error
    });

    $('[data-mission-status-editable]').editable({
      source: basePath + '/events/getMissionsStatuses',
      sourceOptions: {
        dataType: 'json'
      },
      emptytext: 'Non défini',
      url: basePath + '/events/updateCRMStatus',
      ajaxOptions: {
        dataType: 'json',
        type: 'post',
        success: function() {
          if ($('#portlet-activities').length) {
            $('#portlet-activities .update-orders:first').click();
          }
        }
      },
      success: function(data, newValue) {
        if (data.success) {
          toastr.success("Le statut a été correctement mis à jour.");
        }
      },
      error: log_error
    });

  }

  var handleResetFilters = function() {
    $('body').on('click', '#offers button[type="reset"], #confirmed button[type="reset"]', function(e) {
      e.preventDefault();
      var $form = $(this).parents('form');
      $form.find('input:text, input:password, input:file, input[type="number"], select, textarea').val('');
      $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
      $form.submit();
    });
  };
  var handleClientsByCompany = function() {

    $('body').on('change', '#EventCompanyId', function() {
      var $this = $(this);
      var company = $(this).val();
      var clientSelect = $('#EventClientId');

      clientSelect.find('option').remove();
      $('#loading').fadeIn();

      $.ajax({
        url: basePath + '/clients/json',
        type: 'post',
        dataType: 'json',
        data: {
          company_id: company
        },
        success: function(data) {
          if (data) {
            clientSelect.append('<option value=""></option>');
            $.each(data, function(id, name) {
              clientSelect.append('<option value="' + id + '">' + name + '</option>');
            });
            clientSelect.selectpicker('refresh');
          }
        },
        error: log_error
      }).then(function() {
        $('#loading').fadeOut();
      });
    });

    //$('#EventCompanyId').trigger('change');

  }

  var handleCompetences = function() {
    $('body').on('change', '#competences table input[type="checkbox"]', function() {
      var $this = $(this);
      var checked = $this.is(':checked');
      var rel = $this.attr('rel');
      var toDelete = $('input[rel="' + rel + '"][data-radio="1"][data-cell-id!="' + $this.data('cell-id') + '"]:checked').get();
      var id = $this.attr('data-competence-id');

      if (checked) {
        // uncheck all other inputs
        $('input[rel="' + rel + '"][data-radio="1"]').prop('checked', false).uniform('refresh');
        $this.prop('checked', true).uniform('refresh');
        var ids = [];
        $.each(toDelete, function(i, e) {
          ids.push($(e).data('competence-id'));
        });
        $.ajax({
          url: basePath + '/competences/add',
          type: 'post',
          dataType: 'json',
          data: {
            to_delete: ids,
            sector: $this.data('sector'),
            job: $this.data('job'),
            hierarchy: $this.data('hierarchy'),
            user_id: $this.data('user-id'),
            activity_id: $this.data('activity-id'),
            fb_module_id: $this.data('fb-module-id')
          },
          success: function(data) {
            if (data.success) {
              $this.attr('data-competence-id', data.id);
              toastr.success('La compétence a été correctement sauvegardée.');
            }
          },
          error: log_error
        });
      } else {
        // delete competence
        $.ajax({
          url: basePath + '/competences/delete',
          type: 'post',
          dataType: 'json',
          data: {
            id: id
          },
          success: function(data) {
            if (data.success) {
              toastr.success('La compétence a été correctement supprimée.');
            }
          }
        }).then(function() {
          $this.attr('data-competence-id', '');
        });
      }
    });
  }

  var handleBatchesTasksContent = function() {

    var $div = $('#checklist-batches-tasks');
    var $tasks = $div.find('.panel-body ul.task-list');

    var renderBatchesTasks = function(div) {
      var href = div.data('href');
      var last_open_batch = div.find('.panel-collapse.in').attr('id');
      if (!href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        data: {
          last_open_batch: last_open_batch
        },
        success: function(data) {
          div.html(data);
        },
        error: log_error
      }).then(function() {
        div.find('[type="checkbox"]').uniform('refresh');
        sortableTasks($div);
        editableBatchName($div);
        editableTaskName($div);
      });
    }

    if ($div.length) {
      renderBatchesTasks($div);
    }

    $('body').on('blur', '.new-batch', function() {
      var $this = $(this);
      var name = $this.val();
      var href = $this.data('href');
      if (!name || !href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le groupe de tâches a été correctement ajouté.');
          }
        },
        error: log_error
      }).then(function() {
        renderBatchesTasks($div);
      });
    });

    $('body').on('blur', '.new-task input', function() {
      var $this = $(this);
      var name = $this.val();
      var href = $this.data('href');
      if (!name || !href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('La tâche a été correctement ajoutée.');
          }
        },
        error: log_error
      }).then(function() {
        renderBatchesTasks($div);
      });
    });

    $('body').on('click', '.delete-batch', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce groupe? Toutes les tâches liées seront supprimées!', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('Le groupe a été correctement supprimé.');
              }
            },
            error: log_error
          }).then(function() {
            renderBatchesTasks($div);
          });
        }
      });

    });

    $('body').on('click', '.delete-task', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette tâche?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('La tâche a été correctement supprimée.');
              }
            },
            error: log_error
          }).then(function() {
            renderBatchesTasks($div);
          });
        }
      });

    });

    $('body').on('shown.bs.collapse', '.panel-collapse', function() {
      var id = $(this).attr('id');
      $('#lastAccordion').val(id);
    });

    $('body').on('hide.bs.collapse', '.panel-collapse', function() {
      $('#lastAccordion').val('');
    });

    $div.sortable({
      items: ".panel",
      opacity: 0.8,
      placeholder: 'empty-feed',
      revert: 250,
      start: function(e, ui) {
        ui.placeholder.height(ui.item.height() + 2);
      },
      stop: function() {
        var batches = [];
        $div.find('.panel').each(function(weight, item) {
          batches.push($(item).data('checklist-batch-id'));
        });

        $.ajax({
          url: basePath + '/checklist_batches/updateWeights',
          type: 'post',
          dataType: 'json',
          data: {
            batches: batches
          },
          success: function(data) {
            //console.log(data);
          },
          error: log_error
        }).then(function() {
          renderBatchesTasks($div);
        })
      }
    });

    function sortableTasks($div) {
      $div.find('.panel-body').each(function(i, panel) {
        var $tasks = $(panel).find('.task-list');
        $tasks.sortable({
          revert: true,
          items: ".sortable",
          placeholder: "empty-feed",
          start: function(e, ui) {
            ui.placeholder.height(ui.item.height() + 2);
          },
          stop: function(event, ui) {
            var tasks = [];
            ui.item.parents('ul').find('li.sortable').each(function(i, e) {
              tasks.push($(e).data('checklist-task-id'));
            });
            $.ajax({
              url: basePath + '/checklist_tasks/updateWeights',
              type: 'post',
              dataType: 'json',
              data: {
                tasks: tasks
              },
              success: function(data) {
                renderBatchesTasks($div);
              },
              error: log_error
            });
          }
        });
      });
    }

    $('body').on('keypress', '.new-batch, .new-task input', function(event) {
      if (event.keyCode == 10 || event.keyCode == 13) {
        $(this).trigger('blur');
      }
    });

    function editableBatchName($div) {
      $div.find('.batch-name .editable').editable({
        mode: 'inline',
        showbuttons: false,
        url: basePath + '/checklist_batches/updateField',
        ajaxOptions: {
          dataType: 'json',
          type: 'post'
        },
        validate: function(value) {
          if ($.trim(value) == '') {
            return 'Ce champ ne peut être vide!';
          }
        },
        success: function(data, newValue) {
          if (data.success) {
            renderBatchesTasks($div);
          }
        }
      });
    }

    function editableTaskName($div) {
      $div.find('.task-list .task-title-sp .editable').editable({
        mode: 'inline',
        showbuttons: false,
        url: basePath + '/checklist_tasks/updateField',
        ajaxOptions: {
          dataType: 'json',
          type: 'post'
        },
        validate: function(value) {
          if ($.trim(value) == '') {
            return 'Ce champ ne peut être vide!';
          }
        },
        success: function(data, newValue) {
          if (data.success) {
            renderBatchesTasks($div);
          }
        }
      });
    }


  }

  var handleTasksCalendar = function() {
    var renderTasksCalendar = function(defaultView, defaultDate, callback) {

      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,basicDay,basicWeek'
      };

      $('#tasksCalendar').fullCalendar('destroy');
      $('#tasksCalendar').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        editable: true,
        //scrollTime: "08:00:00",
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        eventSources: [{
          url: basePath + '/checklists/calendar.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('tasksCalendarDate'),
              view: localStorage.getItem('tasksCalendarView')
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          },
          success: function(data) {

          }
        }],
        eventRender: function(event, element) {
          element.html(event.html);
          element.attr('style', event.style);
        },
        viewRender: function(view) {

          var formerView = localStorage.getItem('tasksCalendarView');
          localStorage.setItem('tasksCalendarView', view.type);
          localStorage.setItem('tasksCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderTasksCalendar('month', view.intervalStart.format('YYYY-MM-DD'));
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('tasksCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderTasksCalendar('basicWeek', localStorage.getItem('tasksCalendarDate'));
          }
        },
        eventDrop: function(event, delta, revertFunc) {

          bootbox.confirm('Êtes-vous sûr de vouloir déplacer cette tâche?', function(result) {
            if (result) {
              $.ajax({
                url: basePath + '/checklist_tasks/updateField',
                type: 'post',
                dataType: 'json',
                data: {
                  name: 'due_date',
                  pk: event.id,
                  value: event.start.format()
                },
                success: function(data) {
                  if (data.success) {
                    toastr.success("La date d'échéance a bien été modifiée.");
                  } else {
                    revertFunc();
                  }
                },
                error: log_error
              })
            } else {
              revertFunc();
            }
          })

        }
      });

    };

    $('body').on('loaded.bs.modal', '#modal-calendar-task', function() {
      var $modal = $(this);
      handleDatePickers();
      $modal.find('[type="checkbox"]').bootstrapSwitch();
      $modal.find('form').submit(function() {
        var $this = $(this);
        var data = $this.serialize();
        $.ajax({
          url: $this.attr('action'),
          type: $this.attr('method'),
          dataType: 'json',
          data: data,
          success: function(data) {
            if (data.success) {
              toastr.success('La tâche a été mise à jour.');
            }
          },
          error: log_error
        }).then(function() {
          $modal.modal('hide');
          if ($('#tasksCalendar').length) {
            renderTasksCalendar(localStorage.getItem('tasksCalendarView'), localStorage.getItem('tasksCalendarDate'));
          } else {
            window.location.reload();
          }
        });
        return false;
      });
    });

    $('body').on('click', '.delete-task', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $modal = $this.parents('.modal.in');
      var href = $this.attr('href');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer cette tâche?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              if (data.success) {
                toastr.success('La tâche a été correctement supprimée.');
              }
            },
            error: log_error
          }).then(function() {
            $modal.modal('hide');
            renderTasksCalendar(localStorage.getItem('tasksCalendarView'), localStorage.getItem('tasksCalendarDate'));
          });
        }
      });
    });

    if ($('#tasksCalendar').length) {
      var view = localStorage.getItem('tasksCalendarView');
      if (!view) view = 'basicWeek';
      renderTasksCalendar(view, localStorage.getItem('tasksCalendarDate'));
    }

  }

  var handleOrderItems = function() {

    toastr.options.preventDuplicates = true;
    toastr.options.timeOut = 1000;

    var updateOrderItems = function($div) {
      if(typeof $div === 'undefined'){
        return;
      }
      var orderId = $div.attr('data-order-id');
      var href = $div.attr('data-href');
      var $openGroups = $div.parents('.order-items-wrapper').find('.open-groups');

      if (!orderId) {
        return;
      }

      $.ajax({
        url: href,
        type: 'post',
        data: {
          open_groups: $openGroups.val()
        },
        success: function(data) {
          $div.html(data);
          $div.parents('#portlet-orders').addClass('loaded');
          handleBootstrapSelect();
          addStockItem();
          $div.find('.panel-collapse').on('shown.bs.collapse', function() {
            saveOpenGroups($div);
          });
          $div.find('.panel-collapse').on('hidden.bs.collapse', function() {
            saveOpenGroups($div);
          });
        },
        error: log_error
      }).then(function() {
        checkItemsUnavailabilities($div);
      });

    }

    var init = function($div) {
      updateOrderItems($div);
    }

    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    var orders = document.querySelector('#portlet-orders');

    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if(mutation.type == 'attributes'){
          if($(mutation.target).hasClass('loaded')){
            $('[data-order-items]').each(function(i, e) {
              init($(e));
            });
          }
          if(mutation.attributeName == 'data-time'){
            $('#portlet-orders .reload').trigger('click');
          }
        }
      });
    });
    if(orders){
      observer.observe(orders, {
        attributes: true,
        characterData: true
      });
    }


    $('[data-order-items]').each(function(i, e) {
      init($(e));
    });

    $('body').on('click', '#portlet-activities .update-orders, #portlet-fb-modules .update-orders', function(e) {
      e.preventDefault();
      $('[data-order-items]').each(function(i, e) {
        updateOrderItems($(e));
      });
      toastr.success('La commande a été mise à jour.');
    });

    var checkItemUnavailability = function($row) {
      var stock_item_id = $row.find('input[data-stock-item-id][type="hidden"]').val();
      var quantity = $row.find('input[data-field="quantity"]').val();
      var number_of_containers = $row.find('input[data-field="number_of_containers"]').val();
      $row.removeClass('empty-stock').removeClass('non-sufficient-stock').removeClass('sufficient-stock');
      if (!stock_item_id) {
        return;
      }
      if (quantity == 0) {
        return;
      }

      $.ajax({
        url: basePath + '/stock_items/checkAvailabilityByHour',
        type: 'post',
        dataType: 'json',
        data: {
          stock_item_id: stock_item_id,
          model: 'event',
          model_id: $('#EventId').val(),
          amount: number_of_containers ? number_of_containers : quantity
        },
        success: function(data) {
          if (data.emptyStock) {
            $row.addClass('empty-stock');
          }
          if (!data.sufficientStock) {
            $row.addClass('non-sufficient-stock');
          }
          if (data.sufficientStock && !data.emptyStock) {
            $row.addClass('sufficient-stock');
          }
        }
      });
    }

    function checkItemsUnavailabilities($div) {
      if ($div.data('order-items-unavailabilities') === undefined) {
        return;
      }
      $div.find('.table-order-items tbody tr').each(function(i, e) {
        var $row = $(e);
        checkItemUnavailability($row);
      });
      $(window).ajaxStop(function() {
        $div.find('.panel').each(function(i, e) {
          var numberOfNonSufficientStockLines = $(e).find('tr.non-sufficient-stock').get().length;
          var numberOfEmpyStockLines = $(e).find('tr.empty-stock').get().length;
          var numberOfSufficientStockLines = $(e).find('tr.sufficient-stock').get().length;
          if (numberOfNonSufficientStockLines) {
            $(e).find('.accordion-toggle .status .non-sufficient-stock').show();
            $(e).find('.accordion-toggle .status .sufficient-stock').hide();
            $(e).find('.accordion-toggle .status .empty-stock').hide();
          } else if (!numberOfNonSufficientStockLines && numberOfEmpyStockLines) {
            $(e).find('.accordion-toggle .status .empty-stock').show();
            $(e).find('.accordion-toggle .status .non-sufficient-stock').hide();
            $(e).find('.accordion-toggle .status .sufficient-stock').hide();
          } else if (!numberOfNonSufficientStockLines && !numberOfEmpyStockLines && numberOfSufficientStockLines) {
            $(e).find('.accordion-toggle .status .sufficient-stock').show();
            $(e).find('.accordion-toggle .status .empty-stock').hide();
            $(e).find('.accordion-toggle .status .non-sufficient-stock').hide();
          } else {
            $(e).find('.accordion-toggle .status .sufficient-stock').hide();
            $(e).find('.accordion-toggle .status .empty-stock').hide();
            $(e).find('.accordion-toggle .status .non-sufficient-stock').hide();
          }
        });
      });
    }

    function saveOpenGroups($div) {
      var openGroups = [];
      $div.find('.panel-collapse').each(function(i, e) {
        if ($(e).hasClass('in')) {
          openGroups.push($(e).attr('id').replace('group', ''));
        }
      });
      var val = openGroups.join(',');
      var $openGroups = $div.parents('.order-items-wrapper').find('.open-groups');
      $openGroups.val(val);
    }

    var addStockItem = function() {
      $('select.add-stock-item').each(function(i, e) {
        var group_id = $(e).data('order-group-id');
        var $div = $(e).parents('[data-order-items]');
        $(e).select2({
          placeholder: "Ajouter un produit du stock",
          minimumInputLength: 2,
          delay: 250,
          templateResult: formatProduct,
          ajax: {
            url: basePath + '/stock_items/json_advanced',
            dataType: 'json',
            type: 'POST',
            cache: true,
            data: function(params) {
              return {
                term: params.term
              };
            },
            processResults: function(data, page) {
              return {
                results: data
              };
            }
          },
          formatResult: format
        }).on("select2:selecting", function(e) {
          var product = e.params.args.data;
          if (e.params.args.data.id) {
            $.ajax({
              url: basePath + '/order_items/add',
              type: 'post',
              dataType: 'json',
              data: {
                stock_item_id: e.params.args.data.id,
                order_group_id: group_id
              },
              success: function(data) {
                setTimeout(function() {
                  updateOrderItems($div);
                }, 200);
              },
              error: log_error
            });
          }
        });
      });

      function format(product) {
        return product.text;
      }

      function formatProduct(product) {
        if (!product.id) {
          return product.text;
        }
        var $product = $(
          '<span>' + product.btn + product.text + '<small> (' + product.infos + ')</small></span>'
        );
        return $product;
      };
    }

    $('body').on('click', '.add-simple-product', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      var $div = $(this).parents('[data-order-items]');
      if (!href) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        success: function(data) {
          setTimeout(function() {
            updateOrderItems($div);
          }, 200);
        },
        error: log_error
      });
    });

    $('body').on('click', '.delete-order-item', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      var $div = $(this).parents('[data-order-items]');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce produit?', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              setTimeout(function() {
                updateOrderItems($div);
              }, 200);
            },
            error: log_error
          });
        }
      });
    });

    $('body').on('change', '.table-order-items .update-order-item', function() {
      var $this = $(this);
      var value = $this.val();
      var $row = $this.parents('tr');
      var data = $row.find(':input').serializeArray();
      if (!data) {
        return;
      }
      $.ajax({
        url: basePath + '/order_items/update',
        type: 'post',
        dataType: 'json',
        data: data,
        success: function(data) {
          if (data.success) {
            toastr.success('Le produit a été mis à jour.');
          }
        },
        error: log_error
      }).then(function() {
        checkItemUnavailability($row);
      });
    });

    $('body').on('click', '.table-order-items a.language-dependent', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $row = $this.parents('tr');
      var ld = $row.find('input.language-dependent');
      var value = ld.val();
      $this.toggleClass('active');
      if (value == 1) {
        ld.val(0);
      } else {
        ld.val(1);
      }
      $row.find('.update-order-item:first').trigger('change');
    });

    $('body').on('click', '.table-order-items a.set-editable-order-item', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $row = $this.parents('tr');
      var editable = $row.find('input.set-editable-order-item');
      var value = editable.val();
      var $div = $(this).parents('[data-order-items]');
      $this.toggleClass('active');
      if (value == 1) {
        editable.val(0);
      } else {
        editable.val(1);
      }
      $row.find('.update-order-item:first').trigger('change');
      var $portlet = $row.parents('#portlet-orders');
      if ($portlet) {
        setTimeout(function() {
          updateOrderItems($div);
        }, 200);
      }
    });

    $('body').on('change', '.table-order-items .quantity-option', function() {
      var $this = $(this);
      var value = $this.val();
      var row = $this.parents('tr');
      row.removeClass().addClass(value);
    });

    $('body').on('click', '.table-order-items a.lock-order-item', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $row = $this.parents('tr');
      var locked = $row.find('input.lock-order-item');
      var value = locked.val();
      $this.toggleClass('active');
      if (value == 1) {
        locked.val(0);
      } else {
        locked.val(1);
      }
      $row.find('.update-order-item:first').trigger('change');
      var $portlet = $row.parents('#portlet-orders');
      if ($portlet) {
        setTimeout(function() {
          updateOrderItems($this.parents('.order-items-wrapper').find('[data-order-items]'));
        }, 200);
      }
    });

    $('body').on('click', '.delete-order-group', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.attr('href');
      var $div = $(this).parents('[data-order-items]');
      if (!href) {
        return;
      }
      bootbox.confirm('Êtes-vous sûr de vouloir supprimer ce groupe? Tous les produits seront supprimés également!', function(result) {
        if (result) {
          $.ajax({
            url: href,
            type: 'post',
            dataType: 'json',
            success: function(data) {
              toastr.success('Le groupe a été correctement supprimé.');
              setTimeout(function() {
                updateOrderItems($div);
              }, 200);
            },
            error: log_error
          });
        }
      });
    });

    $('body').on('blur', '.order-group-name input', function(e) {
      e.preventDefault();
      var $this = $(this);
      var href = $this.data('href');
      var name = $this.val();
      var $div = $(this).parents('.order-items-wrapper').find('[data-order-items]');
      if (!href || !name) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le groupe a été correctement ajouté.');
            setTimeout(function() {
              updateOrderItems($div);
            }, 200);
          }
        },
        error: log_error
      }).then(function() {
        $this.val('');
      });
    });

    $('body').on('blur', '.rename-order-group', function() {
      var $this = $(this);
      var href = $this.data('href');
      var name = $this.val();
      var $div = $(this).parents('[data-order-items]');
      if (!href || !name) {
        return;
      }
      $.ajax({
        url: href,
        type: 'post',
        dataType: 'json',
        data: {
          name: name
        },
        success: function(data) {
          if (data.success) {
            toastr.success('Le nom du groupe a été correctement modifié.');
            setTimeout(function() {
              updateOrderItems($div);
            }, 200);
          }
        },
        error: log_error
      });
    });

    $('body').on('keypress', '.rename-order-group', function(event) {
      if (event.keyCode == 10 || event.keyCode == 13) {
        $(this).trigger('blur');
      }
    });



    $(document).once().ajaxStop(function() {
      $('#order').mousedown(function() {
        document.activeElement.blur();
      });
      $('.accordion').sortable({
        revert: true,
        items: '.panel',
        placeholder: 'empty-panel',
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height() - 2);
        },
        stop: function(event, ui) {
          var groups = [];
          var $div = $(this).parents('[data-order-items]');
          $('.accordion').find('.panel').each(function(i, panel) {
            var id = $(panel).data('order-group-id');
            if (id) groups.push(id);
          });
          $.ajax({
            url: basePath + '/order_groups/updateWeights',
            type: 'post',
            dataType: 'json',
            data: {
              groups: groups
            },
            success: function(data) {
              if (data.success) {
                toastr.success("L'ordre des groupes a été correctement sauvegardé.");
                setTimeout(function() {
                  updateOrderItems($div);
                }, 200);
              }
            },
            error: log_error
          });
        }
      });
      $(".table-order-items tbody").sortable({
        revert: true,
        items: "tr:not(.empty)",
        placeholder: "empty-line",
        connectWith: '.connectedSortable',
        handle: '.drag',
        candel: 'input',
        start: function(e, ui) {
          ui.placeholder.height(ui.item.height());
        },
        stop: function(event, ui) {
          var id = ui.item.find('input[data-order-item-id]').val();
          var order_group_id = ui.item.parents('.panel-collapse').data('order-group-id');
          if (!id || !order_group_id) {
            return;
          }
          var $tbody = ui.item.parents('tbody');
          if ($tbody.hasClass('empty')) {
            $tbody.find('tr.empty').remove();
          }

          // first we save new order group id for order item
          var item = {
            'OrderItem': {
              'id': id,
              'order_group_id': order_group_id
            }
          };
          $.ajax({
            url: basePath + '/order_items/update',
            type: 'post',
            dataType: 'json',
            data: item,
            success: function(data) {
              if (data.success) {
                toastr.success('Le produit a été correctement déplacé.');
              }
            },
            error: log_error
          }).then(function() {
            // update weights
            $(".table-order-items").each(function(i, table) {
              var items = [];
              $(table).find('tbody tr[data-editable]').each(function(i, row) {
                var id = $(row).find('input[data-order-item-id]').val();
                if (id) items.push(id);
              });
              if (items.length) {
                $.ajax({
                  url: basePath + '/order_items/updateWeights',
                  type: 'post',
                  dataType: 'json',
                  data: {
                    items: items
                  },
                  success: function(data) {

                  },
                  error: log_error
                });
              }
            });
          });
        }
      });
    });

  }

  var handleOrderAssociations = function() {

    $('[data-table-orders-associations]').each(function(i, e) {
      var $table = $(e);
      $table.find('.companies .btn').on('click', function() {
        var $this = $(this);
        var companyId = $this.data('company-id');
        var $rows = $table.find('tbody tr');
        if (!companyId) {
          $rows.show();
          return;
        }
        $rows.hide();
        $table.find('tbody tr[data-company-id="' + companyId + '"]').show();
      });

    });

    $('.orders-resources .btn').draggable({
      appendTo: 'body',
      containment: 'window',
      scroll: false,
      helper: 'clone',
      zIndex: 1000,
      revert: true
    });

    $('.table-associations .resources .btn').draggable({
      revert: 'invalid',
      zIndex: 1000,
    });

    $('.table-associations tbody tr').droppable({
      accept: ".orders-resources .btn",
      hoverClass: "ui-state-active",
      drop: function(event, ui) {
        var $resource = $(ui.draggable);
        var $clone = $resource.clone();
        var $row = $(this);

        if ($clone.data('slug')) {
          $clone.text($clone.data('slug'));
        }

        $.ajax({
          url: basePath + '/order_associations/add',
          type: 'post',
          dataType: 'json',
          data: {
            model: $resource.data('model'),
            model_id: $resource.data('model-id'),
            order_id: $row.data('order-id')
          },
          success: function(data) {
            if (data.success) {
              toastr.success('La resource a été correctement ajoutée.');
              $row.find('.resources').append($clone);
              $clone.draggable();
            }
            if (data.existing) {
              toastr.warning('La resource est déjà présente sur ce moment.');
            }
          },
          error: log_error
        }).then(function() {

        });
      }
    });

    $('.orders-resources .list').droppable({
      accept: ".table-associations .resources .btn",
      tolerance: 'intersect',
      over: function(event, ui) {
        $('.ui-draggable-dragging').addClass('deletable');
      },
      out: function(event, ui) {
        $('.ui-draggable-dragging').removeClass('deletable');
      },
      drop: function(event, ui) {
        var $resource = $(ui.draggable);
        var $row = $resource.parents('tr');
        $resource.addClass('dropped');
        $.ajax({
          url: basePath + '/order_associations/delete',
          type: 'post',
          dataType: 'json',
          data: {
            model: $resource.data('model'),
            model_id: $resource.data('model-id'),
            order_id: $row.data('order-id')
          },
          success: function(data) {
            if (data.success) {
              toastr.success('La resource a été supprimée.');
            }
          },
          error: log_error
        }).then(function() {
          $resource.remove();
        });
      }
    });


    $('body').on('keydown', '[data-search-orders-resources]', function() {
      var $this = $(this);
      setTimeout(function() {
        $('.orders-resources .btn').removeClass('btn--hidden');
        var term = $this.val().toLowerCase();
        $('.orders-resources .btn').each(function(i, e) {
          var string = $(e).text().toLowerCase();
          if (string.indexOf(term) < 0) {
            $(e).addClass('btn--hidden');
          } else {
            $(e).removeClass('btn--hidden');
          }
        });
      }, 50);
    });

  }

  var handleOutfitAssociations = function() {

    $('[data-table-outfits-associations]').each(function(i, e) {
      var $table = $(e);
      $table.find('.companies .btn').on('click', function() {
        var $this = $(this);
        var companyId = $this.data('company-id');
        var $rows = $table.find('tbody tr');
        if (!companyId) {
          $rows.show();
          return;
        }
        $rows.hide();
        $table.find('tbody tr[data-company-id="' + companyId + '"]').show();
      });

    });

    $('.outfits-resources .btn').draggable({
      appendTo: 'body',
      containment: 'window',
      scroll: false,
      helper: 'clone',
      zIndex: 1000,
      revert: true
    });

    $('.table-associations .resources .btn').draggable({
      revert: 'invalid',
      zIndex: 1000,
    });

    $('.table-associations tbody tr').droppable({
      accept: ".outfits-resources .btn",
      hoverClass: "ui-state-active",
      drop: function(event, ui) {
        var $resource = $(ui.draggable);
        var $clone = $resource.clone();
        var $row = $(this);

        if ($clone.data('slug')) {
          $clone.text($clone.data('slug'));
        }

        $.ajax({
          url: basePath + '/outfit_associations/add',
          type: 'post',
          dataType: 'json',
          data: {
            model: $resource.data('model'),
            model_id: $resource.data('model-id'),
            outfit_id: $row.data('outfit-id')
          },
          success: function(data) {
            if (data.success) {
              toastr.success('La resource a été correctement ajoutée.');
              $row.find('.resources').append($clone);
              $clone.draggable();
            }
            if (data.existing) {
              toastr.warning('La resource est déjà présente sur ce moment.');
            }
          },
          error: log_error
        }).then(function() {

        });
      }
    });

    $('.outfits-resources .list').droppable({
      accept: ".table-associations .resources .btn",
      tolerance: 'intersect',
      over: function(event, ui) {
        $('.ui-draggable-dragging').addClass('deletable');
      },
      out: function(event, ui) {
        $('.ui-draggable-dragging').removeClass('deletable');
      },
      drop: function(event, ui) {
        var $resource = $(ui.draggable);
        var $row = $resource.parents('tr');
        $resource.addClass('dropped');
        $.ajax({
          url: basePath + '/outfit_associations/delete',
          type: 'post',
          dataType: 'json',
          data: {
            model: $resource.data('model'),
            model_id: $resource.data('model-id'),
            outfit_id: $row.data('outfit-id')
          },
          success: function(data) {
            if (data.success) {
              toastr.success('La resource a été supprimée.');
            }
          },
          error: log_error
        }).then(function() {
          $resource.remove();
        });
      }
    });


    $('body').on('keydown', '[data-search-outfits-resources]', function() {
      var $this = $(this);
      setTimeout(function() {
        $('.outfits-resources .btn').removeClass('btn--hidden');
        var term = $this.val().toLowerCase();
        $('.outfits-resources .btn').each(function(i, e) {
          var string = $(e).text().toLowerCase();
          if (string.indexOf(term) < 0) {
            $(e).addClass('btn--hidden');
          } else {
            $(e).removeClass('btn--hidden');
          }
        });
      }, 50);
    });

  }

  var handleOutfitAssociations1 = function() {

    $('body').on('change', '.table-outfit-associations [type="checkbox"]', function() {
      var $this = $(this);
      var checked = $this.is(':checked');
      if (checked) {
        // create association
        $.ajax({
          url: basePath + '/outfit_associations/add',
          type: 'post',
          dataType: 'json',
          data: {
            outfit_id: $this.data('outfit-id'),
            model_id: $this.data('model-id'),
            model: $this.data('model')
          },
          success: function(data) {
            if (data.success) {
              toastr.success("L'association a été créée.");
            }
          },
          error: log_error
        });
      } else {
        //delete association
        $.ajax({
          url: basePath + '/outfit_associations/delete',
          type: 'post',
          dataType: 'json',
          data: {
            outfit_id: $this.data('outfit-id'),
            model_id: $this.data('model-id'),
            model: $this.data('model')
          },
          success: function(data) {
            if (data.success) {
              toastr.success("L'association a été supprimée.");
            }
          },
          error: log_error
        });
      }
    });

  }

  var handlePortfolio = function() {
    var initCubePortfolio = function(element, cols) {
      element.cubeportfolio({
        layoutMode: 'grid',
        mediaQueries: [{
          width: 320,
          cols: 3
        }],
        caption: ''
      });
    };
    $('[data-cube-portfolio]:visible').each(function(i, e) {
      initCubePortfolio($(e));
    });
    $('body').on('shown.bs.tab', '[data-toggle="tab"]', function() {
      var id = $(this).attr('href');
      if ($(id).find('.cbp').length) {
        var cbp = $(id).find('.cbp');
        initCubePortfolio(cbp)
      }
    });
  }

  var handleImportTool = function() {

    var $table = $('.table-import-images');

    var importImage = function($row) {
      $row.removeClass().addClass('processing');
      var filename = $row.data('filename');
      if (!filename) {
        return;
      }
      $.ajax({
        url: basePath + '/stock_items/import/image/',
        type: 'post',
        dataType: 'json',
        data: {
          filename: filename
        },
        success: function(data) {
          $row.removeClass();
          if (data.success == 1) {
            $row.addClass('done').delay(1000).fadeOut();
          } else {
            $row.addClass('error');
          }
          if (data.message) {
            $row.find('.status').append(data.message);
          }
        },
        error: log_error
      }).then(function() {
        var $next = $table.find('tr.waiting:first');
        importImage($next);
      });
    }

    $('body').on('click', '.start-import-images', function(e) {
      e.preventDefault();
      var $row = $table.find('tr.waiting:first');
      importImage($row);
    })

  }

  var handlePlacesCalendar = function() {
    var renderPlacesCalendar = function(defaultView, defaultDate, callback) {

      var h = {};
      h = {
        left: 'title',
        center: '',
        right: 'prev,next,today,basicWeek,month'
      };

      var filters = [];
      // get all selected values of filters
      $('.filters select.bs-select').each(function(i, e) {
        var value = $(e).val();
        var field = $(e).data('field');
        filters.push({
          'field': field,
          'value': value
        });
      });

      $('#placesCalendar').fullCalendar('destroy');
      $('#placesCalendar').fullCalendar({
        locale: 'fr',
        header: h,
        firstDay: 1,
        //scrollTime: "08:00:00",
        minTime: "06:00:00",
        defaultView: defaultView,
        defaultDate: defaultDate,
        eventSources: [{
          url: basePath + '/places/calendar.json',
          type: 'POST',
          data: function() {
            return {
              date: localStorage.getItem('placesCalendarDate'),
              view: localStorage.getItem('placesCalendarView'),
              filters: filters
            }
          },
          error: function() {
            alert('there was an error while fetching events!');
          },
          success: function(data) {

          }
        }],
        eventRender: function(event, element) {
          element.html(event.html);
          element.attr('style', event.style);
        },
        eventClick: function(event) {
          if (event.url) {
            window.open(event.url);
            return false;
          }
        },
        viewRender: function(view) {

          var formerView = localStorage.getItem('placesCalendarView');
          localStorage.setItem('placesCalendarView', view.type);
          localStorage.setItem('placesCalendarDate', view.intervalStart.format('YYYY-MM-DD'));
          if (formerView != 'month' && view.type == 'month') {
            renderPlacesCalendar('month', view.intervalStart.format('YYYY-MM-DD'));
          }
          if (formerView == 'month' && view.type == 'basicWeek') {
            var d = new Date();
            var day = d.getDay();
            diff = d.getDate() - day + (day == 0 ? -6 : 1);
            var startDate = new Date(d.setDate(diff));
            var day = startDate.getDate();
            var monthIndex = startDate.getMonth() + 1;
            var year = startDate.getFullYear();
            localStorage.setItem('placesCalendarDate', view.intervalStart.format(year + '-' + monthIndex + '-' + day));
            renderPlacesCalendar('basicWeek', localStorage.getItem('placesCalendarDate'));
          }
        }
      });

    };

    if ($('#placesCalendar').length) {
      var view = localStorage.getItem('placesCalendarView');
      if (!view) view = 'basicWeek';
      renderPlacesCalendar(view, localStorage.getItem('placesCalendarDate'));
    }

    $('body').on('hidden.bs.select', '.filters .bs-select', function(e) {
      renderPlacesCalendar(localStorage.getItem('placesCalendarView'), localStorage.getItem('placesCalendarDate'));
    });

  }

  var handleCompetencesEvaluation = function() {

    $('body').on('click', '.tools [data-company]', function(e) {
      e.preventDefault();
      var speed = 200;
      var $this = $(this);
      var company = $this.data('company');
      var $table = $('.table-hierarchies');
      $('.tools [data-company]').removeClass('active');
      $this.addClass('active');
      $table.find('tbody tr').hide();
      if (company != 'all') {
        $table.find('tr[data-company="' + company + '"]').fadeIn(speed);
      } else {
        $table.find('tbody tr').fadeIn(speed);
      }
    });

    $('body').on('click', '[data-evaluate-competence]', function(e) {
      e.preventDefault();
      var $this = $(this);
      var $row = $this.parents('tr');
      var $group = $this.parents('.btn-group');
      var $feedback = $this.parents('td').find('.feedback');
      var $loading = $feedback.find('.loading');
      var $success = $feedback.find('.success');
      var $error = $feedback.find('.error');
      $group.hide();
      $loading.show();
      $.ajax({
        url: $this.attr('href'),
        type: 'post',
        dataType: 'json',
        success: function(data) {
          $loading.hide();
          if (data.success) {
            $success.show();
            // setTimeout(function(){
            //   $row.fadeOut();
            // }, 3000);
            $row.addClass('done');
          } else {
            $error.show();
          }
        },
        error: function() {
          toastr.error('Une erreur est survenue!');
          $loading.hide();
          $error.show();
        }
      });
    });

  }

  var handleSearchResults = function() {
    $('body').on('click', '[data-search-filters] a', function(e) {
      var model = $(this).data('model');
      if (model != 'all') {
        $('[data-search-results] tbody tr').hide();
      } else {
        $('[data-search-results] tbody tr').show();
      }
      $('[data-search-results] tbody tr[data-model="' + model + '"]').show();
    })
  }

  var handleTabDrop = function() {
    if ($().tabdrop) {
      // $('.tabbable-tabdrop .nav-pills, .tabbable-tabdrop .nav-tabs').tabdrop({
      //   text: '<i class="fa fa-ellipsis-v"></i>&nbsp;<i class="fa fa-angle-down"></i>'
      // });
      //$('.tabbable-tabdrop .nav-pills, .tabbable-tabdrop .nav-tabs').tabdrop('layout');
    }
  }

  var handleOptionsEditable = function() {
    $('[data-reporting-option-editable]').editable({
      url: basePath + '/reporting_options/update',
      disabled: false,
      ajaxOptions: {
        dataType: 'json',
        type: 'post'
      },
      params: function(params) {
        var $this = $(this);
        params.company_id = $this.data('company-id');
        params.month = $this.data('month');
        params.type = $this.data('option-type');
        params.year = $this.data('year');
        console.log(params);
        return params;
      },
      success: function(data, newValue) {
        if (data.success) {
          toastr.success("La valeur a été correctement sauvegardée.");
        }
      },
      error: log_error
    });
  }

  var handleExportDeposit = function() {
    $('body').on('click', '[data-festiloc-export-deposit]', function(e) {
      var $this = $(this);
      var id = $this.data('stockorder-id');
      e.preventDefault();
      $('#loading').fadeIn();
      $.ajax({
        url: basePath + '/revelate/export/stockorders/3/1',
        dataType: 'json',
        type: 'post',
        data: {
          itemId: id,
          companyId: 3
        },
        success: function(data) {
          if (data.success) {
            toastr.success("L'acompte a été correctement exporté.");
          } else {
            toastr.error("L'acompte n'a pas été exporté.");
          }
        },
        error: log_error
      }).then(function() {
        $('#loading').fadeOut();
      });
    });
  }

  // public functions
  return {

    //main function
    init: function() {

      $('body').on('focus', 'input[type=number]', function(e) {
        $(this).on('mousewheel.disableScroll', function(e) {
          e.preventDefault()
        })
      });
      $('body').on('blur', 'input[type=number]', function(e) {
        $(this).off('mousewheel.disableScroll')
      });

      toastr.options = {
        preventDuplicates: true,
        hideDuration: 1
      }

      //initialize here something.
      handleDatePickers();
      handleBootstrapSelect();
      handleRemoveConfiguration();
      handleRemoveActivity();
      handleConfigurations();
      handleEditDocument();
      handleDeleteDocument();
      handleSelectCommune();
      portletDraggable();
      handleLoadEvent();
      $('[data-toggle="tooltip"]').tooltip();
      handleLastTab();
      handleCheckEmail();
      handleSelectStockItem($("#selectStockItem"), $("#selectStockItem").data('company-id'));
      handleBootboxConfirmation();
      handleXEditable();
      handleNoteModal();
      handleUserModal();
      handleSelect2Tags();
      handlePortfolio();
      handleInputMasks();
      handleTabDrop();

      initCkeditor();
    },

    user: function() {
      handleBootstrapSelect();
      handleDatePickers();
      handleAvailabilities();
      handleRiseUser();
      handleRadioButtons();
    },

    search: function() {
      handleCheckAll();
      handleLoadEvent();
      handleRecruit();
      handleDatePickers();
      //mapMarkerPlaces();
      //handleShowMapList();
      handlePlacesResults();
      handleDatePickers();
      handleSearchResults();

      // var $container = $('#places-results');
      //
      // $container.infinitescroll({
      // 	navSelector	: '.next',	// selector for the paged navigation
      // 	nextSelector : '.next a',	// selector for the NEXT link (to page 2)
      // 	itemSelector : '.place',	 // selector for all items you'll retrieve
      // 	debug : false,
      // 	dataType : 'html',
      // 		loading:{
      // 			msgText: 'Chargement...'
      // 		}
      // });

    },

    gmap: function() {
      mapMarker();
    },

    places: function() {
      handleDocumentsActions();
      handlePlacesCalendar();
    },

    place: function() {
      initPlaceAgenda();
    },

    activities: function() {
      handleSidebarNav();
      handleActivityPortfolio();
    },

    clients: function() {
      handleAddContactPeople();
      cresus();
      turnover2015();
    },

    stockitems: function() {
      handleSelectStockItem($("#selectStockItem"), $("#selectStockItem").data('company-id'));
      handleAddStock();
      handleImportTool();
    },

    stockitem: function() {
      //handleSectionsFamilies();
      handleDeleteStockItem();
      handleDeleteUnavailability();
    },

    stockitemChart: function() {
      $('input[name="daterange"]').daterangepicker({
        separator: ' à ',
        startDate: moment().format('DD.MM.YYYY'),
        endDate: moment().add('10', 'days').format('DD.MM.YYYY'),
        minDate: moment().subtract(30, 'days').format('DD.MM.YYYY'),
        maxDate: moment().add(6, 'months').format('DD.MM.YYYY'),
        format: 'DD.MM.YYYY',
        locale: {
          "format": "DD.MM.YYYY",
          "separator": " - ",
          "applyLabel": "Appliquer",
          "cancelLabel": "Annuler",
          "fromLabel": "De",
          "toLabel": "Jusqu'à",
          "customRangeLabel": "Custom",
          "daysOfWeek": [
            "Di",
            "Lu",
            "Ma",
            "Me",
            "Je",
            "Ve",
            "Sa"
          ],
          "monthNames": [
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Août",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
          ],
          "firstDay": 1
        },
      });

      $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
        var start = picker.startDate.format('YYYY-MM-DD');
        var end = picker.endDate.format('YYYY-MM-DD');
        var id = $(this).data('stock-item-id');
        renderChart(start, end, id);
      });

      setTimeout(function() {
        renderChart(moment().format('YYYY-MM-DD'), moment().add(10, 'days').format('YYYY-MM-DD'), $('input[name="daterange"]').data('stock-item-id'));
      }, 100);

      function renderChart(start, end, stockItemId) {
        $.ajax({
          url: basePath + '/stock_items/daterangepicker',
          type: 'post',
          dataType: 'json',
          data: {
            stock_item_id: stockItemId,
            start: start,
            end: end
          },
          success: function(data) {
            updateChart(data);
          },
          error: log_error
        });
      }

      var updateChart = function(dataProvider) {
        var chart = AmCharts.makeChart("chart_1", {
          "type": "serial",
          "theme": "light",
          "pathToImages": App.getGlobalPluginsPath() + "amcharts/amcharts/images/",
          "autoMargins": false,
          "marginLeft": 30,
          "marginRight": 8,
          "marginTop": 10,
          "marginBottom": 26,
          "fontFamily": 'Open Sans',
          "color": '#888',
          "dataProvider": dataProvider,
          "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0,
            "position": "left",
            "integersOnly": true
          }],
          "startDuration": 1,
          "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "bottom",
            "useGraphSettings": true,
            "markerSize": 10
          },
          "graphs": [{
            "balloonText": "<span style='font-size:13px;'>[[title]] : <b>[[value]]</b> [[additional]]</span>",
            "bullet": "round",
            "dashLengthField": "dashLengthLine",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Quantité totale",
            "valueField": "totalQuantity",
            "lineColor": "#ccc"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Quantité restante",
            "type": "column",
            "valueField": "quantity",
            "labelText": "[[value]]",
            "lineColor": "#8775a7",
            "color": "#fff"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Quantité confirmée",
            "type": "column",
            "valueField": "confirmed",
            "labelText": "[[value]]",
            "lineColor": "#26a69a",
            "color": "#fff"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Quantité en offre",
            "type": "column",
            "valueField": "potential",
            "labelText": "[[value]]",
            "lineColor": "#3598dc",
            "color": "#fff"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Quantité en recondionnement",
            "type": "column",
            "valueField": "reconditionning",
            "labelText": "[[value]]",
            "lineColor": "#f3c200",
            "color": "#fff"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Quantité en réparation",
            "type": "column",
            "valueField": "reparation",
            "labelText": "[[value]]",
            "lineColor": "#E08283",
            "color": "#fff"
          }, {
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:13px;'>[[title]] le [[category]] : <b>[[value]]</b> [[additional]]</span>",
            "dashLengthField": "dashLengthColumn",
            "fillAlphas": 1,
            "title": "Autre",
            "type": "column",
            "valueField": "other",
            "labelText": "[[value]]",
            "lineColor": "#C49F47",
            "color": "#fff"
          }],
          "categoryField": "day",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 0
          },
          "export": {
            "enabled": true
          }
        });
      };
    },

    prepare: function() {
      if (document.location.hostname == 'localhost' || document.location.hostname == '192.168.1.138') {
        basePath = '/myubic';
        googleMapsDelay = 600;
      }
      if ($('#debug-kit-toolbar').length > 0) {
        $('#debug-kit-toolbar').addClass('hidden-print');
      }
      //handleRadioButtons();
      // $('li.has-children ul').addClass('dropdown-menu');
      // $('ul.page-sidebar-menu').attr('data-auto-scroll', true).attr('data-slide-speed', 200);
    },

    competences: function() {
      handleCompetencesJobs('competences');
      handleCompetences();
      handleCompetencesEvaluation();
    },

    events: function() {
      handleCompetencesJobs('jobs');
      handleMoments('moments');
      handleContactPeople('#EventClientId', '#EventContactPeopleId');
      handleUpdateOption();
      if ($('#feeling').length > 0) {
        $('#feeling').noUiSlider({
          start: $('#EventFeeling').val(),
          step: 5,
          connect: "lower",
          range: {
            'min': 0,
            'max': 100
          }
        });
        $("#feeling").Link('lower').to($('#EventFeeling'));
        $("#feeling").Link('lower').to($('#EventFeelingSpan'));
      }
      initEventsCalendar();
      initEventsStaff();
      handleModalClient();
      handleSteps();
      handleTrackingSheets();
      handleEventWork();
      handleCRMEditable();
      handleResetFilters();
      handleClientsByCompany();
    },

    stockorders: function() {
      handleAddStockItem();
      handleContactPeople('#StockOrderClientId', '#StockOrderContactPeopleId, #StockOrderDeliveryContactPeopleId, #StockOrderReturnContactPeopleId');
      handleStockOrderMoments();
      handleStockOrderDistanceCovered();
      handleSelectStockOrderPlace();
      if ($('.computeTotal').length) handleComputeTotal();
      handleWeekPreparation();
      handleInvoice();
      handleSearchGooglePlaces();
      handleModalClient();
      handleGetContactPeople();
      handleAutomaticSave();
      handleBootbox();
      handleDraggableProducts();
      handleCancelStockOrder();
      handleShowSubmitButtons();
      handleStockOrderType();
      handleReturnAddress();
      handleExportDeposit();
    },

    communes: function() {
      handleGetLatLngCommune();
    },

    pricelist: function() {
      handleSelectCategory();
    },

    modules: function() {
      handleSelectModuleCategory();
      handleUserModules();
    },

    warehouse: function() {
      handleDepotPlan();
    },

    messages: function() {
      initSendMessage();
    },

    festiloc: function() {
      handleFestilocLive();
      handleConflicts();
      $('body').on('change', '.updatePackaging', function() {
        var stockOrderId = $('#StockOrderId').val();
        var numberOfPallets = $('#StockOrderNumberOfPallets1').val().length ? $('#StockOrderNumberOfPallets1').val() : '';
        var numberOfRollis = $('#StockOrderNumberOfRollis1').val().length ? $('#StockOrderNumberOfRollis1').val() : '';
        var numberOfPalletsXL = $('#StockOrderNumberOfPalletsXl1').val().length ? $('#StockOrderNumberOfPalletsXl1').val() : '';
        var numberOfRollisXL = $('#StockOrderNumberOfRollisXl1').val().length ? $('#StockOrderNumberOfRollisXl1').val() : '';
        $.ajax({
          url: basePath + '/stock_orders/updatePackaging',
          type: 'POST',
          dataType: 'json',
          data: {
            id: stockOrderId,
            numberOfPallets: numberOfPallets,
            numberOfRollis: numberOfRollis,
            numberOfPalletsXL: numberOfPalletsXL,
            numberOfRollisXL: numberOfRollisXL
          },
          success: function(data) {
            if (data.success == 1) {
              toastr.options.timeOut = 800;
              toastr.remove();
              toastr.success('La commande a été sauvegardée.');
            }
          },
          error: log_error
        });
      });
    },

    stockitemunavailabilities: function() {
      handleSelectStockItem($("#StockItemUnavailabilityStockItemId"), 3);
    },

    me: function() {
      handleScrollSpy();
      handleHideAlert();
      handleAvailabilities();
      handleRadioButtons();
    },

    vehicles: function() {
      handleSelectVehicleCompany('#VehicleVehicleCompanyId', '#VehicleVehicleCompanyModelId');
      handleSelectVehicleCompanyModel();
      handleVehicleReservations();
      handleDraggableOrders();
    },

    vehicle_reservations: function() {
      handleSelectVehicleCompany('#VehicleReservationVehicleCompanyId', '#VehicleReservationVehicleCompanyModelId');
      handleSelectVehicleCompanyModel();
    },

    vehicle_tours: function() {
      handleTourPlanification();
    },

    artist: function() {
      handleTagsSelection();
    },

    client: function() {
      handleModalClient();
      handleTimeline();
    },

    checklist: function() {
      handleBatchesTasksContent();
    },

    checklist_tasks: function() {
      handleTasksCalendar();
    },

    orders: function() {
      handleOrderItems();
    },

    order_associations: function() {
      handleOrderAssociations();
      $(window).scroll(function() {
        scrollProfileMenu1();
      });
      $(window).load(function() {
        scrollProfileMenu1();
      });
      var scrollProfileMenu1 = function() {
        if ($(window).scrollTop() > 200) {
          $(".orders-resources .list").css("top", $(window).scrollTop() - 200 + "px");
        } else {
          $(".orders-resources .list").css("top", "0px");
        }
      }
    },

    outfit_associations: function() {
      handleOutfitAssociations();
    },

    reporting: function() {
      handleOptionsEditable();
    }

  }
}();
