$(document).ready(function(){

	if($('.my-btn-group').length){
		var greenClass = 'green-jungle';
		var redClass = 'red-intense';

		$('.my-btn-group label').each(function(i,e){
			var checkbox = $(e).find('input');
			if(checkbox.is(':checked')){
				$(e).addClass(greenClass);
				$(e).addClass('active');
			} else {
				$(e).addClass(redClass);				
			}
		});
		$('.my-btn-group label input').change(function(e){
			var isChecked = $(this).is(':checked');
			var label = $(this).parents('label');
			if(isChecked){
				label.addClass(greenClass).removeClass(redClass);
			} else {
				label.addClass(redClass).removeClass(greenClass);
			}
		})
	}

	$('body').on('input', '.configuration-name', function(event){
		$(this).parents('.configuration-form').find('.caption').text($(this).val());
	})

	$('body').on('click', 'a.add-activity', function(event){
		event.preventDefault();

		var activityDiv = $('.activity-form:first');
		var clone = activityDiv.clone();
		clone.find('.caption').text('New activity');
		clone.hide();
		clone.find('input, textarea').each(function(i,e){
			if($(e).val() != 'place'){
				$(e).val('');
			}
			var index = $('.activity-form').get().length;
			if($(e).attr('name') !== undefined) var name = $(e).attr('name').replace('[0]', '['+index+']');
			var id = $(e).attr('id').replace('0', index);
			$(e).attr('name', name);
			$(e).attr('id', id);
		});
		$('#activities .form-actions').before(clone);
		clone.fadeIn();
	});

	$('body').on('input', '.activity-name', function(event){
		$(this).parents('.activity-form').find('.caption').text($(this).val());
	})

})