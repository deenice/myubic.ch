<?php $this->assign('page_title', __('Users')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i><?php echo __('List of all users'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="table-toolbar">
			<div class="row">
				<div class="col-md-6">
					<div class="btn-group">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="btn-group pull-right">
						<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="#">
								Print </a>
							</li>
							<li>
								<a href="#">
								Save as PDF </a>
							</li>
							<li>
								<a href="#">
								Export to Excel </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><?php echo __('First name'); ?></th>
					<th><?php echo __('Last name'); ?></th>
					<th><?php echo __('Email'); ?></th>
					<th><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($users as $user): //debug($user); ?>
				<tr>
					<td><?php if(!empty($user['Document'][0]['url'])) echo $this->ImageSize->crop(DS . $user['Document'][0]['url'], 40, 40); ?></td>
					<td><?php echo $user['User']['first_name']; ?></td>
					<td><?php echo $user['User']['last_name']; ?></td>
					<td><?php echo $user['User']['email']; ?></td>
					<td>
						<?php echo $this->Html->link('<i class="fa fa-search"></i> View', array('controller' => 'users', 'action' => 'view', $user['User']['id']), array('class' => 'btn default btn-xs', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('controller' => 'users', 'action' => 'edit', $user['User']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Delete', array('controller' => 'users', 'action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
			</table>
		</div>
	</div>
</div>