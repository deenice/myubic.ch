<?php echo $this->Form->create('JobBriefing', array('class' => 'form-horizontal form-job-briefing', 'url' => array('controller' => 'job_briefings', 'action' => 'edit', empty($briefing) ? 0 : $briefing['JobBriefing']['id']))); ?>
<?php echo empty($briefing) ? '' : $this->Form->input('JobBriefing.id', array('value' => $briefing['JobBriefing']['id'], 'type' => 'hidden')); ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo empty($briefing) ? __('New briefing') : $briefing['JobBriefing']['name']; ?></h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('JobBriefing.name', array('label' => false, 'class' => 'form-control', 'value' => empty($briefing) ? '' : $briefing['JobBriefing']['name']));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Job'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('JobBriefing.job_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $jobs, 'value' => empty($briefing) ? 1 : $briefing['JobBriefing']['job_id']));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Content'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('JobBriefing.content', array('label' => false, 'class' => 'form-control ckeditor', 'rows' => 50, 'id' => 'ckeditor', 'type' => 'textarea', 'value' => empty($briefing) ? $this->element('JobBriefings/default') : $briefing['JobBriefing']['content']));?>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <button type="button" class="btn btn-danger delete-job-briefing" data-job-briefing-id="<?php echo empty($briefing) ? '' : $briefing['JobBriefing']['id']; ?>"><?php echo __('Delete'); ?></button>
  <button type="submit" class="btn btn-success"><?php echo __('Save'); ?></button>
</div>
<?php echo $this->Form->end(); ?>
