<p>Salut <?php echo $userFirstName; ?>,</p>

<p>Depuis quelques semaines, tu as déjà accès à notre plateforme www.myubic.ch. Si ce n'est pas encore le cas, ne t'en fais pas, tu auras toutes les informations dans le présent e-mail :-) </p>

<p>Vous avez pour le moment accès uniquement à votre profil. Pas de panique, vous pourrez prochainement voir vos prochains événements, vous informer sur les nouveautés et vous inscrire directement sur les postes disponibles. Vous serez informés en temps et en heure de ces changement.</p>

<p>Voici encore quelques informations pratiques pour bien remplir votre profil :</p>

<p>
	<strong>Comment accéder au site ?</strong><br>
	Se rendre sous www.myubic.ch<br />
	User : <?php echo $userUsername; ?><br />
	Mot de passe : 123456 (mot de passe par défaut - si vous ne l'avez pas encore changé, vous pouvez le faire)
</p>

<p>Ton profil est accessible via le menu de gauche, onglet "Profil".</p>

<p>
	<strong>Données personnelles</strong><br />
	Merci de remplir TOUS les champs. Nous pourrons ainsi vous souhaiter un bon anniversaire, savoir quel taille d'habits vous mettre ou vous envoyer une carte postale...
</p>

<p>
	<strong>Compétences</strong><br />
	Il est important pour nous de savoir si vous disposez de permis de conduire, et surtout, les langues maîtrisées ! Si en plus, vous avez un talent particulier, c'est le moment de l'indiquer.
</p>

<p>
	<strong>Informations bancaires</strong><br />
	Si vous voulez être payés le prochain mois, merci de remplir tous les champs sans exception ! A partir de maintenant, tout se fait depuis cette plateforme. Donc, même si vous travaillez chez nous depuis des lustres, merci de rafraîchir ces infos.
</p>

<p>
	<strong>Disponibilités</strong><br />
	De même, indiquez vos disponibilités de manière générale entre Oui / Non / Peut-être.<br />
	Le champs "remarques" peut être utiliser pour indiquer vos indisponibilités pour cause d'armée, de vacances ou toute info utile liée à votre agenda. Vous pouvez changer en tout temps ces informations.
</p>

<p>​Plus votre profil est complet, plus nous en saurons sur vous tous et pourrons vous proposer du travail. Nous avons actuellement plus de 300 collaborateurs, d'où la difficulté de s'y retrouver !</p>

<p>A votre disposition pour toute question et belle journée.</p>

<p></p>
<p>Floriane</p>