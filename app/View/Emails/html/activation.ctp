<strong><?php echo __('Your account is now activated.'); ?></strong><br>
<p><?php echo __('You can now edit your profile by clicking the following link.'); ?></p>
<p><?php echo $this->Html->link(__('Edit my account'), $this->Html->url($link, true)); ?></p>