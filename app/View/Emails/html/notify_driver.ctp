<p>
  Salut <?php echo $first_name; ?>
</p>
<p>
  Tu trouveras, en pièce jointe, ton planning pour<br />
  la <strong>tournée <?php echo $code; ?></strong><br />
  du <strong><?php echo $this->Time->format($date, '%A %d %B %Y'); ?></strong>.
</p>
<p>
  Bonne route
</p>
<p>
  A ta disposition pour toute question.<br /><?php echo $loggedUser; ?>
</p>
<p>
  <em>Cet email a été envoyé automatiquement.</em>
</p>
