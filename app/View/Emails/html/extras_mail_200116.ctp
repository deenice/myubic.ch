<p>	*** mail automatique envoyé à tous les extras en animation ***</p>
<p>

</p>
<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous sommes encore à la recherche d'extras en animation pour les missions suivantes. <br/>
Si tu ne travailles pas pour nous et que tu es dispo, merci de me le faire savoir par retour d'e-mail.</p>
<p>
	<strong>Samedi 23 janvier 2016 - Berne</strong><br />
1 Animateur Casino des Sens - français<br />
Timing au départ de Villars-sur-Glâne : 15h00 - 01h00
</p>
<p>
	<strong>Mercredi 27 janvier 2016 - Zürich</strong><br />
	1 Aide-animateur Banquet Gaming - anglais<br />
	Timing au départ de Villars-sur-Glâne : 16h00-00h00
</p>
<p>
	<strong>Jeudi 28 janvier 2016 - Bâle</strong><br />
	3 Aide-animateurs Création artistique - anglais souhaité mais pas indispensable<br />
	Timing au départ de Villars-sur-Glâne : 8h00-19h00
</p>
<p>
	<strong>Jeudi 28 janvier 2016 - Moléson</strong><br />
	1 animateur Snake Gliss (assez basique) - anglais indispensable<br />
	Timing au départ de Villars-sur-Glâne : 13h30-20h00
</p>
<p>
	<strong>Vendredi 29 janvier 2016 - Morat</strong><br />
	1 aide-animateur Casino des sens - français<br />
Timing au départ de Villars-sur-Glâne : 15h15-21h45
</p>
<p>
	Merci pour votre rapide retour si vous avez des dispos.
</p>
<p>
	Bonne journée !!
</p>
<p>
	Floriane
</p>
