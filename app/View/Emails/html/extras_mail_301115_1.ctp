<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous recherchons encore 1 extra de service pour cette fin de semaine. Es-tu disponible et intéressé :</p>

<p>
	<strong>Vendredi 4 décembre 2015 - Granges-Paccot</strong><br />
	Horaire sur place : 15h30-01h00<br />
	Service crew - repas servi à table
</p>

<p>
	Si tu es disponible et intéressé, merci de m'informer par retour d'e-mail rapidement.<br />
En te remerciant d'avance pour ton retour !
</p>

<p>
	Floriane
</p>

<p>
	<em>PS : ceci est <b>un message automatique</b> envoyé à tous nos extras actifs en Service. Si vous travaillez déjà ce jour-là ou si vous m'avez déjà informée de votre indisponibilité, vous pouvez directement supprimer ce message sans me répondre.</em>
</p>
