<p>Bonjour <?php echo $civilities[$stockorder['ContactPeopleClient']['civility']]; ?> <?php echo $stockorder['ContactPeopleClient']['last_name']; ?>,</p>

<p>Par ce message, nous vous confirmons l’annulation de votre commande.</p>
<p>Nous nous tenons volontiers à votre disposition pour de futures demandes.</p>
<p>En vous remerciant pour l'intérêt que vous portez à nos produits, nous vous souhaitons une agréable journée et vous adressons nos meilleures salutations.</p>

<p>
<br /><br />
</p>

<img src="http://www.festiloc.ch/data/web/festiloc3.ch/uploads/festiloc_seul_logo.png" width="140px">

<h3 style="font-size: 14px;"><?php echo $senderName; ?><span style="font-weight: normal"><?php if(!empty($senderFunction)): ?> | <?php echo $senderFunction; ?><?php endif; ?></span></h3>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a"></span>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Dépôt et retraits</strong><br />
	Rte du Tir Fédéral 10<br/>
	1762 Givisiez
</p>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Administration</strong><br />
	Rte du Petit-Moncor 1c<br/>
	1752 Villars-sur-Glâne
</p>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a; padding-top: 20px"></span>
<p style="font-size: 12px">
	Contactez-nous par mail sur info@festiloc.ch ou par téléphone<br />
	au 026 676 01 17 (lu-ve de 9h-12h et de 13h30-16h)<br />
	<strong>Découvrez tout notre assortiment sur <a href="http://www.festiloc.ch" style="color: #000; text-decoration: none">www.festiloc.ch</a></strong>
</p>
