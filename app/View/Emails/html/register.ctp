<strong><?php echo __('Thanks for your registration'); ?></strong><br>
<p><?php echo __('You need to activate your account by clicking the following link.'); ?></p>
<p><?php echo $this->Html->link(__('Activate my account'), $this->Html->url($link, true)); ?></p>