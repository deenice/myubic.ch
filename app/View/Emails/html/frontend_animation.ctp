<p>Salut <?php echo $userFirstName; ?>,</p>

<p>Pour nos événements de fin d'année, nous recherchons parmi nos freelances ceux qui ont l'âme d'un présentateur pour animer certaines de nos activités. Les aptitudes que nous recherchons sont les suivantes:</p>

<ul>
	<li>Aisance à s'exprimer face à un large public</li>
	<li>Talent d'expression orale</li>
	<li>Charisme</li>
	<li>Bonne présentation</li>
	<li>Bon niveau de maturité</li>
</ul>

<p>Les activités qui demandent ce type de profil sont le Casino des sens, la Cook'n'chef, les Soirées loup-garou, le Qui est qui, les Banquet gaming et Pursuit Gaming.</p>

<p>Nous allons prochainement organiser des formations spécifiques à ce type d'animation, alors si tu penses correspondre au profil et que tu es disponibles cette fin d'année, témoigne-nous ton intérêt en cliquant sur OUI. (en cas de doute, lance-toi!)<br />Une sélection sera établie en fonction des candidatures que nous recevons et de nos besoins. Les places sont limitées, merci de répondre au plus vite!</p>

<p>Si tu es intéressé(-e), il te suffit juste de cliquer sur le lien ci-dessous et tu seras automatiquement inscrit(-e) pour ce nouveau poste sur ton profil Myubic.
<br />
<?php echo $url; ?>
</p>

<p>A bientôt</p>
<p></p>
<p>Floriane</p>