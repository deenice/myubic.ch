<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous sommes à la recherche encore de 4 extras de service pour un événement se déroulant le <strong>vendredi 27 novembre 2015</strong>.</p>

<p>
	Les horaires sont encore à définir. Différents mandats débuteront à 13h00 et d'autres autour de 16h00 pour se terminer au plus tard à 04h00 du matin.<br />
Le lieu de l'événement se situe à la salle Davel à Cully.
</p>
<p>
	Si tu es disponible et intéressé, merci de m'informer par retour d'e-mail de vos dispos générales pour ce jour-là.<br />
En vous remerciant d'avance pour votre retour !
</p>
<p>
	Floriane
</p>

<p>
	<em>PS : ceci est <strong>un message automatique</strong> envoyé à tous nos extras de service. Si vous travaillez déjà ce jour-là ou si vous m'avez déjà informée de votre indisponibilité, vous pouvez directement supprimer ce message.</em>
</p>
