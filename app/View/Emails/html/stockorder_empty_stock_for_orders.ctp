<p>Notification automatique</p>

<p>Le produit <?php echo $this->Html->link($stockitem['StockItem']['code'] . ' ' . $stockitem['StockItem']['name'], array('controller' => 'stock_items', 'action' => 'view', 'full_base' => true, $stockitem['StockItem']['id'])); ?> sera en quantité insuffisante pour les commandes suivantes:</p>
<ul>
	<?php foreach($orders as $orderId => $orders1): ?>
		<?php foreach($orders1 as $order): ?>
		<li><?php echo $order['order_number']; ?> <?php echo $order['name']; ?> - <?php echo $order['date']; ?><br /><?php echo $order['needed']; ?> produits manquants</li>
		<?php endforeach; ?>
	<?php endforeach; ?>
</ul>

<p><?php echo $this->Html->link('Accéder à la page des conflits', array('controller' => 'stock_orders', 'action' => 'conflicts')); ?></p>

<p>http://www.myubic.ch</p>