<p>	*** mail automatique envoyé à tous les extras de service ***</p>
<p>

</p>
<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Le vendredi 15 janvier 2016 est d'ores et déjà chargé en événements pour Une-bonne-idée.ch &amp; Maier Grill.<br />
De ce fait, nous recherchons encore beaucoup de personnel de service pour différents événements se déroulant en soirée dans différents lieux en Romandie.
</p>
<p>
	Si vous n'êtes pas déjà inscrits pour travailler ce jour-là et que vous êtes intéressés et disponible, je vous remercie de m'indiquer <strong>à partir de quelle heure vous êtes disponible le vendredi 15 janvier</strong>.<br />
En fonction des réponses, nous vous proposerons le job extra adapté à votre profil et à vos horaires.
</p>
<p>
	Ce mail étant envoyé à plus de 150 personnes, <u>je ne répondrai à aucun e-mail</u>. Vous recevrez dans un second temps l'extra qui vous sera attribué par la personne en charge de l'événement en question.
</p>
<p>
	Floriane
</p>
