<p>	*** mail automatique envoyé à tous les extras de service ***</p>
<p>

</p>
<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>En dernière minute, nous recherchons deux extras de service pour l'événement suivant :</p>
<p>
	<strong>Vendredi 8 janvier 2016</strong><br />
Service d'un apéritif - service plateau demandé<br />
Apéritif organisé par le Conseiller National, Dominique de Buman.
</p>
<p>
	Timing : 17h00 à 20h00
</p>
<p>
	Lieu : Caveau de Dominique de Buman situé à côté de la Cathédrale de Fribourg
</p>
<p></p>
<p>
	Merci aux personnes intéressées et disponibles de se manifester par e-mail.
</p>
<p>
	Bon début d'année à tous !
</p>
<p>
	Floriane
</p>
