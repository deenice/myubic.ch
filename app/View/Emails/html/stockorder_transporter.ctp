<p>Bonjour Monsieur <?php echo $toName; ?>,</p>

<p>Veuillez trouver ci-joint votre bulletin de transport.</p>

<p>
	<ul>
		<li><?php echo $type; ?> par vos soins le: <?php echo $date; ?></li>
		<li>Poids approximatif: <?php echo $weight; ?></li>
		<li>Nombre de palettes: <?php echo $packaging['pallets']; ?></li>
		<li>Nombre de rollis: <?php echo $packaging['rollis']; ?></li>
		<li>Nombre de palettes XL: <?php echo $packaging['pallets_xl']; ?></li>
		<li>Nombre de rollis XL: <?php echo $packaging['rollis_xl']; ?></li>
	</ul>
</p>

<p>Pour toute question, vous pouvez nous contacter au 026 409 70 15.</p>

<p>Adresse et personne de contact sur place: selon bulletin de livraison/reprise ci-joint.</p>

<p>Merci de remettre le document ci-joint dûment signé à ma collègue, Marie, lors du retrait de votre matériel au dépôt.</p>
  
<p>En vous remerciant pour votre collaboration, nous vous souhaitons une agréable journée et vous envoyons nos meilleures salutations.</p>

<p>
<br /><br />
</p>

<img src="http://www.festiloc.ch/data/web/festiloc3.ch/uploads/festiloc_seul_logo.png" width="140px">

<h3 style="font-size: 14px;">Nicolas Perrinjaquet<span style="font-weight: normal"> | Dispatcher</span></h3>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a"></span>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Dépôt et retraits</strong><br />
	Rte du Tir Fédéral 10<br/>
	1762 Givisiez
</p>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Administration</strong><br />
	Rte du Petit-Moncor 1c<br/>
	1752 Villars-sur-Glâne
</p>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a; padding-top: 20px"></span>
<p style="font-size: 12px">
	Contactez-nous par téléphone au 026 409 70 15 pour toute question.
</p>
