<p>	*** mail automatique envoyé à tous les extras en animation parlant anglais ***</p>
<p>

</p>
<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous sommes à la recherche de deux extras anglophones pour un wine gaming le lundi 14 mars 2016 à Lausanne<br/>
Horaire approximatif: 13h30- 21h30<br />
Nombre de participants: 25
</p>
<p>
	Le Wine gaming est une enquête. Après le meurtre d'un vigneron les participants doivent retrouver le coupable à travers différent défis à l'aide d'une tablette.
</p>
<p>
	Cette activité nécessite une formation préalable entre jeudi et vendredi (Lorène responsable de l'activité étant en vacances après)<br />
	Ce n'est pas une activité compliquée, il y a de la mise en place à faire et ensuite les participants suivent le processus grâce à la tablette.
</p>
<p>
	Si tu es dispo cette fin de semaine pour une formation et le lundi 14 mars. Le job est pour toi!<br />
	Si tu es dispo uniquement le lundi 14 mars, dit moi toujours et on regardera comment faire cela.<br>
</p>
<p>
	Je vous souhaite une belle journée
</p>
<p>
	Fabienne
</p>
