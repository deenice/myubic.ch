<p>Bonjour <?php echo $civilities[$stockorder['ContactPeopleClient']['civility']]; ?> <?php echo $stockorder['ContactPeopleClient']['last_name']; ?>,</p>

<p>Nous vous remercions pour votre commande et avons le plaisir de vous remettre en pièces jointes notre confirmation avec les conditions de location.</p>

<p>Pour le paiement, merci de vous référer à nos conditions générales. Vous avez la possibilité de régler directement le montant à notre dépôt de Givisiez, en cash ou par cartes EC. La facture finale et le bulletin de versement vous parviendront par courrier dans la semaine suivant le retour du matériel. Celle-ci devra être réglée dans les 10 jours.</p>

<p>Pour toute modification de commande une fois la confirmation reçue de notre part, des frais peuvent être demandés.</p>

<p>Nous restons volontiers à votre disposition pour tout renseignement complémentaire.</p>

<p>En vous remerciant de la confiance que vous nous témoignez, nous vous souhaitons une agréable journée et vous adressons nos meilleures salutations.</p>

<p>
<br /><br />
</p>

<img src="http://www.festiloc.ch/data/web/festiloc3.ch/uploads/festiloc_seul_logo.png" width="140px">

<h3 style="font-size: 14px;"><?php echo $senderName; ?><span style="font-weight: normal"><?php if(!empty($senderFunction)): ?> | <?php echo $senderFunction; ?><?php endif; ?></span></h3>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a"></span>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Dépôt et retraits</strong><br />
	Rte du Tir Fédéral 10<br/>
	1762 Givisiez
</p>

<p style="font-size: 12px">
	<strong style="font-size: 11px">Administration</strong><br />
	Rte du Petit-Moncor 1c<br/>
	1752 Villars-sur-Glâne
</p>

<span style="width: 20px; display: block; border-bottom: 1px solid #78171a; padding-top: 20px"></span>
<p style="font-size: 12px">
	Contactez-nous par mail sur info@festiloc.ch ou par téléphone<br />
	au 026 676 01 17 (lu-ve de 9h-12h et de 13h30-16h)<br />
	<strong>Découvrez tout notre assortiment sur <a href="http://www.festiloc.ch" style="color: #000; text-decoration: none">www.festiloc.ch</a></strong>
</p>
