<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous recherchons encore plusieurs extras pour cette fin de semaine. Es-tu disponible et intéressé :</p>

<p>
	<strong>Jeudi 3 décembre 2015 - Neuchâtel</strong><br />
	Horaires au départ de Villars-sur-Glâne : 6h-16h45<br />
	Aide-animation Cook'n'Chef
</p>
<p>
	<strong>Vendredi 4 décembre 2015 - Lausanne</strong><br />
	Horaire au départ de Villars-sur-Glâne : 15h-21h30<br />
	Animateur Casino des Sens
</p>
<p>
	<strong>Vendredi 4 décembre 2015 - Vuissens</strong><br />
	Horaire au départ de Villars-sur-Glâne : 15h45-00h30
</p>

<p>
	Si tu es disponible et intéressé, merci de m'informer par retour d'e-mail rapidement.<br />
En te remerciant d'avance pour ton retour !
</p>

<p>
	Floriane
</p>

<p>
	<em>PS : ceci est <b>un message automatique</b> envoyé à tous nos extras actifs en animation. Si vous travaillez déjà ce jour-là ou si vous m'avez déjà informée de votre indisponibilité, vous pouvez directement supprimer ce message sans me répondre.</em>
</p>
