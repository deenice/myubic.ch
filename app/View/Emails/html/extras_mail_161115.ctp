<p>Bonjour <?php echo $userFirstName; ?>,</p>

<p>Nous sommes à la recherche encore d'un animateur pour l'activité "Les Experts" pour un événement se déroulant ce <strong>vendredi 20 novembre 2015</strong>.</p>

<p>
	Les horaires sont de <strong>14h45 à 20h30 sur place à Yverdon</strong>. Avec possibilité de co-voiturage au départ de Villars-sur-Glâne.
</p>

<p>
	Si tu es disponible et intéressé, merci de m'informer par retour d'e-mail rapidement.<br />
En te remerciant d'avance pour ton retour !
</p>

<p>
	Floriane
</p>

<p>
	<em>PS : ceci est <strong>un message automatique</strong> envoyé à tous nos extras actifs en animation. Si vous travaillez déjà ce jour-là ou si vous m'avez déjà informée de votre indisponibilité, vous pouvez directement supprimer ce message.</em>
</p>
