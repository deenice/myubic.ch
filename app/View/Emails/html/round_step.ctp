<p>Salut <?php echo $userFirstName; ?>,<br><br>

Voici une proposition de job faite pour toi ! Es-tu disponible et intéressé(-e) par le poste suivant :
<ul>
<li><?php echo $eventDate; ?></li>
<li><?php echo $eventName; ?></li>
<li>Client : <?php echo $clientName; ?></li>
<li><?php echo $eventPax; ?> PAX</li>
<?php if(!empty($placeName)): ?><li><?php echo $placeName; ?></li><?php endif; ?>
</ul>
Horaire approximatif de ton extra (calculé au départ et à l'arrivée de Villars-sur-Glâne)<br>
de <?php echo $jobStartTime; ?> à <?php echo $jobEndTime; ?><br /><br />

Ta mission sera donc : <?php echo $jobJob; ?>, <?php echo (!empty($jobActivityName)) ?  $jobActivityName : ''; ?> (<?php echo $jobSalary; ?> CHF / heure)<br /><br />

<?php if(!empty($message)): ?>
	<?php echo nl2br($message); ?><br /> <br />
<?php endif ?>


Si tu es intéressé par le poste, clique sur ce bouton:
<br />
<br />
<?php echo $this->element('Emails/button', array('background' => '#1BBC9B', 'title' => __("I'm interested"), 'href' => '#')); ?>
<br />
<br />
Si tu ne peux pas être présent, merci de cliquer sur ce bouton:
<br />
<br />
<?php echo $this->element('Emails/button', array('background' => '#E7505A', 'title' => __('Decline'), 'href' => '#')); ?>
<br />
<br />
Toute l'équipe d'Une-bonne-idée.ch te souhaite une belle journée.<br />
<strong>Floriane</strong>
</p>
<p></p>
<p>*** Ce mail a été envoyé automatiquement. ***</p>
