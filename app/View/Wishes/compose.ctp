<?php $this->assign('page_title', $this->Html->link(__('Wishes'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Compose message')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-tree"></i><?php echo __('Clients 2016'); ?>
		</div>
	</div>
	<div class="portlet-body">
    <?php echo $this->Form->create('SendWish', array('url' => array('controller' => 'wishes', 'action' => 'send'))); ?>
    <div class="row">
      <div class="col-md-9">
		    <?php echo $this->Form->input('SendWish.message', array('label' => false, 'value' => $this->element('Emails/default_wishes'), 'class' => 'form-control ckeditor', 'type' => 'textarea', 'id' => 'ckeditor')); ?>
      </div>
      <div class="col-md-3">
        <div class="alert alert-info">
          <strong>Placeholders disponibles</strong><br>
          <ul class="list-unstyled">
            <li>%first_name% : Prénom de la personne de contact</li>
            <li>%last_name% : Nom de famille de la personne de contact</li>
            <li>%url% : Lien vers la carte digitale (ne pas supprimer!)</li>
            <li>%resp_full_name% : Responsable du dossier</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-12">
          <br>
          <button type="submit" class="btn blue"><i class="fa fa-envelope-o"></i> <?php echo __('Send'); ?></button>
          <br>
          <br>
        </div>
      </div>
    </div>
    <table class="table table-striped table-wishes">
      <thead>
        <tr>
          <th class="check">&nbsp;</th>
          <th class="client"><?php echo __('Client'); ?></th>
          <th class="contact"><?php echo __('Contact person'); ?></th>
          <th class="contact"><?php echo __('Responsable'); ?></th>
          <th class="events"><?php echo __('Events'); ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($wishes as $wish): if(empty($wish['ContactPeople']['email'])){continue;}?>
        <?php if($wish['Wish']['sent']): ?>
        <tr class="sent">
        <?php else: ?>
        <tr>
        <?php endif; ?>
          <td>
            <?php echo $wish['Wish']['sent'] == 0 ? $this->Form->input('SendWish.send.', array('type' => 'checkbox', 'value' => $wish['Wish']['id'], 'label' => false, 'div' => false, 'hiddenField'=>false)) : ''; ?>
          </td>
          <td><?php echo $this->Html->link($wish['Client']['name'], array('controller' => 'clients', 'action' => 'view', $wish['Wish']['client_id']), array('target' => '_blank')); ?></td>
          <td><?php echo $wish['ContactPeople']['full_name']; ?> <br> <?php echo $wish['ContactPeople']['email']; ?></td>
          <td><?php echo $wish['User']['full_name']; ?></td>
          <td>
            <ul class="list-unstyled">
              <?php foreach($wish['events'] as $event):?>
                <li>
                  <?php echo $statuses[$event['Event']['crm_status']]; ?>:
                  <?php if(!empty($event['Event']['confirmed_date'])): ?>
                  <?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?>
                  <?php elseif(!empty($event['Date'])): ?>
                    <?php foreach($event['Date'] as $k => $date): ?>
                    <?php echo $this->Time->format('d.m.Y', $date['date']); ?>
                    <?php if(isset($event['Date'][$k+1])): ?>, <?php endif; ?>
                    <?php endforeach; ?>
                  <?php elseif(!empty($event['Event']['period_start'])): ?>
                    <?php echo $this->Time->format('d.m.Y', $event['Event']['period_start']); ?>
                    <?php if(!empty($event['Event']['period_end'])): ?>
                     - <?php echo $this->Time->format('d.m.Y', $event['Event']['period_end']); ?>
                    <?php endif; ?>
                  <?php endif; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
	</div>
</div>


<?php
$this->start('page_level_plugins');
echo $this->Html->script('//cdn.ckeditor.com/4.6.0/basic/ckeditor.js');
$this->end();
?>