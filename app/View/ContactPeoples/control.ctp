<?php $this->assign('page_title', __('Contact persons')); ?>
<?php $this->assign('page_subtitle', __('Control')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i><?php echo __('Contact persons'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('ContactPeoples'); ?>
	</div>
</div>