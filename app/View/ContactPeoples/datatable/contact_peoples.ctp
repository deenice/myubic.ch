<?php

foreach ($dtResults as $result) {

	// $actions = $this->Html->link(
	//     '<i class="fa fa-search"></i> ' . __("View"), 
	//     array('controller' => 'activities', 'action' => 'view', $result['Activity']['id']), 
	//     array('class' => 'btn default btn-xs', 'escape' => false)
	// );
	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"), 
	    array('controller' => 'contact_peoples', 'action' => 'edit', $result['ContactPeople']['id']), 
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);
	// $actions .= $this->Html->link(
	//     '<i class="fa fa-file-o"></i> ' . __("Download PDF"), 
	//     array('controller' => 'activities', 'action' => 'pdf', $result['Activity']['id'] . '.pdf'), 
	//     array('class' => 'btn default btn-xs', 'escape' => false)
	// );
	
    $this->dtResponse['aaData'][] = array(
        $result['ContactPeople']['name'],
        $result['ContactPeople']['first_name'],
        $result['ContactPeople']['last_name'],
        $actions
    );
}