<?php $this->assign('page_title', $this->Html->link(__('Clients'), array('controller' => 'clients', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Add a contact person'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-user font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a contact person'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('ContactPeople', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 1)); ?>
		<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('First name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('first_name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Last name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('last_name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Civility'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('civility', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.civilities')));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('email', array('type' => 'email', 'label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Function'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('function', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Department'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('department', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Language'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('language', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.languages')));?>
				</div>
			</div>		
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>			
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>