<?php $this->assign('page_title', __('Artists')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-star"></i><?php echo __('List of all artists'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add an artist'), array('controller' => 'artists', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('All'); ?>
	</div>
</div>
<?php
$this->start('init_scripts');
//echo 'Custom.activities();';
$this->end();
?>