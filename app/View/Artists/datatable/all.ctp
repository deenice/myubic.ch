<?php
foreach ($dtResults as $result) {

    $contact = $result['Artist']['contact_person_first_name'];
	$contact .= ' ' . $result['Artist']['contact_person_last_name'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['Artist']['contact_person_email'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['Artist']['contact_person_phone'];

	// $address = $result['Client']['address'] . $this->Html->tag('br') . $result['Client']['zip'] . ' ' . $result['Client']['city'];

	// $actions = $this->Html->link(
 //        '<i class="fa fa-search"></i> ' . __("View"), 
 //        array('controller' => 'artists', 'action' => 'view', $result['Artist']['id']), 
 //        array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
 //    );
 //    $actions .= $this->Html->tag('');
    $actions = $this->Html->link(
        '<i class="fa fa-edit"></i> ' . __("Edit"), 
        array('controller' => 'artists', 'action' => 'edit', $result['Artist']['id']), 
        array('class' => 'btn btn-warning btn-xs', 'escape' => false)
    );

    $tags = '';
    if(!empty($result['Tag'])){
        foreach($result['Tag'] as $tag){
            $icon = $this->Html->tag('i', '', array('class' => 'fa fa-tag')) . ' ';
            $tags .= $this->Html->tag('span', $icon . $tag['value'], array('class' => 'btn btn-xs bg-green'));
        }
    }

    $this->dtResponse['aaData'][] = array(
        $result['Artist']['name'],
        $contact,
        $tags,
        $actions
    );
}