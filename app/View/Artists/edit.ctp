<?php $this->assign('page_title', $this->Html->link(__('Artists'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $artist['Artist']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-star font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $artist['Artist']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Artist', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
		<?php echo $this->Form->input('Artist.id', array('type' => 'hidden')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
						<span class="help-block"><?php echo __('Troup name for example'); ?></span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Website'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('website', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Tags'); ?></label>
					<div class="col-md-9">									
						<?php echo $this->Form->input('Artist.tags', array('type' => 'hidden', 'class' => 'form-control select2', 'label' => false, 'data-category' => 'artist', 'value' => (!empty($tags['artist'])) ? implode(',', $tags['artist']):'')); ?>
					</div>					
				</div>
				<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('First name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_first_name', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Last name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_last_name', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_phone', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_email', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.artist();';
$this->end();
?>