<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Pricelist')); ?>
<div class="row hidden-print">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-filter"></i><?php echo __('Filter the products'); ?>
				</div>
				<div class="actions">
					<?php echo $this->Html->link(
						'<i class="fa fa-file-pdf-o"></i> ' . __('Export in PDF'), 
						array(
							'controller' => 'stockitems', 
							'action' => 'pricelist', 
							!empty($category) ? $category : 0, 
							!empty($section) ? $section : 0, 
							'liste.pdf'),
						array(
							'escape' => false,
							'class' => 'btn btn-primary'
						)
					); ?>
				</div>
			</div>
			<div class="portlet-body">
				<?php echo $this->Form->create('PriceList'); ?>
				<div class="row">
					<div class="col-md-5">
						<?php echo $this->Form->input('category_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true)); ?>
					</div>
					<div class="col-md-5">
						<?php echo $this->Form->input('section_id', array('label' => false, 'class' => 'form-control', 'value' => !empty($section) ? $section : '')); ?>
					</div>
					<div class="col-md-2">
						<button class="btn default btn-block" type="submit"><?php echo __('View articles'); ?></button>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="row top">
					<div class="col-md-3">
						<img src="http://www.festiloc.ch/data/web/festiloc2.ch/templates/img/logo.png" class="img-responsive">
					</div>
					<div class="col-md-6">
						<h3>liste de prix<br>location vaisselle</h3>
						<h4>prix 2015</h4>
					</div>
					<div class="col-md-3">
						<p>
							Festiloc<br />
							Route du Tir Fédéral 10<br />
							1762 Givisiez<br />
							026 676 01 17<br />
							www.festiloc.ch
						</p>
					</div>
				</div>
				<?php foreach($articles as $sectionArticles): ?>
					<div class="row header">
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-2">location <?php echo strtolower($sectionArticles[0]['StockSection']['name']); ?></div>
								<div class="col-md-7">no ref <br /> nom article</div>
								<div class="col-md-3">prix [CHF]</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-2">&nbsp;</div>
								<div class="col-md-7">no ref <br /> nom article</div>
								<div class="col-md-3">prix [CHF]</div>
							</div>
						</div>
					</div>
					<div class="row">
					<?php foreach($sectionArticles as $article): ?>
						<div class="col-md-6 product">
							<div class="row">
								<div class="col-md-2 image" data-article-id="<?php echo $article['StockItem']['id']; ?>">
									<?php //echo $this->Html->image(DS . $article['Document'][1]['url'], array('class' => 'img-responsive')); ?>
								</div>
								<div class="col-md-7">
									<p>
										<?php echo $article['StockItem']['code']; ?><br />
										<?php echo $article['StockItem']['name']; ?><br />
										<?php echo $article['StockFamily']['name']; ?>
									</p>
								</div>
								<div class="col-md-3">
									<p><?php echo $article['StockItem']['price']; ?></p>
								</div>	
							</div>
						</div>
					<?php endforeach; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/admin/pages/css/invoice.css');
echo $this->Html->css('http://fonts.googleapis.com/css?family=Cabin:400,500,600,700');
$this->end();
$this->start('init_scripts');
echo 'Custom.pricelist()';
$this->end();
?>
