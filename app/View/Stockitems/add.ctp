<?php $this->assign('page_title', !empty($companyId) ? $companies[$companyId] : __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Add a stock item'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-basket font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a stock item'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockItem', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reference'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('reference', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">
							Fournisseur, Référence du produit, Prix, Remarques
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'data-company' => '', 'options' => $companies, 'empty' => true, 'required' => true, 'value' => empty($company) ? '' : $company['Company']['id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_category_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-category' => '', 'options' => array(), 'empty' => true, 'required' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Section'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_section_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-section' => '', 'options' => array(), 'empty' => true, 'required' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Family'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_family_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-family' => '', 'options' => array(), 'empty' => true, 'required' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Supplier'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('supplier_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $suppliers, 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Price'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('price', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Replacement price'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('replacement_price', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Quantity'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('quantity', array('label' => false, 'class' => 'form-control input-inline input-small'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Default reprocessing duration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('default_reconditionning_duration', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Status'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Stockitems.status')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Status informations'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Online'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('online', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('To process'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('to_process', array('label' => false, 'class' => 'bs-select form-control', 'options' => Configure::read('StockItems.checkStatuses')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Subject to discount'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('subject_to_discount', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small', 'checked' => true));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockitems();';
echo 'Custom.stockitem();';
$this->end();
?>
