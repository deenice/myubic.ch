<?php
class UserHelper extends AppHelper {

	public function hasRights($params = array()){
		App::import("Model", "User");  
		$User = new User();
		$User->id = AuthComponent::user('id');
		return $User->hasRights(
			empty($params['controller']) ? $this->params['controller'] : $params['controller'],
			empty($params['action']) ? $this->params['action'] : $params['action']
		);
	}

	public function getRole(){
		App::import("Model", "User");  
		$User = new User();
		$User->id = AuthComponent::user('id');
		return $User->getRole();
	}

	public function getGroup(){
		App::import("Model", "User");  
		$User = new User();
		$User->id = AuthComponent::user('id');
		return $User->getGroup();
	}

}