<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class TalentsHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

    public function generateInput($model) {
    	$talents = array();
    	if(isset($model['Tag'])){
	    	foreach($model['Tag'] as $tag){
	    		if($tag['category'] == 'talent') $talents[] = $tag['id'];
	    	}
	    }

        echo $this->Form->input('talents', array('type' => 'hidden', 'id' => 'select2_talents', 'label' => false, 'class' => 'select2 form-control', 'default' => implode(',',$talents)));

    }
}