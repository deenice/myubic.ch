<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class AvailabilityHelper extends AppHelper {

	public $helpers = array('Html', 'Form', 'ImageSize');

    public function generateButton($model, $tagId, $label) {

        if(isset($model['Tag'])){
            foreach($model['Tag'] as $tag){
                if($tag['category'] == 'availability') $tagIds[] = $tag['id'];
            }
        }        
        if(isset($tagIds) && in_array($tagId, $tagIds)){
        	$checked = 1;
        } else {
        	$checked = 0;
        }
        echo $this->Form->checkbox('availability', array('value' => $tagId, 'label' => false, 'hiddenField' => false, 'name' => 'data[User][availability][]', 'class' => 'toggle', 'checked' => $checked)) . $label;

    }

    public function generateButtons($ids = array(), $data = array()){
        //prepare data to check if id exists
        $existingIds = array();
        if(!empty($data)) foreach($data as $d){
            if($d['category'] == 'availability') $existingIds[] = $d['id'];
        }
        // generate output
        $output = '';
        $labels = array(0 => __('No'), 1 => __('Yes'), 2 => __('Maybe'), 3 => __("Don't know"));
        array_push($ids, null);
        foreach($ids as $k => $id){
            $checked = '';
            $labelClass = 'btn btn-default btn-xs';
            if(in_array($id, $existingIds)){
                $labelClass .= ' active';
                $checked = 'checked';
            } elseif($k == 3){
                if(!in_array($id, $existingIds) && !in_array($ids[$k-1], $existingIds) && !in_array($ids[$k-2], $existingIds)&& !in_array($ids[$k-3], $existingIds)){
                    $labelClass .= ' active';                    
                }
            }
            $input = $this->Form->checkbox('User.availabilities.', array(
                'class' => 'toggle', 
                'div' => false, 
                'value' => $id, 
                'hiddenField' => false, 
                'checked' => $checked)
            ) . ' ' . $labels[$k];
            $label = $this->Html->tag('label', $input, array('class' => $labelClass));
            $output .= $label;
        }
        echo $this->Html->tag('div', $output, array('class' => 'btn-group', 'data-toggle' => 'buttons'));
    }

    public function generateLine($ids = array(), $data = array()){

        $stop = false;
        $existingIds = array();
        foreach($data as $tag){
            $existingIds[$tag['id']] = $tag;
        }
        foreach($ids as $k => $id){
            if(array_key_exists($id, $existingIds)){
                $tmp = explode('_', $existingIds[$id]['value']);
                $tdText = __(ucfirst($tmp[2]));
                if($tmp[2] == 'yes') $tdClass = 'bg-green-jungle';
                if($tmp[2] == 'maybe') $tdClass = 'bg-yellow-saffron';
                $stop = true;
            } elseif(!$stop) {
                $tdClass = 'bg-red-pink';
                $tdText = __('No');
            }
        }
        echo $this->Html->tag('td', $tdText, array('class' => $tdClass));

    }

    public function generateUsers($users, $day){
        if(!empty($users[$day])){
            $output = '';
            foreach($users[$day] as $key => $user){
                $portrait = '';
                $badges = '';
                $sections = '';
                $h4 = $this->Html->tag('h4', $user['User']['full_name']);
                if(!empty($user['Section'])){
                    foreach($user['Section'] as $i => $section){
                        $sections .= $section['value'];
                        if(!empty($user['Section'][$i+1])) $sections .= ' - ';
                    }
                }
                $h5 = $this->Html->tag('h5', $sections);
                $options = array('class' => 'btn grey-cararra btn-icon-only input-sm tooltips', 'escape' => false, 'data-placement' => 'top');
                $options1 = array_merge($options, array('data-original-title' => __('Send an email')));
                $options2 = array_merge($options, array('data-original-title' => __('Make a phone call')));
                $options3 = array_merge($options, array('data-original-title' => __('View profile')));
                $mail = $this->Html->link(
                    '<i class="fa fa-envelope-o"></i>', 
                    'mailto:' . $user['User']['email'], 
                    $options1
                );
                $phone = $this->Html->link(
                    '<i class="fa fa-phone"></i>', 
                    'tel:' . $user['User']['phone'], 
                    $options2
                );
                $view = $this->Html->link(
                    '<i class="fa fa-user"></i>', 
                    array('controller' => 'users', 'action' => 'view', $user['User']['id']),
                    $options3
                );
                $actions = $this->Html->tag('p', $mail.$phone.$view);
                foreach ($user['Availabilities'] as $key => $item) {
                    $availability = explode('_', $item['value']);
                    if($availability[2] == 'yes') $bgClass = 'bg-green';
                    if($availability[2] == 'maybe') $bgClass = 'bg-yellow';
                    if($availability[0] == $day) $badges .= $this->Html->tag('span', $availability[1], array('class' => 'badge badge-roundless ' . $bgClass));
                }
                $badges1 = $this->Html->tag('p', $badges);
                $caption = $this->Html->tag('div', $h4.$h5.$actions.$badges1, array('class' => 'caption'));
                if(!empty($user['Document'][0]['url'])){
                    $portrait = $this->Html->image($this->ImageSize->crop($user['Document'][0]['url'], 200, 200));
                } else {
                    $portrait = ($user['User']['gender'] == 'F') ? $this->Html->image('icons/user_F.png') : $this->Html->image('icons/user_M.png');
                }
                $thumbnail = $this->Html->tag('div', $portrait.$caption, array('class' => 'thumbnail'));
                $output .= $this->Html->tag('div', $thumbnail, array('class' => 'col-md-3 col-lg-2 col-sm-4 text-center availability-user'));
            }
            return $this->Html->tag('div', $output, array('class' => 'row'));
        } else {
            $warning    = $this->Html->tag('div', __('No collaborator is available!'), array('class' => 'alert alert-warning'));
            $col        = $this->Html->tag('div', $warning, array('class' => 'col-md-12'));
            return $this->Html->tag('div', $col, array('class' => 'row'));
        }
    }

    public function getActualStock($stockItemId){
        App::import("Model", "StockItem");  
        $StockItem = new StockItem();  
        $StockItem->id = $stockItemId;
        return $StockItem->getActualAvailibility(); 
    }
}