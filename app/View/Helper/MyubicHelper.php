<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class MyubicHelper extends AppHelper {

	public $helpers = array('Html');

    public function acronym( $string ) {
      $string = Inflector::slug($string, ' ');
      return preg_replace('~\b(\w)|.~', '$1', $string);
    }
}
