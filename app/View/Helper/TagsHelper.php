<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class TagsHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

    public function generateInput($inputName, $category, $model) {
        $data = array();
        $class = 'select2 form-control';
        if(isset($model['Tag'])){
            foreach($model['Tag'] as $tag){
                if($tag['category'] == $category) $data[] = $tag['id'];
            }
        }
        if($category == 'configuration_type'){
            $class .= ' configuration-type';
        }
        $id = str_replace('.', '', $inputName);
        echo $this->Form->input($inputName, array('type' => 'hidden', 'id' => 'select2_' . $id, 'label' => false, 'class' => $class, 'default' => implode(',',$data)));
    }

    public function generateButton($model, $tagId, $label) {

        if(isset($model['Tag'])){
            foreach($model['Tag'] as $tag){
                if($tag['category'] == 'availability') $tagIds[] = $tag['id'];
            }
        }        
        if(isset($tagIds) && in_array($tagId, $tagIds)){
        	$checked = 1;
        } else {
        	$checked = 0;
        }
        echo $this->Form->checkbox('availability', array('value' => $tagId, 'label' => false, 'hiddenField' => false, 'name' => 'data[User][availability][]', 'class' => 'toggle', 'checked' => $checked)) . $label;
    }
}