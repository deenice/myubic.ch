<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class MenusHelper extends AppHelper {

	public $helpers = array('Html', 'Form', 'ImageSize');

  public function build( $name = null ){

    App::import("Model", "Menu");
    $Menu = new Menu();
    $html = '';

    $parents = $Menu->find('all', array(
			'order' => 'Menu.lft',
			'conditions' => array(
				'parent_id' => null,
				'menu' => $name
			)
		));

		$menu = array();
		foreach($parents as $parent){
			$menu[$parent['Menu']['id']] = $parent;
			$menu[$parent['Menu']['id']]['children'] = array();
			$models = $Menu->children($parent['Menu']['id'], true);
			foreach($models as $model){
				$menu[$parent['Menu']['id']]['children'][$model['Menu']['id']] = $model;
				$menu[$parent['Menu']['id']]['children'][$model['Menu']['id']]['children'] = array();
				$items = $Menu->children($model['Menu']['id'], true);
				foreach($items as $item){
					$menu[$parent['Menu']['id']]['children'][$model['Menu']['id']]['children'][$item['Menu']['id']] = $item;
				}
			}
		}

    foreach($menu as $parent){
      $html .= $this->_View->element('Menus/parent', array('parent' => $parent));
    }

    echo $this->_View->element('Menus/base', array('base' => $html));

  }

}
