<?php

/* /app/View/Helper/LienHelper.php */
App::uses('AppHelper', 'View/Helper');

class LanguagesHelper extends AppHelper {

	public $helpers = array('Html', 'Form');

    public function generateInput($model) {
    	$langs = array();
    	if(isset($model['Tag'])){
	    	foreach($model['Tag'] as $tag){
	    		if($tag['category'] == 'language') $langs[] = $tag['id'];
	    	}
	    }

        echo $this->Form->input('languages', array('type' => 'hidden', 'id' => 'select2_languages', 'label' => false, 'class' => 'select2 form-control', 'default' => implode(',',$langs)));

    }
}