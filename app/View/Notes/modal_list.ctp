<?php $categories = Configure::read('Notes.categories'); ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo $title; ?></h4>
</div>
<div class="modal-body note-list">
	<div class="row">
		<div class="col-md-12">
			<div class="timeline">
        <?php foreach($notes as $note): ?>
        <div class="timeline-item">
          <div class="timeline-badge">
            <?php if(!empty($note['User']['Portrait'][0]['url'])): ?>
            <?php echo $this->Html->image(
              $this->ImageSize->crop($note['User']['Portrait'][0]['url'], 80, 80),
              array('class' => 'timeline-badge-userpic')
            ); ?>
            <?php else: ?>
            <?php echo $this->Html->image('icons/user.png', array('class' => 'timeline-badge-userpic')); ?>
            <?php endif; ?>
          </div>
          <div class="timeline-body <?php echo $note['Note']['level']; ?>">
            <div class="timeline-body-arrow"></div>
            <div class="timeline-body-head">
              <div class="timeline-body-head-caption">
                <a href="javascript:;" class="timeline-body-title font-blue-madison"><?php echo $note['User']['full_name']; ?></a>
                <span class="timeline-body-time font-grey-cascade"><?php echo __('Creation date: %s', $this->Time->format($note['Note']['created'], '%d %B %Y %H:%M')); ?></span>
              </div>
              <div class="timeline-body-head-actions">
                <?php if(!empty($note['Note']['category'])): ?>
                  <?php if($note['Note']['model'] == 'event'): ?>
                  <a href="#portlet-<?php echo $note['Note']['category']; ?>" class="btn btn-xs btn-default">
                  <?php else: ?>
                  <a href="javascript:;" class="btn btn-xs btn-default">
                  <?php endif; ?>
                    <i class="fa fa-tag"></i> <?php echo strtolower($categories[$note['Note']['category']]); ?></a>
                  <?php if($note['Note']['sticky']): ?>&nbsp;<?php endif; ?>
                <?php endif; ?>
                <?php if($note['Note']['sticky']): ?>
                  <i class="icon-pin"></i>
                <?php endif; ?>
              </div>
            </div>
            <div class="timeline-body-content">
              <span class="uppercase font-grey-cascade"><?php echo $note['Note']['title']; ?></span><br>
              <span class="font-grey-cascade"><?php echo nl2br($note['Note']['message']); ?></span>
              <div class="text-right">
                <a href="#" class="btn btn-xs btn-default edit-note" data-toggle="modal" data-target="#new-note" data-note-id="<?php echo $note['Note']['id']; ?>" data-category="<?php echo $note['Note']['category'];?>"><i class="icon-note"></i> <?php echo __('Edit'); ?></a>
                <a href="#" class="btn btn-xs btn-danger delete-note" data-note-id="<?php echo $note['Note']['id']; ?>"><i class="icon-trash"></i> <?php echo __('Delete'); ?></a>
              </div>
            </div>
          </div>
        </div>
			  <?php endforeach; ?>
      </div>
		</div>
	</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <a href="#" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#new-note" data-model="<?php echo $note['Note']['model']; ?>" data-model-id="<?php echo $note['Note']['model_id']; ?>"><?php echo __('Add another note'); ?></a>
</div>
