<?php $this->assign('page_title', $this->Html->link(__('Activities'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add an activity'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-trophy font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an activity'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Activity', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'autofocus' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Acronym'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('slug', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'value' => 2)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Rules'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('rules', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Hints'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('hints', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Infos TOP'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('infos_top', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
//echo 'Custom.activities();';
$this->end();
?>
