<?php $this->assign('page_title', __('Activities')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-trophy"></i><?php echo __('List of all activities'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add an activity'), array('controller' => 'activities', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Activity'); ?>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.activities();';
$this->end();
?>