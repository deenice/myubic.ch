<?php $this->assign('page_title', __('Activities'));?>
<?php $this->assign('page_subtitle', $activity['Activity']['name']);?>
<div class="row activity">
	<div class="col-md-3" id="activity-menu">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light profile-sidebar-portlet">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic">
						<?php //echo $this->Html->image($this->ImageSize->crop($user['Portrait'][0]['url'], 200, 200), array('class' => 'img-responsive')); ?>
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name"><?php echo $activity['Activity']['name']; ?></div>
						<?php if(!empty($activity['Game'])): ?>
						<div class="profile-usertitle-job"><?php echo __('%s games', sizeof($activity['Game'])) ?></div>
						<?php endif; ?>
						<?php if(!empty($activity['Company'])): ?>
							<button class="btn btn-sm btn-company btn-company-<?php echo $activity['Company']['class']; ?>"></button>
						<?php endif; ?>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu" id="activity-nav">
						<ul class="nav" >
							<li class="active">
								<a href="#activity">
									<i class="icon-trophy"></i>
									<strong><?php echo $activity['Activity']['name']; ?></strong>
									<span class="pull-right font-grey">
										<?php if(!empty($documents['photos'])): ?>
										<i class="fa fa-image"></i>
										<?php endif; ?>
										<?php if(!empty($documents['videos'])): ?>
										<i class="fa fa-film"></i>
										<?php endif; ?>
										<?php if(!empty($documents['documents'])): ?>
										<i class="fa fa-files-o"></i>
										<?php endif; ?>
									</span>
								</a>
							</li>
							<?php if(!empty($activity['Game'])): ?>
								<?php foreach($activity['Game'] as $game): ?>
							<li>
								<a href="#game<?php echo $game['id']; ?>">
									<i class="icon-game-controller"></i>
									<?php echo $game['name']; ?>
									<span class="pull-right font-grey">
										<?php if(!empty($documents['games'][$game['id']]['photos'])): ?>
										<i class="fa fa-image"></i>
										<?php endif; ?>
										<?php if(!empty($documents['games'][$game['id']]['videos'])): ?>
										<i class="fa fa-film"></i>
										<?php endif; ?>
										<?php if(!empty($documents['games'][$game['id']]['documents'])): ?>
										<i class="fa fa-files-o"></i>
										<?php endif; ?>
									</span>
								</a>
							</li>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php if(!empty($staff)): ?>
							<li>
								<a href="#staff">
									<i class="icon-graduation"></i>
									<?php echo __('Trained staff'); ?>
									<span class="pull-right font-grey">
										(<?php echo $number_of_staff; ?>)
									</span>
								</a>
							</li>
							<?php endif; ?>
						</ul>
					</div>
					<!-- END MENU -->
				</div>

			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="row">
			<div class="col-md-12" id="activity">
				<div class="portlet light bordered active">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject bold uppercase"><i class="fa fa-trophy"></i> <?php echo $activity['Activity']['name']; ?></span>
						</div>
					</div>
					<div class="portlet-body">
		        <ul class="nav nav-tabs">
		          <li class="active">
		            <a href="#description" data-toggle="tab"> <?php echo __('Description'); ?> </a>
		          </li>
		          <li>
		            <a href="#rules" data-toggle="tab"> <?php echo __('Rules'); ?> </a>
		          </li>
		          <li>
		            <a href="#hints" data-toggle="tab"> <?php echo __('Hints'); ?> </a>
		          </li>
							<?php if(!empty($activity['Activity']['infos_top'])): ?>
		          <li>
		            <a href="#infos" data-toggle="tab"> <?php echo __('Infos TOP'); ?> </a>
		          </li>
							<?php endif; ?>
							<?php if(!empty($documents['photos'])): ?>
		          <li>
		            <a href="#photos" data-toggle="tab"> <?php echo __('Photos'); ?> </a>
		          </li>
							<?php endif; ?>
							<?php if(!empty($documents['videos'])): ?>
		          <li>
		            <a href="#videos" data-toggle="tab"> <?php echo __('Videos'); ?> </a>
		          </li>
							<?php endif; ?>
							<?php if(!empty($documents['documents'])): ?>
		          <li>
		            <a href="#documents" data-toggle="tab"> <?php echo __('Documents'); ?> </a>
		          </li>
							<?php endif; ?>
		        </ul>
		        <div class="tab-content">
		          <div class="tab-pane active" id="description">
		            <?php echo nl2br($activity['Activity']['description']); ?>
		          </div>
		          <div class="tab-pane" id="rules">
		            <?php echo nl2br($activity['Activity']['rules']); ?>
		          </div>
		          <div class="tab-pane" id="hints">
		            <?php echo nl2br($activity['Activity']['hints']); ?>
		          </div>
							<?php if(!empty($activity['Activity']['infos_top'])): ?>
		          <div class="tab-pane" id="infos">
		            <?php echo nl2br($activity['Activity']['infos_top']); ?>
		          </div>
							<?php endif; ?>
							<?php if(!empty($documents['photos'])): ?>
		          <div class="tab-pane" id="photos">
              	<div class="cbp activity-portfolio">
									<?php foreach($documents['photos'] as $photo): ?>
                    <div class="cbp-item">
                      <a href="<?php echo Router::url(DS . $photo['Document']['url']); ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $photo['Document']['name']; ?>">
                        <div class="cbp-caption-defaultWrap">
                          <?php //echo $this->Html->image(DS . $photo['Document']['url']); ?>
                          <?php echo $this->Html->image($this->ImageSize->crop(DS . $photo['Document']['url'], 400, 250)); ?>
												</div>
                        <div class="cbp-caption-activeWrap">
                          <div class="cbp-l-caption-alignLeft">
                            <div class="cbp-l-caption-body">
                              <div class="cbp-l-caption-title"><?php echo $photo['Document']['name']; ?></div>
                              <div class="cbp-l-caption-desc"><?php echo __('added by %s', $photo['User']['full_name']); ?></div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
									<?php endforeach; ?>
                </div>
		          </div>
							<?php endif; ?>
							<?php if(!empty($documents['videos'])): ?>
		          <div class="tab-pane" id="videos">
								<table class="table table-striped table-bordered table-advance">
									<tbody>
									<?php foreach($documents['videos'] as $video): ?>
										<?php $ext = pathinfo($video['Document']['url'], PATHINFO_EXTENSION); ?>
										<tr>
											<td>
											<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('width' => '24px')); ?>
											<?php echo $this->Html->link($video['Document']['name'], DS . $video['Document']['url'], array('target' => '_blank')); ?></td>
											<td class="hidden">
												<?php echo $this->Html->link(__('Download'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
												<?php echo $this->Html->link(__('Share'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
		          </div>
							<?php endif; ?>
							<?php if(!empty($documents['documents'])): ?>
		          <div class="tab-pane" id="documents">
								<table class="table table-striped table-bordered table-advance">
									<tbody>
									<?php foreach($documents['documents'] as $doc): ?>
										<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
										<tr>
											<td>
											<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('width' => '24px')); ?>
											<?php echo $this->Html->link($doc['Document']['name'], DS . $doc['Document']['url'], array('target' => '_blank')); ?></td>
											<td class="hidden">
												<?php echo $this->Html->link(__('Download'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
												<?php echo $this->Html->link(__('Share'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
		          </div>
							<?php endif; ?>
		        </div>
					</div>
				</div>
			</div>
			<?php if(!empty($activity['Game'])): ?>
				<?php foreach($activity['Game'] as $game): ?>
					<div class="col-md-12" id="game<?php echo $game['id']; ?>">
						<div class="portlet light bordered">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-gamepad"></i> <span class="caption-subject bold uppercase"><?php echo $game['name']; ?></span>
								</div>
							</div>
							<div class="portlet-body">
				        <ul class="nav nav-tabs">
				          <li class="active">
				            <a href="#game_description_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Description'); ?> </a>
				          </li>
				          <li>
				            <a href="#game_settingup_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Setting up'); ?> </a>
				          </li>
				          <li>
				            <a href="#game_rules_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Rules'); ?> </a>
				          </li>
				          <li>
				            <a href="#game_animation_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Animation'); ?> </a>
				          </li>
									<?php if(!empty($game['anecdote'])): ?>
				          <li>
				            <a href="#game_anecdote_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Assets'); ?> </a>
				          </li>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['photos'])): ?>
				          <li>
				            <a href="#game_photos_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Photos'); ?> </a>
				          </li>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['videos'])): ?>
				          <li>
				            <a href="#game_videos_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Videos'); ?> </a>
				          </li>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['documents'])): ?>
				          <li>
				            <a href="#game_documents_<?php echo $game['id']; ?>" data-toggle="tab"> <?php echo __('Documents'); ?> </a>
				          </li>
									<?php endif; ?>
				        </ul>
				        <div class="tab-content">
				          <div class="tab-pane active" id="game_description_<?php echo $game['id']; ?>">
				            <?php echo nl2br($game['description']); ?>
				          </div>
				          <div class="tab-pane" id="game_settingup_<?php echo $game['id']; ?>">
				            <?php echo nl2br($game['setting_up']); ?>
				          </div>
				          <div class="tab-pane" id="game_rules_<?php echo $game['id']; ?>">
				            <?php echo nl2br($game['rules']); ?>
				          </div>
				          <div class="tab-pane" id="game_animation_<?php echo $game['id']; ?>">
				            <?php echo nl2br($game['animation']); ?>
				          </div>
									<?php if(!empty($game['anecdote'])): ?>
				          <div class="tab-pane" id="game_anecdote_<?php echo $game['id']; ?>">
				            <?php echo nl2br($game['anecdote']); ?>
				          </div>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['photos'])): ?>
				          <div class="tab-pane" id="game_photos_<?php echo $game['id']; ?>">
		              	<div class="cbp game-portfolio">
											<?php foreach($documents['games'][$game['id']]['photos'] as $photo): ?>
		                    <div class="cbp-item">
		                      <a href="<?php echo Router::url(DS . $photo['Document']['url']); ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $photo['Document']['name']; ?>">
		                        <div class="cbp-caption-defaultWrap">
															<?php echo $this->Html->image($this->ImageSize->crop(DS . $photo['Document']['url'], 400, 250)); ?>
														</div>
		                        <div class="cbp-caption-activeWrap">
		                          <div class="cbp-l-caption-alignLeft">
		                            <div class="cbp-l-caption-body">
		                              <div class="cbp-l-caption-title"><?php echo $photo['Document']['name']; ?></div>
		                              <div class="cbp-l-caption-desc"><?php echo __('added by %s', $photo['User']['full_name']); ?></div>
		                            </div>
		                          </div>
		                        </div>
		                      </a>
		                    </div>
											<?php endforeach; ?>
		                </div>
				          </div>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['videos'])): ?>
				          <div class="tab-pane" id="game_videos_<?php echo $game['id']; ?>">
										<table class="table table-striped table-bordered table-advance">
											<tbody>
											<?php foreach($documents['games'][$game['id']]['videos'] as $video): ?>
												<?php $ext = pathinfo($video['Document']['url'], PATHINFO_EXTENSION); ?>
												<tr>
													<td>
													<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('width' => '24px')); ?>
													<?php echo $this->Html->link($video['Document']['name'], DS . $video['Document']['url'], array('target' => '_blank')); ?></td>
													<td class="hidden">
														<?php echo $this->Html->link(__('Download'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
														<?php echo $this->Html->link(__('Share'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
													</td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
				          </div>
									<?php endif; ?>
									<?php if(!empty($documents['games'][$game['id']]['documents'])): ?>
				          <div class="tab-pane" id="game_documents_<?php echo $game['id']; ?>">
										<table class="table table-striped table-bordered table-advance">
											<tbody>
											<?php foreach($documents['games'][$game['id']]['documents'] as $doc): ?>
												<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
												<tr>
													<td>
													<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('width' => '24px')); ?>
													<?php echo $this->Html->link($doc['Document']['name'], DS . $doc['Document']['url'], array('target' => '_blank')); ?></td>
													<td class="hidden">
														<?php echo $this->Html->link(__('Download'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
														<?php echo $this->Html->link(__('Share'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
													</td>
												</tr>
											<?php endforeach; ?>
											</tbody>
										</table>
				          </div>
									<?php endif; ?>
				        </div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
			<div class="col-md-12" id="staff">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject bold uppercase"><?php echo __('Trained staff'); ?> <?php echo sprintf('(%d)', $number_of_staff); ?></span>
						</div>
					</div>
					<div class="portlet-body trained-staff">
						<div class="tabbable-line">
							<ul class="nav nav-tabs">
								<?php $i=0; foreach($staff as $job => $hierarchies1): ?>
									<li<?php echo $i==0 ? ' class="active"' : ''; ?>>
										<a href="#<?php echo $job; ?>" data-toggle="tab">
											<?php echo $jobs[$job]; ?>
										</a>
									</li>
								<?php $i++; endforeach; ?>
							</ul>
							<div class="tab-content">
								<?php $i=0; foreach($staff as $job => $hierarchies1): ?>
								<div class="tab-pane<?php echo $i==0 ? ' active':''; ?>" id="<?php echo $job; ?>">
									<?php foreach($hierarchies1 as $hierarchy => $users): ?>
										<h4><?php echo $hierarchies[$hierarchy]; ?></h4>
										<div class="staff">
										<?php foreach($users as $user): ?>
											<?php if(!empty($user['Portrait'][0]['url'])): ?>
											<a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $user['User']['id'])); ?>" data-toggle="modal" data-target="#modal-user-ajax" class="tooltips" data-placement="top" data-original-title="<?php echo $user['User']['full_name']; ?>">
												<?php if(!empty($user['Portrait'][0]['url'])) echo $this->Html->image($this->ImageSize->crop(DS . $user['Portrait'][0]['url'], 50, 50)); ?>
											</a>
											<?php else: ?>
											<?php //echo $this->Html->image("http://lorempixel.com/40/40/people/" . rand(0,10)); ?>
											<?php endif; ?>
										<?php endforeach; ?>
										<div class="clearfix"></div>
										</div>
									<?php endforeach; ?>
								</div>
								<?php $i++; endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/theme/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.activities();';
$this->end();
?>
