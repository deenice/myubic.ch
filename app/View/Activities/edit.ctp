<?php $this->assign('page_title', $this->Html->link(__('Activities'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $activity['Activity']['name']);?>
<div class="tabbable-custom tabbable-tabdrop" id="tabs">
  <ul class="nav nav-tabs">
    <li class="active">
      <a href="#activity" data-toggle="tab"><i class="icon-trophy"></i> <strong><?php echo $activity['Activity']['name']; ?></strong></a>
    </li>
    <?php if(!empty($activity['Game'])): ?>
      <?php foreach($activity['Game'] as $game): ?>
      <li>
        <a href="#game<?php echo $game['id']; ?>" data-toggle="tab"><i class="icon-game-controller"></i> <?php echo $game['name']; ?></a>
      </li>
      <?php endforeach; ?>
    <?php endif; ?>
    <li>
      <a href="#add" data-toggle="tab"><i class="fa fa-plus"></i> <?php echo __('Add a game'); ?></a>
    </li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="activity">
      <div class="portlet light">
        <div class="portlet-title tabbable-line">
          <ul class="nav nav-tabs">
            <li class="active">
              <a href="#activityInfos<?php echo $activity['Activity']['id']; ?>" data-toggle="tab">
              <?php echo __('Infos'); ?> </a>
            </li>
            <li>
              <a href="#activityPhotos<?php echo $activity['Activity']['id']; ?>" data-toggle="tab">
              <?php echo __('Photos'); ?> </a>
            </li>
            <li>
              <a href="#activityVideos<?php echo $activity['Activity']['id']; ?>" data-toggle="tab">
              <?php echo __('Videos'); ?> </a>
            </li>
            <li>
              <a href="#activityDocuments<?php echo $activity['Activity']['id']; ?>" data-toggle="tab">
              <?php echo __('Documents'); ?> </a>
            </li>
            <li>
              <a href="#planning<?php echo $activity['Activity']['id']; ?>" data-toggle="tab">
              <?php echo __('Planning'); ?> </a>
            </li>
          </ul>
        </div>
        <div class="portlet-body form">
          <div class="tab-content">
            <div class="tab-pane active" id="activityInfos<?php echo $activity['Activity']['id']; ?>">
              <!-- BEGIN FORM-->
              <?php echo $this->Form->create('Activity', array('class' => 'form-horizontal')); ?>
              <?php echo $this->Form->input('Activity.id', array('type' => 'hidden', 'value' => $activity['Activity']['id'])); ?>
                <div class="form-body">
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Acronym'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('slug', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Company'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Description'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Rules'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('rules', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Hints'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('hints', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3"><?php echo __('Infos TOP'); ?></label>
                    <div class="col-md-9">
                      <?php echo $this->Form->input('infos_top', array('label' => false, 'class' => 'form-control')); ?>
                    </div>
                  </div>
                </div>
                <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
                      <button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
                    </div>
                  </div>
                </div>
              <?php echo $this->Form->end(); ?>
              <!-- END FORM-->
            </div>
            <div class="tab-pane" id="activityPhotos<?php echo $activity['Activity']['id']; ?>">
              <?php if(!empty($documents['photos'])): ?>
              <div class="row mix-grid">
                <div class="col-md-12">
                  <h3><?php echo __('Existing photos'); ?></h3>
                  <?php foreach($documents['photos'] as $k => $doc): ?>
                    <?php $title = $doc['Document']['name']; ?>
                    <div class="col-md-3 col-sm-4 mix" data-rel="activityPhoto<?php echo $k; ?>" data-id="<?php echo $doc['Document']['id']; ?>">
                      <div class="mix-inner">
                        <?php echo $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')); ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $title; ?></h4>
                            <small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
                          </div>
                          <div class="panel-body text-center">
                             <div class="btn-group">
                              <?php echo $this->Html->link(
                                '<i class="fa fa-search"></i> ' . __('See'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'fancybox-button btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-rel' => 'fancybox-button'
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-times"></i> ' . __('Remove'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs delete-document',
                                  'data-document-id' => $doc['Document']['id']
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-pencil"></i> ' . __('Edit'),
                                '#activityPhoto' . $k,
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-toggle' => 'modal'
                                )
                              ); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="activityPhoto<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit photo</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                              <?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                            <button type="button" class="btn blue edit-document">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
              <?php endif; ?>
              <div class="row">
                <div class="col-md-12">
                  <h3><?php echo __('New photos'); ?></h3>
                  <?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
                  <?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Activity']['id'])); ?>
                  <?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'activity_photo')); ?>
                  <?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'activities')); ?>
                  <?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
                  <?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
                  <?php echo $this->Form->end(); ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="activityVideos<?php echo $activity['Activity']['id']; ?>">
              <?php if(!empty($documents['videos'])): ?>
              <div class="row mix-grid">
                <div class="col-md-12">
                  <h3><?php echo __('Existing videos'); ?></h3>
                  <?php foreach($documents['videos'] as $k => $doc): ?>
                    <?php $title = $doc['Document']['name']; ?>
                    <?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
                    <div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
                      <div class="mix-inner">
                        <?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $title; ?></h4>
                            <small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
                          </div>
                          <div class="panel-body text-center">
                             <div class="btn-group">
                              <?php echo $this->Html->link(
                                '<i class="fa fa-eye"></i> ' . __('See'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'fancybox-button btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-rel' => 'fancybox-button',
                                  'target' => '_blank'
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-times"></i> ' . __('Remove'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs delete-document',
                                  'data-document-id' => $doc['Document']['id']
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-pencil"></i> ' . __('Edit'),
                                '#activityDocument' . $k,
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-toggle' => 'modal'
                                )
                              ); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="activityVideo<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit video</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                              <?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                            <button type="button" class="btn blue edit-document">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
              <?php endif; ?>
              <div class="row">
                <div class="col-md-12">
                  <h3><?php echo __('New videos'); ?></h3>
                  <?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
                  <?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Activity']['id'])); ?>
                  <?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'activity_video')); ?>
                  <?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'activities')); ?>
                  <?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
                  <?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'mp4,flv')); ?>
                  <?php echo $this->Form->end(); ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="activityDocuments<?php echo $activity['Activity']['id']; ?>">
              <?php if(!empty($documents['documents'])): ?>
              <div class="row mix-grid">
                <div class="col-md-12">
                  <h3><?php echo __('Existing documents'); ?></h3>
                  <?php foreach($documents['documents'] as $k => $doc): ?>
                    <?php $title = $doc['Document']['name']; ?>
                    <?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
                    <div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
                      <div class="mix-inner">
                        <?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"><?php echo $title; ?></h4>
                            <small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
                          </div>
                          <div class="panel-body text-center">
                             <div class="btn-group">
                              <?php echo $this->Html->link(
                                '<i class="fa fa-eye"></i> ' . __('See'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'fancybox-button btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-rel' => 'fancybox-button',
                                  'target' => '_blank'
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-times"></i> ' . __('Remove'),
                                DS . $doc['Document']['url'],
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs delete-document',
                                  'data-document-id' => $doc['Document']['id']
                                )
                              ); ?>
                              <?php echo $this->Html->link(
                                '<i class="fa fa-pencil"></i> ' . __('Edit'),
                                '#activityDocument' . $k,
                                array(
                                  'escape' => false,
                                  'class' => 'btn btn-default btn-xs',
                                  'title' => $title,
                                  'data-toggle' => 'modal'
                                )
                              ); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="activityDocument<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit document</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                              <?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                            <button type="button" class="btn blue edit-document">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php endforeach; ?>
                </div>
              </div>
              <?php endif; ?>
              <div class="row">
                <div class="col-md-12">
                  <h3><?php echo __('New documents'); ?></h3>
                  <?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
                  <?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Activity']['id'])); ?>
                  <?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'activity_document')); ?>
                  <?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'activities')); ?>
                  <?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
                  <?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf,doc,docx')); ?>
                  <?php echo $this->Form->end(); ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="planning<?php echo $activity['Activity']['id']; ?>">
            <?php echo $this->Form->create('Activity'); ?>
            <?php echo $this->Form->input('Activity.id'); ?>
              <table class="table table-striped table-activity-planning">
                <thead>
                  <tr>
                    <th class="duration"><?php echo __('Duration'); ?></th>
                    <th class="name"><?php echo __('Name'); ?></th>
                    <th class="remarks"><?php echo __('Remarks'); ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($activity['PlanningBoardMoment'])): ?>
                    <?php foreach($activity['PlanningBoardMoment'] as $k => $moment): ?>
                      <tr>
                        <td>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', $k), array('value' => $this->Time->format('G:i', $moment['default_duration']), 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
                          <span class="help-inline">min</span>
                        </td>
                        <td>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', $k), array('value' => $moment['name'], 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        </td>
                        <td>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', $k), array('type' => 'text', 'value' => $moment['remarks'], 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.id', $k), array('value' => $moment['id'], 'type' => 'hidden')); ?>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', $k), array('value' => $activity['Activity']['id'], 'type' => 'hidden')); ?>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', $k), array('value' => 'activity', 'type' => 'hidden')); ?>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', $k), array('value' => $k, 'type' => 'hidden')); ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                    <tr>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', sizeof($activity['PlanningBoardMoment'])), array('value' => '0:00', 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
                        <span class="help-inline">min</span>
                      </td>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', sizeof($activity['PlanningBoardMoment'])), array('value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                      </td>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', sizeof($activity['PlanningBoardMoment'])), array('type' => 'text', 'value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', sizeof($activity['PlanningBoardMoment'])), array('value' => $activity['Activity']['id'], 'type' => 'hidden')); ?>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', sizeof($activity['PlanningBoardMoment'])), array('value' => 'activity', 'type' => 'hidden')); ?>
                          <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', sizeof($activity['PlanningBoardMoment'])), array('value' => sizeof($activity['PlanningBoardMoment']), 'type' => 'hidden')); ?>
                      </td>
                    </tr>
                  <?php else: ?>
                  <?php for($i=0;$i<3;$i++): ?>
                    <tr>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', $i), array('value' => '0:00', 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
                        <span class="help-inline">min</span>
                      </td>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', $i), array('value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                      </td>
                      <td>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', $i), array('type' => 'text', 'value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', $i), array('value' => $activity['Activity']['id'], 'type' => 'hidden')); ?>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', $i), array('value' => 'activity', 'type' => 'hidden')); ?>
                        <?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', $i), array('value' => $i, 'type' => 'hidden')); ?>
                      </td>
                    </tr>
                  <?php endfor; ?>
                  <?php endif; ?>
                </tbody>
              </table>
              <div class="form-actions">
                <div class="col-md-12">
                  <button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
                  <button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button> 
                </div>             
              </div>
              <?php echo $this->Form->end(); ?>
            </div>	
          </div>
        </div>
      </div>
    </div>
    <?php if(!empty($activity['Game'])): ?>
      <?php foreach($activity['Game'] as $game): ?>
      <div class="tab-pane" id="game<?php echo $game['id']; ?>">
        <div class="portlet light">
          <div class="portlet-title tabbable-line">
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#infos<?php echo $game['id']; ?>" data-toggle="tab">
                <?php echo __('Infos'); ?> </a>
              </li>
              <li>
                <a href="#photos<?php echo $game['id']; ?>" data-toggle="tab">
                <?php echo __('Photos'); ?> </a>
              </li>
              <li>
                <a href="#videos<?php echo $game['id']; ?>" data-toggle="tab">
                <?php echo __('Videos'); ?> </a>
              </li>
            </ul>
          </div>
          <div class="portlet-body form">
            <div class="tab-content">
              <div class="tab-pane active" id="infos<?php echo $game['id']; ?>">
                <!-- BEGIN FORM-->
                <?php echo $this->Form->create('Game', array('class' => 'form-horizontal', 'url' => array('action' => 'edit', 'controller' => 'games', $game['id']))); ?>
                <?php echo $this->Form->input('Game.id', array('type' => 'hidden', 'value' => $game['id'])); ?>
                <?php echo $this->Form->input('Game.activity_id', array('type' => 'hidden', 'value' => $activity['Activity']['id'])); ?>
                  <div class="form-body">
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.name', array('label' => false, 'class' => 'form-control', 'value' => $game['name'])); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Description'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.description', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => $game['description'])); ?>
                        <span class="help-block">Type et but du jeu</span>
                        <span class="help-block">Inspiré de ...</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Setting up'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.setting_up', array('label' => false, 'class' => 'form-control', 'value' => $game['setting_up'])); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Rules'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.rules', array('label' => false, 'class' => 'form-control', 'value' => $game['rules'])); ?>
                        <span class="help-block">Comment faire pour gagner des points</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Animation'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.animation', array('label' => false, 'class' => 'form-control', 'value' => $game['animation'])); ?>
                        <span class="help-block">Comment animer pour que ça fonctionne</span>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3"><?php echo __('Assets'); ?></label>
                      <div class="col-md-9">
                        <?php echo $this->Form->input('Game.anecdote', array('label' => false, 'class' => 'form-control', 'value' => $game['anecdote'])); ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
                        <button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
                      </div>
                    </div>
                  </div>
                <?php echo $this->Form->end(); ?>
                <!-- END FORM-->
              </div>
              <div class="tab-pane" id="photos<?php echo $game['id']; ?>">
                <?php if(!empty($documents['games'][$game['id']]['photos'])): ?>
                <div class="row mix-grid">
                  <div class="col-md-12">
                    <h3><?php echo __('Existing photos'); ?></h3>
                    <?php foreach($documents['games'][$game['id']]['photos'] as $k => $doc): ?>
                      <?php $title = $doc['Document']['name']; ?>
                      <div class="col-md-3 col-sm-4 mix" data-rel="activityPhoto<?php echo $k; ?>" data-id="<?php echo $doc['Document']['id']; ?>">
                        <div class="mix-inner">
                          <?php echo $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')); ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title"><?php echo $title; ?></h4>
                              <small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
                            </div>
                            <div class="panel-body text-center">
                               <div class="btn-group">
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-search"></i> ' . __('See'),
                                  DS . $doc['Document']['url'],
                                  array(
                                    'escape' => false,
                                    'class' => 'fancybox-button btn btn-default btn-xs',
                                    'title' => $title,
                                    'data-rel' => 'fancybox-button'
                                  )
                                ); ?>
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-times"></i> ' . __('Remove'),
                                  DS . $doc['Document']['url'],
                                  array(
                                    'escape' => false,
                                    'class' => 'btn btn-default btn-xs delete-document',
                                    'data-document-id' => $doc['Document']['id']
                                  )
                                ); ?>
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-pencil"></i> ' . __('Edit'),
                                  '#activityPhoto' . $k,
                                  array(
                                    'escape' => false,
                                    'class' => 'btn btn-default btn-xs',
                                    'title' => $title,
                                    'data-toggle' => 'modal'
                                  )
                                ); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="activityPhoto<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h4 class="modal-title">Edit photo</h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                <?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                              <button type="button" class="btn blue edit-document">Save</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <?php endif; ?>
                <div class="row">
                  <div class="col-md-12">
                    <h3><?php echo __('New photos'); ?></h3>
                    <?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
                    <?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $game['id'])); ?>
                    <?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'game_photo')); ?>
                    <?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'games')); ?>
                    <?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
                    <?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
                    <?php echo $this->Form->end(); ?>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="videos<?php echo $game['id']; ?>">

                <?php if(!empty($documents['games'][$game['id']]['videos'])): ?>
                <div class="row mix-grid">
                  <div class="col-md-12">
                    <h3><?php echo __('Existing videos'); ?></h3>
                    <?php foreach($documents['games'][$game['id']]['videos'] as $k => $doc): ?>
                      <?php $title = $doc['Document']['name']; ?>
                      <?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
                      <div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
                        <div class="mix-inner">
                          <?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
                          <div class="panel panel-default">
                            <div class="panel-heading">
                              <h4 class="panel-title"><?php echo $title; ?></h4>
                              <small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
                            </div>
                            <div class="panel-body text-center">
                               <div class="btn-group">
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-eye"></i> ' . __('See'),
                                  DS . $doc['Document']['url'],
                                  array(
                                    'escape' => false,
                                    'class' => 'fancybox-button btn btn-default btn-xs',
                                    'title' => $title,
                                    'data-rel' => 'fancybox-button',
                                    'target' => '_blank'
                                  )
                                ); ?>
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-times"></i> ' . __('Remove'),
                                  DS . $doc['Document']['url'],
                                  array(
                                    'escape' => false,
                                    'class' => 'btn btn-default btn-xs delete-document',
                                    'data-document-id' => $doc['Document']['id']
                                  )
                                ); ?>
                                <?php echo $this->Html->link(
                                  '<i class="fa fa-pencil"></i> ' . __('Edit'),
                                  '#activityDocument' . $k,
                                  array(
                                    'escape' => false,
                                    'class' => 'btn btn-default btn-xs',
                                    'title' => $title,
                                    'data-toggle' => 'modal'
                                  )
                                ); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div id="activityVideo<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                              <h4 class="modal-title">Edit document</h4>
                            </div>
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-md-12">
                                <?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                              <button type="button" class="btn blue edit-document">Save</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <?php endif; ?>

                <div class="row">
                  <div class="col-md-12">
                    <h3><?php echo __('New documents'); ?></h3>
                    <?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
                    <?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $game['id'])); ?>
                    <?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'game_video')); ?>
                    <?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'games')); ?>
                    <?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
                    <?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'mp4,flv')); ?>
                    <?php echo $this->Form->end(); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
    <?php endif; ?>
    <div class="tab-pane" id="add">
      <div class="portlet light">
        <div class="portlet-title">
          <div class="caption">
            <i class="icon-game-controller font-blue-madison"></i>
            <span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a game'); ?></span>
          </div>
        </div>
        <div class="portlet-body form">
          <!-- BEGIN FORM-->
          <?php echo $this->Form->create('Game', array('class' => 'form-horizontal', 'url' => array('action' => 'add', 'controller' => 'games'))); ?>
          <?php echo $this->Form->input('Game.activity_id', array('type' => 'hidden', 'value' => $activity['Activity']['id'])); ?>
            <div class="form-body">
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.name', array('label' => false, 'class' => 'form-control')); ?>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Description'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.description', array('label' => false, 'class' => 'form-control')); ?>
                  <span class="help-block">Type et but du jeu</span>
                  <span class="help-block">Inspiré de ...</span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Setting up'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.setting_up', array('label' => false, 'class' => 'form-control')); ?>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Rules'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.rules', array('label' => false, 'class' => 'form-control')); ?>
                  <span class="help-block">Comment faire pour gagner des points</span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Animation'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.animation', array('label' => false, 'class' => 'form-control')); ?>
                  <span class="help-block">Comment animer pour que ça fonctionne</span>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3"><?php echo __('Assets'); ?></label>
                <div class="col-md-9">
                  <?php echo $this->Form->input('Game.anecdote', array('label' => false, 'class' => 'form-control')); ?>
                </div>
              </div>
            </div>
            <div class="form-actions">
              <div class="row">
                <div class="col-md-offset-3 col-md-9">
                  <button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
                </div>
              </div>
            </div>
          <?php echo $this->Form->end(); ?>
          <!-- END FORM-->
        </div>
      </div>
    </div>
  </div>
</div>
<?php
$this->start('init_scripts');
echo 'Portfolio.init();';
//echo 'Custom.activities();';
$this->end();
?>
