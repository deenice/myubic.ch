<style type="text/css" media="screen,print">
.break{
   display: block;
   clear: both;
   page-break-after: always;
}
h1, h2 {
	font-weight: bold;
	text-transform: uppercase;
}
h3 {
	margin: 0 0 10px 0;
	font-weight: bold;
}
h3.panel-title {
	text-transform: uppercase;
}
.portlet {
	margin: 0;
}
</style>

<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<h1><?php echo $activity['Activity']['name']; ?></h1>
		</div>
	</div>
	<div class="portlet-body">
		<?php if(!empty($activity['Activity']['description'])): ?>
			<h3><?php echo __('Description'); ?></h3>
			<p><?php echo nl2br($activity['Activity']['description']); ?></p>
		<?php endif; ?>
		<?php if(!empty($activity['Activity']['rules'])): ?>
			<h3><?php echo __('Rules'); ?></h3>
			<p><?php echo nl2br($activity['Activity']['rules']); ?></p>
		<?php endif; ?>
		<?php if(!empty($activity['Activity']['hints'])): ?>
		<h3><?php echo __('Hints'); ?></h3>
		<p><?php echo nl2br($activity['Activity']['hints']); ?></p>
		<?php endif; ?>
		<?php if($photo && !empty($activity['Photo'])): ?>
			<h3><?php echo __('Photos'); ?></h3>
			<div class="row">
				<?php foreach($activity['Photo'] as $doc): ?>
					<div class="col-xs-4" style="margin-bottom: 10px">
						<?php //echo $this->Html->image($this->ImageSize->crop(DS . $doc['url'], 400, 250)); ?>
            <?php echo $this->Html->image(DS.$doc['url'], ['class' => 'img-responsive']); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<div class="break"></div>
<?php foreach($activity['Game'] as $game): ?>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<h2><?php echo $activity['Activity']['name'] . ' - ' . $game['name']; ?></h2>
		</div>
	</div>
	<div class="portlet-body">
		<?php if(!empty($game['description'])): ?>
			<h3><?php echo __('Description'); ?></h3>
			<p><?php echo nl2br($game['description']); ?></p>
		<?php endif; ?>
		<?php if(!empty($game['rules'])): ?>
			<h3><?php echo __('Rules'); ?></h3>
			<p><?php echo nl2br($game['rules']); ?></p>
		<?php endif; ?>
		<?php if(!empty($game['animation'])): ?>
			<h3><?php echo __('Animation'); ?></h3>
			<p><?php echo nl2br($game['animation']); ?></p>
		<?php endif; ?>
		<?php if(!empty($game['setting_up'])): ?>
			<h3><?php echo __('Setting up'); ?></h3>
			<p><?php echo nl2br($game['setting_up']); ?></p>
		<?php endif; ?>
		<?php if(!empty($game['anecdote'])): ?>
			<h3><?php echo __('Anecdote'); ?></h3>
			<p><?php echo nl2br($game['anecdote']); ?></p>
		<?php endif; ?>
		<?php if($photo && !empty($game['Photo'])): ?>
			<h3><?php echo __('Photos'); ?></h3>
			<div class="row">
				<?php foreach($game['Photo'] as $doc): ?>
					<div class="col-xs-4" style="margin-bottom: 10px">
						<?php echo $this->Html->image(DS.$doc['url'], ['class' => 'img-responsive']); ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>
<div class="break"></div>
<?php endforeach; ?>
