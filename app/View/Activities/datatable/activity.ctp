<?php

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
	    '<i class="fa fa-search"></i> ' . __("View"),
	    array('controller' => 'activities', 'action' => 'view', $result['Activity']['id']),
	    array('class' => 'btn default btn-xs', 'escape' => false)
	);
	$actions .= $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'activities', 'action' => 'edit', $result['Activity']['id']),
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);
	$actions .= $this->Html->link(
	    '<i class="fa fa-file-o"></i> ' . __("Download PDF without images"),
	    array('controller' => 'activities', 'action' => 'pdf', $result['Activity']['id'] . '.pdf'),
	    array('class' => 'btn default btn-xs', 'escape' => false)
	);
	$actions .= $this->Html->link(
	    '<i class="fa fa-file-image-o"></i> ' . __("Download PDF with images"),
	    array('controller' => 'activities', 'action' => 'pdf', 'photos' => true, $result['Activity']['id'], 'ext' => 'pdf'),
	    array('class' => 'btn default btn-xs', 'escape' => false)
	);

    $this->dtResponse['aaData'][] = array(
        $result['Activity']['name'],
        $result['Company']['name'],
        $actions
    );
}
