<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<?php $authComponentDocument = AuthComponent::user('Portrait'); ?>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

		<head>
			<?php echo $this->Html->charset(); ?>
			<title>
				<?php echo __($title_for_layout); ?>
			</title>
			<?php echo $this->fetch('meta'); ?>
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta content="width=device-width, initial-scale=1" name="viewport"/>

			<!-- BEGIN GLOBAL MANDATORY STYLES -->
			<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
			<!-- END GLOBAL MANDATORY STYLES -->
			<!-- BEGIN PAGE LEVEL STYLES -->
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
			<?php
			echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/css/select2.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/css/select2-bootstrap.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/dropzone/dropzone.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/fancybox/source/jquery.fancybox.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
			//echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/datatables.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/pace/themes/pace-theme-big-counter.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/fullcalendar/fullcalendar.min.css');
			?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/css/components.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/css/plugins.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/blog.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/search.min.css'); ?>
			<?php echo $this->fetch('page_level_styles'); ?>
			<!-- END PAGE LEVEL STYLES -->
			<!-- BEGIN THEME STYLES -->
			<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/layout.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/themes/default.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/invoice.css'); ?>
			<?php
			echo $this->Html->css('/metronic/theme/assets/pages/css/portfolio.css');
			echo $this->Html->css('/metronic/theme/assets/pages/css/profile.css');
			?>
			<?php echo $this->Html->css('/metronic/custom/3d-corner-ribbons.css'); ?>
			<?php echo $this->Html->css('/metronic/custom/_all.css'); ?>
			<?php echo $this->Html->css('/flags-css/css/flag-icon.min.css'); ?>
			<!-- END THEME STYLES -->
			<?php echo $this->fetch('css'); ?>
			<link href='https://fonts.googleapis.com/css?family=Kreon:400,700,300' rel='stylesheet' type='text/css'>
		</head>

    <body class="page-container-bg-solid page-header-menu-fixed page-boxed<?php echo AuthComponent::user('id') == 1 ? ' admin':''; ?>">
			<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
										<!-- BEGIN LOGO -->
											<?php if(AuthComponent::user('role') == 'temporary'): ?>
											<a id="index" class="navbar-brand" href="<?php echo Router::url(array('controller' => 'me', 'action' => 'dashboard'), true); ?>">
											<?php else: ?>
											<a id="index" class="navbar-brand" href="<?php echo Router::url('/', true); ?>">
											<?php endif; ?>
												<div class="logo">
													<?php echo file_get_contents(IMAGES_URL . 'myubic/myubic-logo.svg'); ?>
												</div>
											</a>
										<!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
													<?php if($this->User->getRole() == 'fixed'): ?>
													<li class="dropdown dropdown-extended dropdown-notification dropdown-dark">
                            <a href="https://bitbucket.org/deenice/myubic.ch/issues?status=new&status=open" class="dropdown-toggle" target="_blank">
                              <i class="fa fa-bug"></i>
                            </a>
                          </li>
													<?php endif; ?>
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
																	<?php if(!empty($authComponentDocument[0]['url'])): ?>
																	<?php echo $this->Html->image(
																		$this->ImageSize->crop($authComponentDocument[0]['url'], 60, 60),
																		array('class' => 'hide1')
																	); ?>
																	<?php else: ?>
																	<?php echo $this->Html->image('icons/user.png', array('class' => 'hide1')); ?>
																	<?php endif; ?>
                                </a>
																<ul class="dropdown-menu dropdown-menu-default">
																	<?php if($this->User->hasRights(array('controller' => 'users', 'action' => 'view'))): ?>
																	<li>
																		<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'users', 'action' => 'view', AuthComponent::user('id')), array('escape' => false)); ?>
																	</li>
																	<?php else: ?>
																	<li>
																		<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'me', 'action' => 'profile'), array('escape' => false)); ?>
																	</li>
																	<?php endif; ?>
																	<?php if($this->User->hasRights(array('controller' => 'cache', 'action' => 'clear'))): ?>
																	<li>
																		<?php echo $this->Html->link('<i class="fa fa-recycle"></i> ' . __('Clear cache'), array('controller' => 'cache', 'action' => 'clear'), array('escape' => false)); ?>
																	</li>
																	<?php endif; ?>
																	<li class="divider"></li>
																	<li>
																		<?php echo $this->Html->link('<i class="icon-logout"></i> ' . __('Logout'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?>
																	</li>
																</ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">
									<?php if($this->User->getRole() == 'fixed'): ?>
									<?php echo $this->Form->create('Search', array('url' => array('controller' => 'search', 'action' => 'index'), 'class' => 'search-form', 'type' => 'get')); ?>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="<?php echo __('Search on myubic.ch') ?>" name="term">
										<span class="input-group-btn">
											<a href="javascript:;" class="btn submit">
												<i class="icon-magnifier"></i>
											</a>
										</span>
									</div>
									<?php echo $this->Form->end(); ?>
									<?php endif; ?>
										<div class="hor-menu">
											<?php if(AuthComponent::user('role') == 'temporary'): ?>
											<?php echo $this->Menus->build( 'extra-menu' ); ?>
											<?php else: ?>
											<?php echo $this->Menus->build( 'main-menu' ); ?>
											<?php endif; ?>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <!-- BEGIN PAGE CONTENT BODY -->
                <div class="page-content">
                    <div class="container">
                        <!-- BEGIN PAGE BREADCRUMBS -->
												<div class="row">
													<div class="col-md-8">
														<?php if($this->fetch('page_title')):?>
														<ul class="page-breadcrumb breadcrumb">
															<li><a><?php echo $this->fetch('page_title'); ?></a><i class="fa fa-circle"></i></li>
															<li><?php echo $this->fetch('page_subtitle'); ?></li>
														</ul>
														<?php endif;?>
													</div>
													<?php if(in_array($this->params->controller, array('festiloc', 'stock_orders', 'stock_items', 'stock_item_unavailabilities', 'stock_inventories', 'stock_storage_types'))): ?>
													<div class="col-md-4">
														<div class="search-form pull-right" style="margin-bottom: 20px">
			                        <div class="input-group">
																<div class="selectStockItemInput">
																<?php echo $this->Form->input('selectStockItem', array('type' => 'select', 'class' => 'form-control input-lg', 'label' => false, 'id' => 'selectStockItem', 'data-company-id' => isset($company) ? $company['Company']['id'] : 3)); ?>
																</div>
			                        </div>
				                    </div>
													</div>
													<?php endif; ?>
												</div>

                        <!-- END PAGE BREADCRUMBS -->
                        <!-- BEGIN PAGE CONTENT INNER -->
												<div id="flashMessage">
													<?php echo $this->Session->flash(); ?>
												</div>
                        <div class="page-content-inner" id="content">
                            <?php echo $this->fetch('content'); ?>
                        </div>
                        <!-- END PAGE CONTENT INNER -->
                    </div>
                </div>
                <!-- END PAGE CONTENT BODY -->
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container">
							<?php echo date('Y'); ?> &copy; MYUBIC
						</div>
        </div>
        <div class="scroll-to-top hidden-print">
            <i class="icon-arrow-up"></i>
        </div>

				<?php echo $this->element('Modals/add-note', array()); ?>
				<?php echo $this->element('Modals/list-note', array()); ?>
				<?php echo $this->element('Modals/user-ajax', array()); ?>
				<?php echo $this->element('Modals/preview-checklist', array()); ?>
				<?php echo $this->element('Modals/modal-calendar-task', array()); ?>
				<?php echo $this->element('Modals/ask-vehicle', array()); ?>
				<?php echo $this->element('Modals/show-conflicts', array()); ?>


        <!-- BEGIN CORE PLUGINS -->
				<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.min.js'); ?>
				<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
				<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-migrate.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.cokie.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.blockui.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
        <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
				<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootbox/bootbox.min.js'); ?>
				<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
        <!-- END CORE PLUGINS -->
				<?php echo $this->Js->writeBuffer(); ?>
				<!-- END CORE PLUGINS -->
				<!-- BEGIN PAGE LEVEL PLUGINS -->
				<?php echo $this->fetch('page_level_plugins'); ?>
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <?php echo $this->Html->script('/metronic/theme/assets/global/scripts/app.js'); ?>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
				<?php echo $this->Html->script('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/scripts/layout.min.js'); ?>
				<?php

				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr-CH.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');
				echo $this->Html->script('https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js');
				echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery-once/2.1.1/jquery.once.js');
				//echo $this->Html->script('/js/yadcf/jquery.dataTables.yadcf.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
				//echo $this->Html->script('/js/infinite-scroll/jquery.infinitescroll.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/dropzone/dropzone.min.js');
				echo $this->Html->script('/metronic/theme/assets/pages/scripts/portfolio.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/select2/js/select2.full.min.js');

				echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/lib/moment.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/fullcalendar.min.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/locale/fr.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js');
				echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js');
				echo $this->Html->script('//cdn.ckeditor.com/4.6.0/basic/ckeditor.js');
				?>
				<?php echo $this->fetch('page_level_scripts'); ?>
				<?php
				echo $this->fetch('dataTableSettings');
				echo $this->fetch('script');
				?>
				<?php echo $this->Html->script('/metronic/custom/custom.js?tok=' . time()); ?>
				<?php //echo $this->Html->script('/metronic/custom/custom.min.js'); ?>
				<!-- END PAGE LEVEL SCRIPTS -->
				<script>
				jQuery(document).ready(function() {
				  Custom.prepare();
				  Custom.init();
				  <?php echo $this->fetch('init_scripts'); ?>
				});
				</script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
