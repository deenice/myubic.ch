<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<?php echo $this->Html->css('/metronic/pages/css/coming-soon.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/global/css/components.css'); ?>
	<?php echo $this->Html->css('/metronic/global/css/plugins.css'); ?>
	<?php echo $this->Html->css('/metronic/css/layout.css'); ?>
	<?php echo $this->Html->css('/metronic/css/themes/dark.css'); ?>
	<?php echo $this->Html->css('/metronic/css/custom.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 coming-soon-header">
				<a class="brand" href="index.html">
				<img src="<?php echo $this->webroot; ?>metronic/img/logo_my_ubic.png" alt="" width="200px"/>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 coming-soon-content">
				<h1><?php echo __('Myubic.ch is under maintenance'); ?></h1>
				<p><?php echo __('For any question please contact <a href="mailto:denis@une-bonne-idee.ch">denis@une-bonne-idee.ch</a>'); ?></p>
			</div>
		</div>
		<!--/end row-->
		<div class="row">
			<div class="col-md-12 coming-soon-footer">
				 2014 &copy; Myubic.ch. Intranet of Ubic Group
			</div>
		</div>
	</div>
	<!-- END FOOTER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('/metronic/global/plugins/respond.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/excanvas.min.js'); ?>
	<![endif]-->
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-migrate.min.js'); ?>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.blockui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.cokie.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/uniform/jquery.uniform.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo $this->fetch('page_level_plugins'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php echo $this->Html->script('/metronic/global/scripts/metronic.js'); ?>
	<?php echo $this->Html->script('/metronic/scripts/layout.js'); ?>
	<?php echo $this->Html->script('/metronic/scripts/quick-sidebar.js'); ?>
	<?php echo $this->Html->script('/metronic/pages/scripts/index.js'); ?>
	<?php echo $this->Html->script('/metronic/pages/scripts/tasks.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/backstretch/jquery.backstretch.min.js'); ?>
	<?php //echo $this->Html->script('/metronic/pages/scripts/coming-soon.js'); ?>
	<?php echo $this->fetch('page_level_scripts'); ?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	jQuery(document).ready(function() {    
	   Metronic.init(); // init metronic core componets
	   Layout.init(); // init layout
	   //ComingSoon.init() // init quick sidebar
	   <?php
	   echo '// init background slide images
       $.backstretch([
	        "'.$this->webroot.'metronic/pages/media/bg/1.jpg",
	        "'.$this->webroot.'metronic/pages/media/bg/2.jpg",
	        "'.$this->webroot.'metronic/pages/media/bg/3.jpg",
	        "'.$this->webroot.'metronic/pages/media/bg/4.jpg"
	        ], {
	          fade: 1000,
	          duration: 8000
	    }
	    );
';
	   ?>
	   <?php echo $this->fetch('init_scripts'); ?>
	});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
</html>
