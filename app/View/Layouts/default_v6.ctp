<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
	<?php
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/select2.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/dropzone/css/dropzone.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/fancybox/source/jquery.fancybox.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.css');
	?>
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/components.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/plugins.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/css/layout.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/pages/css/invoice.css'); ?>
	<?php
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/portfolio.css');
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/profile-old.css');
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/profile.css');
	?>
	<?php echo $this->Html->css('/metronic/custom/3d-corner-ribbons.css'); ?>
	<?php //echo $this->Html->css('/metronic/custom/custom.min.css'); ?>
	<?php echo $this->Html->css('/metronic/custom/custom.css'); ?>
	<?php echo $this->Html->css('/flags-css/css/flag-icon.min.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
</head>
<!-- END HEAD -->
<?php $authComponentDocument = AuthComponent::user('Portrait'); ?>
<?php if(in_array($this->params->controller, array('festiloc', 'stock_orders', 'stock_items', 'stock_item_unavailabilities'))){
	$class = 'festiloc';
	} else {
		$class = '';
		} ?>
<body class="page-quick-sidebar-over-content metronic<?php echo Configure::read('Metronic.version'); ?> <?php echo $class; ?>">
<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>
	<!-- BEGIN MAIN LAYOUT -->
	<!-- HEADER BEGIN -->
    <header class="page-header">
        <nav class="navbar" role="navigation">
            <div class="container-fluid">
                <div class="havbar-header">
                	<!-- BEGIN LOGO -->
                    <a id="index" class="navbar-brand" href="<?php echo Router::url('/', true); ?>">
                        <!-- <img src="<?php echo $this->webroot; ?>/metronic/theme/assets/admin/layout<?php echo Configure::read('Metronic.version'); ?>/img/logo.png" alt="Logo"> -->
                        <h1 style="margin: 0; color: #fff; font-weight: 700">MYUBIC<small style="color: inherit">.ch</small></h1>
                    </a>
                	<!-- END LOGO -->
                	<?php if(Configure::read('debug')): ?>
					<a href="#" class="navbar-brand">
						<h1 style="margin: 0 0 0 50px; color: #fff; font-weight: normal; opacity: 0.3">
							Site de développement
						</h1>
					</a>
					<?php endif; ?>
	                <!-- BEGIN TOPBAR ACTIONS -->
	                <div class="topbar-actions">
	                	<?php if(AuthComponent::user('id') == 1): ?>
	                	<div class="btn-group-red btn-group">
							<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<i class="fa fa-plus"></i>
							</button>
							<ul class="dropdown-menu-v2" role="menu">
								<li>
									<?php echo $this->Html->link(__('Add a note'), array('controller' => 'notes', 'action' => 'add'), array('data-toggle' => 'modal', 'data-target' => '#note')); ?>
								</li>
							</ul>
						</div>
						<?php endif; ?>
	                	<?php if(1==2): ?>
		                <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
						<form class="search-form" action="extra_search.html" method="GET">
							<div class="input-group">
								<input type="text" class="form-control" placeholder="Search here" name="query">
								<span class="input-group-btn">
									<a href="javascript:;" class="btn submit"><i class="fa fa-search"></i></a>
								</span>
							</div>
						</form>
						<!-- END HEADER SEARCH BOX -->

	                	<!-- BEGIN GROUP NOTIFICATION -->
						<div class="btn-group-notification btn-group" id="header_notification_bar">
							<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<span class="badge">9</span>
							</button>
							<ul class="dropdown-menu-v2">
								<li class="external">
									<h3><span class="bold">12 pending</span> notifications</h3>
									<a href="#">view all</a>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px; padding: 0;" data-handle-color="#637283">
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-success">
														<i class="fa fa-plus"></i>
													</span>
													New user registered.
												</span>
												<span class="time">just now</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Server #12 overloaded.
												</span>
												<span class="time">3 mins</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-warning">
														<i class="fa fa-bell-o"></i>
													</span>
													Server #2 not responding.
												</span>
												<span class="time">10 mins</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-info">
														<i class="fa fa-bullhorn"></i>
													</span>
													Application error.
												</span>
												<span class="time">14 hrs</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Database overloaded 68%.
												</span>
												<span class="time">2 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
												A 	user IP blocked.
											</span>
												<span class="time">3 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-warning">
														<i class="fa fa-bell-o"></i>
													</span>
													Storage Server #4 not responding dfdfdfd.
												</span>
												<span class="time">4 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-info">
														<i class="fa fa-bullhorn"></i>
													</span>
													System Error.
												</span>
												<span class="time">5 days</span>
											</a>
										</li>
										<li>
											<a href="javascript:;">
												<span class="details">
													<span class="label label-sm label-icon label-danger">
														<i class="fa fa-bolt"></i>
													</span>
													Storage server failed.
												</span>
												<span class="time">9 days</span>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</div>
	                	<!-- END GROUP NOTIFICATION -->
						<?php endif; ?>
	                	<!-- BEGIN USER PROFILE -->
		                <div class="btn-group-img btn-group">
							<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
								<?php if(!empty($authComponentDocument[0]['url'])): ?>
								<?php echo $this->Html->image(
									$this->ImageSize->crop($authComponentDocument[0]['url'], 60, 60),
									array('class' => 'hide1')
								); ?>
								<?php else: ?>
								<?php echo $this->Html->image('icons/user.png', array('class' => 'hide1')); ?>
								<?php endif; ?>
							</button>
							<ul class="dropdown-menu-v2">
								<?php if($this->User->hasRights(array('controller' => 'users', 'action' => 'view'))): ?>
								<li>
									<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'users', 'action' => 'view', AuthComponent::user('id')), array('escape' => false)); ?>
								</li>
								<?php else: ?>
								<li>
									<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'me', 'action' => 'profile'), array('escape' => false)); ?>
								</li>
								<?php endif; ?>
								<?php if($this->User->hasRights(array('controller' => 'cache', 'action' => 'clear'))): ?>
								<li>
									<?php echo $this->Html->link('<i class="fa fa-recycle"></i> ' . __('Clear cache'), array('controller' => 'cache', 'action' => 'clear'), array('escape' => false)); ?>
								</li>
								<?php endif; ?>
								<li class="divider">
								</li>
								<li>
									<?php echo $this->Html->link('<i class="icon-logout"></i> ' . __('Logout'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?>
								</li>
							</ul>
						</div>
						<!-- END USER PROFILE -->
					</div>
	                <!-- END TOPBAR ACTIONS -->
                </div>
            </div>
            <!--/container-->
        </nav>
    </header>
	<!-- HEADER END -->

	<!-- PAGE CONTENT BEGIN -->
    <div class="container-fluid">
    	<div class="page-content page-content-popup">
    		<!-- BEGIN PAGE CONTENT FIXED -->
			<div class="page-content-fixed-header hidden-print">
				<?php if($this->fetch('page_title')): ?>
				<ul class="page-breadcrumb">
					<li><a><?php echo $this->fetch('page_title'); ?></a></li>
					<li><?php echo $this->fetch('page_subtitle'); ?></li>
				</ul>
				<?php endif;?>

				<div class="row">
					<div class="col-md-4 pull-right">
						<div class="selectStockItemInput">
						<?php echo $this->Form->input('selectStockItem', array('class' => 'form-control select2', 'label' => false, 'placeholder' => 'Recherche un produit par code / nom', 'id' => 'selectStockItem')); ?>
						</div>
					</div>
				</div>

				<div class="content-header-menu">
					<!-- <div style="line-height: 56px; padding-right: 20px" id="selectStockItemInput">
						<?php echo $this->Form->input('selectStockItem', array('label' => false, 'placeholder' => __('Enter the code/name of a product to view its data.'), 'class' => 'form-control')); ?>
					</div> -->

					<?php if(1==2): ?>
    				<!-- BEGIN DROPDOWN AJAX MENU -->
    				<div class="dropdown-ajax-menu btn-group">
						<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="fa fa-circle"></i>
							<i class="fa fa-circle"></i>
							<i class="fa fa-circle"></i>
						</button>
						<ul class="dropdown-menu-v2">
							<li> <a href="start.html">Application</a> </li>
							<li> <a href="start.html">Reports</a> </li>
							<li> <a href="start.html">Templates</a> </li>
							<li> <a href="start.html">Settings</a> </li>
						</ul>
					</div>
    				<!-- END DROPDOWN AJAX MENU -->
	    			<?php endif; ?>
    				<!-- BEGIN MENU TOGGLER -->
    				<button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
	                    <span class="toggle-icon">
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </span>
	                </button>
    				<!-- END MENU TOGGLER -->
    			</div>
			</div>

			<!-- BEGIN SIDEBAR -->
			<div class="page-sidebar-wrapper">
				<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
				<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
				<div class="page-sidebar navbar-collapse collapse">
					<!-- BEGIN SIDEBAR MENU -->
					<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
					<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
					<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
					<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
					<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
					<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
					<?php //echo $this->MenuBuilder->build('menu', array('class' => 'page-sidebar-menu')); ?><br />
					<?php echo $this->MenuBuilder->build('ubic', array('class' => 'page-sidebar-menu')); ?><br />
					<?php echo $this->MenuBuilder->build('festiloc', array('class' => 'page-sidebar-menu')); ?><br />
					<?php if(AuthComponent::user('role') == 'fixed'): ?>
					<?php echo $this->MenuBuilder->build('common', array('class' => 'page-sidebar-menu')); ?>
					<?php endif; ?>
					<!-- END SIDEBAR MENU -->
				</div>
			</div>
			<!-- END SIDEBAR -->

			<div class="page-fixed-main-content">
				<div id="flashMessage">
					<?php echo $this->Session->flash(); ?>
				</div>
				<div class="clearfix"></div>
				<?php echo $this->fetch('content'); ?>
			</div>
    		<!-- END PAGE CONTENT FIXED -->

    		<!-- Copyright BEGIN -->
			<p class="copyright-v2 hidden-print">2015 © MYUBIC</p>
			<!-- Copyright END -->

			<?php if(1==2): ?>
			<!-- BEGIN QUICK SIDEBAR TOGGLER -->
            <button type="button" class="quick-sidebar-toggler" data-toggle="collapse">
                <span class="sr-only">Toggle Quick Sidebar</span>
                <i class="icon-logout"></i>
                <div class="quick-sidebar-notification">
					<span class="badge badge-danger">7</span>
                </div>
            </button>
            <!-- END QUICK SIDEBAR TOGGLER -->

    		<!-- BEGIN QUICK SIDEBAR -->
			<a href="javascript:;" class="page-quick-sidebar-toggler"><i class="icon-login"></i></a>
			<div class="page-quick-sidebar-wrapper">
				<div class="page-quick-sidebar">
					<div class="nav-justified">
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a href="#quick_sidebar_tab_1" data-toggle="tab">
								Users <span class="badge badge-danger">2</span>
								</a>
							</li>
							<li>
								<a href="#quick_sidebar_tab_2" data-toggle="tab">
								Alerts <span class="badge badge-success">7</span>
								</a>
							</li>
							<li class="dropdown">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
								More<i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-bell"></i> Alerts </a>
									</li>
									<li>
										<a href="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-info"></i> Notifications </a>
									</li>
									<li>
										<a href="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-speech"></i> Activities </a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="#quick_sidebar_tab_3" data-toggle="tab">
										<i class="icon-settings"></i> Settings </a>
									</li>
								</ul>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
								<div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list">
									<h3 class="list-heading">Staff</h3>
									<ul class="media-list list-items">
										<li class="media">
											<div class="media-status">
												<span class="badge badge-success">8</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar3.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Bob Nilson</h4>
												<div class="media-heading-sub">
													 Project Manager
												</div>
											</div>
										</li>
										<li class="media">
											<img class="media-object" src="../../assets/admin/layout/img/avatar1.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Nick Larson</h4>
												<div class="media-heading-sub">
													 Art Director
												</div>
											</div>
										</li>
										<li class="media">
											<div class="media-status">
												<span class="badge badge-danger">3</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar4.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Deon Hubert</h4>
												<div class="media-heading-sub">
													 CTO
												</div>
											</div>
										</li>
										<li class="media">
											<img class="media-object" src="../../assets/admin/layout/img/avatar2.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Ella Wong</h4>
												<div class="media-heading-sub">
													 CEO
												</div>
											</div>
										</li>
									</ul>
									<h3 class="list-heading">Customers</h3>
									<ul class="media-list list-items">
										<li class="media">
											<div class="media-status">
												<span class="badge badge-warning">2</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar6.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Lara Kunis</h4>
												<div class="media-heading-sub">
													 CEO, Loop Inc
												</div>
												<div class="media-heading-small">
													 Last seen 03:10 AM
												</div>
											</div>
										</li>
										<li class="media">
											<div class="media-status">
												<span class="label label-sm label-success">new</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar7.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Ernie Kyllonen</h4>
												<div class="media-heading-sub">
													 Project Manager,<br>
													 SmartBizz PTL
												</div>
											</div>
										</li>
										<li class="media">
											<img class="media-object" src="../../assets/admin/layout/img/avatar8.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Lisa Stone</h4>
												<div class="media-heading-sub">
													 CTO, Keort Inc
												</div>
												<div class="media-heading-small">
													 Last seen 13:10 PM
												</div>
											</div>
										</li>
										<li class="media">
											<div class="media-status">
												<span class="badge badge-success">7</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar9.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Deon Portalatin</h4>
												<div class="media-heading-sub">
													 CFO, H&amp;D LTD
												</div>
											</div>
										</li>
										<li class="media">
											<img class="media-object" src="../../assets/admin/layout/img/avatar10.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Irina Savikova</h4>
												<div class="media-heading-sub">
													 CEO, Tizda Motors Inc
												</div>
											</div>
										</li>
										<li class="media">
											<div class="media-status">
												<span class="badge badge-danger">4</span>
											</div>
											<img class="media-object" src="../../assets/admin/layout/img/avatar11.jpg" alt="...">
											<div class="media-body">
												<h4 class="media-heading">Maria Gomez</h4>
												<div class="media-heading-sub">
													 Manager, Infomatic Inc
												</div>
												<div class="media-heading-small">
													 Last seen 03:10 AM
												</div>
											</div>
										</li>
									</ul>
								</div>
								<div class="page-quick-sidebar-item">
									<div class="page-quick-sidebar-chat-user">
										<div class="page-quick-sidebar-nav">
											<a href="javascript:;" class="page-quick-sidebar-back-to-list"><i class="icon-arrow-left"></i>Back</a>
										</div>
										<div class="page-quick-sidebar-chat-user-messages">
											<div class="post out">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar3.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Bob Nilson</a>
													<span class="datetime">20:15</span>
													<span class="body">
													When could you send me the report ? </span>
												</div>
											</div>
											<div class="post in">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar2.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Ella Wong</a>
													<span class="datetime">20:15</span>
													<span class="body">
													Its almost done. I will be sending it shortly </span>
												</div>
											</div>
											<div class="post out">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar3.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Bob Nilson</a>
													<span class="datetime">20:15</span>
													<span class="body">
													Alright. Thanks! :) </span>
												</div>
											</div>
											<div class="post in">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar2.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Ella Wong</a>
													<span class="datetime">20:16</span>
													<span class="body">
													You are most welcome. Sorry for the delay. </span>
												</div>
											</div>
											<div class="post out">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar3.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Bob Nilson</a>
													<span class="datetime">20:17</span>
													<span class="body">
													No probs. Just take your time :) </span>
												</div>
											</div>
											<div class="post in">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar2.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Ella Wong</a>
													<span class="datetime">20:40</span>
													<span class="body">
													Alright. I just emailed it to you. </span>
												</div>
											</div>
											<div class="post out">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar3.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Bob Nilson</a>
													<span class="datetime">20:17</span>
													<span class="body">
													Great! Thanks. Will check it right away. </span>
												</div>
											</div>
											<div class="post in">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar2.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Ella Wong</a>
													<span class="datetime">20:40</span>
													<span class="body">
													Please let me know if you have any comment. </span>
												</div>
											</div>
											<div class="post out">
												<img class="avatar" alt="" src="../../assets/admin/layout/img/avatar3.jpg"/>
												<div class="message">
													<span class="arrow"></span>
													<a href="javascript:;" class="name">Bob Nilson</a>
													<span class="datetime">20:17</span>
													<span class="body">
													Sure. I will check and buzz you if anything needs to be corrected. </span>
												</div>
											</div>
										</div>
										<div class="page-quick-sidebar-chat-user-form">
											<div class="input-group">
												<input type="text" class="form-control" placeholder="Type a message here...">
												<div class="input-group-btn">
													<button type="button" class="btn blue"><i class="icon-paper-clip"></i></button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
								<div class="page-quick-sidebar-alerts-list">
									<h3 class="list-heading">General</h3>
									<ul class="feeds list-items">
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-shopping-cart"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 New order received with <span class="label label-sm label-danger">
															Reference Number: DR23923 </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 30 mins
												</div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-success">
															<i class="fa fa-user"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 You have 5 pending membership that requires a quick review.
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 24 mins
												</div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-danger">
															<i class="fa fa-bell-o"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 Web server hardware needs to be upgraded. <span class="label label-sm label-warning">
															Overdue </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 2 hours
												</div>
											</div>
										</li>
										<li>
											<a href="javascript:;">
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-default">
															<i class="fa fa-briefcase"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 IPO Report for year 2013 has been released.
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 20 mins
												</div>
											</div>
											</a>
										</li>
									</ul>
									<h3 class="list-heading">System</h3>
									<ul class="feeds list-items">
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-shopping-cart"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 New order received with <span class="label label-sm label-success">
															Reference Number: DR23923 </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 30 mins
												</div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-success">
															<i class="fa fa-user"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 You have 5 pending membership that requires a quick review.
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 24 mins
												</div>
											</div>
										</li>
										<li>
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-warning">
															<i class="fa fa-bell-o"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 Web server hardware needs to be upgraded. <span class="label label-sm label-default ">
															Overdue </span>
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 2 hours
												</div>
											</div>
										</li>
										<li>
											<a href="javascript:;">
											<div class="col1">
												<div class="cont">
													<div class="cont-col1">
														<div class="label label-sm label-info">
															<i class="fa fa-briefcase"></i>
														</div>
													</div>
													<div class="cont-col2">
														<div class="desc">
															 IPO Report for year 2013 has been released.
														</div>
													</div>
												</div>
											</div>
											<div class="col2">
												<div class="date">
													 20 mins
												</div>
											</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
								<div class="page-quick-sidebar-settings-list">
									<h3 class="list-heading">General Settings</h3>
									<ul class="list-items borderless">
										<li>
											 Enable Notifications <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
										<li>
											 Allow Tracking <input type="checkbox" class="make-switch" data-size="small" data-on-color="info" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
										<li>
											 Log Errors <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
										<li>
											 Auto Sumbit Issues <input type="checkbox" class="make-switch" data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
										<li>
											 Enable SMS Alerts <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="success" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
									</ul>
									<h3 class="list-heading">System Settings</h3>
									<ul class="list-items borderless">
										<li>
											 Security Level
											<select class="form-control input-inline input-sm input-small">
												<option value="1">Normal</option>
												<option value="2" selected>Medium</option>
												<option value="e">High</option>
											</select>
										</li>
										<li>
											 Failed Email Attempts <input class="form-control input-inline input-sm input-small" value="5"/>
										</li>
										<li>
											 Secondary SMTP Port <input class="form-control input-inline input-sm input-small" value="3560"/>
										</li>
										<li>
											 Notify On System Error <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="danger" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
										<li>
											 Notify On SMTP Error <input type="checkbox" class="make-switch" checked data-size="small" data-on-color="warning" data-on-text="ON" data-off-color="default" data-off-text="OFF">
										</li>
									</ul>
									<div class="inner-content">
										<button class="btn btn-success"><i class="icon-settings"></i> Save Changes</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END QUICK SIDEBAR -->
			<?php endif; ?>
    	</div>
    </div>
	<!-- PAGE CONTENT END -->
    <!-- END MAIN LAYOUT -->
    <a href="#index" class="go2top"><i class="icon-arrow-up"></i></a>
	<div class="modal fade" id="note" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel"><?php echo __('Add a note'); ?></h4>
				</div>
				<div class="modal-body">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
		</div>
	</div>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/respond.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/excanvas.min.js'); ?>
	<![endif]-->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-migrate.min.js'); ?>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.blockui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.cokie.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
	<?php echo $this->Js->writeBuffer(); ?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo $this->fetch('page_level_plugins'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/scripts/metronic.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/scripts/layout.js'); ?>
	<?php //echo $this->Html->script('/metronic/scripts/quick-sidebar.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/index.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/tasks.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootbox/bootbox.min.js'); ?>
	<?php
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/select2/select2.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr-CH.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
	echo $this->Html->script('https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js');
	echo $this->Html->script('/js/yadcf/jquery.dataTables.yadcf.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
	echo $this->Html->script('/js/infinite-scroll/jquery.infinitescroll.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/dropzone/dropzone.js');
	echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/table-advanced.js');
	echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/portfolio.js');
	?>
	<?php echo $this->fetch('page_level_scripts'); ?>
	<?php
	echo $this->fetch('dataTableSettings');
	echo $this->fetch('script');
	?>
	<?php echo $this->Html->script('/metronic/custom/custom.js?tok=' . time()); ?>
	<?php //echo $this->Html->script('/metronic/custom/custom.min.js'); ?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	jQuery(document).ready(function() {
	   Metronic.init(); // init metronic core componets
	   Layout.init(); // init layout
	   //QuickSidebar.init() // init quick sidebar
	   Custom.prepare();
	   Custom.init();
	   <?php echo $this->fetch('init_scripts'); ?>
	});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
</html>
