<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 4.5.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

		<head>
			<?php echo $this->Html->charset(); ?>
			<title>
				<?php echo __($title_for_layout); ?>
			</title>
			<?php echo $this->fetch('meta'); ?>
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta content="width=device-width, initial-scale=1" name="viewport"/>

			<!-- BEGIN GLOBAL MANDATORY STYLES -->
			<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
			<!-- END GLOBAL MANDATORY STYLES -->
			<!-- BEGIN PAGE LEVEL STYLES -->
			<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
			<?php
			echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/css/select2.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/css/select2-bootstrap.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/dropzone/dropzone.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/fancybox/source/jquery.fancybox.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/datatables.min.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/pace/themes/pace-theme-big-counter.css');
			echo $this->Html->css('/metronic/theme/assets/global/plugins/fullcalendar/fullcalendar.min.css');
			?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/css/components.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/global/css/plugins.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/blog.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/search.min.css'); ?>
			<?php echo $this->fetch('page_level_styles'); ?>
			<!-- END PAGE LEVEL STYLES -->
			<!-- BEGIN THEME STYLES -->
			<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/layout.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/themes/default.min.css'); ?>
			<?php echo $this->Html->css('/metronic/theme/assets/pages/css/invoice.css'); ?>
			<?php
			echo $this->Html->css('/metronic/theme/assets/pages/css/portfolio.css');
			echo $this->Html->css('/metronic/theme/assets/pages/css/profile.css');
			?>
			<?php echo $this->Html->css('/metronic/custom/3d-corner-ribbons.css'); ?>
			<?php echo $this->Html->css('/metronic/custom/_all.css'); ?>
			<?php echo $this->Html->css('/flags-css/css/flag-icon.min.css'); ?>
			<!-- END THEME STYLES -->
			<?php echo $this->fetch('css'); ?>
			<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
			<link href='https://fonts.googleapis.com/css?family=Kreon:400,700,300' rel='stylesheet' type='text/css'>
		</head>

		<body class="festiloc festiloc-depot">
			<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>
			<!-- BEGIN CONTAINER -->
			<div class="page-container">
				<!-- BEGIN CONTENT -->
				<div class="page-content-wrapper">
					<div class="page-content">
						<!-- BEGIN PAGE CONTENT-->
						<div class="row">
							<div class="col-md-12">
								<?php echo $this->fetch('content'); ?>
							</div>
						</div>
						<!-- END PAGE CONTENT-->
					</div>
				</div>
			</div>


      <!-- BEGIN CORE PLUGINS -->
			<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.min.js'); ?>
			<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
			<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-migrate.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.cokie.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.blockui.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
      <?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
			<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootbox/bootbox.min.js'); ?>
			<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
      <!-- END CORE PLUGINS -->
			<?php echo $this->Js->writeBuffer(); ?>
			<!-- END CORE PLUGINS -->
			<!-- BEGIN PAGE LEVEL PLUGINS -->
			<?php echo $this->fetch('page_level_plugins'); ?>
      <!-- BEGIN THEME GLOBAL SCRIPTS -->
      <?php echo $this->Html->script('/metronic/theme/assets/global/scripts/app.min.js'); ?>
      <!-- END THEME GLOBAL SCRIPTS -->
      <!-- BEGIN THEME LAYOUT SCRIPTS -->
			<?php echo $this->Html->script('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/scripts/layout.min.js'); ?>
			<?php
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/select2/js/select2.full.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr-CH.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js');
			echo $this->Html->script('https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js');
			echo $this->Html->script('/js/yadcf/jquery.dataTables.yadcf.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
			echo $this->Html->script('/js/infinite-scroll/jquery.infinitescroll.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/dropzone/dropzone.min.js');
			echo $this->Html->script('/metronic/theme/assets/pages/scripts/portfolio.min.js');

			echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/lib/moment.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/fullcalendar.min.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/fullcalendar/locale/fr.js');
			echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js');
			echo $this->Html->script('//cdn.ckeditor.com/4.6.0/basic/ckeditor.js');
			?>
			<?php echo $this->fetch('page_level_scripts'); ?>
			<?php
			echo $this->fetch('dataTableSettings');
			echo $this->fetch('script');
			?>
			<?php echo $this->Html->script('/metronic/custom/custom.js?tok=' . time()); ?>
			<?php //echo $this->Html->script('/metronic/custom/custom.min.js'); ?>
			<!-- END PAGE LEVEL SCRIPTS -->
			<script>
			jQuery(document).ready(function() {
			   Custom.prepare();
			   Custom.init();
			   <?php echo $this->fetch('init_scripts'); ?>
			});
			</script>
      <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
