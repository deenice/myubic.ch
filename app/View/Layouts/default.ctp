<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/components.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/plugins.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/css/layout.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/css/themes/blue.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/css/3d-corner-ribbons.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/pages/css/invoice.css'); ?>
	<?php echo $this->Html->css('/metronic/css/custom.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<?php
/*if(!is_null(AuthComponent::user('id'))){
	$logged = true;
	$bodyClasses = 'page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed page-sidebar-closed-hide-logo';
} else {
	$logged = false;
	$bodyClasses = 'login asd';
}*/
$authComponentDocument = AuthComponent::user('Portrait');
?>
<body class="page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed">
<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x"></i></div></div>
<?php if(/*!$logged &&*/ 1==2):?>
<div class="logo">
	<a href="index.html">
	<img src="<?php echo $this->webroot; ?>metronic/img/logo-big.png" alt=""/>
	</a>
</div>
<div class="content">
	<div id="flashMessage">
		<?php echo $this->Session->flash(); ?>
	</div>
	<?php echo $this->fetch('content'); ?>
</div>
<?php else: ?>
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top metronic<?php echo Configure::read('Metronic.version'); ?>">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.html" style="position: relative">
				<img src="<?php echo $this->webroot; ?>metronic/img/logo_my_ubic.png" width="100px" alt="logo" class="logo-default"/>
				<span class="badge badge-default badge-roundless" style="position:absolute; right:-20px; top:15px;-ms-transform: rotate(30deg);-webkit-transform: rotate(30deg);transform: rotate(30deg);">BÊTA</span>
				</a>
				<div class="menu-toggler sidebar-toggler">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN PAGE ACTIONS -->
			<!-- DOC: Remove "hide" class to enable the page header actions -->
			<!--div class="page-actions">
				<div class="btn-group hide">
					<button type="button" class="btn btn-circle red-pink dropdown-toggle" data-toggle="dropdown">
					<i class="icon-bar-chart"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="#">
							<i class="icon-user"></i> New User </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-present"></i> New Event <span class="badge badge-success">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-basket"></i> New order </a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="#">
							<i class="icon-flag"></i> Pending Orders <span class="badge badge-danger">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-users"></i> Pending Users <span class="badge badge-warning">12</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-circle green-haze dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-plus"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="#">
							<i class="icon-docs"></i> New Post </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-tag"></i> New Comment </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-share"></i> Share </a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="#">
							<i class="icon-flag"></i> Comments <span class="badge badge-success">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-users"></i> Feedbacks <span class="badge badge-danger">2</span>
							</a>
						</li>
					</ul>
				</div>
			</div-->
			<!-- END PAGE ACTIONS -->
			<!-- BEGIN PAGE TOP -->
			<div class="page-top">
				<!-- BEGIN HEADER SEARCH BOX -->
				<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
				<!--form class="search-form search-form-expanded" action="extra_search.html" method="GET">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search..." name="query">
						<span class="input-group-btn">
						<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
						</span>
					</div>
				</form-->
				<!-- END HEADER SEARCH BOX -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<?php if(1==2): ?>
						<!-- BEGIN NOTIFICATION DROPDOWN -->
						<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="icon-bell"></i>
							<span class="badge badge-danger">
							7 </span>
							</a>
							<ul class="dropdown-menu">
								<li>
									<p>
										 You have 14 new notifications
									</p>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px;">
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-success">
											<i class="fa fa-plus"></i>
											</span>
											New user registered. <span class="time">
											Just now </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-danger">
											<i class="fa fa-bolt"></i>
											</span>
											Server #12 overloaded. <span class="time">
											15 mins </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-warning">
											<i class="fa fa-bell-o"></i>
											</span>
											Server #2 not responding. <span class="time">
											22 mins </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-info">
											<i class="fa fa-bullhorn"></i>
											</span>
											Application error. <span class="time">
											40 mins </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-danger">
											<i class="fa fa-bolt"></i>
											</span>
											Database overloaded 68%. <span class="time">
											2 hrs </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-danger">
											<i class="fa fa-bolt"></i>
											</span>
											2 user IP blocked. <span class="time">
											5 hrs </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-warning">
											<i class="fa fa-bell-o"></i>
											</span>
											Storage Server #4 not responding. <span class="time">
											45 mins </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-info">
											<i class="fa fa-bullhorn"></i>
											</span>
											System Error. <span class="time">
											55 mins </span>
											</a>
										</li>
										<li>
											<a href="#">
											<span class="label label-sm label-icon label-danger">
											<i class="fa fa-bolt"></i>
											</span>
											Database overloaded 68%. <span class="time">
											2 hrs </span>
											</a>
										</li>
									</ul>
								</li>
								<li class="external">
									<a href="#">
									See all notifications <i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
						<!-- END NOTIFICATION DROPDOWN -->
						<!-- BEGIN INBOX DROPDOWN -->
						<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="icon-envelope-open"></i>
							<span class="badge badge-primary">
							4 </span>
							</a>
							<ul class="dropdown-menu">
								<li>
									<p>
										 You have 12 new messages
									</p>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px;">
										<li>
											<a href="inbox.html?a=view">
											<span class="photo">
											<img src="<?php echo $this->webroot; ?>metronic/img/avatar2.jpg" alt=""/>
											</span>
											<span class="subject">
											<span class="from">
											Lisa Wong </span>
											<span class="time">
											Just Now </span>
											</span>
											<span class="message">
											Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
											</a>
										</li>
										<li>
											<a href="inbox.html?a=view">
											<span class="photo">
											<img src="<?php echo $this->webroot; ?>metronic/img/avatar3.jpg" alt=""/>
											</span>
											<span class="subject">
											<span class="from">
											Richard Doe </span>
											<span class="time">
											16 mins </span>
											</span>
											<span class="message">
											Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
											</a>
										</li>
										<li>
											<a href="inbox.html?a=view">
											<span class="photo">
											<img src="<?php echo $this->webroot; ?>metronic/img/avatar1.jpg" alt=""/>
											</span>
											<span class="subject">
											<span class="from">
											Bob Nilson </span>
											<span class="time">
											2 hrs </span>
											</span>
											<span class="message">
											Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
											</a>
										</li>
										<li>
											<a href="inbox.html?a=view">
											<span class="photo">
											<img src="<?php echo $this->webroot; ?>metronic/img/avatar2.jpg" alt=""/>
											</span>
											<span class="subject">
											<span class="from">
											Lisa Wong </span>
											<span class="time">
											40 mins </span>
											</span>
											<span class="message">
											Vivamus sed auctor 40% nibh congue nibh... </span>
											</a>
										</li>
										<li>
											<a href="inbox.html?a=view">
											<span class="photo">
											<img src="<?php echo $this->webroot; ?>metronic/img/avatar3.jpg" alt=""/>
											</span>
											<span class="subject">
											<span class="from">
											Richard Doe </span>
											<span class="time">
											46 mins </span>
											</span>
											<span class="message">
											Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
											</a>
										</li>
									</ul>
								</li>
								<li class="external">
									<a href="inbox.html">
									See all messages <i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
						<!-- END INBOX DROPDOWN -->
						<!-- BEGIN TODO DROPDOWN -->
						<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<i class="icon-calendar"></i>
							<span class="badge badge-success">
							3 </span>
							</a>
							<ul class="dropdown-menu extended tasks">
								<li>
									<p>
										 You have 12 pending tasks
									</p>
								</li>
								<li>
									<ul class="dropdown-menu-list scroller" style="height: 250px;">
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											New release v1.2 </span>
											<span class="percent">
											30% </span>
											</span>
											<div class="progress">
												<div style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 40% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											Application deployment </span>
											<span class="percent">
											65% </span>
											</span>
											<div class="progress progress-striped">
												<div style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 65% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											Mobile app release </span>
											<span class="percent">
											98% </span>
											</span>
											<div class="progress">
												<div style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 98% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											Database migration </span>
											<span class="percent">
											10% </span>
											</span>
											<div class="progress progress-striped">
												<div style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 10% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											Web server upgrade </span>
											<span class="percent">
											58% </span>
											</span>
											<div class="progress progress-striped">
												<div style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 58% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											Mobile development </span>
											<span class="percent">
											85% </span>
											</span>
											<div class="progress progress-striped">
												<div style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 85% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
										<li>
											<a href="page_todo.html">
											<span class="task">
											<span class="desc">
											New UI release </span>
											<span class="percent">
											18% </span>
											</span>
											<div class="progress progress-striped">
												<div style="width: 18%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
													<div class="sr-only">
														 18% Complete
													</div>
												</div>
											</div>
											</a>
										</li>
									</ul>
								</li>
								<li class="external">
									<a href="page_todo.html">
									See all tasks <i class="icon-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>
						<!-- END TODO DROPDOWN -->
						<!-- BEGIN QUICK SIDEBAR TOGGLER -->
						<!-- END QUICK SIDEBAR TOGGLER -->
						<?php endif; ?>
						<!-- BEGIN USER LOGIN DROPDOWN -->
						<li class="dropdown dropdown-user">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<?php if(!empty($authComponentDocument[0]['url'])): ?>
							<?php echo $this->Html->image(
								$this->ImageSize->crop($authComponentDocument[0]['url']),
								array('class' => 'img-circle hide1')
							); ?>
							<?php else: ?>
							<?php echo $this->Html->image('icons/user.png', array('class' => 'img-circle hide1')); ?>
							<?php endif; ?>
							<span class="username username-hide-on-mobile">
							<?php echo AuthComponent::user('full_name'); ?> </span>
							<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu">
								<?php if($this->User->hasRights(array('controller' => 'users', 'action' => 'view'))): ?>
								<li>
									<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'users', 'action' => 'view', AuthComponent::user('id')), array('escape' => false)); ?>
								</li>
								<?php else: ?>
								<li>
									<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'me', 'action' => 'profile'), array('escape' => false)); ?>
								</li>
								<?php endif; ?>
								<li>
									<?php echo $this->Html->link('<i class="fa fa-recycle"></i> ' . __('Clear cache'), array('controller' => 'cache', 'action' => 'clear'), array('escape' => false)); ?>
								</li>
								<!--li>
									<a href="page_calendar.html">
									<i class="icon-calendar"></i> My Calendar </a>
								</li>
								<li>
									<a href="inbox.html">
									<i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
									3 </span>
									</a>
								</li>
								<li>
									<a href="page_todo.html">
									<i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
									7 </span>
									</a>
								</li-->
								<li class="divider">
								</li>
								<!-- <li>
									<?php echo $this->Html->link('<i class="icon-lock"></i> ' . __('Lock screen'), array('controller' => 'users', 'action' => 'lock', AuthComponent::user('id')), array('escape' => false)); ?>
								</li> -->
								<li>
									<?php echo $this->Html->link('<i class="icon-logout"></i> ' . __('Logout'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?>
								</li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
				</div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END PAGE TOP -->
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<div class="clearfix">
	</div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->

				<?php echo $this->MenuBuilder->build('sidebar-menu', array('class' => 'page-sidebar-menu')); ?>
				<?php if(12==1): ?>
				<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
					<li class="start ">
						<?php echo $this->Html->link('<i class="icon-grid"></i><span class="title">Dashboard</span>', array('controller' => 'dashboard', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					<li class="<?php echo $this->params['controller'] == 'search' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-magnifier"></i>
						<span class="title"><?php echo __('Search'); ?></span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('Users'), array('controller' => 'search', 'action' => 'users')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Places'), array('controller' => 'search', 'action' => 'places')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('History'), array('controller' => 'search', 'action' => 'history')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'users' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-user"></i>
						<span class="title"><?php echo __('Human resources'); ?></span>
						<span class="arrow "></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all collaborators'), array('controller' => 'users', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add a collaborator'), array('controller' => 'users', 'action' => 'add')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Availabilities'), array('controller' => 'users', 'action' => 'availabilities')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Modules'), array('controller' => 'modules', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Search'), array('controller' => 'search', 'action' => 'users')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'places' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-pointer"></i>
						<span class="title"><?php echo __('Places'); ?></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all places'), array('controller' => 'places', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add a place'), array('controller' => 'places', 'action' => 'add')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add a commune'), array('controller' => 'communes', 'action' => 'add')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Status of the options'), array('controller' => 'places', 'action' => 'options')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Search'), array('controller' => 'search', 'action' => 'places')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'activities' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-trophy"></i>
						<span class="title"><?php echo __('Activities'); ?></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all activities'), array('controller' => 'activities', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add an activity'), array('controller' => 'activities', 'action' => 'add')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'events' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-calendar"></i>
						<span class="title"><?php echo __('Events'); ?></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all events'), array('controller' => 'events', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add an event'), array('controller' => 'events', 'action' => 'add')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'clients' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-wallet"></i>
						<span class="title"><?php echo __('Clients'); ?></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all clients'), array('controller' => 'clients', 'action' => 'index')); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Add a client'), array('controller' => 'clients', 'action' => 'add')); ?>
							</li>
						</ul>
					</li>
					<li class="<?php echo $this->params['controller'] == 'stock_items' ? 'active open':'' ?>">
						<a href="javascript:;">
						<i class="icon-basket-loaded"></i>
						<span class="title"><?php echo __('Festiloc'); ?></span>
						<span class="arrow"></span>
						</a>
						<ul class="sub-menu">
							<li>
								<?php echo $this->Html->link(__('See all stock items'), array('controller' => 'stock_items', 'action' => 'index'), array('escape' => false)); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('Price list'), array('controller' => 'stock_items', 'action' => 'pricelist'), array('escape' => false)); ?>
							</li>
							<li>
								<?php echo $this->Html->link(__('See all stock orders'), array('controller' => 'stock_orders', 'action' => 'index'), array('escape' => false)); ?>
							</li>
						</ul>
					</li>
				</ul>
				<?php endif; ?>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Modal title</h4>
							</div>
							<div class="modal-body">
								 Widget settings form goes here
							</div>
							<div class="modal-footer">
								<button type="button" class="btn blue">Save changes</button>
								<button type="button" class="btn default" data-dismiss="modal">Close</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<?php echo $this->fetch('page_title'); ?>
				</h3>
				<!--div class="page-bar">
					<ul class="page-breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Page Layouts</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Blank Page</a>
						</li>
					</ul>
					<div class="page-toolbar">
						<div class="btn-group pull-right">
							<button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							Actions <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</div>
					</div>
				</div-->
				<!-- END PAGE HEADER-->
				<!-- BEGIN PAGE CONTENT-->
				<div class="row" id="flashMessage">
				<?php echo $this->Session->flash(); ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">
			 <?php echo date('Y'); ?> &copy; MYUBIC.CH
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END FOOTER -->
<?php endif; ?>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/respond.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/excanvas.min.js'); ?>
	<![endif]-->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-migrate.min.js'); ?>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.blockui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.cokie.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
	<?php echo $this->Js->writeBuffer(); ?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo $this->fetch('page_level_plugins'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/scripts/metronic.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/scripts/layout.js'); ?>
	<?php //echo $this->Html->script('/metronic/scripts/quick-sidebar.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/index.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/tasks.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootbox/bootbox.min.js'); ?>
	<?php echo $this->fetch('page_level_scripts'); ?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	jQuery(document).ready(function() {
	   Metronic.init(); // init metronic core componets
	   //Layout.init(); // init layout
	   Custom.prepare();
	   //QuickSidebar.init() // init quick sidebar
	   <?php echo $this->fetch('init_scripts'); ?>
	});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
</html>
