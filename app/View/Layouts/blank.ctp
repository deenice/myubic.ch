<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
	<?php
	echo $this->Html->css('/metronic/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/fancybox/source/jquery.fancybox.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
	?>
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/layout.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/layouts/layout'.Configure::read('Metronic.version').'/css/themes/default.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/pages/css/invoice.css'); ?>
	<?php echo $this->Html->css('/metronic/custom/3d-corner-ribbons.css'); ?>
	<?php //echo $this->Html->css('/metronic/custom/custom.min.css'); ?>
	<?php echo $this->Html->css('/metronic/custom/_all.css'); ?>
	<?php echo $this->Html->css('/flags-css/css/flag-icon.min.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
</head>
<body>
	<div class="container">
		<?php echo $this->fetch('content'); ?>
	</div>
</body>
</html>
