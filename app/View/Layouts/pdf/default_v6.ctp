<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/plugins/font-awesome/css/font-awesome.min.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/plugins/bootstrap/css/bootstrap.min.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/plugins/uniform/css/uniform.default.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/css/components.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/global/css/plugins.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/css/layout.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/css/themes/blue.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/pages/css/invoice.css', true)); ?>
<?php echo $this->Html->css($this->Html->url('/metronic/css/custom.css', true)); ?>
<?php echo $this->fetch('content'); ?>