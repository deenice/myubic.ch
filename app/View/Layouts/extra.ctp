<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->Html->css('/metronic/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/global/css/components.css'); ?>
	<?php echo $this->Html->css('/metronic/global/css/plugins.css'); ?>
	<?php echo $this->Html->css('/metronic/css/layout.css'); ?>
	<?php echo $this->Html->css('/metronic/css/themes/blue.css'); ?>
	<?php echo $this->Html->css('/metronic/css/custom.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<?php
if(AuthComponent::user('id')){
	$logged = true;
	$bodyClasses = 'page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo page-sidebar-fixed page-sidebar-closed-hide-logo';
} else {
	$logged = false;
	$bodyClasses = 'login';
}
$authComponentDocument = AuthComponent::user('Document');
?>
<body class="<?php echo $bodyClasses; ?>">
<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x"></i></div></div>
<?php if(!$logged):?>
<div class="logo">
	<a href="index.html">
	<img src="<?php echo $this->webroot; ?>metronic/img/logo-big.png" alt=""/>
	</a>
</div>
<div class="content">
	<div id="flashMessage">
		<?php echo $this->Session->flash(); ?>
	</div>
	<?php echo $this->fetch('content'); ?>
</div>
<?php else: ?>
	<!-- BEGIN HEADER -->
	<div class="page-header navbar navbar-fixed-top">
		<!-- BEGIN HEADER INNER -->
		<div class="page-header-inner">
			<!-- BEGIN LOGO -->
			<div class="page-logo">
				<a href="index.html" style="position: relative">
				<img src="<?php echo $this->webroot; ?>metronic/img/logo_my_ubic.png" width="100px" alt="logo" class="logo-default"/>
				<span class="badge badge-default badge-roundless" style="position:absolute; right:-20px; top:15px;-ms-transform: rotate(30deg);-webkit-transform: rotate(30deg);transform: rotate(30deg);">BÊTA</span>
				</a>
				<div class="menu-toggler sidebar-toggler">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
			<!-- END LOGO -->
			<!-- BEGIN RESPONSIVE MENU TOGGLER -->
			<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
			</a>
			<!-- END RESPONSIVE MENU TOGGLER -->
			<!-- BEGIN PAGE ACTIONS -->
			<!-- DOC: Remove "hide" class to enable the page header actions -->
			<!--div class="page-actions">
				<div class="btn-group hide">
					<button type="button" class="btn btn-circle red-pink dropdown-toggle" data-toggle="dropdown">
					<i class="icon-bar-chart"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="#">
							<i class="icon-user"></i> New User </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-present"></i> New Event <span class="badge badge-success">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-basket"></i> New order </a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="#">
							<i class="icon-flag"></i> Pending Orders <span class="badge badge-danger">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-users"></i> Pending Users <span class="badge badge-warning">12</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="btn-group">
					<button type="button" class="btn btn-circle green-haze dropdown-toggle" data-toggle="dropdown">
					<i class="fa fa-plus"></i>&nbsp;<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="#">
							<i class="icon-docs"></i> New Post </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-tag"></i> New Comment </a>
						</li>
						<li>
							<a href="#">
							<i class="icon-share"></i> Share </a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="#">
							<i class="icon-flag"></i> Comments <span class="badge badge-success">4</span>
							</a>
						</li>
						<li>
							<a href="#">
							<i class="icon-users"></i> Feedbacks <span class="badge badge-danger">2</span>
							</a>
						</li>
					</ul>
				</div>
			</div-->
			<!-- END PAGE ACTIONS -->
			<!-- BEGIN PAGE TOP -->
			<div class="page-top">
				<!-- BEGIN HEADER SEARCH BOX -->
				<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
				<!--form class="search-form search-form-expanded" action="extra_search.html" method="GET">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search..." name="query">
						<span class="input-group-btn">
						<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
						</span>
					</div>
				</form-->
				<!-- END HEADER SEARCH BOX -->
				<!-- BEGIN TOP NAVIGATION MENU -->
				<div class="top-menu">
					<ul class="nav navbar-nav pull-right">
						<li class="dropdown dropdown-user">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<?php if(!empty($authComponentDocument[0]['url'])): ?>
							<?php echo $this->Html->image(
								$this->ImageSize->crop($authComponentDocument[0]['url']),
								array('class' => 'img-circle hide1')
							); ?>
							<?php else: ?>
							<?php echo $this->Html->image('icons/user.png', array('class' => 'img-circle hide1')); ?>
							<?php endif; ?>
							<span class="username username-hide-on-mobile">
							<?php echo AuthComponent::user('first_name'); ?> </span>
							<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li>
									<?php echo $this->Html->link('<i class="icon-user"></i> ' . __('My profile'), array('controller' => 'users', 'action' => 'view', AuthComponent::user('id')), array('escape' => false)); ?>
								</li>
								<!--li>
									<a href="page_calendar.html">
									<i class="icon-calendar"></i> My Calendar </a>
								</li>
								<li>
									<a href="inbox.html">
									<i class="icon-envelope-open"></i> My Inbox <span class="badge badge-danger">
									3 </span>
									</a>
								</li>
								<li>
									<a href="page_todo.html">
									<i class="icon-rocket"></i> My Tasks <span class="badge badge-success">
									7 </span>
									</a>
								</li-->
								<li class="divider">
								</li>
								<li>
									<?php echo $this->Html->link('<i class="icon-lock"></i> ' . __('Lock screen'), array('controller' => 'users', 'action' => 'lock', AuthComponent::user('id')), array('escape' => false)); ?>
								</li>
								<li>
									<?php echo $this->Html->link('<i class="icon-logout"></i> ' . __('Logout'), array('controller' => 'users', 'action' => 'logout'), array('escape' => false)); ?>
								</li>
							</ul>
						</li>
						<!-- END USER LOGIN DROPDOWN -->
					</ul>
				</div>
				<!-- END TOP NAVIGATION MENU -->
			</div>
			<!-- END PAGE TOP -->
		</div>
		<!-- END HEADER INNER -->
	</div>
	<!-- END HEADER -->
	<div class="clearfix">
	</div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<div class="page-sidebar navbar-collapse collapse">
				<!-- BEGIN SIDEBAR MENU -->
				<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
					<li class="start ">
						<?php echo $this->Html->link('<i class="icon-grid"></i><span class="title">Dashboard</span>', array('controller' => 'dashboard', 'action' => 'index'), array('escape' => false)); ?>
					</li>
					<li>
						<?php echo $this->Html->link('<i class="icon-user"></i><span class="title">Profil</span>', array('controller' => 'users', 'action' => 'view', AuthComponent::user('id')), array('escape' => false)); ?>
					</li>	
					<li>
						<?php echo $this->Html->link('<i class="icon-calendar"></i><span class="title">Events</span>', array('controller' => 'events', 'action' => 'index'), array('escape' => false)); ?>
					</li>					
				</ul>
				<!-- END SIDEBAR MENU -->
			</div>
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
				<?php echo $this->fetch('page_title'); ?>
				</h3>
				<!-- BEGIN PAGE CONTENT-->
				<div class="row" id="flashMessage">
				<?php echo $this->Session->flash(); ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN FOOTER -->
	<div class="page-footer">
		<div class="page-footer-inner">
			 2014 &copy; Metronic by keenthemes.
		</div>
		<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
		</div>
	</div>
	<!-- END FOOTER -->
<?php endif; ?>
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('/metronic/global/plugins/respond.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/excanvas.min.js'); ?>
	<![endif]-->
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-migrate.min.js'); ?>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.blockui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/jquery.cokie.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/uniform/jquery.uniform.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
	<?php echo $this->Js->writeBuffer(); ?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo $this->fetch('page_level_plugins'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php echo $this->Html->script('/metronic/global/scripts/metronic.js'); ?>
	<?php echo $this->Html->script('/metronic/scripts/layout.js'); ?>
	<?php //echo $this->Html->script('/metronic/scripts/quick-sidebar.js'); ?>
	<?php echo $this->Html->script('/metronic/pages/scripts/index.js'); ?>
	<?php echo $this->Html->script('/metronic/pages/scripts/tasks.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
	<?php echo $this->Html->script('/metronic/global/plugins/bootbox/bootbox.min.js'); ?>
	<?php echo $this->fetch('page_level_scripts'); ?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	jQuery(document).ready(function() {    
	   Metronic.init(); // init metronic core componets
	   Layout.init(); // init layout
	   Custom.prepare();
	   //QuickSidebar.init() // init quick sidebar
	   <?php echo $this->fetch('init_scripts'); ?>
	});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
</html>
