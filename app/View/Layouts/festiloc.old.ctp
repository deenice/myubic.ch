<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?>
	</title>
	<?php echo $this->fetch('meta'); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<?php echo $this->Html->css('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/font-awesome/css/font-awesome.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/simple-line-icons/simple-line-icons.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap/css/bootstrap.min.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/uniform/css/uniform.default.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.css'); ?>
	<?php
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/select2/select2.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/jquery-multi-select/css/multi-select.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/dropzone/css/dropzone.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/fancybox/source/jquery.fancybox.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css');
	echo $this->Html->css('/metronic/theme/assets/global/plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.css');
	?>
	<?php echo $this->fetch('page_level_styles'); ?>
	<!-- END PAGE LEVEL STYLES -->
	<!-- BEGIN THEME STYLES -->
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/components.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/global/css/plugins.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/css/layout.css'); ?>
	<?php echo $this->Html->css('/metronic/theme/assets/admin/pages/css/invoice.css'); ?>
	<?php
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/portfolio.css');
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/profile-old.css');
	echo $this->Html->css('/metronic/theme/assets/admin/pages/css/profile.css');
	?>
	<?php echo $this->Html->css('/metronic/custom/3d-corner-ribbons.css'); ?>
	<?php //echo $this->Html->css('/metronic/custom/custom.min.css'); ?>
	<?php echo $this->Html->css('/metronic/custom/custom.css'); ?>
	<?php echo $this->Html->css('/flags-css/css/flag-icon.min.css'); ?>
	<!-- END THEME STYLES -->
	<?php echo $this->fetch('css'); ?>
	<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
</head>
<body class="festiloc festiloc-depot">
	<div id="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN PAGE CONTENT-->
				<div class="row">
					<div class="col-md-12">
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
	</div>
	<!-- END CONTAINER -->
	<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
	<!-- BEGIN CORE PLUGINS -->
	<!--[if lt IE 9]>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/respond.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/excanvas.min.js'); ?>
	<![endif]-->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-migrate.min.js'); ?>
	<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.blockui.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery.cokie.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
	<?php echo $this->Js->writeBuffer(); ?>
	<!-- END CORE PLUGINS -->
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<?php echo $this->fetch('page_level_plugins'); ?>
	<!-- END PAGE LEVEL PLUGINS -->
	<!-- BEGIN PAGE LEVEL SCRIPTS -->
	<?php echo $this->Html->script('/metronic/theme/assets/global/scripts/metronic.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/layout'.Configure::read('Metronic.version').'/scripts/layout.js'); ?>
	<?php //echo $this->Html->script('/metronic/scripts/quick-sidebar.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/index.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/tasks.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-toastr/toastr.min.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js'); ?>
	<?php echo $this->Html->script('/metronic/theme/assets/global/plugins/bootbox/bootbox.min.js'); ?>
	<?php
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-select/bootstrap-select.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/select2/select2.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr-CH.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js');
	echo $this->Html->script('https://cdn.datatables.net/1.10.6/js/jquery.dataTables.min.js');
	echo $this->Html->script('/js/yadcf/jquery.dataTables.yadcf.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
	echo $this->Html->script('/js/infinite-scroll/jquery.infinitescroll.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
	echo $this->Html->script('/metronic/theme/assets/global/plugins/dropzone/dropzone.js');
	echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/table-advanced.js');
	echo $this->Html->script('/metronic/theme/assets/admin/pages/scripts/portfolio.js');
	?>
	<?php echo $this->fetch('page_level_scripts'); ?>
	<?php
	echo $this->fetch('dataTableSettings');
	echo $this->fetch('script');
	?>
	<?php echo $this->Html->script('/metronic/custom/custom.js'); ?>
	<!-- END PAGE LEVEL SCRIPTS -->
	<script>
	jQuery(document).ready(function() {
	   Metronic.init(); // init metronic core componets
	   Layout.init(); // init layout
	   //QuickSidebar.init() // init quick sidebar
	   Custom.prepare();
	   Custom.init();
	   <?php echo $this->fetch('init_scripts'); ?>
	});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
</html>
