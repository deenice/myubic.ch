<?php if(!empty($users)): ?>
<div class="row">
  <div class="col-md-12">
    <a href="#" data-list="users" class="btn btn-primary btn-lg sync pull-right"><i class="fa fa-exchange"></i> <?php echo __('Convert'); ?></a>
  </div>
  <div class="col-md-12" style="margin-top: 10px">
    <div class="progress progress-striped active">
      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"> 40% Complete (success) </span>
      </div>
    </div>
  </div>
</div>
<table class="table table-striped table-bordered" id="users">
  <thead>
    <tr>
      <th>#</th>
      <th><?php echo __('Collaborator'); ?></th>
      <th><?php echo __('Status'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($users as $k => $user): ?>
      <tr data-item-id="<?php echo $user['User']['id']; ?>">
        <td><?php echo ++$k; ?></td>
        <td>
          <?php echo $user['User']['full_name']; ?>
        </td>
        <td><i class="fa fa-minus"></i></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif;?>

<?php $this->start('init_scripts'); ?>

  $(document).ready(function(){
    var basePath = "";
    if(document.location.hostname == 'localhost' || document.location.hostname == '192.168.1.126'){
        basePath = '/myubic';
    }
    var sync = function( list, id ){

      var row = $('#'+list+' tbody tr:eq('+id+')');
      var itemId = row.data('item-id');
      var max = $('#'+list+' tbody tr').get().length;
      row.find('td:last').html('<i class="fa fa-spin fa-spinner"></i>');

      $.ajax({
        url: basePath + '/schedule_days/convert/',
        dataType: 'json',
        type: 'post',
        data: {
          itemId: itemId
        },
        success: function(data){
          console.log(data);
          if(data){
            if(data.success == 1){
              row.find('td:last').html('<i class="fa fa-check font-green"></i>');
            } else if(data.success == 0){
              row.find('td:last').html('<i class="fa fa-times font-red"></i>');
            }
          }
        }
      }).then(function(){
        id++;
        var percent = id / max * 100;
        $('.progress-bar').css('width', percent + '%');
        if(id < max){
          sync( list, id );
        } else {
          return;
        }
      });

    }
    $('body').on('click', '.sync', function(e){
      e.preventDefault();
      var list = $(this).data('list');
      sync(list, 0);
    });
  });

<?php $this->end(); ?>
