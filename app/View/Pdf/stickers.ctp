<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	* {
		margin: 0;
		padding: 0;
	}
	body {
		font-weight: 300;
		font-family: 'Helvetica', sans-serif;
	}
	p {
		margin: 0;
		padding: 0;
	}
	.items {
		width: 100%;
		margin: 0;
		padding: 0;
	}
	.items, .items tr, .items td {
		page-break-inside: avoid !important;
	}
	.wrapper {
		position: relative;
		width: 100%;
		height: 80px;
		margin-bottom: 20px;
		clear: both;
	}
	.wrapper div {
		float: left;
	}
	.wrapper .logo {
		width: 30px;
	}
	.wrapper .logo img{
		height: 60px;
	}
	.wrapper .image {
		position: relative;
		width: 60px;
		height: 60px;
		margin-right: 10px;
	}
	.wrapper .image img {
		position: absolute;
		max-height: 100%;
    max-width: 100%;
    width: auto;
    height: auto;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
	}
	.wrapper .infos {
		width: 500px;
		line-height: 14px;
	}
	.wrapper .infos .code {
		display: inline-block;;
		font-size: 30px;
		line-height: 30px;
	}
	.wrapper .infos .name {
		display: inline-block;;
		font-size: 14px;
	}
	.wrapper .infos .stock {
		display: inline-block;;
		font-size: 14px;
	}
	.wrapper .qrcode {
		float: right;
	}
	.wrapper .qrcode .qrcode-inner {
		position: relative;
	}
	.wrapper .qrcode .qrcode-inner img {
		position: absolute;
		top: -11px;
		right: 0;
	}
	.items tr:last-child .wrapper {
		border-bottom: 0;
		margin-bottom: 0;
	}
	.break{
	   display: block;
	   clear: both;
	   page-break-after: always;
	}
</style>
<?php foreach($stockitems as $k => $item): $i = $k+1;?>
<?php if($i%10==1): ?><table class="items"><?php endif; ?>
	<tr>
		<td>
			<div class="wrapper">
				<div class="logo"><img src="<?php echo WWW_ROOT . 'img' . DS . 'festiloc_sticker.jpg'; ?>" alt="" /></div>
				<div class="image">
					<div class="image-inner">
						<?php $imageUrl = ''; ?>
						<?php if(!empty($item['Document'])): ?>
						<?php foreach($item['Document'] as $doc){
							if(strpos($doc['url'], '_1_S') && strlen($doc['filename']) == 15){
								$imageUrl = 'http://localhost/myubic/' . $doc['url'];
							}
							}?>
						<?php //if(!empty($imageUrl)) echo $this->Html->image($this->ImageSize->crop(DS . $doc['url'], 60, 60), array('fullBase' => true)); ?>
						<?php if(!empty($imageUrl)) echo $this->Html->image(DS . $doc['url'], array('fullBase' => true)); ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="infos">
					<span class="code"><?php echo $item['StockItem']['code']; ?></span><br>
					<span class="name"><?php echo $item['StockItem']['name']; ?></span><br>
					<span class="stock"><?php echo sprintf('Stock au %s: <strong>%s</strong>', date('d.m.Y'), $item['StockItem']['quantity']); ?></span>
				</div>
				<div class="qrcode">
					<div class="qrcode-inner">
						<img src="<?php echo $item['StockItem']['qrcode']; ?>" alt=""/>
					</div>
				</div>
			</div>
		</td>
	</tr>
<?php if($i%10==0 && $k!=0): ?></table><?php endif; ?>
<?php endforeach; ?>
