<style type="text/css" media="screen,print">
.break{
   display: block;
   clear: both;
   page-break-after: always;
}
h2 {
  font-size: 22px;
  font-weight: 700;
}
h3 {
	margin: 0;
  font-size: 18px;
}
h3.panel-title {
	text-transform: uppercase;
  font-weight: 700;
}
h3.panel-title.smaller {
	font-size: 1.1em;
}
.panel-body h1 {
	margin: 0;
  font-size: 22px;
}
.panel-body h2 {
	font-weight: normal;
}
.portlet {
	margin: 0;
}
</style>
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row invoice-logo">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>CHE-112.145.798 TVA</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h2>
						<?php if($data['type'] == 'delivery'): ?>
						Livraison du <?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d.%m.%Y'); ?>
						<?php elseif($data['type'] == 'return'): ?>
						Reprise du <?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d.%m.%Y'); ?>
						<?php endif; ?>
						 -
						 <?php if($stockorder['StockOrder'][$data['type']]): ?>
							<?php if($stockorder['StockOrder'][$data['type'].'_mode'] == 'transporter'): ?>
								<?php if(!empty($stockorder[ucfirst($data['type']) . 'Transporter'])): ?>
									<?php echo strtoupper($stockorder[ucfirst($data['type']) . 'Transporter']['name']); ?>
								<?php else: ?>
									TRANSPORTEUR
								<?php endif; ?>
							<?php elseif($stockorder['StockOrder'][$data['type'] . '_mode'] == 'festiloc'): ?>
								FESTILOC
							<?php endif; ?>
						<?php else: ?>
							CLIENT
						<?php endif; ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Client</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['Client']['name'] ?></h3>
						</div>
					</div>
				</div>
				<?php if($data['type'] == 'delivery'): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de livraison</h3>
						</div>
						<div class="panel-body">
							 <h3>
							 	<?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
							 		<?php echo $stockorder['StockOrder']['delivery_place'] ?> -
							 	<?php endif; ?>
							 	<?php echo $stockorder['StockOrder']['delivery_address'] ?> - <?php echo $stockorder['StockOrder']['delivery_zip_city']; ?>
							 	</h3>
						</div>
					</div>
				</div>
				<?php elseif($data['type'] == 'return'): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de reprise</h3>
						</div>
						<div class="panel-body">
							 <h3>
							 	<?php if(!empty($stockorder['StockOrder']['return_place'])): ?>
							 		<?php echo $stockorder['StockOrder']['return_place'] ?> -
							 	<?php endif; ?>
							 	<?php echo $stockorder['StockOrder']['return_address'] ?> - <?php echo $stockorder['StockOrder']['return_zip_city']; ?></h3>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de livraison</h3>
						</div>
						<div class="panel-body">
							 <h3>Festiloc - Route du Tir Fédéral 10 - 1763 Givisiez</h3>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Numéro de commande</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['StockOrder']['id'] ?> / <?php echo $stockorder['Client']['id']; ?></h3>
						</div>
					</div>
				</div>
        <?php if($data['type'] == 'delivery'): ?>
          <div class="col-xs-6">
  					<div class="panel panel-default">
  						<div class="panel-heading bg-grey">
  							<h3 class="panel-title">Personne de contact</h3>
  						</div>
  						<div class="panel-body">
  							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
  							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
  						</div>
  					</div>
  				</div>
        <?php endif; ?>
        <?php if($data['type'] == 'return'): ?>
          <div class="col-xs-6">
  					<div class="panel panel-default">
  						<div class="panel-heading bg-grey">
  							<h3 class="panel-title">Personne de contact</h3>
  						</div>
  						<div class="panel-body">
  							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
  							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
  						</div>
  					</div>
  				</div>
        <?php endif; ?>
			</div>
			<div class="row">
				<?php if($data['type'] == 'delivery'): ?>
				<?php if(!empty($stockorder['StockOrder']['delivery_remarks'])): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques livraison</h3>
						</div>
						<div class="panel-body">
							<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<?php if($data['type'] == 'return'): ?>
				<?php if(!empty($stockorder['StockOrder']['return_remarks'])): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques reprise</h3>
						</div>
						<div class="panel-body">
							<?php echo $stockorder['StockOrder']['return_remarks']; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endif; ?>
			</div>
			<div class="row">
				<?php if(!empty($packaging['pallets'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Palettes</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['pallets'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['pallets_xl'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Palettes XL</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['pallets_xl'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Rollis</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['rollis'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis_xl'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Rollis XL</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['rollis_xl'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Poids</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Date</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Visa</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
