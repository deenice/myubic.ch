<?php $moments = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<?php
// debug($stockorder['StockOrder']['invoiced_total']);exit;
$invoice = false;
$steps = 1;
$designationFooter = __('Total');
if ($stockorder['StockOrder']['status'] == 'offer'){
  $designation = __('Offer');
  $designationFooter = __("Total de l'offre");
} elseif(in_array($stockorder['StockOrder']['status'], array('confirmed', 'delivered', 'in_progress', 'processed'))){
  $designation = __('Order confirmation');
  $designationFooter = __("Total");
} elseif($stockorder['StockOrder']['status'] == 'to_invoice' OR $stockorder['StockOrder']['status'] == 'invoiced' OR (isset($_GET['invoice']) && $_GET['invoice'] == 1)){
  $designation = __('Invoice');
  $invoice = true;
  if(!empty($reminder)){
    $designation = __('REMINDER: Invoice');
  }
} elseif($stockorder['StockOrder']['status'] == 'cancelled'){
  $designation = __('Commande annulée');
} else {
  $designation = __('Order confirmation');
}
if(isset($_GET['invoice']) && $_GET['invoice'] == 1){
  $invoice = true;
  if(isset($_GET['temp']) && $_GET['temp'] == 1){
    $designation = __('Provisional invoice');
  }
}
if($invoice && $stockorder['StockOrder']['total_deposit'] > 0){
  $designationFooter = 'Reste à payer';
}
$showPrices = isset($this->request->query['showPrices']) ? false : true;
if(isset($this->request->query['source'])){
  if($this->request->query['source'] == 'depot'){
    $designation = __('Delivery note');
  }
}
$showCoefficient = false;
if(empty($stockorder['StockOrder']['type'])){
  $showCoefficient = true;
} else {
  $showCoefficient = false;
}
foreach($stockorder['StockOrderBatch'] as $batch){
  if($batch['coefficient'] > 1){
    $showCoefficient = true;
  }
}
if($stockorder['StockOrder']['type'] == 'sale'){
  $showCoefficient = false;
}

if($invoice){
  $steps = 2;
}
if($invoice && $stockorder['StockOrder']['invoiced_total'] < 0){
  $designationFooter = __("Solde en votre faveur");
}
?>
<style type="text/css">
  .uppercase {
    text-transform: uppercase;
  }
  table thead th {
    vertical-align: top;
  }
  table.totals,
  .totals tr,
  .totals td {
    page-break-inside: avoid !important;
  }
  .break{
     display: block;
     clear: both;
     page-break-after: always;
  }
  td p {
    margin: 0;
    padding: 0;
    font-size: 80%;
  }
  .invoice {
    padding: 0;
    margin: 0;
  }
</style>
<body onload="subst()">
  <?php for($i=0; $i<$steps;$i++): ?>
  <div class="portlet light" style="padding-top: 0; padding-bottom: 0; margin: 0">
    <div class="portlet-body" style="padding-top: 0; padding-bottom: 0; margin: 0">
      <div class="invoice">
        <div class="row" style="margin-bottom: 30px">
          <div class="col-xs-6">
            <img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
          </div>
          <div class="col-xs-3 address">
            <ul class="list-unstyled">
              <li>Route du Petit-Moncor 1c</li>
              <li>1752 Villars-sur-Glâne</li>
              <li>T + 41 26 676 01 17</li>
              <li>F + 41 26 676 01 19</li>
              <li>info@festiloc.ch</li>
            </ul>
          </div>
          <div class="col-xs-3 address">
            <ul class="list-unstyled">
              <li>Adresse du dépôt</li>
              <li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
              <li>Heures d'ouverture</li>
              <li>8h30 - 11h45 / 13h30 - 16h45</li>
            </ul>
          </div>
        </div>
        <div class="row" style="margin-bottom: 30px">
          <?php if($invoice && $i==0): ?>
          <div class="col-xs-6">
            <h1 class="bold" style="margin: 0">COPIE</h1>
          </div>
          <?php endif; ?>
          <div class="col-xs-6 <?php if(!$invoice || ($invoice && $i==1)): ?>col-xs-offset-6<?php endif; ?>">
            <ul class="list-unstyled client">
              <li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
              <?php if(!empty($stockorder['StockOrder']['invoice_client'])): ?>
              <li><?php echo $stockorder['StockOrder']['invoice_client']; ?></li>
              <li><?php echo $stockorder['StockOrder']['invoice_contact_person']; ?></li>
              <?php else: ?>
              <?php if($stockorder['ContactPeopleClient']['full_name'] != $stockorder['Client']['name']): ?>
              <li><?php echo $stockorder['Client']['name']; ?></li>
              <?php endif; ?>
              <li>
                <?php echo (!empty($civilities[$stockorder['ContactPeopleClient']['civility']])) ? $civilities[$stockorder['ContactPeopleClient']['civility']] : ''; ?>
                <?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
              </li>
              <?php endif; ?>
              <li><?php echo $stockorder['StockOrder']['invoice_address']; ?></li>
              <li><?php echo $stockorder['StockOrder']['invoice_zip_city']; ?></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6">
            <?php if(!empty($stockorder['StockOrder']['name'])): ?><strong>Concerne: <?php echo $stockorder['StockOrder']['name']; ?></strong><?php endif; ?>
          </div>
          <div class="col-xs-6">
            <?php if($invoice): ?>
            Villars-sur-Glâne le <?php echo $this->Time->format(empty($stockorder['StockOrder']['date_of_invoice']) ? $stockorder['StockOrder']['return_date'] : $stockorder['StockOrder']['date_of_invoice'], '%e %b %Y'); ?>
            <?php else: ?>
            Villars-sur-Glâne le <?php echo $this->Time->format(empty($stockorder['StockOrder']['date_of_invoice']) ? $stockorder['StockOrder']['modified'] : $stockorder['StockOrder']['date_of_invoice'], '%e %b %Y'); ?>
            <?php endif; ?>
            <?php if(!empty($stockorder['StockOrder']['import_id'])): ?>
              / <?php echo $stockorder['StockOrder']['import_id']; ?>
            <?php endif; ?>
          </div>
        </div>
        <?php if(!$invoice): ?>
        <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
          <?php if($stockorder['StockOrder']['type'] != 'withdrawal'): ?>
          <div class="col-xs-6">
            <h4>Livraison</h4>
            <ul class="list-unstyled">
              <li>
                <?php if($stockorder['StockOrder']['delivery']): ?>
                  <strong class="uppercase">AVEC LIVRAISON <?php echo (!empty($stockorder['StockOrder']['delivery_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['delivery_mode'])) : ''; ?></strong>
                <?php else: ?>
                  <strong>EMPORTÉ PAR LE CLIENT</strong>
                <?php endif; ?>
              </li>
              <li>
                <strong>Date</strong>
                <?php if($stockorder['StockOrder']['delivery_date'] == '1970-01-01'): ?>
                <span class="uppercase bold">À définir</span>
                <?php else: ?>
                  <?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?>
                  <?php if(!empty($stockorder['StockOrder']['delivery_moment'])): ?>
                  (<?php echo strtolower($moments[$stockorder['StockOrder']['delivery_moment']]); ?><?php if($stockorder['StockOrder']['delivery_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['delivery_moment_hour'], '%H:%M'); ?>)
                  <?php endif; ?>
                <?php endif; ?>
              </li>
              <?php if(!empty($stockorder['StockOrder']['delivery_zip_city'])): ?>
              <li>
                <strong>Adresse</strong><br />
                <?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
                <?php echo $stockorder['StockOrder']['delivery_place'] ?><br />
                <?php endif; ?>
                <?php if(!empty($stockorder['StockOrder']['delivery_address'])): ?>
                <?php echo $stockorder['StockOrder']['delivery_address'] ?><br />
                <?php endif; ?>
                <?php echo $stockorder['StockOrder']['delivery_zip_city'] ?>
              </li>
              <?php endif; ?>
              <li>
                <strong>Personne de contact</strong><br />
                <?php echo (!empty($stockorder['ContactPeopleDelivery']['civility'])) ? $civilities[$stockorder['ContactPeopleDelivery']['civility']] : ''; ?>
                <?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
                <?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
              </li>
              <?php if(!empty($stockorder['StockOrder']['delivery_remarks'])): ?>
              <li>
                <strong>Remarques</strong><br />
                <?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
              </li>
              <?php endif; ?>
            </ul>
          </div>
          <?php endif; ?>
          <?php if($stockorder['StockOrder']['type'] != 'sale' AND $stockorder['StockOrder']['type'] != 'delivery'): ?>
          <div class="col-xs-6">
            <h4><?php if($stockorder['StockOrder']['type'] == 'withdrawal'): ?><?php echo __('Withdrawal'); ?><?php else: ?><?php echo __('Return'); ?><?php endif; ?></h4>
            <ul class="list-unstyled">
              <li>
                <?php if($stockorder['StockOrder']['return']): ?>
                  <strong class="uppercase">AVEC REPRISE <?php echo (!empty($stockorder['StockOrder']['return_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['return_mode'])) : ''; ?></strong>
                <?php else: ?>
                  <strong class="uppercase">RETOUR PAR LE CLIENT</strong>
                <?php endif; ?>
              </li>
              <li>
                <strong>Date</strong>
                <?php if($stockorder['StockOrder']['return_date'] == '1970-01-01'): ?>
                <span class="uppercase bold">À définir</span>
                <?php else: ?>
                  <?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %B %Y'); ?>
                  <?php if(!empty($stockorder['StockOrder']['return_moment'])): ?>
                  (<?php echo strtolower($moments[$stockorder['StockOrder']['return_moment']]); ?><?php if($stockorder['StockOrder']['return_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['return_moment_hour'], '%H:%M'); ?>)
                  <?php endif; ?>
                <?php endif; ?>
              </li>
              <?php if(!empty($stockorder['StockOrder']['return_zip_city'])): ?>
              <li>
                <strong>Adresse</strong><br />
                <?php if(!empty($stockorder['StockOrder']['return_place'])): ?>
                <?php echo $stockorder['StockOrder']['return_place'] ?><br />
                <?php endif; ?>
                <?php if(!empty($stockorder['StockOrder']['return_address'])): ?>
                <?php echo $stockorder['StockOrder']['return_address'] ?><br />
                <?php endif; ?>
                <?php echo $stockorder['StockOrder']['return_zip_city'] ?>
              </li>
              <?php endif; ?>
              <li>
                <strong>Personne de contact</strong><br />
                <?php echo (!empty($stockorder['ContactPeopleReturn']['civility'])) ? $civilities[$stockorder['ContactPeopleReturn']['civility']] : ''; ?>
                <?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
                <?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
              </li>
              <?php if(!empty($stockorder['StockOrder']['return_remarks'])): ?>
              <li>
                <strong>Remarques</strong><br />
                <?php echo $stockorder['StockOrder']['return_remarks']; ?>
              </li>
              <?php endif; ?>
            </ul>
          </div>
          <?php endif; ?>
        </div>
        <?php else: ?>
        <br>
        <?php endif; ?>
        <div class="row">
          <div class="col-xs-12">
            <ul class="list-unstyled offer">
              <li>
                <h4><?php echo $designation; ?> <?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?></h4>
              </li>
              <?php if($stockorder['StockOrder']['type'] != 'sale'): ?>
              <li>
                <strong>
                <?php if($stockorder['StockOrder']['service_date_begin'] == $stockorder['StockOrder']['service_date_end'] OR empty($stockorder['StockOrder']['service_date_end'])): ?>
                Date de prestation <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?>
                <?php else: ?>
                Date de prestation du <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['StockOrder']['service_date_end'], '%d.%m.%y'); ?>
                <?php endif; ?>
                </strong>
              </li>
              <?php endif; ?>
            </ul>
            <hr style="margin: 15px 0 0 0">
            <?php if(!$invoice): ?>
            <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th style="width: 60%"><?php echo __('Stock item'); ?></th>
                <th class="text-right"><?php echo __('Quantity'); ?></th>
                <?php if($showPrices): ?>
                <th class="text-right"><?php echo __('Prix HT'); ?></th>
                <th class="text-right"></th>
                <?php if($showCoefficient): ?>
                <th class="text-right"><?php echo __('Coefficient'); ?></th>
                <?php endif; ?>
                <th class="text-right"><?php echo __('Total'); ?></th>
                <?php else: ?>
                <th><?php echo __('Remarks'); ?></th>
                <?php endif; ?>
              </tr>
            </thead>
            <tbody>
              <?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
                <?php if($batch['stock_item_id'] == -1){
                  $price = $batch['price'];
                } else {
                  $price = $batch['StockItem']['price'];
                } ?>
                <tr>
                  <td><?php echo $k + 1; ?></td>
                  <td>
                    <?php if($batch['stock_item_id'] == -1): ?>
                    <?php echo $batch['name']; ?>
                    <?php else: ?>
                    <?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
                    <?php endif; ?>
                  </td>
                  <td class="text-right"><?php echo $batch['quantity']; ?></td>
                  <?php if($showPrices): ?>
                  <td class="text-right"><?php echo $price; ?></td>
                  <td class="text-right">
                    <?php if($batch['discount'] > 0): ?>
                      <?php echo $batch['discount']; ?> %
                    <?php endif; ?>
                  </td>
                  <?php if($showCoefficient): ?>
                  <td class="text-right"><?php echo number_format($batch['coefficient'], 2); ?></td>
                  <?php endif; ?>
                  <td class="text-right">
                    <?php if($batch['discount'] > 0): ?>
                      <?php
                        echo number_format($price * $batch['coefficient'] * $batch['quantity'] * (100 - $batch['discount']) / 100, 2, '.', '');
                      ?>
                    <?php else: ?>
                      <?php echo number_format($price * $batch['coefficient'] * $batch['quantity'], 2, '.', ''); ?>
                    <?php endif; ?>
                  </td>
                  <?php else: ?>
                  <td><input class="form-control input-sm" type="text"></td>
                  <?php endif;?>
                </tr>
              <?php endforeach; ?>
            </tbody>
            </table>
            <?php else: ?>
            <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th style="width: 45%"><?php echo __('Stock item'); ?></th>
                <th class="text-right" style="width: 10%"><?php echo __('Ordered quantity'); ?></th>
                <th class="text-right"><?php echo __('Prix HT'); ?></th>
                <th class="text-right"></th>
                <th class="text-right"><?php echo __('Total'); ?></th>
                <th class="text-right" style="width: 10%"><?php echo __('Delivered quantity'); ?></th>
                <th class="text-right" style="width: 10%"><?php echo __('Returned quantity'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($stockorder['StockOrderBatch'] as $k => $batch):?>
                <?php if($batch['stock_item_id'] == -1){
                  $price = $batch['price'];
                } else {
                  $price = $batch['StockItem']['price'];
                } ?>
                <tr>
                  <td><?php echo $k + 1; ?></td>
                  <td>
                    <?php if($batch['stock_item_id'] == -1): ?>
                    <?php echo $batch['name']; ?>
                    <?php else: ?>
                    <?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
                    <?php endif; ?>
                  </td>
                  <td class="text-right"><?php echo $batch['quantity']; ?></td>
                  <td class="text-right"><?php echo number_format($price * $batch['coefficient'], 2, '.', ''); ?></td>
                  <td class="text-right">
                    <?php if($batch['discount'] > 0): ?>
                      <?php echo $batch['discount']; ?> %
                    <?php endif; ?>
                  </td>
                  <td class="text-right">
                    <?php if($batch['discount'] > 0): ?>
                      <?php
                        echo number_format($price * $batch['coefficient'] * $batch['quantity'] * (100 - $batch['discount']) / 100, 2, '.', '');
                      ?>
                    <?php else: ?>
                      <?php echo number_format($price * $batch['coefficient'] * $batch['quantity'], 2, '.', ''); ?>
                    <?php endif; ?>
                  </td>
                  <td class="text-right"><?php echo $batch['delivered_quantity']; ?></td>
                  <td class="text-right"><?php echo $batch['returned_quantity']; ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            </table>
            <?php if(!empty($stockorder['StockOrder']['replacement_costs']) AND $stockorder['StockOrder']['replacement_costs'] > 0): ?>
            <strong>Frais de remplacement</strong>
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo __('Stock item'); ?></th>
                  <th><?php echo __('Missing quantity'); ?></th>
                  <th><?php echo __('Replacement price'); ?></th>
                  <th><?php echo __('Total'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
                  <?php if((is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) > $batch['returned_quantity'] AND $batch['invoice_replacement']):
                  $replacementPrice = empty($batch['StockItem']['replacement_price']) ? $batch['StockItem']['price'] * 5 : $batch['StockItem']['replacement_price'];
                  ?>
                  <tr>
                    <td><?php echo $k+1; ?></td>
                    <td><?php echo $batch['StockItem']['code'] . ' ' . $batch['StockItem']['name']; ?></td>
                    <td><?php echo (is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) - $batch['returned_quantity']; ?></td>
                    <td><?php echo number_format($replacementPrice, 2, '.', ''); ?></td>
                    <td><?php echo number_format(((is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) - $batch['returned_quantity']) * $replacementPrice, 2, '.', ''); ?></td>
                  </tr>
                  <?php endif; ?>
                <?php endforeach ?>
              </tbody>
            </table>
            <?php endif; ?>
            <?php if(!empty($stockorder['StockOrder']['extrahours_costs']) AND $stockorder['StockOrder']['extrahours_costs'] > 0): ?>
            <strong><?php echo __('Extra hours'); ?></strong>
            <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo __('Task'); ?></th>
                  <th><?php echo __('Duration'); ?></th>
                  <th><?php echo __('Price'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($stockorder['StockOrderExtraHour'] as $k => $task): ?>
                  <tr>
                    <td><?php echo $k+1; ?></td>
                    <td><?php echo $task['task']; ?></td>
                    <td><?php echo $task['duration']; ?> minutes</td>
                    <td><?php echo number_format($task['duration'],2,'.',''); ?> CHF</td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
            <?php endif; ?>
            <?php endif; ?>
          </div>
        </div>
        <?php if($showPrices): ?>
        <table class="totals" style="width: 100%; padding: 0; margin: 0">
          <tr>
            <td>
              <div class="row" style="margin-top: 20px">
                <div class="col-xs-6">
                  <table class="table">
                    <tr>
                      <td>Total HT</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['total_ht']; ?></td>
                    </tr>
                    <?php if($stockorder['StockOrder']['quantity_discount_percentage'] > 0): ?>
                    <tr>
                      <td>Rabais de quantité</td>
                      <td class="text-right">CHF - <?php echo $stockorder['StockOrder']['quantity_discount']; ?>&nbsp;&nbsp;(<?php echo number_format($stockorder['StockOrder']['quantity_discount_percentage'], 2, '.',''); ?> %)</td>
                    </tr>
                    <?php endif; ?>
                    <?php if($stockorder['StockOrder']['fidelity_discount_amount'] > 0): ?>
                    <tr>
                      <td>Rabais de fidélité</td>
                      <td class="text-right">CHF - <?php echo $stockorder['StockOrder']['fidelity_discount_amount']; ?>&nbsp;&nbsp;(<?php echo number_format($stockorder['StockOrder']['fidelity_discount_percentage'], 2, '.',''); ?> %)</td>
                    </tr>
                    <?php endif; ?>
                    <?php if(!empty($stockorder['StockOrder']['forced_delivery_costs'])): ?>
                    <tr>
                      <td>Frais de livraison</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['forced_delivery_costs']; ?></td>
                    </tr>
                    <?php elseif($stockorder['StockOrder']['delivery_costs'] > 0): ?>
                    <tr>
                      <td>Frais de livraison</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['delivery_costs']; ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if($invoice): ?>
                    <?php if(!empty($stockorder['StockOrder']['replacement_costs']) AND $stockorder['StockOrder']['replacement_costs'] > 0): ?>
                    <tr>
                      <td>Frais de remplacement</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['replacement_costs']; ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if(!empty($stockorder['StockOrder']['extrahours_costs']) AND $stockorder['StockOrder']['extrahours_costs'] > 0): ?>
                    <tr>
                      <td>Heures supplémentaires</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['extrahours_costs']; ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php endif; ?>
                  </table>
                </div>
                <div class="col-xs-6">
                  <table class="table">
                    <tr>
                      <td>Sous-total HT</td>
                      <td class="text-right">CHF <?php echo number_format($stockorder['StockOrder']['subtotal_ht'], 2, '.', ''); ?></td>
                    </tr>
                    <?php if(!empty(floatval($stockorder['StockOrder']['arrondi'])) && $invoice): ?>
                    <tr>
                      <td>Arrondi</td>
                      <td class="text-right">CHF <?php echo number_format($stockorder['StockOrder']['arrondi'], 2, '.', ''); ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                      <td>TVA 8%</td>
                      <td class="text-right">CHF <?php echo $stockorder['StockOrder']['tva']; ?></td>
                    </tr>
                    <?php if($stockorder['StockOrder']['total_deposit'] > 0 && $invoice): ?>
                    <tr>
                      <td>Total</td>
                      <td class="text-right">CHF <?php echo number_format($stockorder['StockOrder']['forced_net_total'], 2, '.', ''); ?></td>
                    </tr>
                    <tr>
                      <td>Acompte</td>
                      <td class="text-right">CHF -<?php echo number_format($stockorder['StockOrder']['total_deposit'], 2, '.', ''); ?></td>
                    </tr>
                    <?php endif; ?>
                    <?php if(!$stockorder['StockOrder']['solde'] || !$invoice): ?>
                    <tr>
                      <td style="font-weight: 700;"><strong><?php echo $designationFooter; ?></strong></td>
                      <td class="text-right"><strong>CHF <?php echo number_format(!empty(floatval($stockorder['StockOrder']['invoiced_total'])) ? abs($stockorder['StockOrder']['invoiced_total']) : abs($stockorder['StockOrder']['net_total']), 2, '.', ''); ?></strong></td>
                    </tr>
                    <?php endif; ?>
                    <?php if($stockorder['StockOrder']['invoiced_total'] == '0.00' && $invoice && !isset($_GET['temp'])): ?>
                    <tr>
                      <td style="font-weight: 700;"><strong><?php echo $designationFooter; ?></strong></td>
                      <td class="text-right"><strong>CHF 0.00</strong></td>
                    </tr>
                    <?php endif; ?>
                    <?php if($stockorder['StockOrder']['invoiced_total'] == '0.00' && $invoice && isset($_GET['temp']) && $_GET['temp'] == 1): ?>
                    <tr>
                      <td style="font-weight: 700;"><strong><?php echo $designationFooter; ?></strong></td>
                      <td class="text-right"><strong>CHF <?php echo number_format($stockorder['StockOrder']['solde'], 2, '.', ''); ?></strong></td>
                    </tr>
                    <?php endif; ?>
                    <?php if($stockorder['StockOrder']['invoice_method'] == 'deposit' && !$invoice): ?>
                      <tr>
                        <td>Demande d'acompte</td>
                        <td class="text-right">CHF <?php echo number_format(!empty($stockorder['StockOrder']['deposit']) ? $stockorder['StockOrder']['deposit'] : 0, 2, '.', ''); ?></td>
                      </tr>
                    <?php endif; ?>
                    <?php if($invoice && !empty($stockorder['StockOrder']['total_deposit']) && !empty($stockorder['StockOrder']['solde']) && $stockorder['StockOrder']['invoiced_total'] != '0.00' && $stockorder['StockOrder']['solde'] == $stockorder['StockOrder']['invoiced_total']): ?>
                      <tr>
                        <td>Reste à payer</td>
                        <td class="text-right">CHF <?php echo number_format($stockorder['StockOrder']['solde'], 2, '.', ''); ?></td>
                      </tr>
                    <?php elseif($invoice && !empty($stockorder['StockOrder']['total_deposit']) && !empty($stockorder['StockOrder']['solde']) && $stockorder['StockOrder']['invoiced_total'] != '0.00'): ?>
                      <tr>
                        <td>Reste à payer</td>
                        <td class="text-right">CHF <?php echo number_format($stockorder['StockOrder']['invoiced_total'], 2, '.', ''); ?></td>
                      </tr>
                    <?php endif; ?>
                  </table>
                </div>
                <?php if($invoice): ?>
                <div class="col-xs-12">
                  <p>Paiement net à 10 jours</p>
                </div>
                <?php endif; ?>
              </div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border-top: 1px solid #ddd">
              <?php if(AuthComponent::user('id') != 359): // depot user ?>
              <p style="padding:0; margin:0">Votre personne de contact chez Festiloc: <?php echo AuthComponent::user('full_name'); ?></p>
              <?php endif; ?>
              <?php if(!$invoice AND $stockorder['StockOrder']['status'] == 'offer' ): ?>
                <?php $date = date("Y-m-d"); $offerValidity = strtotime($date."+ 20 days"); ?>
                <?php if($offerValidity > strtotime($stockorder['StockOrder']['return_date'])){
                    $offerValidity = strtotime($stockorder['StockOrder']['return_date']);
                  } ?>
                <p style="padding:0; margin:0">Offre valable jusqu'au <?php echo $this->Time->format($offerValidity, '%d %b %Y'); ?>.</p>
              <?php endif; ?>
            </td>
          </tr>
          <?php if($stockorder['StockOrder']['status'] == 'offer' || $stockorder['StockOrder']['status'] == 'confirmed'): ?>
          <tr>
            <td colspan="2">
              <p style="margin: 0; padding: 0">
                Nettoyage compris uniquement pour assiettes, verres, couverts, nappes et serviettes.
                Si livraison par transporteur uniquement de plein pied, impossible de définir l'heure de livraison et/ou de retrait.
                Le matériel doit être rendu dans le même conditionnement et le même état.
              </p>
            </td>
          </tr>
          <?php endif; ?>
        </table>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <?php if($i==0 && $steps > 1): ?>
  <div class="break"></div>
  <?php endif; ?>
  <?php endfor; ?>
</body>
