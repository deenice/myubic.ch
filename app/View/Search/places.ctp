<?php $this->assign('page_title', __('Search')); //debug($data);?>
<?php $this->assign('page_subtitle', __('Places')); //debug($data);?>
<?php $configurationsOptions = Configure::read('Configurations.options'); ?>
<?php $placesOptions = Configure::read('Places.options'); ?>
<?php $placesTypes = Configure::read('Places.types'); ?>
<?php $placesIdealFor = Configure::read('Places.ideal_for'); ?>
<?php if(!empty($dates)){
	$dates1 = implode('&nbsp;&nbsp;', array_map(function($el){ return $el['status'] == 'potential' ? $el['date'] : null; }, $dates));
} else {
	$dates1 = '';
} ?>
<div class="row-fluid">
	<?php if(!empty($previousSearches)): ?>
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-history"></i> <?php echo __('Previous searches'); ?>
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
				<a href="javascript:;" class="remove" data-original-title="" title=""></a>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning">
						<p><i class="fa fa-warning"></i> Tu as déjà effectué des recherches de lieux. Peux-être devrais-tu reprendre une recherche existance...</p>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th><?php echo __('Created on'); ?></th>
								<th><?php echo __('Event'); ?></th>
								<th><?php echo __('Event date'); ?></th>
								<th><?php echo __('Client'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($previousSearches as $k => $search): ?>
							<tr>
								<td><?php echo $k+1; ?></td>
								<td><?php echo $this->Time->format($search['Search']['created'], '%d %B %Y'); ?></td>
								<td><?php echo $search['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($search['Event']['confirmed_date'], '%d %B %Y'); ?></td>
								<td><?php echo !empty($search['Event']['Client']['name']) ? $search['Event']['Client']['name']:''; ?></td>
								<td>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-search"></i> %s', __('View results')), array('controller' => 'search', 'action' => 'places', $search['Search']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-archive"></i> %s', __('Archive')), array('controller' => 'search', 'action' => 'archive', $search['Search']['id'], '?' => array('destination' => 'places')), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-cogs"></i> <?php echo __('Specify your criteria'); ?>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">

					<?php if($displayResults == false): ?>

					<div class="alert alert-info">
						<p><i class="fa fa-info-circle"></i> <?php echo __('Please select a commune to find places for your event.'); ?></p>
					</div>

					<?php endif ;?>
					<div class="booking-search">
						<?php echo $this->Form->create('Search', array('class' => 'form')); ?>
						<?php echo $this->Form->input('Search.id'); ?>
						<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
						<?php echo $this->Form->input('Search.model', array('value' => 'place', 'type' => 'hidden')); ?>
							<div class="row form-group">
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Commune'); ?></label>
									<div class="" data-commune-select2>
										<?php echo $this->Form->input('ZipCity', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'value' => empty($data['Search']['zip_city']) ? '' : 0, 'options' => empty($data['Search']['zip_city']) ? array() : array(0 => $data['Search']['zip_city'])));?>
										<?php echo $this->Form->input('place_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden', 'value' => empty($data['Search']['place_zip']) ? '' : $data['Search']['place_zip'])); ?>
										<?php echo $this->Form->input('place_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden', 'value' => empty($data['Search']['place_city']) ? '' : $data['Search']['place_city'])); ?>
									</div>
									<?php echo $this->Form->input('origin', array('type' => 'hidden', 'class' => 'origin', 'value' => !empty($data['Search']['zip_city']) ? urlencode($data['Search']['zip_city']) : ''));?>
									<?php echo $this->Form->input('latitude', array('type' => 'hidden', 'value' => !empty($commune['Commune']['latitude']) ? $commune['Commune']['latitude'] : ''));?>
									<?php echo $this->Form->input('longitude', array('type' => 'hidden', 'value' => !empty($commune['Commune']['longitude']) ? $commune['Commune']['longitude'] : ''));?>
								</div>
								<div class="col-md-3">
									<div>
										<label class="control-label"><?php echo __('Radius'); ?></label>
									</div>
									<?php echo $this->Form->input('radius', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline', 'data-default' => $data['Search']['radius']));?>
									<span class="help-inline">km</span>
									<span class="help-block">Par défaut 50km</span>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('Place type'); ?></label>
									<?php echo $this->Form->input('place_types', array('class' => 'bs-select form-control', 'label' => false, 'options' => $placesTypes, 'multiple' => 'multiple', 'selected' => unserialize($data['Search']['place_type']))); ?>
									<span class="help-block"><?php echo __('You can select more than one type.'); ?></span>
								</div>
								<div class="col-md-2">
									<label class="control-label"><?php echo __('Number of persons'); ?></label>
									<div>
										<?php echo $this->Form->input('place_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'value' => !empty($data['Search']['place_number_of_persons']) ? $data['Search']['place_number_of_persons'] : ''));?>
									</div>
								</div>
							</div>
							<hr>
							<div class="row form-group">
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Event'); ?></label>
									<?php echo $this->Form->input('event_id', array('label' => false, 'class' => 'form-control bs-select select2me', 'empty' => '--'.__('Select an event'))); ?>
									<span class="help-block">
										La recherche s'opère sur le nom de l'évènement (et non le client).
									</span>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Client'); ?></label>
									<?php echo $this->Form->input('Event.client_id', array('label' => false, 'class' => 'form-control', 'empty' => '', 'disabled' => true, 'value' => !empty($data['Event']['Client']['id']) ? $data['Event']['Client']['id'] : '')); ?>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Event date'); ?></label>
									<?php echo $this->Form->input('Event.confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline date-picker', 'data-size' => 16, 'disabled' => true, 'value' => !empty($data['Event']['confirmed_date']) ? $this->Time->format($data['Event']['confirmed_date'], '%A %d %B %Y') : '' ));?>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Potential dates'); ?></label>
									<?php echo $this->Form->input('Event.potentialDates', array('label' => false, 'class' => 'form-control', 'disabled' => true, 'type' => 'text', 'value' => $dates1, 'escape' => false)); ?>
								</div>
							</div>
							<hr>
							<div class="panel-group accordion" id="accordion1">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
										<?php echo __('Advanced criteria'); ?></a>
										</h4>
									</div>
									<style>
									#collapse_1 .col-md-3{
										padding-top: 10px;
										padding-bottom: 10px;
									}
									</style>
									<div id="collapse_1" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="row">
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Parking'); ?></label>
													<?php echo $this->Form->input('place_parking', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Reduced mobility'); ?></label>
													<?php echo $this->Form->input('place_reduced_mobility', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Internal caterer'); ?></label>
													<?php echo $this->Form->input('place_internal_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('External caterer'); ?></label>
													<?php echo $this->Form->input('place_external_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Maximum surface'); ?></label>
													<?php echo $this->Form->input('place_maximum_surface', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Number of rooms'); ?></label>
													<?php echo $this->Form->input('place_number_of_rooms', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Ideal for'); ?></label>
													<?php echo $this->Form->input('place_ideal_for', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-small', 'options' => $placesIdealFor, 'empty' => true));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Potential'); ?></label>
													<?php echo $this->Form->input('place_degree', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-small', 'options' => Configure::read('Places.degrees'), 'empty' => true));?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="text-center">
								<button class="btn blue margin-top-20"><?php echo __('SEARCH'); ?> <i class="m-icon-swapright m-icon-white"></i></button>
							</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if(!empty($places)): ?>
	<h2><?php echo __('%s places match your criteria', array($numberOfPlaces)); ?></h2>
	<div class="row">
		<?php if($complexSearch): ?>
		<div class="col-md-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-warning"></i><?php echo __('Select the date and the moment to see the different options'); ?>
					</div>
					<div class="actions">
						<button type="button tooltips" class="btn btn-default btn-icon-only active" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo __('Display results in a list'); ?>" rel="list"><i class="fa fa-th"></i></button>
						<button type="button tooltips" class="btn btn-default btn-icon-only" data-placement="top" data-toggle="tooltip" data-original-title="<?php echo __('Show results on a map'); ?>" rel="map"><i class="fa fa-map-marker"></i></button>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row margin-bottom-20">
						<div class="col-md-12">
							<?php foreach($dates as $k => $date): ?>
								<button type="button" class="btn btn-default date <?php echo $k==0 ? 'active':''; ?>" data-date="<?php echo $date['date']; ?>"> <i class="fa fa-calendar"></i> <?php echo $this->Time->format($date['date'], '%d %B %Y'); ?></button>
							<?php endforeach; ?>
						</div>
					</div>
					<?php if(!empty($moments)): ?>
					<div class="row">
						<div class="col-md-12">
							<?php foreach($moments as $k => $moment): ?>
								<button type="button" class="btn btn-default moment <?php echo $k==0 ? 'active':''; ?>" data-moment-id="<?php echo $moment['id']; ?>"> <i class="fa fa-clock-o"></i> <strong><?php echo $moment['name']; ?></strong> <?php echo $moment['start_hour']; ?> <?php echo __('to'); ?> <?php echo $moment['end_hour']; ?></button>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div class="col-md-12">
			<div class="alert alert-warning alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
				<i class="fa fa-warning"></i> Le <strong>temps de calcul</strong> des distances et temps de parcours peut prendre du temps.
			</div>
		</div>

		<?php if(!$complexSearch AND $numberOfPages > 1): ?>
		<div class="col-md-12 text-right">
			<ul class="pagination">
				<li>
					<?php echo $this->Html->link(__('Previous'), array('controller' => 'search', 'action' => 'places', $data['Search']['id'], $prevPage)); ?>
				</li>
				<?php for($i=0; $i < $numberOfPages; $i++): ?>
					<li class="<?php echo $i == $activePage ? 'active':''; ?>">
					<?php echo $this->Html->link($i+1, array('controller' => 'search', 'action' => 'places', $data['Search']['id'], $i)); ?>
					</li>
				<?php endfor; ?>
				<li>
					<?php echo $this->Html->link(__('Next'), array('controller' => 'search', 'action' => 'places', $data['Search']['id'], $nextPage)); ?>
				</li>
			</ul>
		</div>
		<div class="clearfix"></div>
		<?php endif; ?>

		<div id="list">
		<?php foreach($places as $item): ?>
			<div class="col-md-6">
				<div class="portlet light bordered place" data-place-id="<?php echo $item['Place']['id']; ?>" id="place<?php echo $item['Place']['id']; ?>" data-title="<?php echo $item['Place']['name']; ?>" data-lat="<?php echo $item['Place']['latitude']; ?>" data-lng="<?php echo $item['Place']['longitude']; ?>" style="height: 405px;" data-complex-search="<?php echo $complexSearch == false ? 0:1; ?>">
					<?php if($item['Place']['degree'] == 4): ?>
					<div class="ribbon ribbon-yellow ribbon-small">
					    <div class="banner">
					        <div class="text"></div>
					    </div>
					</div>
					<?php endif; ?>
					<div class="portlet-title tabbable-line">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#infos<?php echo $item['Place']['id']; ?>" data-toggle="tab">
								<?php echo __('Infos'); ?> </a>
							</li>
							<li>
								<a href="#contact<?php echo $item['Place']['id']; ?>" data-toggle="tab">
								<?php echo __('Contact'); ?> </a>
							</li>
							<?php if($complexSearch && false): ?>
							<li class="">
								<a href="#options<?php echo $item['Place']['id']; ?>" data-toggle="tab">
								<?php echo __('Options'); ?> </a>
							</li>
							<?php endif; ?>
							<li>
								<a href="#configurations<?php echo $item['Place']['id']; ?>" data-toggle="tab">
								<?php echo __('Configurations'); ?> </a>
							</li>
							<li class="dropdown pull-right">
								<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><?php echo $this->Html->link('<i class="fa fa-search"></i> ' . __('View place'), array('controller' => 'places', 'action' => 'view', $item['Place']['id']), array('target' => '_blank', 'escape' => false)); ?>
									</li>
									<li><?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit place'), array('controller' => 'places', 'action' => 'edit', $item['Place']['id']), array('target' => '_blank', 'escape' => false)); ?>
									</li>
								</ul>
							</li>
						</ul>
						<div class="caption">
							<?php echo $this->Html->link(
								'<i class="icon-pointer font-blue-steel"></i>
								<span class="caption-subject bold font-blue-steel uppercase"> ' . $this->Text->truncate($item['Place']['name'], 19) . '</span>',
								array('controller' => 'places', 'action' => 'view', $item['Place']['id']), array('target' => '_blank', 'escape' => false, 'data-original-title' => $item['Place']['name'], 'class' => 'tooltips')); ?>
						</div>
					</div>
					<div class="portlet-body portlet-empty"></div>
				</div>
			</div>
		<?php endforeach; ?>

		<div class="clearfix"></div>
		<div id="notInterestingPlaces">
			<?php if(!empty($notInterestingPlaces)): ?>
			<div class="col-md-12">
				<h2><?php echo __('Places with no interest'); ?></h2>
			</div>
			<?php foreach($notInterestingPlaces as $item): ?>
				<div class="col-md-4">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<?php echo $this->Html->link(
								'<i class="icon-pointer font-blue-steel"></i>
								<span class="caption-subject bold font-blue-steel uppercase"> ' . $this->Text->truncate($item['Place']['name'], 21) . '</span>',
								array('controller' => 'places', 'action' => 'view', $item['Place']['id']), array('target' => '_blank', 'escape' => false)); ?>

							</div>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif; ?>
		</div>
		</div>
	</div>
	<div id="map" style="overflow: hidden">
		<div id="gmap_places" class="gmaps" style="height: 600px"></div>
	</div>
	<div class="portlet hidden origin place" data-lat="<?php echo $commune['Commune']['latitude']; ?>" data-lng="<?php echo $commune['Commune']['longitude']; ?>" data-title="<?php echo $commune['Commune']['name']; ?>"></div>
<?php endif; ?>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/admin/pages/css/search.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&language=fr');
echo $this->Html->script('/metronic/theme/assets/global/plugins/gmaps/gmaps.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.search();';
$this->end();
?>
