<?php $this->assign('page_title', __('Search')); //debug($users);?>
<?php $this->assign('page_subtitle', __('Collaborators')); //debug($users);?>
<?php $competencesJobs = array(
	'F&B' => Configure::read('Competences.fb_jobs'), 
	'Animation' => Configure::read('Competences.animation_jobs'),
	'Logistics' => Configure::read('Competences.logistics_jobs')
); ?>
<?php 
$competencesJobsMerge = array_merge(Configure::read('Competences.fb_jobs'), Configure::read('Competences.animation_jobs'), Configure::read('Competences.logistics_jobs'));
$competencesHierarchies = Configure::read('Competences.hierarchies');
$languagesLevels = Configure::read('Languages.levels'); 
?>
<div class="row-fluid">
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-cogs"></i> <?php echo __('Specify your criteria'); ?>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="booking-search">
						<?php echo $this->Form->create('Search', array('class' => 'form')); ?>
						<?php echo $this->Form->input('Search.id'); ?>
						<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
						<?php echo $this->Form->input('Search.model', array('value' => 'user', 'type' => 'hidden')); ?>
							<div class="row form-group">
								<div class="col-md-4">
									<label class="control-label"><?php echo __('Event'); ?></label>
									<?php echo $this->Form->input('Search.event_id', array('label' => false, 'class' => 'form-control bs-select select2me', 'empty' => '--'.__('Select an event'))); ?>
									<span class="help-block">
										<?php echo $this->Html->link(__('Edit the event'), array('controller' => 'events', 'action' => 'edit', $data['Event']['id'])); ?>
									</span>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('Client'); ?></label>
									<?php echo $this->Form->input('Event.client_id', array('label' => false, 'class' => 'form-control', 'empty' => '', 'disabled' => true, 'value' => $data['Event']['Client']['id'])); ?>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('Event date'); ?></label>
									<?php echo $this->Form->input('Event.confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline date-picker', 'data-size' => 16, 'disabled' => true, 'value' => !empty($data['Event']['confirmed_date']) ? $this->Time->format($data['Event']['confirmed_date'], '%A %d %B %Y') : '' ));?>
								</div>
							</div>
							<hr>
							<!--div class="row form-group">
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Sector'); ?></label>
									<?php echo $this->Form->input('Search.competence_sector', array('class' => 'form-control bs-select', 'label' => false,'options' => Configure::read('Competences.sectors'), 'empty' => __('Select a sector'))); ?>
								</div>
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Job'); ?></label>
									<?php echo $this->Form->input('Search.competence_job', array('class' => 'form-control select2me', 'label' => false,'options' => $competencesJobs, 'empty' => __('Select a job'))); ?>
								</div>
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Hierarchy'); ?></label>
									<?php echo $this->Form->input('Search.competence_hierarchy', array('class' => 'form-control bs-select', 'label' => false,'options' => Configure::read('Competences.hierarchies'), 'empty' => __('Select a hierarchy'))); ?>
								</div>
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Activity'); ?></label>
									<?php echo $this->Form->input('Search.competence_activity', array('class' => 'form-control select2me', 'label' => false,'options' => $activities, 'empty' => __('Select an activity'))); ?>
								</div>
							</div>
							<hr-->
							<div class="row form-group">
								<div class="col-md-4">
									<label class="control-label"><?php echo __('French level'); ?></label>
									<?php echo $this->Form->input('Search.french_level', array('class' => 'form-control bs-select', 'label' => false,'options' => Configure::read('Languages.levels'), 'empty' => __('Select the minimal required level'))); ?>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('German level'); ?></label>
									<?php echo $this->Form->input('Search.german_level', array('class' => 'form-control bs-select', 'label' => false,'options' => Configure::read('Languages.levels'), 'empty' => __('Select the minimal required level'))); ?>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('English'); ?></label>
									<?php echo $this->Form->input('Search.english_level', array('class' => 'form-control bs-select', 'label' => false,'options' => Configure::read('Languages.levels'), 'empty' => __('Select the minimal required level'))); ?>
								</div>
							</div>
							<hr>
							<div class="text-center">
								<button class="btn blue margin-top-20"><?php echo __('SEARCH'); ?> <i class="m-icon-swapright m-icon-white"></i></button>
							</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if(!empty($users)): ?>
	<h3><?php echo __('Search results'); ?></h3>
	<div class="tabbable-custom">
		<ul class="nav nav-tabs">
			<?php $i=0; foreach($jobs as $k => $job): ?>
			<li class="<?php echo ($i++ == 0)?'active':''; ?>">
				<a href="#job<?php echo $k; ?>" data-toggle="tab">
					<?php echo $competencesJobsMerge[$job['job']]; ?> <?php echo $competencesHierarchies[$job['hierarchy']]; ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
		<div class="tab-content">
			<?php $i=0; foreach($jobs as $k => $job): ?>
			<div class="tab-pane <?php echo ($i++==0)?'active':''; ?>" id="job<?php echo $k; ?>">
				<div class="well">
					<strong><?php echo $job['name']; ?></strong>
					<p>
						Salaire horaire : <?php echo $job['salary']; ?> CHF<br />
						Heure de début : <?php echo $job['start_time']; ?><br />
						Heure de fin : <?php echo $job['end_time']; ?>
					</p>
				</div>
				<?php if(!empty($users[$job['id']])): ?>
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th style="width:20px"><?php echo $this->Form->checkbox('Email.User.checkAll', array('hiddenField' => false)); ?></th>
								<th style="width:78px"></th>
								<th><?php echo __('Infos'); ?></th>
								<th><?php echo __('Availability'); ?></th>
								<th><?php echo __('Hierarchy'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($users[$job['id']] as $user): ?>
							<tr>
								<td><?php echo $this->Form->checkbox('Email.User.id.', array('value' => $user['User']['id'], 'hiddenField' => false)); ?></td>
								<td><?php if(!empty($user['Portrait'][0]['url'])) echo $this->Html->image($this->ImageSize->crop(DS . $user['Portrait'][0]['url'], 60, 60)); ?></td>
								<td>
									<?php echo $this->Html->link($user['User']['full_name'], array('controller' => 'users', 'action' => 'view', $user['User']['id']) ); ?><br />
									<?php echo $this->Html->link($user['User']['email'], 'mailto:'.$user['User']['email']); ?><br>
									<?php echo $this->Html->link($user['User']['phone'], 'tel:'.$user['User']['phone']); ?>
									<br />
									<?php if(!empty($user['User']['french_level'])): ?>
										<span class="flag-icon flag-icon-fr"></span>
										<?php //echo $languagesLevels[$user['User']['french_level']]; ?>
									<?php endif ?>
									<?php if(!empty($user['User']['german_level'])): ?>
										<span class="flag-icon flag-icon-de"></span>
										<?php //echo $languagesLevels[$user['User']['german_level']]; ?>
									<?php endif ?>
									<?php if(!empty($user['User']['english_level'])): ?>
										<span class="flag-icon flag-icon-gb"></span>
										<?php //echo $languagesLevels[$user['User']['english_level']]; ?>
									<?php endif ?>


								</td>
								<td>
									<?php foreach($user['Availabilities'] as $item): $availability = explode('_', $item['value']); ?>
									<?php if($availability[2] == 'yes') $bgClass = 'bg-green'; ?>
									<?php if($availability[2] == 'maybe') $bgClass = 'bg-yellow'; ?>
									<span class="badge badge-roundless <?php echo $bgClass; ?>"><?php echo __($availability[1]); ?></span>
									<?php endforeach; ?>
									<?php if(!empty($user['User']['availabilities_infos'])): ?>
										<p><?php echo $user['User']['availabilities_infos']; ?></p>
									<?php endif; ?>
								</td>
								<td>
									<?php echo $competencesHierarchies[$user['Competence'][0]['hierarchy']]; ?>
								</td>
								<td>
									<?php if(!empty($user['StaffQueue'])): ?>
										<?php if($user['StaffQueue'][0]['status'] == 'waiting'): ?>
										<?php echo $this->Html->link(
											'<i class="fa fa-clock-o"></i> '. __('Waiting for response'),
											array( 
												'controller' => 'users', 
												'action' => 'recruit', 
												$user['User']['id'],
												1
											), 
											array(
												'class' => 'btn btn-warning btn-xs relaunch',
												'escape' => false,
												'data-search-id' => $data['Search']['id'],
												'data-job-id' => $job['id'],
												'data-user-id' => $user['User']['id'],
												'data-full-name' => $user['User']['full_name'],
												'data-relaunch' => 1
											)
										); ?>
										<?php elseif($user['StaffQueue'][0]['status'] == 'relaunched'): ?>
										<?php echo $this->Html->link(
											'<i class="fa fa-bullhorn"></i> '. __('Has been relaunched'),
											array( 
												'controller' => 'users', 
												'action' => 'recruit', 
												$user['User']['id'],
												1
											), 
											array(
												'class' => 'btn bg-yellow-gold btn-xs disabled', 
												'escape' => false,
												'data-search-id' => $data['Search']['id'],
												'data-job-id' => $job['id'],
												'data-user-id' => $user['User']['id'],
												'data-full-name' => $user['User']['full_name'],
												'data-relaunch' => 1
											)
										); ?>
										<?php elseif($user['StaffQueue'][0]['status'] == 'declined'): ?>
										<?php echo $this->Html->link(
											'<i class="fa fa-times"></i> '. __('Has declined'),
											array( 
												'controller' => 'users', 
												'action' => 'recruit', 
												$user['User']['id']
											), 
											array(
												'class' => 'btn btn-danger btn-xs disabled', 
												'escape' => false
											)
										); ?>										
										<?php endif; ?>
									<?php else: ?>
									<?php echo $this->Html->link(
										'<i class="fa fa-envelope"></i> '. __('Recruit'),
										array( 
											'controller' => 'users', 
											'action' => 'recruit', 
											$user['User']['id'],
											0
										), 
										array(
											'class' => 'btn btn-primary btn-xs recruit', 
											'escape' => false,
											'data-search-id' => $data['Search']['id'],
											'data-job-id' => $job['id'],
											'data-user-id' => $user['User']['id'],
											'data-full-name' => $user['User']['full_name'],
											'data-relaunch' => 0
										)
									); ?>
									<?php endif; ?>
									<?php echo $this->Html->link('<i class="fa fa-search"></i> Voir le profil', array('controller' => 'users', 'action' => 'view', $user['User']['id']), array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '__blank')); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php else:?>
				<div class="alert alert-warning">
					<p><?php echo __("Nobody is available or qualified for this job.") ?></p>
				</div>
				<?php endif; ?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>	
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/pages/css/search.css');
$this->end();
$this->start('init_scripts');
echo 'Custom.search();';
$this->end();
?>
