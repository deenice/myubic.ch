<?php $this->assign('page_title', __('Search'));?>
<?php $this->assign('page_subtitle', __('Search results for %s', $this->Html->tag('i', $term)));?>
<?php $modelNames = Configure::read('Models.names'); ?>
<?php $orderStatuses = Configure::read('StockOrders.status'); ?>
<div class="search-page search-content-4">

  <div class="search-bar bordered">
    <div class="row">
      <div class="col-lg-12">
        <?php echo $this->Form->create('Search', array('url' => array('controller' => 'search', 'action' => 'index'), 'class' => 'search-form', 'type' => 'get')); ?>
        <div class="input-group">
          <input type="text" class="form-control" name="term" value="<?php echo $term; ?>">
          <span class="input-group-btn">
            <button class="btn green-soft uppercase bold" type="button"><?php echo __('Search'); ?></button>
          </span>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>

  <div class="search-bar bordered">
    <div class="row" data-search-filters>
      <div class="col-md-2">
        <?php echo $this->Html->link(__('%s result(s)', $this->Html->tag('strong', $sizes['total'])), 'javascript:;', array('data-model' => 'all', 'escape' => false)); ?>
      </div>
      <?php if($sizes['clients']): ?>
        <div class="col-md-2">
          <?php echo $this->Html->link(__('%s client(s)', $this->Html->tag('strong', $sizes['clients'])), 'javascript:;', array('data-model' => 'client', 'escape' => false)); ?>
        </div>
      <?php endif; ?>
      <?php if($sizes['events']): ?>
        <div class="col-md-2">
          <?php echo $this->Html->link(__('%s event(s)', $this->Html->tag('strong', $sizes['events'])), 'javascript:;', array('data-model' => 'event', 'escape' => false)); ?>
        </div>
      <?php endif; ?>
      <?php if($sizes['contacts']): ?>
        <div class="col-md-2">
          <?php echo $this->Html->link(__('%s contact person(s)', $this->Html->tag('strong', $sizes['contacts'])), 'javascript:;', array('data-model' => 'contactpeople', 'escape' => false)); ?>
        </div>
      <?php endif; ?>
      <?php if($sizes['orders']): ?>
        <div class="col-md-2">
          <?php echo $this->Html->link(__('%s stockorder(s)', $this->Html->tag('strong', $sizes['orders'])), 'javascript:;', array('data-model' => 'stockorder', 'escape' => false)); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="search-table table-responsive" data-search-results>
    <table class="table table-bordered table-striped table-condensed">
      <thead class="bg-blue">
        <tr>
          <th style="width: 50%">
              <a href="javascript:;"><?php echo __('Name'); ?></a>
          </th>
          <th style="width: 25%">
              <a href="javascript:;"><?php echo __('Informations'); ?></a>
          </th>
          <th style="width: 15%">
              <a href="javascript:;"><?php echo __('Model'); ?></a>
          </th>
          <th style="width: 10%">
              <a href="javascript:;"><?php echo __('Actions'); ?></a>
          </th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($results as $result): $keys = array_keys($result); $model = $keys[0]; ?>
        <tr data-model="<?php echo strtolower($model); ?>">
          <td class="table-title">
            <h3>
              <?php if($model == 'Client'): //debug($result['ContactPeople']); ?>
              <?php if(!empty($result['Company'])): ?><span class="btn btn-xs btn-company btn-company-<?php echo $result['Company'][0]['class']; ?>"></span><?php endif; ?>
              <?php echo $this->Html->link($result[$model]['name'], array('controller' => 'clients', 'action' => 'view', $result[$model]['id']), array()); ?>
              <?php elseif($model == 'ContactPeople'): ?>
              <?php if(!empty($result['Client']['Company'])): ?><span class="btn btn-xs btn-company btn-company-<?php echo $result['Client']['Company'][0]['class']; ?>"></span><?php endif; ?>
              <?php echo $this->Html->link($result[$model]['full_name'], array('controller' => 'clients', 'action' => 'view', $result['Client']['id']), array()); ?>
              <br /><small><?php echo $result['Client']['name']; ?></small>
              <?php elseif($model == 'Event'): ?>
              <?php if(!empty($result['Client']['Company'])): ?><span class="btn btn-xs btn-company btn-company-<?php echo $result['Client']['Company'][0]['class']; ?>"></span><?php endif; ?>
                <?php echo $this->Html->link(empty($result[$model]['code_name']) ? $result[$model]['name'] : $result[$model]['code_name'], array('controller' => 'events', 'action' => 'work', $result[$model]['id']), array()); ?>
              <?php elseif($model == 'StockOrder'): ?>
                <span class="btn btn-xs btn-company btn-company-fl"></span>
                <?php echo $this->Html->link(sprintf('%s %s', $result[$model]['order_number'], $result['Client']['name']), array('controller' => 'stock_orders', 'action' => 'edit', $result[$model]['id']), array()); ?>
              <?php endif; ?>
            </h3>
            <p>
              <?php echo __('Last modification'); ?>
              <span class="font-grey-cascade"><?php echo $this->Time->format('d.m.Y H:i:s', $result[$model]['modified']); ?></span>
            </p>
          </td>
          <td class="table-infos">
          <?php if($model == 'Client'): ?>
          <strong><?php echo $result['Client']['address']; ?>, <?php echo $result['Client']['zip_city']; ?></strong>
          <?php endif; ?>
          <?php if(!empty($result['ContactPeople']) && $model == 'Client'): ?><br />
            <?php foreach($result['ContactPeople'] as $cp): ?>
              <?php echo $cp['full_name']; ?>
            <?php endforeach; ?>
          <?php elseif($model == 'Event'): ?>
            <strong><?php echo $this->Html->link($result['Client']['name'], array('controller' => 'clients', 'action' => 'view', $result['Client']['id'])); ?></strong><br>
            <?php echo $result['Client']['address']; ?>, <?php echo $result['Client']['zip_city']; ?>
          <?php elseif($model == 'StockOrder'): ?>
            <strong><?php echo __('Status'); ?></strong><br>
            <?php echo $orderStatuses[$result['StockOrder']['status']]; ?>
          <?php endif; ?>
          </td>
          <td class="table-date font-blue">
              <a href="javascript:;"><?php echo $modelNames[$model]; ?></a>
          </td>
          <td class="table-download">
            <?php if($model == 'Event'): ?>
            <?php echo $this->Html->link('<i class="icon-note font-green-soft"></i>', array('controller' => 'events', 'action' => 'work', $result['Event']['id']), array('escape' => false)); ?>
          <?php elseif($model == 'StockOrder'): ?>
            <?php echo $this->Html->link('<i class="icon-note font-green-soft"></i>', array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id']), array('escape' => false)); ?>
            <?php else: ?>
            <?php echo $this->Html->link('<i class="icon-note font-green-soft"></i>', array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']), array('escape' => false)); ?>
            <?php endif; ?>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>

</div>

<?php
$this->start('init_scripts');
echo 'Custom.search();';
$this->end();
?>
