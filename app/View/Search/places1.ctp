<?php $this->assign('page_title', __('Search')); //debug($data);?>
<?php $this->assign('page_subtitle', __('Places')); //debug($data);?>
<?php $configurationsOptions = Configure::read('Configurations.options'); ?>
<?php $placesOptions = Configure::read('Places.options'); ?>
<?php $placesDegrees = Configure::read('Places.degrees'); ?>
<?php $placesTypes = Configure::read('Places.types'); ?>
<?php $placesIdealFor = Configure::read('Places.ideal_for'); ?>
<?php if(!empty($dates)){
	$dates1 = implode('&nbsp;&nbsp;', array_map(function($el){ return $el['status'] == 'potential' ? $el['date'] : null; }, $dates));
} else {
	$dates1 = '';
} ?>
<?php $current = $this->Paginator->current('Place'); ?>
<div class="row-fluid">
	<?php if(!empty($previousSearches)): ?>
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-history"></i> <?php echo __('Previous searches'); ?>
			</div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title="" title=""></a>
				<a href="javascript:;" class="remove" data-original-title="" title=""></a>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-warning">
						<p><i class="fa fa-warning"></i> Tu as déjà effectué des recherches de lieux. Peux-être devrais-tu reprendre une recherche existance...</p>
					</div>
				</div>
				<div class="col-md-12">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th><?php echo __('Created on'); ?></th>
								<th><?php echo __('Event'); ?></th>
								<th><?php echo __('Event date'); ?></th>
								<th><?php echo __('Client'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($previousSearches as $k => $search): ?>
							<tr>
								<td><?php echo $k+1; ?></td>
								<td><?php echo $this->Time->format($search['Search']['created'], '%d %B %Y'); ?></td>
								<td><?php echo $search['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($search['Event']['confirmed_date'], '%d %B %Y'); ?></td>
								<td><?php echo !empty($search['Event']['Client']['name']) ? $search['Event']['Client']['name']:''; ?></td>
								<td>
									<?php echo $this->Html->link('<i class="fa fa-search"></i> View results', array('controller' => 'search', 'action' => 'places', $search['Search']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-archive"></i> Archive', array('controller' => 'search', 'action' => 'archive', $search['Search']['id'], '?' => array('destination' => 'places')), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>	
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="portlet light bordered">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-cogs"></i> <?php echo __('Specify your criteria'); ?>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-12">
					<div class="booking-search">
						<?php echo $this->Form->create('Search', array('class' => 'form')); ?>
						<?php echo $this->Form->input('Search.id'); ?>
						<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
						<?php echo $this->Form->input('Search.model', array('value' => 'place', 'type' => 'hidden')); ?>
							<div class="row form-group">
								<div class="col-md-3">
									<label class="control-label"><?php echo __('Commune'); ?></label>
									<?php echo $this->Form->input('ZipCity', array('label' => false, 'class' => 'form-control zip-city', 'data-default' => $data['Search']['zip_city']));?>
									<?php echo $this->Form->input('place_city', array('type' => 'hidden', 'class' => 'city'));?>
									<?php echo $this->Form->input('place_zip', array('type' => 'hidden', 'class' => 'zip'));?>
									<?php echo $this->Form->input('origin', array('type' => 'hidden', 'class' => 'origin', 'value' => !empty($data['Search']['zip_city']) ? urlencode($data['Search']['zip_city']) : ''));?>
									<?php echo $this->Form->input('latitude', array('type' => 'hidden', 'value' => !empty($commune['Commune']['latitude']) ? $commune['Commune']['latitude'] : ''));?>
									<?php echo $this->Form->input('longitude', array('type' => 'hidden', 'value' => !empty($commune['Commune']['longitude']) ? $commune['Commune']['longitude'] : ''));?>
								</div>
								<div class="col-md-3">
									<div>
										<label class="control-label"><?php echo __('Radius'); ?></label>
									</div>
									<?php echo $this->Form->input('radius', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline', 'data-default' => $data['Search']['radius']));?>
									<span class="help-inline">km</span>
									<span class="help-block">Par défaut 50km</span>
								</div>
								<div class="col-md-4">
									<label class="control-label"><?php echo __('Place type'); ?></label>
									<?php echo $this->Form->input('place_types', array('class' => 'bs-select form-control', 'label' => false, 'options' => $placesTypes, 'multiple' => 'multiple', 'selected' => unserialize($data['Search']['place_type']))); ?>
									<span class="help-block"><?php echo __('You can select more than one type.'); ?></span>
								</div>
								<div class="col-md-2">
									<label class="control-label"><?php echo __('Number of persons'); ?></label>
									<div>
										<?php echo $this->Form->input('place_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'value' => !empty($data['Search']['place_number_of_persons']) ? $data['Search']['place_number_of_persons'] : ''));?>
									</div>
								</div>
							</div>
							<hr>
							<div class="row form-group">
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Event'); ?></label>
									<?php echo $this->Form->input('event_id', array('label' => false, 'class' => 'form-control bs-select select2me', 'empty' => '--'.__('Select an event'))); ?>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Client'); ?></label>
									<?php echo $this->Form->input('Event.client_id', array('label' => false, 'class' => 'form-control', 'empty' => '', 'disabled' => true, 'value' => !empty($data['Event']['Client']['id']) ? $data['Event']['Client']['id'] : '')); ?>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Event date'); ?></label>
									<?php echo $this->Form->input('Event.confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline date-picker', 'data-size' => 16, 'disabled' => true, 'value' => !empty($data['Event']['confirmed_date']) ? $this->Time->format($data['Event']['confirmed_date'], '%A %d %B %Y') : '' ));?>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?php echo __('Potential dates'); ?></label>
									<?php echo $this->Form->input('Event.potentialDates', array('label' => false, 'class' => 'form-control', 'disabled' => true, 'type' => 'text', 'value' => $dates1, 'escape' => false)); ?>
								</div>
							</div>
							<hr>
							<div class="panel-group accordion" id="accordion1">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
										<?php echo __('Advanced criteria'); ?></a>
										</h4>
									</div>
									<style>
									#collapse_1 .col-md-3{
										padding-top: 10px;
										padding-bottom: 10px;
									}
									</style>
									<div id="collapse_1" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="row">
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Parking'); ?></label>
													<?php echo $this->Form->input('place_parking', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Reduced mobility'); ?></label>
													<?php echo $this->Form->input('place_reduced_mobility', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Internal caterer'); ?></label>
													<?php echo $this->Form->input('place_internal_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('External caterer'); ?></label>
													<?php echo $this->Form->input('place_external_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Maximum surface'); ?></label>
													<?php echo $this->Form->input('place_maximum_surface', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Number of rooms'); ?></label>
													<?php echo $this->Form->input('place_number_of_rooms', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Ideal for'); ?></label>
													<?php echo $this->Form->input('place_ideal_for', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-small', 'options' => $placesIdealFor, 'empty' => true));?>
												</div>
												<div class="col-md-3 form-inline">
													<label class="control-label margin-right-10"><?php echo __('Potential'); ?></label>
													<?php echo $this->Form->input('place_degree', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-small', 'options' => Configure::read('Places.degrees'), 'empty' => true));?>
												</div>
											</div>									
										</div>
									</div>
								</div>
							</div>
							<div class="text-center">
								<button class="btn btn-primary margin-top-20"><i class="fa fa-search"></i> <?php echo __('SEARCH'); ?></button>
							</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<div id="places-results" class="row">
<?php foreach($places as $k => $place): ?>
	<div class="col-md-6 place">
		<?php if($place['Place']['degree'] == 4): ?>
		<div class="ribbon ribbon-yellow ribbon-small">
		    <div class="banner">
		        <div class="text"></div>
		    </div>
		</div>
		<?php endif; ?>
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-pointer"></i><?php echo $place['Place']['name']; ?>
				</div>
				<div class="tools">
					<span class="badge badge-primary"></span>
				</div>		
			</div>
			<div class="portlet-body tabs-below">
				<div class="tab-content">
					<div class="tab-pane active" id="infos<?php echo $place['Place']['id'] ?>">
						<div class="row">
							<div class="col-md-8">
								<ul class="list-unstyled">
									<li>
										<i class="fa fa-map-marker"></i> 
										<?php if(!empty($place['Place']['address'])): ?><?php echo $place['Place']['address']; ?>, <?php endif; ?><?php echo $place['Place']['zip_city']; ?>
									</li>
									<li>
										<i class="fa fa-car"></i> <span class="distance"><i class="fa fa-spin fa-spinner"></i> </span><span class="time"></span>
									</li>
									<li>
										<i class="fa fa-home"></i> <span class="distanceUBIC"><i class="fa fa-spin fa-spinner"></i> </span><span class="timeUBIC"></span> <?php echo __('depuis UBIC'); ?>
									</li>
									<li>
										<i class="fa fa-star"></i> <?php echo $placesDegrees[$place['Place']['degree']]; ?>
									</li>
									<li>
										<?php if(!empty($place['Place']['max_number_of_persons'])): ?>
											<i class="fa fa-users"></i>
											<?php echo $place['Place']['max_number_of_persons'] . ' '. __("persons maximum"); ?>
										<?php endif; ?>
									</li>
									<li>
										<?php if(!empty($place['Place']['max_surface'])): ?>
											<i class="fa fa-expand"></i>
											<?php echo __("Maximum surface"); ?>: <?php echo $place['Place']['max_surface']; ?> m2
										<?php endif; ?>
									</li>
								</ul>
							</div>
							<div class="col-md-4">
								<?php if(!empty($place['Thumbnail'])): ?>
								<?php echo $this->Html->image(DS . $place['Thumbnail'][0]['url'], array('class' => 'img-responsive', 'style' => 'width:100%')); ?>
								<?php else: ?>
								<?php //echo $this->Html->image('http://dummyimage.com/177x4:3/eee/a8a8a8.png&text='.$place['Place']['name'], array('class' => 'img-responsive')); ?>
								<?php endif; ?>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="tab-pane" id="contact<?php echo $place['Place']['id'] ?>">
						<p>
							 Howdy, I'm in Section 2.
						</p>
						<p>
							 Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.
						</p>
						<p>
							<a class="btn yellow" href="ui_tabs_accordions_navs.html#tab_16_2_2" target="_blank">
							Activate this tab via URL </a>
						</p>
						<div class="clearfix"></div>
					</div>
					<div class="tab-pane" id="options<?php echo $place['Place']['id'] ?>">
						<p>
							 Howdy, I'm in Section 3.
						</p>
						<p>
							 Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate.
						</p>
						<p>
							<a class="btn purple" href="ui_tabs_accordions_navs.html#tab_16_2_3" target="_blank">
							Activate this tab via URL </a>
						</p>
						<div class="clearfix"></div>
					</div>
					<div class="tab-pane" id="remarks<?php echo $place['Place']['id'] ?>">
						<?php if(!empty($place['Place']['unavailabilities'])): ?>
						<div class="col-md-6">
							<div class="note note-danger">
								<strong><?php echo __('Unavailabilities') ?></strong>
								<p><?php echo nl2br($place['Place']['unavailabilities']); ?></p>
							</div>							
						</div>
						<?php endif; ?>
						<?php if(!empty($place['Place']['to_check'])): ?>
						<div class="col-md-6">	
							<div class="note note-warning">						
								<strong><?php echo __('Fields to be checked') ?></strong>
								<p><?php echo nl2br($place['Place']['to_check']); ?></p>
							</div>
						</div>
						<?php endif; ?>
						<div class="clearfix"></div>
					</div>
					<div class="tab-pane" id="configurations<?php echo $place['Place']['id'] ?>">
						<p>
							 Howdy, I'm in Section 3.
						</p>
						<p>
							 Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate.
						</p>
						<p>
							<a class="btn purple" href="ui_tabs_accordions_navs.html#tab_16_2_3" target="_blank">
							Activate this tab via URL </a>
						</p>
						<div class="clearfix"></div>
					</div>
				</div>				
				<ul class="nav nav-pills">
					<li class="active">
						<a href="#infos<?php echo $place['Place']['id']; ?>" data-toggle="tab"><?php echo __('Informations'); ?></a>
					</li>
					<?php if($place['Place']['show_remarks']): ?>
					<li>
						<a href="#remarks<?php echo $place['Place']['id']; ?>" data-toggle="tab"><?php echo __('Remarks'); ?></a>
					</li>
					<?php endif; ?>
					<li>
						<a href="#contact<?php echo $place['Place']['id']; ?>" data-toggle="tab"><?php echo __('Contact'); ?></a>
					</li>
					<li>
						<a href="#options<?php echo $place['Place']['id']; ?>" data-toggle="tab"><?php echo __('Options'); ?></a>
					</li>
					<li>
						<a href="#configurations<?php echo $place['Place']['id']; ?>" data-toggle="tab"><?php echo __('Configurations'); ?></a>
					</li>
				</ul>
			</div>
		</div>						
	</div>
	<?php if(($k+1)%2==0): ?>
	<div class="clearfix"></div>
	<?php endif; ?>
<?php endforeach; ?>
</div>

<?php echo $this->Paginator->next('Show more star wars posts...'); ?>

<?php

$this->start('init_scripts');
echo 'Custom.search();';
$this->end();

?>