<?php $placesOptions = Configure::read('Places.options'); ?>
<?php $placesDegrees = Configure::read('Places.degrees'); ?>
<?php echo $this->Form->input('latitude', array('type' => 'hidden', 'value' => $place['Place']['latitude'], 'class' => 'latitude', 'name' => '', 'id' => ''));?>
<?php echo $this->Form->input('longitude', array('type' => 'hidden', 'value' => $place['Place']['longitude'], 'class' => 'longitude', 'name' => '', 'id' => ''));?>
<?php echo $this->Form->input('destination', array('type' => 'hidden', 'value' => urlencode($place['Place']['address'].' '.$place['Place']['zip_city']), 'class' => 'destination', 'name' => '', 'id' => ''));?>
<div class="tab-content">
	<div class="tab-pane active" id="infos<?php echo $place['Place']['id']; ?>">
		<div class="scroller">
			<div class="row">
				<div class="col-md-8">
					<div class="well">
						<ul class="list-unstyled">
							<li>
								<i class="fa fa-tag"></i> <strong class="title"><?php echo $place['Place']['name']; ?></strong>
							</li>
							<li>
								<i class="fa fa-map-marker"></i> 
								<?php if(!empty($place['Place']['address'])): ?><?php echo $place['Place']['address']; ?>, <?php endif; ?><?php echo $place['Place']['zip_city']; ?>
							</li>
							<li>
								<i class="fa fa-car"></i> <span class="distance"><i class="fa fa-spin fa-spinner"></i> </span><span class="time"></span>
							</li>
							<li>
								<i class="fa fa-home"></i> <span class="distanceUBIC"><i class="fa fa-spin fa-spinner"></i> </span><span class="timeUBIC"></span> <?php echo __('depuis UBIC'); ?>
							</li>
							<li>
								<i class="fa fa-star"></i> <?php echo $placesDegrees[$place['Place']['degree']]; ?>
							</li>
							<li>
								<?php if(!empty($place['Place']['max_number_of_persons'])): ?>
									<i class="fa fa-users"></i>
									<?php echo $place['Place']['max_number_of_persons'] . ' '. __("persons maximum"); ?>
								<?php endif; ?>
							</li>
							<li>
								<?php if(!empty($place['Place']['max_surface'])): ?>
									<i class="fa fa-expand"></i>
									<?php echo __("Maximum surface"); ?>: <?php echo $place['Place']['max_surface']; ?> m2
								<?php endif; ?>
							</li>
						</ul>
					</div>									
				</div>
				<div class="col-md-4">
					<?php if(!empty($place['Thumbnail'])): ?>
					<?php echo $this->Html->image(DS . $place['Thumbnail'][0]['url'], array('class' => 'img-responsive', 'style' => 'width:100%')); ?>
					<?php else: ?>
					<?php //echo $this->Html->image('http://dummyimage.com/177x4:3/eee/a8a8a8.png&text='.$place['Place']['name'], array('class' => 'img-responsive')); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<?php if(!empty($place['Place']['unavailabilities'])): ?>
				<div class="col-md-6">
					<div class="note note-danger">
						<strong><?php echo __('Unavailabilities') ?></strong>
						<p><?php echo nl2br($place['Place']['unavailabilities']); ?></p>
					</div>							
				</div>
				<?php endif; ?>
				<?php if(!empty($place['Place']['to_check'])): ?>
				<div class="col-md-6">	
					<div class="note note-warning">						
						<strong><?php echo __('Fields to be checked') ?></strong>
						<p><?php echo nl2br($place['Place']['to_check']); ?></p>
					</div>
				</div>
				<?php endif; ?>	
			</div>	
		</div>
	</div>
	<div class="tab-pane" id="contact<?php echo $place['Place']['id']; ?>">
		<ul class="list-unstyled">
			<?php if(!empty($place['Place']['contact_person_name'])): ?>
			<li><i class="fa fa-user"></i> <?php echo $place['Place']['contact_person_name']; ?></li>
			<?php endif; ?>
			<?php if(!empty($place['Place']['contact_person_phone1'])): ?>
			<li><i class="fa fa-phone"></i> <?php echo $place['Place']['contact_person_phone1']; ?></li>
			<?php endif; ?>
			<?php if(!empty($place['Place']['contact_person_phone2'])): ?>
			<li><i class="fa fa-phone"></i> <?php echo $place['Place']['contact_person_phone2']; ?></li>
			<?php endif; ?>
			<?php if(!empty($place['Place']['contact_person_email'])): ?>
			<li><i class="fa fa-envelope-o"></i> <a href="mailto:<?php echo $place['Place']['contact_person_email']; ?>"><?php echo $place['Place']['contact_person_email']; ?></a></li>
			<?php endif; ?>
			<li>
			<i class="fa fa-download"></i> <?php echo $this->Html->link(__('Download visit card'), array('controller' => 'clients', 'action' => 'download', $place['Place']['id']), array('target' => '_blank', 'escape' => false)); ?></li>
		</ul>
	</div>
	<div class="tab-pane" id="options<?php echo $place['Place']['id']; ?>">
		<div class="form-group">
			<div class="btn-group options btn-group-sm" data-toggle="buttons">
				<?php foreach($placesOptions as $key => $option): ?>
					<label class="btn btn-default <?php echo $key; ?>">
					<input type="radio" class="toggle" 
						data-value="<?php echo $key; ?>" 
						data-place-id="<?php echo $place['Place']['id']; ?>"
						data-user-id="<?php echo AuthComponent::user('id'); ?>">
					<?php echo __($option); ?> </label>
				<?php endforeach; ?>
				<input type="hidden" value="" class="option_id">
				<div class="clearfix"></div>
				<p style="display:none; margin: 5px 0 0 0;" class="text-primary">
					<i class="fa fa-info-circle"></i>
					Option mise par <strong class="user"></strong> pour l'évènement <strong class="event"></strong> (<span class="moment"></span> de <span class="start_hour"></span> à <span class="end_hour"></span>) pour le client <strong class="client"></strong>.
				</p>
				<p style="display:none; margin: 5px 0 0 0;" class="text-warning">
					<i class="fa fa-warning"></i>
					Une option a été mise par <strong class="user"></strong> pour l'évènement <strong class="event"></strong> (<span class="moment"></span> de <span class="start_hour"></span> à <span class="end_hour"></span>) pour le client <strong class="client"></strong>.
				</p>
			</div>
		</div>															
		<div class="form-group">
			<?php echo $this->Form->input('remarks', array('class' => 'form-control remarks', 'label' => false, 'div' => false, 'placeholder' => __('Remarks'))); ?>
		</div>
		<div class="row">
			<div class="form-group col-md-9">
				<?php echo $this->Form->input('valid_until', array('class' => 'form-control date-picker', 'label' => false, 'div' => false, 'placeholder' => __('Valid until'), 'id' => '')); ?>
				<script type="text/javascript">
					$('.date-picker').datepicker({
		                orientation: "left",
		                autoclose: true,
		                weekStart:1,
		                language: 'fr-FR',
		                format: 'dd-mm-yyyy'
		            });
				</script>
			</div>
			<div class="form-group col-md-3">
				<?php echo $this->Html->link(__('Save'), array('controller' => 'options', 'action' => 'save'), array('class' => 'btn btn-primary btn-block save-option', 'rel' => 'place' . $place['Place']['id'])); ?>
			</div>
		</div>
	</div>
	<div class="tab-pane" id="configurations<?php echo $place['Place']['id']; ?>">
		<?php if(!empty($place['Configuration'])): ?>
			<div class="row scroller">
				<div class="col-md-12">
					<table class="table table-striped">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>
								<th><?php echo __('Number of persons'); ?></th>
								<th><?php echo __('Surface'); ?></th>
								<th><?php echo __('Volume'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($place['Configuration'] as $config): ?>
						<tr>
							<td><?php echo $config['name']; ?></td>
							<td>
								<?php echo __('From'); ?> <?php echo !empty($config['min_number_of_persons']) ? $config['min_number_of_persons'] : 0; ?> <?php echo __('to'); ?> <?php echo $config['max_number_of_persons']; ?>
							</td>
							<td><?php echo $config['surface']; ?> m2</td>
							<td><?php echo $config['volume']; ?> m3</td>
						</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>	
		<?php endif; ?>
	</div>
</div>

<div class="infowindow hidden" rel="<?php echo $place['Place']['id']; ?>">
	<div class="portlet light">
		<div class="portlet-title">
			<div class="caption">
				<span class="caption-subject bold font-blue-steel uppercase"><?php echo $place['Place']['name']; ?></span>
			</div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="col-md-8">
					<div class="well">
						<ul class="list-unstyled">
							<li>
								<i class="fa fa-map-marker"></i> 
								<?php if(!empty($place['Place']['address'])): ?><?php echo $place['Place']['address']; ?>, <?php endif; ?><?php echo $place['Place']['zip_city']; ?>
							</li>
							<li>
								<i class="fa fa-car"></i> <span class="distance"><i class="fa fa-spin fa-spinner"></i> </span><span class="time"></span>
							</li>
							<li>
								<i class="fa fa-home"></i> <span class="distanceUBIC"><i class="fa fa-spin fa-spinner"></i> </span><span class="timeUBIC"></span> <?php echo __('depuis UBIC'); ?>
							</li>
							<li>
								<i class="fa fa-star"></i> <?php echo $placesDegrees[$place['Place']['degree']]; ?>
							</li>
							<li>
								<?php if(!empty($place['Place']['max_number_of_persons'])): ?>
									<i class="fa fa-users"></i>
									<?php echo $place['Place']['max_number_of_persons'] . ' '. __("persons maximum"); ?>
								<?php endif; ?>
							</li>
							<li>
								<?php if(!empty($place['Place']['max_surface'])): ?>
									<i class="fa fa-expand"></i>
									<?php echo __("Maximum surface"); ?>: <?php echo $place['Place']['max_surface']; ?> m2
								<?php endif; ?>
							</li>
						</ul>
					</div>									
				</div>
				<div class="col-md-4">
					<?php if(!empty($place['Thumbnail'])): ?>
					<?php echo $this->Html->image(DS . $place['Thumbnail'][0]['url'], array('class' => 'img-responsive', 'style' => 'width:100%')); ?>
					<?php else: ?>
					<?php echo $this->Html->image('http://dummyimage.com/177x4:3/eee/a8a8a8.png&text='.$place['Place']['name'], array('class' => 'img-responsive')); ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row">
				<?php if(!empty($place['Place']['unavailabilities'])): ?>
				<div class="col-md-6">
					<div class="note note-danger">
						<strong><?php echo __('Unavailabilities') ?></strong>
						<p><?php echo nl2br($place['Place']['unavailabilities']); ?></p>
					</div>							
				</div>
				<?php endif; ?>
				<?php if(!empty($place['Place']['to_check'])): ?>
				<div class="col-md-6">	
					<div class="note note-warning">						
						<strong><?php echo __('Fields to be checked') ?></strong>
						<p><?php echo nl2br($place['Place']['to_check']); ?></p>
					</div>
				</div>
				<?php endif; ?>	
			</div>		
		</div>
	</div>
</div>


