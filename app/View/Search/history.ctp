<?php $this->assign('page_title', __('Search')); ?>
<?php $this->assign('page_subtitle', __('History')); ?>
<div class="tabbable-custom" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#places" data-toggle="tab"><i class="icon-pointer"></i> <?php echo __('Places'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane hidden" id="users">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-history"></i><?php echo __('History of your searches for collaborators'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Search for a collaborator') . ' <i class="fa fa-search"></i>', array('controller' => 'search', 'action' => 'users'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table">
						<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th><?php echo __('Created on'); ?></th>
								<th><?php echo __('Event'); ?></th>
								<th><?php echo __('Event date'); ?></th>
								<th><?php echo __('Client'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($searches['users'] as $search): ?>
							<tr>
								<td><?php echo $this->Time->format($search['Search']['created'], '%d %B %Y'); ?></td>
								<td><?php echo $search['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($search['Event']['confirmed_date'], '%d %B %Y'); ?></td>
								<td><?php echo !empty($search['Event']['Client']['name']) ? $search['Event']['Client']['name'] : ''; ?></td>
								<td>
									<?php echo $this->Html->link('<i class="fa fa-search"></i> View results', array('controller' => 'search', 'action' => 'users', $search['Search']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-archive"></i> Archive', array('controller' => 'search', 'action' => 'archive', $search['Search']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane active" id="places">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-history"></i><?php echo __('History of your searches for places'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Search for a place') . ' <i class="fa fa-search"></i>', array('controller' => 'search', 'action' => 'places'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table">
						<table class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th><?php echo __('Created on'); ?></th>
								<th><?php echo __('Event'); ?></th>
								<th><?php echo __('Event date'); ?></th>
								<th><?php echo __('Client'); ?></th>
								<th><?php echo __('Region'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($searches['places'] as $search): ?>
							<tr>
								<td><?php echo $this->Time->format($search['Search']['created'], '%d %B %Y'); ?></td>
								<td><?php echo $search['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($search['Event']['confirmed_date'], '%d %B %Y'); ?></td>
								<td><?php echo !empty($search['Event']['Client']['name']) ? $search['Event']['Client']['name'] : ''; ?></td>
								<td><?php echo $search['Search']['zip_city']; ?></td>
								<td>
									<?php echo $this->Html->link('<i class="fa fa-search"></i> View results', array('controller' => 'search', 'action' => 'places', $search['Search']['id']), array('class' => 'btn btn-info btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-archive"></i> Archive', array('controller' => 'search', 'action' => 'archive', $search['Search']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
