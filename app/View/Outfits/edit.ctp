<?php $this->assign('page_title', $this->Html->link(__('Outfits'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add an outfit'));?>
<?php global $COMPANIES; ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-moustache font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an outfit'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Outfit', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('Outfit.id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $COMPANIES)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Outfits.types'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Items'); ?></label>
					<div class="col-md-9">
						<?php if(!empty($outfit['OutfitItem'])): ?>
						<?php foreach($outfit['OutfitItem'] as $k => $item): ?>
							<?php echo $this->Form->input(sprintf('OutfitItem.%s.name', $k), array('div' => false, 'label' => false, 'class' => 'form-control', 'value' => $item['name'])); ?>
							<?php echo $this->Form->input(sprintf('OutfitItem.%s.id', $k), array('type' => 'hidden', 'value' => $item['id'])); ?>
							<?php echo $this->Form->input(sprintf('OutfitItem.%s.outfit_id', $k), array('type' =>'hidden', 'value' => $outfit['Outfit']['id'])); ?>
							<br>
						<?php endforeach; ?>
						<?php echo $this->Form->input(sprintf('OutfitItem.%s.name', sizeof($outfit['OutfitItem'])), array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
						<?php echo $this->Form->input(sprintf('OutfitItem.%s.outfit_id', sizeof($outfit['OutfitItem'])), array('type' => 'hidden', 'value' => $outfit['Outfit']['id'])); ?>
						<?php else: ?>
							<?php for($i=0; $i<3; $i++): ?>
								<?php echo $this->Form->input('OutfitItem..name', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
								<br>
							<?php endfor; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
