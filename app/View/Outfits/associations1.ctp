<?php $this->assign('page_title', $this->Html->link(__('Orders'), array('controller' => 'outfits', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Associations'));?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-mustache font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"> <?php echo __('Associations'); ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-condensed table-outfit-associations">
			<thead>
				<tr>
					<th class="model"></th>
					<?php foreach($outfits as $outfit): ?>
						<th class="order">
							<div><span class="btn btn-xs btn-company btn-company-<?php echo $outfit['Company']['class']; ?>"></span> <?php echo $outfit['Outfit']['name']; ?></div>
						</th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($items as $company => $itemss): ?>
					<tr class="company">
						<td colspan="<?php echo sizeof($outfits) + 1; ?>">
							<strong class="uppercase"><?php echo $company; ?></strong>
						</td>
					</tr>
					<?php foreach($itemss as $item): ?>
					<tr>
						<td>
							<?php echo $item['name']; ?>
						</td>
						<?php for($i=0; $i<sizeof($outfits);$i++): ?>
							<td>
								<input type="checkbox" name="name" <?php echo in_array($outfits[$i]['Outfit']['id'], $item['outfits']) ? 'checked="checked"':''; ?> data-model-id="<?php echo $item['id']; ?>" data-model="<?php echo $item['model']; ?>" data-outfit-id="<?php echo $outfits[$i]['Outfit']['id']; ?>">
							</td>
						<?php endfor; ?>
					</tr>
				<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.outfit_associations();';
$this->end();
?>
