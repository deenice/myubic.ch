<?php
$types = Configure::read('Outfits.types');
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'outfits', 'action' => 'edit', $result['Outfit']['id']),
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $this->dtResponse['aaData'][] = array(
      $result['Outfit']['name'],
      $types[$result['Outfit']['type']],
      $actions
  );
}
