<?php $this->assign('page_title', __('Outfits')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-map"></i><?php echo __('List of all outfits'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add an outfit'), array('controller' => 'outfits', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-link"></i> ' . __('Associations'), array('controller' => 'outfits', 'action' => 'associations'), array('class' => 'btn btn-sm blue', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Outfit'); ?>
	</div>
</div>
