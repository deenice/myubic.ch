<?php $this->assign('page_title', $this->Html->link(__('Outfits'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add an outfit'));?>
<?php global $COMPANIES; ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-moustache font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an outfit'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Outfit', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $COMPANIES)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Outfits.types'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Items'); ?></label>
					<div class="col-md-9">
						<?php for($i=0; $i<3; $i++): ?>
							<?php echo $this->Form->input('OutfitItem..name', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
							<br>
						<?php endfor; ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
