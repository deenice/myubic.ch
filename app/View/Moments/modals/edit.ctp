<div class="modal-body">
	<?php echo $this->Form->create('Moment', array('class' => 'form-horizontal form-moment', 'url' => array('controller' => 'moments', 'action' => 'edit'))); ?>
	<?php echo $this->Form->input('Moment.event_id', array('type' => 'hidden', 'value' => $moment['Moment']['event_id'])); ?>
	<?php echo $this->Form->input('Moment.id', array('type' => 'hidden', 'value' => '')); ?>

	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.type', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Moments.types'), 'value' => empty($moment['Moment']['type']) ? '' : $moment['Moment']['type']));?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.start_hour', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($moment['Moment']['start_hour']) ? '' : $moment['Moment']['start_hour']));?>
			<?php echo $this->Form->input('Moment.end_hour', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($moment['Moment']['end_hour']) ? '' : $moment['Moment']['end_hour']));?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.name', array('label' => false, 'class' => 'form-control', 'value' => empty($moment['Moment']['name']) ? '' : $moment['Moment']['name']));?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.remarks', array('label' => false, 'class' => 'form-control', 'value' => empty($moment['Moment']['remarks']) ? '' : $moment['Moment']['remarks']));?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Place'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.place_id', array('label' => false, 'class' => 'form-control bs-select', 'value' => empty($moment['Moment']['place_id']) ? '' : $moment['Moment']['place_id']));?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-md-3"><?php echo __('Activity'); ?></label>
		<div class="col-md-9">
			<?php echo $this->Form->input('Moment.activity_id', array('label' => false, 'class' => 'form-control bs-select', 'value' => empty($moment['Moment']['activity_id']) ? '' : $moment['Moment']['activity_id']));?>
			<span class="help-block">Lister que les activités sélectionnées?</span>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
