<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', $this->request->data['StockFamily']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-basket font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['StockFamily']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockFamily', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('StockFamily.id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies, 'data-company' => '', 'data-value' => $this->request->data['StockFamily']['company_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_category_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $categories, 'data-stock-category' => '', 'data-value' => $this->request->data['StockFamily']['stock_category_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Section'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_section_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $sections, 'data-stock-section' => '', 'data-value' => $this->request->data['StockFamily']['stock_section_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'data-stock-family' => '', 'data-value' => $this->request->data['StockFamily']['name']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Code'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('code', array('label' => false, 'class' => 'form-control', 'readonly' => true));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.stockitems();';
$this->end();
?>
