<?php
$countries = Configure::read('Countries');
foreach ($dtResults as $result) {

	$contact = $result['ContactPeople']['full_name'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['ContactPeople']['phone'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['ContactPeople']['email'];

	$address = $result['Partner']['address'] . $this->Html->tag('br') . $result['Partner']['zip'] . ' ' . $result['Partner']['city'] . $this->Html->tag('br') . $countries[$result['Partner']['country']];

	$actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'partners', 'action' => 'view', $result['Partner']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false)
  );
  $actions .= $this->Html->tag('');
  $actions .= $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'partners', 'action' => 'edit', $result['Partner']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['Partner']['name'],
    $contact,
    $address,
    $actions
  );
}
