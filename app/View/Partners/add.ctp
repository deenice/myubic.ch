<?php $this->assign('page_title', $this->Html->link(__('Partners'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Add a partner'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wallet font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a partner'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">

		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Partner', array('class' => 'form-horizontal', 'type' => 'file')); ?>
		<?php echo $this->Form->input('Partner.type', array('type' => 'hidden', 'value' => 'partner')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('companies', array('label' => false, 'class' => 'form-control bs-select', 'multiple' => true, 'options' => $companies, 'value' => 2));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Logo'); ?></label>
				<div class="col-md-9">
					<div class="fileinput fileinput-new" data-provides="fileinput">
						<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
							<?php if(!empty($thumbnail)): ?>
								<?php echo $this->Html->image(DS . $thumbnail[0]['Document']['url'], array('max-height' => 140, 'width' => 140)); ?>
							<?php else: ?>
								<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+logo" alt=""/>
							<?php endif; ?>
						</div>
						<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
						<div>
							<span class="btn default btn-file">
							<span class="fileinput-new"><?php echo __('Select image'); ?></span>
							<span class="fileinput-exists"><?php echo __('Change'); ?></span>
							<?php echo $this->Form->input('logo', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
							</span>
							<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group" data-commune-select2>
				<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array(), 'value' => ''));?>
					<?php echo $this->Form->input('zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
					<?php echo $this->Form->input('city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'value' => 'CH', 'options' => Configure::read('Countries')));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Website'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('website', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue margin-bottom-5" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue margin-bottom-5" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					<br>
					<button type="submit" class="btn blue" value="contactpeople" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and add a contact person'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
