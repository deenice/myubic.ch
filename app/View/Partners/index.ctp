<?php $this->assign('page_title', $this->Html->link(__('Partners'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<?php $civilities = Configure::read('ContactPeople.civilities'); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-users"></i><?php echo __('List of all partners'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a partner'), array('controller' => 'partners', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Partners'); ?>
	</div>
</div>
