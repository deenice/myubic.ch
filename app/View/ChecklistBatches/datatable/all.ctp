<?php
foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'checklist_phases', 'action' => 'edit', $result['ChecklistPhase']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );
  $actions .= $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit the checklist"),
    array('controller' => 'checklists', 'action' => 'edit', $result['Checklist']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );
  $actions .= $this->Html->link(
    '<i class="fa fa-plus"></i> ' . __("Add a task"),
    array('controller' => 'checklist_tasks', 'action' => 'add', 'checklist_phase_id' => $result['ChecklistPhase']['id']),
    array('class' => 'btn btn-success btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['ChecklistPhase']['name'],
    $result['Checklist']['name'],
    $actions
  );
}
