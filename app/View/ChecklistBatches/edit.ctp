<?php $this->assign('page_title', $this->Html->link(__('Checklist phases'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $phase['ChecklistPhase']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-list font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $phase['ChecklistPhase']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('ChecklistPhase', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Checklist'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('checklist_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $checklists)); ?>
					</div>
				</div>
				<?php if(!empty($phases)): ?>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Position'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('position', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Before'), 'data-off-text' => __('After'), 'checked' => 1, 'type' => 'checkbox'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phase'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('neighbor', array('class' => 'form-control bs-select', 'label' => false, 'options' => $phases)); ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
