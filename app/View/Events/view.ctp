<?php
$configurationsOptions = Configure::read('Configurations.options');
$eventsStatus = Configure::read('Events.status');
?>
<?php $this->assign('page_title', $this->Html->link(__('Events'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $event['Event']['name']);?>
<div class="row profile">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-calendar font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Informations'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<ul class="list-unstyled">
							<li>
								<strong>Date</strong><br />
								<?php echo $this->Time->format($event['Event']['confirmed_date'], '%A %d %b %Y'); ?>
							</li>
							<li>
								<strong>Nombre de personnes</strong><br />
								<?php echo $event['Event']['min_number_of_persons']; ?>
								<?php echo __('to'); ?> 
								<?php echo $event['Event']['max_number_of_persons']; ?>
							</li>
							<?php if(!empty($event['Event']['language'])): ?>
								<li>
									<strong><?php echo __('Language of correspondence'); ?></strong><br />
									<span class="flag-icon flag-icon-<?php echo $event['Event']['language'] == 'en' ? 'gb' : $event['Event']['language']; ?>"></span>
								</li>
							<?php endif; ?>
							<?php if(!empty($event['Event']['languages'])): $languages = explode(',', $event['Event']['languages']); ?>
								<li>
									<strong><?php echo __('Language during event'); ?></strong><br />
									<?php foreach($languages as $language): ?>
									<span class="flag-icon flag-icon-<?php echo $language == 'en' ? 'gb' : $language; ?>"></span>
									<?php endforeach; ?>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>	
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-money font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Client') ;?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<h3 class="no-margin"><?php echo $event['Client']['name']; ?></h3><br />
								<strong><?php echo $event['ContactPeople']['name']; ?></strong><br />
								<?php if(!empty($event['ContactPeople']['department'])): ?>
								<span><?php echo $event['ContactPeople']['department']; ?></span><br />
								<?php endif; ?>
								<?php if(!empty($event['ContactPeople']['function'])): ?>
								<span><?php echo $event['ContactPeople']['function']; ?></span><br />
								<?php endif; ?>
								<?php if(!empty($event['ContactPeople']['phone'])): ?>
								<span><?php echo $event['ContactPeople']['phone']; ?></span><br />
								<?php endif; ?>
								<?php if(!empty($event['ContactPeople']['email'])): ?>
								<span><?php echo $event['ContactPeople']['email']; ?></span>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-user font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Person in charge') ;?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-3">
								<?php echo $this->Html->image(
									$this->ImageSize->crop($event['User']['Portrait'][0]['url'], 120, 120), array('class' => 'img-responsive')); ?>
							</div>
							<div class="col-md-9">
								<h4 class="no-margin"><?php echo $event['User']['full_name']; ?></h4>
								<h5><?php echo $event['User']['function']; ?></h5>
								<h5><?php echo $event['User']['email']; ?></h5>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-clock-o font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Moments'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th><?php echo __('Moment'); ?></th>
									<th><?php echo __('Start'); ?></th>
									<th><?php echo __('End'); ?></th>
									<th><?php echo __('Place'); ?></th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($event['Moment'] as $key => $moment): ?>
								<tr>
									<td><?php echo $key + 1; ?></td>
									<td><?php echo $moment['name']; ?></td>
									<td><?php echo $moment['start_hour']; ?></td>
									<td><?php echo $moment['end_hour']; ?></td>
									<td><?php echo (!empty($moment['Place'])) ? $moment['Place']['name'] : ''; ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>	
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-users font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Staff'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table table-hover table-striped">
							<thead>
								<tr>
									<th>#</th>
									<th>Job</th>
									<th>Staff</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach($event['Job'] as $key => $job): ?>
								<tr>
									<td><?php echo $key + 1; ?></td>
									<td><?php echo $job['name']; ?></td>
									<td><?php echo (!empty($job['User'])) ? $job['User']['full_name'] : __('still to provide') ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>	
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-files-o font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Documents'); ?></span>
						</div>
					</div>
				</div>	
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-clipboard font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Notes'); ?></span>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_styles');
$this->end();
$this->start('page_level_plugins');
$this->end();
$this->start('page_level_scripts');
$this->end();
$this->start('init_scripts');
$this->end();
?>
