<?php $this->assign('page_title', __('Events')); ?>
<?php $this->assign('page_subtitle', __('Calendar')); ?>

<div class="portlet light">
  <div class="portlet-body">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption">
        			<span class="uppercase bold"><?php echo __('Filters'); ?></span>
        		</div>
            <div class="actions">
              <div class="btn-group">
                <a href="#calendar" class="btn default active" data-toggle="tab"> <i class="fa fa-calendar"></i> </a>
                <a href="#list" class="btn default" data-toggle="tab"> <i class="fa fa-list"></i> </a>
              </div>
            </div>
          </div>
          <div class="portlet-body">
            <div class="row filters filters1">
              <div class="col-md-4">
                <?php echo $this->Form->input('status', array('type' => 'select', 'options' => Configure::read('Events.status'), 'empty' => __('Select a status'), 'class' => 'form-control bs-select', 'data-field' => 'status')); ?>
              </div>
              <div class="col-md-4">
                <?php echo $this->Form->input('event_manager', array('type' => 'select', 'options' => $managers, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'resp_id')); ?>
              </div>
              <div class="col-md-4">
                <?php echo $this->Form->input('event_category_id', array('type' => 'select', 'label' => __('Category'), 'options' => $categories, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'event_category_id')); ?>
              </div>
              <div class="col-md-2 hidden">
                <?php echo $this->Form->input('place', array('type' => 'select', 'options' => $places, 'empty' => true, 'class' => 'form-control bs-select', 'data-field' => 'place_id')); ?>
              </div>
              <div class="col-md-3 hidden">
                <?php echo $this->Form->input('activity', array('type' => 'select', 'options' => $activities, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'activity_id')); ?>
              </div>
              <div class="col-md-3 hidden">
                <?php echo $this->Form->input('moment', array('type' => 'select', 'options' => array('standard' => 'Sans préparation/rangement', 'complex' => 'Avec préparation/rangement'), 'class' => 'form-control bs-select', 'value' => 'standard', 'data-field' => 'moment')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 hidden">
        <div class="portlet light bordered">
          <div class="portlet-title">
            <div class="caption">
        			<span class="uppercase bold"><?php echo __('Legend'); ?></span>
        		</div>
          </div>
          <div class="portlet-body">
            <ul class="list-unstyled">
              <li><span class="badge bg-blue badge-roundless">&nbsp;&nbsp;</span> <?php echo __('Confirmed events'); ?></li>
              <li><span class="badge bg-yellow-crusta badge-roundless">&nbsp;&nbsp;</span> <?php echo __('Under development events'); ?></li>
              <li><span class="badge bg-yellow-gold badge-roundless">&nbsp;&nbsp;</span> <?php echo __('Options'); ?></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="calendar">
          <div class="col-md-12">
            <div class="portlet light calendar">
              <div class="portlet-title"></div>
              <div class="portlet-body">
      					<div class="row">
      						<div class="col-md-12">
      							<div id="eventsCalendar"></div>
      						</div>
      					</div>
      				</div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="list">
          <div class="col-md-12">
            <div class="portlet light">
              <div class="portlet-title"></div>
              <div class="portlet-body">
      					<div class="row">
      						<div class="col-md-12">
      							<?php echo $this->DataTable->render('Upcoming'); ?>
      						</div>
      					</div>
      				</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="context">
	<ul class="dropdown-menu" role="menu" style="/*height: 400px; overflow-y: scroll*/">
		<li class="disabled uppercase bold"><a href="#">Event manager</a></li>
    <li class=""><a href="#">Floriane Pochon</a></li>
    <li class="divider"></li>
		<li class="disabled uppercase bold"><a href="#">Place</a></li>
    <li class=""><a href="#">Forum Fribourg</a></li>
    <li class="divider"></li>
		<li class="disabled uppercase bold"><a href="#">Activity</a></li>
    <li class=""><a href="#">Pursuit Gaming</a></li>
		<li class="disabled uppercase bold"><a href="#">Actions</a></li>
    <li class=""><a href="#">Trouver du personnel</a></li>
    <li class=""><a href="#">Trouver un lieu</a></li>
    <li class=""><a href="#">Imprimer feuille palette</a></li>
	</ul>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
