<?php $this->assign('page_title', __('Events')); ?>
<?php $this->assign('page_subtitle', __('Staff')); ?>

<div class="portlet light">
  <div class="portlet-body">
    <div class="row">
			<div class="col-md-12">
				<div class="portlet light calendar">
					<div class="portlet-title">
						<div class="row">
							<div class="col-md-9 filter-events">
								<ul></ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="eventsStaff" data-company-ids="<?php echo implode(',', $companyIds); ?>"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
