<div class="portlet light">
	<div class="portlet-body">
		<div class="panel-group accordion" id="filters1">
	    <div class="panel panel-default">
	        <div class="panel-heading">
            <h4 class="panel-title">
              <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#filters1" href="#filtersBlock1"> <?php echo __('Filters'); ?> </a>
            </h4>
	        </div>
	        <div id="filtersBlock1" class="panel-collapse collapse in">
            <div class="panel-body">
              <table class="table">
              	<tbody>
              		<tr>
              			<td>Resp.</td>
										<td>
											<input type="checkbox" name="name" value=""> Lorène
											<input type="checkbox" name="name" value=""> Fabienne
											<input type="checkbox" name="name" value=""> Michèle
										</td>
              		</tr>
									<tr>
										<td>Langue de correspondance</td>
										<td>
											<input type="checkbox" name="name" value=""> <span class="flag-icon flag-icon-fr"></span>
											<input type="checkbox" name="name" value=""> <span class="flag-icon flag-icon-de"></span>
											<input type="checkbox" name="name" value=""> <span class="flag-icon flag-icon-gb"></span>
										</td>
									</tr>
									<tr>
										<td>CA</td>
										<td>
											<input type="number" name="name" value="" class="form-control input-small input-inline">
											<input type="number" name="name" value="" class="form-control input-small input-inline">
										</td>
									</tr>
									<tr>
										<td>Statut CRM</td>
										<td>
											<input type="checkbox" name="name" value=""> Nouvelle demande
											<input type="checkbox" name="name" value=""> Offre en cours
											<input type="checkbox" name="name" value=""> Offre non valide / nulle
											<input type="checkbox" name="name" value=""> Offre délivrée
											<input type="checkbox" name="name" value=""> Offre en attente
											<input type="checkbox" name="name" value=""> Offre à adapter
											<input type="checkbox" name="name" value=""> Offre relancée
											<input type="checkbox" name="name" value=""> Offre confirmée
											<input type="checkbox" name="name" value=""> Offre refusée
										</td>
									</tr>
              	</tbody>
              </table>
            </div>
	        </div>
	    </div>
    </div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>Resp.</th>
					<th>Dossier</th>
					<th>Ouverture</th>
					<th>Client</th>
					<th>L.C.</th>
					<th>L.E.</th>
					<th>Date(s)</th>
					<th>PAX</th>
					<th>Feeling</th>
					<th>Statut CRM</th>
					<th>CA</th>
					<th>Notes</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><span class="btn btn-company btn-company-ubic btn-xs"></span></td>
					<td>Laurie</td>
					<td>2016-12</td>
					<td>20.12.2015</td>
					<td>Philip Morris</td>
					<td><span class="flag-icon flag-icon-fr"></span></td>
					<td><span class="flag-icon flag-icon-fr"></span> <span class="flag-icon flag-icon-gb"></span></td>
					<td>Jeudi 03.03.2016</td>
					<td>80</td>
					<td>80%</td>
					<td><a href="" title="20.01.2016">Offre envoyée</a></td>
					<td>12520 CHF</td>
					<td><a href="#" data-toggle="modal" data-target="#">Afficher</a></td>
				</tr>
				<tr>
					<td><span class="btn btn-company btn-company-ubic btn-xs"></span></td>
					<td>Fabienne</td>
					<td>2016-12</td>
					<td>20.12.2015</td>
					<td>Philip Morris International &amp; Co (Mont-sur-Rolle)</td>
					<td><span class="flag-icon flag-icon-de"></span></td>
					<td><span class="flag-icon flag-icon-de"></span></td>
					<td>01.04.2016 - 15.04.2016</td>
					<td>80</td>
					<td>80%</td>
					<td><a href="" title="20.01.2016">Offre envoyée</a></td>
					<td>12520 CHF</td>
					<td><a href="#" data-toggle="modal" data-target="#">Afficher</a></td>
				</tr>
			</tbody>
		</table>

	</div>
</div>


<div class="portlet light">
	<div class="portlet-body">
		<div class="panel-group accordion" id="filters">
	    <div class="panel panel-default">
	        <div class="panel-heading">
            <h4 class="panel-title">
              <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#filters" href="#filtersBlock"> <?php echo __('Filters'); ?> </a>
            </h4>
	        </div>
	        <div id="filtersBlock" class="panel-collapse collapse in">
            <div class="panel-body">
							<?php echo $this->Form->create('Filter'); ?>
              <table class="table">
              	<tbody>
              		<tr>
              			<td style="width: 20%">Resp.</td>
										<td>
											<?php echo $this->Form->input('Filter.resp_id', array('label' => false, 'multiple' => 'checkbox', 'options' => $managers, 'class' => 'input-inline')); ?>
										</td>
              		</tr>
									<tr>
										<td>Langue pendant l'évènement</td>
										<td>
											<?php echo $this->Form->input('Filter.languages', array('div' => false, 'label' => false, 'multiple' => 'checkbox', 'options' => $eventLanguages, 'class' => 'input-inline', 'escape' => false)); ?>
										</td>
									</tr>
									<tr>
										<td>Contenu</td>
										<td>
											<?php echo $this->Form->input('Filter.content_id', array('div' => false, 'label' => false, 'multiple' => 'checkbox', 'class' => 'input-inline', 'options' => array('bts', 'mg'))); ?>
										</td>
									</tr>
									<tr>
										<td>PAX</td>
										<td>
											<span class="help-inline"><?php echo __('From'); ?></span>
											<?php echo $this->Form->input('Filter.min_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
											<span class="help-inline"><?php echo __('to'); ?></span>
											<?php echo $this->Form->input('Filter.max_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
										</td>
									</tr>
									<tr>
										<td>Date(s)</td>
										<td>
											<span class="help-inline"><?php echo __('From'); ?></span>
											<?php echo $this->Form->input('Filter.min_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
											<span class="help-inline"><?php echo __('to'); ?></span>
											<?php echo $this->Form->input('Filter.max_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<?php echo $this->Form->button(sprintf('<i class="fa fa-check"></i> %s', __('Apply filters')), array('label' => false, 'type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false)); ?>
											<?php echo $this->Form->button(sprintf('<i class="fa fa-times"></i> %s', __('Reset filters')), array('label' => false, 'type' => 'reset', 'class' => 'btn btn-default', 'escape' => false)); ?>
										</td>
									</tr>
              	</tbody>
              </table>
							<?php echo $this->Form->end(); ?>
            </div>
	        </div>
	    </div>
    </div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><?php echo $this->Paginator->sort('PersonInCharge.first_name', 'Resp.'); ?></th>
					<th><?php echo $this->Paginator->sort('Event.code', 'Dossier'); ?></th>
					<th>Nom</th>
					<th>Client</th>
					<th>Contenu</th>
					<th>L.E.</th>
					<th>Date(s)</th>
					<th>PAX</th>
					<th>Lieu(x)</th>
					<th>Poste(s)</th>
					<th class="hidden">Suivi</th>
					<th class="hidden">Notes</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($events as $event): ?>
				<tr class="danger1">
					<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
					<td><a href="#"><?php echo $event['PersonInCharge']['first_name']; ?></a></td>
					<td><a href="#"><?php echo empty($event['Event']['code']) ? 'xxxx-xx' : $event['Event']['code']; ?></a></td>
					<td><?php echo $this->Text->truncate($event['Event']['name'], 20); ?></td>
					<td><?php echo $this->Text->truncate($event['Client']['name'], 20); ?></td>
					<td>
						<span class="btn btn-info btn-xs"><i class="fa fa-trophy"></i> BTS</span>
						<span class="btn btn-danger btn-xs"><i class="fa fa-cutlery"></i> MG</span>
						<span class="btn btn-warning btn-xs"><i class="fa fa-beer"></i> OB</span>
						<span class="btn btn-default btn-xs"><i class="fa fa-truck"></i> Test</span>
					</td>
					<td>
						<?php if(!empty($event['Event']['languages'])): $langs = explode(',', $event['Event']['languages']); ?>
							<?php foreach($langs as $lang): if(empty($lang)) continue; ?>
								<span class="flag-icon flag-icon-<?php echo ($lang == 'en') ? 'gb' : $lang; ?>"></span>
							<?php endforeach; ?>
						<?php endif; ?>
					</td>
					<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></td>
					<td>
						<?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
						<?php echo $event['Event']['confirmed_number_of_persons']; ?>
						<?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
							<?php echo $event['Event']['min_number_of_persons']; ?> -
							<?php echo $event['Event']['max_number_of_persons']; ?>
						<?php endif; ?>
					</td>
					<td>
						<?php if(!empty($event['Moment'])): $place = ''; ?>
							<?php foreach($event['Moment'] as $moment): if(empty($moment['Place'])) continue; ?>
								<?php $place .= $moment['Place']['name']; ?>
							<?php endforeach; ?>
						<?php endif; ?>
						<?php echo $this->Text->truncate($place, 20); ?>
					</td>
					<td>5 / 10</td>
					<td></td>
					<td class="hidden">67%</td>
					<td class="hidden"><a href="#" data-toggle="modal" data-target="#">Afficher</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
