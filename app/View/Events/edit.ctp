<?php $competencesSectors = Configure::read('Competences.sectors'); ?>
<?php $competencesJobs = array_merge(Configure::read('Competences.fb_jobs'), Configure::read('Competences.animation_jobs'), Configure::read('Competences.logistics_jobs')); ?>
<?php $competencesHierarchies = Configure::read('Competences.hierarchies'); ?>
<?php $optionsStatus = Configure::read('Configurations.options'); ?>
<?php $this->assign('page_title', $this->Html->link(__('Events'), array('action' => 'index', 'confirmed', $this->request->data['Company']['class']))); ?>
<?php $this->assign('page_subtitle', $this->request->data['Event']['name']);?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="icon-calendar font-blue-madison"></i>
					<span class="caption-subject font-blue-madison bold uppercase"> <?php echo $this->request->data['Event']['code_name']; ?></span>
				</div>
				<div class="actions">
					<?php echo $this->Html->link(sprintf('<i class="fa fa-stethoscope"></i> %s', __('Print needs analysis')), array('controller' => 'events', 'action' => 'tracking_sheet', $this->request->data['Event']['id'], 'standard', 'analyse', 'ext' => 'pdf'), array('target'=> '_blank', 'escape' => false, 'class' => 'btn default')); ?>
				</div>
			</div>
			<div class="portlet-body">
				<?php echo $this->Form->create('Event', array('class' => 'form-horizontal')); ?>
				<?php echo $this->Form->input('id'); ?>
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Folder number'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('code', array('label' => false, 'class' => 'form-control', 'readonly' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								<?php echo __('Opening date'); ?>
							</label>
							<div class="col-md-9">
								<?php echo $this->Form->input('opening_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16, 'value' => $this->Time->format('d-m-Y', empty($this->request->data['Event']['opening_date']) ? $this->request->data['Event']['created'] : $this->request->data['Event']['opening_date'])));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Status'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('crm_status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.crm_status'), 'empty' => true));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('event_category_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $categories));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Desired activity type'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('desired_activity_type', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.activity_types'))); ?>
							</div>
						</div>
    				<div class="form-group">
    					<label class="control-label col-md-3"><?php echo __('Event type'); ?></label>
    					<div class="col-md-9">
    						<?php echo $this->Form->input('eg_mg_type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.eg_mg_types'))); ?>
                <span class="help-block">Pour EG et MG uniquement</span>
              </div>
    				</div>
    				<div class="form-group">
    					<label class="control-label col-md-3"><?php echo __('Grills'); ?></label>
    					<div class="col-md-9">
                <div class="row">
                  <div class="col-md-3">
                    <?php echo $this->Form->input('grills_rondo', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                    <span class="help-block">Rondo</span>
                  </div>
                  <div class="col-md-3">
                    <?php echo $this->Form->input('grills_torro_l', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                    <span class="help-block">Torro L</span>
                  </div>
                  <div class="col-md-3">
                    <?php echo $this->Form->input('grills_torro_s', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                    <span class="help-block">Torro S</span>
                  </div>
                </div>
              </div>
    				</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Former outings'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('former_outings', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Person in charge'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('resp_id', array('label' => false, 'class' => 'form-control select2me', 'empty' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Needs analysis'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('needs_analysis', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.needs_analysis'))); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Client'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('client_id', array('label' => false, 'class' => 'form-control select2me client', 'empty' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-9">
								<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new client'), '#', array('class' => 'label new-client label-default', 'data-toggle' => 'modal', 'data-target' => '#new-client', 'escape' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('contact_people_id', array('label' => false, 'class' => 'form-control bs-select contact-people')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-9">
								<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new contact person'), array('#' => 'new-contact-people'), array('class' => 'label new-contact-people label-default', 'data-toggle' => 'modal', 'escape' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Language of correspondence'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('language', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.languages'), 'empty' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Language during event'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('languages', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.languages'), 'empty' => true, 'multiple' => true, 'value' => !empty($this->request->data['Event']['languages']) ? explode(',', $this->request->data['Event']['languages']):'')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3">
								<?php echo $this->request->data['Event']['crm_status'] != 'confirmed' ? __('Desired date') : __('Confirmed date'); ?>
							</label>
							<div class="col-md-9">
								<?php echo $this->Form->input('confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Potential dates'); ?></label>
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-3">
										<?php echo $this->Form->input('Date.0.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'required' => false));?>
										<?php echo $this->Form->input('Date.0.id', array('type' => 'hidden'));?>
									</div>
									<div class="col-md-3">
										<?php echo $this->Form->input('Date.1.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'required' => false));?>
										<?php echo $this->Form->input('Date.1.id', array('type' => 'hidden'));?>
									</div>
									<div class="col-md-3">
										<?php echo $this->Form->input('Date.2.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'required' => false));?>
										<?php echo $this->Form->input('Date.2.id', array('type' => 'hidden'));?>
									</div>
									<div class="col-md-3">
										<?php echo $this->Form->input('Date.3.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'required' => false));?>
										<?php echo $this->Form->input('Date.3.id', array('type' => 'hidden'));?>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Potential period'); ?></label>
							<div class="col-md-9">
								<div class="input-group input-large date-picker input-daterange">
                  <?php echo $this->Form->input('period_start', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
                  <span class="input-group-addon"> <?php echo __('to'); ?> </span>
	                  <?php echo $this->Form->input('period_end', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Start hour'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('start_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24', 'value' => empty($this->request->data['Event']['start_hour']) ? 0 : $this->request->data['Event']['start_hour']));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('End hour'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('end_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24', 'value' => empty($this->request->data['Event']['end_hour']) ? 0 : $this->request->data['Event']['end_hour']));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Objective'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('objective', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Target people'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('target_people', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Gender balance'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('gender_balance', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Age'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('age', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Number of persons'); ?></label>
							<div class="col-md-9">
								<span class="help-inline"><?php echo __('From'); ?></span>
								<?php echo $this->Form->input('min_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
								<span class="help-inline"><?php echo __('to'); ?></span>
								<?php echo $this->Form->input('max_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Confirmed number of persons'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('confirmed_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Number of persons for the offer'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('offer_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
								<span class="help-inline">Sert uniquement pour le chiffrage de l'offre</span>
							</div>
						</div>
						<div class="form-group" data-commune-select2>
							<label class="control-label col-md-3"><?php echo __('Region'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('regionZipCity', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'readonly' => true, 'options' => array($this->request->data['Event']['region_zip_city']), 'value' => $this->request->data['Event']['region_zip_city']));?>
								<?php echo $this->Form->input('region_zip', array('label' => false, 'div' => false, 'class' => 'form-control zip', 'rel' => 'return', 'data-name' => 'zip', 'type' => 'hidden')); ?>
								<?php echo $this->Form->input('region_city', array('label' => false, 'div' => false, 'class' => 'form-control city', 'rel' => 'return', 'data-name' => 'city', 'type' => 'hidden')); ?>
								<span class="help-block">Ce champ n'est plus utilisé. Veuillez utiliser le champ ci-dessous.</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Desired region'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Region.id.', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => $regions, 'multiple' => true, 'value' => $eventRegions)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Radius'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('region_radius', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline')); ?>
								<span class="help-inline">km</span>
								<?php echo $this->Form->input('region_radius_minutes', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline')); ?>
								<span class="help-inline">min.</span>
								<span class="help-block">Vous pouvez renseigner un champ ou les deux.</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Budget'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('budget', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
								<span class="help-inline">CHF / <?php echo __('person'); ?></span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Projected turnover'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('projected_turnover', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline', 'required' => true));?>
								<span class="help-inline">CHF</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Feeling'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('feeling', array('type' => 'hidden', 'value' => !empty($this->request->data['Event']['feeling']) ? $this->request->data['Event']['feeling'] : 0));?>
								<div class="row">
									<div class="col-md-4">
										<div class="noUi-control noUi-success" id="feeling"></div>
										<span class="help-block"><span id="EventFeelingSpan"></span>%</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Offer sending deadline'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('offer_sending_deadline', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control date-picker input-medium'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Relaunch date'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('relaunch_date', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control date-picker input-medium'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Knowledge'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('ubic_knowledge', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.ubic_knowledge'))); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Contact by'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('contact_method', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.contact_method'))); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<h3 class="form-section"><?php echo __('Invoicing'); ?></h3>
						<div class="form-group">
							<div class="col-md-9 col-md-offset-3">
								<span class="help-block">Ces champs sont à remplir s'ils sont différents des informations du client.</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Business name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_business_name', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('PO'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_po', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_address', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group" data-commune-select2>
							<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array($this->request->data['Event']['invoice_zip_city']), 'value' => empty($this->request->data['Event']['invoice_zip_city']) ? '' : $this->request->data['Event']['invoice_zip_city']));?>
								<?php echo $this->Form->input('invoice_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
								<?php echo $this->Form->input('invoice_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
								<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue editing'); ?></button>
								<button type="submit" class="btn blue" value="work" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and work on event'); ?></button>
							</div>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>
				<!-- END FORM-->
			</div>
		</div>
	</div>
</div>

<?php if(false): ?>
<div class="tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#moments" data-toggle="tab"><i class="fa fa-clock-o"></i> <?php echo __('Moments'); ?></a>
		</li>
		<li>
			<a href="#options" data-toggle="tab"><i class="fa fa-check-square-o"></i> <?php echo __('Options'); ?></a>
		</li>
		<li>
			<a href="#jobs" data-toggle="tab"><i class="fa fa-flag"></i> <?php echo __('Jobs'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
			<!-- BEGIN FORM-->

		</div>
		<div class="tab-pane" id="moments" style="padding-bottom: 100px">
			<div id="moments1">
				<?php if(!empty($this->request->data['Moment'])): ?>
				<?php foreach($this->request->data['Moment'] as $k => $moment): ?>
				<div class="portlet light bg-grey-cararra moment">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="name"><?php echo $moment['name']; ?></span></strong>
							<span class="start_hour"><?php echo $this->Time->format($moment['start_hour'], '%H:%M'); ?></span>
							<span class="end_hour"><?php echo $this->Time->format($moment['end_hour'], '%H:%M'); ?></span>
						</div>
						<div class="actions">
							<a href="#" class="btn btn-circle btn-default remove-moment"><i class="fa fa-trash-o"></i> <?php echo __('Remove this moment');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group row">
							<div class="col-md-4">
								<?php echo $this->Form->input('Moment.id.', array('type' => 'hidden', 'class' => 'id', 'value' => $moment['id'])); ?>
								<?php echo $this->Form->input('Moment.event_id.', array('type' => 'hidden', 'value' => $this->request->data['Event']['id'], 'class' => 'event_id')) ; ?>
								<?php echo $this->Form->input('Moment.name.', array('class' => 'form-control name', 'label' => false, 'placeholder' => __('Name'), 'value' => $moment['name'])); ?>
							</div>
							<div class="col-md-2">
								<div class="input-group">
									<?php echo $this->Form->input('Moment.start_hour.', array('class' => 'form-control timepicker timepicker-24 start_hour', 'label' => false, 'div' => false, 'value' => $this->Time->format($moment['start_hour'], '%H:%M'))); ?>
								</div>
							</div>
							<div class="col-md-2">
								<div class="input-group">
									<?php echo $this->Form->input('Moment.end_hour.', array('class' => 'form-control timepicker timepicker-24 end_hour', 'label' => false, 'div' => false, 'value' => $this->Time->format($moment['end_hour'], '%H:%M'))); ?>
								</div>
							</div>
							<div class="col-md-4">
								<?php echo $this->Form->input('Moment.place_id.', array('type' => 'hidden', 'class' => 'place_id', 'value' => $moment['place_id'])); ?>
								<?php echo $this->Form->input('Moment.places.', array('class' => 'form-control places select2me', 'label' => false, 'options' => $places, 'empty' => __('Select a place'), 'value' => $moment['place_id'])); ?>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-10">
								<?php echo $this->Form->input('Moment.remarks', array('class' => 'form-control remarks', 'label' => false, 'placeholder' => __('Remarks'), 'type' => 'text', 'value' => $moment['remarks'])); ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Html->link('<i class="fa fa-map-marker"></i> ' . __('Search for a place'), array('controller' => 'events', 'action' => 'searchPlace', $this->request->data['Event']['id']), array('class' => 'btn default btn-block', 'escape' => false, 'target' => '_blank')); ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="portlet light bg-grey-cararra moment empty">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="name"></span></strong>
							<span class="start_hour"></span>
							<span class="end_hour"></span>
						</div>
						<div class="actions">
							<a href="" class="btn btn-circle btn-default add-moment"><i class="fa fa-plus"></i> <?php echo __('Add a moment');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group row">
							<div class="col-md-4">
								<?php echo $this->Form->input('Moment.id.', array('type' => 'hidden', 'class' => 'id')); ?>
								<?php echo $this->Form->input('Moment.event_id.', array('type' => 'hidden', 'value' => $this->request->data['Event']['id'], 'class' => 'event_id')) ; ?>
								<?php echo $this->Form->input('Moment.name.', array('class' => 'form-control name', 'label' => false, 'placeholder' => __('Name'))); ?>
							</div>
							<div class="col-md-2">
								<div class="input-group">
									<?php echo $this->Form->input('Moment.start_hour.', array('class' => 'form-control timepicker timepicker-24 start_hour', 'label' => false, 'div' => false)); ?>
								</div>
							</div>
							<div class="col-md-2">
								<div class="input-group">
									<?php echo $this->Form->input('Moment.end_hour.', array('class' => 'form-control timepicker timepicker-24 end_hour', 'label' => false, 'div' => false)); ?>
								</div>
							</div>
							<div class="col-md-4">
								<?php echo $this->Form->input('Moment.place_id.', array('type' => 'hidden', 'class' => 'place_id')); ?>
								<?php echo $this->Form->input('Moment.places.', array('class' => 'form-control places select2me', 'label' => false, 'options' => $places, 'empty' => __('Select a place'))); ?>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<?php echo $this->Form->input('Moment.remarks', array('class' => 'form-control remarks', 'label' => false, 'placeholder' => __('Remarks'), 'type' => 'text')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="jobs" style="padding-bottom: 100px">
			<div id="elements">
				<?php if(!empty($this->request->data['Job'])): ?>
				<?php foreach($this->request->data['Job'] as $k => $job): ?>
				<div class="portlet light bg-grey-cararra element">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="job"><?php echo $competencesJobs[$job['job']]; ?></span></strong>
							<span class="hierarchy"><?php echo $competencesHierarchies[$job['hierarchy']]; ?></span>
							<span class="activity"><?php echo !empty($job['activity_id']) ? $activities['all'][$job['activity_id']] : ''; ?></span>
						</div>
						<div class="actions">
							<a href="" class="btn btn-circle btn-default remove-element"><i class="fa fa-trash-o"></i> <?php echo __('Remove this job');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<div class="row">
								<div class="col-md-2">
									<div class="input-group">
										<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
										</span>
										<?php echo $this->Form->input('Job.start_time.', array('class' => 'form-control timepicker timepicker-24 start_time', 'label' => false, 'div' => false, 'value' => empty($job['start_time']) ? '' : $job['start_time'])); ?>
									</div>
								</div>
								<div class="col-md-2">
									<div class="input-group">
										<span class="input-group-addon">
										<i class="fa fa-clock-o"></i>
										</span>
										<?php echo $this->Form->input('Job.end_time.', array('class' => 'form-control timepicker timepicker-24 end_time', 'label' => false, 'div' => false, 'value' => empty($job['end_time']) ? '' : $job['end_time'])); ?>
									</div>
								</div>
								<div class="col-md-2">
									<?php echo $this->Form->input('Job.model.', array('type' => 'hidden', 'value' => 'event', 'class' => 'model')) ; ?>
									<?php echo $this->Form->input('Job.event_id.', array('type' => 'hidden', 'value' => $this->request->data['Event']['id'], 'class' => 'event_id')) ; ?>
									<?php echo $this->Form->input('Job.id.', array('type' => 'hidden', 'class' => 'id', 'value' => $job['id'], 'id' => '')); ?>
									<?php echo $this->Form->input('Job.sector.', array('class' => 'form-control sector bs-select', 'label' => false, 'options' => Configure::read('Competences.sectors'), 'empty' => __('Select a sector'), 'value' => $job['sector'], 'id' => '')); ?>
								</div>
								<div class="col-md-2">
									<?php if(array_key_exists($job['job'], Configure::read('Competences.fb_jobs'))): ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control fb_jobs jobs bs-select', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'), 'value' => $job['job'], 'id' => '')); ?>
									<?php else: ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control fb_jobs jobs hidden bs-select', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
									<?php endif ?>
									<?php if(array_key_exists($job['job'], Configure::read('Competences.logistics_jobs'))): ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control logistics_jobs jobs bs-select', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'), 'value' => $job['job'], 'id' => '')); ?>
									<?php else: ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control logistics_jobs jobs hidden bs-select', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
									<?php endif; ?>
									<?php if(array_key_exists($job['job'], Configure::read('Competences.animation_jobs'))): ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control animation_jobs jobs bs-select', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'), 'value' => $job['job'], 'id' => '')); ?>
									<?php else: ?>
									<?php echo $this->Form->input('Job.job.', array('class' => 'form-control animation_jobs jobs hidden bs-select', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
									<?php endif; ?>
								</div>
								<?php if(array_key_exists($job['job'], Configure::read('Competences.animation_jobs'))): ?>
								<div class="col-md-2 activities">
								<?php else: ?>
								<div class="col-md-2 activities hidden">
								<?php endif; ?>
									<?php if(array_key_exists($job['activity_id'], $activities['ubic'])): ?>
									<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control ubicActivities bs-select', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'), 'value' => $job['activity_id'], 'id' => '')); ?>
									<?php else: ?>
									<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control ubicActivities hidden bs-select', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'), 'id' => '')); ?>
									<?php endif; ?>
									<?php if(array_key_exists($job['activity_id'], $activities['ug'])): ?>
									<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control ugActivities bs-select', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'), 'value' => $job['activity_id'], 'id' => '')); ?>
									<?php else: ?>
									<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control ugActivities hidden bs-select', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'), 'id' => '')); ?>
									<?php endif; ?>
								</div>
								<div class="col-md-2">
									<?php echo $this->Form->input('Job.hierarchy.', array('class' => 'form-control hierarchies bs-select', 'label' => false, 'options' => Configure::read('Competences.hierarchies'), 'empty' => __('Select a hierarchy'), 'value' => $job['hierarchy'], 'id' => '')); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<div class="input-group">
										<span class="input-group-addon">
										<i class="fa fa-user"></i>
										</span>
										<?php echo $this->Form->input('Job.user_id.', array('class' => 'form-control select2me user_id', 'label' => false, 'options' => $possibleWorkers, 'empty' => true, 'div' => false, 'value' => empty($job['user_id']) ? '' : $job['user_id'], 'placeholder' => __('Committed staff') )); ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="input-group">
										<span class="input-group-addon">
										<i class="icon-bubbles"></i>
										</span>
										<?php echo $this->Form->input('Job.languages.', array('class' => 'form-control bs-select languages', 'label' => false, 'value' => !empty($job['languages']) ? explode(',', $job['languages']) : '', 'placeholder' => __('Desired language'), 'options' => Configure::read('Jobs.languages'), 'empty' => __('Desired language'), 'multiple' => true )); ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="input-inline input-small">
										<div class="input-group">
											<span class="input-group-addon">
											<i class="fa fa-money"></i>
											</span>
											<?php echo $this->Form->input('Job.salary.', array('class' => 'form-control salaries', 'label' => false, 'value' => !empty($job['salary']) ? $job['salary'] : '')); ?>
										</div>
									</div>
									<span class="help-inline">CHF / h</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->Form->input('Job.remarks.', array('type' => 'text', 'class' => 'form-control remarks', 'label' => false, 'value' => !empty($job['remarks']) ? $job['remarks'] : '', 'placeholder' => __('Remarks') )); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="portlet light bg-grey-cararra element empty">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="job"></span></strong>
							<span class="hierarchy"></span>
							<span class="activity"></span>
						</div>
						<div class="actions">
							<a href="" class="btn btn-circle btn-default add-element"><i class="fa fa-plus"></i> <?php echo __('Add a job');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group row">
							<?php echo $this->Form->input('Job.id.', array('type' => 'hidden', 'class' => 'id')) ; ?>
							<?php echo $this->Form->input('Job.model.', array('type' => 'hidden', 'value' => 'event', 'class' => 'model')) ; ?>
							<?php echo $this->Form->input('Job.event_id.', array('type' => 'hidden', 'value' => $this->request->data['Event']['id'], 'class' => 'event_id')) ; ?>
							<div class="col-md-1">
								<div class="input-group">
									<?php echo $this->Form->input('Job.start_time.', array('class' => 'form-control timepicker timepicker-24 start_time', 'label' => false, 'div' => false)); ?>
								</div>
							</div>
							<div class="col-md-1">
								<div class="input-group">
									<?php echo $this->Form->input('Job.end_time.', array('class' => 'form-control timepicker timepicker-24 end_time', 'label' => false, 'div' => false)); ?>
								</div>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('Job.id.', array('type' => 'hidden', 'class' => 'id')); ?>
								<?php echo $this->Form->input('Job.sector.', array('class' => 'form-control sector', 'label' => false, 'options' => Configure::read('Competences.sectors'), 'empty' => __('Select a sector'))); ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('Job.job.', array('class' => 'form-control hidden fb_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'))); ?>
								<?php echo $this->Form->input('Job.job.', array('class' => 'form-control hidden logistics_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'))); ?>
								<?php echo $this->Form->input('Job.job.', array('class' => 'form-control hidden animation_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'))); ?>
							</div>
							<div class="col-md-2 activities hidden">
								<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control hidden ubicActivities', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'))); ?>
								<?php echo $this->Form->input('Job.activity.', array('class' => 'form-control hidden ugActivities', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'))); ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('Job.hierarchy.', array('class' => 'form-control hidden hierarchies', 'label' => false, 'options' => Configure::read('Competences.hierarchies'), 'empty' => __('Select a hierarchy'))); ?>
							</div>
							<div class="col-md-2">
								<?php echo $this->Form->input('Job.salary.', array('class' => 'form-control salaries hidden disabled', 'label' => false, 'div' => false, 'placeholder' => 'CHF')); ?>
								<span class="help-block salaries hidden">CHF / h</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="options">
			<div class="portlet light" style="padding-bottom: 150px">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-check-square-o"></i><?php echo __('Options'); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table">
						<table class="table table-striped table-bordered table-hover" id="options1">
						<thead>
							<tr>
								<th><?php echo __('Moment'); ?></th>
								<th><?php echo __('Time'); ?></th>
								<th><?php echo __('Place'); ?></th>
								<th><?php echo __('Collaborator'); ?></th>
								<th><?php echo __('Value'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($this->request->data['Moment'] as $key => $moment): //debug($moment)?>
							<?php foreach($moment['Option'] as $option): ?>
								<tr data-option-id="<?php echo $option['id']; ?>">
									<td><?php echo $moment['name']; ?></td>
									<td>
										<?php echo __('From'); ?>
										<?php echo $this->Time->format($moment['start_hour'], '%H:%M'); ?>
										<?php echo __('to'); ?>
										<?php echo $this->Time->format($moment['end_hour'], '%H:%M'); ?>
									</td>
									<td><?php echo $option['Place']['name']; ?></td>
									<td><?php echo $option['User']['full_name']; ?></td>
									<td>
										<?php echo $this->Form->input('option', array('class' => 'form-control bs-select update-option', 'options' => $optionsStatus, 'label' => false, 'div' => false, 'value' => $option['value'])); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<div class="modal fade" id="new-client" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('Client', array('controller' => 'clients', 'action' => 'add')); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new client / contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Client'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('Business name'); ?></label>
							<?php echo $this->Form->input('Client.name', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('Client.companies', array('class' => 'form-control bs-select', 'label' => false, 'options' => $companies, 'value' => 2)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Address'); ?></label>
							<?php echo $this->Form->input('Client.address', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('ZIP'); ?></label>
							<?php echo $this->Form->input('Client.zip', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('City'); ?></label>
							<?php echo $this->Form->input('Client.city', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Country'); ?></label>
							<?php echo $this->Form->input('Client.country', array('class' => 'form-control bs-select', 'options' => Configure::read('Countries'), 'value' => 'CH', 'data-live-search' => true, 'label' => false)); ?>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.0.status', array('type' => 'hidden', 'value' => 1)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="new-contact-people" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('ContactPeople', array('controller' => 'contact_peoples', 'action' => 'add')); ?>
	<?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 1)); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.client_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('ContactPeople.status', array('type' => 'hidden', 'value' => 1)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/nouislider/jquery.nouislider.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/nouislider/jquery.nouislider.all.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
