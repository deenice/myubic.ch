<?php

$options = Configure::read('Places.options');

foreach ($dtResults as $result) {

	$hours = __('From') . ' ' . $this->Time->format($result['Moment']['start_hour'], '%H:%M') . ' ' . __('to') . ' ' . $this->Time->format($result['Moment']['end_hour'], '%H:%M');
	$eventName = '';
	if(!empty($result['Event']['code'])){
		$eventName .= $this->Html->tag('strong', $result['Event']['code']) . $this->Html->tag('br');
	}
	$eventName .= $result['Event']['name'];
	$eventName  .= $this->Html->tag('br');
	$eventName  .= $result['Client']['name'];

	$actions = $this->Html->link(
		'<i class="fa fa-edit"></i> ' .  __("Edit"), 
		array('controller' => 'events', 'action' => 'edit', $result['Event']['id'], '#' => 'options'), 
		array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);
	

    $this->dtResponse['aaData'][] = array(
        __($options[$result['Option']['value']]),
        $eventName,
        $this->Time->format($result['Option']['date'], '%d %b %Y'),
        $result['Moment']['name'],
        $hours,
        $result['User']['first_name'] . ' ' . $result['User']['last_name'],
        $actions
    );
}