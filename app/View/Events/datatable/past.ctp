<?php
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
		    '<i class="fa fa-search"></i> ' . __("View"),
		    array('controller' => 'events', 'action' => 'view', $result['Event']['id']),
		    array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
		);
	$actions .= $this->Html->tag('br');
	$actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'events', 'action' => 'edit', $result['Event']['id']),
	    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
	    );
	$actions .= $this->Html->tag('br');
	$actions .= $this->Html->link(
		'<i class="fa fa-money"></i> ' . __('Edit client'),
		array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
		array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
	);

	$eventName = empty($result['Event']['code']) ? '' : $this->Html->tag('strong', $result['Event']['code']) . $this->Html->tag('br');
	$eventName .= empty($result['Event']['name']) ? '' : $result['Event']['name'];

	if(!empty($result['Event']['remarks'])){
		$icon = $this->Html->tag('i', '', array('class' => 'fa fa-warning'));
		$p = $this->Html->tag('p', $icon . ' ' . $result['Event']['remarks'], array('class' => 'text-muted', 'style' => 'padding-top: 5px'));
		$eventName .= $p;
	}

	$persons = '';
	if(is_null($result['Event']['confirmed_number_of_persons'])){
		if(!is_null($result['Event']['min_number_of_persons']) && !is_null($result['Event']['max_number_of_persons'])){
			$persons .= __('From') . ' ' . $result['Event']['min_number_of_persons'] . ' ' . __('to') . ' ' . $result['Event']['max_number_of_persons'] . ' ' . __('persons');
		} elseif(is_null($result['Event']['min_number_of_persons']) && !is_null($result['Event']['max_number_of_persons'])){
			$persons .= $result['Event']['max_number_of_persons'] . ' ' . __('persons') . ' ' . __('maximum');
		} elseif(!is_null($result['Event']['min_number_of_persons']) && is_null($result['Event']['max_number_of_persons'])){
			$persons .= $result['Event']['min_number_of_persons'] . ' ' . __('persons') . ' ' . __('minimum');
		}
	} elseif($result['Event']['confirmed_number_of_persons'] > 0) {
		$persons = $result['Event']['confirmed_number_of_persons'] . ' ' . __('persons');
	}

    $this->dtResponse['aaData'][] = array(
        $eventName,
        $result['Client']['name'],
        $this->Time->format($result['Event']['confirmed_date'], '%A %d %b %Y'),
        $persons,
        $actions
    );
}
