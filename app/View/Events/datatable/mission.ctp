<?php

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'missions', 'action' => 'edit', $result['Mission']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $schedule = '';
  if(!empty($result['Mission']['start_hour'])){
    $schedule .= $this->Time->format('H:i', $result['Mission']['start_hour']);
    if(!empty($result['Mission']['end_hour'])){
      $schedule .= ' - ' . $this->Time->format('H:i', $result['Mission']['end_hour']);
    }
  }

  $this->dtResponse['aaData'][] = array(
    $result['Mission']['name'],
    $this->Time->format('d.m.Y', $result['Mission']['confirmed_date']),
    $schedule,
    $result['Company']['name'],
    $actions
  );
}
