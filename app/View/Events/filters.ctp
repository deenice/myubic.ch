<?php if(!empty($events)): ?>
<?php foreach($events as $event): ?>
	<li>
		<label>
			<input type="checkbox" checked="checked" name="name" value="<?php echo $event['Event']['id']; ?>" data-event-id="">
			<span class="label label-info label-sm" style="background-color: <?php echo $event['Event']['calendar_color']; ?>"><?php echo $event['Event']['name']; ?></span>
		</label>
	</li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($missions)): ?>
<?php foreach($missions as $mission): ?>
	<li>
		<label>
			<input type="checkbox" checked="checked" name="name" value="<?php echo $mission['Mission']['id']; ?>" data-event-id="">
			<span class="label label-info label-sm mission"><i class="fa fa-crosshairs"></i> <?php echo $mission['Mission']['name']; ?></span>
		</label>
	</li>
<?php endforeach; ?>
<?php endif; ?>
