<?php $this->Paginator->options(array(
'update' => '#content', // Ca c'est l'id de la div ou sera afficher le contenu de tes pages tu le trouve dans la vue default.ctp sous le repertoire app/view/layout
'evalScripts' => true // ca c'est pour dire que si dans le code de retour ou la page de retour chargé par ajax il y a un script, est ce qu'il doit l'evaluer/executer ou non
)); ?>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('id','id'); ?></th>
            <th><?php echo $this->Paginator->sort('name','nom'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data as $item): ?>
        <tr>
            <td><?php echo h($item['Event']['id']); ?> </td>
            <td><?php echo h($item['Event']['name']); ?> </td>
        </tr>
            <?php endforeach; ?>
    </tbody>


    </table>
    <div class="alert alert-info">
    <?php
    echo $this->Paginator->counter(array(
    'format' => __('Page <strong>{:page}</strong> sur<strong>{:pages}</strong>, affichage de <strong>{:current}</strong> sur un total de <strong>{:count}</strong>, premier <strong>{:start}</strong>, Dernier <strong>{:end}</strong>')
    ));
    ?>  </div>
    <ul class="pagination">
    <?php
        echo $this->Paginator->prev('<i class="fa fa-angle-left"></i>', array('tag'=>'li', 'escape' => false), null, array('tag'=>'li', 'class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
        echo $this->Paginator->next('>', array('tag'=>'li'), null, array('tag'=>'a','class' => 'disabled'));
    ?>
</ul>
