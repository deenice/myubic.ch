<?php $this->assign('page_title', $this->Html->link($page['title'], $page['url'])); ?>
<?php $this->assign('page_subtitle', empty($event['Event']['code_name']) ? $event['Event']['name'] : $event['Event']['code_name']);?>
<?php $statuses = Configure::read('Events.status'); ?>
<?php $types = Configure::read('Events.activity_types'); ?>
<?php $sectors = Configure::read('Competences.sectors'); ?>
<?php echo $this->Form->input('EventId', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>
<div class="row">
  <div class="col-md-4">
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-client">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Client'); ?></span>
        </div>
        <div class="actions">
          <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit client')), array('controller' => 'clients', 'action' => 'edit', $event['Client']['id']), array('target' => '_blank', 'escape' => false, 'class' => 'btn btn-sm btn-default')); ?>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'client', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <div class="portlet light bordered" id="portlet-tasks">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Tasks'); ?></span>
        </div>
        <div class="actions">
          <?php echo $this->Form->input('checklists', array('label' => false, 'empty' => __('Insert a checklist'), 'class' => 'bs-select form-control input-medium insert-checklist', 'options' => $checklists, 'data-live-search' => true)); ?>
        </div>
        <div class="tools">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'tasks', $event['Event']['id'])); ?>" class="reload hidden"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <div class="portlet light bordered" id="portlet-infos" data-portlet-editable data-portlet-name="infos">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Informations'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'infos', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
        <div class="actions">
          <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit basic informations')), array('controller' => $mission ? 'missions' : 'events', 'action' => 'edit', $event['Event']['id']), array('class' => 'btn btn-xs btn-default', 'escape' => false)); ?>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php if(!$mission): ?>
    <div class="portlet light bordered hidden" id="portlet-weather">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Weather'); ?></span>
          <span class="label label-warning">En développement</span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'weather', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered hidden" id="portlet-client-transport">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Transport client'); ?></span>
          <span class="label label-warning">En développement</span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'client_transport', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered hidden" id="portlet-plus">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Les plus'); ?></span>
          <span class="label label-warning">En développement</span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'plus', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-invoicing" data-portlet-editable data-portlet-name="invoicing">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Invoicing'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'invoicing', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-accounting" data-portlet-editable data-portlet-name="accounting">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Accounting'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'accounting', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <div class="portlet light bordered" id="portlet-closing">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Cloture'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'closing', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="portlet light bordered" id="portlet-downloads">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Downloadable documents'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'downloads', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <div class="portlet light bordered" id="portlet-notes" data-portlet-notes>
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Notes'); ?></span>
        </div>
        <div class="tools">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" class="reload hidden"> </a>
        </div>
        <div class="actions">
          <a href="#" class="btn btn-default" data-target="#new-note" data-toggle="modal" data-model="Event" data-model-id="<?php echo $event['Event']['id']; ?>">
            <i class="fa fa-plus"></i>
            <?php echo __('Add a note'); ?>
          </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php if(!in_array($event['Company']['id'], array(4,5)) && !$mission): ?>
    <div class="portlet light bordered" id="portlet-activities">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Activities'); ?></span>
        </div>
        <div class="actions">
          <?php echo $this->Form->input('activities', array('label' => false, 'empty' => __('Add an activity'), 'class' => 'bs-select form-control input-large', 'options' => $activities, 'data-live-search' => true)); ?>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'activities', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-fb-modules">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('F&amp;B'); ?></span>
        </div>
        <div class="actions">
          <?php echo $this->Form->input('fb_modules', array('label' => false, 'empty' => __('Add a F&B module'), 'class' => 'bs-select form-control input-large', 'options' => $fb_modules, 'data-live-search' => true)); ?>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'fb_modules', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-places">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Places'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'places', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
        <div class="actions">
          <a href="javascript:;" class="btn btn-default hidden"><i class="fa fa-search"></i> Sélection par critères</a>
          <a href="javascript:;" class="btn btn-default hidden"><i class="fa fa-print"></i> Imprimer fiche de travail</a>
          <a href="javascript:;" class="btn btn-default hidden"><i class="fa fa-download"></i> Exporter fichier Excel</a>
          <?php echo $this->Html->link(sprintf('<i class="fa fa-search"></i> %s', __('Search')), array('controller' => 'search', 'action' => 'places'), array('target' => '_blank', 'escape' => false, 'class' => 'btn btn-default')); ?>
          <?php echo $this->Form->input('place', array('label' => false, 'empty' => __('Manual attribution'), 'class' => 'form-control input-large', 'options' => array())); ?>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <div class="portlet light bordered" id="portlet-recruiting">
        <div class="portlet-title">
          <div class="caption">
            <span class="caption-subject bold uppercase"><?php echo __('Recrutement'); ?></span>
          </div>
          <div class="actions">
            <?php echo $this->Html->link(sprintf('<i class="fa fa-plus"></i> %s', __('Add a job')), array('controller' => 'jobs', 'action' => 'modal', 0, $event['Event']['id']), array('escape' => false, 'class' => 'btn btn-default', 'data-target' => '#modal-add-job', 'data-toggle' => 'modal')); ?>
            <?php echo $this->Html->link(sprintf('<i class="fa fa-plus"></i> %s', __('Add a briefing')), array('controller' => 'job_briefings', 'action' => 'modal', 0, $event['Event']['id']), array('escape' => false, 'class' => 'btn btn-default', 'data-target' => '#modal-add-job-briefing', 'data-toggle' => 'modal')); ?>
            <a href="#" class="btn btn-default insert-hours"><i class="fa fa-clock-o"></i> <?php echo __('Insert hours') ?></a>
          </div>
          <div class="tools hidden">
            <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'recruiting', $event['Event']['id'])); ?>" class="reload"> </a>
          </div>
        </div>
        <div class="portlet-body"></div>
    </div>
    <div class="portlet light bordered <?php echo $event['Event']['confirmed_date'] <= date('Y-m-d') ? '':'hidden'; ?>" id="portlet-hours">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Hours'); ?></span>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'hours', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-planning">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Planning'); ?></span>
        </div>
        <div class="actions">
          
          <a class="btn btn-info" data-toggle="modal" href='#modal-id'><i class="fa fa-info-circle"></i> Infos utiles</a>
          <div class="modal fade" id="modal-id">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Infos utiles</h4>
                </div>
                <div class="modal-body">
                  <ul>
                    <li>Le panneau <b>Général</b> est le plus important, les autres ne sont pas obligatoires.</li>
                    <li>Tout moment ajouté sera également ajouté dans la planche <b>Général</b>.</li>
                    <li>Dans la planche <b>Panneau d'affichage</b>, vous pouvez mettre les moments clefs de l'évènement. Ils seront repris lors de la génération du PDF Panneau d'affichage.</li>
                    <li>Chaque moment associé au <b>Client</b> sera repris dans l'info pratique et la confirmation d'engagement.</li>
                    <li>Chaque moment associé à un <b>Extra</b> sera repris dans son planning personnel dans sa confirmation d'engagement.</li>
                    <li>Il est judicieux d'ajouter le <b>Lieu</b> aux moments. Il sera repris dans les différents documents téléchargeables.</li>
                    <li>Si vous supprimez une <b>Activité</b> de l'événement, les moments associés à cette dernière seront effacés!</li>
                    <li>Si vous supprimez un <b>Concept F&amp;B</b> de l'événement, les moments associés à ce dernier seront effacés!</li>
                    <li>L'annulation d'un <b>Véhicule</b> supprimera la resource liée au moment. Le moment concerné reste.</li>
                    <li>L'annulation d'un <b>Extra</b> supprimera la resource liée au moment. Le moment concerné reste.</li>
                    <li>L'annulation d'un <b>Lieu</b> supprimera la resource liée au moment. Le moment concerné reste.</li>
                    <li>Pour le covoiturage, il vous suffit d'ajouter un ou plusieurs moment(s) de type <em>Départ covoiturage</em>.</li>
                  </ul>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
              </div>
            </div>
          </div>
          
          <a href="<?php echo Router::url(array('controller' => 'planning_boards', 'action' => 'add', $event['Event']['id'], 'event')); ?>" class="btn btn-default" data-toggle="modal" data-target="#modal-planning-board">
            <i class="fa fa-plus"></i> <?php echo __('Add a board'); ?> </a>
          <a class="btn btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
        <div class="tools">
          <a class="reload hidden" href="#" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'planning', $event['Event']['id'])); ?>"> </a>
        </div>
        <?php echo $this->Form->input('planning_active_board', array('type' => 'hidden', 'value' => '')); ?>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php endif; ?>
    <div class="portlet light bordered" id="portlet-vehicles">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Vehicles'); ?></span>
        </div>
        <div class="inputs">
          <div class="portlet-input input-large input-inline">
            <div class="input-group">
              <?php echo $this->Form->input('vehicle_families', array('label' => false, 'empty' => __('Ask for a vehicle/trailer'), 'class' => 'bs-select form-control input-large', 'options' => Configure::read('VehiclesTrailers.families'), 'data-live-search' => true)); ?>
            </div>
          </div>
        </div>
        <div class="tools hidden">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'vehicles', $event['Event']['id'])); ?>" class="reload"> </a>
        </div>
      </div>
      <div class="portlet-body"></div>
    </div>
    <?php if(!$mission): ?>
    <div class="portlet light bordered" id="portlet-orders">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('Orders'); ?></span>
        </div>
        <div class="actions">
          <div class="dropdown pull-right" style="margin-left: 10px;">
            <a class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:;"> <?php echo __('Actions'); ?>
              <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
              <li>
                <?php echo $this->Html->link(__('Control sheet'), array('controller' => 'events', 'action' => 'orders', $event['Event']['id'], 'order', 'ext' => 'pdf'), array('target' => '_blank', 'escape' => false)); ?>
              </li>
              <li>
                <?php echo $this->Html->link(__('Preparation sheet'), array('controller' => 'events', 'action' => 'orders', $event['Event']['id'], 'locations', 'order', 'ext' => 'pdf'), array('target' => '_blank', 'escape' => false)); ?>
              </li>
              <li>
                <?php echo $this->Html->link(__('Supplier sheet'), array('controller' => 'events', 'action' => 'orders', $event['Event']['id'], 'suppliers', 'order', 'ext' => 'pdf'), array('target' => '_blank', 'escape' => false)); ?>
              </li>
              <li role="separator" class="divider"></li>
              <li>
                <?php echo $this->Html->link(__('Notify suppliers'), '#', array('target' => '_blank', 'escape' => false)); ?>
              </li>
            </ul>
            <a class="btn btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
          </div>
        </div>
        <div class="inputs">
          <div class="portlet-input input-large input-inline">
            <div class="input-group">
              <?php echo $this->Form->input('orders', array('label' => false, 'div' => false, 'empty' => __('Insert a order model'), 'class' => 'form-control bs-select input-large insert-order', 'options' => $orders, 'data-live-search' => true)); ?>
            </div>
          </div>
        </div>
        <div class="tools">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'events', 'action' => 'portlet', 'orders', $event['Event']['id'])); ?>" class="reload hidden"> </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="row">
          <div class="col-md-12">

          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>



<div class="modal modal-job" id="add-job" data-edit="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <?php echo $this->Form->create('Job', array('class' => 'form-horizontal form-job', 'url' => array('controller' => 'jobs', 'action' => 'edit'))); ?>
      <?php echo $this->Form->input('Job.event_id', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo __('Add a job'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.start_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker-24 input-inline input-small'));?>
            <?php echo $this->Form->input('Job.end_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker-24 input-inline input-small'));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Staff amount'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.staff_needed', array('label' => false, 'class' => 'form-control input-small', 'type' => 'number', 'value' => 1, 'min' => 1));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Sector'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.sector', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Competences.sectors'), 'empty' => true));?>
          </div>
        </div>
        <div class="form-group group-jobs hide-on-add">
          <label class="control-label col-md-3"><?php echo __('Job'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.job1', array('label' => false, 'class' => 'form-control bs-select input-jobs input-fb-jobs hide-on-add', 'options' => Configure::read('Competences.fb_jobs'), 'empty' => true));?>
            <?php echo $this->Form->input('Job.job2', array('label' => false, 'class' => 'form-control bs-select input-jobs input-logistics-jobs hide-on-add', 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => true));?>
            <?php echo $this->Form->input('Job.job3', array('label' => false, 'class' => 'form-control bs-select input-jobs input-animation-jobs hide-on-add', 'options' => Configure::read('Competences.animation_jobs'), 'empty' => true));?>
            <?php echo $this->Form->input('Job.job4', array('label' => false, 'class' => 'form-control bs-select input-jobs input-other-jobs hide-on-add', 'options' => Configure::read('Competences.other_jobs'), 'empty' => true));?>
          </div>
        </div>
        <div class="form-group group-activities hide-on-add">
          <label class="control-label col-md-3"><?php echo __('Activity'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.activity_id', array('label' => false, 'class' => 'form-control bs-select input-activities', 'options' => $activities, 'empty' => true, 'data-live-search' => true));?>
          </div>
        </div>
        <div class="form-group group-fb-modules hide-on-add">
          <label class="control-label col-md-3"><?php echo __('F&B Module'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.fb_module_id', array('label' => false, 'class' => 'form-control bs-select input-fb-modules', 'options' => $fbmodules, 'empty' => true, 'data-live-search' => true));?>
          </div>
        </div>
        <div class="form-group group-activities hidden">
          <label class="control-label col-md-3"><?php echo __('Free field'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.free', array('label' => false, 'class' => 'form-control'));?>
          </div>
        </div>
        <div class="form-group group-hierarchies">
          <label class="control-label col-md-3"><?php echo __('Hierarchy'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.hierarchy', array('label' => false, 'class' => 'form-control bs-select input-hierarchies', 'options' => Configure::read('Competences.hierarchies'), 'empty' => true));?>
          </div>
        </div>
        <div class="form-group group-languages">
          <label class="control-label col-md-3"><?php echo __('Language'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.languages', array('label' => false, 'class' => 'form-control bs-select input-languages', 'multiple' => true, 'value' => !empty($event['Event']['languages']) ? explode(',', $event['Event']['languages']) : '', 'options' => Configure::read('Jobs.languages'), 'empty' => true, 'mulitple' => true));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Staff'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.user_ids', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => $users, 'empty' => true, 'multiple' => true));?>
            <span class="help-block">Vous pouvez sélectionner directement un extra dans la liste déroulante.</span>
          </div>
        </div>
        <div class="form-group group-remarks">
          <label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.remarks', array('label' => false, 'class' => 'form-control input-remarks'));?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>

<?php if(false): ?>
<?php foreach($jobs as $sector => $jobss): ?>
<?php foreach($jobss as $job): ?>
<div class="modal modal-job" id="edit-job<?php echo $job['id']; ?>" data-edit="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <?php echo $this->Form->create('Job', array('id' => 'form' . $job['id'], 'class' => 'form-horizontal form-job', 'url' => array('controller' => 'jobs', 'action' => 'edit'))); ?>
      <?php echo $this->Form->input('Job.id', array('type' => 'hidden', 'value' => $job['id'])); ?>
      <?php echo $this->Form->input('Job.event_id', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo __('Edit a job'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.start_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($job['start_time']) ? '' : $job['start_time']));?>
            <?php echo $this->Form->input('Job.end_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($job['end_time']) ? '' : $job['end_time']));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Staff amount'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.staff_needed', array('label' => false, 'class' => 'form-control input-small', 'type' => 'number', 'value' => $job['staff_needed'], 'min' => 1));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Sector'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.sector', array('label' => false, 'class' => 'form-control bs-select input-sector', 'options' => Configure::read('Competences.sectors'), 'empty' => true, 'value' => $job['sector']));?>
          </div>
        </div>
        <div class="form-group group-jobs">
          <label class="control-label col-md-3"><?php echo __('Job'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.job1', array('label' => false, 'class' => 'form-control bs-select input-jobs input-fb-jobs', 'options' => Configure::read('Competences.fb_jobs'), 'empty' => true, 'value' => $job['job']));?>
            <?php echo $this->Form->input('Job.job2', array('label' => false, 'class' => 'form-control bs-select input-jobs input-logistics-jobs', 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => true, 'value' => $job['job']));?>
            <?php echo $this->Form->input('Job.job3', array('label' => false, 'class' => 'form-control bs-select input-jobs input-animation-jobs', 'options' => Configure::read('Competences.animation_jobs'), 'empty' => true, 'value' => $job['job']));?>
          </div>
        </div>
        <div class="form-group group-activities">
          <label class="control-label col-md-3"><?php echo __('Activity'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.activity_id', array('label' => false, 'class' => 'form-control bs-select input-activities', 'options' => $activities, 'empty' => true, 'data-live-search' => true, 'value' => empty($job['activity_id']) ? '' : $job['activity_id']));?>
          </div>
        </div>
        <div class="form-group group-fb-modules">
          <label class="control-label col-md-3"><?php echo __('F&B Module'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.fb_module_id', array('label' => false, 'class' => 'form-control bs-select input-fb-modules', 'options' => $fbmodules, 'empty' => true, 'data-live-search' => true, 'value' => empty($job['fb_module_id']) ? '' : $job['fb_module_id']));?>
          </div>
        </div>
        <div class="form-group group-hierarchies">
          <label class="control-label col-md-3"><?php echo __('Hierarchy'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.hierarchy', array('label' => false, 'class' => 'form-control bs-select input-hierarchies', 'options' => Configure::read('Competences.hierarchies'), 'empty' => true, 'value' => empty($job['hierarchy']) ? '' : $job['hierarchy']));?>
          </div>
        </div>
        <div class="form-group group-languages">
          <label class="control-label col-md-3"><?php echo __('Language'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.languages', array('label' => false, 'class' => 'form-control bs-select input-languages', 'options' => Configure::read('Jobs.languages'), 'empty' => true, 'multiple' => true, 'value' => !empty($job['languages']) ? explode(',', $job['languages']) : ''));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Staff'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.user_ids', array('label' => false, 'class' => 'form-control bs-select input-inline', 'data-live-search' => true, 'multiple' => true, 'options' => $users, 'empty' => true, 'value' => $job['user_ids']));?>
            <span class="help-block">Vous pouvez sélectionner directement un extra dans la liste déroulante.</span>
          </div>
        </div>
        <div class="form-group group-remarks">
          <label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Job.remarks', array('label' => false, 'class' => 'form-control input-remarks', 'value' => empty($job['remarks']) ? '' : $job['remarks']));?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
        <button type="button" class="btn btn-danger delete-job" data-job-id="<?php echo $job['id']; ?>"><?php echo __('Delete'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<?php endforeach; ?>
<?php endforeach; ?>
<?php endif; ?>

<div class="modal modal-moment" id="add-moment" data-edit="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <?php echo $this->Form->create('Moment', array('class' => 'form-horizontal form-moment', 'url' => array('controller' => 'moments', 'action' => 'edit'))); ?>
      <?php echo $this->Form->input('Moment.event_id', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo __('Add a moment'); ?></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Type'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.type', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Moments.types')));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.start_hour', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small'));?>
            <?php echo $this->Form->input('Moment.end_hour', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small'));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.name', array('label' => false, 'class' => 'form-control'));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.remarks', array('label' => false, 'class' => 'form-control'));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Place'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.place_id', array('label' => false, 'class' => 'form-control bs-select'));?>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('Activity'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('Moment.activity_id', array('label' => false, 'class' => 'form-control bs-select'));?>
            <span class="help-block">Lister que les activités sélectionnées?</span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>

<div class="modal modal-moment" id="edit-moment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><?php echo __('Edit a moment'); ?></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
        <button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-job" id="modal-edit-job" role="basic" aria-hidden="true" data-edit="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>
<div class="modal modal-job" id="modal-add-job" role="basic" aria-hidden="true" data-edit="true" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content"></div>
  </div>
</div>
<div class="modal modal-job-briefing" id="modal-add-job-briefing" role="basic" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-full">
    <div class="modal-content"></div>
  </div>
</div>

<?php echo $this->element('Modals/add-planning-board'); ?>

<div id="event-navigation">
  <ul class="list-unstyled">
    <li>
      <a href="#portlet-client">
        <span class="icon"><i class="fa fa-money"></i></span>
        <span class="name"><?php echo __('Client'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-tasks">
        <span class="icon"><i class="fa fa-tasks"></i></span>
        <span class="name"><?php echo __('Tasks'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-infos">
        <span class="icon"><i class="fa fa-info-circle"></i></span>
        <span class="name"><?php echo __('Infos'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-accounting">
        <span class="icon"><i class="fa fa-cc-visa"></i></span>
        <span class="name"><?php echo __('Accounting'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-closing">
        <span class="icon"><i class="fa fa-lock"></i></span>
        <span class="name"><?php echo __('Closing'); ?></span>
      </a>
    </li>
    <li class="divider"></li>
    <li>
      <a href="#portlet-notes">
        <span class="icon"><i class="fa fa-list"></i></span>
        <span class="name"><?php echo __('Notes'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-activities">
        <span class="icon"><i class="fa fa-trophy"></i></span>
        <span class="name"><?php echo __('Activities'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-fb-modules">
        <span class="icon"><i class="fa fa-cutlery"></i></span>
        <span class="name"><?php echo __('F&B'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-places">
        <span class="icon"><i class="fa fa-map-marker"></i></span>
        <span class="name"><?php echo __('Places'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-recruiting">
        <span class="icon"><i class="fa fa-users"></i></span>
        <span class="name"><?php echo __('Recruiting'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-hours">
        <span class="icon"><i class="fa fa-clock-o"></i></span>
        <span class="name"><?php echo __('Hours'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-planning">
        <span class="icon"><i class="fa fa-hourglass"></i></span>
        <span class="name"><?php echo __('Planning'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-vehicles">
        <span class="icon"><i class="fa fa-truck"></i></span>
        <span class="name"><?php echo __('Vehicles'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-orders">
        <span class="icon"><i class="fa fa-clipboard"></i></span>
        <span class="name"><?php echo __('Orders'); ?></span>
      </a>
    </li>
    <li>
      <a href="#portlet-downloads">
        <span class="icon"><i class="fa fa-download"></i></span>
        <span class="name"><?php echo __('Downloadable documents'); ?></span>
      </a>
    </li>
  </ul>
</div>

<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/nouislider/jquery.nouislider.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/nouislider/jquery.nouislider.all.min.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js');
echo $this->Html->script('/js/jquery.ajax-progress.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
