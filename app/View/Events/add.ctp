<?php $this->assign('page_title', $this->Html->link(__('Events'), array('action' => 'index', 'confirmed'))); ?>
<?php $this->assign('page_subtitle', __('Add an event'));?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-calendar font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an event'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Event', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('Event.user_id', array('value' => AuthComponent::user('id'), 'type' => 'hidden')); ?>
		<?php echo $this->Form->input('Event.type', array('value' => 'standard', 'type' => 'hidden')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Folder number'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('code', array('label' => false, 'class' => 'form-control', 'readonly' => false, 'placeholder' => 'No selon dossier serveur')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'placeholder' => "ex: Sortie de fin d'année, Sortie des cadres,... ou selon nom donné par le client"));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">
						<?php echo __('Opening date'); ?>
					</label>
					<div class="col-md-9">
						<?php echo $this->Form->input('opening_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16, 'value' => date('d-m-Y')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Status CRM'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('crm_status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.crm_status'), 'value' => 'new'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'value' => empty($client) ? '' : $client['Company'][0]['id'])); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('event_category_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $categories));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Desired activity type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('desired_activity_type', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.activity_types'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Event type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('eg_mg_type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.eg_mg_types'))); ?>
            <span class="help-block">Pour EG et MG uniquement</span>
          </div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Grills'); ?></label>
					<div class="col-md-9">
            <div class="row">
              <div class="col-md-3">
                <?php echo $this->Form->input('grills_rondo', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                <span class="help-block">Rondo</span>
              </div>
              <div class="col-md-3">
                <?php echo $this->Form->input('grills_torro_l', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                <span class="help-block">Torro L</span>
              </div>
              <div class="col-md-3">
                <?php echo $this->Form->input('grills_torro_s', array('type' => 'number', 'label' => false, 'class' => 'form-control')); ?>
                <span class="help-block">Torro S</span>
              </div>
            </div>
          </div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Former outings'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('former_outings', array('label' => false, 'class' => 'form-control', 'placeholder' => 'Historique de leurs évènements avec/sans UBIC')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Person in charge'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('resp_id', array('label' => false, 'class' => 'form-control select2me', 'empty' => true, 'value' => AuthComponent::user('id'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Needs analysis'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('needs_analysis', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Events.needs_analysis'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Client'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('client_id', array('label' => false, 'class' => 'form-control bs-select client', 'empty' => empty($client) ? __('Please select a company first') : false, 'data-live-search' => true, 'options' => empty($client) ? array() : array($client['Client']['id'] => $client['Client']['name']), 'value' => empty($client) ? '' : $client['Client']['id'])); ?>
						<span class="help-block">
							Si le client n'apparaît pas, veuillez contrôler que les champs d'adresse sont remplis et qu'il est associé à la bonne entreprise. <br>
							Vous pouvez le chercher via la barre de recherche en haut du site.
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"></label>
					<div class="col-md-9">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new client'), array('#' => 'new-client'), array('class' => 'label label-default new-client', 'data-toggle' => 'modal', 'escape' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_people_id', array('label' => false, 'class' => 'form-control bs-select contact-people', 'options' => empty($contactPeoples) ? array() : $contactPeoples)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"></label>
					<div class="col-md-9">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new contact person'), array('#' => 'new-contact-people'), array('class' => 'label label-default new-contact-people', 'data-toggle' => 'modal', 'escape' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Language of correspondence'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('language', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.languages'), 'empty' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Language during event'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('languages', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.languages'), 'empty' => true, 'multiple' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Desired date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16, 'data-date-format' => 'yyyy-mm-dd', 'placeholder' => '1 date - pas de flexibilité du client'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Potential dates'); ?></label>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-3">
								<?php echo $this->Form->input('Date.0.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd', 'required' => false, 'data-date-start-date' => '+0d', 'placeholder' => 'Choix de dates encore ouvertes chez le client'));?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Date.1.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd', 'required' => false, 'data-date-start-date' => '+0d'));?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Date.2.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd', 'required' => false, 'data-date-start-date' => '+0d'));?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Date.3.date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd', 'required' => false, 'data-date-start-date' => '+0d'));?>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Potential period'); ?></label>
					<div class="col-md-9">
						<div class="input-group input-large date-picker input-daterange">
							<?php echo $this->Form->input('period_start', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
							<span class="input-group-addon"> <?php echo __('to'); ?> </span>
								<?php echo $this->Form->input('period_end', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
						</div>
						<span class="help-block">Les évènements avec période ne s'affichent pas dans le calendrier.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start hour'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24', 'value' => 0));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End hour'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('end_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24', 'value' => 0));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Objective'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('objective', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Target people'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('target_people', array('label' => false, 'class' => 'form-control', 'placeholder' => "Par exemple: ouvriers / administratif / cadres / apprentis / etc...")); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Gender balance'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('gender_balance', array('label' => false, 'class' => 'form-control', 'placeholder' => "Par ex: 50/50")); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Age'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('age', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Number of persons'); ?></label>
					<div class="col-md-9">
						<span class="help-inline"><?php echo __('From'); ?></span>
						<?php echo $this->Form->input('min_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('max_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
						<span class="help-block">Remplir les deux champs</span>
					</div>

				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Confirmed number of persons'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('confirmed_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small'));?>
						<span class="help-block">Au plus tard 10 jours avant l'évènement!</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Number of persons for the offer'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('offer_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
						<span class="help-inline">Sert uniquement pour le chiffrage de l'offre, minimum requis</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Desired region'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Region..id', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => $regions, 'multiple' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Radius'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('region_radius', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline')); ?>
						<span class="help-inline">km</span>
						<?php echo $this->Form->input('region_radius_minutes', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline')); ?>
						<span class="help-inline">min.</span>
						<span class="help-block">Vous pouvez renseigner un champ ou les deux.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Budget'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('budget', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
						<span class="help-inline">CHF / <?php echo __('person'); ?></span>
						<span class="help-block">Estimé/donné par le client</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Projected turnover'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('projected_turnover', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline', 'required' => true));?>
						<span class="help-inline">CHF</span>
						<span class="help-block">Selon offre envoyée</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Feeling'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('feeling', array('type' => 'hidden', 'value' => !empty($this->request->data['Event']['feeling']) ? $this->request->data['Event']['feeling'] : 0));?>
						<div class="row">
							<div class="col-md-4">
								<div class="noUi-control noUi-success" id="feeling"></div>
								<span class="help-block"><span id="EventFeelingSpan"></span>%</span>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Offer sending deadline'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('offer_sending_deadline', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control date-picker'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Relaunch date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('relaunch_date', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control date-picker input-medium'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Knowledge'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ubic_knowledge', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.ubic_knowledge'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Contact by'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_method', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Events.contact_method'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Invoicing'); ?></h3>
				<div class="form-group">
					<div class="col-md-9 col-md-offset-3">
						<span class="help-block">Ces champs sont à remplir s'ils sont différents des informations du client.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Business name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_business_name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('PO'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_po', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_address', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group" data-commune-select2>
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array(), 'value' => ''));?>
						<?php echo $this->Form->input('invoice_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('invoice_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						<button type="submit" class="btn blue" value="work" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and work on event'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<div class="modal fade" id="new-client" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('Client', array('controller' => 'clients', 'action' => 'add')); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new client / contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Client'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('Business name'); ?></label>
							<?php echo $this->Form->input('Client.name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('Client.companies', array('class' => 'form-control bs-select', 'label' => false, 'options' => $companies, 'value' => 2)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Address'); ?></label>
							<?php echo $this->Form->input('Client.address', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('ZIP'); ?></label>
							<?php echo $this->Form->input('Client.zip', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('City'); ?></label>
							<?php echo $this->Form->input('Client.city', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Country'); ?></label>
							<?php echo $this->Form->input('Client.country', array('class' => 'form-control bs-select', 'options' => Configure::read('Countries'), 'value' => 'CH', 'data-live-search' => true, 'label' => false)); ?>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.0.status', array('type' => 'hidden', 'value' => 1)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="new-contact-people" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('ContactPeople', array('controller' => 'contact_peoples', 'action' => 'add')); ?>
	<?php echo $this->Form->input('status', array('type' => 'hidden', 'value' => 1)); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.client_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('ContactPeople.status', array('type' => 'hidden', 'value' => 1)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/nouislider/jquery.nouislider.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('wnumb/wnumb.min.js');
echo $this->Html->script('/metronic/global/plugins/nouislider/jquery.nouislider.all.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
