<?php

foreach($data as $row){
  if(!empty($row['Event']['confirmed_date'])){
    $date = $this->Time->format('d.m.Y', $row['Event']['confirmed_date']);
  } else {
    $date = '';
    if(!empty($row['Date'])){
      foreach ($row['Date'] as $key => $tmp) {
        $date .= $this->Time->format('d.m.Y', $tmp['date']);
        if(isset($row['Date'][$key+1])) $date .= '/';
      }
    } elseif(empty($row['Date']) && !empty($row['Event']['period_start'])){
      $date = $this->Time->format('d.m.Y', $row['Event']['period_start']);
    }
  }
  $temp = array(
    str_replace(',',' - ',$row['PersonInCharge']['first_name']),
    str_replace(',',' - ',$row['Client']['name']),
    str_replace(',',' - ',$row['ContactPeople']['first_name']),
    str_replace(',',' - ',$row['ContactPeople']['last_name']),
    $row['ContactPeople']['email'],
    str_replace(',',' - ',$row['Event']['code']),
    str_replace(',',' - ',$row['Event']['name']),
    $date,
    isset($statuses[$row['Event']['crm_status']]) ? $statuses[$row['Event']['crm_status']] : '',
    $row['Event']['projected_turnover']
  );
  echo implode(",", $temp) . "\n";
}
