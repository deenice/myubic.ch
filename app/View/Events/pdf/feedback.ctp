<div class="event__documents event__documents--feedback <?php echo $event['Company']['class']; ?>">
  <?php echo $this->element('Events/documents/confirmation/header', array('event' => $event, 'clientArrival' => $clientArrival, 'title' => 'feedback')); ?>
  <table class="boxes">
    <?php if(in_array($event['Event']['company_id'], array(2,6))): ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Préparation du matériel', 'subtitle' => 'conditionnement, etc.')); ?>  
    <?php endif; ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Lieu(x)', 'placeholder' => 'Ressources disponibles (matériel, nbre et dimension)<br>Accès et espace extérieur<br>Parking (nbre de place)<br>Mobilité réduite (accès chaise roulante WC et salles principales)<br>Nbre de pièce et nbre de place dans chacune', 'subtitle' => 'contact, éléments dérangeants, idées d\'activités top pour ce lieu, etc.')); ?>  
    <?php if(in_array($event['Event']['company_id'], array(4,5))): ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Fournisseurs', 'subtitle' => '')); ?>  
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Collaborateurs EG et UBIC', 'subtitle' => '')); ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Matériel', 'subtitle' => '')); ?>
    <?php endif; ?>
    <?php if(in_array($event['Event']['company_id'], array(2,6))): ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Activité', 'subtitle' => 'dynamique de groupe, intérêt des clients, choses à améliorer, etc.')); ?>
    <?php endif; ?>
    <?php if(in_array($event['Event']['company_id'], array(2))): ?>  
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Nourriture et boissons', 'subtitle' => 'qualité, quantité, etc.')); ?>  
    <?php endif; ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Client', 'subtitle' => 'remarques, attitudes, etc.')); ?>  
    <?php if(in_array($event['Event']['company_id'], array(6))): ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Bug?', 'subtitle' => 'extras, timing, vos idées :-), etc.')); ?>  
    <?php endif; ?>
    <?php echo $this->element('Events/documents/feedback/box', array('title' => 'Divers', 'subtitle' => 'extras, timing, vos idées :-), etc.')); ?>  
  </table>
  <?php if($event['Event']['company_id'] == 2): ?>
  <small class="font-red">A remplir par l'Event Manager à la fin de l'évènement.</small>
  <?php elseif($event['Event']['company_id'] == 6): ?>
  <small class="font-red">A remplir par l'Urban Leader à la fin de l'évènement.</small>
  <?php endif; ?>
</div>
