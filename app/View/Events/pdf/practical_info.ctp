<div class="event__documents event__documents-pi <?php echo $event['Company']['class']; ?>">

  <div class="row">
    <div class="col-xs-12">
      <?php echo $this->element('Events/documents/practical_info/header', array('event' => $event)); ?>
      <div class="event__documents-container">
        <?php echo $this->element('Events/documents/practical_info/intro', array('event' => $event)); ?>
        <?php echo $this->element('Events/documents/practical_info/details', array('event' => $event, 'clientArrival' => $clientArrival, 'clientDeparture' => $clientDeparture)); ?>
        <?php echo $this->element('Events/documents/practical_info/program', array('moments' => $clientMoments)); ?>
        <?php echo $this->element('Events/documents/practical_info/misc', array('event' => $event)); ?>
        <?php echo $this->element('Events/documents/practical_info/contact', array('event' => $event, 'emdo' => $emdo)); ?>
      </div>
    </div>
  </div>

</div>
