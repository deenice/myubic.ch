<style>
	.logo {
		text-align: center;
	}
	.logo img {
		max-width: 100%;
    max-height: 100px;
    margin-top: 10px;
	}
	td {
		vertical-align: middle !important;
	}
	th.check {
		width: 5%;
	}
	th.quantity {
		width: 7%;
	}
	th.name {
		width: 40%;
	}
	th.supplier {
		width: 13%;
	}
	th.remarks {
		width: 28%;
	}
  th.alley {
    width: 7%;
  }
	tr.spacer td {
		border: 0 !important;
		height: 10px;
	}
	tr.title td {
		padding: 7px 0 10px 0;
	}
	.table-infos tr:first-child td {
		border-top: 0 !important;
	}
	.table-orders, .table-orders tr, .table-orders td{
		page-break-inside: avoid !important;
	}
	h1 {
		margin: 0;
		padding: 5px 0;
		font-family: 'Kameron';
		text-transform: uppercase;
		font-weight: 700;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
	}
	h3 {
		margin: 0 0 20px 0;
		padding: 5px 0;
		font-family: 'Kameron';
		text-transform: uppercase;
		font-weight: 700;
		text-align: center;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
	}
	.break{
		display: block;
		clear: both;
		page-break-after: always;
	}
	.visa {
		position: relative;
		height: 70px;
		border: 1px solid #000;
		margin-bottom: 20px;
	}
	.visa .label {
		font-family: 'Kameron';
		position: absolute;
		top: 5px;
		left: 5px;
		font-weight: bold;
		text-transform: uppercase;
		border: 0;
		padding: 0;
		margin: 0;
	}

	.cover th {
		font-size: 18px;
		text-transform: uppercase;
	}

</style>
<?php if(!empty($locations)): ?>
	<?php foreach($locations as $location): if(empty($location['orders'])){continue;} ?>
		<div class="invoice">
			<?php echo $this->element('Orders/pdf/header', array('company' => $event['Company'], 'event' => $event, 'mode' => $mode, 'location' => $location)); ?>
			<?php echo $this->element('Orders/pdf/visa'); ?>
			<?php echo $this->element('Orders/pdf/items', array('orders' => $location['orders'], 'show_depot' => false, 'show_check' => false)); ?>
			<div class="break"></div>
		</div>
	<?php endforeach; ?>
<?php elseif(!empty($suppliers)): ?>
	<div class="invoice cover">
		<?php echo $this->element('Orders/pdf/cover', array('event' => $event, 'suppliers' => $suppliers, 'emdo' => $emdo, 'depotArrival' => $depotArrival, 'depotLeaving' => $depotLeaving)); ?>
		<div class="break"></div>
	</div>
	<?php foreach($suppliers as $supplier): if(empty($supplier['orders'])){continue;} ?>
		<div class="invoice">
			<?php echo $this->element('Orders/pdf/header', array('company' => $event['Company'], 'event' => $event, 'mode' => $mode, 'supplier' => $supplier)); ?>
			<?php echo $this->element('Orders/pdf/items', array('orders' => $supplier['orders'], 'show_depot' => false, 'show_check' => false)); ?>
			<div class="break"></div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
<div class="invoice">
	<?php echo $this->element('Orders/pdf/header', array('company' => $event['Company'], 'event' => $event, 'mode' => $mode)); ?>
	<?php echo $this->element('Orders/pdf/items', array('orders' => $event['Order'], 'show_depot' => true, 'show_check' => true)); ?>
</div>
<?php endif; ?>
