<div class="tracking-sheet-pdf wide">
  <div class="row">
    <div class="col-xs-7">
      <h1 class="uppercase bordered">Création d'évènements</h1>
    </div>
    <div class="col-xs-3">
      <div class="label">Contact client</div><div class="input"><?php echo !empty($event) ? $this->Time->format('d.m.Y', $event['Event']['opening_date']) : ''; ?></div>
      <div class="label">Délai de suivi</div><div class="input"></div>
    </div>
    <div class="col-xs-2 text-center">
      <img src="<?php echo IMAGES; ?>/logo_ubic.jpg" alt="" class="img-responsive"/>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12" style="margin-top: 8px">
      <table class="table-bordered">
        <tr>
          <td rowspan="2" colspan="2" class="bg-grey">
            <p>
              <strong>Entreprise</strong>
              <?php if(!empty($event['Client'])): ?>
                <span class="value"><?php echo $event['Client']['name']; ?></span>
              <?php else: ?>
              <br /><br />
              <?php endif; ?>
            </p>
            <p>
              <strong>Adresse</strong>
              <?php if(!empty($event['Client'])): ?>
                <span class="value"><?php echo $event['Client']['address']; ?>, <?php echo $event['Client']['zip_city']; ?></span>
              <?php else: ?>
              <br /><br />
              <?php endif; ?>
            </p>
            <p>
              <strong>Contact</strong>
              <?php if(!empty($event['ContactPeople'])): ?>
                <span class="value"><?php echo $event['ContactPeople']['full_name']; ?></span>
              <?php else: ?>
              <br /><br /><br />
              <?php endif; ?>
            </p>
            <p>
              <strong>Téléphone direct</strong>
              <?php if(!empty($event['ContactPeople'])): ?>
                <span class="value"><?php echo $event['ContactPeople']['phone']; ?></span>
              <?php else: ?>
              <?php endif; ?>
            </p>
            <p>
              <strong>Email</strong>
              <?php if(!empty($event['ContactPeople'])): ?>
                <span class="value"><?php echo $event['ContactPeople']['email']; ?></span>
              <?php else: ?>
              <br /><br />
              <?php endif; ?>
            </p>
          </td>
          <td style="width:25%" class="bg-grey"><h3>Programme</h3></td>
          <td style="width:25%" class="bg-grey"><h3>Animation / Décoration / Thématique</h3></td>
        </tr>
        <tr>
          <td>
            <p><strong>Heure de début</strong><br /><small>Compté depuis où?</small></p><br />
            <p><strong>Plages imposées</strong><br /><small>Partie officielle?</small></p><br />
            <p><strong>Heure de fin</strong><br /><small>Partie officielle?</small></p><br />
          </td>
          <td>
            <p>
              <small>Souhaitez-vous animation participative / passive?</small><br />
              <small>Avez-vous une thématique de soirée en tête?</small><br />
              <small>Qu'entendez-vous par décoration?</small>
            </p>
          </td>
        </tr>
        <tr>
          <td class="bg-grey" style="width: 25%"><h3>Généralités</h3></td>
          <td class="bg-grey" style="width: 25%"><h3>Évènements passés</h3></td>
          <td class="bg-grey" style="width: 25%"><h3>Moments</h3></td>
          <td class="bg-grey" style="width: 25%"><h3>Lieux // Transports</h3></td>
        </tr>
        <tr>
          <td>
            <p>
              <strong>Date</strong>
              <?php if(!empty($event)): ?>
                <span class="value">
                <?php if(!empty($event['Event']['confirmed_date'])): ?>
                  <?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d %b %Y'); ?>
                <?php elseif(!empty($event['Date'])): ?>
                  <?php foreach($event['Date'] as $k => $date): ?>
                    <?php echo $this->Time->format('d.m.Y', $date['date']); ?>
                    <?php echo !empty($event['Date'][$k+1]) ? __('or') : ''; ?>
                  <?php endforeach; ?>
                <?php elseif(!empty($event['Event']['period_start'])): ?>
                  <?php echo $this->Time->format('d.m.Y', $event['Event']['period_start']); ?> <?php echo __('to'); ?> <?php echo $this->Time->format('d.m.Y', $event['Event']['period_end']); ?>
                <?php endif;?>
                </span>
              <?php endif; ?>
            </p>
            <br />
            <p><strong>Motif</strong><br /><small>Pour quelle occasion organisez-vous cet évènement?</small></p><br /><br /><br />
          </td>
          <td>
            <p>
              <small>Qu'avez-vous fait les années précédentes?</small><br />
              <small>Quels sont les éléments qui ont bien plu/fonctionné?</small><br />
              <small>Où / Quoi / F&amp;B?</small>
              <?php if(!empty($event['Client']['Event'])): ?>
                <ul class="list-unstyled">
                  <?php foreach($event['Client']['Event'] as $k => $ev): ?>
                    <li class="value no-margin"><?php echo $this->Time->format('d.m.y', $ev['confirmed_date']) . ' ' . $ev['name']; ?><?php echo isset($event['Client']['Event'][$k+1]) ? ',':''; ?></li>
                  <?php endforeach; ?>
                </ul>
              <?php elseif(!empty($event['Event']['former_outings'])): ?>
                <br><span class="value"><?php echo $event['Event']['former_outings']; ?></span>
              <?php else: ?>
              <?php endif; ?>
            </p>
          </td>
          <td rowspan="3">
            <p><strong>Avant</strong><br />
              <small>Programme à prévoir avant le repas?</small><br />
              <small>Organisez-vous quelque chose à l'interne avant cet évènement (séminaire, visite, etc...)?</small>
            </p><br /><br />
            <p><strong>Apéritif</strong><br />
              <small>Type d'apéritif souhaité</small><br />
              <small>Animation?</small><br />
              <small>Boissons standards ou cocktails de bienvenue, coupe de champagne?</small>
            </p><br /><br />
            <p><strong>Repas</strong><br />
              <small>Quel type de repas souhaitez-vous?</small><br />
              <small>Entrée, plat, dessert?</small><br />
              <small>Quels sont les éléments importants pour vous?</small>
            </p><br /><br />
            <p><strong>After</strong><br />
              <small>Avez-vous une équipe plutôt festive?</small><br />
              <small>Souhaitez-vous un bar avec alcool fort?</small><br />
              <small>Les gens aiment-ils danser?</small>
            </p><br /><br />
          </td>
          <td>
            <p>
              <small>Quel est le périmètre pour le lieu?</small><br />
              <small>Les participants se déplacent-ils individuellement? Faut-il prévoir un transport en car? Ou un nez-rouge pour rentrer?</small>
            </p>
          </td>
        </tr>
        <tr>
          <td class="bg-grey" style="width: 25%"><h3>Participants</h3></td>
          <td class="bg-grey" style="width: 25%"><h3>Budget</h3></td>
          <td class="bg-grey" style="width: 25%"><h3>Prochaines étapes</h3></td>
        </tr>
        <tr>
          <td>
            <p><strong>Nb pax</strong><br />
              <small>Est-ce le nombre total d'invités ou la moyenne de participation?</small><br />
              <small>Y a-t-il des invités à compter en plus : pers. accompagnées, interne?</small>
              <?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
                <br><span class="value no-margin"><?php echo $event['Event']['confirmed_number_of_persons']; ?></span>
              <?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
                <br><span class="value no-margin"><?php echo $event['Event']['min_number_of_persons']; ?></span>
              <?php elseif(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
                <br><span class="value no-margin"><?php echo $event['Event']['max_number_of_persons']; ?></span>
              <?php endif; ?>
            </p>
            <br />
            <p><strong>Qui est invité?</strong><br />
              <small>Est-ce les collaborateurs / clients?</small><br />
              <small>Quel secteur?</small>
              <?php if(!empty($event['Event']['target_people'])): ?>
                <br><span class="value no-margin"><?php echo $event['Event']['target_people']; ?></span>
              <?php else: ?>
              <?php endif; ?>
            </p>
            <br />
            <p>
              <strong>Âge</strong>
              <?php if(!empty($event['Event']['age'])): ?>
                <span class="value"><?php echo $event['Event']['age']; ?></span>
              <?php endif; ?>
            </p>
            <br />
          </td>
          <td>
            <p><strong>Souhaité</strong><br />
              <small>Est-ce que le budget est fixé ou dépend du nombre de particpants?</small>
            </p><br />
            <p><strong>Maximum?</strong></p>
            <p><strong>Minimum?</strong></p>
            <p><strong>Réserve possible?</strong></p>
          </td>
          <td>
            <p><strong>Délais</strong><br />
              <small>Quel est votre processus de sélection à l'interne?</small><br />
              <small>Y a-t-il un comité d'organisation?</small>
            </p><br />
            <p><strong>Présentation de l'offre / Visite?</strong></p>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
