<div class="tracking-sheet-pdf">
  <div class="row">
    <div class="col-xs-3">
      <h1 class="uppercase">Profil<br />Entreprise</h1>
    </div>
    <div class="col-xs-5 text-center">
      <img src="<?php echo IMAGES; ?>/logo_ubic.jpg" alt="" style="width: 120px;"/>
    </div>
    <div class="col-xs-4">
      <div class="label">Contact client</div><div class="input"><?php echo empty($event) ? '' : $this->Time->format('d.m.Y', $event['Event']['opening_date']); ?></div>
      <div class="label">Délai de suivi</div><div class="input"></div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <table class="table-bordered">
        <tr>
          <td class="bg-grey" style="width: 15%"><div><strong>Coordonnées</strong></div></td>
          <td style="width: 50%">
            <div>
              <p>
                Civilité&nbsp;&nbsp;&nbsp;
                <?php if(!empty($event)): ?>
                  <?php if(!empty($event['ContactPeople'])): ?>
                    <?php if(in_array($event['ContactPeople']['civility'], array('mr', 'dr'))): ?>
                      <i class="fa fa-check-square-o"></i> Monsieur&nbsp;&nbsp;&nbsp;
                    <?php else: ?>
                      <i class="fa fa-square-o"></i> Monsieur&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                    <?php if(in_array($event['ContactPeople']['civility'], array('ms', 'mrs'))): ?>
                      <i class="fa fa-check-square-o"></i> Madame&nbsp;&nbsp;&nbsp;
                    <?php else: ?>
                      <i class="fa fa-square-o"></i> Madame&nbsp;&nbsp;&nbsp;
                    <?php endif; ?>
                  <?php else: ?>
                    <i class="fa fa-square-o"></i> Monsieur&nbsp;&nbsp;&nbsp;
                    <i class="fa fa-square-o"></i> Madame
                  <?php endif; ?>
                <?php else: ?>
                <i class="fa fa-square-o"></i> Monsieur&nbsp;&nbsp;&nbsp;
                <i class="fa fa-square-o"></i> Madame
                <?php endif;?>
              </p>
              <p>
                Nom de l'entreprise
                <?php if(!empty($event) && !empty($event['Client'])): ?>
                  <span class="value"><?php echo $event['Client']['name']; ?></span>
                <?php endif; ?>
              </p>
              <p>Rue / N&deg;
                <?php if(!empty($event) && !empty($event['Client'])): ?>
                  <span class="value"><?php echo $event['Client']['address']; ?></span>
                <?php endif; ?>
              </p>
              <p>NPA / Localité
                <?php if(!empty($event) && !empty($event['Client'])): ?>
                  <span class="value"><?php echo $event['Client']['zip_city']; ?></span>
                <?php endif; ?>
              </p>
              <p>Personne de contact
                <?php if(!empty($event) && !empty($event['ContactPeople'])): ?>
                  <span class="value"><?php echo $event['ContactPeople']['full_name']; ?></span>
                <?php endif; ?>
              </p>
              <p>Téléphone direct
                <?php if(!empty($event) && !empty($event['ContactPeople'])): ?>
                  <span class="value"><?php echo $event['ContactPeople']['phone']; ?></span>
                <?php endif; ?>
              </p>
              <p>E-mail
                <?php if(!empty($event) && !empty($event['ContactPeople'])): ?>
                  <span class="value"><?php echo $event['ContactPeople']['email']; ?></span>
                <?php endif; ?>
              </p>
            </div>
          </td>
          <td class="bg-grey" rowspan="2">
            <strong>Suivi de l'offre</strong>
            <p><br /></p>
            <p>
              Resp. dossier
              <?php if(!empty($event) && !empty($event['PersonInCharge'])): ?>
                <span class="value"><?php echo $event['PersonInCharge']['full_name']; ?></span>
              <?php endif; ?>
              <br />
            </p>
            <p>Mail récap. à envoyer le</p>
            <hr>
            <p><br /><br />Envoi offre 1.0</p>
            <p>Appel réception offre</p>
            <p>Rappel décision</p>
          </td>
        </tr>
        <tr>
          <td class="bg-grey"><div><strong>Connaissance UBIC</strong></div></td>
          <td>
              <?php if(!empty($event['Event']['ubic_knowledge'])): ?>
                <p>
                  <?php foreach($knowledge as $key => $k): ?>
                    <span class="type">
                    <?php if($key == $event['Event']['ubic_knowledge']): ?>
                      <u><?php echo $k; ?></u>
                    <?php else: ?>
                      <?php echo $k; ?>
                    <?php endif; ?>
                    </span>
                  <?php endforeach; ?>
                </p>
              <?php else: ?>
              <p>
                <?php echo implode(' / ', $knowledge); ?>
              </p>
              <?php endif; ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <br />
  <div class="row">
    <div class="col-xs-12">
      <table class="table-bordered">
        <tr>
          <td class="bg-grey" style="width:50%"><h3>Besoins</h3></td>
          <td class="bg-grey" style="width:50%"><h3>Expériences et souhaits</h3></td>
        </tr>
        <tr>
          <td>
            <div>
              <?php if(!empty($event['Event']['desired_activity_type'])): ?>
                <p>
                  <?php foreach($activityTypes as $key => $at): ?>
                    <span class="type">
                    <?php if($key == $event['Event']['desired_activity_type']): ?>
                      <u><?php echo $at; ?></u>
                    <?php else: ?>
                      <?php echo $at; ?>
                    <?php endif; ?>
                    </span>
                  <?php endforeach; ?>
                </p>
              <?php else: ?>
              <p>
                <?php echo implode(' / ', $activityTypes); ?>
              </p>
              <?php endif; ?>
              <p>
                Date
                <?php if(!empty($event)): ?>
                  <span class="value">
                  <?php if(!empty($event['Event']['confirmed_date'])): ?>
										<?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d %b %Y'); ?>
									<?php elseif(!empty($event['Date'])): ?>
										<?php foreach($event['Date'] as $k => $date): ?>
											<?php echo $this->Time->format('d.m.Y', $date['date']); ?>
											<?php echo !empty($event['Date'][$k+1]) ? __('or') : ''; ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['Event']['period_start'])): ?>
										<?php echo $this->Time->format('d.m.Y', $event['Event']['period_start']); ?> <?php echo __('to'); ?> <?php echo $this->Time->format('d.m.Y', $event['Event']['period_end']); ?>
									<?php endif;?>
                  </span>
                <?php endif; ?>
              </p>
              <p>
                Timing
                <?php if(!empty($event)): ?>
                  <span class="value">
                    <?php if(!empty($event['Event']['start_hour'])): ?>
                      <?php echo $this->Time->format('H:i', $event['Event']['start_hour']); ?>
                      <?php if(!empty($event['Event']['end_hour'])): ?>
                         - <?php echo $this->Time->format('H:i', $event['Event']['end_hour']); ?>
                      <?php endif; ?>
                    <?php endif; ?>
                  </span>
                <?php endif; ?>
              </p>
              <p>Type objectif
                <?php if(!empty($event['Event']['objective'])): ?>
                  <span class="value"><?php echo $event['Event']['objective']; ?></span>
                <?php else: ?>
                  <br><br><br>
                <?php endif; ?>
            </p>
              <p>Budget
                <?php if(!empty($event)): ?>
                  <?php if(!empty($event['Event']['budget'])): ?>
                    <span class="value"><?php echo __('%s CHF / person', $event['Event']['budget']); ?></span>
                  <?php endif; ?>
                  <?php if(!empty($event['Event']['projected_turnover'])): ?>
                    <span class="value">
                      <?php echo __('%s CHF', number_format($event['Event']['projected_turnover'], 2, '.', "'")); ?>
                    </span>
                  <?php endif; ?>
                <?php endif; ?>
              </p>
            </div>
          </td>
          <td>
            <div>
              <p>
                <?php echo __('Former outings'); ?>
                <?php if(!empty($event['Client']['Event'])): ?>
                  <ul class="list-unstyled former-outings">
                    <?php foreach($event['Client']['Event'] as $k => $ev): ?>
                      <li><?php echo $this->Time->format('d.m.y', $ev['confirmed_date']) . ' ' . $ev['name']; ?><?php echo isset($event['Client']['Event'][$k+1]) ? ',':''; ?></li>
                    <?php endforeach; ?>
                  </ul>
                <?php elseif(!empty($event['Event']['former_outings'])): ?>
                  <br><span class="value"><?php echo $event['Event']['former_outings']; ?></span>
                <?php else: ?>
                <br /><br /><br />
                <?php endif; ?>
              </p>
              <p>Souhaits particuliers<br /></p>
              <p>Activités
                <?php if(!empty($event['SuggestedActivity'])): ?>
                  <?php foreach($event['SuggestedActivity'] as $activity): ?>
                    <?php echo $this->Html->tag('span', sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('class' => 'btn btn-xs default uppercase')); ?>
                  <?php endforeach; ?>
                <?php else: ?>
                <br /><br /><br />
                <?php endif; ?>
              </p>
              <p>Région
                <?php if(!empty($event)): ?>
                  <?php if(!empty($event['Event']['region_zip_city'])): ?>
                    <span class="value"><?php echo $event['Event']['region_zip_city']; ?></span>
                  <?php endif; ?>
                <?php endif; ?>
                <br />
              </p>
            </div>
          </td>
        </tr>
        <tr>
          <td class="bg-grey" style="width:50%"><h3>Participants</h3></td>
          <td class="bg-grey" style="width:50%"><h3>Remarques - propositions</h3></td>
        </tr>
        <tr>
          <td>
            <div>
              <p><?php echo __('PAX'); ?>
                <?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
                  <span class="value"><?php echo $event['Event']['confirmed_number_of_persons']; ?></span>
                <?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
                  <span class="value"><?php echo $event['Event']['min_number_of_persons']; ?></span>
                <?php elseif(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
                  <span class="value"><?php echo $event['Event']['max_number_of_persons']; ?></span>
                <?php endif; ?>
              </p>
              <p><?php echo __('Target people'); ?>
                <?php if(!empty($event['Event']['target_people'])): ?>
                  <span class="value"><?php echo $event['Event']['target_people']; ?></span>
                <?php else: ?>
                  <br /><br />
                <?php endif; ?>
              </p>
              <p><?php echo __('Language'); ?>
                <?php if(!empty($event['Event']['languages'])): $languages = explode(',', $event['Event']['languages']); ?>
                  <?php foreach($languages as $lang): ?>
                    <span class="value"><?php echo $eventLanguages[$lang]; ?></span>
                  <?php endforeach; ?>
                <?php else: ?><br />
                <?php endif; ?>
              </p>
              <p><?php echo __('Age'); ?>
                <?php if(!empty($event['Event']['age'])): ?>
                  <span class="value"><?php echo $event['Event']['age']; ?></span>
                <?php endif; ?>
              </p>
              <p><?php echo __('Gender balance'); ?>
                <?php if(!empty($event['Event']['gender_balance'])): ?>
                  <span class="value"><?php echo $event['Event']['gender_balance']; ?></span>
                <?php else: ?>
                  <br />
                <?php endif; ?>
              </p>
            </div>
          </td>
          <td>
            <div>
              <?php if(!empty($event['Event']['remarks'])): ?>
                <p>
                  <span class="value no-margin"><?php echo nl2br($event['Event']['remarks']); ?></span>
                </p>
              <?php endif; ?>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
