<div class="event__documents event__documents--hours <?php echo $event['Company']['class']; ?>">
  <?php echo $this->element('Events/documents/confirmation/header', array('event' => $event, 'clientArrival' => $clientArrival, 'title' => 'staff')); ?>
  <div class="row">
    <div class="col-xs-12">
      <small class="font-red">Le temps est compté aux 15minutes. Merci de faire fonctionner votre bon sens pour noter de manière équitable pour tous.</small>
    </div>
    <?php foreach($staff as $k => $extra): ?>
      <?php echo $this->element('Events/documents/hours/staff', array('extra' => $extra)); ?>
    <?php endforeach; ?>
  </div>
</div>
