<div class="<?php echo $event['Company']['class']; ?>">
  <div class="event__documents event__documents--briefing">
    <?php foreach($event['Job'] as $i => $job): ?>
    <?php if(!empty($job['JobBriefing'])): ?>
    <?php foreach($job['JobBriefing'] as $k => $briefing): ?>
    <br>
    <h5><?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?> - <?php echo $event['Client']['name']; ?></h5>
    <h1><?php echo $briefing['name']; ?></h1>
    <h3><?php echo $job['name']; ?></h3>
    <?php echo $briefing['content']; ?>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php if(isset($event['Job'][$i+1]) && !empty($event['Job'][$i+1]['JobBriefing'])): ?><div class="break"></div><?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>
