<div class="event__documents event__documents--confirmations <?php echo $event['Company']['class']; ?>">
  <?php echo $this->element('Events/documents/confirmation/header', array('event' => $event, 'clientArrival' => $clientArrival, 'title' => 'client')); ?>
  <?php echo $this->element('Events/documents/confirmation/planning', array('planning' => $event['GeneralPlanning'])); ?>
  <div class="break"></div>
  <?php foreach($staff as $k => $extra): ?>
    <?php echo $this->element('Events/documents/confirmation/header', array('event' => $event, 'clientArrival' => $clientArrival, 'title' => 'extra', 'extra' => $extra)); ?>
    <?php echo $this->element('Events/documents/confirmation/staff', array('staff' => $staff)); ?>
    <?php echo $this->element('Events/documents/confirmation/schedule', array('extra' => $extra, 'carSharingStarts' => $carSharingStarts)); ?>
    <?php echo $this->element('Events/documents/confirmation/client', array('planning' => $clientPlanning)); ?>
    <?php echo $this->element('Events/documents/confirmation/outfit', array('extra' => $extra)); ?>
    <?php echo $this->element('Events/documents/confirmation/remarks', array('extra' => $extra)); ?>
    <?php if(isset($staff[$k+1])): ?><div class="break"></div><?php endif; ?>
  <?php endforeach; ?>
</div>
