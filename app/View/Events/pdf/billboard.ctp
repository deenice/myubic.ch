<div class="event__documents event__documents--billboard">
  <?php echo $this->element('Events/documents/billboard/eg-mg-type', array('event' => $event)); ?>
  <div class="row">
    <div class="col-xs-12">
      <div style="margin-top: -20px"><?php echo $this->element('Events/documents/billboard/client', array('event' => $event)); ?></div>
      <?php echo $this->element('Events/documents/billboard/separator', array('event' => $event)); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-7">
      <?php echo $this->element('Events/documents/billboard/infos', array('event' => $event)); ?>
      <?php echo $this->element('Events/documents/billboard/planning', array('event' => $event)); ?>
    </div>
    <div class="col-xs-5">
      <?php echo $this->element('Events/documents/billboard/jobs', array('event' => $event)); ?>
    </div>
  </div>

</div>

<?php echo $this->element('Events/documents/billboard/footer', array('event' => $event)); ?>
