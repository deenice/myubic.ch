<div class="event__documents event__documents--evaluation <?php echo $event['Company']['class']; ?>">
  <?php foreach($staff as $k => $extra): ?>
  <div class="row">
    <div class="col-xs-12">
      <h3>Grille d'évaluation - Extra UBIC</h3>
      <p>
        Ce document permet d’évaluer un nouveau collaborateur extra au sein d’Une-bonne-idée.ch. Merci d’évaluer ses compétences entre 1 et 5 avec l’interdiction de mettre 3.<br>
        5 = dépasse largement les exigences.<br>
        Si un critère ne s’applique pas, le laisser en blanc.
      </p>
      <h4>Détails</h4>
      <table class="table table-bordered table--details">
        <tbody>
          <tr>
            <td class="label">Prénom, nom</td>
            <td><?php echo $extra['User']['full_name']; ?></td>
          </tr>
          <tr>
            <td class="label">Nom de l'évènement, date</td>
            <td><?php echo $event['Client']['name']; ?>, <?php echo $event['Event']['name']; ?>, <?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?></td>
          </tr>
          <tr>
            <td class="label">Poste occupé</td>
            <td><?php echo $extra['Job']['name']; ?></td>
          </tr>
          <tr>
            <td class="label">Evalué par</td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12">
      <h4>Habiletes et qualités personnelles</h4>
      <table class="table table--evaluation">
        <tbody>
          <tr>
            <td class="label">
              Apparence<br>
              <small>Allure, sympathie, tenue, propreté, sourire, professionnalisme</small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Dynamisme, motivation, engagement<br>
              <small>
                Maintient un intérêt et un effort constant face à son travail. <br>
                Il démontre un souci de réussite et de collaboration. Il se montre disponible
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Autonomie, initiative<br>
              <small>
                Une fois la mission donnée, il travaille de manière autonome et consciencieuse. <br>
                Il ne se tourne pas les pouces une fois sa mission terminée.<br>
                Son raisonnement le pousse à prendre des initiatives, ou du moins, à en proposer.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Esprit d’équipe<br>
              <small>
                Il sait s’intégrer dans l’équipe et se sent concerné par la mission commune.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Directives et compromis<br>
              <small>
                Il se sent à l’aise avec les directives. Il fait preuve de tact dans son rapport avec les autres.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Flexibilité et adaptation<br>
              <small>
                Il ajuste ses aptitudes et ses attitudes face aux situations.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Tolérance au stress et à l’ambiguïté<br>
              <small>
                Il maintient un rendement efficace malgré les contraintes et des conditions caractérisées<br>par l’absence d’information, d’appui ou d’encadrement.<br>
                Il gère efficacement le stress, la pression et le temps.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Leadership, influence<br>
              <small>
                Intéresse et mobilise l’équipe de travail et les participants pour favoriser une coopération directe.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Ecoute et compréhension<br>
              <small>
               Il se montre ouvert et intéressé aux autres. Il perçoit et réagit à leurs besoins.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Communication verbale<br>
              <small>
               Il est en mesure de s’exprimer de manière claire et distincte et adapte son langage au public. Il se sent à l’aise.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="break"></div>
    <div class="col-xs-12">
      <h4>Compétences organisationnelles</h4>
      <table class="table table--evaluation">
        <tbody>
          <tr>
            <td class="label">
              Compréhension organisationnelle<br>
              <small>
                Il comprend la dynamique interne d’Une-bonne-idée.ch, ses enjeux et ses orientations.<br>
                Il intègre la culture et les valeurs de l’organisme.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Approche des participants<br>
              <small>
                Il est soucieux d’offrir un service de qualité aux clients.<br>
                Il manifeste du respect et de l’intérêt pour les clients.<br>
                Il stimule l’implication des participants.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Approche avec les partenaires et autres extras<br>
              <small>
                Il participe à assurer la représentation et la visibilité d’Une-bonne-idée.ch
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Exécution du travail<br>
              <small>
                Qualité du travail<br>
                Il travaille avec minutie, exactitude et précision.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Productivité / Rythme<br>
              <small>
                Il travaille à une vitesse adéquate, n’est pas trop lent.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
          <tr><td colspan="6" class="spacer">&nbsp;</td></tr>
          <tr>
            <td class="label">
              Capacité et volonté d’apprendre<br>
              <small>
                Il a les compétences d’acquérir aisément de nouvelles connaissances et à une soif d’apprendre.
              </small>
            </td>
            <td class="score">1</td><td class="score">2</td><td class="score font-grey">3</td><td class="score">4</td><td class="score">5</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12">
      <h4>Remarques</h4>
    </div>
  </div>
  <?php if(isset($staff[$k+1])): ?><div class="break"></div><?php endif; ?>
  <?php endforeach; ?>
</div>
