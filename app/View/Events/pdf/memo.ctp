<div class="<?php echo $event['Company']['class']; ?>">
  <div class="event__documents event__documents--memo">
    <h1><?php echo $event['Client']['name']; ?> - <?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></h1>
    <h2><?php echo $event['Event']['code']; ?></h2>
    <h3>Fiche memo</h3>
    <table class="table table-striped">
      <tbody>
        <tr>
          <th>Animation</th>
          <td>
          <?php if(!empty($event['SelectedActivity'])): ?>
            <?php foreach($event['SelectedActivity'] as $k => $activity): ?>
              <?php echo !empty($event['SelectedActivity']['name']) ? $event['SelectedActivity']['name'] : $activity['Activity']['name']; ?>
              <?php if(isset($event['SelectedActivity'][$k+1])): ?>, <?php endif; ?>
            <?php endforeach; ?>
          <?php endif; ?>      
          </td>
        </tr>
        <tr>
          <th>Nourriture</th>
          <td>
            <?php if(!empty($event['SelectedFBModule'])): ?>
              <?php foreach($event['SelectedFBModule'] as $k => $module): ?>
                <?php echo !empty($event['SelectedFBModule']['name']) ? $event['SelectedFBModule']['name'] : $module['FBModule']['name']; ?>
                <?php if(isset($event['SelectedFBModule'][$k+1])): ?>, <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </td>
        </tr>
      <tr><th>Boissons</th><td></td></tr>
      <tr>
        <th>Nbre</th>
        <td>
          <?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
            <?php echo $event['Event']['confirmed_number_of_persons']; ?>
          <?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
            <?php echo $event['Event']['min_number_of_persons']; ?>
            <?php if(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
                - <?php echo $event['Event']['max_number_of_persons']; ?>
            <?php endif; ?>
          <?php endif; ?>        
        </td>
      </tr>
        <tr>
          <th>Langue</th>
          <td>
            <?php if(!empty($event['Event']['languages'])): ?>
              <?php foreach($event['Event']['languages'] as $k => $lang): ?>
              <?php echo $languages[$lang]; ?>
              <?php if(isset($event['Event']['languages'][$k+1])): ?>, <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <th>Lieu</th>
          <td>
            <?php if(!empty($event['ConfirmedEventPlace'])): ?>
              <?php foreach($event['ConfirmedEventPlace'] as $k => $place): ?>
                <?php echo $place['Place']['name']; ?>
                <?php echo $place['Place']['zip_city']; ?>
                <?php if(isset($event['ConfirmedEventPlace'][$k+1])): ?>, <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </td>
        </tr>
      <tr class="small">
        <th>Covoiturage extras</th>
        <td>
          <?php if(!empty($carSharingStarts)): ?>
          <?php foreach($carSharingStarts as $start): ?>
          <?php echo $start['PlanningBoardMoment']['name']; ?>
          <?php echo $this->Time->format('H:i', $start['PlanningBoardMoment']['start']); ?>
          <?php endforeach; ?>
          <?php endif; ?>
        </td>
      </tr>
      <tr class="small"><th>Confirmation réception engagement</th><td></td></tr>
      <tr class="small">
        <th>Contact clients natel - sur place</th>
        <td>
          <?php if(!empty($event['ContactPeople'])): ?>
          <?php echo $event['ContactPeople']['full_name']; ?>
          <?php echo $event['ContactPeople']['phone']; ?>
          <?php endif; ?>
        </td>
      </tr>
      </tbody>
    </table>
  </div>
</div>
