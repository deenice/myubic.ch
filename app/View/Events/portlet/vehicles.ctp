<div class="panel-group accordion" id="vehicle_reservations">
  <?php if(!empty($openReservations)):?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#vehicle_reservations" href="#open_reservations">
          <?php if(sizeof($openReservations) == 1): ?>
          <?php echo __('1 desired vehicle/trailer'); ?>
          <?php else: ?>
          <?php echo __('%d desired vehicles/trailers', sizeof($openReservations)); ?>
          <?php endif; ?>
        </a>
      </h4>
    </div>
    <div id="open_reservations" class="panel-collapse in">
        <div class="panel-body">
          <table class="table table-striped table-open-reservations">
            <thead>
              <tr>
                <th style="width: 22%"><?php echo __('Besoin'); ?></th>
                <th style="width: 22%"><?php echo __('Schedule'); ?></th>
                <th style="width: 22%"></th>
                <th style="width: 17%"><?php echo __('Prise'); ?></th>
                <th style="width: 17%"><?php echo __('Remise'); ?></th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($openReservations as $k => $reservation): ?>
                <tr data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>">
                  <td><?php echo $families[$reservation['VehicleReservation']['family']]; ?></td>
                  <td>
                    <?php echo $this->Form->input('start', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control datetime-picker input-small input-sm input-inline vehicle-reservation-schedule', 'readonly' => true, 'data-field' => 'start', 'value' => $this->Time->format($reservation['VehicleReservation']['start'], '%d-%m-%Y - %H:%M'))); ?>
                  </td>
                  <td>
                    <?php echo $this->Form->input('end', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control datetime-picker input-small input-sm input-inline vehicle-reservation-schedule', 'readonly' => true, 'data-field' => 'end', 'value' => $this->Time->format($reservation['VehicleReservation']['end'], '%d-%m-%Y - %H:%M'))); ?>
                  </td>
                  <td>
                    <?php echo $this->Form->input('collect', array('label' => false, 'div' => false, 'options' => $locations, 'class' => 'form-control input-inline input-small input-sm vehicle-reservation-place', 'empty' => true, 'data-field' => 'start_place', 'value' => $reservation['VehicleReservation']['start_place'])); ?>
                  </td>
                  <td>
                    <?php echo $this->Form->input('giveback', array('label' => false, 'div' => false, 'options' => $locations, 'class' => 'form-control input-inline input-small input-sm vehicle-reservation-place', 'empty' => true, 'data-field' => 'end_place', 'value' => $reservation['VehicleReservation']['end_place'])); ?>
                  </td>
                  <td><a href="javascript:;" class="btn btn-danger btn-xs delete-vehicle-reservation" data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>"><i class="fa fa-times"></i></a></td>
                </tr>
                <tr data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>" class="drivers">
                  <td>Chauffeur(s)</td>
                  <td colspan="2">
                    <?php if(!empty($drivers)): ?>
                    <?php echo $this->Form->input('driver_id', array('label' => false, 'div' => false, 'options' => $drivers, 'class' => 'form-control bs-select assign-driver', 'empty' => true, 'data-field' => 'driver_id', 'value' => $reservation['VehicleReservation']['driver_id'] )); ?>
                    <?php else: ?>
                    <span class="label label-sm label-warning">
                        <i class="fa fa-warning"></i> Aucun poste dans l'évènement!
                    </span>
                    <?php endif; ?>
                  </td>
                  <td colspan="2">
                    <?php if(!empty($drivers)): ?>
                      <?php echo $this->Form->input('back_driver_id', array('label' => false, 'div' => false, 'options' => $drivers, 'class' => 'form-control bs-select assign-driver', 'empty' => true, 'data-field' => 'back_driver_id', 'value' => $reservation['VehicleReservation']['back_driver_id'] )); ?>
                    <?php else: ?>
                    <span class="label label-sm label-warning">
                        <i class="fa fa-warning"></i> Aucun poste dans l'évènement!
                    </span>
                    <?php endif; ?>
                  </td>
                  <td>&nbsp;</td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
    </div>
  </div>
  <?php endif; ?>
  <?php if(!empty($confirmedReservations)): ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#vehicle_reservations" href="#confirmed_reservations">
          <?php if(sizeof($confirmedReservations) == 1): ?>
          <?php echo __('1 confirmed vehicle/trailer'); ?>
          <?php else: ?>
          <?php echo __('%d confirmed vehicles/trailers', sizeof($confirmedReservations)); ?>
          <?php endif; ?>
        </a>
      </h4>
    </div>
    <div id="confirmed_reservations" class="panel-collapse <?php if(empty($openReservations) && !empty($confirmedReservations)):?>in<?php else: ?>collapse<?php endif; ?>">
      <div class="panel-body">
        <table class="table table-striped">
          <thead>
            <tr>
              <th style="width:22%"><?php echo __('Reservation'); ?></th>
              <th style="width:14%"><?php echo __('Schedule'); ?></th>
              <th style="width:18%"><?php echo __('Prise / Remise'); ?></th>
              <th style="width:23%"><?php echo __('Driver forth'); ?></th>
              <th style="width:23%"><?php echo __('Driver back'); ?></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($confirmedReservations as $k => $reservation): ?>
            <tr data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>">
              <td><?php echo $reservation['Vehicle']['VehicleCompanyModel']['VehicleCompany']['name'] . ' - ' . $reservation['Vehicle']['name_number']; ?></td>
              <td><?php echo sprintf('%s - %s', $this->Time->format('H:i', $reservation['VehicleReservation']['start']), $this->Time->format('H:i', $reservation['VehicleReservation']['end'])); ?></td>
              <td>
                <?php echo $locations[strtolower($reservation['VehicleReservation']['start_place'])]; ?> / <?php echo $locations[strtolower($reservation['VehicleReservation']['end_place'])]; ?>
              </td>
              <td>
                <?php if(!empty($drivers)): ?>
                <?php echo $this->Form->input('driver_id', array('label' => false, 'div' => false, 'class' => 'form-control bs-select assign-driver', 'value' => empty($reservation['VehicleReservation']['driver_id']) ? '' : $reservation['VehicleReservation']['driver_id'], 'data-field' => 'driver_id', 'options' => $drivers, 'empty' => __('Assign a driver'))); ?>
                <?php else: ?>
                <span class="label label-sm label-warning">
                    <i class="fa fa-warning"></i> Aucun poste dans l'évènement!
                </span>
                <?php endif; ?>
              </td>
              <td>
                <?php if(!empty($drivers)): ?>
                <?php echo $this->Form->input('back_driver_id', array('label' => false, 'div' => false, 'class' => 'form-control bs-select assign-driver', 'value' => empty($reservation['VehicleReservation']['back_driver_id']) ? '' : $reservation['VehicleReservation']['back_driver_id'], 'data-field' => 'back_driver_id', 'options' => $drivers, 'empty' => __('Assign a driver'))); ?>
                <?php else: ?>
                <span class="label label-sm label-warning">
                    <i class="fa fa-warning"></i> Aucun poste dans l'évènement!
                </span>
                <?php endif; ?>
              </td>
              <td><a href="javascript:;" class="btn btn-danger btn-xs cancel-vehicle-reservation" data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>"><i class="fa fa-times"></i></a></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <br>
        <span class="text-info">
          <i class="fa fa-warning"></i> Pour modifier l'horaire de réservation ou lieu de prise/remise, il faut le demander au reponsable des véhicules.
        </span>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <?php if(!empty($cancelledReservations)):?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#vehicle_reservations" href="#cancelled_reservations">
          <?php if(sizeof($cancelledReservations) == 1): ?>
          <?php echo __('1 cancelled vehicle/trailer'); ?>
          <?php else: ?>
          <?php echo __('%d cancelled vehicles/trailers', sizeof($cancelledReservations)); ?>
          <?php endif; ?>
        </a>
      </h4>
    </div>
    <div id="cancelled_reservations" class="panel-collapse collapse">
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th style="width: 22%"><?php echo __('Reservation'); ?></th>
                <th style="width: 22%"><?php echo __('Schedule'); ?></th>
                <th style="width: 18%"><?php echo __('Prise / Remise'); ?></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($cancelledReservations as $k => $reservation): ?>
              <tr data-vehicle-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>">
                <td><s><?php echo $reservation['Vehicle']['VehicleCompanyModel']['VehicleCompany']['name'] . ' - ' . $reservation['Vehicle']['name_number']; ?></s></td>
                <td><s><?php echo sprintf('%s - %s', $this->Time->format('H:i', $reservation['VehicleReservation']['start']), $this->Time->format('H:i', $reservation['VehicleReservation']['end'])); ?></s></td>
                <td>
                  <s><?php echo $locations[strtolower($reservation['VehicleReservation']['start_place'])]; ?> / <?php echo $locations[strtolower($reservation['VehicleReservation']['end_place'])]; ?></s>
                </td>
                <td>
                  <?php if($reservation['VehicleReservation']['archived']): ?>
                    <span class="text-success"><i class="fa fa-check"></i> L'annulation a été traitée.</span>
                  <?php else: ?>
                    <span class="text-warning"><i class="fa fa-warning"></i> L'annulation n'a pas encore été traitée.</span>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
    </div>
  </div>
  <?php endif; ?>
</div>

<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'vehicles', 'notes' => $portletNotes)); ?>
