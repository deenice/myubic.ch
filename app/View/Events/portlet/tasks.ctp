<?php if(!empty($event['Checklist'])): ?>
  <div class="panel-group accordion" id="accordiontasks">
    <?php foreach($event['Checklist'] as $list): ?>
      <strong class="checklist-name">
        <?php echo $list['name']; ?>
        <span class="actions">
          <?php echo $this->Html->link('<i class="fa fa-times font-red"></i>', array('controller' => 'checklists', 'action' => 'delete', $list['id']), array('class' => 'delete-checklist', 'escape' => false)); ?>
        </span>
      </strong>
      <?php foreach($list['ChecklistBatch'] as $batch): $batch['ChecklistBatch'] = $batch;?>
    		<?php echo $this->element('Portlets/checklist_batch', array('batch' => $batch, 'classes' => array('panel-title' => 'collapsed', 'panel-collapse' => 'collapse'))); ?>
      <?php endforeach; ?>
      <input type="text" class="form-control new-batch" data-href="<?php echo Router::url(array('controller' => 'checklist_batches', 'action' => 'add', 'checklist_id' => $list['id'])); ?>" placeholder="<?php echo __('New group of tasks'); ?>">
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<div class="text-center">
<?php echo $this->Html->link(sprintf('<i class="fa fa-list"></i> %s', __('Show all available checklists')), array('controller' => 'checklists', 'action' => 'preview', $event['Event']['id']), array('class' => 'btn btn-xs btn-success', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-preview-checklist')); ?>
</div>

<div id="datepicker" class="hidden">
  <div class="dropdown-menu">
    <div class="date-picker date-picker-task" data-date-format="YYYY-MM-DD"> </div>
  </div>
</div>
