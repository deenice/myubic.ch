<div class="row">
	<div class="col-md-12">
		<?php if(!empty($event['FBModule'])): ?>
		<table class="table">
			<thead>
				<tr>
					<th><?php echo __('F&B module'); ?></th>
					<th style="width: 20%" class="text-right"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($event['FBModule'] as $module): //debug($module); ?>
				<tr class="<?php // echo $activity['ActivitiesEvent']['status'] == 'selected' ? 'success':''; ?>">
					<td class="title">
						<?php if($module['standard']): ?>
							<?php if($module['EventsFbModule']['status'] == 'selected'): ?>
								<i class="fa fa-trophy font-yellow-crusta"></i>&nbsp;
								<strong><?php echo $this->Html->link($module['name'], array('controller' => 'fb_modules', 'action' => 'view', $module['id']), array('target' => '_blank')); ?></strong>
							<?php else: ?>
								<?php echo $this->Html->link($module['name'], array('controller' => 'fb_modules', 'action' => 'view', $module['id']), array('target' => '_blank')); ?>
							<?php endif; ?>
						<?php else: ?>
							<?php if($module['EventsFbModule']['status'] == 'selected'): ?>
								<i class="fa fa-trophy font-yellow-crusta"></i>&nbsp;
							<?php endif; ?>
							<?php echo $this->Form->input('non_standard_module_name', array('div' => false, 'label' => false, 'id' => false, 'class' => 'form-control input-sm non-standard-module', 'value' => empty($module['EventsFbModule']['name']) ? $module['name'] : $module['EventsFbModule']['name'], 'data-event-fb-module-id' => $module['EventsFbModule']['id'])); ?>
						<?php endif; ?>
						<?php if($module['EventsFbModule']['status'] == 'selected'): ?>
						<table class="table table-fb-module-infos">
							<thead>
								<tr>
									<th>Personnes</th>
									<th>Staff</th>
									<th>Tables</th>
									<th>Pers / Table</th>
									<th>Buffets</th>
									<th>Équipes</th>
									<th>Pers / Équipe</th>
								</tr>
							</thead>
							<tbody>
								<tr data-event-fb-module-id="<?php echo $module['EventsFbModule']['id']; ?>">
									<td><?php echo $this->Form->input('number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_persons', 'value' => empty($module['EventsFbModule']['number_of_persons']) ? 0 : $module['EventsFbModule']['number_of_persons'])); ?></td>
									<td><?php echo $this->Form->input('number_of_staff', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_staff', 'value' => empty($module['EventsFbModule']['number_of_staff']) ? 0 : $module['EventsFbModule']['number_of_staff'])); ?></td>
									<td><?php echo $this->Form->input('number_of_tables', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_tables', 'value' => empty($module['EventsFbModule']['number_of_tables']) ? 0 : $module['EventsFbModule']['number_of_tables'])); ?></td>
									<td><?php echo $this->Form->input('number_of_persons_per_table', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_persons_per_table', 'value' => empty($module['EventsFbModule']['number_of_persons_per_table']) ? 0 : $module['EventsFbModule']['number_of_persons_per_table'])); ?></td>
									<td><?php echo $this->Form->input('number_of_buffets', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_buffets', 'value' => empty($module['EventsFbModule']['number_of_buffets']) ? 0 : $module['EventsFbModule']['number_of_buffets'])); ?></td>
									<td><?php echo $this->Form->input('number_of_teams', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_teams', 'value' => empty($module['EventsFbModule']['number_of_teams']) ? 0 : $module['EventsFbModule']['number_of_teams'])); ?></td>
									<td><?php echo $this->Form->input('number_of_persons_per_team', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_persons_per_team', 'value' => empty($module['EventsFbModule']['number_of_persons_per_team']) ? 0 : $module['EventsFbModule']['number_of_persons_per_team'])); ?></td>
								</tr>
							</tbody>
						</table>
						<?php endif; ?>
					</td>
					<td class="actions text-right">
						<?php if($module['EventsFbModule']['status'] == 'selected'): ?>
            <a href="javascript:;" class="btn default btn-sm update-orders tooltips" data-original-title="Mettre à jour les quantités de la commande" data-event-fb-module-id="<?php echo $module['EventsFbModule']['id']; ?>"><i class="fa fa-refresh"></i></a>
						<a href="javascript:;" class="btn btn-default btn-sm select-fb-module bg-yellow-crusta font-white" data-event-fb-module-id="<?php echo $module['EventsFbModule']['id']; ?>" data-status="selected"><i class="fa fa-trophy"></i></a>
						<?php else: ?>
						<a href="javascript:;" class="btn btn-default btn-sm select-fb-module" data-event-fb-module-id="<?php echo $module['EventsFbModule']['id']; ?>" data-status="suggested"><i class="fa fa-trophy"></i></a>
						<?php endif; ?>
						<a href="javascript:;" class="btn btn-danger btn-sm delete-fb-module" data-event-fb-module-id="<?php echo $module['EventsFbModule']['id']; ?>"><i class="fa fa-times"></i></a>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else: ?>
			<div class="alert alert-warning">
				<p>
					Aucun module F&amp;B n'a été proposée pour le moment.
				</p>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'fb_modules', 'notes' => $portletNotes)); ?>
