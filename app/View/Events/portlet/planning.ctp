<?php if(!empty($event['PlanningBoard'])): ?>
<div class="tabbable tabbable-tabdrop">
  <ul class="nav nav-tabs ">
    <?php foreach($event['PlanningBoard'] as $k => $board): ?>
    <li class="<?php echo ( ($k==0 && !$activeBoard) || $activeBoard == $board['id']) ? 'active':''; ?>">
      <a href="#board<?php echo $board['id']; ?>" data-board="<?php echo $board['id']; ?>" data-toggle="tab"> <?php echo $board['name']; ?> </a>
    </li>
    <?php endforeach; ?>
  </ul>
  <div class="tab-content">
    <?php foreach($event['PlanningBoard'] as $k => $board): ?>
    <div class="tab-pane<?php echo ( ($k==0 && !$activeBoard) || $activeBoard == $board['id']) ? ' active':''; ?>" id="board<?php echo $board['id']; ?>">
      <?php if(!empty($board['PlanningBoardMoment'])): ?>
      <div class="row">
        <div class="col-md-8">
          <table class="table table-striped table-condensed table-planning">
            <thead>
              <tr>
                <th class="schedule" colspan="2"><?php echo __('Schedule');?></th>
                <th class="what"><?php echo __('What');?></th>
                <th class="who"><?php echo __('Who');?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($board['PlanningBoardMoment'] as $moment): ?>
                <tr class="sortable" data-planning-board-moment-id="<?php echo $moment['id']; ?>">
                  <td class="schedule"><?php echo $this->Time->format('H:i', $moment['start']); ?></td>
                  <td class="schedule"><?php echo $this->Time->format('H:i', $moment['end']); ?></td>
                  <td>
                    <?php echo $this->Html->link($moment['name'], array('controller' => 'planning_board_moments', 'action' => 'modal', $moment['id']), array('data-target' => '#modal-planning-moment', 'data-toggle' => 'modal')); ?>
                    <?php if($moment['remarks']): ?>
                      <br>
                      <small><?php echo $moment['remarks']; ?></small>
                    <?php endif; ?>
                  </td>
                  <td class="who">
                    <?php if(!empty($moment['PlanningResource'])): ?>
                      <?php foreach($moment['PlanningResource'] as $resource): ?>
                        <span class="badge badge-roundless badge--<?php echo $resource['resource_model']; ?>" data-model="<?php echo $resource['resource_model']; ?>" data-model-id="<?php echo $resource['resource_id']; ?>" data-resource-id="<?php echo $resource['id']; ?>"><?php echo $resource['slug']; ?></span>
                      <?php endforeach; ?>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <div class="col-md-4">
          <div class="row planning-resources">
            <div class="col-md-12">
              <span class="bold uppercase planning-resources__title">Ressources</span>
              <?php if(!empty($resources)): ?>
                <ul class="list-unstyled">
                <?php foreach($resources as $class => $tags): ?>
                  <?php foreach($tags as $tag): ?>
                  <li class="hidden">
                    <a href="#" class="tag" data-model="<?php echo $tag['model']; ?>" data-model-id="<?php echo $tag['id']; ?>">
                      <?php echo sprintf('<i class="%s"></i> %s', $tag['icon'], $tag['label']); ?>
                    </a>
                  </li>
                  <li class="<?php echo $class; ?>">
                    <a class="tag" href="javascript:;" data-model="<?php echo $tag['model']; ?>" data-model-id="<?php echo $tag['id']; ?>">
                      <?php echo $tag['label']; ?>
                    </a>
                  </li>
                  <?php endforeach; ?>
                <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </div>
          </div>
          <div class="row-fluid planning-resources planning-resources--legend">
            <div class="col-md-12">
              <small class="bold uppercase">Légende</small>
              <ul class="list-unstyled">
                <li class="client"><a href="#" class="tag">Client</a></li>
                <li class="manager"><a href="#" class="tag">Event coordinator</a></li>
                <li class="extra"><a href="#" class="tag">Extra</a></li>
                <li class="place"><a href="#" class="tag">Lieu</a></li>
                <li class="activity"><a href="#" class="tag">Activité</a></li>
                <li class="fb_module"><a href="#" class="tag">Module F&amp;B</a></li>
                <li class="vehicle"><a href="#" class="tag">Véhicule</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <?php endif; ?>
      <div class="row">
        <div class="col-md-12">
          <?php if($board['type'] != 'general'): ?>
          <?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit %s', $board['name'])), array('controller' => 'planning_boards', 'action' => 'modal', $board['id']), array('escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-planning-board', 'class' => 'btn btn-xs btn-default')); ?>
          <?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete %s', $board['name'])), array('controller' => 'planning_boards', 'action' => 'delete', $board['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger delete-planning-board')); ?>
          <?php endif; ?>
          <?php echo $this->Html->link(sprintf('<i class="fa fa-plus"></i> %s', __('Add a moment')), array('controller' => 'planning_board_moments', 'action' => 'add', $board['id']), array('escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-planning-moment', 'class' => 'btn btn-xs btn-success')); ?>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>

<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'planning', 'notes' => $portletNotes)); ?>

<?php echo $this->element('Modals/planning-moment'); ?>
