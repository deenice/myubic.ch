<?php foreach($event['Order'] as $order): ?>
<div class="order-items-wrapper">
  <div data-order-items <?php if(!in_array($event['Event']['crm_status'], array('null', 'refused', 'partner'))): ?>data-order-items-unavailabilities<?php endif; ?> data-order-id="<?php echo $order['id']; ?>" data-href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'html', $order['id'])); ?>">
    <i class="fa fa-spin fa-spinner"></i> en chargement...
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="input-group order-group-name">
        <?php echo $this->Form->input('order-group-name', array('div' => false, 'label' => false, 'class' => 'form-control', 'placeholder' => __('Add a group'), 'data-href' => Router::url(array('controller' => 'order_groups', 'action' => 'add', 'order_id' => $order['id'])))); ?>
        <span class="input-group-btn">
          <button class="btn blue" type="button"><i class="fa fa-plus"></i></button>
        </span>
      </div>
      <?php echo $this->Form->input('open-groups', array('type' => 'hidden', 'value' => implode(',', $firstGroup), 'class' => 'open-groups')); ?>
    </div>
  </div>
  <hr>
</div>
<?php endforeach; ?>
<script type="text/javascript">
  $('#portlet-orders').addClass('loaded');
</script>
