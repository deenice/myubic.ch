<?php $statuses = Configure::read('Events.crm_status'); ?>
<?php $statuses1 = Configure::read('Missions.status'); ?>
<?php $types = Configure::read('Events.activity_types'); ?>
<table class="table table-striped table-bordered">
  <tbody>
    <tr>
      <td style="width: 45%"><strong><?php echo __('Name'); ?></strong></td>
      <td><?php echo empty($event['Event']['code_name']) ? $event['Event']['name'] : $event['Event']['code_name']; ?></td>
    </tr>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('Status'); ?></strong></td>
      <td>
        <a href="javascript:;" data-crm-status-editable data-mode="inline" data-showbuttons="false" data-value="<?php echo empty($event['Event']['crm_status']) ? '' : $event['Event']['crm_status']; ?>" data-type="select" data-pk="<?php echo $event['Event']['id']; ?>">
          <?php echo !empty($event['Event']['crm_status']) ? $statuses[$event['Event']['crm_status']] : ''; ?>
        </a>
      </td>
    </tr>
    <?php else: ?>
    <tr>
      <td><strong><?php echo __('Status'); ?></strong></td>
      <td>
        <a href="javascript:;" data-mission-status-editable data-mode="inline" data-showbuttons="false" data-value="<?php echo empty($event['Event']['crm_status']) ? '' : $event['Event']['crm_status']; ?>" data-type="select" data-pk="<?php echo $event['Event']['id']; ?>">
          <?php echo !empty($event['Event']['crm_status']) ? $statuses1[$event['Event']['crm_status']] : ''; ?>
        </a>
      </td>
    </tr>
    <?php endif; ?>
    <tr>
      <td><strong><?php echo ($event['Event']['crm_status'] != 'confirmed' && !$mission) ? __('Desired date') : __('Confirmed date'); ?></strong></td>
      <td>
        <a href="javascript:;" class="editable" data-type="date" data-mode="inline" data-name="confirmed_date" data-value="<?php echo empty($event['Event']['confirmed_date']) ? '' : $event['Event']['confirmed_date']; ?>" data-pk="<?php echo $event['Event']['id']; ?>">
          <?php echo !empty($event['Event']['confirmed_date']) ? $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y') : ''; ?></a>
        <?php if(!empty($event['Event']['confirmed_date'])):?>
          <br>
          <a href="javascript:;" class="editable" data-mode="inline" data-name="start_hour" data-value="<?php echo $event['Event']['start_hour']; ?>" data-pk="<?php echo $event['Event']['id']; ?>">
          <small><?php echo $this->Time->format($event['Event']['start_hour'], '%H:%M'); ?></small>
          </a>
           - 
          <a href="javascript:;" class="editable" data-mode="inline" data-name="end_hour" data-value="<?php echo $event['Event']['end_hour']; ?>" data-pk="<?php echo $event['Event']['id']; ?>">
          <small><?php echo $this->Time->format($event['Event']['end_hour'], '%H:%M'); ?></small>
          </a>
        <?php endif; ?>
      </td>
    </tr>
    <?php if($event['Event']['crm_status'] != 'confirmed'): ?>
    <?php if(!empty($event['Date'])): ?>
    <tr>
      <td><strong><?php echo __('Potential dates'); ?></strong></td>
      <td>
        <?php foreach($event['Date'] as $k => $date): ?>
          <?php echo $this->Time->format($date['date'], '%d.%m.%Y'); ?><?php echo (!empty($event['Date'][$k])) ? ', ' : ''; ?>
        <?php endforeach; ?>
      </td>
    </tr>
    <?php endif; ?>
    <?php if(!empty($event['Event']['period_start'])): ?>
    <tr>
      <td><strong><?php echo __('Desired period'); ?></strong></td>
      <td><?php echo __('From %s to %s', $this->Time->format('d.m.Y', $event['Event']['period_start']), $this->Time->format('d.m.Y', $event['Event']['period_end'])) ?></td>
    </tr>
    <?php endif; ?>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('Category') ?></strong></td>
      <td><?php echo $event['EventCategory']['name']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(empty($event['Event']['confirmed_number_of_persons']) && !$mission): ?>
    <tr>
      <td><strong><?php echo __('PAX (not confirmed)'); ?></strong></td>
      <td>
        <?php if(!empty($event['Event']['min_number_of_persons']) && !empty($event['Event']['max_number_of_persons']) && empty($event['Event']['confirmed_number_of_persons'])): ?>
        <?php echo __('From %s to %s persons', $event['Event']['min_number_of_persons'], $event['Event']['max_number_of_persons']) ?>
        <?php elseif(!empty($event['Event']['min_number_of_persons']) && empty($event['Event']['max_number_of_persons'])): ?>
        <?php echo __('%s persons', $event['Event']['min_number_of_persons']) ?>
        <?php elseif(empty($event['Event']['min_number_of_persons']) && !empty($event['Event']['max_number_of_persons'])): ?>
        <?php echo __('%s persons', $event['Event']['max_number_of_persons']) ?>
        <?php endif; ?>
      </td>
    </tr>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('PAX (confirmed)'); ?></strong></td>
      <td>
        <a href="javascript:;" class="editable" data-mode="inline" data-name="confirmed_number_of_persons" data-value="<?php echo empty($event['Event']['confirmed_number_of_persons']) ? '' : $event['Event']['confirmed_number_of_persons']; ?>" data-pk="<?php echo $event['Event']['id']; ?>">
          <?php echo !empty($event['Event']['confirmed_number_of_persons']) ? __('%s persons', $event['Event']['confirmed_number_of_persons']) : ''; ?>
        </a>
      </td>
    </tr>
    <?php endif; ?>
    <?php if(!empty($event['Event']['budget'])): ?>
    <tr>
      <td><strong><?php echo __('Budget'); ?></strong></td>
      <td><?php echo __('%s CHF / person', $event['Event']['budget']); ?></td>
    </tr>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('Turnover'); ?></strong></td>
      <td>
        <a href="#" class="editable" data-mode="inline" data-showbuttons="false" data-name="projected_turnover" data-value="<?php echo empty($event['Event']['projected_turnover']) ? '' : $event['Event']['projected_turnover']; ?>" data-pk="<?php echo $event['Event']['id']; ?>">
          <?php echo !empty($event['Event']['projected_turnover']) ? __('%s CHF', $event['Event']['projected_turnover']) : ''; ?>
        </a>
      </td>
    </tr>
    <?php endif; ?>
    <tr>
      <td><strong><?php echo __('Person in charge'); ?></strong></td>
      <td><?php echo $event['PersonInCharge']['full_name']; ?></td>
    </tr>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('Place(s)'); ?></strong></td>
      <td>
        <?php if(!empty($event['ConfirmedEventPlace'])): ?>
          <?php foreach($event['ConfirmedEventPlace'] as $k => $cep): ?>
            <?php echo $cep['Place']['name']; ?>
            <?php if(isset($event['ConfirmedEventPlace'][$k+1])): ?><br><?php endif; ?>
          <?php endforeach; ?>
        <?php else: ?>
          <?php echo __('No confirmed place'); ?>
        <?php endif; ?>
      </td>
    </tr>
    <?php endif; ?>
    <?php if(!$mission): ?>
    <tr>
      <td><strong><?php echo __('Needs analysis'); ?></strong></td>
      <td><?php echo $this->Html->link(__('Print'), array('controller' => 'events', 'action' => 'tracking_sheet', $event['Event']['id'], $event['Event']['needs_analysis'], 'analyse', 'ext' => 'pdf'), array('target'=> '_blank', 'escape' => false)); ?></td>
    </tr>
    <?php endif; ?>
    <tr>
      <td><strong><?php echo __('Notes'); ?></strong></td>
      <td>
        <?php if(!empty($event['Note'])): ?>
          <a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
            <?php if(sizeof($event['Note']) == 1): ?>
              <?php echo __('1 note'); ?>
            <?php else: ?>
              <?php echo __('%d notes', sizeof($event['Note'])); ?>
            <?php endif; ?>
          </a>
        <?php else: ?>
          <a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
        <?php endif; ?>
      </td>
    </tr>
  </tbody>
</table>
