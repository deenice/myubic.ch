<table class="table table-striped table-bordered table-invoicing">
	<tr>
		<th><?php echo __('Business name'); ?></th>
		<td>
			<?php if(!empty($event['Event']['invoice_business_name'])): ?>
				<a href="javascript:;" class="editable" data-pk="<?php echo $event['Event']['id']; ?>" data-value="<?php echo $event['Event']['invoice_business_name']; ?>" data-name="invoice_business_name">
					<?php echo $event['Event']['invoice_business_name']; ?>
				</a>
			<?php else: ?>
				<?php echo $event['Client']['name']; ?>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th><?php echo __('PO'); ?></th>
		<td>
			<a href="javascript:;" class="editable" data-pk="<?php echo $event['Event']['id']; ?>" data-value="<?php echo $event['Event']['invoice_po']; ?>" data-name="invoice_po">
				<?php echo $event['Event']['invoice_po']; ?>
			</a>
		</td>
	</tr>
	<tr>
		<th><?php echo __('Address'); ?></th>
		<td>
			<?php if(!empty($event['Event']['invoice_address'])): ?>
				<a href="javascript:;" class="editable" data-pk="<?php echo $event['Event']['id']; ?>" data-value="<?php echo $event['Event']['invoice_address']; ?>" data-name="invoice_address">
					<?php echo $event['Event']['invoice_address']; ?>
				</a>
			<?php else: ?>
				<?php echo $event['Client']['address']; ?>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th><?php echo __('ZIP / City'); ?></th>
		<td>
			<?php if(!empty($event['Event']['invoice_zip'])): ?>
				<a href="javascript:;" class="editable" data-pk="<?php echo $event['Event']['id']; ?>" data-value="<?php echo $event['Event']['invoice_zip']; ?>" data-name="invoice_zip">
					<?php echo $event['Event']['invoice_zip']; ?>
				</a>
				<a href="javascript:;" class="editable" data-pk="<?php echo $event['Event']['id']; ?>" data-value="<?php echo $event['Event']['invoice_city']; ?>" data-name="invoice_city">
					<?php echo $event['Event']['invoice_city']; ?>
				</a>
			<?php else: ?>
				<?php echo $event['Client']['zip_city']; ?>
			<?php endif; ?>
		</td>
	</tr>
</table>
