<div class="row">
  <div class="col-md-12">
    <strong class="uppercase hidden">Animation</strong>
    <table class="table" data-hours>
      <thead>
        <tr>
          <th class="portrait"></th>
          <th class="name"></th>
          <th class="start"><?php echo __('Start'); ?></th>
          <th class="end"><?php echo __('End'); ?></th>
          <th class="break"><?php echo __('Break'); ?></th>
          <th class="salary"><?php echo __('Salary'); ?></th>
          <th class="hours"><?php echo __('Hours'); ?></th>
          <th class="total"><?php echo __('Total'); ?></th>
          <th class="check"><?php echo __('Validated'); ?>?</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($event['Job'] as $job): ?>
          <?php foreach($job['User'] as $user): //debug($user); ?>
            <tr data-job-id="<?php echo $job['id']; ?>" data-user-id="<?php echo $user['id']; ?>">
              <td>
                <?php if(!empty($user['Portrait'])): ?>
                <?php echo $this->Html->image($this->ImageSize->crop(DS . $user['Portrait'][0]['url'], 30, 30)); ?>
                <?php endif; ?>
              </td>
              <td>
                <?php echo $user['full_name']; ?><br />
                <span class="salary"><?php echo empty($user['JobsUser']['salary']) ? $job['salary'] : $user['JobsUser']['salary']; ?></span> CHF / h
              </td>
              <td><?php echo $this->Form->input('start', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm input-xsmall timepicker-24', 'value' => $this->Time->format('G:i', empty($user['JobsUser']['effective_start_hour']) ? $job['start_time'] : $user['JobsUser']['effective_start_hour']))); ?></td>
              <td><?php echo $this->Form->input('end', array('label' => false, 'type' => 'text', 'class' => 'form-control input-sm input-xsmall timepicker-24', 'value' => $this->Time->format('G:i', empty($user['JobsUser']['effective_end_hour']) ? $job['end_time'] : $user['JobsUser']['effective_end_hour']))); ?></td>
              <td><?php echo $this->Form->input('break', array('label' => false, 'class' => 'form-control input-sm input-xsmall timepicker-24', 'value' => $this->Time->format('G:i', empty($user['JobsUser']['effective_break']) ? '00:00:00' : $user['JobsUser']['effective_break']))); ?></td>
              <td><?php echo $this->Form->input('salary', array('label' => false, 'class' => 'form-control input-sm input-xsmall salary', 'value' => empty($user['JobsUser']['salary']) ? $job['salary'] : $user['JobsUser']['salary'])); ?></td>
              <td class="hours"><?php echo !empty($user['JobsUser']['hours']) ? gmdate('H:i', floor($user['JobsUser']['hours'] * 3600)) : ''; ?></td>
              <td class="total"><?php echo sprintf('%s CHF', empty($user['JobsUser']['total']) ? 0 : $user['JobsUser']['total']); ?></td>
              <td><?php echo $this->Form->input('validated', array('type' => 'checkbox', 'label' => false, 'div' => false, 'checked' => $user['JobsUser']['validated'], 'data-user-id' => AuthComponent::user('id'))); ?></td>
            </tr>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>

<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'hours', 'notes' => $portletNotes)); ?>
