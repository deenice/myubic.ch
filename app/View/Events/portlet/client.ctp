<?php if(!empty($event['Client']['Logo'])): ?>
<?php echo $this->Html->image(DS . $event['Client']['Logo']['url'], array('class' => 'client-logo', 'alt' => $event['Client']['name'])); ?>
<hr>
<?php endif; ?>
<?php echo $event['Client']['name']; ?><br />
<?php echo $event['Client']['address']; ?><br />
<?php echo $event['Client']['zip_city']; ?><br />
<?php echo $countries[$event['Client']['country']]; ?>
<hr>
<?php echo empty($event['ContactPeople']['full_name']) ? '' : $event['ContactPeople']['full_name']; ?><br>
<?php echo empty($event['ContactPeople']['phone']) ? '' : $event['ContactPeople']['phone']; ?><br>
<?php echo empty($event['ContactPeople']['email']) ? '' : $event['ContactPeople']['email']; ?>

<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'client', 'notes' => $portletNotes)); ?>
