<div class="tiles">
  <?php foreach($downloads as $id => $download): ?>
    <?php if($download['modal']): ?>
    <a href="<?php echo $download['modal']; ?>" data-toggle="modal" data-target="#modal-download-<?php echo $id; ?>" data-href="<?php echo $download['href']; ?>" data-callback="<?php echo $download['callback']; ?>" data-download-item>
    <?php else: ?>
    <a href="javascript:;" data-href="<?php echo $download['href']; ?>" data-callback="<?php echo $download['callback']; ?>" data-download-item>
    <?php endif; ?>
      <div class="tile <?php echo $download['bg']; ?>">
        <div class="tile-body">
          <i class="<?php echo $download['icon']; ?>"></i>
        </div>
        <div class="tile-object">
          <div class="name"> <?php echo $download['title']; ?> </div>
        </div>
      </div>
    </a>
    <?php if($download['modal']): ?>
    
    <div class="modal" id="modal-download-<?php echo $id; ?>" data-download-modal>
      <div class="modal-dialog">
        <div class="modal-content"></div>
      </div>
    </div>
    
    <?php endif; ?>
  <?php endforeach; ?>
</div>
