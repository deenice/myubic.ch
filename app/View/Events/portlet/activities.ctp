<div class="row">
	<div class="col-md-12">
		<?php if(!empty($event['Activity'])): ?>
		<table class="table">
			<thead>
				<tr>
					<th><?php echo __('Activity'); ?></th>
					<th style="width: 20%" class="text-right"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($event['Activity'] as $activity): //debug($activity); ?>
				<tr class="<?php // echo $activity['ActivitiesEvent']['status'] == 'selected' ? 'success':''; ?>">
					<td class="title">

						<?php if($activity['standard']): ?>
							<?php if($activity['ActivitiesEvent']['status'] == 'selected'): ?>
								<i class="fa fa-trophy font-yellow-crusta"></i>&nbsp;
								<strong><?php echo $this->Html->link($activity['name'], array('controller' => 'activities', 'action' => 'view', $activity['id']), array('target' => '_blank')); ?></strong>
							<?php else: ?>
								<?php echo $this->Html->link($activity['name'], array('controller' => 'activities', 'action' => 'view', $activity['id']), array('target' => '_blank')); ?>
							<?php endif; ?>
						<?php else: ?>
							<?php if($activity['ActivitiesEvent']['status'] == 'selected'): ?>
								<i class="fa fa-trophy font-yellow-crusta"></i>&nbsp;
							<?php endif; ?>
							<?php echo $this->Form->input('non_standard_activity_name', array('div' => false, 'label' => false, 'id' => false, 'class' => 'form-control input-sm non-standard-activity', 'value' => empty($activity['ActivitiesEvent']['name']) ? $activity['name'] : $activity['ActivitiesEvent']['name'], 'data-activity-event-id' => $activity['ActivitiesEvent']['id'])); ?>
						<?php endif; ?>

						<?php if(!empty($activity['Game'])): ?><br />
						<small>
							<?php foreach($activity['Game'] as $k => $game): ?>
								<?php echo $game['name']; ?><?php echo $k < sizeof($activity['Game'])-1 ? ', ':''; ?>
							<?php endforeach; ?>
							</small>
						<?php endif; ?>
						<?php if($activity['ActivitiesEvent']['status'] == 'selected'): ?>
						<table class="table table-activity-infos">
							<thead>
								<tr>
									<th>Personnes</th>
									<th>Groupes</th>
									<th>Équipes</th>
									<th>Animateurs</th>
									<th>Pers / Équipe</th>
								</tr>
							</thead>
							<tbody>
								<tr data-activity-event-id="<?php echo $activity['ActivitiesEvent']['id']; ?>">
									<td><?php echo $this->Form->input('number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_persons', 'value' => empty($activity['ActivitiesEvent']['number_of_persons']) ? 0 : $activity['ActivitiesEvent']['number_of_persons'])); ?></td>
									<td><?php echo $this->Form->input('number_of_groups', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_groups', 'value' => empty($activity['ActivitiesEvent']['number_of_groups']) ? 0 : $activity['ActivitiesEvent']['number_of_groups'])); ?></td>
									<td><?php echo $this->Form->input('number_of_teams', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_teams', 'value' => empty($activity['ActivitiesEvent']['number_of_teams']) ? 0 : $activity['ActivitiesEvent']['number_of_teams'])); ?></td>
									<td><?php echo $this->Form->input('number_of_animators', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_animators', 'value' => empty($activity['ActivitiesEvent']['number_of_animators']) ? 0 : $activity['ActivitiesEvent']['number_of_animators'])); ?></td>
									<td><?php echo $this->Form->input('number_of_persons_per_team', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-sm', 'data-field' => 'number_of_persons_per_team', 'value' => empty($activity['ActivitiesEvent']['number_of_persons_per_team']) ? 0 : $activity['ActivitiesEvent']['number_of_persons_per_team'])); ?></td>
								</tr>
							</tbody>
						</table>
						<?php endif; ?>
					</td>
					<td class="actions text-right">
						<?php if($activity['ActivitiesEvent']['status'] == 'selected'): ?>
						<a href="javascript:;" class="btn default btn-sm update-orders tooltips" data-original-title="Mettre à jour les quantités de la commande" data-activity-event-id="<?php echo $activity['ActivitiesEvent']['id']; ?>"><i class="fa fa-refresh"></i></a>
						<a href="javascript:;" class="btn btn-default btn-sm select-activity bg-yellow-crusta font-white" data-activity-event-id="<?php echo $activity['ActivitiesEvent']['id']; ?>" data-status="selected"><i class="fa fa-trophy"></i></a>
						<?php else: ?>
						<a href="javascript:;" class="btn btn-default btn-sm select-activity" data-activity-event-id="<?php echo $activity['ActivitiesEvent']['id']; ?>" data-status="suggested"><i class="fa fa-trophy"></i></a>
						<?php endif; ?>
						<a href="javascript:;" class="btn btn-danger btn-sm delete-activity" data-activity-event-id="<?php echo $activity['ActivitiesEvent']['id']; ?>"><i class="fa fa-times"></i></a>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php else: ?>
			<div class="alert alert-warning">
				<p>
					Aucune activité n'a été proposée pour le moment.
				</p>
			</div>
		<?php endif; ?>
	</div>
</div>
<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'activities', 'notes' => $portletNotes)); ?>
