<div class="row">
	<div class="col-md-12">
		<div class="">
			<span class="bold uppercase"><?php echo __('Feedback'); ?></span>
			<br />
			<a href="#" id="feedback1" class="editable-disabled" data-type="textarea" data-pk="<?php echo $event['Event']['id']; ?>"><?php echo empty($event['Event']['feedback']) ? '' : $event['Event']['feedback']; ?></a>
		</div>
		<?php if(!empty($event['Document'])): ?>
			<span class="bold uppercase"><?php echo __('Documents'); ?></span>
			<ul class="list-unstyled">
			<?php foreach($event['Document'] as $doc): ?>
				<li>
					<span style="max-width: 85%; display: inline-block">
					<i class="fa fa-file-pdf-o"></i> <?php echo $this->Html->link($doc['name'], DS . $doc['url'], array('target' => '_blank')); ?>
					</span>
					<span class="pull-right">
						<a href="#" class="rename-document" data-document-name="<?php echo $doc['name']; ?>" data-document-id="<?php echo $doc['id']; ?>"><i class="fa fa-edit"></i></a>
						<a href="#" class="font-red delete-document" data-document-id="<?php echo $doc['id']; ?>"><i class="fa fa-times"></i></a>
					</span>
				</li>
			<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<span class="bold uppercase"><?php echo __('New documents'); ?></span>
		<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'documentsForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
		<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $event['Event']['id'])); ?>
		<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'event_document')); ?>
		<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'events')); ?>
		<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
		<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf,doc,docx,xls,xlsx')); ?>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'closing', 'notes' => $portletNotes)); ?>
