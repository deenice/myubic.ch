<?php if(!empty($searches)): ?>
<strong>Recherches effectuées pour cet évènement</strong>
<table class="table table-striped table-searches">
  <thead>
    <tr>
      <th>#</th>
      <th><?php echo __('Created on'); ?></th>
      <th><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($searches as $k => $search): ?>
    <tr>
      <td><?php echo $k+1; ?></td>
      <td><?php echo $this->Time->format($search['Search']['created'], '%d %B %Y'); ?></td>
      <td>
        <?php echo $this->Html->link(sprintf('<i class="fa fa-search"></i> %s', __('View results')), array('controller' => 'search', 'action' => 'places', $search['Search']['id']), array('class' => '', 'escape' => false, 'target' => '_blank')); ?>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<?php if(!empty($event['EventPlace'])): ?>
<table class="table table-striped table-places">
  <thead>
    <tr>
      <th class="name"><?php echo __('Name'); ?></th>
      <th class="option"><?php echo __('Option'); ?></th>
      <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($event['EventPlace'] as $ep): ?>
    <tr data-event-place-id="<?php echo $ep['id']; ?>">
      <td>
        <?php if(in_array($ep['option'], array('confirmed', 'managed_by_client'))): ?>
          <i class="fa fa-trophy font-yellow-crusta"></i>&nbsp;
          <strong>
        <?php endif; ?>
          <?php if($ep['Place']['private']): ?><i class="fa fa-lock"></i><?php endif; ?>
          <?php echo $this->Html->link($ep['Place']['name'], array('controller' => 'places', 'action' => 'view', $ep['Place']['id']), array('target' => '_blank')); ?>
          <?php if(in_array($ep['option'], array('confirmed', 'managed_by_client'))): ?>
          </strong>
        <?php endif; ?>
        <?php if(!empty($ep['Place']['to_check'])): ?>
          <br>
          <small><?php echo nl2br($ep['Place']['to_check']); ?></small>
        <?php endif; ?>
      </td>
      <td>
        <?php echo $this->Form->input('option', array('id' => 'option' . $ep['id'], 'class' => 'form-control bs-select input-xs', 'label' => false, 'options' => Configure::read('Places.options'), 'value' => empty($ep['option']) ? 'free' : $ep['option'])); ?>
        <?php if(!empty($ep['options'])): ?>
          <?php if($ep['all_cancelled']): ?>
            <small class="text-success"><i class="fa fa-check"></i>
              <?php echo __('This place is available.'); ?>
            </small>
          <?php else: ?>
            <small class="text-danger"><i class="fa fa-times"></i>
              <?php echo __('This place is not available.'); ?>
            </small>
          <?php endif; ?>
          <br>
          <?php foreach($ep['options'] as $option): ?>
           <small class="text-muted">
             <?php echo $this->Html->link($option['Event']['code'] . ' ' . $option['Event']['name'], array('controller' => 'events', 'action' => 'work', $option['Event']['id']), array('target' => '_blank')); ?> -
             <?php echo $this->Html->link($option['Client']['name'], array('controller' => 'clients', 'action' => 'view', $option['Client']['id']), array('target' => '_blank')); ?>: Option <strong><?php echo $placesOptions[$option['EventPlace']['option']]; ?></strong></small><br>
          <?php endforeach; ?>
        <?php else: ?>
          <small class="text-success"><i class="fa fa-check"></i> <?php echo __('This place is available.'); ?></small>
        <?php endif; ?>
      </td>
      <td class="actions">
        <?php if(!empty($ep['Place']['Note'])): ?>
          <?php echo $this->Html->link(sprintf('<i class="fa fa-files-o"></i>'), array('controller' => 'notes', 'action' => 'modal', 'list', 'Place', $ep['Place']['id']), array('class' => 'btn btn-sm btn-default', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-note-list')); ?>
        <?php endif; ?>
        <?php echo $this->Html->link(sprintf('<i class="fa fa-times"></i>'), 'javascript:;', array('class' => 'btn btn-sm btn-danger delete-option', 'escape' => false)); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'places', 'notes' => $portletNotes)); ?>

<div class="hidden">
<div class="panel-group accordion" id="accordion10">
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion10" href="#collapse10">
                Lieux avec une option et celui définitif
              </a>
          </h4>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
          <div class="panel-body">

          </div>
      </div>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion10" href="#collapse11">
                Lieux potentiels
              </a>
          </h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse">
          <div class="panel-body">

          </div>
      </div>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">
          <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion10" href="#collapse12">
                Résultats de recherche
              </a>
          </h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse">
          <div class="panel-body">
            <div class="row">
              <?php for($i=1; $i<=3; $i++): ?>
              <div class="col-md-4">
                <div class="portlet light bordered place-result">
                  <div class="ribbon ribbon-<?php echo ($i==2) ? 'yellow' : 'green'; ?> ribbon-small ribbon-xsmall">
                      <div class="banner">
                          <div class="text"></div>
                      </div>
                  </div>
                  <div class="portlet-body">
                    <small class="bold">Chateau de Mont à Mont-sur-Rolle</small>
                    <div class="row">
                      <div class="col-md-4">
                        <img class="img-responsive" src="http://images.gadmin.st.s3.amazonaws.com/n43656/fribourg/detail800/fribourgforum.jpg" alt="64x64" style="width: 64px; height: 64px;">
                      </div>
                      <div class="col-md-8">
                        <small>Caveau</small><br />
                        <small>3 salles, 110 PAX</small><br />
                        <small>34km, 28 min</small>
                        <div class="btn-group pull-right">
                          <a href="javascript:;" class="btn btn-xs font-grey-gallery tooltips" data-placement="top" data-original-title="Sélectionner ce lieu"><i class="fa fa-trophy"></i></a>
                          <a href="javascript:;" class="btn btn-xs font-grey-gallery"><i class="fa fa-check"></i></a>
                          <a href="javascript:;" class="btn btn-xs font-grey-gallery"><i class="fa fa-arrow-up"></i></a>
                          <a href="javascript:;" class="btn btn-xs font-grey-gallery"><i class="fa fa-arrow-down"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <?php endfor; ?>
            </div>
          </div>
      </div>
  </div>
</div>
</div>
