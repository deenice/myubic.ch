<?php $sectors = Configure::read('Competences.sectors'); ?>
<?php foreach($jobs as $sector => $jobss): ?>
<div class="panel-group accordion" id="accordion<?php echo $sector; ?>" data-jobs-accordion>
  <strong class="uppercase"><?php echo $sectors[$sector]; ?></strong>
  <?php foreach($jobss as $job): ?>
  <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="font-size: 1em">
            <div class="accordion-toggle">
              <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('controller' => 'jobs', 'action' => 'modal', $job['id'], $event['Event']['id']), array('escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-edit-job')); ?>
              <span data-toggle="collapse" data-parent="#accordion<?php echo $sector; ?>" href="#collapse<?php echo $job['id']; ?>">
                <?php echo $job['name']; ?>
                <?php echo $job['start_time']; ?> -
                <?php echo $job['end_time']; ?>
              </span>
              <div class="staff">
                <?php for($i=0; $i<$job['staff_needed']; $i++): ?>
                <?php if(!empty($job['User'][$i])): //debug($job['User']); ?>
                <?php if($job['User'][$i]['has_competences']): ?>
                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $job['User'][$i]['id'], $job['Event']['confirmed_date'])); ?>" data-toggle="modal" data-target="#modal-user-ajax" class="item tooltips" data-original-title="<?php echo $job['User'][$i]['full_name']; ?>">
                <?php else: ?>
                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $job['User'][$i]['id'], $job['Event']['confirmed_date'])); ?>" data-toggle="modal" data-target="#modal-user-ajax" class="item tooltips triangle triangle__top-left triangle--red" data-original-title="<?php echo $job['User'][$i]['full_name']; ?>">
                <?php endif; ?>
                  <?php if(!empty($job['User'][$i]['Portrait'][0]['url'])): ?>
                    <?php echo $this->Html->image($this->ImageSize->crop(DS . $job['User'][$i]['Portrait'][0]['url'], 100, 100)); ?>
                  <?php else: ?>
                    <span class="empty">
                      <span class="btn btn-icon-only font-green"> <i class="icon icon-user-follow"></i> </span>
                    </span>
                  <?php endif; ?>
                </a>
                <?php else: ?>
                <span class="empty">
                  <span class="btn btn-icon-only font-red"> <i class="icon icon-user-unfollow"></i> </span>
                </span>
                <?php endif; ?>
                <?php endfor; ?>
              </div>
            </div>
        </h4>
      </div>
      <div id="collapse<?php echo $job['id']; ?>" class="panel-collapse collapse" data-job-id="<?php echo $job['id']; ?>" data-event-id="<?php echo $event['Event']['id']; ?>">
        <div class="panel-body" style="min-height: 200px; position: relative;">
          <div class="loading"><div><i class="fa fa-spin fa-spinner fa-4x" style="font-size:4em !important;"></i></div></div>
          <div class="staff-container"></div>
        </div>
      </div>
  </div>
  <?php endforeach; ?>
</div>
<?php endforeach; ?>

<?php if(!empty($briefings)): ?>
<div class="panel-group accordion" id="briefings">
  <strong class="uppercase">Briefings</strong>
  <?php foreach($briefings as $briefing): ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <div class="accordion-toggle">
          <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('controller' => 'job_briefings', 'action' => 'modal', $briefing['JobBriefing']['id'], $event['Event']['id']), array('escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-add-job-briefing')); ?>
          <span href="#briefing<?php echo $briefing['JobBriefing']['id']; ?>" data-toggle="collapse" data-parent="#briefings" class="accordion-toggle-styled collapsed">
            <?php echo $briefing['JobBriefing']['name']; ?> - 
            <?php echo $briefing['Job']['name']; ?>
            
          </span>
        </div>
      </h4>
    </div>
    <div class="panel-collapse collapse" id="briefing<?php echo $briefing['JobBriefing']['id']; ?>">
      <div class="panel-body">
        <?php echo $briefing['JobBriefing']['content']; ?>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
</div>
<?php endif; ?>

<div class="panel-group accordion" id="routes">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#routes" href="#routes_1"><?php echo __('Routes'); ?></a>
      </h4>
    </div>
    <div id="routes_1" class="panel-collapse collapse">
      <div class="panel-body">
        <table class="table table-striped" data-routes>
          <thead>
            <tr>
              <th><?php echo __('Name'); ?></th>
              <th><?php echo __('Route'); ?></th>
              <th><?php echo __('Duration'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($event['Job'] as $job): ?>
              <?php foreach($job['User'] as $user): //debug($user); ?>
              <tr data-job-user-id="<?php echo $user['JobsUser']['id']; ?>">
                <td><?php echo $user['full_name']; ?></td>
                <td>
                  <div class="btn-group btn-group-xs" data-toggle="buttons">
                    <label class="btn btn-default<?php if($user['JobsUser']['route'] == 'yes'): ?> active<?php endif; ?>">
                      <input type="radio" class="toggle" value="yes"> <?php echo __('Yes'); ?> </label>
                    <label class="btn btn-default<?php if($user['JobsUser']['route'] == 'no'): ?> active<?php endif; ?>">
                      <input type="radio" class="toggle" value="no"> <?php echo __('No'); ?> </label>
                    <label class="btn btn-default<?php if($user['JobsUser']['route'] == 'duration'): ?> active<?php endif; ?>">
                      <input type="radio" class="toggle" value="duration"> <?php echo __('Duration'); ?> </label>
                  </div>
                </td>
                <td>
                  <?php echo $this->Form->input('duration', array('value' => is_null($user['JobsUser']['route_duration']) ? '0:00' : $this->Time->format('G:i', $user['JobsUser']['route_duration']), 'div' => false, 'label' => false, 'class' => 'form-control input-inline input-sm timepicker-24', 'data-default-time' => "true")); ?>
                </td>
              </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<?php echo $this->element('Portlets/add_note', array('model' => 'event', 'id' => $event['Event']['id'], 'category' => 'recruiting', 'notes' => $portletNotes)); ?>
