<?php $this->assign('page_title', $this->Html->link(__('Events'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="tabbable-custom" id="events_tabs">
	<ul class="nav nav-tabs">
		<li class="<?php echo $tab == 'offers' ? 'active':''; ?>">
			<a href="<?php echo Router::url(array('controller' => 'events', 'action' => 'index', 'offers', $slug)); ?>"><i class="fa fa-list"></i> <?php echo __('Offers'); ?></a>
		</li>
		<li class="<?php echo $tab == 'confirmed' ? 'active':''; ?>">
			<a href="<?php echo Router::url(array('controller' => 'events', 'action' => 'index', 'confirmed', $slug)); ?>"><i class="fa fa-check"></i> <?php echo __('Confirmed events'); ?></a>
		</li>
		<li class="<?php echo $tab == 'missions' ? 'active':''; ?>">
			<a href="<?php echo Router::url(array('controller' => 'events', 'action' => 'index', 'missions', $slug)); ?>"><i class="fa fa-crosshairs"></i> <?php echo __('Missions'); ?></a>
		</li>
		<li class="<?php echo $tab == 'calendar' ? 'active':''; ?>">
			<a href="<?php echo Router::url(array('controller' => 'events', 'action' => 'index', 'calendar', $slug)); ?>"><i class="fa fa-calendar"></i> <?php echo __('Calendar'); ?></a>
		</li>
		<li class="<?php echo $tab == 'past' ? 'active':''; ?>">
			<a href="<?php echo Router::url(array('controller' => 'events', 'action' => 'index', 'past', $slug)); ?>"><i class="fa fa-history"></i> <?php echo __('Past events'); ?></a>
		</li>
		<li class="pull-right dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
				<i class="fa fa-print"></i>
				<?php echo __('Print needs analysis'); ?>
				<i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu" role="menu">
				<?php foreach($needsAnalysis as $key => $value): ?>
				<li>
					<?php echo $this->Html->link($value, array('controller' => 'events', 'action' => 'tracking_sheet', 0, $key, 'analyse', 'ext' => 'pdf'), array('target'=> '_blank', 'escape' => false)); ?>
				</li>
				<?php endforeach; ?>
			</ul>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane <?php echo $tab == 'offers' ? 'active':''; ?>" id="offers">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="panel-group accordion" id="filters1">
				    <div class="panel panel-default">
				        <div class="panel-heading">
			            <h4 class="panel-title">
			              <a class="accordion-toggle accordion-toggle-styled <?php if(empty($this->request->params['named']['FilterOffer'])): ?>collapsed<?php endif; ?>" data-toggle="collapse" data-parent="#filters1" href="#filtersBlock1"> <?php echo __('Filter offers'); ?> </a>
			            </h4>
				        </div>
				        <div id="filtersBlock1" class="panel-collapse collapse <?php if(!empty($this->request->params['named']['FilterOffer'])): ?>in<?php endif; ?>">
			            <div class="panel-body">
										<?php echo $this->Form->create('FilterOffer'); ?>
			              <table class="table">
			              	<tbody>
			              		<tr>
			              			<td style="width: 20%"><?php echo __('Person in charge'); ?></td>
													<td>
														<?php echo $this->Form->input('FilterOffer.resp_id', array('label' => false, 'multiple' => 'checkbox', 'options' => $managers, 'class' => 'input-inline', 'value' => empty($this->request->params['named']['FilterOffer']['resp_id']) ? '' : $this->request->params['named']['FilterOffer']['resp_id'])); ?>
													</td>
			              		</tr>
												<tr>
													<td><?php echo __('Language of correspondence'); ?></td>
													<td>
														<?php echo $this->Form->input('FilterOffer.language', array('div' => false, 'label' => false, 'multiple' => 'checkbox', 'options' => $languages, 'class' => 'input-inline', 'escape' => false)); ?>
													</td>
												</tr>
												<tr>
													<td><?php echo __('Turnover'); ?></td>
													<td>
														<span class="help-inline"><?php echo __('From'); ?></span>
														<?php echo $this->Form->input('FilterOffer.min_turnover', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-small input-sm input-inline')); ?>
														<span class="help-inline"><?php echo __('to'); ?></span>
														<?php echo $this->Form->input('FilterOffer.max_turnover', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-small input-sm input-inline')); ?>
													</td>
												</tr>
												<tr>
													<td><?php echo __('CRM status'); ?></td>
													<td>
														<?php echo $this->Form->input('FilterOffer.crm_status', array('multiple' => 'checkbox', 'value' => $offerSetStatuses, 'options' => $crmStatuses, 'class' => 'input-inline', 'label' => false, 'div' => false)); ?>
													</td>
												</tr>
												<tr>
													<td>PAX</td>
													<td>
														<span class="help-inline"><?php echo __('From'); ?></span>
														<?php echo $this->Form->input('FilterOffer.min_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
														<span class="help-inline"><?php echo __('to'); ?></span>
														<?php echo $this->Form->input('FilterOffer.max_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
													</td>
												</tr>
												<tr>
													<td></td>
													<td>
														<?php echo $this->Form->button(sprintf('<i class="fa fa-check"></i> %s', __('Apply filters')), array('label' => false, 'type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false)); ?>
														<?php echo $this->Form->button(sprintf('<i class="fa fa-times"></i> %s', __('Reset filters')), array('label' => false, 'type' => 'reset', 'class' => 'btn btn-default', 'escape' => false)); ?>
													</td>
												</tr>
			              	</tbody>
			              </table>
										<?php echo $this->Form->end(); ?>
			            </div>
				        </div>
				    </div>
			    </div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="company">&nbsp;</th>
								<th>Resp.</th>
								<th>Dossier</th>
								<th>Ouverture</th>
								<th class="client">Client</th>
								<th>L.C.</th>
								<th>L.E.</th>
								<th>Date(s)</th>
								<th>PAX</th>
								<th>Feeling</th>
								<th>Statut CRM</th>
								<th>CA (CHF)</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($offers as $event): ?>
							<tr>
								<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
								<td><?php echo $event['PersonInCharge']['first_name']; ?></td>
								<td><?php echo $this->Html->link(empty($event['Event']['code']) ? 'xxx' : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
								<td><?php echo $this->Time->format('d.m.Y', empty($event['Event']['opening_date']) ? $event['Event']['created'] : $event['Event']['opening_date']); ?></td>
								<td>
                  <?php if(($event['Event']['company_id'] == 4 && $event['Client']['id'] == 2986) || ($event['Event']['company_id'] == 5 && $event['Client']['id'] == 2972)): ?>
                    <?php echo $this->Html->link($this->Text->truncate($event['Event']['name'], 35), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?>
                  <?php else: ?>
                    <?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 35), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?>
                  <?php endif; ?>
                </td>
								<td><?php echo $this->element('flag', array('language' => $event['Event']['language'])); ?></td>
								<td>
									<?php if(!empty($event['Event']['languages'])): $langs = explode(',', $event['Event']['languages']);?>
										<?php foreach($langs as $lang): ?>
											<?php echo $this->element('flag', array('language' => $lang)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['Event']['confirmed_date'])): ?>
										<?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?>
									<?php elseif(!empty($event['Date'])): ?>
										<?php foreach($event['Date'] as $k => $date): ?>
											<?php echo $this->Time->format('d.m.Y', $date['date']); ?>
											<?php echo !empty($event['Date'][$k+1]) ? __('or') : ''; ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['Event']['period_start'])): ?>
										<?php echo $this->Time->format('d.m.Y', $event['Event']['period_start']); ?> <?php echo __('to'); ?> <?php echo $this->Time->format('d.m.Y', $event['Event']['period_end']); ?>
									<?php endif;?>
								</td>
								<td>
									<?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
										<?php echo $event['Event']['confirmed_number_of_persons']; ?>
									<?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
										<?php echo $event['Event']['min_number_of_persons']; ?>
										<?php if(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
											 - <?php echo $event['Event']['max_number_of_persons']; ?>
										<?php endif; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php echo ($event['Event']['feeling'] != 0) ? sprintf('%s%%', round($event['Event']['feeling'],0)) : ''; ?>
								</td>
								<td>
									<a href="javascript:;" data-crm-status-editable data-mode="inline" data-showbuttons="false" data-value="<?php echo empty($event['Event']['crm_status']) ? '' : $event['Event']['crm_status']; ?>" data-type="select" data-pk="<?php echo $event['Event']['id']; ?>">
										<?php echo !empty($event['Event']['crm_status']) ? $crmStatuses[$event['Event']['crm_status']] : ''; ?>
									</a>
								<td><?php echo !empty($event['Event']['projected_turnover']) ? sprintf('%s', $event['Event']['projected_turnover']) : ''; ?></td>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane <?php echo $tab == 'confirmed' ? 'active':''; ?>" id="confirmed">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="panel-group accordion" id="filters">
				    <div class="panel panel-default">
				        <div class="panel-heading">
			            <h4 class="panel-title">
										<a class="accordion-toggle accordion-toggle-styled <?php if(empty($this->request->params['named']['FilterConfirmed'])): ?>collapsed<?php endif; ?>" data-toggle="collapse" data-parent="#filters" href="#filtersBlock"> <?php echo __('Filter confirmed events'); ?> </a>
			            </h4>
				        </div>
								<div id="filtersBlock" class="panel-collapse collapse <?php if(!empty($this->request->params['named']['FilterConfirmed'])): ?>in<?php endif; ?>">
			            <div class="panel-body">
										<?php echo $this->Form->create('FilterConfirmed'); ?>
			              <table class="table">
			              	<tbody>
			              		<tr>
			              			<td style="width: 20%"><?php echo __('Person in charge'); ?></td>
													<td>
														<?php echo $this->Form->input('FilterConfirmed.resp_id', array('label' => false, 'multiple' => 'checkbox', 'options' => $managers, 'class' => 'input-inline', 'value' => $this->request->params['named']['FilterConfirmed']['resp_id'])); ?>
													</td>
			              		</tr>
												<tr>
													<td><?php echo __('Language during event'); ?></td>
													<td>
														<?php echo $this->Form->input('FilterConfirmed.languages', array('div' => false, 'label' => false, 'multiple' => 'checkbox', 'options' => $eventLanguages, 'class' => 'input-inline', 'escape' => false)); ?>
													</td>
												</tr>
												<tr>
													<td><?php echo __('Content'); ?></td>
													<td>
														<span class="label label-warning">En développement</span>
														<?php //echo $this->Form->input('FilterConfirmed.content_id', array('div' => false, 'label' => false, 'multiple' => 'checkbox', 'class' => 'input-inline', 'options' => array('bts', 'mg'))); ?>
													</td>
												</tr>
												<tr>
													<td>PAX</td>
													<td>
														<span class="help-inline"><?php echo __('From'); ?></span>
														<?php echo $this->Form->input('FilterConfirmed.min_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
														<span class="help-inline"><?php echo __('to'); ?></span>
														<?php echo $this->Form->input('FilterConfirmed.max_number_of_persons', array('label' => false, 'div' => false, 'type' => 'number', 'class' => 'form-control input-xsmall input-sm input-inline')); ?>
													</td>
												</tr>
												<tr>
													<td>Date(s)</td>
													<td>
														<div class="input-group input-small date-picker input-daterange">
						                  <?php echo $this->Form->input('FilterConfirmed.start_date', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'type' => 'text')); ?>
						                  <span class="input-group-addon"> <?php echo __('to'); ?> </span>
							                  <?php echo $this->Form->input('FilterConfirmed.end_date', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'type' => 'text')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td></td>
													<td>
														<?php echo $this->Form->button(sprintf('<i class="fa fa-check"></i> %s', __('Apply filters')), array('label' => false, 'type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false)); ?>
														<?php echo $this->Form->button(sprintf('<i class="fa fa-times"></i> %s', __('Reset filters')), array('label' => false, 'type' => 'reset', 'class' => 'btn btn-default', 'escape' => false)); ?>
													</td>
												</tr>
			              	</tbody>
			              </table>
										<?php echo $this->Form->end(); ?>
			            </div>
				        </div>
				    </div>
			    </div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th><?php echo $this->Paginator->sort('PersonInCharge.first_name', 'Resp.'); ?></th>
								<th><?php echo $this->Paginator->sort('Event.code', 'Dossier'); ?></th>
								<th class="name">Nom</th>
								<th>Client</th>
								<th class="content">Contenu</th>
								<th>L.E.</th>
								<th class="date">Date(s)</th>
								<th>PAX</th>
								<th>Lieu(x)</th>
								<th>Poste(s)</th>
								<th class="hidden">Suivi</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($confirmed as $event): $place = ''; ?>
							<tr class="danger1">
								<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
								<td><a href="#"><?php echo $event['PersonInCharge']['first_name']; ?></a></td>
								<td><?php echo $this->Html->link(empty($event['Event']['code']) ? 'xxx' : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
								<td><?php echo $this->Text->truncate($event['Event']['name'], 20); ?></td>
								<td><?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 20), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?></td>
								<td>
									<!-- <span class="btn btn-default btn-xs"><i class="fa fa-truck"></i> Test</span> -->
									<?php if(!empty($event['SelectedActivity'])): ?>
										<?php foreach($event['SelectedActivity'] as $activity): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', empty($activity['Activity']['slug']) ? $this->Myubic->acronym($activity['Activity']['name']) : $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['SuggestedActivity'])): ?>
										<?php foreach($event['SuggestedActivity'] as $activity): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', empty($activity['Activity']['slug']) ? $this->Myubic->acronym($activity['Activity']['name']) : $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<?php if(!empty($event['SelectedFBModule'])): ?>
										<?php foreach($event['SelectedFBModule'] as $fb_module): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? $this->Myubic->acronym($fb_module['FBModule']['name']):$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['SuggestedFBModule'])): ?>
										<?php foreach($event['SuggestedFBModule'] as $fb_module): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? $this->Myubic->acronym($fb_module['FBModule']['name']):$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<?php if(!empty($event['VehicleReservation'])): ?>
										<?php foreach($event['VehicleReservation'] as $reservation): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-truck"></i> %s', $reservation['slug']), array('controller' => 'vehicle_reservations', 'action' => 'view', $reservation['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['Event']['languages'])): $langs = explode(',', $event['Event']['languages']);?>
										<?php foreach($langs as $lang): if(empty($lang)) continue;?>
											<?php echo $this->element('flag', array('language' => $lang)); ?>&nbsp;
										<?php endforeach; ?>
									<?php endif; ?>
								</td>
								<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></td>
								<td>
									<?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
										<?php echo $event['Event']['confirmed_number_of_persons']; ?>
									<?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
										<?php echo $event['Event']['min_number_of_persons']; ?>
										<?php if(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
											 - <?php echo $event['Event']['max_number_of_persons']; ?>
										<?php endif; ?>
									<?php elseif(empty($event['Event']['confirmed_number_of_persons']) && empty($event['Event']['min_number_of_persons']) && !empty($event['Event']['offer_number_of_persons'])): ?>
										<span class="font-grey-silver"><?php echo $event['Event']['offer_number_of_persons']; ?></span>
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['ConfirmedEventPlace'])): ?>
					          <?php foreach($event['ConfirmedEventPlace'] as $k => $cep): ?>
					            <?php echo $cep['Place']['name']; ?><?php if(!empty($cep['Place']['zip_city'])): ?>, <?php echo $cep['Place']['zip_city']; ?><?php endif; ?>
					            <?php if(isset($event['ConfirmedEventPlace'][$k+1])): ?><br><?php endif; ?>
					          <?php endforeach; ?>
					        <?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['Event']['number_of_jobs'])): ?>
										<?php echo $event['Event']['number_of_filled_jobs']; ?> / <?php echo $event['Event']['number_of_jobs']; ?>
									<?php endif; ?>
								</td>
								<td class="hidden">67%</td>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane <?php echo $tab == 'calendar' ? 'active':''; ?>" id="calendar">
			<div class="row">
				<div class="col-md-12">
					<div class="portlet light bordered">
						<div class="portlet-title">
							<div class="caption">
								<span class="uppercase bold"><?php echo __('Filters'); ?></span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row filters filters1">
								<div class="col-md-2">
									<?php echo $this->Form->input('crm_status', array('label' => __('Status'), 'type' => 'select', 'options' => Configure::read('Events.crm_status'), 'empty' => __('Select a status'), 'class' => 'form-control bs-select', 'data-field' => 'crm_status', 'multiple' => true)); ?>
								</div>
								<div class="col-md-2">
									<?php echo $this->Form->input('event_manager', array('type' => 'select', 'options' => $managers, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'resp_id')); ?>
								</div>
								<div class="col-md-2">
									<?php echo $this->Form->input('event_category_id', array('type' => 'select', 'label' => __('Category'), 'options' => $categories, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'event_category_id')); ?>
								</div>
								<div class="col-md-3">
									<?php echo $this->Form->input('company_id', array('type' => 'select', 'label' => __('Company'), 'options' => $companies, 'multiple' => true, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'company_id', 'value' => empty($companyIds) ? array(2,6) : $companyIds)); ?>
								</div>
								<div class="col-md-3">
									<?php echo $this->Form->input('type', array('type' => 'select', 'label' => __('Type'), 'options' => array('standard' => __('Event'), 'mission' => __('Mission')), 'multiple' => true, 'empty' => true, 'class' => 'form-control bs-select', 'data-field' => 'type', 'value' => empty($types) ? array('standard', 'mission') : $types)); ?>
								</div>
								<div class="col-md-2 hidden">
									<?php echo $this->Form->input('place', array('type' => 'select', 'options' => $places, 'empty' => true, 'class' => 'form-control bs-select', 'data-field' => 'place_id')); ?>
								</div>
								<div class="col-md-3 hidden">
									<?php echo $this->Form->input('activity', array('type' => 'select', 'options' => $activities, 'empty' => true, 'class' => 'form-control bs-select', 'data-live-search' => true, 'data-field' => 'activity_id')); ?>
								</div>
								<div class="col-md-3 hidden">
									<?php echo $this->Form->input('moment', array('type' => 'select', 'options' => array('standard' => 'Sans préparation/rangement', 'complex' => 'Avec préparation/rangement'), 'class' => 'form-control bs-select', 'value' => 'standard', 'data-field' => 'moment')); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="portlet light calendar" id="eventsCalendarPortlet">
						<div class="portlet-title"></div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">
									<div id="eventsCalendar"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="context">
				<ul class="dropdown-menu" role="menu" style="/*height: 400px; overflow-y: scroll*/">
					<li class="disabled uppercase bold"><a href="#">Event manager</a></li>
			    <li class=""><a href="#">Floriane Pochon</a></li>
			    <li class="divider"></li>
					<li class="disabled uppercase bold"><a href="#">Place</a></li>
			    <li class=""><a href="#">Forum Fribourg</a></li>
			    <li class="divider"></li>
					<li class="disabled uppercase bold"><a href="#">Activity</a></li>
			    <li class=""><a href="#">Pursuit Gaming</a></li>
					<li class="disabled uppercase bold"><a href="#">Actions</a></li>
			    <li class=""><a href="#">Trouver du personnel</a></li>
			    <li class=""><a href="#">Trouver un lieu</a></li>
			    <li class=""><a href="#">Imprimer feuille palette</a></li>
				</ul>
			</div>
		</div>
		<div class="tab-pane <?php echo $tab == 'past' ? 'active':''; ?>" id="past">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="col-md-12">
						<div class="pull-right">
							<ul class="pagination pagination-sm">
								<li>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-angle-left"></i> %s', $year - 1), array('controller' => 'events', 'action' => 'index', 'past', 'year' => $year-1), array('escape' => false)); ?>
								</li>
								<li class="active">
									<?php echo $this->Html->link($year, array('controller' => 'events', 'action' => 'index', 'past', 'year' => $year)); ?>
								</li>
								<li>
									<?php echo $this->Html->link(sprintf('%s <i class="fa fa-angle-right"></i>', $year + 1), array('controller' => 'events', 'action' => 'index', 'past', 'year' => $year + 1), array('escape' => false)); ?>
								</li>
							</ul>
						</div>
					</div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th><?php echo $this->Paginator->sort('PersonInCharge.first_name', 'Resp.'); ?></th>
								<th><?php echo $this->Paginator->sort('Event.code', 'Dossier'); ?></th>
								<th>Nom</th>
								<th>Client</th>
								<th>Date</th>
								<th>Contrôle</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($past as $event): //debug($event['Activity']); ?>
							<tr>
								<td>
									<span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span>
								</td>
								<td><a href="#"><?php echo $event['PersonInCharge']['first_name']; ?></a></td>
								<td>
									<?php echo $this->Html->link(empty($event['Event']['code']) ? __('Edit') : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?>
								</td>
								<td>
									<?php if($event['Event']['type'] == 'mission'): ?>
										<i class="fa fa-crosshairs"></i>
									<?php endif; ?>
									<?php echo $this->Text->truncate($event['Event']['name'], 30); ?>
								</td>
								<td><?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 30), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?></td>
								<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></td>
								<td>
									<!-- feedback, compta, heures -->
									<?php if($event['Event']['type'] == 'standard'): ?>
										<?php if(empty($event['Event']['feedback'])): ?>
											<span class="badge badge-warning badge-roundless"><i class="fa fa-warning"></i> <?php echo __('Feedback'); ?> </span>
										<?php else: ?>
											<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Feedback'); ?> </span>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($event['Event']['number_of_jobs']): ?>
										<?php if($event['Event']['number_of_validated_jobs']  == 0): ?>
											<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Hours'); ?> </span>
										<?php elseif($event['Event']['number_of_validated_jobs'] < $event['Event']['number_of_filled_jobs']): ?>
											<span class="badge badge-warning badge-roundless"><i class="fa fa-warning"></i> <?php echo __('Hours'); ?> </span>
										<?php elseif($event['Event']['number_of_validated_jobs'] >= $event['Event']['number_of_filled_jobs']): ?>
											<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Hours'); ?> </span>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($event['Event']['type'] == 'standard'): ?>
										<?php if($event['Event']['accounting_match']): ?>
										<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Accounting'); ?> </span>
										<?php else: ?>
										<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Accounting'); ?> </span>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($event['Event']['checklists_complete']): ?>
									<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Tasks'); ?> </span>
									<?php else: ?>
									<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Tasks'); ?> </span>
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="8" class="text-right">
									<div class="dataTables_paginate paging_simple_numbers">
										<?php echo $this->Paginator->numbers(array('first' => 'Première page')); ?>
									</div>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<div class="tab-pane <?php echo $tab == 'missions' ? 'active':''; ?>" id="missions">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="col-md-12">
						<div class="pull-right">
							<ul class="pagination pagination-sm">
								<li>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-angle-left"></i> %s', $year - 1), array('controller' => 'events', 'action' => 'index', 'missions', 'year' => $year-1), array('escape' => false)); ?>
								</li>
								<li class="active">
									<?php echo $this->Html->link($year, array('controller' => 'events', 'action' => 'index', 'missions', 'year' => $year)); ?>
								</li>
								<li>
									<?php echo $this->Html->link(sprintf('%s <i class="fa fa-angle-right"></i>', $year + 1), array('controller' => 'events', 'action' => 'index', 'missions', 'year' => $year + 1), array('escape' => false)); ?>
								</li>
							</ul>
						</div>
					</div>
					<table class="table table-striped">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Resp.</th>
								<th>Nom</th>
								<th>Date</th>
								<th>Horaire</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($missions as $mission): //debug($event['Activity']); ?>
							<tr>
								<td>
									<span class="btn btn-company btn-company-<?php echo $mission['Company']['class']; ?> btn-xs"></span>
								</td>
								<td><a href="#"><?php echo $mission['PersonInCharge']['first_name']; ?></a></td>
								<td>
									<?php echo $this->Html->link($mission['Event']['name'], array('controller' => 'events', 'action' => 'work', $mission['Event']['id'])); ?>
								</td>
								<td><?php echo $this->Time->format($mission['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></td>
								<td>
									<?php echo $this->Time->format('H:i', $mission['Event']['start_hour']); ?> -
									<?php echo $this->Time->format('H:i', $mission['Event']['end_hour']); ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.events();';
$this->end();
?>
