<?php
foreach($events as $event){
	$id = $event['Event']['id'];
	$date = $event['Event']['confirmed_date'];
	$hour1 = $event['Event']['start_hour'];
	$hour2 = $event['Event']['end_hour'];
	$seconds1 = strtotime("1970-01-01 $hour1 UTC");
	$seconds2 = strtotime("1970-01-01 $hour2 UTC");
	$start = gmdate('Ymd\THis\Z', strtotime($date) + $seconds1);
	if($hour2 < $hour1){
		$end = gmdate('Ymd\THis\Z', strtotime($date . "+1 day") + $seconds2);
	} else {
		$end = gmdate('Ymd\THis\Z', strtotime($date) + $seconds2);
	}
	$location = '';
	if(!empty($event['ConfirmedEventPlace'])){
		$location = $event['ConfirmedEventPlace'][0]['Place']['name'];
		if(!empty($event['ConfirmedEventPlace'][0]['Place']['address'])) $location .= ', ' . $event['ConfirmedEventPlace'][0]['Place']['address'];
		if(!empty($event['ConfirmedEventPlace'][0]['Place']['zip_city'])) $location .= ', ' . $event['ConfirmedEventPlace'][0]['Place']['zip_city'];
	}
	$summary = '';
	if(in_array($event['Event']['crm_status'], array_keys(Configure::read('Events.crm_status_default')))){
		$summary .= 'OPTION ';
	}
	$summary .= $event['Event']['code_name'];
	if(!empty($event['Client'])){
		$summary .= "\\n" . $event['Client']['name'];
	}
	if(!empty($event['Event']['confirmed_number_of_persons'])){
		$summary .= "\\n" . $event['Event']['confirmed_number_of_persons'] . ' PAX';
	}
	if(!empty($event['Event']['min_number_of_persons']) && empty($event['Event']['confirmed_number_of_persons'])){
		$summary .= "\\n" . $event['Event']['min_number_of_persons'];
		if(!empty($event['Event']['max_number_of_persons']) && $event['Event']['max_number_of_persons'] != $event['Event']['min_number_of_persons']){
			$summary .= ' - ' . $event['Event']['min_number_of_persons'];
		}
		$summary .= ' PAX';
	}
	if(!empty($event['SelectedActivity'])){
		$summary .= "\\n";
		foreach($event['SelectedActivity'] as $k => $activity){
			if(empty($activity['Activity']['slug'])){
				continue;
			}
			$summary .= $activity['Activity']['slug'];
			if(isset($event['SelectedActivity'][$k+1])){
				$summary .= ', ';
			}
		}
	}
	if(!empty($event['SuggestedActivity']) && empty($event['SelectedActivity'])){
		$summary .= "\\n";
		foreach($event['SuggestedActivity'] as $k => $activity){
			if(empty($activity['Activity']['slug'])){
				continue;
			}
			$summary .= $activity['Activity']['slug'];
			if(isset($event['SuggestedActivity'][$k+1])){
				$summary .= ', ';
			}
		}
	}
	$data = array(
		'start' => $start,
		'end' => $end,
		'summary' => $summary,
		'description' => $event['Company']['name'] . "\\n" . $event['Client']['name'] . "\\n" . Router::url(array('controller' => 'events', 'action' => 'work', $id), true),
		'organizer' => $event['PersonInCharge']['full_name'],
		'class' => 'public',
		'timestamp' => time(),
		'id' => 'event-'.$id,
		'location' => $location,
		'timezone' => 'Europe/Berlin'
	);
	$this->Ical->add($data);
}
echo $this->Ical->generate();
