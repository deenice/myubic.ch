<?php $this->assign('page_title', __('Festiloc')); ?>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-map"></i><?php echo __('Depot plan'); ?>
		</div>
		<div class="tools">
			<span class="badge badge-roundless bg-red" style="margin-left: 20px">&nbsp;&nbsp;</span> Point de stockage
			<span class="badge badge-roundless bg-green" style="margin-left: 20px">&nbsp;&nbsp;</span> Passage
			<span class="badge badge-roundless bg-grey" style="margin-left: 20px">&nbsp;&nbsp;</span> Bloc
			<span class="badge badge-roundless bg-yellow" style="margin-left: 20px">&nbsp;&nbsp;</span> Non défini
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-2 pull-right">
				<button class="btn btn-block btn-primary initWarehouse" data-warehouse-id="<?php echo $id; ?>"><i class="fa fa-cogs"></i> Initialisation</button>
			</div>
		</div>
		<div class="row" style="margin-top: 20px;">
			<div class="col-md-12">
				<table class="table depot" data-warehouse-id="<?php echo $id; ?>">
					<?php for($i=0;$i<=27;$i++): ?>
					<tr>
						<?php for($j=0;$j<=27;$j++): ?>
							<?php if(!empty($cells[$j][$i])): $cell =  $cells[$j][$i]['cell']; $content = ''; ?>
								<?php if($cell['WarehouseCell']['flag'] == 1){
									$bgClass = 'bg-red storage-point ' . $cell['WarehouseCell']['orientation'];
									$content = $cells[$j][$i]['sp']['WarehouseStoragePoint']['id'];
									} ?>
								<?php if($cell['WarehouseCell']['flag'] == 2) $bgClass = 'bg-green passage'; ?>
								<?php if($cell['WarehouseCell']['flag'] == 3) $bgClass = 'bg-grey block'; ?>
								<?php if($cell['WarehouseCell']['flag'] == 4) $bgClass = 'bg-yellow undefined'; ?>
								<?php if(!empty($cells[$j][$i]['node'])){
									$bgClass = 'bg-green node';
									$content = $cells[$j][$i]['node']['WarehouseNode']['id'];
									//$content = '&nbsp;';
								} ?>
								<td data-x="<?php echo $j; ?>" data-y="<?php echo $i; ?>" data-id="<?php echo $cell['WarehouseCell']['id']; ?>" class="<?php echo $bgClass; ?>"><?php //echo $content; ?><small><?php //echo $j; ?>,<?php //echo $i; ?></small></td>
							<?php else: ?>
								<td data-x="<?php echo $j; ?>" data-y="<?php echo $i; ?>" data-id="" data-adjacent="">&nbsp;</td>
							<?php endif; ?>
						<?php endfor; ?>
					</tr>
					<?php endfor; ?>
				</table>	
			</div>
		</div>
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css');
echo $this->Html->css('/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css');
echo $this->Html->css('/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/pages/scripts/table-advanced.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.warehouse();';
$this->end();
?>