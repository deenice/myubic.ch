<?php

foreach ($dtResults as $result) {

	// $actions = $this->Html->link(
	//     '<i class="fa fa-search"></i> ' . __("View"),
	//     array('controller' => 'activities', 'action' => 'view', $result['Activity']['id']),
	//     array('class' => 'btn default btn-xs', 'escape' => false)
	// );
	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'myjeu', 'action' => 'edit', $result['Myjeu']['id']),
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $tags = '';
  if(!empty($result['Tag'])){
    foreach($result['Tag'] as $k => $tag){
      $tags .= $this->Html->tag('span', sprintf('<i class="fa fa-tag"></i> %s', $tag['value']), array('class' => 'badge badge-roundless badge-default', 'escape' => false));
      $tags .= '&nbsp;';
    }
  }

  $this->dtResponse['aaData'][] = array(
      $result['Myjeu']['name'],
      $result['Myjeu']['min_pax'] . ' / ' . $result['Myjeu']['max_pax'],
      $tags,
      $actions
  );
}
