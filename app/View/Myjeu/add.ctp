<?php $this->assign('page_title', $this->Html->link(__('Myjeu'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add content'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-dropbox font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add content'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Myjeu', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('PAX'); ?></label>
					<div class="col-md-9">
						<span class="help-inline"><?php echo __('From'); ?></span>
						<?php echo $this->Form->input('min_pax', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small')); ?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('max_pax', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Material?'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('material', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Bibliogaphy'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bibliography', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Tags'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('tags', array('type' => 'select', 'multiple' => true, 'data-select2-tags' => '', 'class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
$this->end();
$this->start('init_scripts');
//echo 'Custom.myjeu();';
$this->end();
?>
