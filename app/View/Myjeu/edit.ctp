<?php $this->assign('page_title', $this->Html->link(__('Myjeu'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $myjeu['Myjeu']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title tabbable-line">
		<div class="caption">
			<i class="fa fa-dropbox font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $myjeu['Myjeu']['name']; ?></span>
		</div>
		<ul class="nav nav-tabs">
			<li>
				<a href="#infos" data-toggle="tab"> <?php echo __('General data'); ?> </a>
			</li>
			<li>
				<a href="#images" data-toggle="tab"> <?php echo __('Images'); ?> </a>
			</li>
			<li>
				<a href="#documents" data-toggle="tab"> <?php echo __('Documents'); ?> </a>
			</li>
    </ul>
	</div>
	<div class="portlet-body form">
		<div class="tab-content">
			<div class="tab-pane" id="infos">
				<!-- BEGIN FORM-->
				<?php echo $this->Form->create('Myjeu', array('class' => 'form-horizontal')); ?>
				<?php echo $this->Form->input('id'); ?>
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('PAX'); ?></label>
							<div class="col-md-9">
								<span class="help-inline"><?php echo __('From'); ?></span>
								<?php echo $this->Form->input('min_pax', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small')); ?>
								<span class="help-inline"><?php echo __('to'); ?></span>
								<?php echo $this->Form->input('max_pax', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Material?'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('material', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Bibliogaphy'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('bibliography', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Tags'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('tags', array('type' => 'select', 'multiple' => true, 'data-select2-tags' => '', 'class' => 'form-control', 'label' => false, 'value' => (!empty($myjeutags)) ? $myjeutags:'')); ?>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
								<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							</div>
						</div>
					</div>
				<?php echo $this->Form->end(); ?>
				<!-- END FORM-->
			</div>
			<div class="tab-pane" id="images">
				<?php if(!empty($documents['images'])): ?>
				<div class="row mix-grid">
					<div class="col-md-12">
						<h3><?php echo __('Existing images'); ?></h3>
						<div class="cbp" data-cube-portfolio>
							<?php foreach($documents['images'] as $k => $doc): ?>
								<div class="cbp-item mix">
									<a href="<?php echo Router::url(DS . $doc['Document']['url']); ?>" class="cbp-caption cbp-lightbox" data-title="<?php echo $doc['Document']['name']; ?>">
										<div class="cbp-caption-defaultWrap">
											<?php echo $this->Html->image($this->ImageSize->crop(DS . $doc['Document']['url'], 400, 250)); ?>
										</div>
										<div class="cbp-caption-activeWrap">
											<div class="cbp-l-caption-alignLeft">
												<div class="cbp-l-caption-body">
													<div class="cbp-l-caption-title">
														<?php echo $doc['Document']['name']; ?>
														<div class="actions pull-right">
															<span class="btn btn-circle btn-icon-only delete-document" data-document-id="<?php echo $doc['Document']['id']; ?>">
																<i class="fa fa-trash-o"></i>
															</span>
														</div>
													</div>
													<div class="cbp-l-caption-desc"><?php echo __('added by %s', $doc['User']['full_name']); ?></div>
												</div>
											</div>
										</div>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-12">
						<h3><?php echo __('New images'); ?></h3>
						<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
						<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Myjeu']['id'])); ?>
						<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'myjeu_image')); ?>
						<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'myjeu')); ?>
						<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
						<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="documents">
				<?php if(!empty($documents['documents'])): ?>
				<div class="row mix-grid">
					<div class="col-md-12">
						<h3><?php echo __('Existing documents'); ?></h3>
						<?php foreach($documents['documents'] as $k => $doc): ?>
							<?php $title = $doc['Document']['name']; ?>
							<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
							<div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
								<div class="mix-inner">
									<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title"><?php echo $title; ?></h4>
											<small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
										</div>
										<div class="panel-body text-center">
											 <div class="btn-group">
												<?php echo $this->Html->link(
													'<i class="fa fa-eye"></i> ' . __('See'),
													DS . $doc['Document']['url'],
													array(
														'escape' => false,
														'class' => 'fancybox-button btn btn-default btn-xs',
														'title' => $title,
														'data-rel' => 'fancybox-button',
														'target' => '_blank'
													)
												); ?>
												<?php echo $this->Html->link(
													'<i class="fa fa-times"></i> ' . __('Remove'),
													DS . $doc['Document']['url'],
													array(
														'escape' => false,
														'class' => 'btn btn-default btn-xs delete-document',
														'data-document-id' => $doc['Document']['id']
													)
												); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="myjeuDocument<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Edit document</h4>
										</div>
										<div class="modal-body">
											<div class="row">
												<div class="col-md-12">
												<?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
												</div>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
											<button type="button" class="btn blue edit-document">Save</button>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<?php endif; ?>
				<div class="row">
					<div class="col-md-12">
						<h3><?php echo __('New documents'); ?></h3>
						<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
						<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Myjeu']['id'])); ?>
						<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'myjeu_document')); ?>
						<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'myjeu')); ?>
						<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
						<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf,doc,docx')); ?>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
echo $this->Html->css('/metronic/theme/assets/global/plugins/cubeportfolio/css/cubeportfolio.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js');
$this->end();
$this->start('init_scripts');
echo 'Portfolio.init();';
$this->end();
?>
