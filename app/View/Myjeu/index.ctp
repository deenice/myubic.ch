<?php $this->assign('page_title', __('Myjeu')); ?>
<?php $this->assign('page_subtitle', __('Filter')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-search"></i><?php echo __('Filter'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add content'), array('controller' => 'myjeu', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->Form->create('MyjeuSearch', array('class' => 'form-horizontal')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Tags'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('tags', array('type' => 'select', 'multiple' => true, 'data-select2-tags' => '', 'class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('PAX'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('pax', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue"><i class="fa fa-search"></i> <?php echo __('Search'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<hr>
		<?php echo $this->DataTable->render('Filters'); ?>
	</div>
</div>
