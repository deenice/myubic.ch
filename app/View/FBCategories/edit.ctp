<?php $this->assign('page_title', $this->Html->link(__('F&B Categories'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $this->request->data['FBCategory']['name']);?>

<?php echo $this->Form->create('FBCategory', array('class' => 'form-horizontal')); ?>
<?php echo $this->Form->input('FBCategory.id'); ?>

<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-cup font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['FBCategory']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete')), array('controller' => 'f_b_categories', 'action' => 'delete', $this->request->data['FBCategory']['id']), array('escape' => false, 'class' => 'btn btn-danger bootbox')); ?>
					</div>
				</div>
			</div>
		<!-- END FORM-->
	</div>
</div>
<?php echo $this->Form->end();?>
