<?php $this->assign('page_title', __('Clients'));?>
<?php $this->assign('page_subtitle', $client['Client']['name']);?>
<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $civilities = Configure::read('ContactPeople.civilities'); ?>
<div class="row">
	<div class="col-md-12">
		<a href="#" class="btn default" data-toggle="modal" data-target="#new-contact-people" data-client-id="<?php echo $client['Client']['id']; ?>"><i class="fa fa-user"></i> <?php echo __('Add a contact person') ?></a>
		<?php echo $this->Html->link('<i class="fa fa-bullhorn"></i> ' . __('New demand'), array('controller' => 'leads', 'action' => 'add'), array('escape' => false, 'class' => 'btn default hidden')); ?>
    <?php if($client['Company'][0]['id'] == 3): ?>
      <?php echo $this->Html->link('<i class="fa fa-shopping-cart"></i> ' . __('New order'), array('controller' => 'stock_orders', 'action' => 'add', 'client_id' => $client['Client']['id']), array('escape' => false, 'class' => 'btn default')); ?>
    <?php endif; ?>
    <?php echo $this->Html->link('<i class="fa fa-calendar"></i> ' . __('New event'), array('controller' => 'events', 'action' => 'add', 'client_id' => $client['Client']['id']), array('escape' => false, 'class' => 'btn default')); ?>
		<?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit client'), array('controller' => 'clients', 'action' => 'edit', $client['Client']['id']), array('escape' => false, 'class' => 'btn default')); ?>
		<a href="#" class="btn default" data-toggle="modal" data-target="#new-note" data-model="client" data-model-id="<?php echo $client['Client']['id']; ?>"><i class="fa fa-sticky-note-o"></i> <?php echo __('New note'); ?></a>
		<a href="#" class="btn default hidden" id="enable"><i class="fa fa-edit"></i> <?php echo __('Enable edition'); ?></a>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-3">
		<div class="portlet light bordered">
			<div class="portlet-body">
				<?php if(!empty($client['Logo']['id'])): ?>
				<?php echo $this->Html->image(DS . $client['Logo']['url'], array('class' => 'client-logo', 'alt' => $client['Client']['name'])); ?>
				<hr>
				<?php endif; ?>
				<p>
					<strong><?php echo $this->Html->link($client['Client']['name'], 'javascript:;', array('id' => 'name', 'class' => 'editable', 'data-type' => 'text', 'data-pk' => $client['Client']['id'])) ; ?></strong><br />
					<?php echo $this->Html->link($client['Client']['address'], 'javascript:;', array('id' => 'address', 'class' => 'editable', 'data-type' => 'text', 'data-pk' => $client['Client']['id'])) ; ?><br />
					<?php echo $client['Client']['zip_city']; ?><br />
					<?php echo $this->Html->link($client['Client']['country'], 'javascript:;', array('id' => 'country', 'class' => 'editable', 'data-type' => 'text', 'data-pk' => $client['Client']['id'])) ; ?>
					<?php if(!empty($client['Client']['website'])): ?>
					<br />
					<a href="<?php echo $client['Client']['website']; ?>" target="_blank"><?php echo $client['Client']['website']; ?></a>
					<?php endif; ?>
				</p>
			</div>
		</div>
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Contact persons'); ?></span>
				</div>
			</div>
			<div class="portlet-body">
				<?php foreach($client['ContactPeople'] as $key => $contact): ?>
					<p>
						<strong><?php echo !empty($contact['civility']) ? $civilities[$contact['civility']] : ''; ?> <?php echo $contact['full_name']; ?></strong><br />
						<?php if(!empty($contact['function'])): ?>
						<?php echo $contact['function']; ?><br />
						<?php endif; ?>
						<?php if(!empty($contact['department'])): ?>
						<?php echo $contact['department']; ?><br />
						<?php endif; ?>
						<?php if(!empty($contact['email'])): ?>
						<a href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a><br />
						<?php endif; ?>
						<?php if(!empty($contact['phone'])): ?>
						<a href="tel:<?php echo $contact['phone']; ?>"><?php echo $contact['phone']; ?></a><br>
						<?php endif; ?>
						<?php if(!empty($numberOfNotes) && false): ?>
							<a href="#"> <i class="fa fa-file"></i> <?php echo __('%s notes', $numberOfNotes); ?></a>
						<?php endif; ?>
					</p>
					<?php if(!empty($client['ContactPeople'][$key+1])): ?><hr><?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
		<?php if(!empty($client['Client']['invoice_address'])): ?>
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Comptabilité'); ?></span>
				</div>
			</div>
			<div class="portlet-body">
				<p>
					<strong><?php echo __('Invoice address'); ?></strong><br>
					<?php echo $client['Client']['invoice_address']; ?><br />
					<?php echo $client['Client']['invoice_zip_city']; ?>
				</p>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="col-md-9">
		<div class="portlet light bordered" data-portlet-notes>
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Notes'); ?></span>
				</div>
        <div class="tools">
          <a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'portlet', 'Client', $client['Client']['id'], 0, 1)); ?>" class="reload hidden"> </a>
        </div>
			</div>
			<div class="portlet-body">

			</div>
		</div>
		<?php if(!empty($timeline)): ?>
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Flux d\'activité'); ?></span>
				</div>
				<div class="actions">
					<a href="#" class="btn btn-sm btn-company btn-company-<?php echo $company['class']; ?>"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="timeline-filters">
					<a href="#" class="btn btn-sm btn-outline grey-gallery" data-type="all"> <i class="fa fa-tag"></i> <?php echo __('All (%s)', $sizes['timeline']); ?></a>
					<?php if(!empty($sizes['notes'])): ?>
					<a href="#" class="btn btn-sm btn-outline blue-sharp" data-type="note"> <i class="fa fa-tag"></i> <?php echo __('Notes (%s)', $sizes['notes']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['leads'])): ?>
					<a href="#" class="btn btn-sm btn-outline green-haze" data-type="marketing"> <i class="fa fa-tag"></i> <?php echo __('Marketing (%s)', $sizes['leads']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['offers'])): ?>
					<a href="#" class="btn btn-sm btn-outline yellow-casablanca" data-type="offer"> <i class="fa fa-tag"></i> <?php echo __('Offers (%s)', $sizes['offers']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['events'])): ?>
					<a href="#" class="btn btn-sm btn-outline purple" data-type="event"> <i class="fa fa-tag"></i> <?php echo __('Events (%s)', $sizes['events']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['refused'])): ?>
					<a href="#" class="btn btn-sm btn-outline red" data-type="refused"> <i class="fa fa-tag"></i> <?php echo __('Refused events (%s)', $sizes['refused']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['null'])): ?>
					<a href="#" class="btn btn-sm btn-outline grey-cascade" data-type="null"> <i class="fa fa-tag"></i> <?php echo __('Null offers (%s)', $sizes['null']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['partner'])): ?>
					<a href="#" class="btn btn-sm btn-outline grey-gallery" data-type="partner"> <i class="fa fa-tag"></i> <?php echo __('Transmitted to partner (%s)', $sizes['partner']); ?></a>
					<?php endif;?>
					<?php if(!empty($sizes['stockorders'])): ?>
					<a href="#" class="btn btn-sm btn-outline red-intense" data-type="stockorder"> <i class="fa fa-tag"></i> <?php echo __('Stock orders (%s)', $sizes['stockorders']); ?></a>
					<?php endif;?>
				</div>
				<div class="timeline">
					<?php foreach($timeline as $date => $items): ?>
					<?php foreach($items as $item): ?>
						<div class="timeline-item" data-type="<?php echo $item['type']; ?>">
								<div class="timeline-badge">
										<div class="timeline-icon">
												<i class="<?php echo $item['icon']; ?> <?php echo $item['color']; ?>"></i>
										</div>
								</div>
								<div class="timeline-body">
										<div class="timeline-body-arrow"> </div>
										<div class="timeline-body-head">
												<div class="timeline-body-head-caption">
														<span class="timeline-body-alerttitle <?php echo $item['color']; ?>">
															<?php echo $item['title']; ?>
														</span>
														<span class="timeline-body-time font-grey-cascade">
															<?php if(!empty($item['author'])): ?>
																<?php echo $item['author']; ?> -
															<?php endif; ?>
															<?php echo $item['date']; ?>
														</span>
												</div>
												<?php if(!empty($item['actions'])): ?>
												<div class="timeline-body-head-actions">
													<?php foreach($item['actions'] as $action): ?>
													<?php echo $this->Html->link($action['title'], $action['url'], array('class' => sprintf('btn btn-circle1 btn-outline btn-sm %s', $action['color']), 'target' => '_blank', 'escape' => false)); ?>
													<?php endforeach; ?>
												</div>
												<?php endif; ?>
										</div>
										<div class="timeline-body-content">
											<?php echo $item['description']; ?>
											<?php if(1==2): ?>
											<strong><?php echo __('Status') ?></strong> <?php echo $statuses[$item['data']['StockOrder']['status']]; ?><br />
											<strong><?php echo __('Date of service'); ?></strong>
											<?php echo $this->Time->format('d.m.Y', $item['data']['StockOrder']['service_date_begin']); ?>
											<?php if($item['data']['StockOrder']['service_date_begin'] != $item['data']['StockOrder']['service_date_end']): ?>
												 -
												<?php echo $this->Time->format('d.m.Y', $item['data']['StockOrder']['service_date_end']); ?>
											<?php endif; ?>
											<?php endif; ?>
										</div>
								</div>
						</div>
					<?php endforeach; ?>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>

<?php echo $this->element('Modals/contact_people_add'); ?>

<?php
$this->start('init_scripts');
echo 'Custom.client();';
$this->end();
?>
