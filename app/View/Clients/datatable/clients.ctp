<?php
$countries = Configure::read('Countries');
foreach ($dtResults as $result) {

	$contact = $result['ContactPeople']['full_name'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['ContactPeople']['phone'];
	$contact .= $this->Html->tag('br');
	$contact .= $result['ContactPeople']['email'];

	$address = $result['Client']['address'] . $this->Html->tag('br') . $result['Client']['zip'] . ' ' . $result['Client']['city'] . $this->Html->tag('br') . $countries[$result['Client']['country']];

	$actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'clients', 'action' => 'view', $result['Client']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false)
  );
  $actions .= $this->Html->tag('');
  $actions .= $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['Client']['name'],
    $contact,
    $result['Company']['name'],
    $address,
    $actions
  );
}
