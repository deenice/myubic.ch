<?php $this->assign('page_title', $this->Html->link(__('Clients'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<?php $civilities = Configure::read('ContactPeople.civilities'); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wallet"></i><?php echo __('List of all clients'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a client'), array('controller' => 'clients', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Clients'); ?>
		<?php if(1==2): ?>
		<div class="table">
			<table
				class="table table-striped table-bordered table-hover" 
	            data-dataTable 
	            data-url="<?php echo Router::url(array('controller' => 'clients', 'action' => 'index_json.json')); ?>" 
	            data-columns="Client.name|ContactPeople.name[<br>]|Client.address[<br />]|actions[ ]">
			<thead>
				<tr>
					<th><?php echo __('Company'); ?></th>
					<th><?php echo __('Contact person'); ?></th>
					<th><?php echo __('Address'); ?></th>
					<th><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody></tbody>
			</table>
		</div>
		<?php endif; ?>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.clients();';
$this->end();
?>