<?php $this->assign('page_title', __('Clients')); ?>
<?php $this->assign('page_subtitle', __('Cresus synchronisation')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-money"></i><?php echo __('Clients'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table">
			<thead>
				<tr>	
					<th><?php echo __('ID'); ?></th>
					<th style="width: 25%"><?php echo __('Client'); ?></th>
					<th><?php echo __('Contact person'); ?></th>
					<th><?php echo __('ID cresus'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($clients as $client): ?>
					<tr>
						<td><?php echo $client['Client']['id']; ?></td>
						<td><?php echo $client['Client']['name']; ?></td>
						<td><?php echo $client['ContactPeople'][0]['full_name']; ?></td>
						<td><?php echo $this->Form->input('Client.import_id', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control cresus', 'data-id' => $client['Client']['id'], 'value' => empty($client['Client']['import_id']) ? '':$client['Client']['import_id'], 'readonly' => empty($client['Client']['import_id']) ? false:true)); ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.clients();';
$this->end();
?>