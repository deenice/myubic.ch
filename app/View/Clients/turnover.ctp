<?php $this->assign('page_title', __('Clients')); ?>
<?php $this->assign('page_subtitle', __('Turnover')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-money"></i><?php echo __('Clients'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table">
			<thead>
				<tr>
					<th style="width: 30%"><?php echo __('Client'); ?></th>
					<th style="width: 30%"><?php echo __('Company'); ?></th>
					<th><?php echo __('Turnover'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($clients as $client): ?>
					<tr>
						<td><?php echo $client['Client']['name']; ?></td>
						<td>
              <?php foreach($client['Company'] as $company): ?>
                <span style="display: block"><?php echo $company['name']; ?></span>
              <?php endforeach; ?>
            </td>
						<td><?php echo $this->Form->input('Client.2015_turnover', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control turnover', 'data-id' => $client['Client']['id'], 'value' => empty($client['Client']['2015_turnover']) ? '':$client['Client']['2015_turnover'])); ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.clients();';
$this->end();
?>
