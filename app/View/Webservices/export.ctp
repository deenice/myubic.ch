<?php if(!empty($clients)): ?>
<a href="#" data-list="clients" class="btn btn-primary sync pull-right"><i class="fa fa-refresh"></i> Synchroniser</a>
<table class="table" id="clients">
  <thead>
    <tr>
      <th>#</th>
      <th>Client</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($clients as $k => $client): ?>
      <tr data-item-id="<?php echo $client['Client']['id']; ?>" data-company-id="<?php echo $company['Company']['id']; ?>" class="<?php echo empty($client['Client']['uuid']) ? '' : 'exported'; ?>">
        <td><?php echo ++$k; ?></td>
        <td><?php echo $client['Client']['name']; ?></td>
        <td><i class="fa fa-minus"></i></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif;?>

<?php if(!empty($stockorders)): ?>
<a href="#" data-list="stockorders" class="btn btn-primary sync pull-right"><i class="fa fa-refresh"></i> Synchroniser</a>
<table class="table" id="stockorders">
  <thead>
    <tr>
      <th>#</th>
      <th>Commande</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($stockorders as $k => $order): ?>
      <tr data-item-id="<?php echo $order['StockOrder']['id']; ?>" data-company-id="<?php echo $company['Company']['id']; ?>" class="<?php echo empty($order['StockOrder']['uuid']) ? '' : 'exported'; ?>">
        <td><?php echo ++$k; ?></td>
        <td><?php echo $order['StockOrder']['order_number']; ?></td>
        <td><i class="fa fa-minus"></i></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif;?>

<?php $this->start('init_scripts'); ?>

  $(document).ready(function(){
    var basePath = "";
    if(document.location.hostname == 'localhost' || document.location.hostname == '192.168.1.126'){
        basePath = '/myubic';
    }
    var sync = function( list, id ){

      var row = $('#'+list+' tbody tr:eq('+id+')');
      var itemId = row.data('item-id');
      var companyId = row.data('company-id');
      var max = $('#'+list+' tbody tr').get().length;
      row.find('td:last').html('<i class="fa fa-spin fa-spinner"></i>');

      $.ajax({
        url: basePath + '/webservices/export/'+list+'/' + companyId,
        dataType: 'json',
        type: 'post',
        data: {
          itemId: itemId,
          companyId: companyId
        },
        success: function(data){
          if(data){
            if(data.success == 1){
              row.find('td:last').html('<i class="fa fa-check font-green"></i>');
            } else if(data.success == 0){
              row.find('td:last').html('<i class="fa fa-times font-red"></i>');
            }
          }
        }
      }).then(function(){
        id++;
        if(id < max){
          sync( list, id );
        } else {
          return;
        }
      });

    }
    $('body').on('click', '.sync', function(e){
      e.preventDefault();
      var list = $(this).data('list');
      sync(list, 0);
    });
  });

<?php $this->end(); ?>
