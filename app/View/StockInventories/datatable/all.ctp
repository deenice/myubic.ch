<?php
foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'stock_inventories', 'action' => 'edit', $result['StockInventory']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['StockInventory']['name'],
    $this->Time->format('d.m.Y', $result['StockInventory']['start']),
    $this->Time->format('d.m.Y', $result['StockInventory']['end']),
    $actions
  );
}
