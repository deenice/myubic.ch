<?php
foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'stock_items', 'action' => 'view', $result['StockItem']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false)
  );

  $actions .= $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'stock_items', 'action' => 'edit', $result['StockItem']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['StockItem']['code'],
    $result['StockItem']['name'],
    $actions
  );
}
