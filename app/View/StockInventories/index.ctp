<?php $this->assign('page_title', __('Stock inventories')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>

<div class="tabbable-custom tabbable-tabdrop" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#all" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Stock inventories'); ?></a>
		</li>
		<li>
			<a href="#empty" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Products not itemised'); ?></a>
		</li>
		<?php foreach($inventories as $inventory): ?>
		<li>
			<a href="#inventory<?php echo $inventory['StockInventory']['id']; ?>" data-toggle="tab"><i class="fa fa-list"></i> <?php echo $inventory['StockInventory']['name']; ?></a>
		</li>
		<?php endforeach; ?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="all">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
					 <?php echo __('List of all stock inventories'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a stock inventory'), array('controller' => 'stock_inventories', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('All'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="empty">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
					 <?php echo __('List of all products not itemised'); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Empty'); ?>
				</div>
			</div>
		</div>
		<?php foreach($inventories as $inventory): ?>
			<div class="tab-pane" id="inventory<?php echo $inventory['StockInventory']['id']; ?>">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
						 <?php echo __('Stock items edited during this inventory'); ?>
						</div>
					</div>
					<div class="portlet-body">
						<?php if(!empty($inventory['StockItem'])): ?>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($inventory['StockItem'] as $item): ?>
									<tr>
										<td><?php echo sprintf('%s - %s', $item['code'], $item['name']); ?></td>
										<td>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-search"></i> %s', __('View')), array('controller' => 'stock_items', 'action' => 'view', $item['id']), array('escape' => false, 'class' => 'btn default btn-xs')); ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit')), array('controller' => 'stock_items', 'action' => 'edit', $item['id']), array('escape' => false, 'class' => 'btn btn-warning btn-xs')); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
