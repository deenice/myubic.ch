<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Edit a stock inventory'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-layers font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Edit a stock inventory'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockInventory', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('value' => $this->request->data['StockInventory']['id'])); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('end', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker'));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
