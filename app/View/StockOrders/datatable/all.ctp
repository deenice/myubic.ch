<?php
$statuses = Configure::read('StockOrders.status');

foreach ($dtResults as $result) {

    $stockorderName = '';
	if($result['StockOrder']['to_check'] == 1){
		$stockorderName .= $this->Html->tag('span', __('To check'), array('class' => 'btn btn-warning btn-xs'));
	}

    $stockorderName .= $this->Html->tag('strong', $result['StockOrder']['order_number']);
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $this->Html->link(
        $result['StockOrder']['name'],
        array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id'])
    );
    $stockorderName .= $this->Html->tag('hr');
    $stockorderName .= $this->Html->link(
        $result['Client']['name'],
        array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
        array('target' => '_blank')
    );
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $result['ContactPeopleClient']['first_name'] . ' ' . $result['ContactPeopleClient']['last_name'];
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $result['StockOrder']['net_total'] . ' CHF';

    if(in_array($result['StockOrder']['status'], array('to_invoice', 'invoiced'))){
        $links1 = $this->Html->link(
            '<i class="fa fa-edit"></i> ' . __('Edit'),
            array('controller' => 'stock_orders', 'action' => 'edit_invoice', $result['StockOrder']['id']),
            array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
        );
    } else {
        $links1 = $this->Html->link(
            '<i class="fa fa-edit"></i> ' . __('Edit'),
            array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id']),
            array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
        );
    }
    if($result['StockOrder']['status'] == 'offer') {
        $links1 .= $this->Html->link(
            '<i class="fa fa-check"></i> ' . __('Confirm'),
            array('controller' => 'stock_orders', 'action' => 'confirm', $result['StockOrder']['id']),
            array('class' => 'btn btn-success btn-xs margin-bottom-5 bootbox', 'escape' => false)
        );
    }
    $links1 .= $this->Html->link(
        '<i class="fa fa-times"></i> ' . __('Cancel'),
        array('controller' => 'stock_orders', 'action' => 'cancel', $result['StockOrder']['id']),
        array('class' => 'btn btn-danger btn-xs margin-bottom-5 bootbox', 'escape' => false)
    );
    $links1 = $this->Html->tag('div', $links1, array('class' => 'btn-group'));

    if(!in_array($result['StockOrder']['status'], array('to_invoice', 'invoiced'))){
        $links2 = $this->Html->link(
            '<i class="fa fa-download"></i> ' . __('Download'),
            array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf'),
            array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
        );
        $links2 .= $this->Html->link(
            '<i class="fa fa-file-o"></i> ' . __('View offer'),
            array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf', '?' => 'download=0'),
            array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
        );
        if(empty($result['History'])){
            $links2 .= $this->Html->link(
                '<i class="fa fa-send-o"></i> ' . __('Send to client'),
                array('controller' => 'stock_orders', 'action' => 'send', $result['StockOrder']['id'], 'client', 'offer_sent'),
                array('class' => 'btn default btn-xs margin-bottom-5 bootbox', 'escape' => false)
            );
        } elseif(!empty($result['History']) AND $result['StockOrder']['modified'] >= $result['History'][0]['date']){
            $links2 .= $this->Html->link(
                '<i class="fa fa-send-o"></i> ' . __('Resend to client'),
                array('controller' => 'stock_orders', 'action' => 'send',
                    $result['StockOrder']['id'], 'client', 'offer_sent'),
                array('class' => 'btn default btn-xs margin-bottom-5 bootbox', 'escape' => false)
            );
        }
    } else {
        $links2 = $this->Html->link(
            '<i class="fa fa-file-o"></i> ' . __('View invoice'),
            array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf', '?' => 'download=0'),
            array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
        );
    }
    $links2 .= $this->Html->link(
        '<i class="fa fa-copy"></i> ' . __('Duplicate'),
        array('controller' => 'stock_orders', 'action' => 'duplicate', $result['StockOrder']['id']),
        array('class' => 'btn default btn-xs margin-bottom-5 bootbox', 'escape' => false)
    );

    $links2 = $this->Html->tag('div', $links2, array('class' => 'btn-group'));

    $actions = $links1 . $this->Html->tag('br') . $links2;

    $deliveryDate = $result['StockOrder']['delivery_date'] == '1970-01-01' ? $this->Html->tag('span', __('To define!'), array('class' => 'label label-danger uppercase')) : $this->Time->format($result['StockOrder']['delivery_date'], '%A %d %B %Y');
    $returnDate = $result['StockOrder']['return_date'] == '1970-01-01' ? $this->Html->tag('span', __('To define!'), array('class' => 'label label-danger uppercase')) : $this->Time->format($result['StockOrder']['return_date'], '%A %d %B %Y');

    $this->dtResponse['aaData'][] = array(
        $stockorderName,
        __($statuses[$result['StockOrder']['status']]),
        $deliveryDate,
        $returnDate,
        $this->Time->format($result['StockOrder']['service_date_begin'], '%A %d %B %Y'),
        $actions
    );
}
