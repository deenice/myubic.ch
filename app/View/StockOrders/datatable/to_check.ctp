<?php
foreach ($dtResults as $result) {

    $stockorderName = $result['StockOrder']['name'];

    $stockorderName = $this->Html->tag('strong', $result['StockOrder']['order_number']);
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $this->Html->link(
        $result['StockOrder']['name'],
        array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id'])
    );
    $stockorderName .= $this->Html->tag('hr');
    $stockorderName .= $this->Html->link(
        $result['Client']['name'],
        array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
        array('target' => '_blank')
    );
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $result['ContactPeopleClient']['first_name'] . ' ' . $result['ContactPeopleClient']['last_name'];
    $stockorderName .= $this->Html->tag('br');
    $stockorderName .= $result['StockOrder']['net_total'] . ' CHF';

    $links1 = $this->Html->link(
        '<i class="fa fa-edit"></i> ' . __('Edit'),
        array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id']),
        array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
    );
    $links1 = $this->Html->tag('div', $links1, array('class' => 'btn-group'));

    $links2 = $this->Html->link(
        '<i class="fa fa-download"></i> ' . __('Download'),
        array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf'),
        array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
    );
    $links2 .= $this->Html->link(
        '<i class="fa fa-file-o"></i> ' . __('View offer'),
        array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf', '?' => 'download=0'),
        array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
    );

    $links2 = $this->Html->tag('div', $links2, array('class' => 'btn-group'));

    $actions = $links1 . $this->Html->tag('br') . $links2;

    $this->dtResponse['aaData'][] = array(
        $stockorderName,
        $this->Time->format($result['StockOrder']['delivery_date'], '%A %d %B %Y'),
        $this->Time->format($result['StockOrder']['return_date'], '%A %d %B %Y'),
        $this->Time->format($result['StockOrder']['service_date_begin'], '%A %d %B %Y'),
        $actions
    );
}
