<?php
foreach ($dtResults as $result) {

  $stockorderName = '';
  if($result['StockOrder']['to_check'] == 1){
    $stockorderName .= $this->Html->tag('span', __('To check'), array('class' => 'btn btn-warning btn-xs'));
  }
  $stockorderName .= $this->Html->tag('strong', $result['StockOrder']['order_number']);
  $stockorderName .= $this->Html->tag('br');
  $stockorderName .= $this->Html->link(
    $result['StockOrder']['name'],
    array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id'])
  );
  $stockorderName .= $this->Html->tag('hr');
  $stockorderName .= $this->Html->link(
    $result['Client']['name'],
    array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
    array('target' => '_blank')
  );
  $stockorderName .= $this->Html->tag('br');
  $stockorderName .= $result['ContactPeopleClient']['first_name'] . ' ' . $result['ContactPeopleClient']['last_name'];
  $stockorderName .= $this->Html->tag('br');
  $stockorderName .= $result['StockOrder']['net_total'] . ' CHF';

  $links1 = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __('Edit'),
    array('controller' => 'stock_orders', 'action' => 'edit', $result['StockOrder']['id']),
    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
  );
  // $links1 .= $this->Html->link(
  //   '<i class="fa fa-check"></i> ' . __('Confirm'),
  //   array('controller' => 'stock_orders', 'action' => 'confirm', $result['StockOrder']['id']),
  //   array('class' => 'btn btn-success btn-xs margin-bottom-5 bootbox', 'escape' => false)
  // );
  $links1 .= $this->Html->link(
    '<i class="fa fa-times"></i> ' . __('Cancel'),
    array('controller' => 'stock_orders', 'action' => 'cancel', $result['StockOrder']['id']),
    array('class' => 'btn btn-danger btn-xs margin-bottom-5 bootbox', 'escape' => false)
  );
  $links1 = $this->Html->tag('div', $links1, array('class' => 'btn-group'));

  $links2 = $this->Html->link(
    '<i class="fa fa-file-o"></i> ' . __('View offer'),
    array('controller' => 'stock_orders', 'action' => 'invoice', $result['StockOrder']['id'] . '.pdf', '?' => 'download=0'),
    array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false, 'target' => '_blank')
  );
  if(empty($result['History'])){
    $links2 .= $this->Html->link(
      '<i class="fa fa-send-o"></i> ' . __('Send to client'),
      array('controller' => 'stock_orders', 'action' => 'send', $result['StockOrder']['id'], 'client', 'offer_sent'),
      array('class' => 'btn default btn-xs margin-bottom-5 bootbox', 'escape' => false)
    );
  }

  $links2 = $this->Html->tag('div', $links2, array('class' => 'btn-group'));

  if(!empty($result['History'])){
    $links2 .= $this->Html->link(
      '<i class="fa fa-bullhorn"></i> ' . __('Revive offer!'),
      array('controller' => 'stock_orders', 'action' => 'send',
        $result['StockOrder']['id'], 'client', 'revive_offer'),
      array('class' => 'btn btn-info btn-xs margin-bottom-5 bootbox', 'escape' => false)
    );
  }


  $actions = $links1 . $this->Html->tag('br') . $links2;

  $history = '';
  if(!empty($result['OfferSendHistory'])){
    $history .= __('Offer sent on:');
    foreach($result['OfferSendHistory'] as $item){
      $history .= $this->Html->tag('br');
      $history .= $this->Time->format($item['date'], '%d.%m.%Y');
    }
  }
  if(!empty($result['OfferReviveHistory'])){
    $history .= $this->Html->tag('br') . __('Offer revived on:');
    foreach($result['OfferReviveHistory'] as $item){
      $history .= $this->Html->tag('br');
      $history .= $this->Time->format($item['date'], '%d.%m.%Y');
    }
  }

  if(!empty($history)){
    $actions .= $this->Html->tag('hr') . $this->Html->tag('small', $history, array('class' => 'text-muted'));
  }

  if(!empty($result['StockOrder']['remarks']) && !empty($result['History'])){
    $warning = $this->Html->tag('i', '', array('class' => 'fa fa-warning text-warning'));
    $warning = $this->Html->tag('div', $warning, array('class' => 'pull-right'));
    $actions .= $warning;
  }

  $deliveryDate = $result['StockOrder']['delivery_date'] == '1970-01-01' ? $this->Html->tag('span', __('To define!'), array('class' => 'label label-danger uppercase')) : $this->Time->format($result['StockOrder']['delivery_date'], '%A %d %B %Y');
  $returnDate = $result['StockOrder']['return_date'] == '1970-01-01' ? $this->Html->tag('span', __('To define!'), array('class' => 'label label-danger uppercase')) : $this->Time->format($result['StockOrder']['return_date'], '%A %d %B %Y');

  $this->dtResponse['aaData'][] = array(
    $stockorderName,
    $deliveryDate,
    $returnDate,
    $this->Time->format($result['StockOrder']['service_date_begin'], '%A %d %B %Y'),
    $actions
  );
}
