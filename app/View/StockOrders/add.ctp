<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Add an order'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-shopping-cart font-red"></i>
			<span class="caption-subject font-red bold uppercase"><?php echo __('Add an order'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockOrder', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'), 'type' => 'hidden')); ?>
		<?php echo $this->Form->input('company_id', array('value' => 3, 'type' => 'hidden')); ?>
			<div class="form-body">
			<h3 class="form-section"><?php echo __('General data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Client'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('client_id', array('label' => false, 'class' => 'form-control select2me', 'empty' => true)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_people_id', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"></label>
					<div class="col-md-9">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new client'), array('#' => 'new-client'), array('class' => 'label bg-grey-cararra new-client', 'data-toggle' => 'modal', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new contact person'), array('#' => 'new-contact-people'), array('class' => 'label bg-grey-cararra new-contact-people', 'data-toggle' => 'modal', 'escape' => false)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Concern / Event'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Status'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockOrders.status'))); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date of service'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('service_date_begin', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('service_date_end', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
					</div>					
				</div>
				<h3 class="form-section">Facturation</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date of invoice'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('date_of_invoice', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text', 'readonly' => true)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_address', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_zip_city', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
						<?php echo $this->Form->input('invoice_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('invoice_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'hidden')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_phone', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_email', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<h3 class="form-section">Livraison matériel</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Delivery mode'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_mode', array('label' => false, 'class' => 'form-control bs-select computeDistanceCovered', 'options' => Configure::read('StockOrders.delivery_modes'))); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Delivery date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_date', array('label' => false, 'class' => 'form-control date-picker', 'type' => 'text')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Moment'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_moment', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-medium input-inline computeTotal', 'options' => Configure::read('StockOrders.delivery_moments'))); ?>
						<?php echo $this->Form->input('delivery_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline hidden', 'label' => false, 'div' => false, 'type' => 'text')); ?>
						<span class="hidden delivery_moment_hour_invoice">
							<?php echo $this->Form->input('delivery_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
						</span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Place'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_place_id', array('label' => false, 'class' => 'form-control select2me', 'empty' => true, 'options' => $places)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address search'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address_search', array('label' => false, 'class' => 'form-control')); ?>
						<span class="badge badge-warning">En développement!</span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_address', array('label' => false, 'class' => 'form-control computeDistanceCovered')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_zip_city', array('label' => false, 'class' => 'form-control')); ?>
						<?php echo $this->Form->input('delivery_zip_city1', array('label' => false, 'class' => 'form-control hidden computeDistanceCovered')); ?>
						<?php echo $this->Form->input('delivery_zip', array('label' => false, 'class' => 'form-control computeDistanceCovered', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('delivery_city', array('label' => false, 'class' => 'form-control computeDistanceCovered', 'type' => 'hidden')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Distance'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_distance', array('label' => false, 'class' => 'form-control input-inline input-small', 'div' => false, 'readonly' => true)); ?>
						<span class="help-text">km</span>
						<span class="help-block delivery_end_address hidden"><?php echo __('Found address:'); ?> <strong></strong></span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_contact_people_id', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('delivery_remarks', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<h3 class="form-section">Retour matériel</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Return mode'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_mode', array('label' => false, 'class' => 'form-control bs-select computeDistanceCovered', 'options' => Configure::read('StockOrders.return_modes'))); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Return date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_date', array('label' => false, 'class' => 'form-control date-picker', 'type' => 'text')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Moment'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_moment', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-medium input-inline computeTotal', 'options' => Configure::read('StockOrders.return_moments'))); ?>
						<?php echo $this->Form->input('return_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline hidden', 'label' => false, 'div' => false, 'type' => 'text')); ?>
						<span class="hidden return_moment_hour_invoice">
							<?php echo $this->Form->input('return_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
						</span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Place'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_place_id', array('label' => false, 'class' => 'form-control select2me', 'empty' => true, 'options' => $places)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_address', array('label' => false, 'class' => 'form-control computeDistanceCovered')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_zip_city', array('label' => false, 'class' => 'form-control')); ?>
						<?php echo $this->Form->input('return_zip_city1', array('label' => false, 'class' => 'form-control hidden computeDistanceCovered')); ?>
						<?php echo $this->Form->input('return_zip', array('label' => false, 'class' => 'form-control computeDistanceCovered', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('return_city', array('label' => false, 'class' => 'form-control computeDistanceCovered', 'type' => 'hidden')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Distance'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_distance', array('label' => false, 'class' => 'form-control input-inline input-small', 'div' => false, 'readonly' => true)); ?>
						<span class="help-text">km</span>
						<span class="help-block return_end_address hidden"><?php echo __('Found address:'); ?> <strong></strong></span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_contact_people_id', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('return_remarks', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>
				<h3 class="form-section"><?php echo __('Stock items'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Add an item'); ?></label>
					<div class="col-md-2">
						<?php echo $this->Form->input('add_stockItem_quantity', array('label' => false, 'class' => 'form-control', 'placeholder' => __('Quantity'), 'type' => 'number')); ?>
					</div>
					<div class="col-md-7">
						<?php echo $this->Form->input('add_stockItem', array('label' => false, 'class' => 'form-control')); ?>
					</div>					
				</div>			
				<div class="form-group">
					<div class="col-md-12">
						<table class="table table-striped table-hover" id="stockitemsTable">
							<thead>
								<tr>
									<th></th>
									<th><?php echo __('Code'); ?></th>
									<th><?php echo __('Stock item'); ?></th>
									<th><?php echo __('Coefficient'); ?></th>
									<th><?php echo __('Quantity'); ?></th>
									<th><?php echo __('Unit price'); ?></th>
									<th><?php echo __('Net HT'); ?></th>
									<th><?php echo __('Discount'); ?></th>
									<th><?php echo __('Total TTC'); ?></th>
								</tr>
							</thead>
							<tbody>
								<tr class="hidden empty">
									<td class="actions" style="width: 42px;">
										<div class="btn-group btn-group-xs">
											<a href="#" class="btn btn-inverse font-grey-gallery remove-stock-item ajax"><i class="fa fa-trash-o"></i> </a>
										</div>
									</td>
									<td class="code"></td>
									<td class="stockitem"></td>
									<td class="coefficient">
										<?php echo $this->Form->input('coefficient', array('class' => 'form-control input-sm input-xsmall coefficient', 'label' => false, 'div' => false, 'id' => false)); ?>
									</td>
									<td class="quantity">
										<span></span>
										<div class="btn-group btn-group-xs pull-right">
											<a href="#" class="btn default increase-stock-item-quantity"><i class="fa fa-caret-up"></i> </a>
											<a href="#" class="btn default decrease-stock-item-quantity"><i class="fa fa-caret-down"></i> </a>
										</div>
									</td>
									<td class="price"></td>
									<td class="net_ht"></td>
									<td class="discount">
										<?php echo $this->Form->input('StockOrderBatch.discount.', array('class' => 'form-control input-sm input-xsmall discount input-inline', 'label' => false, 'div' => false, 'id' => false)); ?>
										<span class="help-text">%</span>
									</td>
									<td class="total_ttc"></td>
									<?php echo $this->Form->input('StockOrderBatch.quantity.', array('type' => 'hidden', 'class' => 'quantity')); ?>
									<?php echo $this->Form->input('StockOrderBatch.stock_item_id.', array('type' => 'hidden', 'class' => 'stock_item_id')); ?>
									<?php echo $this->Form->input('StockOrderBatch.price.', array('type' => 'hidden', 'class' => 'price')); ?>
									<?php echo $this->Form->input('StockOrderBatch.coefficient.', array('type' => 'hidden', 'class' => 'coefficient')); ?>
								</tr>
							</tbody>
						</table>
					</div>					
				</div>
				<h3 class="form-section">Pour le calcul des frais</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Number of paletts'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('number_of_paletts', array('label' => false, 'class' => 'form-control input-small computeTotal', 'type' => 'number')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Number of rollis'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('number_of_rollis', array('label' => false, 'class' => 'form-control input-small computeTotal')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('XL surcharge'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('xl_surcharge', array('label' => false, 'class' => 'form-control input-small computeTotal')); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Distance covered'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('distance_covered', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline computeTotal')); ?>
						<span class="help-inline">km</span>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Fidelity discount'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('fidelity_discount', array('label' => false, 'class' => 'make-switch computeTotal', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>					
				</div>
				<h3 class="form-section">Total</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Total HT'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('total_ht', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small computeTotal'));?>
						<span class="help-inline">CHF</span>
					</div>
					<label class="control-label col-md-3"><?php echo __('TVA'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('tva', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Rabais de quantité'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('quantity_discount', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>
						<?php echo $this->Form->input('quantity_discount_percentage', array('type' => 'hidden'));?>
						<span class="help-inline">CHF</span>
						<span class="help-block discount"><em></em> %</span>
					</div>
					<label class="control-label col-md-3"><strong><?php echo __('Total avec TVA'); ?></strong></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('net_total', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Rabais de fidélité'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('fidelity_discount_amount', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>	
						<span class="help-inline">CHF</span>					
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Frais de transport'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('transportation_costs', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>	
						<span class="help-inline">CHF</span>					
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Frais de conditionnement'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('packaging_costs', array('label' => false, 'div' => false, 'readonly' => true, 'class' => 'form-control input-inline input-small'));?>	
						<span class="help-inline">CHF</span>						
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						<button type="submit" class="btn blue-hoki" name="data[destination]" value="send"><i class="fa fa-send"></i> <?php echo __('Save and send'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<div class="modal fade" id="new-client" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('Client', array('controller' => 'clients', 'action' => 'add')); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new client / contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Client'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('Business name'); ?></label>
							<?php echo $this->Form->input('Client.name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('Client.company_id', array('class' => 'form-control bs-select', 'label' => false, 'options' => $companies, 'value' => 3)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Address'); ?></label>
							<?php echo $this->Form->input('Client.address', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('ZIP'); ?></label>
							<?php echo $this->Form->input('Client.zip', array('class' => 'form-control', 'label' => false)); ?>
						</div>			
						<div class="form-group">
							<label class="control-label"><?php echo __('City'); ?></label>
							<?php echo $this->Form->input('Client.city', array('class' => 'form-control', 'label' => false)); ?>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('Name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.name', array('class' => 'form-control', 'label' => false)); ?>
						</div>		
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>		
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>	
					</div>
				</div>					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="new-contact-people" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('ContactPeople', array('controller' => 'contact_peoples', 'action' => 'add')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.client_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden')); ?>
						</div>		
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>		
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>	
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>	
					</div>
				</div>					
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockorders();';
$this->end();
?>
