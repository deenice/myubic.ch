<?php $this->assign('page_title', __('Stock orders')); ?>
<?php $optionsStatus = Configure::read('Configurations.options'); ?>
<div class="portlet light bordered calendar">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-calendar"></i><?php echo __('Conflicts'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<div id="calendar"></div>
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css');
echo $this->Html->css('/metronic/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css');
echo $this->Html->css('/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/media/js/jquery.dataTables.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js');
echo $this->Html->script('/metronic/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.festiloc();';
$this->end();
?>
