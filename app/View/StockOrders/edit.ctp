<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', empty($stockorder['StockOrder']['name']) ? __('Order %s', empty($stockorder['StockOrder']['order_number']) ? $stockorder['StockOrder']['id'] : $stockorder['StockOrder']['order_number']) : $stockorder['StockOrder']['order_number'] . ' ' . $stockorder['StockOrder']['name']);?>
<?php $attributes = array('legend' => false, 'label' => false, 'class' => 'toggle', 'hiddenField' => false); ?>

<?php echo $this->Form->create('StockOrder', array('class' => 'form-horizontal', 'data-automatic-save' => 1)); ?>
<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'), 'type' => 'hidden')); ?>
<?php echo $this->Form->input('StockOrder.user_id', array('value' => AuthComponent::user('id'), 'type' => 'hidden')); ?>
<?php echo $this->Form->input('StockOrder.id'); ?>
<?php echo $this->Form->input('company_id', array('value' => 3, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('destination', array('type' => 'hidden', 'name' => 'data[destination]')); ?>

<div class="tabbable-custom" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab1" data-toggle="tab"><i class="fa fa-list"></i> Informations client</a>
		</li>
		<li>
			<a href="#tab3" data-toggle="tab"><i class="fa fa-truck"></i> <?php echo __('Delivery') ?> / <?php echo __('Return') ?></a>
		</li>
		<li>
			<a href="#tab4" data-toggle="tab"><i class="fa fa-shopping-cart"></i> <?php echo __('Stock items'); ?></a>
		</li>
		<li>
			<a href="#tab5" data-toggle="tab"><i class="fa fa-sliders"></i> Calcul des frais / Total</a>
		</li>
		<li>
			<a href="#tab6" data-toggle="tab"><i class="fa fa-times"></i> <?php echo __('Cancellation'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison"><?php echo __('General data'); ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Client'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('client_id', array('label' => false, 'class' => 'form-control select2me client', 'value' => empty($stockorder['StockOrder']['client_id']) ? '' : $stockorder['StockOrder']['client_id'], 'empty' => true )); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('contact_people_id', array('label' => false, 'class' => 'form-control bs-select contact-people', 'value' => $this->request->data['StockOrder']['contact_people_id'])); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"></label>
							<div class="col-md-9">
								<div style="margin-bottom: 5px">
									<?php echo $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit client'), array('#' => 'edit-client'), array('class' => 'btn btn-xs bg-grey-cararra edit-client', 'data-toggle' => 'modal', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-pencil"></i> ' . __('Edit contact person'), array('#' => 'edit-contact-people'), array('class' => 'btn btn-xs bg-grey-cararra edit-contact-people', 'data-toggle' => 'modal', 'escape' => false)); ?>
								</div>
								<div>
									<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new client'), array('#' => 'new-client'), array('class' => 'btn btn-xs bg-grey-cararra new-client', 'data-toggle' => 'modal', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a new contact person'), array('#' => 'new-contact-people'), array('class' => 'btn btn-xs bg-grey-cararra new-contact-people', 'data-toggle' => 'modal', 'escape' => false)); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Concern / Event'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Status'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockOrders.status'))); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('To check'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('to_check', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
								<span class="help-block">
									Si coché, Festiloc contrôlera la commande avant de la confirmer.
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockOrders.types')));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Date of service'); ?></label>
							<div class="col-md-9">
								<div class="input-group input-large date-picker input-daterange">
                  <?php echo $this->Form->input('service_date_begin', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
                  <span class="input-group-addon"> <?php echo __('to'); ?> </span>
	                  <?php echo $this->Form->input('service_date_end', array('label' => false, 'div' => false, 'class' => 'form-control', 'type' => 'text')); ?>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison">Facturation</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Date of invoice'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('date_of_invoice', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline ', 'type' => 'text', 'value' => $invoiceDate)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Client / Raison sociale'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_client', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
								<span class="help-block">À remplir si différent du client principal!</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_contact_person', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
								<span class="help-block">À remplir si différent du client principal!</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_address', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group" data-commune-select2>
							<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array($stockorder['StockOrder']['invoice_zip_city']), 'value' => $stockorder['StockOrder']['invoice_zip_city']));?>
								<?php echo $this->Form->input('invoice_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
								<?php echo $this->Form->input('invoice_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_phone', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('invoice_email', array('type' => 'email', 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab3">
			<div class="portlet light" id="deliveryContainer">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison">Livraison matériel</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Delivery'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery', array('label' => false, 'class' => 'make-switch computeDistanceCovered computeTotal', 'data-on-text' => __('With'), 'data-off-text' => __('Without')));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Transporter'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_transporter_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $transporters, 'disabled' => $this->User->getGroup() == 'superadmin' ? true : true, 'empty' => true)); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Delivery date'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_date', array('label' => false, 'class' => 'form-control date-picker ', 'type' => 'text', 'value' => $this->request->data['StockOrder']['delivery_date'] == '01-01-1970' ? '':$this->request->data['StockOrder']['delivery_date'])); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Moment'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_moment', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-medium input-inline computeTotal', 'options' => Configure::read('StockOrders.delivery_moments'))); ?>
							<?php if($this->request->data['StockOrder']['delivery_moment'] == 'hour'): ?>
								<?php echo $this->Form->input('delivery_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline', 'label' => false, 'div' => false, 'type' => 'text')); ?>
								<span class="delivery_moment_hour_invoice">
									<?php echo $this->Form->input('delivery_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
								</span>
							<?php else: ?>
								<?php echo $this->Form->input('delivery_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline hidden', 'label' => false, 'div' => false, 'type' => 'text')); ?>
								<span class="hidden delivery_moment_hour_invoice">
									<?php echo $this->Form->input('delivery_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
								</span>
							<?php endif; ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Place'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_place', array('type' => 'select', 'label' => false, 'class' => 'form-control place', 'rel' => 'delivery', 'value' => empty($stockorder['StockOrder']['delivery_place']) ? '' : $stockorder['StockOrder']['delivery_place'], 'options' => empty($stockorder['StockOrder']['delivery_place']) ? '' : array($stockorder['StockOrder']['delivery_place'] => $stockorder['StockOrder']['delivery_place']))); ?>
							<span class="help-block hidden"><?php echo $this->Html->link(__('Search for a place') . ' <i class="fa fa-external-link"></i>', array('#' => ''), array('escape' => false, 'class' => 'searchGooglePlaces')); ?></span>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Address search'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('address_search', array('label' => false, 'class' => 'form-control')); ?>
							<span class="badge badge-warning">En développement!</span>
						</div>
					</div> -->
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_address', array('label' => false, 'class' => 'form-control computeDistanceCovered address', 'rel' => 'delivery')); ?>
						</div>
					</div>
					<div class="form-group" data-commune-select2>
						<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_zip_city', array('rel' => 'delivery', 'type' => 'select', 'label' => false, 'class' => 'form-control computeDistanceCovered zip-city', 'data-name' => 'zipcity', 'options' => array($stockorder['StockOrder']['delivery_zip_city']), 'value' => $stockorder['StockOrder']['delivery_zip_city']));?>
							<?php echo $this->Form->input('delivery_zip', array('label' => false, 'div' => false, 'class' => 'form-control computeDistanceCovered zip', 'rel' => 'delivery', 'data-name' => 'zip', 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('delivery_city', array('label' => false, 'div' => false, 'class' => 'form-control computeDistanceCovered city', 'rel' => 'delivery', 'data-name' => 'city', 'type' => 'hidden')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Distance'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_distance', array('label' => false, 'class' => 'form-control input-inline input-small distance', 'div' => false, 'readonly' => false, 'rel' => 'delivery')); ?>
							<span class="help-text">km</span>
							<span class="help-block delivery_end_address hidden"><?php echo __('Found address:'); ?> <strong></strong></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_contact_people_id', array('label' => false, 'class' => 'form-control bs-select contact-people', 'options' => $contactPeoples)); ?>
							<span class="help-text delivery"><span class="email"></span>&nbsp;&nbsp;<span class="phone"></span></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_remarks', array('label' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
					<?php if(!empty($stockorder['StockOrder']['delivery_comments'])): ?>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Commentaires issus de Crésus'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('delivery_comments', array('label' => false, 'class' => 'form-control', 'readonly' => true)); ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="portlet light" id="returnContainer">
					<div class="portlet-title">
						<div class="caption">
							<span class="uppercase bold font-blue-madison">Retour matériel</span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Return'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return', array('label' => false, 'class' => 'make-switch computeDistanceCovered computeTotal', 'data-on-text' => __('With'), 'data-off-text' => __('Without')));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Transporter'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_transporter_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $transporters, 'disabled' => $this->User->getGroup() == 'superadmin' ? true : true, 'empty' => true)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Return date'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_date', array('label' => false, 'class' => 'form-control date-picker ', 'type' => 'text', 'value' => $this->request->data['StockOrder']['return_date'] == '01-01-1970' ? '':$this->request->data['StockOrder']['return_date'])); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Moment'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_moment', array('label' => false, 'div' => false, 'class' => 'form-control bs-select input-medium input-inline computeTotal', 'options' => Configure::read('StockOrders.return_moments'))); ?>
								<?php if($this->request->data['StockOrder']['return_moment'] == 'hour'): ?>
									<?php echo $this->Form->input('return_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline', 'label' => false, 'div' => false, 'type' => 'text')); ?>
									<span class="return_moment_hour_invoice">
										<?php echo $this->Form->input('return_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
									</span>
								<?php else: ?>
									<?php echo $this->Form->input('return_moment_hour', array('class' => 'form-control timepicker timepicker-24 input-inline hidden', 'label' => false, 'div' => false, 'type' => 'text')); ?>
									<span class="hidden return_moment_hour_invoice">
										<?php echo $this->Form->input('return_moment_hour_invoice', array('class' => 'form-control input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Add to invoice'), 'div' => false, 'checked' => true)); ?>
									</span>
								<?php endif; ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Same as delivery?'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('delivery_return_same', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
							</div>
						</div>
						<div id="return_coords">
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Place'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('return_place', array('type' => 'select', 'label' => false, 'class' => 'form-control place', 'rel' => 'return', 'value' => empty($stockorder['StockOrder']['return_place']) ? '' : $stockorder['StockOrder']['return_place'], 'options' => empty($stockorder['StockOrder']['return_place']) ? '' : array($stockorder['StockOrder']['return_place'] => $stockorder['StockOrder']['return_place']))); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('return_address', array('label' => false, 'class' => 'form-control computeDistanceCovered address', 'rel' => 'return', 'readonly' => $stockorder['StockOrder']['delivery_return_same'] ? true : false)); ?>
								</div>
							</div>
							<div class="form-group" data-commune-select2>
								<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('return_zip_city', array('rel' => 'return', 'type' => 'select', 'label' => false, 'class' => 'form-control computeDistanceCovered zip-city', 'data-name' => 'zipcity', 'options' => array($stockorder['StockOrder']['return_zip_city']), 'value' => $stockorder['StockOrder']['return_zip_city'], 'readonly' => $stockorder['StockOrder']['delivery_return_same'] ? true : false));?>
									<?php echo $this->Form->input('return_zip', array('label' => false, 'div' => false, 'class' => 'form-control computeDistanceCovered zip', 'rel' => 'return', 'data-name' => 'zip', 'type' => 'hidden')); ?>
									<?php echo $this->Form->input('return_city', array('label' => false, 'div' => false, 'class' => 'form-control computeDistanceCovered city', 'rel' => 'return', 'data-name' => 'city', 'type' => 'hidden')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Distance'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('return_distance', array('label' => false, 'class' => 'form-control distance input-inline input-small', 'div' => false, 'readonly' => $stockorder['StockOrder']['delivery_return_same'] ? true : false)); ?>
									<span class="help-text">km</span>
									<span class="help-block return_end_address hidden"><?php echo __('Found address:'); ?> <strong></strong></span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Contact person'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_contact_people_id', array('label' => false, 'class' => 'form-control bs-select contact-people', 'options' => $contactPeoples)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_remarks', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<?php if(!empty($stockorder['StockOrder']['return_comments'])): ?>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Commentaires issus de Crésus'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('return_comments', array('label' => false, 'class' => 'form-control', 'readonly' => true)); ?>
							</div>
						</div>
						<?php endif ?>
					</div>
			</div>
		</div>
		<div class="tab-pane" id="tab4">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison"><?php echo __('Stock items'); ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Add an item'); ?></label>
						<div class="col-md-2">
							<?php echo $this->Form->input('add_stockItem_quantity', array('label' => false, 'class' => 'form-control no-save', 'placeholder' => __('Quantity'), 'type' => 'number')); ?>
						</div>
						<div class="col-md-7 add-stock-item">
							<?php echo $this->Form->input('add_stockItem', array('label' => false, 'type' => 'select', 'class' => 'form-control no-save')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<table class="table table-striped table-hover" id="stockitemsTable">
								<thead>
									<tr>
										<th></th>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Coefficient'); ?></th>
										<th><?php echo __('Quantity'); ?></th>
										<th><?php echo __('Unit price'); ?></th>
										<th><?php echo __('Net HT'); ?></th>
										<th><?php echo __('Discount'); ?></th>
										<th><?php echo __('Unit price with discount'); ?></th>
										<th><?php echo __('Total HT'); ?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($stockorder['StockOrderBatch'])): $size = sizeof($stockorder['StockOrderBatch'])?>
									<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
										<tr<?php if((!empty($batch['StockItem']['id']) && $batch['StockItem']['id'] != -1) || (!empty($batch['StockItem']['subject_to_discount']) && $batch['StockItem']['subject_to_discount'])): ?> data-subject-to-discount<?php endif; ?>>
											<td class="actions" style="width: 70px">
												<div class="btn-group btn-group-xs">
													<a href="#" class="btn btn-inverse font-grey-silver sortable"><i class="fa fa-arrows"></i></a>
													<a href="#" class="btn btn-inverse font-grey-gallery remove-stock-item ajax"><i class="fa fa-trash-o"></i></a>
												</div>
												<span class="badge-grey-silver"><?php echo $k+1; ?></span>
											</td>
											<td class="code"><?php echo !empty($batch['StockItem']['code']) ? $batch['StockItem']['code'] : ''; ?></td>
											<td class="stockitem">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $this->Form->input('StockOrderBatch.name.', array('class' => 'form-control input-sm', 'label' => false, 'div' => false, 'value' => $batch['name'])); ?>
												<?php else: ?>
												<?php echo $batch['StockItem']['name']; ?>
												<?php echo $this->Form->input('StockOrderBatch.name.', array('type' => 'hidden', 'value' => '')); ?>
												<?php endif; ?>
											</td>
											<td class="coefficient">
												<?php echo $this->Form->input('StockOrderBatch.coefficient.', array('class' => 'form-control input-sm input-xsmall coefficient ', 'label' => false, 'div' => false, 'id' => false, 'value' => $batch['coefficient'], 'tabindex' => 1000 + $k)); ?>
											</td>
											<td class="quantity">
												<?php echo $this->Form->input('StockOrderBatch.quantity.', array('class' => 'form-control input-sm input-xsmall quantity input-inline ', 'label' => false, 'div' => false, 'id' => false, 'type' => 'number', 'value' => $batch['quantity'], 'tabindex' => 1000 + $size+$k)); ?>
												<?php if($batch['stock_item_id'] != -1): ?>
												<span class="help-text"> / <?php echo $batch['StockItem']['quantity']; ?></span>
												<?php endif; ?>
											</td>
											<td class="price">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $this->Form->input('StockOrderBatch.price.', array('class' => 'form-control input-sm input-xsmall price', 'label' => false, 'div' => false, 'value' => $batch['price'])); ?>
												<?php else: ?>
												<?php echo $batch['StockItem']['price']; ?>
												<?php endif; ?>
											</td>
											<td class="net_ht">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo number_format((float)$batch['coefficient'] * $batch['quantity'] * $batch['price'], 2, '.', '');?>
												<?php else: ?>
												<?php echo number_format((float)$batch['coefficient'] * $batch['quantity'] * $batch['StockItem']['price'], 2, '.', '');?>
												<?php endif; ?>
											</td>
											<td class="discount">
												<?php echo $this->Form->input('StockOrderBatch.discount.', array('class' => 'form-control input-sm input-xsmall discount input-inline', 'label' => false, 'div' => false, 'id' => false, 'value' => $batch['discount'], 'tabindex' => 1000 + 2*$size+$k)); ?>
												<span class="help-text">%</span>
											</td>
											<td class="priceWithDisount"></td>
											<td class="total_ttc"></td>
											<td class="feedback">
												<i class="fa fa-spin fa-spinner loader hidden"></i>
												<span class="text-success hidden"><i class="fa fa-check"></i> Stock suffisant</span>
												<?php echo $this->Html->link('<i class="fa fa-times"></i> ' . __('Insufficient stock'), array('controller' => 'stock_items', 'action' => 'showConflicts'), array('class' => 'text-danger hidden', 'data-toggle' => 'modal', 'data-target' => '#conflicts', 'escape' => false)); ?>
												<?php echo $this->Html->link('<i class="fa fa-warning"></i> ' . __('Empty stock'), array('controller' => 'stock_items', 'action' => 'showConflicts'), array('class' => 'text-warning hidden', 'data-toggle' => 'modal', 'data-target' => '#conflicts', 'escape' => false)); ?>
												<span class="text-info hidden"><i class="fa fa-info-circle"></i> Ce produit n'a pas de quantité définie.</span>
												<span class="text-danger text-archived hidden"><i class="fa fa-warning"></i> Ce produit n'exsite plus!</span>
											</td>
											<?php echo $this->Form->input('StockOrderBatch.id.', array('type' => 'hidden', 'class' => 'id ', 'value' => $batch['id'])); ?>
											<?php if($batch['stock_item_id'] != -1): ?>
											<?php echo $this->Form->input('StockOrderBatch.price.', array('type' => 'hidden', 'class' => 'price ', 'value' => $batch['StockItem']['price'])); ?>
											<?php echo $this->Form->input('StockOrderBatch.stock_item_id.', array('type' => 'hidden', 'class' => 'stock_item_id ', 'value' => $batch['StockItem']['id'])); ?>
											<?php else: ?>
											<?php echo $this->Form->input('StockOrderBatch.stock_item_id.', array('type' => 'hidden', 'class' => 'stock_item_id ', 'value' => -1)); ?>
											<?php endif; ?>
											<?php echo $this->Form->input('StockOrderBatch.weight.', array('type' => 'hidden', 'class' => 'weight ', 'value' => $k)); ?>
											<?php echo $this->Form->input('archived', array('id' => false, 'type' => 'hidden', 'class' => 'archived ', 'value' => empty($batch['StockItem']['archived']) ? 0 : $batch['StockItem']['archived'])); ?>
											<?php echo $this->Form->input('StockOrderBatch.subject_to_discount.', array('type' => 'hidden', 'class' => 'subject_to_discount ', 'value' => empty($batch['StockItem']['subject_to_discount']) ? '' : $batch['StockItem']['subject_to_discount'])); ?>
										</tr>
									<?php endforeach; ?>
									<?php endif; ?>
									<tr class="hidden empty" data-subject-to-discount>
										<td class="actions" style="width: 42px;">
											<div class="btn-group btn-group-xs">
												<a href="#" class="btn btn-inverse font-grey-silver sortable"><i class="fa fa-arrows"></i></a>
												<a href="#" class="btn btn-inverse font-grey-gallery remove-stock-item ajax"><i class="fa fa-trash-o"></i></a>
											</div>
											<span class="badge-grey-silver"></span>
										</td>
										<td class="code"></td>
										<td class="stockitem"></td>
										<td class="coefficient">
											<?php echo $this->Form->input('StockOrderBatch.coefficient.', array('class' => 'form-control input-sm input-xsmall coefficient ', 'label' => false, 'div' => false, 'id' => false)); ?>
										</td>
										<td class="quantity">
											<?php echo $this->Form->input('StockOrderBatch.quantity.', array('class' => 'form-control input-sm input-xsmall quantity input-inline ', 'label' => false, 'div' => false, 'id' => false, 'type' => 'number')); ?>
											<span class="help-text"></span>
										</td>
										<td class="price"></td>
										<td class="net_ht"></td>
										<td class="discount">
											<?php echo $this->Form->input('StockOrderBatch.discount.', array('class' => 'form-control input-sm input-xsmall discount input-inline ', 'label' => false, 'div' => false, 'id' => false)); ?>
											<span class="help-text">%</span>
										</td>
										<td class="priceWithDisount"></td>
										<td class="total_ttc"></td>
										<td class="feedback">
											<i class="fa fa-spin fa-spinner loader"></i>
											<span class="text-success hidden"><i class="fa fa-check"></i> Stock suffisant</span>
											<?php echo $this->Html->link('<i class="fa fa-times"></i> ' . __('Insufficient stock') . '<span></span>', array('controller' => 'stock_items', 'action' => 'showConflicts'), array('class' => 'text-danger hidden', 'data-toggle' => 'modal', 'data-target' => '#conflicts', 'escape' => false)); ?>
											<?php echo $this->Html->link('<i class="fa fa-warning"></i> ' . __('Empty stock'), array('controller' => 'stock_items', 'action' => 'showConflicts'), array('class' => 'text-warning hidden', 'data-toggle' => 'modal', 'data-target' => '#conflicts', 'escape' => false)); ?>
												<span class="text-info hidden"><i class="fa fa-info-circle"></i> Ce produit n'a pas de quantité définie.</span>
										</td>
										<?php echo $this->Form->input('StockOrderBatch.id.', array('type' => 'hidden', 'class' => 'id ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.stock_item_id.', array('type' => 'hidden', 'class' => 'stock_item_id ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.price.', array('type' => 'hidden', 'class' => 'price ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.weight.', array('type' => 'hidden', 'class' => 'weight ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.subject_to_discount.', array('type' => 'hidden', 'class' => 'subject_to_discount ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.name.', array('type' => 'hidden')); ?>
									</tr>
									<tr class="hidden free">
										<td class="actions" style="width: 42px;">
											<div class="btn-group btn-group-xs">
												<a href="#" class="btn btn-inverse font-grey-silver sortable"><i class="fa fa-arrows"></i></a>
												<a href="#" class="btn btn-inverse font-grey-gallery remove-stock-item ajax"><i class="fa fa-trash-o"></i></a>
											</div>
											<span class="badge-grey-silver"></span>
										</td>
										<td class="code"></td>
										<td class="stockitem">
											<?php echo $this->Form->input('StockOrderBatch.name.', array('class' => 'form-control input-sm', 'label' => false, 'div' => false)); ?>
										</td>
										<td class="coefficient">
											<?php echo $this->Form->input('StockOrderBatch.coefficient.', array('class' => 'form-control input-sm input-xsmall coefficient ', 'label' => false, 'div' => false, 'id' => false)); ?>
										</td>
										<td class="quantity">
											<?php echo $this->Form->input('StockOrderBatch.quantity.', array('class' => 'form-control input-sm input-xsmall quantity input-inline', 'label' => false, 'div' => false, 'id' => false, 'type' => 'number', 'value' => 1)); ?>
											<span class="help-text"></span>
										</td>
										<td class="price">
											<?php echo $this->Form->input('StockOrderBatch.price.', array('class' => 'form-control input-sm input-xsmall price computeTotal', 'div' => false, 'label' => false, 'value' => 0)); ?>
										</td>
										<td class="net_ht"></td>
										<td class="discount">
											<?php echo $this->Form->input('StockOrderBatch.discount.', array('class' => 'form-control input-sm input-xsmall discount input-inline ', 'label' => false, 'div' => false, 'id' => false)); ?>
											<span class="help-text">%</span>
										</td>
										<td class="priceWithDisount"></td>
										<td class="total_ttc"></td>
										<td class="feedback">
											<span class="text-danger text-not-saved hidden"><i class="fa fa-warning"></i> Cette ligne n'est pas encore enregistrée!</span>
										</td>
										<?php echo $this->Form->input('StockOrderBatch.id.', array('type' => 'hidden', 'class' => 'id ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.stock_item_id.', array('type' => 'hidden', 'value' => -1)); ?>
										<?php echo $this->Form->input('StockOrderBatch.weight.', array('type' => 'hidden', 'class' => 'weight ')); ?>
										<?php echo $this->Form->input('StockOrderBatch.subject_to_discount.', array('type' => 'hidden', 'class' => 'subject_to_discount', 'value' => 0)); ?>
									</tr>
								</tbody>
								<?php echo $this->Form->input('StockOrder.batches_to_delete', array('type' => 'hidden', 'class' => 'batches_to_delete')); ?>
							</table>
							<?php if(in_array($stockorder['StockOrder']['status'], array('delivered')) && $stockorder['StockOrder']['type'] == 'rental'): ?>
							<?php else: ?>
							<?php echo $this->Html->link(__('Add a line'), array(), array('class' => 'btn btn-inverse add-line')); ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab5">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison">Calcul des frais</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Number of paletts'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('number_of_pallets', array('label' => false, 'class' => 'form-control input-small computeTotal', 'type' => 'number', 'tabindex' => 2000)); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Number of paletts XL'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('number_of_pallets_xl', array('label' => false, 'class' => 'form-control input-small computeTotal', 'type' => 'number')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Number of rollis'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('number_of_rollis', array('label' => false, 'class' => 'form-control input-small computeTotal')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Number of rollis XL'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('number_of_rollis_xl', array('label' => false, 'class' => 'form-control input-small computeTotal')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Distance covered'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('distance_covered', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline computeTotal')); ?>
							<span class="help-inline">km</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Fidelity discount'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('fidelity_discount', array('label' => false, 'div' => false, 'class' => 'make-switch computeTotal input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'checked' => empty($fidelityDiscountPercentage) ? false : true));?>
							<?php echo $this->Form->input('fidelity_discount_percentage', array('label' => false, 'div' => false, 'class' => 'form-control computeTotal input-inline input-sm input-xsmall', 'value' => $fidelityDiscountPercentage));?>
							<span class="help-text">%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison">Montant total</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table radio-list">
								<tbody>
									<tr class="bg-grey">
										<td colspan="4"><strong>Total sans livraison</strong></td>
									</tr>
									<tr>
										<td colspan="1">Total HT</td>
										<td colspan="3" class="total_ht"><span><?php echo $stockorder['StockOrder']['total_ht'];?></span> CHF</td>
									</tr>
									<tr>
										<td colspan="1">Total HT avec rabais</td>
										<td colspan="3" class="total_ht_with_discount"><span><?php echo $stockorder['StockOrder']['total_ht'] - $stockorder['StockOrder']['fidelity_discount_amount'] - $stockorder['StockOrder']['quantity_discount'];?></span> CHF</td>
									</tr>
									<tr class="bg-grey">
										<td colspan="4"><strong>Répartition</strong></td>
									</tr>
									<tr>
										<td colspan="1">Location</td>
										<td colspan="3" class=""><?php echo $this->Form->input('repartition_rental', array('class' => 'form-control input-small input-inline', 'label' => false, 'div' => false, 'value' => is_null($stockorder['StockOrder']['repartition_rental']) ? null : $stockorder['StockOrder']['repartition_rental']));?> <span class="help-text">CHF</span></td>
									</tr>
									<tr>
										<td colspan="1">Autres prestations</td>
										<td colspan="3" class=""><?php echo $this->Form->input('repartition_other', array('class' => 'form-control input-small input-inline', 'label' => false, 'div' => false, 'value' => is_null($stockorder['StockOrder']['repartition_other']) ? null : $stockorder['StockOrder']['repartition_other']));?> <span class="help-text">CHF</span></td>
									</tr>
									<tr class="bg-grey">
										<td colspan="2"><strong>Livraison</strong></td>
										<td colspan="2"><strong>Retour</strong></td>
									</tr>
									<tr>
										<td>
											<label>
												<input type="radio" value="client" class="computeTotal delivery_mode" name="data[StockOrder][delivery_mode]" id="StockOrderDeliveryMode" <?php echo ($stockorder['StockOrder']['delivery_mode'] == 'client' OR empty($stockorder['StockOrder']['delivery_mode'])) ? 'checked' : ''; ?>> Client
											</label>
										</td>
										<td>0 CHF</td>
										<td>
											<label>
												<input type="radio" value="client" class="computeTotal return_mode" name="data[StockOrder][return_mode]" id="StockOrderReturnMode" <?php echo ($stockorder['StockOrder']['return_mode'] == 'client' OR empty($stockorder['StockOrder']['return_mode'])) ? 'checked' : ''; ?>> Client
											</label>
										</td>
										<td>0 CHF</td>
									</tr>
									<tr>
										<td>
											<label>
												<input type="radio" value="festiloc" class="computeTotal delivery_mode" name="data[StockOrder][delivery_mode]" id="StockOrderDeliveryMode" <?php echo $stockorder['StockOrder']['delivery_mode'] == 'festiloc' ? 'checked' : ''; ?>> Festiloc
											</label>
										</td>
										<td class="delivery_transportation_costs"><span><?php echo $stockorder['StockOrder']['delivery_transportation_costs'];?></span> CHF</td>
										<td>
											<label>
												<input type="radio" value="festiloc" class="computeTotal return_mode" name="data[StockOrder][return_mode]" id="StockOrderReturnMode" <?php echo $stockorder['StockOrder']['return_mode'] == 'festiloc' ? 'checked' : ''; ?>> Festiloc
											</label>
										</td>
										<td class="return_transportation_costs"><span><?php echo $stockorder['StockOrder']['return_transportation_costs'];?></span> CHF</td>
									</tr>
									<tr>
										<td>
											<label>
												<input type="radio" value="transporter" class="computeTotal delivery_mode" name="data[StockOrder][delivery_mode]" id="StockOrderDeliveryMode" <?php echo $stockorder['StockOrder']['delivery_mode'] == 'transporter' ? 'checked' : ''; ?>> Transporteur
											</label>
										</td>
										<td class="delivery_packaging_costs"><span><?php echo empty($stockorder['StockOrder']['delivery_packaging_costs']) ? 0 : $stockorder['StockOrder']['delivery_packaging_costs'];?></span> CHF</td>
										<td>
											<label>
												<input type="radio" value="transporter" class="computeTotal return_mode" name="data[StockOrder][return_mode]" id="StockOrderReturnMode" <?php echo $stockorder['StockOrder']['return_mode'] == 'transporter' ? 'checked' : ''; ?>> Transporteur
											</label>
										</td>
										<td class="return_packaging_costs"><span><?php echo empty($stockorder['StockOrder']['return_packaging_costs']) ? 0 : $stockorder['StockOrder']['return_packaging_costs'];?></span> CHF</td>
									</tr>
									<tr class="bg-grey">
										<td colspan="4"><strong>Rabais</strong></td>
									</tr>
									<tr>
										<td colspan="1">Rabais de quantité</td>
										<td colspan="3" class="quantity_discount">
											<span><?php echo $stockorder['StockOrder']['quantity_discount'];?></span><span class="help-text"> CHF</span>
											<?php echo $this->Form->input('quantity_discount_percentage', array('class' => 'form-control input-sm input-xsmall input-inline computeTotal', 'label' => false, 'div' => false, 'value' => is_null($stockorder['StockOrder']['quantity_discount_percentage']) ? '' : $stockorder['StockOrder']['quantity_discount_percentage'])); ?>
											<span class="help-text"> %</span>
											<?php echo $this->Form->input('manual_quantity_discount_percentage', array('class' => 'input-inline computeTotal', 'type' => 'checkbox', 'label' => __('Manual discount'), 'div' => false)); ?>
										</td>
									</tr>
									<tr>
										<td colspan="1">Rabais de fidélité</td>
										<td colspan="3" class="fidelity_discount_amount"><span><?php echo $stockorder['StockOrder']['fidelity_discount_amount'];?></span> CHF</td>
									</tr>
									<tr class="bg-grey">
										<td colspan="4"><strong>Totaux</strong></td>
									</tr>
									<tr class="text-muted">
										<td colspan="1">Frais de livraison interne</td>
										<td colspan="3" class="transportation_costs_info"><span></span> CHF</td>
									</tr>
									<tr class="text-muted">
										<td colspan="1">Frais de livraison externe</td>
										<td colspan="3" class="packaging_costs"><span><?php echo empty($stockorder['StockOrder']['packaging_costs']) ? 0 : $stockorder['StockOrder']['packaging_costs'] ; ?></span> CHF</td>
									</tr>
									<!-- <tr class="text-muted">
										<td colspan="1">Différence</td>
										<td colspan="3" class="costs_difference"><span><?php echo empty($stockorder['StockOrder']['costs_difference']) ? 0 : $stockorder['StockOrder']['costs_difference'] ; ?></span> CHF</td>
									</tr> -->
									<tr>
										<td colspan="1">Frais de livraison</td>
										<td colspan="3" class="delivery_costs"><span><?php echo $stockorder['StockOrder']['delivery_costs']; ?></span> CHF</td>
									</tr>
									<tr>
										<td colspan="1">Frais de livraison forcés</td>
										<td colspan="3" class=""><?php echo $this->Form->input('forced_delivery_costs', array('class' => 'form-control input-small input-inline', 'label' => false, 'div' => false, 'value' => is_null($stockorder['StockOrder']['forced_delivery_costs']) ? null : $stockorder['StockOrder']['forced_delivery_costs']));?> <span class="help-text">CHF</span></td>
									</tr>
									<tr>
										<td colspan="1">TVA</td>
										<td colspan="3" class="tva"><span><?php echo $stockorder['StockOrder']['tva']; ?></span> CHF</td>
									</tr>
									<tr>
										<td colspan="1" class="default">Total net</td>
										<td colspan="3" class="net_total">
											<span><?php echo $stockorder['StockOrder']['net_total']; ?></span> CHF
											<a href="#" class="roundNetTotal" style="margin-left: 20px">Arrondir</a>
										</td>
									</tr>
									<tr>
										<td colspan="1">Méthode de paiement</td>
										<td colspan="3" class=""><?php echo $this->Form->input('invoice_method', array('class' => 'form-control input-inline bs-select', 'label' => false, 'div' => false, 'value' => $stockorder['StockOrder']['invoice_method'], 'options' => Configure::read('StockOrders.invoice_methods')));?></td>
									</tr>
									<tr>
										<td colspan="1">Montant de l'acompte</td>
										<td colspan="3" class="">
											<?php echo $this->Form->input('deposit', array('class' => 'form-control input-inline', 'label' => false, 'div' => false, 'value' => $stockorder['StockOrder']['deposit']));?>
											<span>CHF</span>
											<a href="#" data-festiloc-export-deposit data-stockorder-id="<?php echo $stockorder['StockOrder']['id']; ?>" class="btn btn-sm btn-inverse" style="margin-left: 20px;">Exporter acompte dans Wealthings</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<?php echo $this->Form->input('delivery_transportation_costs', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['delivery_transportation_costs'])); ?>
					<?php echo $this->Form->input('return_transportation_costs', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['return_transportation_costs'])); ?>
					<?php echo $this->Form->input('total_ht', array('type' => 'hidden', 'class' => 'computeTotal'));?>
					<?php echo $this->Form->input('tva', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('net_total', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('forced_net_total', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('quantity_discount', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('fidelity_discount', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('fidelity_discount_amount', array('type' => 'hidden'));?>
					<?php echo $this->Form->input('delivery_costs', array('type' => 'hidden'));?>

					<div class="hidden">
						<h3 class="form-section"><?php echo __('Control'); ?></h3>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Has been controlled'); ?></label>
							<div class="col-md-3">
								<?php echo $this->Form->input('controlled', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Controlled by'); ?></label>
							<div class="col-md-3">
								<?php echo $this->Form->input('controlled_by', array('label' => false, 'class' => 'form-control select2me', 'options' => $controllers, 'value' => AuthComponent::user('id')));?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab6">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison"><?php echo __('Cancellation'); ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('cancellation_reason', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('StockOrders.cancellation_reasons'))); ?>
							<span class="help-block"><?php echo __('Must be filled if order has been cancelled.'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('cancellation_remarks', array('label' => false, 'div' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-actions" style="padding: 30px 0 50px 0">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue check-order" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
					<button type="submit" class="btn blue check-order" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue-hoki check-order" name="data[destination]" value="send"><i class="fa fa-send"></i> <?php echo __('Save and send'); ?></button>
					<?php echo $this->Html->link('<i class="fa fa-file-o"></i> ' . __('View offer'), array('controller' => 'stock_orders', 'action' => 'invoice', $stockorder['StockOrder']['id'] . '.pdf', '?' => 'download=0'), array('class' => 'btn default view-offer', 'escape' => false, 'target' => '_blank')); ?>
					<?php echo $this->Html->link('<i class="fa fa-copy"></i> ' . __('Duplicate offer'), array('controller' => 'stock_orders', 'action' => 'duplicate', $stockorder['StockOrder']['id']), array('class' => 'btn yellow bootbox', 'escape' => false)); ?>
				</div>
			</div>
			<?php if($stockorder['StockOrder']['status'] == 'offer'): ?>
			<br />
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<?php echo $this->Html->link('<i class="fa fa-check"></i> ' . __('Confirm'), array('action' => 'confirm', $stockorder['StockOrder']['id']), array('class' => 'btn green confirm-offer check-order', 'escape' => false, 'name' => 'data[destination]', 'value' => 'edit')); ?>
					<?php echo $this->Html->link('<i class="fa fa-send"></i> ' . __('Confirm and send'), array('action' => 'confirm', $stockorder['StockOrder']['id']), array('class' => 'btn green confirm-offer check-order', 'escape' => false, 'name' => 'data[destination]', 'value' => 'send')); ?>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php echo $this->Form->end(); ?>

<div class="modal fade" id="new-client" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('Client', array('controller' => 'clients', 'action' => 'add')); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new client / contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Client'); ?></h3>
						<div class="form-group">
							<label class="control-label"><?php echo __('Business name'); ?></label>
							<?php echo $this->Form->input('Client.name', array('class' => 'form-control', 'label' => false, 'value' => '', 'required' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('Client.companies', array('class' => 'form-control bs-select', 'label' => false, 'options' => $companies, 'value' => 3, 'multiple' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Address'); ?></label>
							<?php echo $this->Form->input('Client.address', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('ZIP'); ?></label>
							<?php echo $this->Form->input('Client.zip', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('City'); ?></label>
							<?php echo $this->Form->input('Client.city', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
						<?php echo $this->Form->input('ContactPeople.0.status', array('value' => 1, 'type' => 'hidden')); ?>
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.first_name', array('class' => 'form-control', 'label' => false, 'required' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.last_name', array('class' => 'form-control', 'label' => false, 'required' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.email', array('type' => 'email', 'class' => 'form-control', 'label' => false, 'required' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.phone', array('class' => 'form-control', 'label' => false, 'required' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.0.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>

<?php echo $this->element('Modals/contact_people_add'); ?>

<div class="modal fade" id="edit-client" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('Client', array('controller' => 'clients', 'action' => 'edit')); ?>
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Edit client'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Business name'); ?></label>
							<?php echo $this->Form->input('Client.name', array('class' => 'form-control', 'label' => false, 'value' => '', 'required' => true)); ?>
							<?php echo $this->Form->input('Client.id', array('type' => 'hidden')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('Client.companies', array('class' => 'form-control bs-select', 'label' => false, 'options' => $companies, 'multiple' => true)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Address'); ?></label>
							<?php echo $this->Form->input('Client.address', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('ZIP'); ?></label>
							<?php echo $this->Form->input('Client.zip', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('City'); ?></label>
							<?php echo $this->Form->input('Client.city', array('class' => 'form-control', 'label' => false, 'value' => '')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="edit-contact-people" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('ContactPeople', array('controller' => 'contact_peoples', 'action' => 'edit')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Edit contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.id', array('type' => 'hidden')); ?>
							<?php echo $this->Form->input('ContactPeople.client_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('ContactPeople.status', array('value' => 1, 'type' => 'hidden')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('type' => 'email', 'class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>

<?php
$this->start('init_scripts');
echo 'Custom.stockorders();';
echo 'Custom.stockitems();';
$this->end();
?>
