<?php $moments = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row invoice-logo">
				<div class="col-xs-6">
					<img src="http://www.festiloc.ch/data/web/festiloc2.ch/templates/img/logo.png" class="img-responsive" alt="">					
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41(0)26 676 01 17</li>
						<li>F + 41(0)26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>CHE-112.145.798 TVA</li>
					</ul>
				</div>
				<!-- <div class="col-xs-6 text-right">
					
					<h5><?php echo $this->Time->format(time(), '%e %b %Y'); ?></h5>
				</div> -->
			</div>
			<div class="row">
				<div class="col-xs-6 col-xs-offset-6">
					<ul class="list-unstyled">
						<li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
						<li><?php echo $stockorder['Client']['name']; ?></li>
						<li>
							<?php echo $civilities[$stockorder['ContactPeopleClient']['civility']]; ?>
							<?php echo $stockorder['ContactPeopleClient']['name']; ?>
						</li>
						<li><?php echo $stockorder['Client']['address']; ?></li>
						<li><?php echo $stockorder['Client']['zip_city']; ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled">
						<li><strong>Concerne: <?php echo $stockorder['StockOrder']['concern']; ?></strong></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<h4>Livraison</h4>
					<ul class="list-unstyled">
						<li>
							<strong>Mode de livraison</strong><br />
							<?php echo $deliveryModes[$stockorder['StockOrder']['delivery_mode']] ; ?>
						</li>
						<li>
							<strong>Date</strong><br />
							<?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %b %Y'); ?> (<?php echo strtolower($moments[$stockorder['StockOrder']['delivery_moment']]); ?>)
						</li>
						<li>
							<strong>Adresse</strong><br />
							<?php echo $stockorder['StockOrder']['delivery_address'] ?><br />
							<?php echo $stockorder['StockOrder']['delivery_zip'] ?> <?php echo $stockorder['StockOrder']['delivery_city'] ?>
						</li>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo $civilities[$stockorder['ContactPeopleDelivery']['civility']]; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['name']; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
						</li>
					</ul>
				</div>
				<div class="col-xs-6">
					<h4>Retour</h4>
					<ul class="list-unstyled">
						<li>
							<strong>Mode de retour</strong><br />
							<?php echo $returnModes[$stockorder['StockOrder']['return_mode']] ; ?>
						</li>
						<li>
							<strong>Date</strong><br />
							<?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %b %Y'); ?> (<?php echo strtolower($moments[$stockorder['StockOrder']['return_moment']]); ?>)
						</li>
						<li>
							<strong>Adresse</strong><br />
							<?php echo $stockorder['StockOrder']['return_address'] ?><br />
							<?php echo $stockorder['StockOrder']['return_zip'] ?> <?php echo $stockorder['StockOrder']['return_city'] ?>
						</li>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo $civilities[$stockorder['ContactPeopleReturn']['civility']]; ?>
							<?php echo $stockorder['ContactPeopleReturn']['name']; ?>
							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h4 style="margin: 0; margin-bottom: 5px">Offre #<?php echo $stockorder['StockOrder']['id']; ?></h4>
					<h5 style="margin: 0">Date de prestation du <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['StockOrder']['service_date_end'], '%d.%m.%y'); ?></h5>
					<hr style="margin: 5px 0">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo __('Stock item'); ?></th>
							<th><?php echo __('Quantity'); ?></th>
							<th><?php echo __('Prix HT'); ?></th>
							<th><?php echo __('Total'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<tr>
								<td><?php echo $k + 1; ?></td>
								<td><?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?></td>
								<td><?php echo $batch['StockItem']['quantity']; ?></td>
								<td><?php echo $batch['StockItem']['price']; ?></td>
								<td><?php echo number_format((float)$batch['StockItem']['quantity'] * $batch['StockItem']['price'], 2, '.', '');?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<table class="table">
						<tr>
							<td>Total HT</td>
							<td>2045.00 CHF</td>
						</tr>
						<tr>
							<td>Frais de transport</td>
							<td>200.00 CHF</td>
						</tr>
						<tr>
							<td>Frais de conditionnement</td>
							<td>340.00 CHF</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-6">
					<table class="table">
						<tr>
							<td>TVA 8%</td>
							<td>200.00 CHF</td>
						</tr>
						<tr>
							<td>Rabais</td>
							<td>200.00 CHF</td>
						</tr>
						<tr>
							<td><strong>Total de l'offre</strong></td>
							<td><strong>3000.00 CHF</strong></td>
						</tr>
					</table>
				</div>
				<div class="col-xs-12">
					<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Print'), array(), array('class' => 'btn btn-lg blue hidden-print margin-bottom-5', 'escape' => false, 'onclick' => 'javascript:window.print();')) ; ?>
					<?php echo $this->Html->link('<i class="fa fa-download"></i> ' . __('Download PDF'), array('action' => 'pdf', $stockorder['StockOrder']['id']), array('class' => 'btn btn-lg blue hidden-print margin-bottom-5', 'escape' => false)) ; ?>
				</div>
				<div class="col-xs-12">
					<p>En attendant le plaisir de recevoir votre commande.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
echo $this->Html->script('/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.init();';
echo 'Custom.stockorders();';
$this->end();
?>