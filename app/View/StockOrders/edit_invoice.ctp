<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', empty($stockorder['StockOrder']['name']) ? __('Order %s', empty($stockorder['StockOrder']['order_number']) ? $stockorder['StockOrder']['id'] : $stockorder['StockOrder']['order_number']) : $stockorder['StockOrder']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-shopping-cart font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $stockorder['StockOrder']['order_number']; ?> <?php echo empty($stockorder['StockOrder']['name']) ? __('Order %s', empty($stockorder['StockOrder']['order_number']) ? $stockorder['StockOrder']['id'] : $stockorder['StockOrder']['order_number']) : $stockorder['StockOrder']['name'] ?>
			</span>
		</div>
		<div class="actions">
			<?php if(!empty($stockorder['StockOrder']['uuid'])): ?>
				<small class="text-success bold"><?php echo $stockorder['StockOrder']['uuid']; ?></small>
			<?php endif; ?>
		</div>
	</div>
	<div class="portlet-body form">
		<?php echo $this->Form->create(); ?>
		<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('StockOrder.replacement_costs', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('StockOrder.extrahours_costs', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('StockOrder.net_total', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('StockOrder.total_ht', array('type' => 'hidden')); ?>
		<?php echo $this->Form->input('StockOrder.tva', array('type' => 'hidden')); ?>
    <?php echo $this->Form->input('destination', array('type' => 'hidden', 'name' => 'data[destination]')); ?>
		<div class="row">
			<div class="col-md-12">
				<h3><?php echo __('Date of service'); ?></h3> <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d %b %Y'); ?> - <?php echo $this->Time->format($stockorder['StockOrder']['service_date_end'], '%d %b %Y'); ?>
			</div>
			<div class="col-md-4">
				<h3><?php echo __('Client'); ?></h3>
				<ul class="list-unstyled">
					<li><?php echo $stockorder['Client']['name']; ?></li>
					<li><?php echo $stockorder['Client']['address']; ?></li>
					<li><?php echo $stockorder['Client']['zip_city']; ?></li>
					<li><?php echo $stockorder['ContactPeopleClient']['name']; ?></li>
					<li><?php echo $stockorder['ContactPeopleClient']['phone']; ?></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3><?php echo __('Delivery'); ?></h3>
				<ul class="list-unstyled">
					<li><strong><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%d %b %Y'); ?></strong></li>
					<li><?php echo $stockorder['StockOrder']['delivery_address']; ?></li>
					<li><?php echo $stockorder['StockOrder']['delivery_zip_city']; ?></li>
					<li><?php echo $stockorder['ContactPeopleDelivery']['name']; ?></li>
					<li><?php echo $stockorder['ContactPeopleDelivery']['phone']; ?></li>
				</ul>
			</div>
			<div class="col-md-4">
				<h3><?php echo __('Return'); ?></h3>
				<ul class="list-unstyled">
					<li><strong><?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%d %b %Y'); ?></strong></li>
					<li><?php echo $stockorder['StockOrder']['return_address']; ?></li>
					<li><?php echo $stockorder['StockOrder']['return_zip_city']; ?></li>
					<li><?php echo $stockorder['ContactPeopleReturn']['name']; ?></li>
					<li><?php echo $stockorder['ContactPeopleReturn']['phone']; ?></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>Produits</h3>
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo __('Stock item'); ?></th>
							<th><?php echo __('Coefficient'); ?></th>
							<th><?php echo __('Ordered quantity'); ?></th>
							<th><?php echo __('Delivered quantity'); ?></th>
							<th><?php echo __('Returned quantity'); ?></th>
							<th><?php echo __('Net HT'); ?></th>
							<th><?php echo __('Discount'); ?></th>
							<th><?php echo __('Total TTC'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<?php if($batch['stock_item_id'] == -1){
								$price = $batch['price'];
								$code = '';
								$name = $batch['name'];
								$id = -1;
							} else {
								$price = $batch['StockItem']['price'];
								$code = $batch['StockItem']['code'];
								$name = $batch['StockItem']['name'];
								$id = $batch['StockItem']['id'];
							} ?>
							<tr>
								<td><?php echo $k + 1; ?></td>
								<td><?php echo $code . ' ' . $name; ?></td>
								<td><?php echo $batch['coefficient']; ?></td>
								<td><?php echo $batch['quantity']; ?></td>
								<td><?php echo is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']; ?></td>
								<?php if( (is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) > $batch['returned_quantity'] ): ?>
								<td class="danger"><i class="fa fa-times text-danger"></i> <?php echo $batch['returned_quantity']; ?></td>
								<?php elseif(!is_null($batch['quantity'])): ?>
								<td class="success"><i class="fa fa-check text-success"></i> <?php echo $batch['returned_quantity']; ?></td>
								<?php else: ?>
								<td>&nbsp;</td>
								<?php endif; ?>
								<td><?php echo number_format((float)$batch['coefficient'] * $batch['quantity'] * $price, 2, '.', '');?></td>
								<td><?php echo !empty($batch['discount']) ? $batch['discount'] : '0.00'; ?>%</td>
								<td><?php echo !empty($batch['discount']) ? number_format((float)$batch['coefficient'] * $batch['quantity'] * $price * (100-$batch['discount'])/100, 2, '.', '') : number_format((float)$batch['coefficient'] * $batch['quantity'] * $price, 2, '.', ''); ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php if(!empty($stockorder['StockOrder']['remarks'])): ?>
			<div class="col-md-12">
				<h3>Remarques générales</h3>
				<?php echo $this->Form->input('StockOrder.remarks', array('label' => false, 'class' => 'form-control', 'value' => $stockorder['StockOrder']['remarks'])); ?>
			</div>
			<?php endif; ?>
			<?php if(!empty($stockorder['StockOrder']['return_control_remarks'])): ?>
			<div class="col-md-12">
				<h3>Remarques du retour</h3>
				<p><?php echo $stockorder['StockOrder']['return_control_remarks']; ?></p>
			</div>
			<?php endif; ?>
			<?php if(!empty($batchesToReplace)):?>
			<div class="col-md-12">
				<h3>Frais de remplacement</h3>
				<table class="table" id="replacementCostsTable">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo __('Stock item'); ?></th>
							<th><?php echo __('Missing quantity'); ?></th>
							<th><?php echo __('Replacement price'); ?></th>
							<th><?php echo __('Total'); ?></th>
							<th><?php echo __('Invoice?'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($batchesToReplace as $k => $batch): ?>
							<?php if($batch['stock_item_id'] == -1){
								$price = $batch['price'];
								$code = '';
								$name = $batch['name'];
								$id = -1;
							} else {
								$price = $batch['StockItem']['price'];
								$code = $batch['StockItem']['code'];
								$name = $batch['StockItem']['name'];
								$id = $batch['StockItem']['id'];
							} ?>
							<?php $replacementPrice = empty($batch['StockItem']['replacement_price']) ? $price * 5 : $batch['StockItem']['replacement_price']; ?>
							<tr>
								<td><?php echo $k+1; ?></td>
								<td><?php echo $code . ' ' . $name; ?></td>
								<td><?php echo (is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) - $batch['returned_quantity']; ?></td>
								<td><?php echo number_format($replacementPrice, 2, '.', ''); ?></td>
								<td><?php echo number_format( ((is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) - $batch['returned_quantity']) * $replacementPrice, 2, '.', ''); ?></td>
								<td><?php echo $this->Form->input('StockOrderBatch.'.$k.'.invoice_replacement', array('type' => 'checkbox', 'class' => 'form-control', 'data-field' => 'invoice_replacement', 'data-id' => $batch['id'], 'data-controller' => 'stock_order_batches', 'label' => false, 'div' => false, 'checked' => $batch['invoice_replacement'] ? 'checked' : '')); ?></td>
								<td><?php echo $this->Form->input('StockOrderBatch.'.$k.'.stock_item_id', array('type' => 'hidden', 'value' => $batch['stock_item_id'])); ?></td>
								<?php echo $this->Form->input('replacement_cost', array('type' => 'hidden', 'value' => ((is_null($batch['delivered_quantity']) ? $batch['quantity'] : $batch['delivered_quantity']) - $batch['returned_quantity']) * $replacementPrice, 'class' => 'replacement_cost')); ?>
								<?php echo $this->Form->input('StockOrderBatch.'.$k.'.id', array('type' => 'hidden', 'value' => $batch['id'], 'class' => 'batch_id')); ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<?php endif; ?>
			<?php if(!empty($stockorder['StockOrderExtraHour'])): ?>
			<div class="col-md-12">
				<h3><?php echo __('Extra hours') ?></h3>
				<table class="table" id="extraHoursCostsTable">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo __('Task'); ?></th>
							<th><?php echo __('Duration'); ?></th>
							<th><?php echo __('Price'); ?></th>
							<th><?php echo __('Invoice?'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderExtraHour'] as $k => $task): ?>
							<tr>
								<td><?php echo $k + 1; ?></td>
								<td><?php echo $task['task']; ?></td>
								<td><?php echo $task['duration']; ?> minutes</td>
								<td><span><?php echo $task['duration']; ?></span> CHF</td>
								<td><?php echo $this->Form->input('StockOrderExtraHour.'.$k.'.invoice', array('class' => 'form-control invoice_extrahour', 'label' => false, 'div' => false, 'data-field' => 'invoice', 'data-id' => $task['id'], 'data-controller' => 'stock_order_extra_hours')); ?></td>
								<?php echo $this->Form->input('extrahour_cost', array('type' => 'hidden', 'value' => $task['duration'], 'class' => 'extrahour_cost')); ?>
								<?php echo $this->Form->input('extrahour_id', array('type' => 'hidden', 'value' => $task['id'], 'class' => 'extrahour_id')); ?>
								<?php echo $this->Form->input('StockOrderExtraHour.'.$k.'.id', array('type' => 'hidden', 'value' => $task['id'], 'class' => 'extrahour_id')); ?>
								<?php echo $this->Form->input('StockOrderExtraHour.'.$k.'.duration', array('type' => 'hidden', 'value' => $task['duration'], 'class' => 'extrahour_id')); ?>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<?php endif; ?>
		</div>
		<div class="row" data-festiloc-invoice-totals data-stockorder-id="<?php echo $stockorder['StockOrder']['id']; ?>"></div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-actions">
					<div class="row">
						<div class="col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
							<button type="submit" class="btn btn-success" name="data[destination]" value="validate"><i class="fa fa-check"></i> <?php echo __('Validate invoice'); ?></button>
							<?php echo $this->Html->link('<i class="fa fa-file-o"></i> ' . __('View invoice'), array('controller' => 'stock_orders', 'action' => 'invoice', $stockorder['StockOrder']['id'] . '.pdf', '?' => 'download=0'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
							<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Modifier le retour'), array('controller' => 'festiloc', 'action' => 'depot', 'return', $stockorder['StockOrder']['id']), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
							<?php echo $this->Html->link('<i class="fa fa-list"></i> ' . __('Back to stock orders'), array('controller' => 'stock_orders', 'action' => 'index'), array('class' => 'btn default', 'escape' => false)); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockorders();';
$this->end();
?>
