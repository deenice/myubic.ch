<?php $moments = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<?php
if($stockorder['StockOrder']['status'] == 'offer'){
	$designation = __('Offer');
} elseif($stockorder['StockOrder']['status'] == 'confirmed'){
	$designation = __('Order confirmation');
} else {
	$designation = __('Delivery note');
}
?>
<style type="text/css" media="screen,print">
.break{
   display: block;
   clear: both;
   page-break-after: always;
}
h3 {
	margin: 0;
}
h3.panel-title {
	text-transform: uppercase;
}
.portlet {
	margin: 0;
}
.smaller {
	font-size: 1.2em;
}
</style>


<?php $iterator = 1; for($i=0; $i < $packaging['total']; $i++): ?>
<?php $type = $packaging['distribution'][$i]; ?>
<?php $nextType = !empty($packaging['distribution'][$i+1]) ? $packaging['distribution'][$i+1] : $type; ?>
<?php $prevType = !empty($packaging['distribution'][$i-1]) ? $packaging['distribution'][$i-1] : $type; ?>
<?php if($type != $prevType){
	$iterator = 1;
} ?>
<?php //debug($packaging); exit; ?>
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row invoice-logo">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">					
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>CHE-112.145.798 TVA</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h2>
						FICHE 
						<?php if($type == 'pallets'): ?>
							PALETTE
						<?php elseif($type == 'pallets_xl'): ?>
							PALETTE XL
						<?php elseif($type == 'rollis'): ?>
							ROLLI
						<?php elseif($type == 'rollis_xl'): ?>
							ROLLI XL
						<?php endif; ?> - 
						<?php if($stockorder['StockOrder']['delivery']): ?>
							<?php if($stockorder['StockOrder']['delivery_mode'] == 'transporter'): ?>
								<?php if(!empty($stockorder['DeliveryTransporter'])): ?>
									<?php echo strtoupper($stockorder['DeliveryTransporter']['name']); ?>
								<?php else: ?>
									TRANSPORTEUR
								<?php endif; ?>
							<?php elseif($stockorder['StockOrder']['delivery_mode'] == 'festiloc'): ?>
								FESTILOC
							<?php endif; ?>
						<?php else: ?>
							CLIENT
						<?php endif; ?>
						<span class="pull-right"><?php echo $iterator; ?> / <?php echo $packaging[$type]; ?></span>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Client</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['Client']['name'] ?></h3>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de livraison</h3>
						</div>
						<div class="panel-body">
							 <h3>
						 	<?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
						 		<?php echo $stockorder['StockOrder']['delivery_place'] ?> - 
						 	<?php endif; ?>
							 <?php echo $stockorder['StockOrder']['delivery_address'] ?> - <?php echo $stockorder['StockOrder']['delivery_zip_city']; ?></h3>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Numéro de commande / client</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['StockOrder']['id'] ?> / <?php echo $stockorder['Client']['id']; ?></h3>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Poids</h3>
						</div>
						<div class="panel-body" style="height: 105px"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Personne de contact</h3>
						</div>
						<div class="panel-body">
							<h3>
								<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?><br />
								<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
							</h3>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Conditionnement</h3>
						</div>
						<div class="panel-body">
						 	<?php if(!empty($packaging['pallets'])): ?>
						 		<h3 class="smaller"><?php echo $packaging['pallets'] ?> palettes</h3>
						 	<?php endif; ?>
						 	<?php if(!empty($packaging['rollis'])): ?>
						 		<h3 class="smaller"><?php echo $packaging['rollis'] ?> rollis</h3>
						 	<?php endif; ?>
						 	<?php if(!empty($packaging['pallets_xl'])): ?>
						 		<h3 class="smaller"><?php echo $packaging['pallets_xl'] ?> palettes XL</h3>
						 	<?php endif; ?>
						 	<?php if(!empty($packaging['rollis_xl'])): ?>
						 		<h3 class="smaller"><?php echo $packaging['rollis_xl'] ?> rollis XL</h3>
						 	<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Date de livraison</h3>
						</div>
						<div class="panel-body">
							<h3><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?></h3>
							<?php if($stockorder['StockOrder']['delivery_moment'] == 'hour'): ?>
							<h3><?php echo $this->Time->format($stockorder['StockOrder']['delivery_moment_hour'], '%Hh%M'); ?></h3>
							<?php else: ?>
							<h3><?php echo __($moments[$stockorder['StockOrder']['delivery_moment']]); ?></h3>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques livraison</h3>
						</div>
						<?php if(empty($stockorder['StockOrder']['delivery_remarks'])): ?>
							<div class="panel-body" style="height: 100px"></div>
						<?php else: ?>
							<div class="panel-body">
								<p><?php echo $stockorder['StockOrder']['delivery_remarks']; ?></p>
							</div>
						<?php endif; ?>						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<?php if($type == 'pallets'): ?>
								<h3 class="panel-title">Numéro de palette</h3>
							<?php elseif($type == 'pallets_xl'): ?>
								<h3 class="panel-title">Numéro de palette XL</h3>
							<?php elseif($type == 'rollis'): ?>
								<h3 class="panel-title">Numéro de rolli</h3>
							<?php elseif($type == 'rollis_xl'): ?>
								<h3 class="panel-title">Numéro de rolli XL</h3>
							<?php endif; ?>
							
						</div>
						<div class="panel-body">
							<h3><?php echo $iterator; ?> / <?php echo $packaging[$type]; ?></h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($i != $packaging['total'] - 1): ?>
<div class="break"></div>
<?php endif; ?>
<?php $iterator++; endfor; ?>