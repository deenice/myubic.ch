<?php $moments1 = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<style type="text/css">
	.uppercase {
		text-transform: uppercase;
	}
	table thead th {
		vertical-align: top;
	}
	table.totals,
	.totals tr,
	.totals td {
		page-break-inside: avoid !important;
	}
	.break{
	   display: block;
	   clear: both;
	   page-break-after: always;
	}
	h4 {
		font-weight: normal;
		margin: 0;
	}
	h4 strong {
		text-transform: uppercase;
		font-weight: 700;
	}
  ul.list-unstyled {
    margin-top: 8px;
  }
  ul.list-unstyled li {
    padding-left: 5px;
  }
</style>
<?php foreach($stockorders as $k => $stockorder): ?>
<?php if($k > 0 && isset($stockorders[$k-1])): ?>
<div class="break"></div>
<?php endif; ?>
<div class="portlet light" style="padding-top: 0; padding-bottom: 0">
	<div class="portlet-body" style="padding-top: 0; padding-bottom: 0">
		<div class="invoice">
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>Heures d'ouverture</li>
						<li>8h30 - 11h45 / 13h30 - 16h45</li>
					</ul>
				</div>
			</div>
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<div class="" style="border: 1px solid #999; height: 80px; padding: 10px; text-align: center; font-size: 40px;">
						<strong>RETOUR</strong>
					</div>
				</div>
				<div class="col-xs-6 col-xs-offset-61">
					<ul class="list-unstyled client">
						<li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
						<?php if(!empty($stockorder['StockOrder']['invoice_client'])): ?>
						<li><?php echo $stockorder['StockOrder']['invoice_client']; ?></li>
						<li><?php echo $stockorder['StockOrder']['invoice_contact_person']; ?></li>
						<?php else: ?>
						<?php if($stockorder['ContactPeopleClient']['full_name'] != $stockorder['Client']['name']): ?>
						<li><?php echo $stockorder['Client']['name']; ?></li>
						<?php endif; ?>
						<li>
							<?php echo (!empty($civilities[$stockorder['ContactPeopleClient']['civility']])) ? $civilities[$stockorder['ContactPeopleClient']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
						</li>
						<?php endif; ?>
						<li><?php echo $stockorder['StockOrder']['invoice_address']; ?></li>
						<li><?php echo $stockorder['StockOrder']['invoice_zip_city']; ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<?php if(!empty($stockorder['StockOrder']['name'])): ?><strong>Concerne: <?php echo $stockorder['StockOrder']['name']; ?></strong><?php endif; ?>
				</div>
				<div class="col-xs-6">
					Villars-sur-Glâne le <?php echo $this->Time->format($stockorder['StockOrder']['modified'], '%e %b %Y'); ?>
					<?php if(!empty($stockorder['StockOrder']['import_id'])): ?>
						 / <?php echo $stockorder['StockOrder']['import_id']; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<div class="col-xs-6">
					<h4>Livraison</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['StockOrder']['delivery']): ?>
								<strong class="uppercase">AVEC LIVRAISON <?php echo (!empty($stockorder['StockOrder']['delivery_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['delivery_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong>EMPORTÉ PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['StockOrder']['delivery_moment'])): ?>
							(<?php echo strtolower($moments1[$stockorder['StockOrder']['delivery_moment']]); ?><?php if($stockorder['StockOrder']['delivery_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['delivery_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['delivery_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
							<?php echo $stockorder['StockOrder']['delivery_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['StockOrder']['delivery_address'])): ?>
							<?php echo $stockorder['StockOrder']['delivery_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['StockOrder']['delivery_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleDelivery']['civility'])) ? $civilities[$stockorder['ContactPeopleDelivery']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['delivery_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php if($stockorder['StockOrder']['type'] != 'sale'): ?>
				<div class="col-xs-6">
					<h4>Retour</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['StockOrder']['return']): ?>
								<strong class="uppercase">AVEC REPRISE <?php echo (!empty($stockorder['StockOrder']['return_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['return_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong class="uppercase">RETOUR PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['StockOrder']['return_moment'])): ?>
							(<?php echo strtolower($moments1[$stockorder['StockOrder']['return_moment']]); ?><?php if($stockorder['StockOrder']['return_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['return_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['return_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['StockOrder']['return_place'])): ?>
							<?php echo $stockorder['StockOrder']['return_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['StockOrder']['return_address'])): ?>
							<?php echo $stockorder['StockOrder']['return_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['StockOrder']['return_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleReturn']['civility'])) ? $civilities[$stockorder['ContactPeopleReturn']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['return_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['StockOrder']['return_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php endif; ?>
        <div class="col-xs-12">
          <h4>Conditionnement</h4>
          <ul class="list-unstyled">
            <?php if(!empty($stockorder['packaging']['pallets'])): ?><li><?php echo __('%s pallet(s)', $stockorder['packaging']['pallets']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['pallets_xl'])): ?><li><?php echo __('%s pallet(s) XL', $stockorder['packaging']['pallets_xl']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['rollis'])): ?><li><?php echo __('%s rollis(s)', $stockorder['packaging']['rollis']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['rollis_xl'])): ?><li><?php echo __('%s rollis(s) XL', $stockorder['packaging']['rollis_xl']); ?></li><?php endif; ?>
          </ul>
        </div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled offer">
						<li>
							<h4><?php echo __('Order'); ?> <?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?></h4>
						</li>
						<?php if($stockorder['StockOrder']['type'] != 'sale'): ?>
						<li>
							<strong>
							<?php if($stockorder['StockOrder']['service_date_begin'] == $stockorder['StockOrder']['service_date_end'] OR empty($stockorder['StockOrder']['service_date_end'])): ?>
							Date de prestation <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?>
							<?php else: ?>
							Date de prestation du <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['StockOrder']['service_date_end'], '%d.%m.%y'); ?>
							<?php endif; ?>
							</strong>
						</li>
						<?php endif; ?>
					</ul>
					<hr style="margin: 15px 0 0 0">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th style="width: 50%"><?php echo __('Stock item'); ?></th>
							<th class=""><?php echo __('Delivered quantity'); ?></th>
							<th class=""><?php echo __('Returned quantity'); ?></th>
							<th><?php echo __('Remarks'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<tr>
								<td style="width: 2%"><?php echo $k + 1; ?></td>
								<td>
									<?php if($batch['stock_item_id'] == -1): ?>
									<?php echo $batch['name']; ?>
									<?php else: ?>
									<?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
									<?php endif; ?>
									<?php if(!empty($batch['StockItem']['number_of_items_per_container'])): ?>
										<small><br><?php echo $batch['StockItem']['number_of_items_per_container']; ?> pièces par type de stockage</small>
									<?php endif; ?>
								</td>
								<td class=""><?php echo !is_null($batch['delivered_quantity']) ? $batch['delivered_quantity'] : $batch['quantity']; ?></td>
								<td class=""><input type="text" name="name" value="" class="form-control input-xsmall input-sm"></td>
								<td><input type="text" name="name" value="<?php echo $batch['delivery_remarks']; ?>" class="form-control input-sm input-large"></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?>
