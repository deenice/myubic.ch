<?php $moments = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<style type="text/css" media="screen,print">
.break{
   display: block;
   clear: both;
   page-break-after: always;
}
h2 {
  font-size: 22px;
  font-weight: 700;
}
h3 {
	margin: 0;
  font-size: 18px;
}
h3.panel-title {
	text-transform: uppercase;
  font-weight: 700;
}
h3.panel-title.smaller {
	font-size: 1.1em;
}
.panel-body h1 {
	margin: 0;
  font-size: 22px;
}
.panel-body h2 {
	font-weight: normal;
}
.portlet {
	margin: 0;
}
</style>
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row invoice-logo">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>CHE-112.145.798 TVA</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h2>
						<?php if($data['type'] == 'delivery'): ?>
						Livraison du <?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d.%m.%Y'); ?>
						<?php elseif($data['type'] == 'return'): ?>
						Reprise du <?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d.%m.%Y'); ?>
						<?php endif; ?>
						 -
						 <?php if($stockorder['StockOrder'][$data['type']]): ?>
							<?php if($stockorder['StockOrder'][$data['type'].'_mode'] == 'transporter'): ?>
								<?php if(!empty($stockorder[ucfirst($data['type']) . 'Transporter'])): ?>
									<?php echo strtoupper($stockorder[ucfirst($data['type']) . 'Transporter']['name']); ?>
								<?php else: ?>
									TRANSPORTEUR
								<?php endif; ?>
							<?php elseif($stockorder['StockOrder'][$data['type'] . '_mode'] == 'festiloc'): ?>
								FESTILOC
							<?php endif; ?>
						<?php else: ?>
							CLIENT
						<?php endif; ?>
					</h2>
				</div>
				<?php if($stockorder['StockOrder']['invoice_method'] == 'on_site'): ?>
				<div class="col-xs-5">
					<h2 class="uppercase font-red" style="margin: 0 0 20px 0"><i class="fa fa-warning font-red"></i> Paiement sur place</h2>
				</div>
				<div class="col-xs-4">
					<div class="">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</span>
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="">
						<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-money"></i>
							</span>
							<input type="text" class="form-control">
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Client</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['Client']['name'] ?></h3>
						</div>
					</div>
				</div>
				<?php if($data['type'] == 'delivery'): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de livraison</h3>
						</div>
						<div class="panel-body">
							 <h3>
							 	<?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
							 		<?php echo $stockorder['StockOrder']['delivery_place'] ?> -
							 	<?php endif; ?>
							 	<?php echo $stockorder['StockOrder']['delivery_address'] ?> - <?php echo $stockorder['StockOrder']['delivery_zip_city']; ?>
							 	</h3>
						</div>
					</div>
				</div>
				<?php elseif($data['type'] == 'return'): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de reprise</h3>
						</div>
						<div class="panel-body">
							 <h3>
							 	<?php if(!empty($stockorder['StockOrder']['return_place'])): ?>
							 		<?php echo $stockorder['StockOrder']['return_place'] ?> -
							 	<?php endif; ?>
							 	<?php echo $stockorder['StockOrder']['return_address'] ?> - <?php echo $stockorder['StockOrder']['return_zip_city']; ?></h3>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Adresse de livraison</h3>
						</div>
						<div class="panel-body">
							 <h3>Festiloc - Route du Tir Fédéral 10 - 1763 Givisiez</h3>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<div class="col-xs-6">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Numéro de commande</h3>
						</div>
						<div class="panel-body">
							 <h3><?php echo $stockorder['StockOrder']['id'] ?> / <?php echo $stockorder['Client']['id']; ?></h3>
						</div>
					</div>
				</div>
        <?php if($data['type'] == 'delivery'): ?>
          <div class="col-xs-6">
  					<div class="panel panel-default">
  						<div class="panel-heading bg-grey">
  							<h3 class="panel-title">Personne de contact</h3>
  						</div>
  						<div class="panel-body">
  							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
  							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
  						</div>
  					</div>
  				</div>
        <?php endif; ?>
        <?php if($data['type'] == 'return'): ?>
          <div class="col-xs-6">
  					<div class="panel panel-default">
  						<div class="panel-heading bg-grey">
  							<h3 class="panel-title">Personne de contact</h3>
  						</div>
  						<div class="panel-body">
  							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
  							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
  						</div>
  					</div>
  				</div>
        <?php endif; ?>
			</div>
			<div class="row">
				<?php if($data['type'] == 'delivery'): ?>
				<?php if(!empty($stockorder['StockOrder']['delivery_remarks'])): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques livraison</h3>
						</div>
						<div class="panel-body">
							<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endif; ?>
				<?php if($data['type'] == 'return'): ?>
				<?php if(!empty($stockorder['StockOrder']['return_remarks'])): ?>
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques reprise</h3>
						</div>
						<div class="panel-body">
							<?php echo $stockorder['StockOrder']['return_remarks']; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php endif; ?>
			</div>
			<div class="row">
				<?php if(!empty($packaging['pallets'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Palettes</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['pallets'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['pallets_xl'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Palettes XL</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['pallets_xl'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Rollis</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['rollis'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis_xl'])): ?>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title smaller">Rollis XL</h3>
						</div>
						<div class="panel-body text-center">
							 <h1><?php echo $packaging['rollis_xl'] ?></h1>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Poids</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Date</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Visa</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="panel panel-default">
						<div class="panel-heading bg-grey">
							<h3 class="panel-title">Remarques</h3>
						</div>
						<div class="panel-body" style="height: 80px"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="break"></div>
<div class="portlet light" style="padding-top: 0">
	<div class="portlet-body" style="padding-top: 0">
		<div class="invoice">
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>Heures d'ouverture</li>
						<li>8h30 - 11h45 / 13h30 - 16h45</li>
					</ul>
				</div>
			</div>
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6 col-xs-offset-6">
					<ul class="list-unstyled client">
						<li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
						<?php if(!empty($stockorder['StockOrder']['invoice_client'])): ?>
						<li><?php echo $stockorder['StockOrder']['invoice_client']; ?></li>
						<li><?php echo $stockorder['StockOrder']['invoice_contact_person']; ?></li>
						<?php else: ?>
						<?php if($stockorder['ContactPeopleClient']['full_name'] != $stockorder['Client']['name']): ?>
						<li><?php echo $stockorder['Client']['name']; ?></li>
						<?php endif; ?>
						<li>
							<?php echo (!empty($civilities[$stockorder['ContactPeopleClient']['civility']])) ? $civilities[$stockorder['ContactPeopleClient']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
						</li>
						<?php endif; ?>
						<li><?php echo $stockorder['StockOrder']['invoice_address']; ?></li>
						<li><?php echo $stockorder['StockOrder']['invoice_zip_city']; ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<?php if(!empty($stockorder['StockOrder']['name'])): ?><strong>Concerne: <?php echo $stockorder['StockOrder']['name']; ?></strong><?php endif; ?>
				</div>
				<div class="col-xs-6">
					Villars-sur-Glâne le <?php echo $this->Time->format($stockorder['StockOrder']['modified'], '%e %b %Y'); ?>
					<?php if(!empty($stockorder['StockOrder']['import_id'])): ?>
						 / <?php echo $stockorder['StockOrder']['import_id']; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<div class="col-xs-6">
					<h4>Livraison</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['StockOrder']['delivery']): ?>
								<strong class="uppercase">AVEC LIVRAISON <?php echo (!empty($stockorder['StockOrder']['delivery_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['delivery_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong>EMPORTÉ PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['StockOrder']['delivery_moment'])): ?>
							(<?php echo strtolower($moments[$stockorder['StockOrder']['delivery_moment']]); ?><?php if($stockorder['StockOrder']['delivery_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['delivery_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['delivery_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['StockOrder']['delivery_place'])): ?>
							<?php echo $stockorder['StockOrder']['delivery_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['StockOrder']['delivery_address'])): ?>
							<?php echo $stockorder['StockOrder']['delivery_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['StockOrder']['delivery_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleDelivery']['civility'])) ? $civilities[$stockorder['ContactPeopleDelivery']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['delivery_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php if($stockorder['StockOrder']['type'] != 'sale'): ?>
				<div class="col-xs-6">
					<h4>Retour</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['StockOrder']['return']): ?>
								<strong class="uppercase">AVEC REPRISE <?php echo (!empty($stockorder['StockOrder']['return_mode'])) ? 'par ' . __(ucfirst($stockorder['StockOrder']['return_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong class="uppercase">RETOUR PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['StockOrder']['return_moment'])): ?>
							(<?php echo strtolower($moments[$stockorder['StockOrder']['return_moment']]); ?><?php if($stockorder['StockOrder']['return_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['StockOrder']['return_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['return_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['StockOrder']['return_place'])): ?>
							<?php echo $stockorder['StockOrder']['return_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['StockOrder']['return_address'])): ?>
							<?php echo $stockorder['StockOrder']['return_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['StockOrder']['return_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleReturn']['civility'])) ? $civilities[$stockorder['ContactPeopleReturn']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['StockOrder']['return_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['StockOrder']['return_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled offer">
						<li>
							<h4><?php echo __('Order'); ?> <?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?></h4>
						</li>
						<?php if($stockorder['StockOrder']['type'] != 'sale'): ?>
						<li>
							<strong>
							<?php if($stockorder['StockOrder']['service_date_begin'] == $stockorder['StockOrder']['service_date_end'] OR empty($stockorder['StockOrder']['service_date_end'])): ?>
							Date de prestation <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?>
							<?php else: ?>
							Date de prestation du <?php echo $this->Time->format($stockorder['StockOrder']['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['StockOrder']['service_date_end'], '%d.%m.%y'); ?>
							<?php endif; ?>
							</strong>
						</li>
						<?php endif; ?>
					</ul>
					<hr style="margin: 15px 0 0 0">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th style="width: 60%"><?php echo __('Stock item'); ?></th>
							<th class="text-right"><?php echo __('Ordered quantity'); ?></th>
							<th class="text-right"><?php echo __('Delivered quantity'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<tr>
								<td style="width: 2%"><?php echo $k + 1; ?></td>
								<td>
									<?php if($batch['stock_item_id'] == -1): ?>
									<?php echo $batch['name']; ?>
									<?php else: ?>
									<?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
									<?php endif; ?>
								</td>
								<td class="text-right"><?php echo $batch['quantity']; ?></td>
								<td class="text-right"><?php echo $batch['delivered_quantity']; ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
