<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<span class="uppercase font-blue-madison bold">
				<?php echo $stockorder['StockOrder']['order_number']; ?>
				<?php if(!empty($stockorder['StockOrder']['name'])): ?><br />
				<?php echo $stockorder['StockOrder']['name']; ?>
				<?php endif; ?>
			</span>
		</div>
		<div class="caption pull-right text-right">
			<span class="font-grey-gallery bold uppercase"><?php echo __(strtoupper($mode)); ?></span>
			<?php if(!empty($data['tour_id']) AND $data['tour_id'] != 9999): ?>
			<br />
			<span class="font-grey-gallery bold uppercase"><?php echo __('Tour') . ' ' . $data['tour_id']; ?></span>
			<?php endif; ?>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-4">
				<label class="control-label bold"><?php echo __('Client'); ?></label><br />
				<?php echo $stockorder['Client']['name']; ?><br />
				<?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
			</div>
			<div class="col-md-4">
				<label class="control-label bold"><?php echo __('Adresse'); ?></label><br />
				<?php echo $data['address']; ?><br />
				<?php echo $data['zip_city']; ?>
			</div>
			<div class="col-md-4">
				<label class="control-label bold"><?php echo __('Volume'); ?></label><br />
				<ul class="list-unstyled">
				<?php if(!empty($packaging['pallets'])): ?>
					<li><?php echo $packaging['pallets']; ?> <?php echo __('pallets') ?></li>
				<?php endif; ?>
				<?php if(!empty($packaging['pallets_xl'])): ?>
					<li><?php echo $packaging['pallets_xl']; ?> <?php echo __('pallets XL') ?></li>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis'])): ?>
					<li><?php echo $packaging['rollis']; ?> <?php echo __('rollis') ?></li>
				<?php endif; ?>
				<?php if(!empty($packaging['rollis_xl'])): ?>
					<li><?php echo $packaging['rollis_xl']; ?> <?php echo __('rollis XL') ?></li>
				<?php endif; ?>
				</ul>
			</div>
		</div>
		<div class="row hidden" style="margin-top: 20px">
			<div class="col-md-6">
				<label class="control-label bold"><?php echo __('Tour'); ?></label>
				<?php echo $this->Form->input('tour', array('class' => 'form-control bs-select', 'options' => array(1 => 1,2 => 2,3 => 3,4 => 4,5 => 5), 'empty' => true, 'label' => false, 'data-size' => 2, 'value' => empty($data['tour_id']) ? '' : $data['tour_id'] )); ?>
			</div>
		</div>
		<?php echo $this->Form->input('stock_order_id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
		<?php echo $this->Form->input('mode', array('type' => 'hidden', 'value' => $mode)); ?>
	</div>
</div>
