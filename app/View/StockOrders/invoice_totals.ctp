<div class="col-md-12">
  <h3>Totaux</h3>
</div>
<div class="col-md-6">
  <table class="table">
    <tbody>
      <tr>
        <td><?php echo __('Total HT');?></td>
        <td><?php echo $stockorder['StockOrder']['total_ht']; ?> CHF</td>
      </tr>
      <tr>
        <td><?php echo __('Delivery costs');?></td>
        <td><?php echo (!empty($stockorder['StockOrder']['forced_delivery_costs'])) ? $stockorder['StockOrder']['forced_delivery_costs'] : $stockorder['StockOrder']['delivery_costs']; ?> CHF</td>
      </tr>
      <tr>
        <td><?php echo __('Replacement costs');?></td>
        <td class="replacement_costs"><span><?php echo $stockorder['StockOrder']['replacement_costs']; ?></span> CHF</td>
      </tr>
      <tr>
        <td><?php echo __('Extra hours');?></td>
        <td class="extrahours_costs"><span><?php echo $stockorder['StockOrder']['extrahours_costs']; ?></span> CHF</td>
      </tr>
      <tr>
        <td><?php echo __('Fidelity discount');?></td>
        <td><?php echo $stockorder['StockOrder']['fidelity_discount_amount'];?> CHF</td>
      </tr>
      <tr>
        <td><?php echo __('Quantity discount');?></td>
        <td><?php echo $stockorder['StockOrder']['quantity_discount'];?> CHF</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="col-md-6">
  <table class="table">
    <tbody>
      <tr>
        <td><?php echo __('Subtotal HT');?></td>
        <td><?php echo $stockorder['StockOrder']['subtotal_ht'];?> CHF</td>
      </tr>
      <?php if (!empty($stockorder['StockOrder']['arrondi'])): ?>
      <tr>
        <td><?php echo __('Arrondi');?></td>
        <td><?php echo $stockorder['StockOrder']['arrondi'];?> CHF</td>
      </tr>
      <?php endif; ?>
      <tr>
        <td><?php echo __('TVA');?></td>
        <td class="tva"><span><?php echo $stockorder['StockOrder']['tva'];?></span> CHF</td>
      </tr>
      <tr>
        <td>
        <?php echo __('Paiement au dépôt');?>
        <?php if (!empty($stockorder['StockOrder']['payment_date']) && $stockorder['StockOrder']['payment_method'] != 'invoice'): ?>
        <br><small class="text-info">
        <?php echo __('Payment of %s CHF made on %s', $stockorder['StockOrder']['on_site_payment'], $this->Time->format('d.m.Y', $stockorder['StockOrder']['payment_date'])); ?>
        </small>
        <?php endif; ?>
        </td>
        <td class="on_site_payment"><?php echo $this->Form->input('StockOrder.on_site_payment', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline on_site_payment', 'value' => $stockorder['StockOrder']['on_site_payment'] == 0 ? 0 : $stockorder['StockOrder']['on_site_payment'], 'data-field' => 'on_site_payment', 'data-id' => $stockorder['StockOrder']['id']));?>
        <span class="help-inline">CHF</span></td>
      </tr>
      <tr>
        <td>
        <?php echo __('Deposit');?>
        </td>
        <td class="deposit"><?php echo $this->Form->input('StockOrder.deposit', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline deposit', 'value' => $stockorder['StockOrder']['deposit'] == 0 ? 0 : $stockorder['StockOrder']['deposit'], 'data-field' => 'deposit', 'data-id' => $stockorder['StockOrder']['id'], 'data-default' => $stockorder['StockOrder']['deposit']));?>
        <span class="help-inline">CHF</span></td>
      </tr>
      <tr>
        <td><strong><?php echo __('Net total (without deposit)');?></strong></td>
        <!-- <td class="net_total"><strong><span><?php echo $stockorder['StockOrder']['net_total'];?></span> CHF</strong></td> -->
        <td class="net_total"><?php echo $this->Form->input('StockOrder.forced_net_total', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline forced_net_total', 'value' => $stockorder['StockOrder']['forced_net_total'] == 0 ? $stockorder['StockOrder']['net_total'] : $stockorder['StockOrder']['forced_net_total'], 'data-field' => 'net_total', 'data-id' => $stockorder['StockOrder']['id']));?>
        <span class="help-inline">CHF</span>
        </td>
      </tr>
      <tr>
        <td><strong><?php echo __('Net total (with deposit)');?></strong></td>
        <td class="net_total"><?php echo $this->Form->input('StockOrder.net_total_with_deposit', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline net_total_with_deposit', 'value' => $stockorder['StockOrder']['net_total_with_deposit'], 'data-field' => 'net_total', 'data-id' => $stockorder['StockOrder']['id']));?>
        <span class="help-inline">CHF</span>
        </td>
      </tr>
      <tr class="<?php if (!empty($stockorder['StockOrder']['exported_invoiced_total']) && $stockorder['StockOrder']['invoiced_total'] != $stockorder['StockOrder']['exported_invoiced_total']): ?>danger<?php endif; ?>">
        <td>
        <strong><?php echo __('Invoiced total');?></strong>
        <?php if (!empty($stockorder['StockOrder']['exported_invoiced_total']) && $stockorder['StockOrder']['invoiced_total'] != $stockorder['StockOrder']['exported_invoiced_total']): ?>
        <br>
        <span class="text-danger">Montant facturé exporté: <strong><?php echo $stockorder['StockOrder']['exported_invoiced_total']; ?> CHF</strong></span>
        <?php endif; ?>
        </td>
        <td class="invoiced_total"><?php echo $this->Form->input('StockOrder.invoiced_total', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline invoiced_total', 'value' => $stockorder['StockOrder']['invoiced_total'], 'data-field' => 'invoiced_total', 'data-id' => $stockorder['StockOrder']['id']));?>
        <span class="help-inline">CHF</span>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div class="col-md-6">
  <h3>Statut d'exportation</h3>
  <?php echo $this->Form->input('StockOrder.export_status', array('label' => false, 'class' => 'form-control bs-select', 'value' => $stockorder['StockOrder']['export_status'], 'options' => Configure::read('StockOrders.export_status'))); ?>
  <br>
</div>
