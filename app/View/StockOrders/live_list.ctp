<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $fields = Configure::read('StockOrders.fields'); ?>
<?php $delivery_modes = Configure::read('StockOrders.delivery_modes'); ?>
<?php $return_modes = Configure::read('StockOrders.return_modes'); ?>
<?php if(!empty($cancellations)): ?>
	<?php foreach($cancellations as $stockorder): ?>
		<div class="col-md-12 order">
			<?php $portletClass = 'bg-yellow';
				$fontColor = 'font-grey-gallery';?>
			<div class="portlet light bordered <?php echo $portletClass; ?> <?php echo $stockorder['StockOrder']['status']; ?>" data-stock-order-id="<?php echo $stockorder['StockOrder']['id']; ?>">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject <?php echo $fontColor; ?> bold uppercase">		
							<span class="label" data-status="<?php echo $stockorder['StockOrder']['status']; ?>"></span>
							<span class="title" style="border-color: #555"><?php echo $stockorder['StockOrder']['order_number']; ?></span>
							<span class="title" style="border-color: #555"><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%a %d %B %Y'); ?></span>
							<span class="title" style="border-color: #555"><?php echo $stockorder['Client']['name']; ?></span>
						</span>
					</div>
					<div class="caption pull-right">
						<span class="caption-subject <?php echo $fontColor; ?> bold uppercase pull-right">		
							COMMANDE ANNULÉE
						</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
						<a class="btn btn-default pull-right notify-cancellation" data-stock-order-id="<?php echo $stockorder['StockOrder']['id']; ?>"><?php echo __('Confirm'); ?></a>
						<?php echo $this->Html->link('<i class="fa fa-list"></i> ' . __('View order detail'), array('controller' => 'stock_orders', 'action' => 'live', $stockorder['StockOrder']['id']), array('class' => 'btn btn-default', 'escape' => false)); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
<?php foreach($stockorders as $stockorder): ?>
	<div class="col-md-12 order">
		<?php if(!empty($stockorder['StockOrder']['notify'])){
			$portletClass = 'bg-red-pink';
			$fontColor = 'font-grey-cararra';
		} else {
			$portletClass = 'bg-inverse';
			$fontColor = 'font-red-intense';
			}?>
		<div class="portlet light bordered <?php echo $portletClass; ?> <?php echo $stockorder['StockOrder']['status']; ?>" data-stock-order-id="<?php echo $stockorder['StockOrder']['id']; ?>">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject <?php echo $fontColor; ?> bold uppercase">		
						<span class="label" data-status="<?php echo $stockorder['StockOrder']['status']; ?>"></span>
						<span class="title"><?php echo $stockorder['StockOrder']['order_number']; ?></span>
						<span class="title"><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%a %d %B %Y'); ?></span>
						<span class="title"><?php echo $stockorder['Client']['name']; ?></span>
						<span class="title">
							<?php echo $stockorder['StockOrder']['number_of_products']; ?>
							<?php echo ($stockorder['StockOrder']['number_of_products'] == 1) ? __('article') : __('articles'); ?> 
						</span>
						<span class="title"><?php echo implode(', ', $categories[$stockorder['StockOrder']['id']]); ?></span>
						<span class="title">
							<?php if($stockorder['StockOrder']['delivery_mode'] == 'client'): ?>
								CLIENT
							<?php elseif($stockorder['StockOrder']['delivery_mode'] == 'festiloc'): ?>
								FESTILOC
							<?php elseif($stockorder['StockOrder']['delivery_mode'] == 'transporter'): ?>
								TRANSPORTEUR
							<?php endif; ?>
						</span>
					</span>
				</div>
				<div class="tools" style="padding: 0">
					<select class="bs-select form-control input-medium" data-style="">
						<option value="confirmed"><?php echo __('Confirmed'); ?></option>
						<option value="in_progress"><?php echo __('In progress'); ?></option>
						<option value="processed"><?php echo __('Processed / Ready to deliver'); ?></option>
						<option value="delivered"><?php echo __('Delivered'); ?></option>
					</select>					
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-6">
						<?php if(!empty($stockorder['StockOrder']['notify'])): ?>
							<a class="btn red pulsate btn-sm" data-toggle="modal" href="#modal<?php echo $stockorder['StockOrder']['id']; ?>"><?php echo __('Show modifications'); ?></a>
							<br />
						<?php endif; ?>
						<div class="btn-group">
						<?php echo $this->Html->link('<i class="fa fa-list"></i> ' . __('View order detail'), array('controller' => 'stock_orders', 'action' => 'live', $stockorder['StockOrder']['id']), array('class' => 'btn btn-default btn-sm', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-check"></i> ' . __('Validate issue'), array('controller' => 'stock_orders', 'action' => 'live', $stockorder['StockOrder']['id'], 'issue'), array('class' => 'btn btn-default btn-sm', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-clipboard"></i> ' . __('Control return'), array('controller' => 'stock_orders', 'action' => 'live', $stockorder['StockOrder']['id'], 'return'), array('class' => 'btn btn-default btn-sm', 'escape' => false)); ?>
						</div>
					</div>
					<?php if(in_array($stockorder['StockOrder']['status'], array('confirmed', 'in_progress'))): ?>
					<div class="col-md-6 text-right">
						<a href="#" class="btn default btn-sm disabled">Imprimer <i class="fa fa-angle-double-right"></i> </a>
						<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Delivery note'), array('controller' => 'stock_orders', 'action' => 'invoice', 'ext' => 'pdf', $stockorder['StockOrder']['id'], '?' => 'download=0&showPrices=0&source=depot'), array('class' => 'btn btn-default btn-sm delivery_note', 'escape' => false, 'target' => '_blank')); ?>
						<?php
							if(
								!empty($stockorder['StockOrder']['number_of_pallets']) OR 
								!empty($stockorder['StockOrder']['number_of_pallets1']) OR 
								!empty($stockorder['StockOrder']['number_of_rollis']) OR 
								!empty($stockorder['StockOrder']['number_of_rollis1']) OR 
								!empty($stockorder['StockOrder']['xl_surcharge']) OR 
								!empty($stockorder['StockOrder']['xl_surcharge1'])
							): 
						?>
						<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Pallets/rollis sheets'), array('controller' => 'stock_orders', 'action' => 'pallet', 'ext' => 'pdf', $stockorder['StockOrder']['id'], '?' => 'download=0&source=depot'), array('class' => 'btn btn-default btn-sm print-pallet', 'escape' => false, 'target' => '_blank')); ?>
						<?php endif; ?>
						<?php if(!empty($stockorder['DeliveryTransporter'])): ?>
						<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Transporter sheets'), array('controller' => 'stock_orders', 'action' => 'transporter_pdf', 'ext' => 'pdf', $stockorder['StockOrder']['id'], '?' => 'download=0'), array('class' => 'btn btn-default btn-sm', 'escape' => false, 'target' => '_blank')); ?>
						<?php endif; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if(!empty($stockorder['StockOrder']['notify'])): ?>
	<div class="modal fade" id="modal<?php echo $stockorder['StockOrder']['id']; ?>" tabindex="-1" role="basic" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Modifications</h4>
				</div>
				<div class="modal-body">
					 <div class="row">
						<?php if(!empty($stockorder['StockOrderBatch']['old']) OR !empty($stockorder['StockOrderBatch']['actual'])): ?>
							<div class="col-md-12">
								<h3><?php echo __('Stock items'); ?></h3>
								<table class="table">
									<tr>
										<th><?php echo __('Status'); ?></th>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Name'); ?></th>
										<th><?php echo __('Quantity'); ?></th>
									</tr>
									<?php foreach($stockorder['StockOrderBatch']['old'] as $stockItemId => $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
                                            $key = '9999'.$batch['id'];
										} else {
											$key = $batch['stock_item_id'].$batch['id'];
										} ?>
										<?php if(!empty($stockorder['StockOrderBatch']['actual'][$key])): ?>
											<?php // it means the product is in first and last version of order ?>
											<?php if($stockorder['StockOrderBatch']['old'][$key]['quantity'] != $stockorder['StockOrderBatch']['actual'][$key]['quantity']): ?>
												<tr>
													<td><span class="badge bg-blue badge-roundless">modifié</span></td>
													<td class="font-blue"><?php echo $products[$key]['StockItem']['code']; ?></td>
													<td class="font-blue"><?php echo $products[$key]['StockItem']['name']; ?></td>
													<td class="font-blue">
														<strong>
															<?php echo $stockorder['StockOrderBatch']['actual'][$key]['quantity']; ?></strong>
															au lieu de <?php echo $stockorder['StockOrderBatch']['old'][$key]['quantity']; ?>														
													</td>
												</tr>
											<?php endif; ?>
										<?php endif; ?>
									<?php endforeach; ?>
									<?php foreach($stockorder['StockOrderBatch']['actual'] as $stockItemId => $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
                                            $key = '9999'.$batch['id'];
										} else {
											$key = $batch['stock_item_id'].$batch['id'];
										} ?>
										<?php if(empty($stockorder['StockOrderBatch']['old'][$key])): ?>
											<?php // it means the product has been added ?>
											<tr>
												<td><span class="badge bg-green badge-roundless">ajouté</span></td>
												<td class="font-green"><?php echo $products[$key]['StockItem']['code']; ?></td>
												<td class="font-green"><?php echo $products[$key]['StockItem']['name']; ?></td>
												<td class="font-green"><?php echo $stockorder['StockOrderBatch']['actual'][$key]['quantity']; ?></td>
											</tr>
										<?php endif; ?>
									<?php endforeach; ?>	
									<?php foreach($stockorder['StockOrderBatch']['old'] as $stockItemId => $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
                                            $key = '9999'.$batch['id'];
										} else {
											$key = $batch['stock_item_id'].$batch['id'];
										} ?>
										<?php if(empty($stockorder['StockOrderBatch']['actual'][$key])): ?>
											<?php // it means the product has been removed ?>
											<tr>
												<td><span class="badge bg-red badge-roundless">supprimé</span></td>
												<td class="text-muted"><?php echo $products[$key]['StockItem']['code']; ?></td>
												<td class="text-muted"><?php echo $products[$key]['StockItem']['name']; ?></td>
												<td class="text-muted"><?php echo $stockorder['StockOrderBatch']['old'][$key]['quantity']; ?></td>
											</tr>
										<?php endif; ?>
									<?php endforeach; ?>
								</table>
							</div>
						<?php endif; ?>
						<?php if(!empty($versions[$stockorder['StockOrder']['id']])): ?>
						<?php foreach($versions[$stockorder['StockOrder']['id']] as $model => $keys): ?>
							<div class="col-md-12"><h3><?php echo $model ?></h3></div>
							<div class="col-md-6">
						 		<table class="table">
						 		<tr>
						 			<th colspan="2">Ancienne version</th>
						 		</tr>
						 		<?php foreach($keys as $key => $fields): ?>
						 			<tr>
						 				<td><?php echo $key; ?></td>
						 				<td><?php echo $fields['old']; ?></td>
						 			</tr>
						 		<?php endforeach; ?>
						 		</table>
						 	</div>
						 	<div class="col-md-6">
						 		<table class="table">
						 		<tr>
						 			<th colspan="2">Nouvelle version</th>
						 		</tr>
						 		<?php foreach($keys as $key => $fields): ?>
						 			<tr>
						 				<td><?php echo $key; ?></td>
						 				<td><?php echo $fields['new']; ?></td>
						 			</tr>
						 		<?php endforeach; ?>
						 		</table>
						 	</div>
						<?php endforeach; ?>
						<?php endif; ?>
					 </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-times"></i> Fermer</button>
					<button type="button" class="btn green confirmModifications" data-stock-order-id="<?php echo $stockorder['StockOrder']['id'] ; ?>"><i class="fa fa-check"></i> J'ai pris connaissance des modifications</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<?php endif; ?>
<?php endforeach; ?>