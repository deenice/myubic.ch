<?php if($_GET['page'] == $_GET['topage']): ?>
<table class="table table-footer" style="font-size: 9px;">
	<tbody>
		<tr>
			<td style="border:0">
				<small class="uppercase">Règlement par e-banking</small>
			</td>
			<td style="border:0">
				<small class="uppercase">Versement pour</small><br />
				<small>Raiffeisen Fribourg-Ouest</small><br />
				<small>1752 Villars-sur-Glâne</small>
			</td>
			<td style="border:0">
				<small class="uppercase">En faveur de</small><br />
				<small>CH68 8015 9000 0158 3651 0</small><br />
				<small>Festiloc Sàrl</small><br />
				<small>Route du Petit-Moncor 1c</small><br />
				<small>CH-1752 Villars-sur-Glâne</small>
			</td>
			<td style="border:0">
				<small class="uppercase">N&deg; de compte: 158365.10</small><br />
				<small>N&deg; de clearing: 80159</small><br />
				<small>Swift: RAIFCH22</small><br /><br />
				<small>CHE-112.145.798 TVA</small>
			</td>
		</tr>
	</tbody>
</table>
<?php endif; ?>
