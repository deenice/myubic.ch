<?php $this->assign('page_title', __('Festiloc - Commandes')); ?>
<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $delivery_modes = Configure::read('StockOrders.delivery_modes'); ?>
<?php $return_modes = Configure::read('StockOrders.return_modes'); ?>
<?php $moments = Configure::read('StockOrders.delivery_moments'); ?>
<?php if(!empty($stockorder)): ?>
<div class="row" id="flashMessage">
<?php echo $this->Session->flash(); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Back to stock orders'), array('action' => 'live'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	</div>
	<div class="col-md-6 text-right">
		<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Print delivery note'), array('action' => 'invoice', $stockorder['StockOrder']['id'], 'ext' => 'pdf', '?' => 'download=0&showPrices=0&source=depot'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-red-intense bold uppercase"><?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?>&nbsp;&nbsp;&nbsp;
						<?php echo $stockorder['Client']['name']; ?> - <?php echo $stockorder['StockOrder']['name']; ?></span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<?php if(!empty($stockorder['StockOrder']['remarks'])): ?>
					<div class="col-md-12 margin-bottom-5">
						<div class="alert alert-warning">
							<i class="fa fa-warning"></i> <?php echo $stockorder['StockOrder']['remarks']; ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="col-md-6">
						<h4>Livraison</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Delivery date'); ?></strong></td>
								<td><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Delivery'); ?></strong></td>
								<td>
									<?php if($stockorder['StockOrder']['delivery']): ?>
									Avec livraison
									<?php else: ?>
										Sans livraison
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Moment'); ?></strong></td>
								<td><?php echo !empty($stockorder['StockOrder']['delivery_moment']) ? $moments[$stockorder['StockOrder']['delivery_moment']] : __('During day'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Address'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['delivery_address']; ?><br />
									<?php echo $stockorder['StockOrder']['delivery_zip']; ?>	
									<?php echo $stockorder['StockOrder']['delivery_city']; ?>		
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact person'); ?></strong></td>
								<td>
									<?php echo $stockorder['ContactPeopleDelivery']['name']; ?>	
									<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>	
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Remarks'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>	
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<h4>Retour</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Return date'); ?></strong></td>
								<td><?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %B %Y'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Return'); ?></strong></td>
								<td>
									<?php if($stockorder['StockOrder']['return']): ?>
									Avec livraison
									<?php else: ?>
										Sans livraison
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Moment'); ?></strong></td>
								<td><?php echo !empty($stockorder['StockOrder']['return_moment']) ? $moments[$stockorder['StockOrder']['return_moment']] : __('During day'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Return address'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['return_address']; ?><br />
									<?php echo $stockorder['StockOrder']['return_zip']; ?>	
									<?php echo $stockorder['StockOrder']['return_city']; ?>		
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h4>Client</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Client'); ?></strong></td>
								<td><?php echo $stockorder['Client']['name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact person'); ?></strong></td>
								<td><?php echo $stockorder['ContactPeopleClient']['name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Address'); ?></strong></td>
								<td>
									<?php echo $stockorder['Client']['address']; ?><br />
									<?php echo $stockorder['Client']['zip_city']; ?><br />
									<?php echo $stockorder['Client']['country']; ?>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6 margin-bottom-5">
						<h4>Conditionnement</h4>
						<table class="table">
							<tr>
								<td>Nombre de palettes</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_pallets1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_pallets1']) ? is_null($stockorder['StockOrder']['number_of_pallets']) ? 0 : $stockorder['StockOrder']['number_of_pallets'] : $stockorder['StockOrder']['number_of_pallets1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de palettes XL</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_pallets_xl1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_pallets_xl1']) ? is_null($stockorder['StockOrder']['number_of_pallets']) ? 0 : $stockorder['StockOrder']['number_of_pallets_xl'] : $stockorder['StockOrder']['number_of_pallets_xl1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de rollis</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_rollis1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_rollis1']) ? is_null($stockorder['StockOrder']['number_of_rollis']) ? 0 : $stockorder['StockOrder']['number_of_rollis'] : $stockorder['StockOrder']['number_of_rollis1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de rollis XL</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_rollis_xl1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_rollis_xl1']) ? is_null($stockorder['StockOrder']['number_of_rollis_xl']) ? 0 : $stockorder['StockOrder']['number_of_rollis_xl'] : $stockorder['StockOrder']['number_of_rollis_xl1'])); ?></td>
							</tr>
							<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
						</table>						
					</div>
					<div class="col-md-12">
						<h4>Produits</h4>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Coefficient'); ?></th>
										<th><?php echo __('Quantity'); ?></th>
										<th><?php echo __('Unit price'); ?></th>
										<th><?php echo __('Net HT'); ?></th>
										<th><?php echo __('Discount'); ?></th>
										<th><?php echo __('Total TTC'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
											$price = $batch['price'];
										} else {
											$price = $batch['StockItem']['price'];
										} ?>
										<tr>
											<td class="code">
												<?php echo empty($batch['StockItem']['code']) ? '' : $batch['StockItem']['code']; ?>
											</td>
											<td class="stockitem">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $batch['name']; ?>
												<?php else: ?>
												<?php echo $this->Html->link($batch['StockItem']['name'], array('#' => ''), array('data-toggle' => 'modal', 'data-target' => '#modal' . $batch['StockItem']['id'])); ?>
												<?php endif; ?>
											</td>
											<td class="coefficient"><?php echo $batch['coefficient']; ?></td>
											<td class="quantity"><?php echo $batch['quantity']; ?></td>
											<td class="price"><?php echo $price; ?></td>
											<td class="net_ht">
												<?php echo $net_ht = number_format((float)$batch['coefficient'] * $batch['quantity'] * $price, 2, '.', '');?>
											</td>
											<td class="discount">
												<?php echo empty($batch['discount']) ? 0 : $batch['discount']; ?> %
											</td>
											<td class="total_ttc">
												<?php echo number_format($net_ht - ($net_ht * $batch['discount']), 2, '.', '');?>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
<div class="modal fade" id="modal<?php echo $batch['StockItem']['id']; ?>" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<img src="<?php echo $this->webroot . $batch['StockItem']['Document'][0]['url']; ?>" class="img-responsive">	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?>
<?php else: ?>
<div id="filters">
	<div class="row">
		<div class="col-md-6">
			<div class="row">
				<div class="col-md-12 margin-bottom-5">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">Afficher les commandes de </label>
						</div>
						<div class="col-md-8">
							<?php echo $this->Form->input('from', array('type' => 'text', 'class' => 'form-control date-picker input-inline from refreshStockOrders input-small', 'label' => false, 'div' => false, 'value' => date('d-m-Y'), 'data-default' => date('d-m-Y'))); ?>
							<span class="help-text"> à </span>
							<?php echo $this->Form->input('to', array('type' => 'text', 'class' => 'form-control date-picker input-inline to refreshStockOrders input-small', 'label' => false, 'div' => false, 'value' => date('d-m-Y', strtotime(date('Y-m-d') . "+5 weekday")), 'data-default' => date('d-m-Y', strtotime(date('Y-m-d') . "+5 weekday")))); ?>
						</div>
					</div>						
				</div>
				<div class="col-md-12 margin-bottom-5">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">Filtrer par statut</label>
						</div>
						<div class="col-md-8">
							<?php echo $this->Form->input('status', array('class' => 'form-control refreshStockOrders bs-select input-large', 'label' => false, 'div' => false, 'options' => Configure::read('StockOrders.status'), 'multiple' => true)); ?>
						</div>
					</div>					
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4">
							<label class="control-label">Afficher uniquement les</label>
						</div>
						<div class="col-md-8">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn default">
								<input type="radio" class="toggle" name="type" value="delivery"> Commandes à préparer </label>
								<label class="btn default">
								<input type="radio" class="toggle" name="type" value="return"> Retours de marchandise </label>
								<input type="hidden" id="dateType" value="delivery">
							</div>
						</div>
					</div>							
				</div>
			</div>
		</div>
		<div class="col-md-6 text-right">
			<a class="btn btn-primary refresh btn-sm"><i class="fa fa-refresh"></i> <?php echo __('Refresh'); ?></a>
			<a class="btn btn-danger clear btn-sm"><i class="fa fa-times"></i> <?php echo __('Clear filters'); ?></a><br /><br />
			<p class="text-muted"><i class="fa fa-info-circle"></i> La page se rafraichit toutes les 5 minutes.</p>
			<br>
			<?php echo $this->Form->input('filter', array('class' => 'form-control filter input-medium pull-right','placeholder' => 'Nom/numéro de commande', 'label' => false, 'div' => false)); ?>
		</div>
	</div>
	<br />	
</div>
<div id="orders">
	<div class="row" id="flashMessage" style="padding-top: 30px">
	<?php echo $this->Session->flash(); ?>
	</div>
	<div class="row title">
		<div class="col-md-12">
			<h3 style="display:none"><span></span> commande(s)</h3>
		</div>
	</div>
	<div class="row" id="stockorders">
	</div>
</div>
<?php endif; ?>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/jquery.pulsate.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.festiloc();';
$this->end();
?>