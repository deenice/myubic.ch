<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Stock orders')); ?>
<?php $statuses = Configure::read('StockOrders.status'); ?>
<div class="tabbable-custom tabbable-tabdrop" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#all" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('All'); ?></a>
		</li>
		<li>
			<a href="#tocheck" data-toggle="tab"><i class="fa fa-warning"></i> <?php echo __('To check'); ?></a>
		</li>
		<li>
			<a href="#offer" data-toggle="tab"><i class="fa fa-tag"></i> <?php echo __('Offers'); ?></a>
		</li>
		<li>
			<a href="#confirmed" data-toggle="tab"><i class="fa fa-check"></i> <?php echo __('Order confirmations'); ?></a>
		</li>
		<li>
			<a href="#inprogress" data-toggle="tab"><i class="fa fa-spinner"></i> <?php echo __('In progress'); ?></a>
		</li>
		<li>
			<a href="#processed" data-toggle="tab"><i class="fa fa-truck"></i> <?php echo __('Processed / Ready to deliver'); ?></a>
		</li>
		<li>
			<a href="#delivered" data-toggle="tab"><i class="fa fa-thumbs-up"></i> <?php echo __('Delivered'); ?></a>
		</li>
		<li>
			<a href="#invoices" data-toggle="tab"><i class="fa fa-money"></i> <?php echo __('To invoice'); ?></a>
		</li>
		<li>
			<a href="#invoiced" data-toggle="tab"><i class="fa fa-money"></i> <?php echo __('Invoiced'); ?></a>
		</li>
		<li>
			<a href="#cancelled" data-toggle="tab"><i class="fa fa-times"></i> <?php echo __('Cancelled'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="all">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of all orders'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('All'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tocheck">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of orders to check'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('ToCheck'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="offer">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of all offers'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Offer'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="confirmed">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of all confirmed offers'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Confirmed'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="inprogress">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('In progress orders'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('InProgress'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="processed">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of all processed orders'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Processed'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="delivered">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-shopping-cart"></i><?php echo __('List of all delivered orders'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Delivered'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="invoices">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-money"></i><?php echo __('To invoice'); ?>
					</div>
					<div class="actions">
						<?php if(!empty($ordersToExport)): ?>
							<?php echo $this->Html->link(__('Export the %s invoices in Revelate', $this->Html->tag('strong', $numberOfOrdersToExport)) . ' <i class="fa fa-download"></i>', array('controller' => 'festiloc', 'action' => 'revelate'), array('class' => 'btn btn-sm btn-primary', 'escape' => false)); ?>
						<?php endif; ?>
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('ToInvoice'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="invoiced">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-money"></i><?php echo __('Invoiced'); ?>
					</div>
					<div class="actions">
						<?php if(!empty($ordersToExport)): ?>
							<?php echo $this->Html->link(__('Export the %s invoices in Revelate', $this->Html->tag('strong', $numberOfOrdersToExport)) . ' <i class="fa fa-download"></i>', array('controller' => 'festiloc', 'action' => 'revelate'), array('class' => 'btn btn-sm btn-primary', 'escape' => false)); ?>
						<?php endif; ?>
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Invoiced'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="cancelled">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-times"></i><?php echo __('Cancelled'); ?>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(__('Add an order') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_orders', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Cancelled'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockorders();';
echo 'Custom.stockitems();';
$this->end();
?>
