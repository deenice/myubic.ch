<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $delivery_modes = Configure::read('StockOrders.delivery_modes'); ?>
<?php $return_modes = Configure::read('StockOrders.return_modes'); ?>
<?php $moments = Configure::read('StockOrders.delivery_moments'); ?>
<div class="row" id="flashMessage">
<?php echo $this->Session->flash(); ?>
</div>
<?php if(!empty($stockorder)): ?>
<div class="row hidden-print">
	<div class="col-md-6">
		<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Back to stock orders'), array('action' => 'live'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-red-intense bold uppercase"><?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?>&nbsp;&nbsp;&nbsp;
						<?php echo $stockorder['Client']['name']; ?> - <?php echo $stockorder['StockOrder']['name']; ?></span>
				</div>
			</div>
			<div class="portlet-body">		
				<?php echo $this->Form->create('StockOrder', array('action' => 'save')); ?>
				<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
				<div class="row">
					<?php if(!empty($stockorder['StockOrder']['number_of_pallets']) OR !empty($stockorder['StockOrder']['number_of_rollis'])): ?>
					<div class="col-md-12 margin-bottom-5">
						<h4>Conditionnement</h4>
						<?php if(!empty($stockorder['StockOrder']['number_of_pallets'])): ?>
							Nombre de palettes : <?php echo $stockorder['StockOrder']['number_of_pallets']; ?><br />
						<?php endif; ?>
						<?php if(!empty($stockorder['StockOrder']['number_of_rollis'])): ?>
							Nombre de rollis : <?php echo $stockorder['StockOrder']['number_of_rollis']; ?>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					<div class="col-md-12">
						<h4>Produits</h4>
						<div class="table-responsive">
							<table class="table" id="">
								<thead>
									<tr>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Ordered quantity'); ?></th>
										<th><?php echo __('Delivered quantity'); ?></th>
										<th><?php echo __('Remarks'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
										<tr id="batch<?php echo $batch['id']; ?>" class="batch">
											<td class="code"><?php echo !empty($batch['StockItem']['code']) ? $batch['StockItem']['code'] : ''; ?></td>
											<td class="stockitem">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $batch['name']; ?>
												<?php else: ?>
												<?php echo $batch['StockItem']['name']; ?>
												<?php echo $this->Form->input('StockOrderBatch.name.', array('type' => 'hidden', 'value' => '')); ?>
												<?php endif; ?>
											</td>
											<td class="quantity"><?php echo $batch['quantity']; ?></td>
											<td class="quantity"><?php echo $this->Form->input('StockOrder.delivered_quantity.', array('class' => 'form-control input-xs input-inline input-xsmall delivered_quantity', 'label' => false, 'div' => false, 'type' => 'number', 'value' => $batch['delivered_quantity'] != $batch['quantity'] ? $batch['delivered_quantity']  : $batch['quantity'], 'required' => false)); ?></td>
											<td><?php echo $this->Form->input('StockOrder.delivery_remarks.', array('class' => 'form-control input-large', 'label' => false, 'div' => false, 'required' => false, 'value' => $batch['delivery_remarks'])); ?></td>
											<?php if($batch['stock_item_id'] != -1): ?>
											<?php echo $this->Form->input('StockOrder.stock_item_id.', array('type' => 'hidden', 'value' => $batch['StockItem']['id'], 'class' => 'stockItemId')); ?>
											<?php endif; ?>
											<?php echo $this->Form->input('StockOrder.stock_order_batch_id.', array('type' => 'hidden', 'value' => $batch['id'])); ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="form-group hidden-print">
							<h4><?php echo __('Person in charge'); ?></h4>
							<?php echo $this->Form->input('StockOrder.resp_id', array('class' => 'form-control select2me', 'label' => false, 'div' => false, 'options' => $collaborators, 'empty' => true)); ?>
						</div>						
					</div>
					<div class="col-md-12 hidden-print">
					<br />
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and continue editing'), array('class' => 'btn btn-primary', 'escape' => false, 'type' => 'submit', 'value' => 'issue', 'name' => 'data[destination]')); ?>
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and exit'), array('class' => 'btn btn-warning save-issue', 'escape' => false, 'type' => 'submit', 'value' => 'live', 'name' => 'data[destination]')); ?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/dropzone/css/dropzone.css');
echo $this->Html->css('/metronic/global/plugins/fancybox/source/jquery.fancybox.css');
echo $this->Html->css('/metronic/pages/css/portfolio.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
echo $this->Html->script('/metronic/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.init();';
echo 'Custom.festiloc();';
$this->end();
?>