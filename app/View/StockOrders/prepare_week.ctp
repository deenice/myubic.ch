<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Week planification')); ?>
<div class="tabbable-custom" id="week_tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#calendar1" data-toggle="tab" class="fullcalendar"><i class="icon-calendar"></i> <?php echo __('Calendar'); ?></a>
		</li>
		<li>
			<a href="#map2" data-toggle="tab" class="gmap"><i class="icon-map"></i> <?php echo __('Map'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="calendar1">
			<div class="portlet light calendar">
				<div class="portlet-title">
					<div class="actions pull-left">
					<?php foreach($transporters as $transporter): ?>
					<span class="badge badge-roundless" style="background-color: <?php echo $transporter['Transporter']['color']; ?>">&nbsp;&nbsp;</span> <?php echo $transporter['Transporter']['name']; ?>&nbsp;&nbsp;&nbsp;
					<?php endforeach; ?>
					<span class="badge badge-roundless" style="background-color: #999">&nbsp;&nbsp;</span> Non défini &nbsp;&nbsp;&nbsp;
					<span class="client-badge hidden">
						<span class="badge badge-roundless" style="background-color: #29780F">&nbsp;&nbsp;</span> Client &nbsp;&nbsp;&nbsp;
					</span>
					<br />
					<br />
					<a href="#" class="btn default notifyTransporters hidden"><i class="fa fa-envelope"></i> Notifier les transporteurs</a>
					<label class="checkbox-inline" style="padding-left: 0">
          	<input type="checkbox" id="displayClients" value="1"> <?php echo __('Display orders of clients'); ?>
					</label>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div id="calendar" class="preparationWeekCalendar"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="map2">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold font-blue-madison"></span>
					</div>
				</div>
				<div class="portlet-body">
					<div id="map1" style="overflow: hidden">
						<div class="row margin-bottom-5">
							<!-- <div class="col-md-12 text-right">
								<img src="<?php echo $this->webroot; ?>img/icons/marker-festiloc.png" style="width: 20px"> Festiloc &nbsp;&nbsp;&nbsp;
								<img src="<?php echo $this->webroot; ?>img/icons/marker-client.png" style="width: 20px"> Client &nbsp;&nbsp;&nbsp;
								<img src="<?php echo $this->webroot; ?>img/icons/marker-logista.png" style="width: 20px"> Logista &nbsp;&nbsp;&nbsp;
								<img src="<?php echo $this->webroot; ?>img/icons/marker-zumwald.png" style="width: 20px"> Zumwald
							</div> -->
						</div>
						<div id="gmap1" class="gmaps" style="height: 600px"></div>
					</div>
					<div class="origin hidden" data-lat="46.817129" data-lng="7.127021" data-title="Festiloc"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="context">
	<ul class="dropdown-menu" role="menu" style="height: 400px; overflow-y: scroll">
		<li class="disabled"><a href=""><strong><?php echo __('Assign transporter'); ?></strong></a></li>
		<?php foreach($transporters as $transporter): ?>
		<li class="transporter">
			<a href="javascript:;" data-stock-order-id="" data-transporter-id="<?php echo $transporter['Transporter']['id']; ?>">
				<span class="label" style="background-color: <?php echo $transporter['Transporter']['color']; ?>">&nbsp;&nbsp;&nbsp;</span>
				 <?php echo $transporter['Transporter']['name']; ?>
			</a>
		</li>
		<?php endforeach; ?>
		<li class="divider"></li>
		<li class="disabled"><a href="#" class="transporter"><strong><?php echo __('Notify transporter'); ?></strong></a></li>
		<li class=""><a href="#" class="transporter transporter_pdf"><?php echo __('Transporter sheets'); ?></a></li>
		<li class=""><a href="#" class="transporter transporter_manually_notify"><?php echo __('Informed transporter'); ?></a></li>
		<li class=""><a href="#" class="transporter transporter_notify"><?php echo __('Automatic by email'); ?></a></li>
		<li class="divider tour"></li>
		<li class="disabled tour"><a href=""><strong>Attribuer une tournée</strong></a></li>
		<li class="tour"><a href="#" data-tour-id="1">Tournée 1</a></li>
		<li class="tour"><a href="#" data-tour-id="2">Tournée 2</a></li>
		<li class="tour"><a href="#" data-tour-id="3">Tournée 3</a></li>
		<li class="tour"><a href="#" data-tour-id="4">Tournée 4</a></li>
		<li class="tour"><a href="#" data-tour-id="5">Tournée 5</a></li>
		<li class="tour"><a href="#" data-tour-id="6">Tournée 6</a></li>
		<li class="tour"><a href="#" data-tour-id="7">Tournée 7</a></li>
		<li class="tour"><a href="#" data-tour-id="8">Tournée 8</a></li>
		<li class="tour"><a href="#" data-tour-id="9">Tournée 9</a></li>
		<li class="tour"><a href="#" data-tour-id="10">Tournée 10</a></li>
		<li class="tour"><a href="#" data-tour-id="11">Tournée 11</a></li>
		<li class="tour"><a href="#" data-tour-id="12">Tournée 12</a></li>
		<li class="tour"><a href="#" data-tour-id="0"><strong>Retirer de la tournée</strong></a></li>
	</ul>
</div>
<div class="modal fade" id="hours">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Spécifier les heures de réservation</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"><?php echo __('Start'); ?></label>
					<?php echo $this->Form->input('start', array('class' => 'form-control timepicker timepicker-24 input-small input-inline', 'label' => false, 'value' => '08:00')); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('Start place'); ?></label>
					<?php echo $this->Form->input('start_place', array('class' => 'form-control', 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('End'); ?></label>
					<?php echo $this->Form->input('end', array('class' => 'form-control timepicker timepicker-24 input-small input-inline', 'label' => false, 'value' => '18:00')); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('End place'); ?></label>
					<?php echo $this->Form->input('end_place', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				<button type="button" class="btn btn-primary save-reservation">Sauvegarder</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="stockorder">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Spécifier les heures de réservation</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"><?php echo __('Start'); ?></label>
					<?php echo $this->Form->input('start', array('class' => 'form-control timepicker timepicker-24 input-small input-inline', 'label' => false, 'value' => '08:00')); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('Start place'); ?></label>
					<?php echo $this->Form->input('start_place', array('class' => 'form-control', 'label' => false)); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('End'); ?></label>
					<?php echo $this->Form->input('end', array('class' => 'form-control timepicker timepicker-24 input-small input-inline', 'label' => false, 'value' => '18:00')); ?>
				</div>
				<div class="form-group">
					<label class="control-label"><?php echo __('End place'); ?></label>
					<?php echo $this->Form->input('end_place', array('class' => 'form-control', 'label' => false)); ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				<button type="button" class="btn btn-primary save-reservation">Sauvegarder</button>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('page_level_plugins');
echo $this->Html->script('/js/google-maps-cluster/markerclusterer_compiled.js');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&language=fr&key=' . Configure::read('GoogleMapsAPIKey'));
//echo $this->Html->script('/metronic/theme/assets/global/plugins/gmaps/gmaps.min.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.stockorders();';
$this->end();
?>
