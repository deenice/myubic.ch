<?php $this->assign('page_title', __('Competences')); ?>
<?php $this->assign('page_subtitle', __('Evaluation of competences')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-graduation-cap"></i><?php echo __('Evaluation of competences'); ?>
		</div>
		<div class="tools">
			<?php echo __('Filter'); ?>:&nbsp;&nbsp;
			<span class="btn btn-xs default" data-company="all"> <?php echo __('All'); ?> </span>
			<span class="btn btn-xs btn-company btn-company-ubic" data-company="ubic"></span>
			<span class="btn btn-xs btn-company btn-company-ug" data-company="ug"></span>
			<span class="btn btn-xs btn-company btn-company-eg" data-company="eg"></span>
			<span class="btn btn-xs btn-company btn-company-mg" data-company="mg"></span>
			<span class="btn btn-xs btn-company btn-company-fl" data-company="fl"></span>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-condensed table-hierarchies">
			<thead>
				<tr>
					<th class="company"></th>
					<th class="date"><?php echo __('Date'); ?></th>
					<th class="extra"><?php echo __('Collaborator'); ?></th>
					<th class="job"><?php echo __('Job'); ?></th>
					<th class="event"><?php echo __('Event'); ?></th>
					<th class="client"><?php echo __('Client'); ?></th>
					<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($extras as $extra): ?>
					<?php if(!$extra['j']['hierarchy'] && $extra['j']['sector'] == 'animation'){continue;} ?>
					<tr data-company="<?php echo $extra['co']['class']; ?>">
						<td><span class="btn btn-xs btn-company btn-company-<?php echo $extra['co']['class']; ?>"></span></td>
						<td><?php echo $this->Time->format('d.m.Y', $extra['e']['confirmed_date']); ?></td>
						<td><?php echo $this->Html->link(sprintf('%s %s', $extra['u']['first_name'], $extra['u']['last_name']), array('controller' => 'users', 'action' => 'modal', $extra['u']['id']), array('data-toggle' => 'modal', 'data-target' => '#modal-user-ajax')); ?></td>
						<td><?php echo $extra['j']['name']; ?></td>
						<td><?php echo $extra['e']['code']; ?> <?php echo $this->Text->truncate($extra['e']['name'], 35); ?></td>
						<td><?php echo $this->Text->truncate($extra['c']['name'], 35); ?></td>
						<td class="text-right">
							<?php if(in_array($extra['co']['class'], array('ubic', 'ug', 'fl'))): ?>
								<div class="btn-group btn-group-xs btn-group-solid">
									<?php echo $this->Html->link(
										'<i class="fa fa-angle-double-up"></i> ' . $hierarchies[$extra['j']['hierarchy']+1],
										array(
											'controller' => 'competences',
											'action' => 'evaluate',
											$extra['j']['job'],
											$extra['JobUser']['id'],
											$extra['j']['sector'],
											$extra['j']['activity_id'],
											1
										),
										array('escape' => false, 'class' => 'btn green', 'data-evaluate-competence' => true)
									); ?>
									<?php echo $this->Html->link(
										'<i class="fa fa-angle-double-right"></i> ' . $hierarchies[$extra['j']['hierarchy']],
										array(
											'controller' => 'competences',
											'action' => 'evaluate',
											$extra['j']['job'],
											$extra['JobUser']['id'],
											$extra['j']['sector'],
											$extra['j']['activity_id'],
											0
										),
										array('escape' => false, 'class' => 'btn grey-silver', 'data-evaluate-competence' => true)
									); ?>
								</div>
							<?php elseif(in_array($extra['co']['class'], array('eg', 'mg'))): ?>
								<?php if($extra['j']['job'] == 'test') $job = 'service_crew'; ?>
								<?php if($extra['j']['job'] == 'service_crew') $job = 'event_assistant'; ?>
								<?php if($extra['j']['job'] == 'event_assistant') $job = 'event_manager'; ?>
								<?php if($extra['j']['job'] == 'bar_crew') $job = 'bar_manager'; ?>
								<div class="btn-group btn-group-xs btn-group-solid">
									<?php echo $this->Html->link(
										'<i class="fa fa-angle-double-up"></i> ' . $jobs[$job],
										array(
											'controller' => 'competences',
											'action' => 'evaluate',
											$extra['j']['job'],
											$extra['JobUser']['id'],
											$extra['j']['sector'],
											$extra['j']['fb_module_id'],
											1
										),
										array('escape' => false, 'class' => 'btn green', 'data-evaluate-competence' => true)
									); ?>
									<?php echo $this->Html->link(
										'<i class="fa fa-angle-double-right"></i> ' . $jobs[$extra['j']['job']],
										array(
											'controller' => 'competences',
											'action' => 'evaluate',
											$extra['j']['job'],
											$extra['JobUser']['id'],
											$extra['j']['sector'],
											$extra['j']['fb_module_id'],
											0
										),
										array('escape' => false, 'class' => 'btn grey-silver', 'data-evaluate-competence' => true)
									); ?>
								</div>
							<?php endif; ?>
							<span class="feedback">
								<span class="loading"><i class="fa fa-spin fa-spinner"></i></span>
								<span class="success"><i class="fa fa-check text-success"></i></span>
								<span class="error"><i class="fa fa-times text-danger"></i></span>
							</span>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.competences();';
$this->end();
?>
