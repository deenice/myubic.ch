<?php $this->assign('page_title', $this->Html->link(__('Vehicle models'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $this->request->data['VehicleCompanyModel']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['VehicleCompanyModel']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">

		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('VehicleCompanyModel', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies, 'value' => !empty($vehicle_company_id) ? $vehicle_company_id : ''));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Family'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('family', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('VehiclesTrailers.families')));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('type', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Vehicles.types')));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Dimensions'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('dimensions', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Surface'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('surface', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
					<span class="help-text">m2</span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Maximum weight'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('max_weight', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
					<span class="help-text">kg</span>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Number of pallets'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('number_of_pallets', array('type' => 'number', 'label' => false, 'class' => 'form-control input-small'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Number of rollis'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('number_of_rollis', array('type' => 'number', 'label' => false, 'class' => 'form-control input-small'));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
