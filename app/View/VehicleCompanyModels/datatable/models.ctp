<?php

$types = Configure::read('Vehicles.types');
$families = Configure::read('Vehicles.families');

foreach ($dtResults as $result) {


	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'vehicle_company_models', 'action' => 'edit', $result['VehicleCompanyModel']['id']),
	    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
	);

    if(empty($result['VehicleCompanyModel']['family'])){
        $family = '';
    } else {
        $family = $families[$result['VehicleCompanyModel']['family']];
    }

    $this->dtResponse['aaData'][] = array(
        $result['VehicleCompanyModel']['name'],
        $family,
        $result['VehicleCompany']['name'],
        $types[$result['VehicleCompanyModel']['type']],
        $actions
    );
}
