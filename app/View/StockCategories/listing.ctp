<?php $i = 1; ?>
<table>
	<tbody>
		<!-- première ligne, nom des colonnes -->
		<tr>
		<!-- première colonne th -->
			<th><?php echo $i; ?></th>
			<td>online</td>
			<td>codecatégorie</td>
			<td>catégorie</td>
			<td>codesection</td>
			<td>section2chiffres</td>
			<td>codesoussection</td>
			<td>soussection2chiffres</td>
			<td>nouveaunoproduit</td>
			<td>nouveaunomarticle</td>
			<td>nomarticle</td>
			<td>prixlocation</td>
			<td>qtédisponible</td>
			<td>images</td>
		</tr>
		<?php foreach($data as $item): $i++; ?>
			<tr>
				<th><?php echo $i; ?></th>
				<td><?php echo $item['online']; ?></td>
				<td><?php echo $item['codecatégorie']; ?></td>
				<td><?php echo $item['catégorie']; ?></td>
				<td><?php echo $item['codesection']; ?></td>
				<td><?php echo $item['section2chiffres']; ?></td>
				<td><?php echo $item['codesoussection']; ?></td>
				<td><?php echo $item['soussection2chiffres']; ?></td>
				<td><?php echo $item['nouveaunoproduit']; ?></td>
				<td><?php echo $item['nouveaunomarticle']; ?></td>
				<td><?php echo $item['nomarticle']; ?></td>
				<td><?php echo $item['prixlocation']; ?></td>
				<td><?php echo $item['qtédisponible']; ?></td>
				<td><?php echo (!empty($item['images'])) ? implode(",",$item['images']) : ''; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>