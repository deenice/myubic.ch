<?php $this->assign('page_title', $this->Html->link(__('Checklists'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $checklist['Checklist']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-list font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $checklist['Checklist']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<div class="row">
			<?php echo $this->Form->create('Checklist', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('Checklist.id'); ?>
			<div class="col-md-12">
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies)); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Ideal for'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('model', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Checklists.models'))); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Default list'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('default', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
							<span class="help-block">Si coché, la liste sera disponible pour tous les utilisateurs de myubic.</span>
						</div>
					</div>
					<?php if($checklist['Checklist']['personal']): ?>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Personal list'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('personal', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
							<span class="help-block">Si coché, la liste sera disponible uniquement pour son créateur.</span>
						</div>
					</div>
					<?php endif; ?>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-body">
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Groups / Tasks'); ?></label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12" id="checklist-batches-tasks" data-href="<?php echo Router::url(array('controller' => 'checklists', 'action' => 'batches_tasks', $checklist['Checklist']['id'])); ?>">
									En chargement...
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						</div>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
		<!-- END FORM-->
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.checklist();';
$this->end();
?>
