<?php $this->assign('page_title', __('Checklists')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-tasks"></i><?php echo __('List of all checklists'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a checklist'), array('controller' => 'checklists', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('All'); ?>
	</div>
</div>
