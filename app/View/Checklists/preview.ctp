<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo __('Checklists'); ?></h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-md-3">
			<ul class="nav nav-tabs tabs-left">
				<?php foreach($checklists as $k => $checklist): ?>
        <li class="<?php echo $k==0 ? 'active':''; ?>">
          <a href="#checklist<?php echo $checklist['Checklist']['id']; ?>" data-toggle="tab">
						<?php if($checklist['Checklist']['default']): ?>
							<i class="fa fa-star font-yellow"></i>
						<?php else: ?>
							<i class="fa fa-user"></i>
						<?php endif; ?>
						<?php echo $checklist['Checklist']['name']; ?>
						<span class="btn btn-xs btn-company btn-company-<?php echo $checklist['Company']['class']; ?> pull-right"></span>
					</a>
        </li>
				<?php endforeach; ?>
      </ul>
		</div>
		<div class="col-md-9">
			<div class="tab-content">
				<?php foreach($checklists as $k => $checklist): ?>
					<div class="tab-pane<?php echo $k==0 ? ' active':''; ?>" id="checklist<?php echo $checklist['Checklist']['id']; ?>">
						<div class="panel-group accordion" id="accordiontasks<?php echo $checklist['Checklist']['id']; ?>">
					    <?php foreach($checklist['ChecklistBatch'] as $batch): //debug($batch);?>
					      <div class="panel panel-default">
					        <div class="panel-heading">
					          <h4 class="panel-title">
					            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordiontasks<?php echo $checklist['Checklist']['id']; ?>" href="#accordionbatch_<?php echo $batch['id']; ?>">
					              <?php echo $batch['name']; ?>
					            </a>
					          </h4>
					        </div>
					        <div id="accordionbatch_<?php echo $batch['id']; ?>" class="panel-collapse collapse">
					          <div class="panel-body tasks-widget">
					            <ul class="task-list">
					              <?php foreach($batch['ChecklistTask'] as $task): ?>
					                <li data-checklist-task-id="<?php echo $task['id']; ?>">
					                  <div class="task-checkbox">
					                    <?php echo $this->Form->input('task', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => $task['done'], 'checked' => $task['done'])); ?>
					                  </div>
					                  <div class="task-title">
					                    <span class="task-title-sp"> <?php echo $task['name']; ?> </span>
					                    <?php if(!empty($task['due_date'])): ?>
					                    <span class="pull-right">
					                      <?php if($task['due_date'] < date('Y-m-d')): ?>
					                        <i class="fa fa-warning text-warning"></i>
					                      <?php endif; ?>
					                      <?php echo $this->Time->format('d.m.Y', $task['due_date']); ?>
					                    </span>
					                    <?php endif; ?>
					                  </div>
					                </li>
					              <?php endforeach; ?>
					            </ul>
					          </div>
					        </div>
					      </div>
					    <?php endforeach; ?>
					  </div>
						<a href="#" class="btn btn-success insert-checklist" data-checklist-id="<?php echo $checklist['Checklist']['id']; ?>"><?php echo __('Insert this checklist'); ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
