<?php if(!empty($checklist['ChecklistBatch'])): ?>
<div class="panel-group accordion" id="accordiontasks">
	<?php foreach($checklist['ChecklistBatch'] as $batch): ?>
	<div class="panel panel-default" data-checklist-batch-id="<?php echo $batch['id']; ?>">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a class="accordion-toggle accordion-toggle-styled <?php echo $batch['id'] == $last_open_batch ? '':'collapsed'; ?>" data-toggle="collapse" data-parent="#accordiontasks" href="#accordionphase<?php echo $batch['id']; ?>">
					<?php echo $batch['name']; ?>
					<span class="badge pull-right" style="margin-right: 10px"><?php echo sizeof($batch['ChecklistTask']); ?></span>
				</a>
			</h4>
		</div>
		<div id="accordionphase<?php echo $batch['id']; ?>" class="panel-collapse <?php echo $batch['id'] == $last_open_batch ? 'in':'collapse'; ?>">
			<div class="panel-body tasks-widget">
				<ul class="task-list">
					<li class="batch-name">
						<div class="task-checkbox" style="visibility: hidden"><input type="checkbox" name="name" value=""></div>
						<div class="task-title">
							<span class="task-title-sp">
								<strong class="editable" data-pk="<?php echo $batch['id']; ?>" data-value="<?php echo $batch['name']; ?>" data-name="name"><i class="fa fa-rename"></i></strong>
							</span>
						</div>
					</li>
					<?php if(!empty($batch['ChecklistTask'])): ?>
						<?php foreach($batch['ChecklistTask'] as $task): ?>
						<li class="sortable" data-checklist-task-id="<?php echo $task['id']; ?>" data-toggle="context" data-target="#task-actions<?php echo $task['id']; ?>">
							<div class="task-checkbox">
								<input type="checkbox" name="name" value="">
							</div>
							<div class="task-title">
								<span class="task-title-sp">
									<span class="editable" data-pk="<?php echo $task['id']; ?>" data-name="name"><?php echo $task['name']; ?></span>
									<?php echo $this->Html->link('<i class="fa fa-times font-red"></i>', array('controller' => 'checklist_tasks', 'action' => 'delete', $task['id']), array('escape' => false, 'class' => 'delete-task pull-right')); ?>
								</span>
							</div>
						</li>
						<?php endforeach; ?>
					<?php endif; ?>
					<li class="new-task">
						<div class="task-checkbox">
							<input type="checkbox" name="name" value="">
						</div>
						<div class="task-title">
							<span class="task-title-sp">
								<span class="new-task-input">
									<input type="text" placeholder="<?php echo __('New task'); ?>" class="form-control input-sm" data-href="<?php echo Router::url(array('controller' => 'checklist_tasks', 'action' => 'add', 'checklist_batch_id' => $batch['id'])); ?>">
								</span>
							</span>
						</div>
					</li>
				</ul>
				<div class="text-right" style="margin-top: 10px">
					<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete group')), array('controller' => 'checklist_batches', 'action' => 'delete', $batch['id']), array('class' => 'btn btn-xs btn-danger delete-batch', 'escape' => false)); ?>
				</div>
			</div>
		</div>
	</div>
	<?php endforeach;?>
</div>
<?php else: ?>
	<div class="alert alert-info">
		<p>Aucun gorupe ou tâche n'a été attribuée.</p>
	</div>
<?php endif; ?>
<input type="text" class="form-control new-batch" data-href="<?php echo Router::url(array('controller' => 'checklist_batches', 'action' => 'add', 'checklist_id' => $checklist['Checklist']['id'])); ?>" placeholder="<?php echo __('New group of tasks'); ?>">
