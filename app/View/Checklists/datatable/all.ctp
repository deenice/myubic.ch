<?php
$models = Configure::read('Checklists.models');
foreach ($dtResults as $result) {

  $batches = '';

  if(!empty($result['ChecklistBatch'])){
    foreach($result['ChecklistBatch'] as $batche){
      $batches .= $this->Html->tag('li', $batche['name']);
    }
  }

  $batches = $this->Html->tag('ul', $batches, array('class' => 'list-unstyled'));

  $actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'checklists', 'action' => 'edit', $result['Checklist']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );
  if($result['Checklist']['default'] || $result['Checklist']['user_id'] == AuthComponent::user('id')){
    $actions .= $this->Html->link(
      '<i class="fa fa-copy"></i> ' . __("Duplicate"),
      array('controller' => 'checklists', 'action' => 'duplicate', $result['Checklist']['id']),
      array('class' => 'btn btn-info btn-xs', 'escape' => false)
    );
  }
  if($result['Checklist']['user_id'] == AuthComponent::user('id') || AuthComponent::user('id') == 1){
    $actions .= $this->Html->link(
      '<i class="fa fa-times"></i> ' . __("Delete"),
      array('controller' => 'checklists', 'action' => 'delete', $result['Checklist']['id']),
      array('class' => 'btn btn-danger btn-xs', 'escape' => false)
    );
  }

  $name = '';
  $name .= $this->Html->tag('span', '', array('class' => 'btn btn-xs btn-company btn-company-' . $result['Company']['class']));
  if($result['Checklist']['default'] == 1){
    $name .= $this->Html->tag('i', '', array('class' => 'fa fa-star font-yellow'));
  }
  if($result['Checklist']['personal'] == 1){
    $name .= $this->Html->tag('i', '', array('class' => 'fa fa-user font-grey-silver'));
  }
  $name .= ' ' . $result['Checklist']['name'];

  $this->dtResponse['aaData'][] = array(
    $name,
    $models[$result['Checklist']['model']],
    $batches,
    $actions
  );
}
