<?php $this->assign('page_title', __('Checklists')); ?>
<?php $this->assign('page_subtitle', __('Calendar')); ?>

<div class="portlet light">
  <div class="portlet-body">
    <div class="row">
			<div class="col-md-12">
				<div class="portlet light calendar">
					<div class="portlet-title"></div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="tasksCalendar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.checklist_tasks();';
$this->end();
?>
