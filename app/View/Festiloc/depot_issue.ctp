<div class="row" id="flashMessage">
<?php echo $this->Session->flash(); ?>
</div>
<?php if(!empty($stockorder)): ?>
<div class="row hidden-print">
	<div class="col-md-6">
		<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Back to stock orders'), array('controller' => 'festiloc', 'action' => 'depot#' . $stockorder['StockOrder']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	</div>
	<div class="col-md-6 text-right">
		<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Pallets/rollis sheets'), array('controller' => 'stock_orders', 'action' => 'pallet', $stockorder['StockOrder']['id'], 'ext' => 'pdf', '?' => 'download=0&source=depot'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
		<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Transporter sheets'), array('controller' => 'stock_orders', 'action' => 'transporter_pdf', $stockorder['StockOrder']['id'], 'ext' => 'pdf', '?' => 'download=0'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-red-intense bold uppercase"><?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?>&nbsp;&nbsp;&nbsp;
						<?php echo $stockorder['Client']['name']; ?> - <?php echo $stockorder['StockOrder']['name']; ?></span>
				</div>
				<div class="actions hidden-print">
					<?php echo $this->Html->link('<i class="fa fa-money"></i> ' . __('Invoice'), array('controller' => 'stock_orders', 'action' => 'invoice', 'ext' => 'pdf', $stockorder['StockOrder']['id'], '?' => 'download=0&invoice=1&temp=1'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
				</div>
			</div>
			<div class="portlet-body">
				<?php echo $this->Form->create('StockOrder', array('action' => 'save')); ?>
				<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
				<div class="row">
					<div class="col-md-6 margin-bottom-5">
						<h4>Conditionnement</h4>
						<table class="table">
							<tr>
								<td>Nombre de palettes</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_pallets1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_pallets1']) ? is_null($stockorder['StockOrder']['number_of_pallets']) ? 0 : $stockorder['StockOrder']['number_of_pallets'] : $stockorder['StockOrder']['number_of_pallets1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de palettes XL</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_pallets_xl1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_pallets_xl1']) ? is_null($stockorder['StockOrder']['number_of_pallets']) ? 0 : $stockorder['StockOrder']['number_of_pallets_xl'] : $stockorder['StockOrder']['number_of_pallets_xl1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de rollis</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_rollis1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_rollis1']) ? is_null($stockorder['StockOrder']['number_of_rollis']) ? 0 : $stockorder['StockOrder']['number_of_rollis'] : $stockorder['StockOrder']['number_of_rollis1'])); ?></td>
							</tr>
							<tr>
								<td>Nombre de rollis XL</td>
								<td><?php echo $this->Form->input('StockOrder.number_of_rollis_xl1', array('label' => false, 'class' => 'form-control input-small updatePackaging', 'value' => is_null($stockorder['StockOrder']['number_of_rollis_xl1']) ? is_null($stockorder['StockOrder']['number_of_rollis_xl']) ? 0 : $stockorder['StockOrder']['number_of_rollis_xl'] : $stockorder['StockOrder']['number_of_rollis_xl1'])); ?></td>
							</tr>
							<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
						</table>
					</div>
					<div class="col-md-6 margin-bottom-5">
						<h4><?php echo __('On site payment'); ?></h4>
						<table class="table">
							<tr>
								<td><?php echo __('Payment date'); ?></td>
								<td><?php echo $this->Form->input('StockOrder.payment_date', array('class' => 'form-control date-picker', 'type' => 'text', 'label' => false, 'div' => false, 'value' => empty($stockorder['StockOrder']['payment_date']) ? '' : $stockorder['StockOrder']['payment_date'])); ?></td>
							</tr>
							<tr>
								<td><?php echo __('Amount'); ?></td>
								<td><?php echo $this->Form->input('StockOrder.on_site_payment', array('class' => 'form-control', 'label' => false, 'div' => false, 'value' => empty($stockorder['StockOrder']['on_site_payment']) ? '' : $stockorder['StockOrder']['on_site_payment'])); ?></td>
							</tr>
							<tr>
								<td><?php echo __('Payment method'); ?></td>
								<td><?php echo $this->Form->input('StockOrder.payment_method', array('class' => 'form-control bs-select', 'label' => false, 'div' => false, 'options' => Configure::read('StockOrders.payment_methods'), 'empty' => __('Payment method'), 'value' => $stockorder['StockOrder']['payment_method'])); ?></td>
							</tr>
						</table>
					</div>
					<div class="col-md-12">
						<h4>Produits</h4>
						<div class="table-responsive">
							<table class="table" id="">
								<thead>
									<tr>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Ordered quantity'); ?></th>
										<th><?php echo __('Delivered quantity'); ?></th>
										<th><?php echo __('Remarks'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
										<tr id="batch<?php echo $batch['id']; ?>" class="batch">
											<td class="code"><?php echo !empty($batch['StockItem']['code']) ? $batch['StockItem']['code'] : ''; ?></td>
											<td class="stockitem">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $batch['name']; ?>
												<?php else: ?>
												<?php echo $batch['StockItem']['name']; ?>
												<?php echo $this->Form->input('StockOrderBatch.name.', array('type' => 'hidden', 'value' => '')); ?>
												<?php endif; ?>
											</td>
											<td class="quantity"><?php echo $batch['quantity']; ?></td>
											<td class="quantity"><?php echo $this->Form->input('StockOrder.delivered_quantity.', array('class' => 'form-control input-xs input-inline input-xsmall delivered_quantity', 'label' => false, 'div' => false, 'type' => 'number', 'value' => !is_numeric($batch['delivered_quantity']) ? '' : $batch['delivered_quantity'], 'required' => false)); ?></td>
											<td><?php echo $this->Form->input('StockOrder.delivery_remarks.', array('class' => 'form-control input-large', 'label' => false, 'div' => false, 'required' => false, 'value' => $batch['delivery_remarks'])); ?></td>
											<?php if($batch['stock_item_id'] != -1): ?>
											<?php echo $this->Form->input('StockOrder.stock_item_id.', array('type' => 'hidden', 'value' => $batch['StockItem']['id'], 'class' => 'stockItemId')); ?>
											<?php endif; ?>
											<?php echo $this->Form->input('StockOrder.stock_order_batch_id.', array('type' => 'hidden', 'value' => $batch['id'])); ?>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="form-group hidden-print">
							<h4><?php echo __('Person in charge'); ?></h4>
							<?php echo $this->Form->input('StockOrder.resp_id', array('class' => 'form-control bs-select', 'data-live-search' => true, 'label' => false, 'div' => false, 'options' => $collaborators, 'empty' => true)); ?>
						</div>
						<div class="form-group">
							<h4><?php echo __('Global remarks'); ?></h4>
							<?php echo $this->Form->input('StockOrder.return_control_remarks', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'required' => false, 'value' => empty($stockorder['StockOrder']['return_control_remarks']) ? '' : $stockorder['StockOrder']['return_control_remarks'] )); ?>
						</div>
					</div>
					<div class="col-md-12 hidden-print">
					<br />
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and continue editing'), array('class' => 'btn btn-primary', 'escape' => false, 'type' => 'submit', 'value' => 'depot_issue', 'name' => 'data[destination]')); ?>
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and exit'), array('class' => 'btn btn-warning save-issue', 'escape' => false, 'type' => 'submit', 'value' => 'depot_live', 'name' => 'data[destination]')); ?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
$this->start('init_scripts');
echo 'Custom.festiloc();';
$this->end();
?>
