<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Turnover')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-graph"></i><?php echo __('Turnover'); ?>
		</div>
	</div>
	<div class="portlet-body" style="padding-bottom: 200px">
		<?php echo $this->Form->create('Turnover'); ?>
		<div class="row">
			<div class="col-md-3">
				<?php echo $this->Form->input('from', array('class' => 'form-control date-picker', 'label' => false, 'placeholder' => __('From'))); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->input('to', array('class' => 'form-control date-picker', 'label' => false, 'placeholder' => __('To'))); ?>
			</div>
			<div class="col-md-3">
				<?php echo $this->Form->input('status', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('StockOrders.status'), 'multiple' => true)); ?>
			</div>
			<div class="col-md-3">
				<button type="submit" class="btn btn-block btn-primary"><?php echo __('Compute'); ?></button>
			</div>
		</div>
		<?php if(!empty($rents) || !empty($sales)): ?>
		<div class="row" style="padding-top: 100px;" id="turnover">
			<div class="col-md-12">
				<table class="table">
					<thead>
						<tr>
							<th><?php echo __('Designation'); ?></th>
							<th><?php echo __('Total'); ?></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><?php echo __('Turnover rent (without delivery costs) HT'); ?></td>
							<td><?php echo $rents; ?> CHF</td>
						</tr>
						<tr>
							<td><?php echo __('Turnover sales (without delivery costs) HT'); ?></td>
							<td><?php echo $sales; ?> CHF</td>
						</tr>
						<tr>
							<td><?php echo __('Turnover on  delivery costs HT'); ?></td>
							<td><?php echo $deliveryCosts; ?> CHF</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif; ?>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
