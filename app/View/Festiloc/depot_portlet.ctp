<div class="row">
 <?php if(!empty($data['order']['StockOrderBatch']['old']) OR !empty($data['order']['StockOrderBatch']['actual'])): ?>
   <div class="col-md-12">
     <h3><?php echo __('Stock items'); ?></h3>
     <table class="table">
       <tr>
         <th><?php echo __('Status'); ?></th>
         <th><?php echo __('Code'); ?></th>
         <th><?php echo __('Name'); ?></th>
         <th><?php echo __('Quantity'); ?></th>
       </tr>
       <?php foreach($data['order']['StockOrderBatch']['old'] as $stockItemId => $batch): ?>
         <?php if($batch['stock_item_id'] == -1){
                                 $key = '9999'.$batch['id'];
         } else {
           $key = $batch['stock_item_id'].$batch['id'];
         } ?>
         <?php if(!empty($data['order']['StockOrderBatch']['actual'][$key])): ?>
           <?php // it means the product is in first and last version of order ?>
           <?php if($data['order']['StockOrderBatch']['old'][$key]['quantity'] != $data['order']['StockOrderBatch']['actual'][$key]['quantity']): ?>
             <tr>
               <td><span class="badge bg-blue badge-roundless">modifié</span></td>
               <td class="font-blue"><?php echo $data['products'][$key]['StockItem']['code']; ?></td>
               <td class="font-blue"><?php echo $data['products'][$key]['StockItem']['name']; ?></td>
               <td class="font-blue">
                 <strong>
                   <?php echo $data['order']['StockOrderBatch']['actual'][$key]['quantity']; ?></strong>
                   au lieu de <?php echo $data['order']['StockOrderBatch']['old'][$key]['quantity']; ?>
               </td>
             </tr>
           <?php endif; ?>
         <?php endif; ?>
       <?php endforeach; ?>
       <?php foreach($data['order']['StockOrderBatch']['actual'] as $stockItemId => $batch): ?>
         <?php if($batch['stock_item_id'] == -1){
                                 $key = '9999'.$batch['id'];
         } else {
           $key = $batch['stock_item_id'].$batch['id'];
         } ?>
         <?php if(empty($data['order']['StockOrderBatch']['old'][$key])): ?>
           <?php // it means the product has been added ?>
           <tr>
             <td><span class="badge bg-green badge-roundless">ajouté</span></td>
             <td class="font-green"><?php echo $data['products'][$key]['StockItem']['code']; ?></td>
             <td class="font-green"><?php echo $data['products'][$key]['StockItem']['name']; ?></td>
             <td class="font-green"><?php echo $data['order']['StockOrderBatch']['actual'][$key]['quantity']; ?></td>
           </tr>
         <?php endif; ?>
       <?php endforeach; ?>
       <?php foreach($data['order']['StockOrderBatch']['old'] as $stockItemId => $batch): ?>
         <?php if($batch['stock_item_id'] == -1){
                                 $key = '9999'.$batch['id'];
         } else {
           $key = $batch['stock_item_id'].$batch['id'];
         } ?>
         <?php if(empty($data['order']['StockOrderBatch']['actual'][$key])): ?>
           <?php // it means the product has been removed ?>
           <tr>
             <td><span class="badge bg-red badge-roundless">supprimé</span></td>
             <td class="text-muted"><?php echo $data['products'][$key]['StockItem']['code']; ?></td>
             <td class="text-muted"><?php echo $data['products'][$key]['StockItem']['name']; ?></td>
             <td class="text-muted"><?php echo $data['order']['StockOrderBatch']['old'][$key]['quantity']; ?></td>
           </tr>
         <?php endif; ?>
       <?php endforeach; ?>
     </table>
   </div>
 <?php endif; ?>
 <?php if(!empty($data['versions'][$data['order']['StockOrder']['id']])): ?>
 <?php foreach($data['versions'][$data['order']['StockOrder']['id']] as $model => $keys): ?>
   <div class="col-md-12"><h3><?php echo $model ?></h3></div>
   <div class="col-md-6">
     <table class="table">
     <tr>
       <th colspan="2">Ancienne version</th>
     </tr>
     <?php foreach($keys as $key => $fields): ?>
       <tr>
         <td><?php echo __($key); ?></td>
         <td><?php echo __($fields['old']); ?></td>
       </tr>
     <?php endforeach; ?>
     </table>
   </div>
   <div class="col-md-6">
     <table class="table">
     <tr>
       <th colspan="2">Nouvelle version</th>
     </tr>
     <?php foreach($keys as $key => $fields): ?>
       <tr>
         <td><?php echo __($key); ?></td>
         <td><?php echo __($fields['new']); ?></td>
       </tr>
     <?php endforeach; ?>
     </table>
   </div>
 <?php endforeach; ?>
 <?php endif; ?>
</div>
