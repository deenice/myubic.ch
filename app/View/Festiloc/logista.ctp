<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Statistics Logista')); ?>

<div class="portlet light bordered">
  <div class="portlet-title">
		<div class="caption">
			<i class="icon-bar-chart"></i><?php echo __('Logista'); ?>
		</div>
	</div>
  <div class="portlet-body">
    <ul class="nav nav-tabs">
      <?php $k=0; foreach($data as $month => $orders): ?>
        <li class="<?php echo $k == 0 ? 'active':''; ?>">
          <a href="#month<?php echo $month; ?>" data-toggle="tab"> <?php echo ucfirst($this->Time->format($month, '%B')); ?> </a>
        </li>
      <?php $k++; endforeach; ?>
    </ul>
    <div class="tab-content">
      <?php $k=0; foreach($data as $month => $orders): ?>
        <?php $pallets = $rollis = $xl = $palletsCosts = $rollisCosts = $xlCosts = $total = $estimatedTotal = 0; ?>
        <div class="tab-pane<?php echo $k == 0 ? ' active':''; ?>" id="month<?php echo $month; ?>">
          <table class="table table-striped table-hover table-logista">
            <thead>
              <tr>
                <th class="date"><?php echo __('Date'); ?></th>
                <th class="mode"></th>
                <th class="order"><?php echo __('Order'); ?></th>
                <th class="packaging"><?php echo __('Pallets'); ?></th>
                <th class="cost"></th>
                <th class="packaging"><?php echo __('Rollis'); ?></th>
                <th class="cost"></th>
                <th class="packaging"><?php echo __('XL'); ?></th>
                <th class="cost"></th>
                <th class="packaging">Total</th>
                <th class="packaging">Estimation</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($orders as $order): ?>
              <tr>
                <td>
                  <?php if($order['StockOrder']['mode'] == 'delivery'): ?>
                    <?php echo $this->Time->format('d.m.Y', $order['StockOrder']['delivery_date']); ?>
                  <?php elseif($order['StockOrder']['mode'] == 'return'): ?>
                    <?php echo $this->Time->format('d.m.Y', $order['StockOrder']['return_date']); ?>
                  <?php endif; ?>
                </td>
                <td>
                  <span class="uppercase">
                  <?php if($order['StockOrder']['mode'] == 'delivery'): ?>
                    <?php echo __('Delivery'); ?>
                  <?php elseif($order['StockOrder']['mode'] == 'return'): ?>
                    <?php echo __('Return'); ?>
                  <?php endif; ?>
                  </span>
                </td>
                <td><?php echo $this->Html->link($order['StockOrder']['order_number'], array('controller' => 'stock_orders', 'action' => 'edit', $order['StockOrder']['id']), array('target' => '_blank')); ?> <?php echo $order['Client']['name']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging']['pallets']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging_costs']['pallets']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging']['rollis']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging_costs']['rollis']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging']['xl']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging_costs']['xl']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['packaging_costs']['total']; ?></td>
                <td class="number"><?php echo $order['StockOrder']['estimations']['total']; ?></td>
              </tr>
              <?php
              $pallets += $order['StockOrder']['packaging']['pallets'];
              $rollis += $order['StockOrder']['packaging']['rollis'];
              $xl += $order['StockOrder']['packaging']['xl'];
              $palletsCosts += $order['StockOrder']['packaging_costs']['pallets'];
              $rollisCosts += $order['StockOrder']['packaging_costs']['rollis'];
              $xlCosts += $order['StockOrder']['packaging_costs']['xl'];
              $total += $order['StockOrder']['packaging_costs']['total'];
              $estimatedTotal += $order['StockOrder']['estimations']['total'];
              ?>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="3"><span class="bold uppercase">Total</span></td>
                <td><?php echo $pallets; ?></td>
                <td><?php echo $palletsCosts; ?></td>
                <td><?php echo $rollis; ?></td>
                <td><?php echo $rollisCosts; ?></td>
                <td><?php echo $xl; ?></td>
                <td><?php echo $xlCosts; ?></td>
                <td><?php echo $total; ?></td>
                <td><?php echo $estimatedTotal; ?></td>
              </tr>
            </tfoot>
          </table>
        </div>
      <?php $k++; endforeach; ?>
    </div>
  </div>
</div>
