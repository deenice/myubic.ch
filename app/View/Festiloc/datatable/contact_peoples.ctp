<?php
$countries = Configure::read('Countries');
foreach ($dtResults as $result) {

  $cp = $result['ContactPeople']['full_name'];
  $cp .= $this->Html->tag('br');
  $cp .= $result['ContactPeople']['email'];
  $cp .= $this->Html->tag('br');
  $cp .= $result['ContactPeople']['phone'];


  $address = $result['Client']['address'];
  $address .= $this->Html->tag('br');
  $address .= $result['Client']['zip'] . ' ' . $result['Client']['city'];
  $address .= $this->Html->tag('br');
  $address .= $countries[$result['Client']['country']];

  $actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'clients', 'action' => 'view', $result['Client']['id']),
    array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
  );
  $actions .= $this->Html->tag('');
  $actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'clients', 'action' => 'edit', $result['Client']['id']),
    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
    );

  $this->dtResponse['aaData'][] = array(
    $result['Client']['name'],
    $cp,
    $address,
    $actions
  );
}
