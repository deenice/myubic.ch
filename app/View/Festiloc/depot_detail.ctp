<div class="row" id="flashMessage">
<?php echo $this->Session->flash(); ?>
</div>
<div class="row">
	<div class="col-md-6">
		<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Back to stock orders'), array('controller' => 'festiloc', 'action' => 'depot#' . $stockorder['StockOrder']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	</div>
	<div class="col-md-6 text-right">
		<?php echo $this->Html->link('<i class="fa fa-print"></i> ' . __('Print delivery note'), array('controller' => 'stock_orders', 'action' => 'invoice', $stockorder['StockOrder']['id'], 'ext' => 'pdf', '?' => 'download=0&showPrices=0&source=depot'), array('class' => 'btn default', 'escape' => false, 'target' => '_blank')); ?>
		<?php echo $this->Html->link('<i class="fa fa-check"></i> ' . __('Validate issue'), array('controller' => 'festiloc', 'action' => 'depot', 'issue', $stockorder['StockOrder']['id']), array('class' => 'btn btn-success', 'escape' => false)); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-red-intense bold uppercase"><?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?>&nbsp;&nbsp;&nbsp;
						<?php echo $stockorder['Client']['name']; ?> - <?php echo $stockorder['StockOrder']['name']; ?></span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<?php if(!empty($stockorder['StockOrder']['remarks'])): ?>
					<div class="col-md-12 margin-bottom-5">
						<div class="alert alert-warning">
							<i class="fa fa-warning"></i> <?php echo $stockorder['StockOrder']['remarks']; ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="col-md-6">
						<h4>Livraison</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Delivery date'); ?></strong></td>
								<td><?php echo $this->Time->format($stockorder['StockOrder']['delivery_date'], '%A %d %B %Y'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Delivery'); ?></strong></td>
								<td>
									<?php if($stockorder['StockOrder']['delivery']): ?>
									Avec livraison
									<?php else: ?>
										Sans livraison
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Moment'); ?></strong></td>
								<td><?php echo !empty($stockorder['StockOrder']['delivery_moment']) ? $moments[$stockorder['StockOrder']['delivery_moment']] : __('During day'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Address'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['delivery_address']; ?><br />
									<?php echo $stockorder['StockOrder']['delivery_zip']; ?>
									<?php echo $stockorder['StockOrder']['delivery_city']; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact person'); ?></strong></td>
								<td>
									<?php echo $stockorder['ContactPeopleDelivery']['name']; ?>
									<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Remarks'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['delivery_remarks']; ?>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<h4>Retour</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Return date'); ?></strong></td>
								<td><?php echo $this->Time->format($stockorder['StockOrder']['return_date'], '%A %d %B %Y'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Return'); ?></strong></td>
								<td>
									<?php if($stockorder['StockOrder']['return']): ?>
									Avec livraison
									<?php else: ?>
										Sans livraison
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Moment'); ?></strong></td>
								<td><?php echo !empty($stockorder['StockOrder']['return_moment']) ? $moments[$stockorder['StockOrder']['return_moment']] : __('During day'); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Return address'); ?></strong></td>
								<td>
									<?php echo $stockorder['StockOrder']['return_address']; ?><br />
									<?php echo $stockorder['StockOrder']['return_zip']; ?>
									<?php echo $stockorder['StockOrder']['return_city']; ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<h4>Client</h4>
						<table class="table">
							<tr>
								<td><strong><?php echo __('Client'); ?></strong></td>
								<td><?php echo $stockorder['Client']['name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact person'); ?></strong></td>
								<td><?php echo $stockorder['ContactPeopleClient']['name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Address'); ?></strong></td>
								<td>
									<?php echo $stockorder['Client']['address']; ?><br />
									<?php echo $stockorder['Client']['zip_city']; ?><br />
									<?php echo $stockorder['Client']['country']; ?>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-12">
						<h4>Produits</h4>
						<div class="table-responsive">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Coefficient'); ?></th>
										<th><?php echo __('Quantity'); ?></th>
										<th><?php echo __('Unit price'); ?></th>
										<th><?php echo __('Net HT'); ?></th>
										<th><?php echo __('Discount'); ?></th>
										<th><?php echo __('Total TTC'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
											$price = $batch['price'];
										} else {
											$price = $batch['StockItem']['price'];
										} ?>
										<tr>
											<td class="code">
												<?php echo empty($batch['StockItem']['code']) ? '' : $batch['StockItem']['code']; ?>
											</td>
											<td class="stockitem">
												<?php if($batch['stock_item_id'] == -1): ?>
												<?php echo $batch['name']; ?>
												<?php else: ?>
												<?php echo $this->Html->link($batch['StockItem']['name'], array('#' => ''), array('data-toggle' => 'modal', 'data-target' => '#modal' . $batch['StockItem']['id'])); ?>
												<?php endif; ?>
											</td>
											<td class="coefficient"><?php echo $batch['coefficient']; ?></td>
											<td class="quantity"><?php echo $batch['quantity']; ?></td>
											<td class="price"><?php echo $price; ?></td>
											<td class="net_ht">
												<?php echo $net_ht = number_format((float)$batch['coefficient'] * $batch['quantity'] * $price, 2, '.', '');?>
											</td>
											<td class="discount">
												<?php echo empty($batch['discount']) ? 0 : $batch['discount']; ?> %
											</td>
											<td class="total_ttc">
												<?php echo number_format($net_ht - ($net_ht * $batch['discount'] / 100), 2, '.', '');?>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php foreach($stockorder['StockOrderBatch'] as $batch): ?>
<div class="modal fade" id="modal<?php echo $batch['StockItem']['id']; ?>" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<img src="<?php echo $this->webroot . $batch['StockItem']['Document'][0]['url']; ?>" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endforeach; ?>

<?php
$this->start('init_scripts');
echo 'Custom.festiloc();';
$this->end();
?>
