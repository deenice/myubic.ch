<div class="portlet light calendar depot-calendar">
  <div class="portlet-title">
    <div class="actions pull-left">
      <ul>
        <li><span class="badge badge-roundless bg-green">&nbsp;&nbsp;</span> <?php echo __('Confirmed'); ?></li>
        <li><span class="badge badge-roundless bg-yellow-crusta">&nbsp;&nbsp;</span> <?php echo __('In progress'); ?></li>
        <li><span class="badge badge-roundless bg-blue-madison">&nbsp;&nbsp;</span> <?php echo __('Processed / Ready to deliver'); ?></li>
        <li><span class="badge badge-roundless bg-grey-gallery">&nbsp;&nbsp;</span> <?php echo __('Delivered'); ?></li>
        <li><span class="badge badge-roundless bg-red">&nbsp;&nbsp;</span> <?php echo __('Modified order'); ?></li>
        <li><span class="badge badge-roundless bg-yellow-gold">&nbsp;&nbsp;</span> <?php echo __('Cancelled order'); ?></li>
      </ul>
      <ul>
        <li><span><i class="fa fa-user"></i></span> <?php echo __('Client'); ?></li>
        <li><span><i class="fa fa-truck"></i></span> <?php echo __('Festiloc'); ?></li>
        <li><span><i class="fa fa-envelope-o"></i></span> <?php echo __('Transporter'); ?></li>
      </ul>
      <div class="checkbox-list" style="margin-top: 10px">
        <label class="checkbox-inline">Afficher les</label>
        <label class="checkbox-inline">
          <input type="checkbox" id="display_deliveries" checked="checked"> <?php echo __('Deliveries'); ?>
        </label>
        <label class="checkbox-inline">
          <input type="checkbox" id="display_returns" checked="checked"> <?php echo __('Returns'); ?>
        </label>
        <?php echo $this->Form->input('filter', array('class' => 'form-control filter1 input-medium pull-right','placeholder' => 'Numéro de commande', 'label' => false, 'div' => false)); ?>
      </div>
    </div>
  </div>
  <div class="portlet-body">
    <div class="row">
      <div class="col-md-12">
        <?php echo $this->Form->input('lastModifiedOrder', array('type' => 'hidden', 'value' => empty($id) ? '' : $id)); ?>
        <div id="depotCalendar" class="depotCalendar"></div>
      </div>
    </div>
  </div>
</div>


<div id="context">
  <ul class="dropdown-menu" role="menu" style="height: 400px; overflow-y: scroll; position: relative">
    <li class="order_number disabled bg-grey"><a href=""><h5 class="bold uppercase"><?php echo __('Order'); ?> <span></span></h5></a></li>
		<li class="notifications"><a href="#" class="font-red bold" data-toggle="modal" data-target="#modifications"><i class="fa fa-warning font-red"></i> <?php echo __('Show modifications'); ?></a></li>
		<li class="cancellations"><a href="#" class="font-yellow-crusta bold notify-cancellation" data-stock-order-id><i class="fa fa-warning font-yellow-crusta"></i> <?php echo __('Confirm'); ?></a></li>
    <li class="disabled"><a href="" class="bold"><strong><?php echo __('Actions'); ?></strong></a></li>
		<li><a href="#" class="depot_detail link"><i class="fa fa-list"></i> <?php echo __('View order detail'); ?></a></li>
		<li><a href="#" class="depot_issue link"><i class="fa fa-check"></i> <?php echo __('Validate issue / Payment'); ?></a></li>
		<li><a href="#" class="depot_return link"><i class="fa fa-clipboard"></i> <?php echo __('Control return'); ?></a></li>
    <li class="divider"></li>
    <li class="disabled"><a href="" class="bold"><strong><?php echo __('Prints'); ?></strong></a></li>
		<li><a href="#" class="depot_delivery_note_pdf pdf" data-status="" data-stock-order-id=""><i class="fa fa-print"></i> <?php echo __('Delivery note'); ?></a></li>
		<li><a href="#" class="depot_pallet_pdf pdf" data-status="" data-stock-order-id=""><i class="fa fa-print"></i> <?php echo __('Pallets/rollis sheets'); ?></a></li>
		<li><a href="#" class="depot_transporter_pdf pdf" data-status=""><i class="fa fa-print"></i> <?php echo __('Transporter sheets'); ?></a></li>
		<li><a href="#" class="depot_return_pdf pdf" data-status=""><i class="fa fa-print"></i> <?php echo __('Return sheets'); ?></a></li>
		<li><a href="#" class="depot_return_pdf_all pdf" data-status="" data-stock-order-id=""><i class="fa fa-print"></i> <?php echo __('Today return sheets'); ?></a></li>
    <li class="divider"></li>
    <li class="disabled"><a href="" class="bold"><strong><?php echo __('Management'); ?></strong></a></li>
		<li><a href="#" class="action_change_status"><i class="fa fa-sort-amount-asc"></i> <?php echo __('Change status'); ?></a></li>
		<li><a href="#" class="action_change_weight"><i class="fa fa-sort-amount-asc"></i> <?php echo __('Change preparation order'); ?></a></li>
	</ul>
</div>

<div class="modal fade" id="modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
        <div class="portlet">
           <div class="portlet-title hidden">
              <div class="tools">
                 <a href="javascript:;" data-url="" class="reload"></a>
              </div>
           </div>
           <div class="portlet-body portlet-empty"></div>
        </div>
			</div>
			<div class="modal-footer">
        <button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-times"></i> Je ne traite pas encore la modification</button>
        <button type="button" class="btn green confirmModifications" data-stock-order-id=""><i class="fa fa-check"></i> Je traite la modification</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="weights">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
        <div class="portlet">
           <div class="portlet-title">
             <div class="caption">
               <span class="uppercase bold"><?php echo __('Change preparation order'); ?></span>
             </div>
           </div>
           <div class="portlet-body">
             <?php echo $this->Form->create('StockOrder'); ?>
             <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
             <?php echo $this->Form->input('mode', array('type' => 'hidden')); ?>
             <div class="form-group">
               <label for=""><?php echo __('Rank'); ?></label>
               <?php echo $this->Form->input('delivery_weight', array('type' => 'number', 'label' => false, 'class' => 'form-control delivery_weight')); ?>
               <?php echo $this->Form->input('return_weight', array('type' => 'number', 'label' => false, 'class' => 'form-control return_weight')); ?>
             </div>
             <?php echo $this->Form->end(); ?>
           </div>
        </div>
			</div>
			<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				<button type="button" class="btn btn-primary save-weight">Sauvegarder</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="status">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
        <div class="portlet">
           <div class="portlet-title">
             <div class="caption">
               <span class="uppercase bold"><?php echo __('Change status'); ?></span>
             </div>
           </div>
           <div class="portlet-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->Form->create('StockOrder', array('id' => 'StockOrderDepotFormStatus')); ?>
                <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                <div class="form-group">
    							<label class="control-label"><?php echo __('Status'); ?></label>
    							<?php echo $this->Form->input('status', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('StockOrders.status_for_depot'))); ?>
    						</div>
                <?php echo $this->Form->end(); ?>
              </div>
            </div>
           </div>
        </div>
			</div>
			<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
				<button type="button" class="btn btn-primary save-status">Sauvegarder</button>
			</div>
		</div>
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.festiloc();';
$this->end();
?>
