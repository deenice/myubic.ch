<?php if(!empty($stockorders)): ?>
<div class="row">
  <div class="col-md-12">
    <a href="#" data-list="stockorders" class="btn btn-primary btn-lg sync pull-right"><i class="fa fa-cloud-upload"></i> <?php echo __('Export'); ?></a>
  </div>
  <div class="col-md-12" style="margin-top: 10px">
    <div class="progress progress-striped active">
      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"> 40% Complete (success) </span>
      </div>
    </div>
  </div>
</div>
<div class="portlet light">
  <div class="portlet-body">
    <table class="table" id="stockorders">
      <thead>
        <tr>
          <th>#</th>
          <th><?php echo __('Stock order'); ?></th>
          <th><?php echo __('Status'); ?></th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($stockorders as $k => $order): ?>
          <tr data-item-id="<?php echo $order['StockOrder']['id']; ?>" data-company-id="<?php echo $company['Company']['id']; ?>" class="<?php echo empty($order['StockOrder']['uuid']) ? '' : 'exported'; ?>">
            <td><?php echo ++$k; ?></td>
            <td>
              <strong><?php echo $order['StockOrder']['order_number']; ?></strong>&nbsp;&nbsp;
              <?php echo $order['Client']['name']; ?>&nbsp;&nbsp;&nbsp;
              <?php echo $order['StockOrder']['name']; ?>
            </td>
            <td><i class="fa fa-minus"></i></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>

<?php else: ?>
  <div class="alert alert-info">
    <p>
      <i class="fa fa-info-circle"></i> <?php echo __('There are no invoices to export.'); ?>
    </p>
  </div>
<?php endif;?>

<?php $this->start('init_scripts'); ?>

  $(document).ready(function(){
    var basePath = "";
    if(document.location.hostname == 'localhost' || document.location.hostname == '192.168.1.126'){
        basePath = '/myubic';
    }
    var sync = function( list, id ){

      var row = $('#'+list+' tbody tr:eq('+id+')');
      var itemId = row.data('item-id');
      var companyId = row.data('company-id');
      var max = $('#'+list+' tbody tr').get().length;
      row.find('td:last').html('<i class="fa fa-spin fa-spinner"></i>');

      $.ajax({
        url: basePath + '/revelate/export/'+list+'/' + companyId,
        dataType: 'json',
        type: 'post',
        data: {
          itemId: itemId,
          companyId: companyId
        },
        success: function(data){
          console.log(data);
          if(data){
            if(data.success == 1){
              row.find('td:last').html('<i class="fa fa-check font-green"></i>');
            } else if(data.success == 0){
              row.find('td:last').html('<i class="fa fa-times font-red"></i>');
            }
          }
        }
      }).then(function(){
        id++;
        var percent = Math.ceil(id / max * 100);
        $('.progress-bar').css('width', percent + '%');
        if(percent == 100){
          $('.progress').delay(1000).fadeOut();
        }
        if(id < max){
          sync( list, id );
        } else {
          return;
        }
      });

    }
    $('body').on('click', '.sync', function(e){
      e.preventDefault();
      var list = $(this).data('list');
      sync(list, 0);
    });
  });

<?php $this->end(); ?>
