<div class="row" id="flashMessage">
<?php echo $this->Session->flash(); ?>
</div>
<?php if(!empty($stockorder)): ?>
<div class="row hidden-print">
	<div class="col-md-6">
		<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Back to stock orders'), array('controller' => 'festiloc', 'action' => 'depot#' . $stockorder['StockOrder']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	</div>
</div>
<br />
<div class="row">
	<div class="col-md-12">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject font-red-intense bold uppercase"><?php echo $stockorder['StockOrder']['id']; ?> / <?php echo $stockorder['Client']['id']; ?>&nbsp;&nbsp;&nbsp;
						<?php echo $stockorder['Client']['name']; ?> - <?php echo $stockorder['StockOrder']['name']; ?></span>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-warning visible-print-block">
							<p><i class="fa fa-warning"></i> Ne pas oublier de remplir le retour sur www.myubic.ch</p>
						</div>
					</div>
				</div>
				<?php echo $this->Form->create('StockOrder', array('action' => 'save')); ?>
				<?php echo $this->Form->input('StockOrder.id', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['id'])); ?>
				<?php echo $this->Form->input('StockOrder.return_date', array('type' => 'hidden', 'value' => $stockorder['StockOrder']['return_date'])); ?>
				<div class="row">
					<?php if(!empty($stockorder['StockOrder']['number_of_pallets']) OR !empty($stockorder['StockOrder']['number_of_rollis'])): ?>
					<div class="col-md-12 margin-bottom-5">
						<h4>Conditionnement</h4>
						<?php if(!empty($stockorder['StockOrder']['number_of_pallets'])): ?>
							Nombre de palettes : <?php echo $stockorder['StockOrder']['number_of_pallets']; ?><br />
						<?php endif; ?>
						<?php if(!empty($stockorder['StockOrder']['number_of_rollis'])): ?>
							Nombre de rollis : <?php echo $stockorder['StockOrder']['number_of_rollis']; ?>
						<?php endif; ?>
					</div>
					<?php endif; ?>
					<div class="col-md-12">
						<h4>Produits</h4>
						<div class="table-responsive">
							<table class="table" id="batches1">
								<thead>
									<tr>
										<th><?php echo __('Code'); ?></th>
										<th><?php echo __('Stock item'); ?></th>
										<th><?php echo __('Delivered quantity'); ?></th>
										<th><?php echo __('Returned quantity'); ?></th>
										<th class="hidden-print"><?php echo __('Reprocessing duration'); ?></th>
										<th><?php echo __('Remarks'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
										<?php if($batch['stock_item_id'] == -1){
											$price = $batch['price'];
											$code = '';
											$name = $batch['name'];
											$id = -1;
										} else {
											$price = $batch['StockItem']['price'];
											$code = $batch['StockItem']['code'];
											$name = $batch['StockItem']['name'];
											$id = $batch['StockItem']['id'];
										} ?>
										<tr id="batch<?php echo $batch['id']; ?>" class="batch <?php echo (!empty($batch['reprocessing_duration']) && $batch['reprocessing_duration'] < 4) ? 'danger':''; ?>">
											<td class="code"><?php echo $code; ?></td>
											<td class="stockitem"><?php echo $name; ?></td>
											<td class="quantity"><?php echo $this->Form->input('StockOrder.delivered_quantity.', array('class' => 'form-control input-xs input-inline input-xsmall delivered_quantity', 'label' => false, 'div' => false, 'type' => 'number', 'value' => $batch['delivered_quantity'], 'readonly' => true, 'tabindex' => 1000 + $k)); ?></td>
											<td class="quantity"><?php echo $this->Form->input('StockOrder.returned_quantity.', array('class' => 'form-control input-xs input-inline input-xsmall returned_quantity checkAvailability', 'label' => false, 'div' => false, 'type' => 'number', 'value' => !is_numeric($batch['returned_quantity']) ? '' : $batch['returned_quantity'], 'required' => false, 'tabindex' => 1000 + $k)); ?></td>
											<td class="hidden-print"><?php echo $this->Form->input('StockOrder.reprocessing_duration.', array('class' => 'form-control input-xsmall input-inline checkAvailability reprocessing_duration', 'label' => false, 'div' => false, 'required' => false, 'type' => 'number', 'value' => empty($batch['reprocessing_duration']) ? 4 : $batch['reprocessing_duration'] )); ?>
												<span class="help-text"><?php echo __('working day(s)');?></span>
											</td>
											<td><?php echo $this->Form->input('StockOrder.delivery_remarks.', array('class' => 'form-control input-large', 'label' => false, 'div' => false, 'required' => false, 'value' => $batch['delivery_remarks'])); ?></td>
											<?php echo $this->Form->input('StockOrder.stock_item_id.', array('type' => 'hidden', 'value' => $id, 'class' => 'stockItemId')); ?>
											<?php echo $this->Form->input('StockOrder.stock_order_batch_id.', array('type' => 'hidden', 'value' => $batch['id'])); ?>
										</tr>
										<tr rel="batch<?php echo $batch['id']; ?>" class="hidden hidden-print">
											<td colspan="7">
												<p class="text-warning hidden infoLine">
													<i class="fa fa-warning"></i> <strong>Attention!</strong> Ce produit n'aura pas de stock disponible du <span class="start-date"></span> au <span class="end-date"></span></p>
												<p class="text-danger hidden infoLine">
													<i class="fa fa-exclamation-triangle"></i>
													<strong>Attention!</strong> Ce produit ne sera pas en quantité suffisante sur ces commandes:
													<ul class="text-danger"></ul>
												</p>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="form-group hidden-print">
							<h4><?php echo __('Extra work hours to invoice'); ?></h4>
							<table class="table" id="extra">
								<thead>
									<tr>
										<th><?php echo __('Duration'); ?></th>
										<th><?php echo __('Task'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php if(!empty($stockorder['StockOrderExtraHour'])): ?>
										<?php foreach($stockorder['StockOrderExtraHour'] as $hour): ?>
											<tr>
												<td style="width: 20%">
													<?php echo $this->Form->input('StockOrderExtraHour.duration.', array('class' => 'form-control input-inline', 'label' => false, 'div' => false, 'type' => 'number', 'value' => $hour['duration'])); ?>
													<span class="help-text">min(s)</span>
												</td>
												<td>
													<?php echo $this->Form->input('StockOrderExtraHour.task.', array('class' => 'form-control', 'label' => false, 'div' => false, 'value' => $hour['task'])); ?>
												</td>
												<?php echo $this->Form->input('StockOrderExtraHour.id.', array('type' => 'hidden', 'value' => $hour['id'])); ?>
											</tr>
										<?php endforeach; ?>
									<?php endif; ?>
									<tr class="empty">
										<td style="width: 20%">
											<?php echo $this->Form->input('StockOrderExtraHour.duration.', array('class' => 'form-control input-inline', 'label' => false, 'div' => false, 'type' => 'number')); ?>
											<span class="help-text">min(s)</span>
										</td>
										<td>
											<?php echo $this->Form->input('StockOrderExtraHour.task.', array('class' => 'form-control', 'label' => false, 'div' => false)); ?>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<a class="btn btn-inverse btn-xs add-line"><i class="fa fa-plus"></i> ajouter une ligne</a>
										</td>
									</tr>
								</tfoot>
							</table>
						</div>
						<div class="form-group">
							<h4><?php echo __('Global remarks'); ?></h4>
							<?php echo $this->Form->input('StockOrder.return_control_remarks', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'required' => false, 'value' => empty($stockorder['StockOrder']['return_control_remarks']) ? '' : $stockorder['StockOrder']['return_control_remarks'] )); ?>
						</div>
						<div class="form-group hidden-print">
							<h4><?php echo __('Person in charge'); ?></h4>
							<?php echo $this->Form->input('StockOrder.resp_id', array('class' => 'form-control bs-select', 'label' => false, 'div' => false, 'options' => $collaborators, 'empty' => true, 'data-live-search' => true)); ?>
						</div>
					</div>
					<div class="col-md-12 hidden-print">
					<br />
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and continue editing'), array('class' => 'btn btn-primary', 'escape' => false, 'type' => 'submit', 'value' => 'depot_return', 'name' => 'data[destination]')); ?>
						<?php echo $this->Form->button('<i class="fa fa-check"></i> ' . __('Save and invoice'), array('class' => 'btn btn-warning save-invoice', 'escape' => false, 'type' => 'submit', 'value' => 'invoice', 'name' => 'data[destination]')); ?>
					</div>
				</div>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php
$this->start('init_scripts');
echo 'Custom.festiloc();';
$this->end();
?>
