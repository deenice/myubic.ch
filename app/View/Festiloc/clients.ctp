<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Clients')); ?>
<?php $civilities = Configure::read('ContactPeople.civilities'); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-wallet"></i><?php echo __('List of all clients'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a client'), array('controller' => 'clients', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('ContactPeoples'); ?>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.clients();';
$this->end();
?>