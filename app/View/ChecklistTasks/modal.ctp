<?php echo $this->Form->create('ChecklistTask', array('class' => 'form-horizontal', 'url' => array('controller' => 'checklist_tasks', 'action' => 'edit', $task['ChecklistTask']['id']))); ?>
<?php echo $this->Form->input('id', array('type' => 'hidden', 'value' => $task['ChecklistTask']['id'])); ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo $task['ChecklistTask']['name']; ?></h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Due date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('due_date', array('label' => false, 'class' => 'form-control date-picker', 'type' => 'text', 'value' => $this->Time->format('d-m-Y', $task['ChecklistTask']['due_date']))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Closed?'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('done', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'value'=> $task['ChecklistTask']['done']));?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <a href="<?php echo Router::url(array('controller' => 'checklist_tasks', 'action' => 'delete', $task['ChecklistTask']['id']));?>" class="btn btn-danger delete-task"><i class="fa fa-trash-o"></i> <?php echo __('Delete'); ?></a>
	<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
</div>

<?php echo $this->Form->end(); ?>
