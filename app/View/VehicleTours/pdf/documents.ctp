<?php $moments = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<style type="text/css">
	.uppercase {
		text-transform: uppercase;
	}
	table thead th {
		vertical-align: top;
	}
	table.totals,
	.totals tr,
	.totals td {
		page-break-inside: avoid !important;
	}
	.break{
	   display: block;
	   clear: both;
	   page-break-after: always;
	}
	h4 {
		font-weight: normal;
		margin: 0;
	}
	h4 strong {
		text-transform: uppercase;
		font-weight: 700;
	}
	/*.maneuver {
		display: inline-block;
	    width: 16px;
	    height: 16px;
	    background-image: url(<?php echo IMAGES . 'maneuvers.png'; ?>);
	}
	.maneuver.ramp-right {
	    background-position: 0 -429px
	}

	.maneuver.roundabout-right {
	    background-position: 0 -232px
	}

	.maneuver.turn-right {
	    background-position: 0 -483px
	}

	.maneuver.turn-sharp-right {
	    background-position: 0 -582px
	}

	.maneuver.turn-slight-right {
	    background-position: 0 -51px
	}

	.maneuver.uturn-right {
	    background-position: 0 -35px
	}*/
</style>
<div class="row">
	<div class="col-xs-12 well">
		<h1 class="uppercase">
			<?php echo __('Tour %s', $data['VehicleTour']['code']); ?>
			<small><?php echo $this->Time->format($data['VehicleTour']['date'], '%d %B %Y'); ?></small>
		</h1>

	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<?php foreach($data['StockOrder'] as $k => $stockorder): ?>
			<div class="portlet">
				<div class="portlet-body">
					<h3>
						#<?php echo $k+1; ?>
						<?php echo $stockorder['order_number']; ?> <?php echo $stockorder['name']; ?>
						<?php if($stockorder['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
							 - <?php echo __('Delivery') ?>
						<?php elseif($stockorder['StockOrdersVehicleTour']['type'] == 'return'): ?>
							 - <?php echo __('Return') ?>
						<?php endif; ?>
					</h3>
					<table class="table table-bordered">
						<tbody>
							<?php if($stockorder['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
							<?php if(!empty($stockorder['delivery_place'])): ?>
							<tr>
								<td style="width: 30%"><?php echo __('Place'); ?></td>
								<td><?php echo $stockorder['delivery_place']; ?></td>
							</tr>
							<?php endif; ?>
							<tr>
								<td style="width: 30%"><?php echo __('Address'); ?></td>
								<td><?php echo $stockorder['delivery_address']; ?>, <?php echo $stockorder['delivery_zip_city']; ?></td>
							</tr>
							<tr>
								<td><?php echo __('Contact person'); ?></td>
								<td><?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?> <?php echo $stockorder['ContactPeopleDelivery']['phone']; ?></td>
							</tr>
							<tr>
								<td><?php echo __('Delivery moment'); ?></td>
								<td>
									<?php if($stockorder['delivery_moment'] == 'hour'): ?>
									<?php echo $this->Time->format($stockorder['delivery_moment_hour'], '%H:%M'); ?>
									<?php else: ?>
									<?php echo __($moments[$stockorder['delivery_moment']]); ?>
									<?php endif; ?>
								</td>
							</tr>
							<?php elseif($stockorder['StockOrdersVehicleTour']['type'] == 'return'): ?>
							<?php if(!empty($stockorder['return_place'])): ?>
							<tr>
								<td style="width: 30%"><?php echo __('Place'); ?></td>
								<td><?php echo $stockorder['return_place']; ?></td>
							</tr>
							<?php endif; ?>
							<tr>
								<td style="width: 30%"><?php echo __('Address'); ?></td>
								<td><?php echo $stockorder['return_address']; ?>, <?php echo $stockorder['return_zip_city']; ?></td>
							</tr>
							<tr>
								<td><?php echo __('Contact person'); ?></td>
								<td><?php echo $stockorder['ContactPeopleReturn']['full_name']; ?> <?php echo $stockorder['ContactPeopleReturn']['phone']; ?></td>
							</tr>
							<tr>
								<td><?php echo __('Return moment'); ?></td>
								<td>
									<?php if($stockorder['return_moment'] == 'hour'): ?>
									<?php echo $this->Time->format($stockorder['return_moment_hour'], '%H:%M'); ?>
									<?php else: ?>
									<?php echo __($moments[$stockorder['return_moment']]); ?>
									<?php endif; ?>
								</td>
							</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<div class="break"></div>
<?php foreach($data['StockOrder'] as $stockorder): ?>
<div class="portlet light" style="padding-top: 0">
	<div class="portlet-body" style="padding-top: 0">
		<div class="invoice">
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>Heures d'ouverture</li>
						<li>8h30 - 11h45 / 13h30 - 16h45</li>
					</ul>
				</div>
			</div>
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6 col-xs-offset-6">
					<ul class="list-unstyled client">
						<li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
						<?php if(!empty($stockorder['invoice_client'])): ?>
						<li><?php echo $stockorder['invoice_client']; ?></li>
						<li><?php echo $stockorder['invoice_contact_person']; ?></li>
						<?php else: ?>
						<?php if($stockorder['ContactPeopleClient']['full_name'] != $stockorder['Client']['name']): ?>
						<li><?php echo $stockorder['Client']['name']; ?></li>
						<?php endif; ?>
						<li>
							<?php echo (!empty($civilities[$stockorder['ContactPeopleClient']['civility']])) ? $civilities[$stockorder['ContactPeopleClient']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
						</li>
						<?php endif; ?>
						<li><?php echo $stockorder['invoice_address']; ?></li>
						<li><?php echo $stockorder['invoice_zip_city']; ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<?php if(!empty($stockorder['name'])): ?><strong>Concerne: <?php echo $stockorder['name']; ?></strong><?php endif; ?>
				</div>
				<div class="col-xs-6">
					Villars-sur-Glâne le <?php echo $this->Time->format($stockorder['modified'], '%e %b %Y'); ?>
					<?php if(!empty($stockorder['import_id'])): ?>
						 / <?php echo $stockorder['import_id']; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<div class="col-xs-6">
					<h4>Livraison</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['delivery']): ?>
								<strong class="uppercase">AVEC LIVRAISON <?php echo (!empty($stockorder['delivery_mode'])) ? 'par ' . __(ucfirst($stockorder['delivery_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong>EMPORTÉ PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['delivery_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['delivery_moment'])): ?>
							(<?php echo strtolower($moments[$stockorder['delivery_moment']]); ?><?php if($stockorder['delivery_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['delivery_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['delivery_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['delivery_place'])): ?>
							<?php echo $stockorder['delivery_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['delivery_address'])): ?>
							<?php echo $stockorder['delivery_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['delivery_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleDelivery']['civility'])) ? $civilities[$stockorder['ContactPeopleDelivery']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['delivery_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['delivery_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php if($stockorder['type'] != 'sale'): ?>
				<div class="col-xs-6">
					<h4>Retour</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['return']): ?>
								<strong class="uppercase">AVEC REPRISE <?php echo (!empty($stockorder['return_mode'])) ? 'par ' . __(ucfirst($stockorder['return_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong class="uppercase">RETOUR PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['return_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['return_moment'])): ?>
							(<?php echo strtolower($moments[$stockorder['return_moment']]); ?><?php if($stockorder['return_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['return_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['return_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['return_place'])): ?>
							<?php echo $stockorder['return_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['return_address'])): ?>
							<?php echo $stockorder['return_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['return_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleReturn']['civility'])) ? $civilities[$stockorder['ContactPeopleReturn']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['return_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['return_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled offer">
						<li>
							<h4><?php echo __('Order'); ?> <?php echo $stockorder['id']; ?> / <?php echo $stockorder['Client']['id']; ?></h4>
						</li>
						<?php if($stockorder['type'] != 'sale'): ?>
						<li>
							<strong>
							<?php if($stockorder['service_date_begin'] == $stockorder['service_date_end'] OR empty($stockorder['service_date_end'])): ?>
							Date de prestation <?php echo $this->Time->format($stockorder['service_date_begin'], '%d.%m.%y'); ?>
							<?php else: ?>
							Date de prestation du <?php echo $this->Time->format($stockorder['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['service_date_end'], '%d.%m.%y'); ?>
							<?php endif; ?>
							</strong>
						</li>
						<?php endif; ?>
					</ul>
					<hr style="margin: 15px 0 0 0">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th style="width: 60%"><?php echo __('Stock item'); ?></th>
							<th class="text-right"><?php echo __('Quantity'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<tr>
								<td style="width: 2%"><?php echo $k + 1; ?></td>
								<td>
									<?php if($batch['stock_item_id'] == -1): ?>
									<?php echo $batch['name']; ?>
									<?php else: ?>
									<?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
									<?php endif; ?>
								</td>
								<td class="text-right"><?php echo $batch['quantity']; ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="break"></div>
<?php endforeach; ?>


<div class="row">
	<div class="col-xs-12">
		<img src="https://maps.googleapis.com/maps/api/staticmap?size=640x400&path=weight:5%7Ccolor:blue%7Cenc:<?php echo $polyline; ?><?php echo $markers; ?>" class="img-responsive" alt="">
	</div>
</div>
<div class="break"></div>

<?php if(!empty($travel)): ?>

<?php foreach($travel['routes'] as $route): ?>
<?php foreach($route['legs'] as $leg): ?>
<h4><strong>Départ</strong> <?php echo $leg['start_address']; ?></h4>
<h4><strong>Arrivée</strong> <?php echo $leg['end_address']; ?></h4>
<h5><?php echo $leg['distance']['text']; ?> - <?php echo $leg['duration']['text']; ?></h5>
<ol>
<?php foreach($leg['steps'] as $step): ?>
<li>
	<?php if(!empty($step['maneuver'])): ?>
		<i class="maneuver <?php echo $step['maneuver']; ?>"></i>
	<?php endif; ?>
	<?php echo $step['html_instructions']; ?> (<?php echo $step['distance']['text'] ?>)
</li>
<?php endforeach; ?>
</ol>
<br>
<?php endforeach; ?>
<?php endforeach; ?>

<?php endif; ?>
