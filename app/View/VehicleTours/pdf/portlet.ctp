<?php $moments1 = Configure::read('StockOrders.delivery_moments') ?>
<?php $deliveryModes = Configure::read('StockOrders.delivery_modes') ?>
<?php $returnModes = Configure::read('StockOrders.return_modes') ?>
<?php $civilities = Configure::read('ContactPeople.civilities') ?>
<style type="text/css">
	.uppercase {
		text-transform: uppercase;
	}
	table thead th {
		vertical-align: top;
	}
	table.totals,
	.totals tr,
	.totals td {
		page-break-inside: avoid !important;
	}
	.break{
	   display: block;
	   clear: both;
	   page-break-after: always;
	}
	h4 {
		font-weight: normal;
		margin: 0;
	}
	h4 strong {
		text-transform: uppercase;
		font-weight: 700;
	}
  ul.list-unstyled {
    margin-top: 8px;
  }
  ul.list-unstyled li {
    padding-left: 5px;
  }
</style>
<div class="row">
	<div class="col-xs-12">
		<h1 class="uppercase">
			<?php echo $this->Time->format($tour['VehicleTour']['date'], '%d %B %Y'); ?><br />
			<small><?php echo __('Tour %s', $tour['VehicleTour']['code']); ?></small>
		</h1>
		<hr>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
			<?php if(!empty($tour['Driver']['full_name'])): ?>
			<span class="uppercase bold">Chauffeur</span>
			<h4 class="infos">
				<?php echo empty($tour['Driver']) ? __('none') : $tour['Driver']['full_name']; ?>
			</h4>
			<br />
			<?php endif; ?>
      <?php if(!empty($tour['VehicleReservationVehicle'])): ?>
			<span class="uppercase bold">Véhicule</span>
			<h4 class="infos">
				<?php echo $tour['VehicleReservationVehicle']['VehicleCompanyModel']['VehicleCompany']['name']; ?> -
				<?php echo $tour['VehicleReservationVehicle']['VehicleCompanyModel']['name']; ?> -
				<?php echo empty($tour['VehicleReservationVehicle']['Vehicle']) ? __('undefined') : $tour['VehicleReservationVehicle']['Vehicle']['name_number']; ?>
			</h4>
			<br />
      <?php endif; ?>
      <?php if(!empty($tour['VehicleReservationTrailer'])): ?>
			<span class="uppercase bold">Remorque</span>
			<h4 class="infos">
				<?php echo $tour['VehicleReservationTrailer']['VehicleCompanyModel']['VehicleCompany']['name']; ?> -
				<?php echo $tour['VehicleReservationTrailer']['VehicleCompanyModel']['name']; ?> -
				<?php echo empty($tour['VehicleReservationTrailer']['Vehicle']) ? __('undefined') : $tour['VehicleReservationTrailer']['Vehicle']['name_number']; ?>
			</h4>
			<br />
      <?php endif; ?>
		<?php if(!empty($tour['VehicleTour']['remarks'])): ?>
		<span class="uppercase bold">Remarques</span>
		<p>
			<?php echo $tour['VehicleTour']['remarks']; ?>
		</p>
		<hr>
		<?php endif; ?>
		<?php if(!empty($deliveries)): ?>
			<p class="uppercase bold"><i class="fa fa-warning"></i> Livraisons de la tournée<br />
			<?php echo implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $deliveries); ?></p>
		<?php endif; ?>
		<br />
		<h3 class="uppercase">Planning</h3>
		<ul class="feeds">
      <?php foreach($moments as $k => $moment): //echo $k; ?>
    		<?php echo !empty($moment) ? $this->element('tour_line', array('type' => $moment['VehicleTourMoment']['type'], 'moment' => $moment, 'tour' => $tour, 'pdf' => true)) : ''; ?>
    	<?php endforeach; ?>
		</ul>
	</div>
</div>
<div class="break"></div>
<?php foreach($tour['StockOrder'] as $stockorder): if($stockorder['StockOrdersVehicleTour']['type'] == 'delivery'){continue;} ?>
<div class="portlet light" style="padding-top: 0">
	<div class="portlet-body" style="padding-top: 0">
		<div class="invoice">
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Route du Petit-Moncor 1c</li>
						<li>1752 Villars-sur-Glâne</li>
						<li>T + 41 26 676 01 17</li>
						<li>F + 41 26 676 01 19</li>
						<li>info@festiloc.ch</li>
					</ul>
				</div>
				<div class="col-xs-3 address">
					<ul class="list-unstyled">
						<li>Adresse du dépôt</li>
						<li>Route du Tir-Fédéral 10<br />1762 Givisiez</li>
						<li>Heures d'ouverture</li>
						<li>8h30 - 11h45 / 13h30 - 16h45</li>
					</ul>
				</div>
			</div>
			<div class="row" style="margin-bottom: 30px">
				<div class="col-xs-6">
					<div class="" style="border: 1px solid #999; height: 80px; padding: 10px">
						<strong>Visa client</strong>
					</div>
				</div>
				<div class="col-xs-6 col-xs-offset-61">
					<ul class="list-unstyled client">
						<li class="festiloc">Festiloc Sàrl - Rte du Petit-Moncor 1c - 1752 Villars-sur-Glâne</li>
						<?php if(!empty($stockorder['invoice_client'])): ?>
						<li><?php echo $stockorder['invoice_client']; ?></li>
						<li><?php echo $stockorder['invoice_contact_person']; ?></li>
						<?php else: ?>
						<?php if($stockorder['ContactPeopleClient']['full_name'] != $stockorder['Client']['name']): ?>
						<li><?php echo $stockorder['Client']['name']; ?></li>
						<?php endif; ?>
						<li>
							<?php echo (!empty($civilities[$stockorder['ContactPeopleClient']['civility']])) ? $civilities[$stockorder['ContactPeopleClient']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleClient']['full_name']; ?>
						</li>
						<?php endif; ?>
						<li><?php echo $stockorder['invoice_address']; ?></li>
						<li><?php echo $stockorder['invoice_zip_city']; ?></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-6">
					<?php if(!empty($stockorder['name'])): ?><strong>Concerne: <?php echo $stockorder['name']; ?></strong><?php endif; ?>
				</div>
				<div class="col-xs-6">
					Villars-sur-Glâne le <?php echo $this->Time->format($stockorder['modified'], '%e %b %Y'); ?>
					<?php if(!empty($stockorder['import_id'])): ?>
						 / <?php echo $stockorder['import_id']; ?>
					<?php endif; ?>
				</div>
			</div>
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<div class="col-xs-6">
					<h4>Livraison</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['delivery']): ?>
								<strong class="uppercase">AVEC LIVRAISON <?php echo (!empty($stockorder['delivery_mode'])) ? 'par ' . __(ucfirst($stockorder['delivery_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong>EMPORTÉ PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['delivery_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['delivery_moment'])): ?>
							(<?php echo strtolower($moments1[$stockorder['delivery_moment']]); ?><?php if($stockorder['delivery_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['delivery_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['delivery_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['delivery_place'])): ?>
							<?php echo $stockorder['delivery_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['delivery_address'])): ?>
							<?php echo $stockorder['delivery_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['delivery_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleDelivery']['civility'])) ? $civilities[$stockorder['ContactPeopleDelivery']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleDelivery']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['delivery_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['delivery_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php if($stockorder['type'] != 'sale'): ?>
				<div class="col-xs-6">
					<h4>Retour</h4>
					<ul class="list-unstyled">
						<li>
							<?php if($stockorder['return']): ?>
								<strong class="uppercase">AVEC REPRISE <?php echo (!empty($stockorder['return_mode'])) ? 'par ' . __(ucfirst($stockorder['return_mode'])) : ''; ?></strong>
							<?php else: ?>
								<strong class="uppercase">RETOUR PAR LE CLIENT</strong>
							<?php endif; ?>
						</li>
						<li>
							<strong>Date</strong>
							<?php echo $this->Time->format($stockorder['return_date'], '%A %d %B %Y'); ?>
							<?php if(!empty($stockorder['return_moment'])): ?>
							(<?php echo strtolower($moments1[$stockorder['return_moment']]); ?><?php if($stockorder['return_moment'] == 'hour') echo ' ' . $this->Time->format($stockorder['return_moment_hour'], '%H:%M'); ?>)
							<?php endif; ?>
						</li>
						<?php if(!empty($stockorder['return_zip_city'])): ?>
						<li>
							<strong>Adresse</strong><br />
							<?php if(!empty($stockorder['return_place'])): ?>
							<?php echo $stockorder['return_place'] ?><br />
							<?php endif; ?>
							<?php if(!empty($stockorder['return_address'])): ?>
							<?php echo $stockorder['return_address'] ?><br />
							<?php endif; ?>
							<?php echo $stockorder['return_zip_city'] ?>
						</li>
						<?php endif; ?>
						<li>
							<strong>Personne de contact</strong><br />
							<?php echo (!empty($stockorder['ContactPeopleReturn']['civility'])) ? $civilities[$stockorder['ContactPeopleReturn']['civility']] : ''; ?>
							<?php echo $stockorder['ContactPeopleReturn']['full_name']; ?>
							<?php echo $stockorder['ContactPeopleReturn']['phone']; ?>
						</li>
						<?php if(!empty($stockorder['return_remarks'])): ?>
						<li>
							<strong>Remarques</strong><br />
							<?php echo $stockorder['return_remarks']; ?>
						</li>
						<?php endif; ?>
					</ul>
				</div>
				<?php endif; ?>
        <div class="col-xs-12">
          <h4>Conditionnement</h4>
          <ul class="list-unstyled">
            <?php if(!empty($stockorder['packaging']['pallets'])): ?><li><?php echo __('%s pallet(s)', $stockorder['packaging']['pallets']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['pallets_xl'])): ?><li><?php echo __('%s pallet(s) XL', $stockorder['packaging']['pallets_xl']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['rollis'])): ?><li><?php echo __('%s rollis(s)', $stockorder['packaging']['rollis']); ?></li><?php endif; ?>
            <?php if(!empty($stockorder['packaging']['rollis_xl'])): ?><li><?php echo __('%s rollis(s) XL', $stockorder['packaging']['rollis_xl']); ?></li><?php endif; ?>
          </ul>
        </div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<ul class="list-unstyled offer">
						<li>
							<h4><?php echo __('Order'); ?> <?php echo $stockorder['id']; ?> / <?php echo $stockorder['Client']['id']; ?></h4>
						</li>
						<?php if($stockorder['type'] != 'sale'): ?>
						<li>
							<strong>
							<?php if($stockorder['service_date_begin'] == $stockorder['service_date_end'] OR empty($stockorder['service_date_end'])): ?>
							Date de prestation <?php echo $this->Time->format($stockorder['service_date_begin'], '%d.%m.%y'); ?>
							<?php else: ?>
							Date de prestation du <?php echo $this->Time->format($stockorder['service_date_begin'], '%d.%m.%y'); ?> au <?php echo $this->Time->format($stockorder['service_date_end'], '%d.%m.%y'); ?>
							<?php endif; ?>
							</strong>
						</li>
						<?php endif; ?>
					</ul>
					<hr style="margin: 15px 0 0 0">
					<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th style="width: 60%"><?php echo __('Stock item'); ?></th>
							<th class="text-right"><?php echo __('Ordered quantity'); ?></th>
							<th class="text-right"><?php echo __('Delivered quantity'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($stockorder['StockOrderBatch'] as $k => $batch): ?>
							<tr>
								<td style="width: 2%"><?php echo $k + 1; ?></td>
								<td>
									<?php if($batch['stock_item_id'] == -1): ?>
									<?php echo $batch['name']; ?>
									<?php else: ?>
									<?php echo $batch['StockItem']['code'] . ' - ' . $batch['StockItem']['name']; ?>
									<?php endif; ?>
								</td>
								<td class="text-right"><?php echo $batch['quantity']; ?></td>
								<td class="text-right"><?php echo !is_null($batch['delivered_quantity']) ? $batch['delivered_quantity'] : $batch['quantity']; ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="break"></div>
<?php endforeach; ?>


<div class="row">
	<div class="col-xs-12">
		<img src="https://maps.googleapis.com/maps/api/staticmap?size=640x400&path=weight:5%7Ccolor:blue%7Cenc:<?php echo $polyline; ?><?php echo $markers; ?>" class="img-responsive" alt="">
	</div>
</div>
<div class="break"></div>

<?php if(!empty($travel)): ?>

<?php foreach($travel['routes'] as $route): ?>
<?php foreach($route['legs'] as $leg): ?>
<h4><strong>Départ</strong> <?php echo $leg['start_address']; ?></h4>
<h4><strong>Arrivée</strong> <?php echo $leg['end_address']; ?></h4>
<h5><?php echo $leg['distance']['text']; ?> - <?php echo $leg['duration']['text']; ?></h5>
<ol>
<?php foreach($leg['steps'] as $step): ?>
<li>
	<?php if(!empty($step['maneuver'])): ?>
		<i class="maneuver <?php echo $step['maneuver']; ?>"></i>
	<?php endif; ?>
	<?php echo $step['html_instructions']; ?> (<?php echo $step['distance']['text'] ?>)
</li>
<?php endforeach; ?>
</ol>
<br>
<?php endforeach; ?>
<?php endforeach; ?>

<?php endif; ?>
