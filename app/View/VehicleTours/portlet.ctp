<p class="text-muted hidden">
	<strong><?php echo __('Trailer'); ?></strong> <?php //echo $tour['VehicleReservation'][0]['VehicleCompany']['name']; ?>
	<strong><?php echo __('Driver'); ?></strong> <?php echo $tour['Driver']['full_name']; ?>
</p>
<table class="table tour-infos">
	<tbody>
			<?php if(!empty($tour['VehicleReservationVehicle'])): ?>
				<tr>
					<td><strong><?php echo __('Vehicle'); ?></strong></td>
					<td>
						<?php echo $tour['VehicleReservationVehicle']['VehicleCompanyModel']['VehicleCompany']['name']; ?> -
						<?php echo $tour['VehicleReservationVehicle']['VehicleCompanyModel']['name']; ?> -
						<?php echo empty($tour['VehicleReservationVehicle']['Vehicle']) ? __('undefined') : $tour['VehicleReservationVehicle']['Vehicle']['name_number']; ?>
						<a href="#" class="pull-right font-red remove-vehicle-reservation" data-vehicle-tour-id="<?php echo $tour['VehicleTour']['id']; ?>" data-vehicle-reservation-id="<?php echo $tour['VehicleReservationVehicle']['id']; ?>"><i class="fa fa-times"></i></a>
					</td>
				</tr>
			<?php endif; ?>
			<?php if(!empty($tour['VehicleReservationTrailer'])): ?>
			<tr>
				<td><strong><?php echo __('Trailer'); ?></strong></td>
				<td>
					<?php echo $tour['VehicleReservationTrailer']['VehicleCompanyModel']['VehicleCompany']['name']; ?> -
					<?php echo $tour['VehicleReservationTrailer']['VehicleCompanyModel']['name']; ?> -
					<?php echo empty($tour['VehicleReservationTrailer']['Vehicle']) ? __('undefined') : $tour['VehicleReservationTrailer']['Vehicle']['name_number']; ?>
					<a href="#" class="pull-right font-red remove-vehicle-reservation" data-vehicle-tour-id="<?php echo $tour['VehicleTour']['id']; ?>" data-vehicle-reservation-id="<?php echo $tour['VehicleReservationTrailer']['id']; ?>"><i class="fa fa-times"></i></a>
				</td>
			</tr>
			<?php endif; ?>
			<?php if(!empty($tour['Driver']['id'])):?>
			<tr>
				<td><strong><?php echo __('Driver'); ?></strong></td>
				<td><?php echo $tour['Driver']['full_name']; ?></td>
			</tr>
			<?php endif; ?>
	</tbody>
</table>
<hr>
<ul class="feeds" id="feeds<?php echo $tour['VehicleTour']['id']; ?>" data-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">

	<?php foreach($moments as $k => $moment): //echo $k; ?>
		<?php echo !empty($moment) ? $this->element('tour_line', array('type' => $moment['VehicleTourMoment']['type'], 'moment' => $moment, 'tour' => $tour, 'pdf' => false)) : ''; ?>
	<?php endforeach; ?>

</ul>
<div class="tour-remarks">
	<div class="form-group">
		<label for="" class="control-label"></label>
		<?php echo $this->Form->input('VehicleTour.remarks', array('id' => '', 'class' => 'form-control remarks', 'rows' => 2, 'data-tour-id' => $tour['VehicleTour']['id'], 'value' => empty($tour['VehicleTour']['remarks']) ? '' : $tour['VehicleTour']['remarks'])); ?>
	</div>

</div>
<div class="modal fade assign_driver" id="assignDriver<?php echo $tour['VehicleTour']['id']; ?>" tabindex="-1" aria-hidden="true" data-vehicle-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">
	<?php echo $this->Form->create('VehicleTour', array('controller' => 'vehicle_tours', 'action' => 'assign_driver')); ?>
	<?php echo $this->Form->input('VehicleTour.id', array('type' => 'hidden', 'value' => $tour['VehicleTour']['id'])); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Assign driver'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Driver'); ?></label>
							<?php echo $this->Form->input('VehicleTour.driver_id', array('class' => 'form-control bs-select', 'data-live-search' => true, 'label' => false, 'options' => $drivers, 'value' => empty($tour['VehicleTour']['driver_id']) ? '' : $tour['VehicleTour']['driver_id'])); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade assign_vehicle" id="assignVehicle<?php echo $tour['VehicleTour']['id']; ?>" tabindex="-1" aria-hidden="true" data-vehicle-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">
	<?php echo $this->Form->create('VehicleReservation', array('controller' => 'vehicle_tours', 'action' => 'assign_vehicle', 'id' => 'VehicleReservationVehicle')); ?>
	<?php echo $this->Form->input('VehicleReservation.id', array('type' => 'hidden', 'class' => 'vehicle_reservation_id', 'value' => empty($tour['VehicleReservationVehicle']) ? '' : $tour['VehicleReservationVehicle']['id'])); ?>
	<?php echo $this->Form->input('VehicleTour.id', array('type' => 'hidden', 'value' => $tour['VehicleTour']['id'])); ?>
	<?php echo $this->Form->input('VehicleReservation.start_place', array('type' => 'hidden', 'value' => $tour['VehicleTour']['start_location'], 'class' => 'start_place')); ?>
	<?php echo $this->Form->input('VehicleReservation.end_place', array('type' => 'hidden', 'value' => $tour['VehicleTour']['end_location'], 'class' => 'end_place')); ?>
	<?php echo $this->Form->input('VehicleReservation.confirmed', array('type' => 'hidden', 'value' => empty($tour['VehicleReservation']['confirmed']) ? 0 : 1, 'class' => 'confirmed')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Assign vehicle'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.company_id', array('id' => 'companies_' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select companies', 'label' => false, 'options' => $companies, 'value' => empty($tour['VehicleReservationVehicle']) ? '' : $tour['VehicleReservationVehicle']['VehicleCompanyModel']['VehicleCompany']['id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Vehicle model'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.vehicle_model_id', array('id' => 'models_' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select models', 'label' => false, 'options' => array(), 'data-vehicles-list' => 'vehicles_' . $tour['VehicleTour']['id'], 'data-default' => empty($tour['VehicleReservationVehicle']) ? '' : $tour['VehicleReservationVehicle']['vehicle_model_id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Vehicle'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.vehicle_id', array('id' => 'vehicles_' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select vehicles', 'label' => false, 'options' => array(), 'empty' => true, 'data-default' => empty($tour['VehicleReservationVehicle']) ? '' : $tour['VehicleReservationVehicle']['vehicle_id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Start time'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.start', array('class' => 'form-control datetime-picker start', 'label' => false, 'type' => 'text', 'value' => $start_date_reservation)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('End time'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.end', array('class' => 'form-control datetime-picker end', 'label' => false, 'type' => 'text', 'value' => $end_date_reservation)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="button" class="btn btn-danger remove-vehicle-reservation hidden"><?php echo __('Remove'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade assign_trailer" id="assignTrailer<?php echo $tour['VehicleTour']['id']; ?>" tabindex="-1" aria-hidden="true" data-vehicle-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">
	<?php echo $this->Form->create('VehicleReservation', array('controller' => 'vehicle_tours', 'action' => 'assign_vehicle', 'id' => 'VehicleReservationTrailer')); ?>
	<?php echo $this->Form->input('VehicleReservation.id', array('type' => 'hidden', 'class' => 'vehicle_reservation_id', 'value' => empty($tour['VehicleReservationTrailer']) ? '' : $tour['VehicleReservationTrailer']['id'])); ?>
	<?php echo $this->Form->input('VehicleTour.id', array('type' => 'hidden', 'value' => $tour['VehicleTour']['id'])); ?>
	<?php echo $this->Form->input('VehicleReservation.start_place', array('type' => 'hidden', 'value' => $tour['VehicleTour']['start_location'], 'class' => 'start_place')); ?>
	<?php echo $this->Form->input('VehicleReservation.end_place', array('type' => 'hidden', 'value' => $tour['VehicleTour']['end_location'], 'class' => 'end_place')); ?>
	<?php echo $this->Form->input('VehicleReservation.confirmed', array('type' => 'hidden', 'value' => empty($tour['VehicleReservation']['confirmed']) ? 0 : 1, 'class' => 'confirmed')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Assign trailer'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Company'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.company_id', array('id' => 'companies' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select companies', 'label' => false, 'options' => $companies, 'value' => empty($tour['VehicleReservationTrailer']) ? '' : $tour['VehicleReservationTrailer']['VehicleCompanyModel']['VehicleCompany']['id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Vehicle model'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.vehicle_model_id', array('id' => 'models' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select models', 'label' => false, 'options' => array(), 'data-vehicles-list' => 'vehicles' . $tour['VehicleTour']['id'], 'data-default' => empty($tour['VehicleReservationTrailer']) ? '' : $tour['VehicleReservationTrailer']['vehicle_model_id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Vehicle'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.vehicle_id', array('id' => 'vehicles' . $tour['VehicleTour']['id'], 'class' => 'form-control bs-select vehicles', 'label' => false, 'options' => array(), 'empty' => true, 'data-default' => empty($tour['VehicleReservationTrailer']) ? '' : $tour['VehicleReservationTrailer']['vehicle_id'])); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Start time'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.start', array('class' => 'form-control datetime-picker start', 'label' => false, 'type' => 'text', 'value' => $start_date_reservation)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('End time'); ?></label>
							<?php echo $this->Form->input('VehicleReservation.end', array('class' => 'form-control datetime-picker end', 'label' => false, 'type' => 'text', 'value' => $end_date_reservation)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="button" class="btn btn-danger remove-vehicle-reservation hidden"><?php echo __('Remove'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
