<?php $locations = Configure::read('VehicleTours.locations'); ?>
<?php $this->assign('page_title', __('Vehicle tours')); ?>
<?php $this->assign('page_subtitle', __('Planification')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-clock-o"></i><?php echo __('Tours planification of %s', $date); ?>
		</div>
		<div class="actions">
			<div class="btn-group">
				<?php echo $this->Html->link('<i class="fa fa-calendar"></i> ' . __('Back to week planification'), array('controller' => 'stock_orders', 'action' => 'prepareWeek'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
			</div>
			<?php //echo $this->Html->link('<i class="fa fa-angle-double-left"></i> ' . __('Previous week'), array('controller' => 'vehicles', 'action' => 'plan', $company_id, $previousWeek), array('class' => 'btn default', 'escape' => false)); ?>
			<?php //echo $this->Html->link(__('Next week') . ' <i class="fa fa-angle-double-right"></i>', array('controller' => 'vehicles', 'action' => 'plan', $company_id, $nextWeek), array('class' => 'btn default', 'escape' => false)); ?>
			<div class="btn-group">
				<a class="btn disabled" href="">Afficher</a>
				<a class="btn default display-columns" href="#" data-columns="1">1</a>
				<a class="btn default display-columns active" href="#" data-columns="2">2</a>
				<a class="btn default display-columns" href="#" data-columns="3">3</a>
				<a class="btn disabled" href="">colonne(s)</a>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row" id="tours">
		<?php foreach($tours as $k => $tour): if(empty($tour['VehicleTour']['code'])){continue;} ?>
		<div class="col-md-6 tour">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<span class="uppercase bold"><?php echo __('Tour %s', $tour['VehicleTour']['code']); ?></span>
					</div>
					<div class="actions">
						<div class="btn-group">
							<a href="javascript:;" class="btn btn-sm default" data-toggle="dropdown">Actions <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#" class="add-line" data-type="break" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"> <i class="fa fa-coffee"></i> <?php echo __('Add a break'); ?></a></li>
								<li><a href="#" class="add-line" data-type="loading" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-exchange"></i> <?php echo __('Add loading'); ?></a></li>
								<li><a href="#" class="add-line" data-type="unloading" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-exchange"></i> <?php echo __('Add unloading'); ?></a></li>
								<li><a href="#" class="add-line" data-type="free" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-cog"></i> <?php echo __('Add custom field'); ?></a></li>
								<li class="divider"></li>
								<li><a href="#assignVehicle<?php echo $tour['VehicleTour']['id']; ?>" data-toggle="modal" class="assign-vehicle" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-truck"></i> <?php echo empty($tour['VehicleReservationVehicle']) ? __('Assign vehicle') : __('Change vehicle'); ?></a></li>
								<li><a href="#assignTrailer<?php echo $tour['VehicleTour']['id']; ?>" data-toggle="modal" class="assign-trailer" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-truck"></i> <?php echo empty($tour['VehicleReservationTrailer']) ? __('Assign trailer') : __('Change trailer'); ?></a></li>
								<li><a href="#assignDriver<?php echo $tour['VehicleTour']['id']; ?>" data-toggle="modal" class="assign-driver" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-user"></i> <?php echo empty($tour['Driver']['id']) ? __('Assign driver') : __('Change driver'); ?></a></li>
								<?php if(!empty($tour['Driver']['id'])): ?>
								<li>
									<?php echo $this->Html->link('<i class="fa fa-envelope-o"></i> ' . __('Notify driver'), array('controller' => 'vehicle_tours', 'action' => 'notify_driver', $tour['VehicleTour']['id']), array('escape' => false, 'class' => 'notify_driver')); ?>
								<?php endif; ?>
								<li class="divider"></li>
								<li><?php echo $this->Html->link('<i class="fa fa-files-o"></i>' . __('Documents for driver'), array('controller' => 'vehicle_tours', 'action' => 'portlet', $tour['VehicleTour']['id'] . '.pdf', '?' => 'download=0'), array('escape' => false, 'target' => '_blank')); ?></li>
								<li><?php echo $this->Html->link('<i class="fa fa-map-marker"></i> ' . __('View route on Google Maps'), array('controller' => 'vehicle_tours', 'action' => 'generateRouteUrl', $tour['VehicleTour']['id']), array('escape' => false, 'target' => '_blank')); ?></li>
							</ul>
						</div>
					</div>
					<div class="tools hidden">
						<a href="javascript:;" data-load="true" data-url="<?php echo Router::url(array('controller' => 'vehicle_tours', 'action' => 'portlet', $tour['VehicleTour']['id'])); ?>" class="reload"> </a>
					</div>
				</div>
				<div class="portlet-body portlet-empty"></div>
			</div>
		</div>
		<?php endforeach; ?>
		</div>
	</div>
</div>
<div class="modal fade" id="startLocation" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('VehicleTour', array('controller' => 'vehicle_tours', 'action' => 'edit')); ?>
	<?php echo $this->Form->input('VehicleTour.id', array('type' => 'hidden', 'class' => 'vehicle_tour_id')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Edit location'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Start location'); ?></label>
							<?php echo $this->Form->input('VehicleTour.start_location', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('VehicleTours.locations'), 'required' => true)); ?>
						</div>
						<div class="hidden other">
							<div class="form-group">
								<label class="control-label"><?php echo __('Place'); ?></label>
								<?php echo $this->Form->input('VehicleTour.start_location_custom', array('class' => 'form-control', 'label' => false)); ?>
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo __('Address'); ?></label>
								<?php echo $this->Form->input('VehicleTour.start_address', array('class' => 'form-control', 'label' => false, 'value' => 'Route du Tir Fédéral 10, 1762 Givisiez', 'placeholder' => 'Nom de la rue, NPA Localité')); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Start time'); ?></label>
							<?php echo $this->Form->input('VehicleTour.start', array('class' => 'form-control timepicker timepicker-24', 'label' => false, 'type' => 'text', )); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="endLocation" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('VehicleTour', array('controller' => 'vehicle_tours', 'action' => 'edit')); ?>
	<?php echo $this->Form->input('VehicleTour.id', array('type' => 'hidden', 'class' => 'vehicle_tour_id')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Edit location'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('End location'); ?></label>
							<?php echo $this->Form->input('VehicleTour.end_location', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('VehicleTours.locations'), 'required' => true)); ?>
						</div>
						<div class="hidden other">
							<div class="form-group">
								<label class="control-label"><?php echo __('Place'); ?></label>
								<?php echo $this->Form->input('VehicleTour.end_location_custom', array('class' => 'form-control', 'label' => false)); ?>
							</div>
							<div class="form-group">
								<label class="control-label"><?php echo __('Address'); ?></label>
								<?php echo $this->Form->input('VehicleTour.end_address', array('class' => 'form-control', 'label' => false, 'value' => 'Route du Tir Fédéral 10, 1762 Givisiez', 'placeholder' => 'Nom de la rue, NPA Localité')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="addVehicleTourMoment" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('VehicleTourMoment', array('controller' => 'vehicle_tour_moments', 'action' => 'add')); ?>
	<?php echo $this->Form->input('VehicleTourMoment.vehicle_tour_id', array('type' => 'hidden')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a moment'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Type'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.type', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('VehicleTourMoments.types'))); ?>
						</div>
						<div class="form-group moment-description">
							<label class="control-label"><?php echo __('Description'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.description', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Duration'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.duration', array('class' => 'form-control', 'label' => false, 'type' => 'text', 'placeholder' => __('in minutes'), 'required' => true)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="editVehicleTourMoment" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('VehicleTourMoment', array('controller' => 'vehicle_tour_moments', 'action' => 'edit')); ?>
	<?php echo $this->Form->input('VehicleTourMoment.id', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('VehicleTourMoment.vehicle_tour_id1', array('type' => 'hidden')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Edit a moment'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('Type'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.type', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('VehicleTourMoments.types'))); ?>
						</div>
						<div class="form-group moment-description">
							<label class="control-label"><?php echo __('Description'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.description', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Duration'); ?></label>
							<?php echo $this->Form->input('VehicleTourMoment.duration', array('class' => 'form-control', 'label' => false, 'type' => 'text', 'placeholder' => __('in minutes'), 'required' => true)); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<a class="btn btn-danger delete-moment" href="#" data-vehicle-tour-moment-id="" data-vehicle-tour-id=""><?php echo __('Delete'); ?></a>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
<?php echo $this->element('tour_line', array('type' => 'break', 'empty' => true, 'pdf' => false)); ?>
<?php echo $this->element('tour_line', array('type' => 'loading', 'empty' => true, 'pdf' => false)); ?>
<?php
$this->start('init_scripts');
echo 'Custom.vehicle_tours();';
$this->end();
?>
