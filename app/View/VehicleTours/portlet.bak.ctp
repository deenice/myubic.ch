<ul class="feeds" id="feeds<?php echo $tour['VehicleTour']['id']; ?>" data-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">

	<?php foreach($moments as $k => $moment): ?>
		<?php echo !empty($moment) ? $this->element('tour_line', array('type' => $moment['VehicleTourMoment']['type'])) : ''; ?>
	<?php endforeach; ?>

	<?php if(1==2): ?>
	<li class="start">
		<div class="col1">
			<div class="cont">
				<div class="cont-col1">
					<a class="label label-default label-sm start-point" href="#" data-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">
						<i class="fa fa-home"></i>
					</a>
				</div>
				<div class="cont-col2">
					<div class="desc">
						<span class="uppercase bold"><?php echo __('Start point'); ?></span>
						<?php if(!empty($tour['VehicleTour']['start_location'])): ?>
							<?php echo $locations[$tour['VehicleTour']['start_location']]; ?>
						<?php elseif(!empty($tour['VehicleTour']['start_location_custom'])): ?>
							<?php echo $tour['VehicleTour']['start_location_custom']; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col2">
			<div class="date">
				<?php if(!empty($tour['VehicleTour']['start'])): ?>
					<?php echo $this->Time->format($tour['VehicleTour']['start'], '%H:%M'); ?>
				<?php else: ?>
					00:00
				<?php endif; ?>
			</div>
		</div>
		<?php echo $this->Form->input('address', array('id' => '', 'class' => 'address', 'type' => 'hidden', 'value' => $tour['VehicleTour']['start_address'])); ?>
	</li>
	<?php foreach($tour['StockOrder'] as $k => $order): //debug($order); ?>
	<?php //echo $k==0 ? $this->element('tour_line', array('type' => 'route')) : ''; ?>
	<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
	<li class="sortable order delivery" data-stockorder-id="<?php echo $order['id']; ?>">
	<?php elseif($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
	<li class="sortable order return" data-stockorder-id="<?php echo $order['id']; ?>">
	<?php endif; ?>
		<div class="col1">
			<div class="cont">
				<div class="cont-col1">
					<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
					<div class="label label-sm label-primary">
						<i class="fa fa-arrow-right"></i>
					</div>
					<?php elseif($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
					<div class="label label-sm label-info">
						<i class="fa fa-arrow-left"></i>
					</div>
					<?php endif; ?>
				</div>
				<div class="cont-col2">
					<div class="desc">
						<span class="uppercase bold">
						<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
							<?php echo __('Delivery'); ?>
						<?php elseif($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
							<?php echo __('Return'); ?>
						<?php endif; ?>
						</span>
						<?php echo $order['order_number']; ?> 
						<?php echo $order['name']; ?> | 
						<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
							<?php echo $order['delivery_address']; ?>, 
							<?php echo $order['delivery_zip_city']; ?>
						<?php elseif($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
							<?php echo $order['return_address']; ?>, 
							<?php echo $order['return_zip_city']; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col2">
			<div class="date"></div>
		</div>
		<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
		<?php echo $this->Form->input('address', array('id' => '', 'class' => 'address', 'type' => 'hidden', 'value' => $order['delivery_address'] . ', ' . $order['delivery_zip_city'])); ?>
		<?php elseif($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
		<?php echo $this->Form->input('address', array('id' => '', 'class' => 'address', 'type' => 'hidden', 'value' => $order['return_address'] . ', ' . $order['return_zip_city'])); ?>
		<?php endif; ?>
	</li>
	<?php //echo $this->element('tour_line', array('type' => 'route')); ?>
	<?php endforeach; ?>
	<?php //echo $this->element('tour_line', array('type' => 'break')); ?>
	<?php //echo $this->element('tour_line', array('type' => 'loading')); ?>
	<li class="end">
		<div class="col1">
			<div class="cont">
				<div class="cont-col1">
					<a class="label label-default label-sm end-point" href="#" data-tour-id="<?php echo $tour['VehicleTour']['id']; ?>">
						<i class="fa fa-home"></i>
					</a>
				</div>
				<div class="cont-col2">
					<div class="desc">
						<span class="uppercase bold"><?php echo __('End point'); ?></span>
						<?php if(!empty($tour['VehicleTour']['end_location'])): ?>
							<?php echo $locations[$tour['VehicleTour']['end_location']]; ?>
						<?php elseif(!empty($tour['VehicleTour']['end_location_custom'])): ?>
							<?php echo $tour['VehicleTour']['end_location_custom']; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col2">
			<div class="date"></div>
		</div>
		<?php echo $this->Form->input('address', array('id' => '', 'class' => 'address', 'type' => 'hidden', 'value' => $tour['VehicleTour']['end_address'])); ?>
	</li>
	<?php endif; ?>
</ul>
<hr>
Ajouter <a href="" class="btn btn-danger btn-xs add-line" data-type="break" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-coffee"></i></a>
<a href="" class="btn btn-success btn-xs add-line" data-type="loading" data-tour="<?php echo $tour['VehicleTour']['id']; ?>"><i class="fa fa-exchange"></i></a>