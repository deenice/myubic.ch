<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Add an unavailability'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-basket font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an unavailability'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockItemUnavailability', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Stock item'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('StockItemUnavailability.stock_item_id', array('label' => false, 'class' => 'form-control', 'type' => 'text'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockItemUnavailability.statuses')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Quantity'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('quantity', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'type' => 'number'));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockitemunavailabilities();';
$this->end();
?>