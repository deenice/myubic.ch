<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Edit an unavailability'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-basket font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Edit an unavailability'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockItemUnavailability', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Stock item'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stockitem', array('label' => false, 'class' => 'form-control', 'readonly' => true, 'value' => $stockitem['StockItem']['code_name']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockItemUnavailability.statuses')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('remarks', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Quantity'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('quantity', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'type' => 'number'));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete')), array('controller' => 'stock_item_unavailabilities', 'action' => 'delete', $this->request->data['StockItemUnavailability']['id']), array('class' => 'btn btn-danger delete-unavailability', 'escape' => false)); ?>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockitemunavailabilities();';
echo 'Custom.stockitem();';
$this->end();
?>
