<?php $this->assign('page_title', $this->Html->link(__('Orders'), array('controller' => 'orders', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Associations'));?>

<div class="portlet light bordered">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-clipboard font-blue-madison"></i>
      <span class="caption-subject font-blue-madison bold uppercase"> <?php echo __('Associations'); ?></span>
    </div>
  </div>
  <div class="portlet-body">
    <div class="row">
      <div class="col-md-8">
        <table class="table table-striped table-condensed table-associations" data-table-orders-associations>
          <thead>
            <tr>
              <th class="order"><?php echo __('Order model'); ?></th>
              <th class="companies">
                <?php echo __('Filter by company'); ?>
                <a href="javascript:;" class="btn btn-xs btn-company btn-company-all" data-company-id=""></a>
                <?php foreach($companies as $company): ?>
                  <a href="javascript:;" class="btn btn-xs btn-company btn-company-<?php echo $company['Company']['class']; ?>" data-company-id="<?php echo $company['Company']['id']; ?>"></a>
                <?php endforeach; ?>
              </th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($orders as $order): ?>
            <tr data-company-id="<?php echo $order['Order']['company_id']; ?>" data-order-id="<?php echo $order['Order']['id']; ?>">
              <td class="name">
                <span class="btn btn-xs btn-company btn-company-<?php echo $order['Company']['class']; ?>"></span>
                <?php echo $order['Order']['name']; ?>
              </td>
              <td class="resources">
                <?php if(!empty($order['Activity'])): ?>
                  <?php foreach($order['Activity'] as $activity): ?>
                    <span class="btn btn-xs default btn--activity uppercase" data-model="activity" data-model-id="<?php echo $activity['id']; ?>"> <?php echo empty($activity['slug']) ? $activity['name'] : $activity['slug']; ?> </span>
                  <?php endforeach; ?>
                <?php endif; ?>
                <?php if(!empty($order['FBModule'])): ?>
                  <?php foreach($order['FBModule'] as $module): ?>
                    <span class="btn btn-xs default btn--fb-module uppercase" data-model="fb_module" data-model-id="<?php echo $module['id']; ?>"> <?php echo empty($module['slug']) ? $module['name'] : $module['slug']; ?> </span>
                  <?php endforeach; ?>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <div class="col-md-4 orders-resources">
        <table class="table">
          <thead>
            <tr>
              <th><?php echo __('Resources'); ?></th>
            </tr>
          </thead>
        </table>
        <div class="list--wrapper">
          <div class="list">
            <div class="row input">
              <div class="form-group">
                <label class="control-label col-md-4"><?php echo __('Filter by name'); ?></label>
                <div class="col-md-8">
                  <input type="text" name="name" value="" data-search-orders-resources class="form-control">
                </div>
              </div>
            </div>
            <?php foreach($resources as $model => $items): ?>
              <strong style="display: block"><?php echo $model; ?></strong>
              <?php foreach($items as $id => $item): ?>
                <?php if($model == 'Activity'): ?>
                  <span class="btn btn-xs default btn--<?php echo strtolower($model); ?>" data-model="activity" data-model-id="<?php echo $item['Activity']['id']; ?>" data-slug="<?php echo $item['Activity']['slug']; ?>"><?php echo $item['Activity']['name']; ?></span>
                <?php elseif($model == 'FBModule'): ?>
                  <span class="btn btn-xs default btn--<?php echo strtolower($model); ?>" data-model="fb_module" data-model-id="<?php echo $item['FBModule']['id']; ?>" data-slug="<?php echo $item['FBModule']['slug']; ?>"><?php echo $item['FBModule']['name']; ?></span>
                <?php endif; ?>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.order_associations();';
$this->end();
?>
