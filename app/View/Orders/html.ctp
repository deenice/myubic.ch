<?php if(!empty($order['OrderGroup'])): ?>
  <table class="order-items-title">
    <tr>
      <td class="order-name bold"><?php echo $order['Order']['name']; ?></td>
      <td class="actions text-right"><?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete this order')), array('controller' => 'orders', 'action' => 'delete', $order['Order']['id']), array('class' => 'btn btn-xs btn-danger delete-order', 'escape' => false)); ?></td>
    </tr>
  </table>
  <div class="panel-group accordion" id="order<?php $order['Order']['id']; ?>">
    <?php foreach($order['OrderGroup'] as $k => $group): ?>
      <div class="panel panel-default" data-order-group-id="<?php echo $group['id']; ?>">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a href="#group<?php echo $group['id']; ?>" class="accordion-toggle accordion-toggle-styled<?php echo in_array($group['id'], $openGroups) ? '':' collapsed'; ?>" data-toggle="collapse" data-parent="#order<?php echo $order['Order']['id']; ?>">
            <?php echo $group['name']; ?>
            <span class="pull-right status">
              <span class="text-success sufficient-stock"><i class="fa fa-check"></i> <?php echo __('Sufficient stock'); ?></span>
              <span class="text-danger non-sufficient-stock"><i class="fa fa-times"></i> <?php echo __('Insufficient stock'); ?></span>
              <span class="text-warning empty-stock"><i class="fa fa-warning"></i> <?php echo __('Empty stock'); ?></span>
            </span>
            </a>
          </h4>
        </div>
        <div class="panel-collapse <?php echo in_array($group['id'], $openGroups) ? 'in':'collapse'; ?>" id="group<?php echo $group['id']; ?>" data-order-group-id="<?php echo $group['id']; ?>">
          <div class="panel-body">
            <?php if(!empty($group['UnEditableOrderItem'])): ?>
              <table class="table table-striped table-order-items table-order-items--portlet">
                <thead>
                  <tr>
                    <th><?php echo __('Quantity'); ?></th>
                    <th><?php echo __('Name'); ?></th>
                    <th class="status"></th>
                    <th class="actions"></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($group['UnEditableOrderItem'] as $item): ?>
                    <tr class="<?php echo $item['quantity_option']; ?>">
                      <td class="quantity<?php echo ($item['quantity'] == 0 && empty($item['number_of_containers'])) ? ' has-error' : ''; ?>">
                        <?php if(!empty($item['number_of_containers'])): ?>
                          <?php echo $this->Form->input('OrderItem.number_of_containers', array('label' => false, 'div' => false, 'value' => $item['number_of_containers'], 'readonly' => $item['locked'], 'class' => 'form-control input-inline input-sm input-xsmall update-order-item', 'data-href' => Router::url(array('controller' => 'order_items', 'action' => 'updateField')), 'data-order-item-id' => $item['id'], 'data-field' => 'number_of_containers')); ?>
                          <small>(<?php echo (is_numeric($item['quantity']) && floor($item['quantity']) != $item['quantity']) ? $item['quantity'] : number_format($item['quantity'],0,'.',''); ?><?php echo $item['quantity_unit']; ?>)</small>
                        <?php else: ?>
                          <?php echo $this->Form->input('OrderItem.quantity', array('label' => false, 'div' => false, 'readonly' => $item['locked'], 'value' => (is_numeric($item['quantity']) && floor($item['quantity']) != $item['quantity']) ? $item['quantity'] : number_format($item['quantity'],0,'.',''), 'class' => 'form-control input-inline input-sm input-xsmall update-order-item', 'data-href' => Router::url(array('controller' => 'order_items', 'action' => 'updateField')), 'data-order-item-id' => $item['id'], 'data-field' => 'quantity')); ?>
                          <?php echo $item['quantity_unit']; ?>
                        <?php endif; ?>
                      </td>
                      <td class="name">
                        <?php if(!empty($item['StockItem'])): ?>
                          <?php echo $this->Html->link($item['StockItem']['code_name'], array('controller' => 'stock_items', 'action' => 'view', $item['StockItem']['id']), array('target' => '_blank')); ?>
                          <small>(Stock total: <?php echo !empty($item['StockItem']['quantity']) ? $item['StockItem']['quantity'] : 0; ?>)</small>
                        <?php else: ?>
                          <?php echo $item['name']; ?>
                        <?php endif; ?>
                        <?php if(!empty($item['remarks'])): ?>
                          <br><small class="text-danger"><?php echo $item['remarks']; ?></small>
                        <?php endif; ?>
                      </td>
                      <td class="status">
                        <?php if(!empty($item['StockItem'])): ?>
                          <?php echo $this->Html->link(sprintf('<i class="fa fa-warning"></i> %s', __('Empty stock')), array('controller' => 'stock_items', 'action' => 'showConflicts', $item['StockItem']['id'], $item['quantity'], $event['Event']['confirmed_date'], 0, 'event'), array('escape' => false, 'class' => 'text-warning empty-stock', 'data-toggle' => 'modal', 'data-target' => '#conflicts')); ?>
                          <?php echo $this->Html->link(sprintf('<i class="fa fa-times"></i> %s', __('Insufficient stock')), array('controller' => 'stock_items', 'action' => 'showConflicts', $item['StockItem']['id'], $item['quantity'], $event['Event']['confirmed_date'], 0, 'event'), array('escape' => false, 'class' => 'text-danger non-sufficient-stock', 'data-toggle' => 'modal', 'data-target' => '#conflicts')); ?>
                          <span class="text-success sufficient-stock"><i class="fa fa-check"></i> <?php echo __('Sufficient stock'); ?> </span>
                        <?php endif; ?>
                      </td>
                      <td class="text-right">
                        <?php echo $this->Html->link('<i class="fa fa-edit"></i>', array('controller' => 'order_items', 'action' => 'set_editable', $item['id']), array('class' => 'set-editable-order-item', 'escape' => false)); ?>
                        <?php if($item['locked']): ?>
                          <?php echo $this->Html->link('<i class="fa fa-lock font-grey-gallery"></i>', array('controller' => 'order_items', 'action' => 'lock', $item['id']), array('class' => 'lock-order-item', 'escape' => false)); ?>
                        <?php else: ?>
                          <?php echo $this->Html->link('<i class="fa fa-unlock-alt"></i>', array('controller' => 'order_items', 'action' => 'lock', $item['id']), array('class' => 'lock-order-item', 'escape' => false)); ?>
                        <?php endif; ?>
                        <?php echo $this->Html->link('<i class="fa fa-times font-red"></i>', array('controller' => 'order_items', 'action' => 'delete', $item['id']), array('class' => 'delete-order-item', 'escape' => false)); ?>
                      </td>
                      <?php echo $this->Form->input('OrderItem.id', array('id' => false, 'type' => 'hidden', 'value' => $item['id'], 'data-order-item-id' => '')); ?>
                      <?php echo $this->Form->input('OrderItem.stock_item_id', array('id' => false, 'type' => 'hidden', 'value' => empty($item['stock_item_id']) ? '' : $item['stock_item_id'], 'data-stock-item-id' => '')); ?>
                      <?php echo $this->Form->input('OrderItem.order_group_id', array('id' => false, 'type' => 'hidden', 'value' => $group['id'], 'data-order-group-id' => '')); ?>
                      <?php echo $this->Form->input('OrderItem.editable', array('type' => 'hidden', 'value' => $item['editable'], 'class' => 'set-editable-order-item')); ?>
                      <?php echo $this->Form->input('OrderItem.locked', array('type' => 'hidden', 'value' => $item['locked'], 'class' => 'lock-order-item')); ?>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            <?php endif; ?>
            <?php if(!empty($group['EditableOrderItem'])): ?>
              <table class="table table-striped table-bordered1 table-order-items">
                <thead>
                  <tr>
                    <th class="drag"></th>
                    <th class="option"></th>
                    <th class="quantity"><?php echo __('Quantity'); ?></th>
                    <th class="name"><?php echo __('Name'); ?></th>
                    <th class="depot"><?php echo __('Depot'); ?></th>
                    <th class="comments"><?php echo __('Comments'); ?></th>
                    <th class="supplier"><?php echo __('Supplier'); ?></th>
                    <th class="actions"></th>
                  </tr>
                </thead>
                <tbody class="connectedSortable">
                  <?php foreach($group['EditableOrderItem'] as $item): ?>
                    <tr class="<?php echo $item['quantity_option']; ?>" data-editable>
                    <td class="drag"><i class="fa fa-arrows font-grey"></i></td>
                    <td>
                      <?php echo $this->Form->input('OrderItem.quantity_option', array('div' => false, 'label' => false, 'id' => '', 'value' => $item['quantity_option'], 'class' => 'quantity-option update-order-item', 'options' => Configure::read('Orders.quantity_options'))); ?>
                    </td>
                    <td class="quantity">
                      <?php echo $this->Form->input('OrderItem.default_quantity', array('div' => false, 'label' => false, 'id' => '', 'type' => 'number', 'value' => (is_numeric($item['default_quantity']) && floor($item['default_quantity']) != $item['default_quantity']) ? $item['default_quantity'] : number_format($item['default_quantity'],0,'.',''), 'class' => 'quantity update-order-item')); ?>
                      <span class="help-inline interval-help-text"><small><?php echo __('PAX / unit'); ?></small></span>
                      <span class="help-inline pieces-person-help-text"><small><?php echo __('by person'); ?></small></span>
                      <span class="help-inline pieces-person-team-help-text"><small><?php echo __('by person/team'); ?></small></span>
                      <span class="help-inline pieces-team-help-text"><small><?php echo __('by team'); ?></small></span>
                      <span class="help-inline pieces-staff-help-text"><small><?php echo __('by staff'); ?></small></span>
                      <span class="help-inline pieces-group-help-text"><small><?php echo __('by group'); ?></small></span>
                      <span class="help-inline pieces-animator-help-text"><small><?php echo __('by animator'); ?></small></span>
                      <?php echo $this->Form->input('OrderItem.quantity_unit', array('div' => false, 'label' => false, 'id' => '', 'value' => $item['quantity_unit'], 'class' => 'quantity-unit update-order-item', 'options' => Configure::read('Orders.quantity_units'))); ?>
                      <?php echo $this->Form->input('OrderItem.quantity_per_container', array('div' => false, 'label' => false, 'id' => '', 'type' => 'number', 'value' => $item['quantity_per_container'], 'class' => 'quantity-per-container update-order-item')); ?>
                      <span class="help-inline quantity-per-container-help-text"><small><?php echo __('/ container'); ?></small></span>
                    </td>
                    <td class="name">
                      <?php if(!empty($item['StockItem'])): ?>
                      <?php echo $this->Html->link($item['StockItem']['code_name'], array('controller' => 'stock_items', 'action' => 'view', $item['StockItem']['id']), array('target' => '_blank')); ?>
                      <?php else: ?>
                      <?php echo $this->Form->input('OrderItem.name', array('label' => false, 'div' => false, 'value' => $item['name'], 'class' => 'update-order-item')); ?>
                      <?php endif; ?>
                    </td>
                    <td class="stock_location">
                      <?php echo $this->Form->input('OrderItem.stock_location_id', array('div' => false, 'label' => false, 'empty' => __('Select a depot'), 'id' => '', 'value' => $item['stock_location_id'], 'class' => 'update-order-item', 'options' => $locations)); ?>
                    </td>
                    <td>
                      <?php echo $this->Form->input('OrderItem.remarks', array('label' => false, 'div' => false, 'type' => 'text', 'value' => $item['remarks'], 'class' => 'update-order-item', 'value' => $item['remarks'])); ?>
                    </td>
                    <td>
                      <?php echo $this->Form->input('OrderItem.supplier_id', array('div' => false, 'label' => false, 'id' => '', 'empty' => __('Select a supplier'), 'value' => $item['supplier_id'], 'class' => 'update-order-item', 'options' => $suppliers)); ?>
                    </td>
                    <td>
                      <a href="#" class="language-dependent<?php echo $item['language_dependent'] ? ' active': ''; ?>" title="<?php echo __('Language dependent?'); ?>"><i class="fa fa-globe"></i></a>
                      <?php echo $this->Html->link('<i class="fa fa-check"></i>', array('controller' => 'order_items', 'action' => 'set_editable', $item['id']), array('class' => 'set-editable-order-item', 'escape' => false)); ?>
                      <?php echo $this->Html->link('<i class="fa fa-times font-red"></i>', array('controller' => 'order_items', 'action' => 'delete', $item['id']), array('class' => 'delete-order-item', 'escape' => false)); ?>
                    </td>
                    <?php echo $this->Form->input('OrderItem.id', array('id' => false, 'type' => 'hidden', 'value' => $item['id'], 'data-order-item-id' => '')); ?>
                    <?php echo $this->Form->input('OrderItem.order_group_id', array('id' => false, 'type' => 'hidden', 'value' => $group['id'], 'data-order-group-id' => '')); ?>
                    <?php echo $this->Form->input('OrderItem.language_dependent', array('type' => 'hidden', 'value' => $item['language_dependent'], 'class' => 'language-dependent')); ?>
                    <?php echo $this->Form->input('OrderItem.editable', array('type' => 'hidden', 'value' => $item['editable'], 'class' => 'set-editable-order-item')); ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          <?php endif; ?>
          <?php if(empty($group['UnEditableOrderItem']) && empty($group['EditableOrderItem'])): ?>
            <table class="table">
              <tbody class="connectedSortable empty">
                <tr class="empty"><td colspan="7"></td></tr>
              </tbody>
            </table>
          <?php endif; ?>
          <div class="form">
            <div class="row form-actions">
              <div class="col-md-4">
                <?php echo $this->Form->input('add-stock-item', array('type' => 'select', 'div' => false, 'class' => 'form-control input-inline add-stock-item', 'label' => false, 'id' => false, 'style' => 'width: 100%', 'data-order-group-id' => $group['id'])); ?>
              </div>
              <div class="col-md-3">
                <?php echo $this->Html->link(__('Add a product'), array('controller' => 'order_items', 'action' => 'add', 'order_group_id' => $group['id']), array('class' => 'btn btn-default add-simple-product', 'escape' => false)); ?>
              </div>
              <div class="col-md-3">
                <?php echo $this->Form->input('order-group-name', array('id'=> false, 'label' => false, 'div' => false, 'class' => 'form-control rename-order-group', 'value' => $group['name'], 'data-href' => Router::url(array('controller' => 'order_groups', 'action' => 'rename', $group['id'])))); ?>
                <span class="help-block"><?php echo __('Rename group'); ?></span>
              </div>
              <div class="col-md-2 delete-order-group-wrapper">
                <?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> <span>%s</span>', __('Remove group')), array('controller' => 'order_groups', 'action' => 'delete', $group['id']), array('escape' => false, 'class' => 'btn btn-block btn-danger delete-order-group')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
  </div>
<?php endif; ?>