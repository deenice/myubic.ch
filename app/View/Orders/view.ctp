<?php $this->assign('page_title', __('Order models'));?>
<?php $this->assign('page_subtitle', $order['Order']['name']);?>
<div class="row profile">
	<div class="col-md-12">
		<div class="row">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-clipboard font-blue-madison"></i>
						<span class="caption-subject font-blue-madison bold uppercase"><?php echo $order['Order']['name']; ?></span>
					</div>
					<div class="actions">
						<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit')), array('controller' => 'orders', 'action' => 'edit', $order['Order']['id']), array('class' => 'btn btn-sm btn-default', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="panel-group accordion" id="accordion">
							<?php foreach($order['OrderGroup'] as $k => $group): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $this->Html->link($group['name'], 'javascript:;', array('class' => 'accordion-toggle')); ?>
										</h4>
									</div>
									<div id="group<?php echo $group['id']; ?>" class="panel-collapse">
										<div class="panel-body">
											<table class="table table-striped table-order-items table-order-items--view">
												<thead>
													<tr>
														<th class="quantity text-center"><?php echo __('Quantity'); ?></th>
														<th class="quantity_unit"></th>
														<th class="name"><?php echo __('Name'); ?></th>
														<th class="supplier"><?php echo __('Supplier'); ?></th>
														<th class="remarks"><?php echo __('Remarks'); ?></th>
													</tr>
												</thead>
												<tbody>
													<?php foreach($group['OrderItem'] as $item): ?>
														<tr class="<?php echo $item['quantity_option']; ?>">
															<td class="text-center">
																<?php echo (is_numeric($item['default_quantity']) && floor($item['default_quantity']) != $item['default_quantity']) ? $item['default_quantity'] : number_format($item['default_quantity'],0); ?>
																<?php echo $item['quantity_unit']; ?>
															</td>
															<td>
																<?php echo $item['quantity_per_container']; ?>
																<span class="help-inline quantity-per-container-help-text"><small><?php echo __('/ container'); ?></small></span>
																<span class="help-inline interval-help-text"><small><?php echo __('PAX / unit'); ?></small></span>
																<span class="help-inline pieces-person-help-text"><small><?php echo __('by person'); ?></small></span>
																<span class="help-inline pieces-team-help-text"><small><?php echo __('by team'); ?></small></span>
																<span class="help-inline pieces-staff-help-text"><small><?php echo __('by staff'); ?></small></span>
																<span class="help-inline pieces-group-help-text"><small><?php echo __('by group'); ?></small></span>
																<span class="help-inline pieces-animator-help-text"><small><?php echo __('by animator'); ?></small></span>
															</td>
															<td>
																<?php if(!empty($item['StockItem'])): ?>
																	<?php echo $this->Html->link($item['StockItem']['code_name'], array('controller' => 'stock_items', 'action' => 'view', $item['StockItem']['id']), array('target' => '_blank')); ?>
																<?php else: ?>
																	<?php echo $item['name']; ?>
																<?php endif; ?>
															</td>
															<td><?php echo !empty($item['Supplier']) ? $item['Supplier']['name'] : __('None'); ?></td>
															<td><?php echo $item['remarks']; ?></td>
														</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
