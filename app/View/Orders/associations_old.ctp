<?php $this->assign('page_title', $this->Html->link(__('Orders'), array('controller' => 'orders', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Associations'));?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-clipboard font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"> <?php echo __('Associations'); ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<table class="table table-striped table-condensed table-associations">
			<thead>
				<tr>
					<th class="model"></th>
					<?php foreach($orders as $order): ?>
						<th class="order">
							<div><span class="btn btn-xs btn-company btn-company-<?php echo $order['Company']['class']; ?>"></span> <?php echo $order['Order']['name']; ?></div>
						</th>
					<?php endforeach; ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($items as $company => $itemss): ?>
					<tr class="company">
						<td colspan="<?php echo sizeof($orders) + 1; ?>">
							<strong class="uppercase"><?php echo $company; ?></strong>
						</td>
					</tr>
					<?php foreach($itemss as $item): ?>
					<tr>
						<td>
							<?php echo $item['name']; ?>
						</td>
						<?php for($i=0; $i<sizeof($orders);$i++): ?>
							<td>
								<input type="checkbox" name="name" <?php echo in_array($orders[$i]['Order']['id'], $item['orders']) ? 'checked="checked"':''; ?> data-model-id="<?php echo $item['id']; ?>" data-model="<?php echo $item['model']; ?>" data-order-id="<?php echo $orders[$i]['Order']['id']; ?>">
							</td>
						<?php endfor; ?>
					</tr>
				<?php endforeach; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.order_associations();';
$this->end();
?>
