<?php $this->assign('page_title', $this->Html->link(__('Order models'), array('controller' => 'orders', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $order['Order']['name']);?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-clipboard font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $order['Order']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Order', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('Order.id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => $companies)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('fb_category_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => $categories)); ?>
            <span class="help-block">Seulement pour Effet Gourmand</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-clipboard font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Products'); ?></span>
		</div>
	</div>
	<div class="portlet-body order-items-wrapper order-items-wrapper--disable-stock">
		<div data-order-items data-order-id="<?php echo $order['Order']['id']; ?>" data-href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'html', $order['Order']['id'])); ?>">
			en chargement...
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="input-group order-group-name">
					<?php echo $this->Form->input('order-group-name', array('div' => false, 'label' => false, 'class' => 'form-control', 'placeholder' => __('Add a group'), 'data-href' => Router::url(array('controller' => 'order_groups', 'action' => 'add', 'order_id' => $order['Order']['id'])))); ?>
					<span class="input-group-btn">
						<button class="btn blue" type="button"><i class="fa fa-plus"></i></button>
					</span>
				</div>
				<?php echo $this->Form->input('open-groups', array('type' => 'hidden', 'value' => implode(',', $firstGroup), 'class' => 'open-groups')); ?>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.orders();';
$this->end();
?>
