<?php

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'orders', 'action' => 'view', $result['Order']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false)
	);

	$actions .= $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'orders', 'action' => 'edit', $result['Order']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  if(!empty($result['Order']['remarks'])){
    $actions .= $this->element('Modals/order-remarks', array('id' => $result['Order']['id'], 'remarks' => $result['Order']['remarks'], 'title' => $result['Order']['name']));
  }

  $this->dtResponse['aaData'][] = array(
    $result['Order']['name'],
    $result['Company']['name'],
    $actions
  );
}
