<?php $this->assign('page_title', __('Companies')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-trophy"></i><?php echo __('List of all companies'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Company'); ?>
	</div>
</div>
