<?php

foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'companies', 'action' => 'edit', $result['Company']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $name = '<span class="btn btn-xs btn-company btn-company-'.$result['Company']['class'].'"></span>';
  $name .= ' ' . $result['Company']['name'];

  $this->dtResponse['aaData'][] = array(
    $name,
    $actions
  );
}
