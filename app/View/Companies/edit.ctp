<?php $this->assign('page_title', $this->Html->link(__('Companies'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $company['Company']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-briefcase font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $company['Company']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Company', array('class' => 'form-horizontal', 'type' => 'file')); ?>
		<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'required' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Logo'); ?></label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
								<?php if(!empty($this->request->data['Logo']['url'])): ?>
									<?php echo $this->Html->image(DS . $this->request->data['Logo']['url'], array('max-height' => 140, 'width' => 140)); ?>
								<?php else: ?>
									<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+logo" alt=""/>
								<?php endif; ?>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new"><?php echo __('Select image'); ?></span>
								<span class="fileinput-exists"><?php echo __('Change'); ?></span>
								<?php echo $this->Form->input('logo', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Class'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('class', array('label' => false, 'class' => 'form-control', 'readonly' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('city', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
