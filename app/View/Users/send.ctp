<?php $this->assign('page_title', __('Users'));?>
<?php $this->assign('page_subtitle', __('Send message'));?>
<div class="row profile">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-edit font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Compose message'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<?php echo $this->Form->create('send', array('class' => 'form-horizontal')); ?>
						<div class="row form-group">
							<div class="col-md-9">
								<label class="control-label"><?php echo __('Subject'); ?></label>
								<?php echo $this->Form->input('subject', array('label' => false, 'class' => 'form-control')); ?>
							</div>
							<div class="col-md-3">
								<label class="control-label"><?php echo __('Type'); ?></label>
								<?php echo $this->Form->input('type', array('label' => false, 'class' => 'make-switch', 'checked' => true, 'data-on-text' => 'MAIL', 'data-off-text' => 'SMS', 'data-off-color' => 'success', 'type' => 'checkbox')); ?>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<label class="control-label"><?php echo __('Recipients'); ?></label>
								<?php echo $this->Form->input('recipients', array('label' => false, 'class' => 'form-control')); ?>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<label class="control-label"><?php echo __('Diffusion lists'); ?></label>
								<?php echo $this->Form->input('lists', array('label' => false, 'class' => 'form-control bs-select', 'options' => $lists, 'multiple' => true)); ?>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<label class="control-label"><?php echo __('Message'); ?></label>
								<?php echo $this->Form->input('mailBody', array('label' => false, 'class' => 'form-control ckeditor body mail', 'type' => 'textarea')); ?>
								<?php echo $this->Form->input('smsBody', array('label' => false, 'class' => 'form-control hidden body sms', 'type' => 'textarea')); ?>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-12">
								<?php echo $this->Form->input('receive_copy', array('label' => __('Receive a copy?'), 'class' => 'form-control', 'type' => 'checkbox')); ?>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-12">
									<button type="submit" class="btn blue"><i class="fa fa-envelope-o"></i> <?php echo __('Send'); ?></button>
								</div>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if(!empty($messages)): ?>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-paper-plane font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Sent messages'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table">
							<thead>
								<tr>
									<th>#</th>
									<th><?php echo __('Sent date'); ?></th>
									<th><?php echo __('Subject'); ?></th>
									<th><?php echo __('Recipients'); ?></th>
									<th><?php echo __('Sender'); ?></th>
									<th><?php echo __('Type'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($messages as $k => $message): ?>
									<tr>
										<td><?php echo $k+1; ?></td>
										<td><?php echo $this->Time->format($message['Message']['created'], '%A %d %b %Y'); ?></td>
										<td><?php echo $message['Message']['subject']; ?></td>
										<td><?php echo implode(', ', $message['Message']['to']); ?></td>
										<td><?php echo $message['User']['full_name']; ?></td>
										<td><?php echo strtolower($message['Message']['type']); ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php endif;?>
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/ckeditor/ckeditor.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.messages();';
$this->end();
?>
