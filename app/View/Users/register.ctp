<!-- BEGIN REGISTRATION FORM -->
<?php echo $this->Form->create('User', array('class' => 'register-form', 'action' => 'register', 'style' => 'display:block')); ?>
	<h3><?php echo __('Register'); ?></h3>
	<p><?php echo __("Enter your personal details below"); ?></p>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9"><?php echo __('First Name'); ?></label>
		<?php echo $this->Form->input('first_name', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('First name'))); ?>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9"><?php echo __('Last Name'); ?></label>
		<?php echo $this->Form->input('last_name', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('Last name'))); ?>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9"><?php echo __('Email'); ?></label>
		<?php echo $this->Form->input('email', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('Email'))); ?>
	</div>
	<p>
		 Enter your account details below:
	</p>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Username</label>
		<?php echo $this->Form->input('username', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('Username'))); ?>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<?php echo $this->Form->input('password', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('Password'))); ?>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
		<?php echo $this->Form->input('rpassword', array('class' => 'form-control placeholder-no-fix', 'label' => false, 'placeholder' => __('Re-type your password'), 'required' => true, 'type' => 'password')); ?>
	</div>
	<div class="form-actions">
		<button type="submit" id="register-submit-btn" class="btn blue">
		Register <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
<!-- END REGISTRATION FORM -->
<?php echo $this->Form->end(); ?>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/pages/css/login-soft.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js');
echo $this->Html->script('/metronic/global/plugins/backstretch/jquery.backstretch.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');

$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/pages/scripts/login-soft.js');
$this->end();
$this->start('init_scripts');
echo 'Login.init();';
echo '// init background slide images
       $.backstretch([
        "'.$this->webroot.'metronic/pages/media/bg/1.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/2.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/3.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
';
$this->end();
?>