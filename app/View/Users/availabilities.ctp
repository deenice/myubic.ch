<?php $this->assign('page_title', __('Collaborators'));?>
<?php $this->assign('page_subtitle', __('Availabilities'));?>
<div class="tabbable-custom nav-justified">
	<ul class="nav nav-tabs nav-justified">
		<li class="active">
			<a href="#monday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Monday'); ?></a>
		</li>
		<li>
			<a href="#tuesday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Tuesday'); ?></a>
		</li>
		<li>
			<a href="#wednesday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Wednesday'); ?></a>
		</li>
		<li>
			<a href="#thursday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Thursday'); ?></a>
		</li>
		<li>
			<a href="#friday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Friday'); ?></a>
		</li>
		<li>
			<a href="#saturday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Saturday'); ?></a>
		</li>
		<li>
			<a href="#sunday" data-toggle="tab"><i class="fa fa-calendar"></i> <?php echo __('Sunday'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="monday">
			<?php echo $this->Availability->generateUsers($users, 'monday'); ?>
		</div>
		<div class="tab-pane fade" id="tuesday">
			<?php echo $this->Availability->generateUsers($users, 'tuesday'); ?>
		</div>
		<div class="tab-pane fade" id="wednesday">
			<?php echo $this->Availability->generateUsers($users, 'wednesday'); ?>
		</div>
		<div class="tab-pane fade" id="thursday">
			<?php echo $this->Availability->generateUsers($users, 'thursday'); ?>
		</div>
		<div class="tab-pane fade" id="friday">
			<?php echo $this->Availability->generateUsers($users, 'friday'); ?>
		</div>
		<div class="tab-pane fade" id="saturday">
			<?php echo $this->Availability->generateUsers($users, 'saturday'); ?>
		</div>
		<div class="tab-pane fade" id="sunday">
			<?php echo $this->Availability->generateUsers($users, 'sunday'); ?>
		</div>
	</div>
</div>