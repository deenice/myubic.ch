<?php if(!empty($users)): ?>
<table class="table table-condensed table-striped table-modal-form-job">
  <tbody>
    <?php foreach($users as $user): ?>
      <tr>
        <td><?php echo $user['User']['full_name']; ?></td>
        <td>
          <?php if($user['availability'] == 'yes'): ?>
          <span class="label label-sm label-success">
          <?php elseif($user['availability'] == 'maybe'): ?>
          <span class="label label-sm label-warning">
          <?php elseif($user['availability'] == 'no'): ?>
          <span class="label label-sm label-danger">
          <?php elseif($user['availability'] == 'unknown'): ?>
          <span class="label label-sm label-default">
          <?php endif; ?>
            <?php echo $availabilities[$user['availability']]; ?>
          </span>
        </td>
        <td>
          <?php if(!empty($user['data'])): ?>
            <small>
            <?php if($user['data']['UserUnavailability']['model'] == 'job'): ?>
              <?php if($user['data']['UserUnavailability']['status'] == 'enrolled' || $user['data']['UserUnavailability']['status'] == 'interested'): ?>
              <?php echo __('Works already as a %s on %s for %s', sprintf('<strong>%s</strong><br />', $user['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link(empty($user['data']['Job']['Event']['code_name']) ? $user['data']['Job']['Event']['name'] : $user['data']['Job']['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $user['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['data']['Job']['Event']['Client']['name']); ?>
              <?php elseif($user['data']['UserUnavailability']['status'] == 'not_available'): ?>
              <?php echo __('Not available at this date: according to answer of job %s on %s for %s', sprintf('<br /><strong>%s</strong>', $user['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link($user['data']['Job']['Event']['name'], array('controller' => 'events', 'action' => 'work', $user['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['data']['Job']['Event']['Client']['name']); ?>
              <?php elseif($user['data']['UserUnavailability']['status'] == 'not_interested'): ?>
              <?php echo __('Was not interested for job %s on %s for %s', sprintf('<strong>%s</strong><br />', $user['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link($user['data']['Job']['Event']['name'], array('controller' => 'events', 'action' => 'work', $user['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['data']['Job']['Event']['Client']['name']); ?>
              <?php endif; ?>
            <?php endif; ?>
            </small>
          <?php endif; ?>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
