<?php $this->assign('page_title', $this->Html->link(__('Users'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $user['User']['full_name']);?>
<?php $roles = Configure::read('Users.roles') ?>
<?php $civilStatus = array('single' => __('Single'), 'married' => __('Married'),'divorced' => __('Divorced'), 'separated' => __('Séparé'), 'widow' => __('Widow')); ?>
<?php $sizes = Configure::read('Users.sizes'); ?>
<?php $competencesSectors = Configure::read('Competences.sectors'); ?>
<?php $competencesJobs = array_merge(Configure::read('Competences.fb_jobs'), Configure::read('Competences.animation_jobs'), Configure::read('Competences.logistics_jobs')); ?>
<?php $competencesHierarchies = Configure::read('Competences.hierarchies'); ?>
<?php $attributes = array('legend' => false, 'label' => false, 'class' => 'toggle'); ?>
<?php $attributes1 = array('legend' => false, 'label' => false, 'class' => 'toggle module-user-id-suitable-input', 'hiddenField' => false); ?>
<?php global $counter; $counter = 0; ?>
<div class="tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#competences" data-toggle="tab"><i class="fa fa-cubes"></i> <?php echo __('Competences'); ?></a>
		</li>
		<li>
			<a href="#modules" data-toggle="tab"><i class="fa fa-graduation-cap"></i> <?php echo __('Modules'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<h3 class="form-section"><?php echo __('Connexion data'); ?></h3>
				<div class="form-group hidden">
					<label class="control-label col-md-3"><?php echo __('UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Username'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('username', array('label' => false, 'class' => 'form-control', 'readonly' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('email', array('type' => 'email', 'label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Actual password'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('password_verification', array('label' => false, 'class' => 'form-control', 'type' => 'password'));?>
						<span class="help-block"><?php echo __("Leave blank if you don't want to update your password."); ?></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('New password'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('password_update', array('label' => false, 'class' => 'form-control', 'type' => 'password'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Personal data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('First Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('first_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Last Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('last_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Civility'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('civility', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.civilities'), 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Picture'); ?></label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
								<?php if(!empty($thumbnail)): ?>
									<?php echo $this->Html->image(DS . $thumbnail[0]['Document']['url'], array('max-height' => 140, 'width' => 140)); ?>
								<?php else: ?>
									<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
								<?php endif; ?>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new"><?php echo __('Select image'); ?></span>
								<span class="fileinput-exists"><?php echo __('Change'); ?></span>
								<?php echo $this->Form->input('thumbnail', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Gender'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('gender', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Users.gender'), 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date of birth'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('date_of_birth', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Zip'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('city', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => Configure::read('Countries')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Mobile'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('mobile', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Business phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('business_phone', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Nationality'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('nationality', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Civil status'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('civil_status',array('options' => $civilStatus, 'label' => false, 'empty' => true, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Wedding date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('wedding_date',array('label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Shirt size'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('shirt_size',array('options' => $sizes, 'label' => false, 'empty' => true, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Availability'); ?></label>
					<div class="col-md-9">
						<?php echo $this->element('Users/availabilities', array('user' => $user));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('availabilities_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Administrative data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('companies', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies, 'multiple' => true, 'value' => $userCompanies));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Role'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('role', array('label' => false, 'class' => 'form-control bs-select', 'options' => $roles));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Function'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('function', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Ce champ est à remplir pour les collaborateurs fixes.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Trainee'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('trainee', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Section'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.section', array('class' => 'form-control bs-select', 'label' => false, 'multiple' => true, 'options' => $sections, 'value' => (!empty($tags['section'])) ? $tags['section']:'')); ?>
						<span class="help-block">Ce champ est à remplir pour les extras.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start of collaboration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_of_collaboration', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End of collaboration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('end_of_collaboration', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Work permit'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('work_permit', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Users.work_permits')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('AVS number'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('avs_number', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Income tax assessment'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Code barême impôt source'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('income_tax_code', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Non professional accident insurance'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Postfinance account number'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('postfinance_account_number', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Bank name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Bank clearing'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_clearing', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP and city of the bank'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_zip_city', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('IBAN number / Bank account'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('iban', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Wealthings data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('UBIC UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ubic_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Festiloc UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('festiloc_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Effet Gourmand UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('eg_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Ubic Group UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ubic_group_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Competences'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Car'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Driving licence'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Trailer licence'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Other licences'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('other_licences', array('label' => false, 'class' => 'select2me form-control', 'multiple' => true, 'options' => Configure::read('Users.licences'), 'value' => !empty($this->request->data['User']['other_licences']) ? explode(',',$this->request->data['User']['other_licences']):''));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Talents'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.talents', array('type' => 'select', 'multiple' => true, 'data-select2-tags' => '', 'class' => 'form-control', 'label' => false, 'value' => (!empty($tags['talent'])) ? $tags['talent']:'')); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Languages'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.french_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('French'))); ?>
						<span class="help-block">
							<?php echo $this->Html->link(__('Levels signification'), '#levelsSignification', array('data-toggle' => 'modal')); ?>
						</span>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.german_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('German'))); ?>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.english_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('English'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Other mastered languages'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.other_languages', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Frontal animation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('frontend_animation', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Actual occupation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('actual_occupation', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Professional experience'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('professional_experience', array('class' => 'form-control', 'label' => false)); ?>
						<span class="help-block"><?php echo __('Student job, training, etc'); ?></span>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Internal data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Internal remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('internal_remarks', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Ce champ n'est pas montré aux extras!</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Has worked than 250 hours'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('more_than_250_hours', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Minimal salary'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('minimal_salary', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<!--div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Group'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('group_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $groups));?>
					</div>
				</div-->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
		</div>
		<div class="tab-pane" id="competences">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="tabbable-line">
						<ul class="nav nav-tabs">
              <li class="active">
                  <a href="#ubic" data-toggle="tab" class="no-save"> Animation UBIC </a>
              </li>
              <li>
                  <a href="#ug" data-toggle="tab" class="no-save"> Animation UG </a>
              </li>
              <li>
                  <a href="#fb" data-toggle="tab" class="no-save"> F&amp;B </a>
              </li>
              <li>
                  <a href="#logistics" data-toggle="tab" class="no-save"> <?php echo __('Logistics'); ?> </a>
              </li>
              <li>
                  <a href="#other" data-toggle="tab" class="no-save"> <?php echo __('Other'); ?> </a>
              </li>
            </ul>
						<div class="tab-content">
							<div class="tab-pane active" id="ubic">
								<table class="table table-striped table-hover">
									<tr>
										<td>&nbsp;</td>
										<td colspan="4" class="text-center bold">Aide Animateur</td>
										<td colspan="5" class="text-center bold">Animateur</td>
									</tr>
									<tr class="hierarchies">
										<td>&nbsp;</td>
										<td>Test</td>
										<td>Basique</td>
										<td>EMDO</td>
										<td>Ne convient pas</td>
										<td>Test</td>
										<td>Basique</td>
										<td>EMDO</td>
										<td>MPP</td>
										<td>Ne convient pas</td>
									</tr>
									<?php foreach($activities['ubic'] as $id => $activity): ?>
									<tr class="activities">
										<td class="right-bordered"><?php echo $this->Html->link($activity, array('controller' => 'activities', 'action' => 'view', $id), array('target' => '_blank')); ?></td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator_facilitator', 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator_facilitator'][1][$id]) ? $userCompetences['animation']['animator_facilitator'][1][$id]['id'] : false, 'rel' => 'animator_facilitator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator_facilitator', 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator_facilitator'][2][$id]) ? $userCompetences['animation']['animator_facilitator'][2][$id]['id'] : false, 'rel' => 'animator_facilitator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator_facilitator', 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator_facilitator'][3][$id]) ? $userCompetences['animation']['animator_facilitator'][3][$id]['id'] : false, 'rel' => 'animator_facilitator' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator_facilitator', 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator_facilitator'][0][$id]) ? $userCompetences['animation']['animator_facilitator'][0][$id]['id'] : false, 'rel' => 'animator_facilitator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator', 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator'][1][$id]) ? $userCompetences['animation']['animator'][1][$id]['id'] : false, 'rel' => 'animator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator', 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator'][2][$id]) ? $userCompetences['animation']['animator'][2][$id]['id'] : false, 'rel' => 'animator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator', 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator'][3][$id]) ? $userCompetences['animation']['animator'][3][$id]['id'] : false, 'rel' => 'animator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator', 'hierarchy' => 4, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator'][4][$id]) ? $userCompetences['animation']['animator'][4][$id]['id'] : false, 'rel' => 'animator' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'animator', 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['animator'][0][$id]) ? $userCompetences['animation']['animator'][0][$id]['id'] : false, 'rel' => 'animator' . $id)); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
							<div class="tab-pane" id="ug">
								<table class="table table-striped table-hover">
									<tr>
										<td>&nbsp;</td>
										<td colspan="4" class="text-center bold">Urban Coach</td>
										<td colspan="5" class="text-center bold">Urban Leader</td>
									</tr>
									<tr class="hierarchies">
										<td>&nbsp;</td>
										<td>Test</td>
										<td>Basique</td>
										<td>EMDO</td>
										<td>Ne convient pas</td>
										<td>Test</td>
										<td>Basique</td>
										<td>EMDO</td>
										<td>MPP</td>
										<td>Ne convient pas</td>
									</tr>
									<?php foreach($activities['ug'] as $id => $activity): ?>
									<tr class="activities">
										<td class="right-bordered"><?php echo $this->Html->link($activity, array('controller' => 'activities', 'action' => 'view', $id), array('target' => '_blank')); ?></td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_coach', 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_coach'][1][$id]) ? $userCompetences['animation']['urban_coach'][1][$id]['id'] : false, 'rel' => 'urban_coach' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_coach', 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_coach'][2][$id]) ? $userCompetences['animation']['urban_coach'][2][$id]['id'] : false, 'rel' => 'urban_coach' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_coach', 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_coach'][3][$id]) ? $userCompetences['animation']['urban_coach'][3][$id]['id'] : false, 'rel' => 'urban_coach' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_coach', 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_coach'][0][$id]) ? $userCompetences['animation']['urban_coach'][0][$id]['id'] : false, 'rel' => 'urban_coach' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_leader', 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_leader'][1][$id]) ? $userCompetences['animation']['urban_leader'][1][$id]['id'] : false, 'rel' => 'urban_leader' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_leader', 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_leader'][2][$id]) ? $userCompetences['animation']['urban_leader'][2][$id]['id'] : false, 'rel' => 'urban_leader' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_leader', 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_leader'][3][$id]) ? $userCompetences['animation']['urban_leader'][3][$id]['id'] : false, 'rel' => 'urban_leader' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_leader', 'hierarchy' => 4, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_leader'][4][$id]) ? $userCompetences['animation']['urban_leader'][4][$id]['id'] : false, 'rel' => 'urban_leader' . $id)); ?></td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('activity_id' => $id, 'sector' => 'animation', 'job' => 'urban_leader', 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['animation']['urban_leader'][0][$id]) ? $userCompetences['animation']['urban_leader'][0][$id]['id'] : false, 'rel' => 'urban_leader' . $id)); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
							<div class="tab-pane" id="fb">
								<table class="table table-striped table-hover">
									<tr>
										<td>&nbsp;</td>
										<td class="text-center">Test</td>
										<td class="text-center">Service crew</td>
										<td class="text-center">Event assistant</td>
										<td class="text-center">Event manager</td>
										<td class="text-center">Livreur</td>
										<td class="text-center">Grilleur</td>
										<td class="text-center">Bar manager</td>
										<td class="text-center">Bar crew</td>
										<td class="text-center">Ne convient pas</td>
									</tr>
									<?php foreach($fb_modules as $id => $module): ?>
									<tr class="fb_modules">
										<td class="right-bordered"><?php echo $this->Html->link($module, array('controller' => 'fb_modules', 'action' => 'view', $id), array('target' => '_blank')); ?></td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'test', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['test'][$id]) ? $userCompetences['fb']['test'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'service_crew', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['service_crew'][$id]) ? $userCompetences['fb']['service_crew'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'event_assistant', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['event_assistant'][$id]) ? $userCompetences['fb']['event_assistant'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'event_manager', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['event_manager'][$id]) ? $userCompetences['fb']['event_manager'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'deliverer', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['deliverer'][$id]) ? $userCompetences['fb']['deliverer'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'griller', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['griller'][$id]) ? $userCompetences['fb']['griller'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'bar_manager', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['bar_manager'][$id]) ? $userCompetences['fb']['bar_manager'][$id]['id'] : false, 'rel' => 'fb_' . $id)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'bar_crew', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['bar_crew'][$id]) ? $userCompetences['fb']['bar_crew'][$id]['id'] : false, 'rel' => 'fb_' . $id)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('fb_module_id' => $id, 'sector' => 'fb', 'job' => 'unsuitable', 'hierarchy' => -1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['fb']['unsuitable'][$id]) ? $userCompetences['fb']['unsuitable'][$id]['id'] : false, 'rel' => 'fb' . $id)); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
							<div class="tab-pane" id="logistics">
								<table class="table table-striped table-hover">
									<tr>
										<td>&nbsp;</td>
										<td class="text-center">Test</td>
										<td class="text-center">Basique</td>
										<td class="text-center">EMDO</td>
										<td class="text-center">MPP</td>
										<td class="text-center">Ne convient pas</td>
									</tr>
									<?php foreach($logistics_jobs as $key => $job): ?>
									<tr class="logistics_jobs">
										<td class="right-bordered"><?php echo $job; ?></td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'logistics', 'job' => $key, 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['logistics'][$key][1]) ? $userCompetences['logistics'][$key][1]['id'] : false, 'rel' => 'logistics' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'logistics', 'job' => $key, 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['logistics'][$key][2]) ? $userCompetences['logistics'][$key][2]['id'] : false, 'rel' => 'logistics' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'logistics', 'job' => $key, 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['logistics'][$key][3]) ? $userCompetences['logistics'][$key][3]['id'] : false, 'rel' => 'logistics' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'logistics', 'job' => $key, 'hierarchy' => 4, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['logistics'][$key][4]) ? $userCompetences['logistics'][$key][4]['id'] : false, 'rel' => 'logistics' . $key)); ?>
										</td>
										<td>
											<?php echo $this->element('Competences/checkbox', array('sector' => 'logistics', 'job' => $key, 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['logistics'][$key][0]) ? $userCompetences['logistics'][$key][0]['id'] : false, 'rel' => 'logistics' . $key)); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
							<div class="tab-pane" id="other">
								<table class="table table-striped table-hover">
									<tr>
										<td>&nbsp;</td>
										<td class="text-center">Test</td>
										<td class="text-center">Basique</td>
										<td class="text-center">EMDO</td>
										<td class="text-center">MPP</td>
										<td class="text-center">Ne convient pas</td>
									</tr>
									<?php foreach($other_jobs as $key => $job): ?>
									<tr class="other_jobs">
										<td class="right-bordered"><?php echo $job; ?></td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'other', 'job' => $key, 'hierarchy' => 1, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['other'][$key][1]) ? $userCompetences['other'][$key][1]['id'] : false, 'rel' => 'other' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'other', 'job' => $key, 'hierarchy' => 2, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['other'][$key][2]) ? $userCompetences['other'][$key][2]['id'] : false, 'rel' => 'other' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'other', 'job' => $key, 'hierarchy' => 3, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['other'][$key][3]) ? $userCompetences['other'][$key][3]['id'] : false, 'rel' => 'other' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'other', 'job' => $key, 'hierarchy' => 4, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['other'][$key][4]) ? $userCompetences['other'][$key][4]['id'] : false, 'rel' => 'other' . $key)); ?>
										</td>
										<td class="right-bordered">
											<?php echo $this->element('Competences/checkbox', array('sector' => 'other', 'job' => $key, 'hierarchy' => 0, 'user_id' => $user['User']['id'], 'id' => !empty($userCompetences['other'][$key][0]) ? $userCompetences['other'][$key][0]['id'] : false, 'rel' => 'other' . $key)); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if(false): ?>
			<?php echo $this->Form->create('Competence', array('class' => 'form-horizontal', 'url'=> array('controller'=>'competences', 'action'=>'save'))); ?>
			<div class="form-body" id="elements">
				<?php if(!empty($this->request->data['Competence'])): ?>
				<?php foreach($this->request->data['Competence'] as $k => $competence): ?>
				<div class="portlet light bg-grey-cararra element">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="sector"><?php echo $competencesSectors[$competence['sector']]; ?></span></strong>
							<span class="job"><?php echo $competencesJobs[$competence['job']]; ?></span>
							<span class="hierarchy"><?php echo $competencesHierarchies[$competence['hierarchy']]; ?></span>
							<span class="activity"><?php echo !empty($competence['activity_id']) ? $activities['all'][$competence['activity_id']] : ''; ?></span>
						</div>
						<div class="actions">
							<a href="" class="btn btn-circle btn-default remove-element"><i class="fa fa-trash-o"></i> <?php echo __('Remove this competence');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<div class="col-md-3">
								<?php echo $this->Form->input('Competence.model.', array('type' => 'hidden', 'value' => 'user', 'class' => 'model')) ; ?>
								<?php echo $this->Form->input('Competence.id.', array('type' => 'hidden', 'class' => 'id', 'value' => $competence['id'], 'id' => '')); ?>
								<?php echo $this->Form->input('Competence.user_id.', array('type' => 'hidden', 'value' => $this->request->data['User']['id'], 'class' => 'user_id', 'value' => $competence['user_id'], 'id' => '')); ?>
								<?php echo $this->Form->input('Competence.sector.', array('class' => 'form-control sector', 'label' => false, 'options' => Configure::read('Competences.sectors'), 'empty' => __('Select a sector'), 'value' => $competence['sector'], 'id' => '')); ?>
							</div>
							<div class="col-md-3">
								<?php if(array_key_exists($competence['job'], Configure::read('Competences.fb_jobs'))): ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control fb_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'), 'value' => $competence['job'], 'id' => '')); ?>
								<?php else: ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control fb_jobs jobs hidden', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
								<?php endif ?>
								<?php if(array_key_exists($competence['job'], Configure::read('Competences.logistics_jobs'))): ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control logistics_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'), 'value' => $competence['job'], 'id' => '')); ?>
								<?php else: ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control logistics_jobs jobs hidden', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
								<?php endif; ?>
								<?php if(array_key_exists($competence['job'], Configure::read('Competences.animation_jobs'))): ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control animation_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'), 'value' => $competence['job'], 'id' => '')); ?>
								<?php else: ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control animation_jobs jobs hidden', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'), 'id' => '')); ?>
								<?php endif; ?>
							</div>
							<?php if(array_key_exists($competence['job'], Configure::read('Competences.animation_jobs'))): ?>
							<div class="col-md-3 activities">
							<?php else: ?>
							<div class="col-md-3 activities hidden">
							<?php endif; ?>
								<?php if(array_key_exists($competence['activity_id'], $activities['ubic'])): ?>
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control ubicActivities', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'), 'value' => $competence['activity_id'], 'id' => '')); ?>
								<?php else: ?>
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control ubicActivities hidden', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'), 'id' => '')); ?>
								<?php endif; ?>
								<?php if(array_key_exists($competence['activity_id'], $activities['ug'])): ?>
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control ugActivities', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'), 'value' => $competence['activity_id'], 'id' => '')); ?>
								<?php else: ?>
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control ugActivities hidden', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'), 'id' => '')); ?>
								<?php endif; ?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Competence.hierarchy.', array('class' => 'form-control hierarchies', 'label' => false, 'options' => Configure::read('Competences.hierarchies'), 'empty' => __('Select a hierarchy'), 'value' => $competence['hierarchy'], 'id' => '')); ?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="portlet light bg-grey-cararra element empty">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-spinner fa-spin hidden"></i>
							<strong><span class="sector"></span></strong>
							<span class="job"></span>
							<span class="hierarchy"></span>
							<span class="activity"></span>
						</div>
						<div class="actions">
							<a href="" class="btn btn-circle btn-default add-element"><i class="fa fa-plus"></i> <?php echo __('Add a competence');?></a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<div class="col-md-3">
								<?php echo $this->Form->input('Competence.model.', array('type' => 'hidden', 'value' => 'user', 'class' => 'model')) ; ?>
								<?php echo $this->Form->input('Competence.id.', array('type' => 'hidden', 'class' => 'id')); ?>
								<?php echo $this->Form->input('Competence.user_id.', array('type' => 'hidden', 'value' => $this->request->data['User']['id'], 'class' => 'user_id')); ?>
								<?php echo $this->Form->input('Competence.sector.', array('class' => 'form-control sector', 'label' => false, 'options' => Configure::read('Competences.sectors'), 'empty' => __('Select a sector'))); ?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control hidden fb_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.fb_jobs'), 'empty' => __('Select a job'))); ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control hidden logistics_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => __('Select a job'))); ?>
								<?php echo $this->Form->input('Competence.job.', array('class' => 'form-control hidden animation_jobs jobs', 'label' => false, 'options' => Configure::read('Competences.animation_jobs'), 'empty' => __('Select a job'))); ?>
							</div>
							<div class="col-md-3 activities hidden">
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control hidden ubicActivities', 'label' => false, 'options' => $activities['ubic'], 'empty' => __('Select an activity'))); ?>
								<?php echo $this->Form->input('Competence.activity.', array('class' => 'form-control hidden ugActivities', 'label' => false, 'options' => $activities['ug'], 'empty' => __('Select an activity'))); ?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Competence.hierarchy.', array('class' => 'form-control hidden hierarchies', 'label' => false, 'options' => Configure::read('Competences.hierarchies'), 'empty' => __('Select a hierarchy'))); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="tab-pane" id="modules">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-graduation-cap"></i><?php echo __('Modules'); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table-scrollable">
						<table class="table table-striped table-hover">
						<thead>
						<tr>
							<th>#</th>
							<th><?php echo __('Name'); ?></th>
							<th><?php echo __('Followed'); ?></th>
							<th><?php echo __('Suitable'); ?></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($modules as $key => $module): ?>
						<tr>
							<td><?php echo $key + 1; ?></td>
							<td><?php echo $module['Module']['name']; ?></td>
							<td><?php echo $this->Form->input('module', array('type' => 'checkbox','label' => false, 'div' => false, 'class' => 'input-inline module followed', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small', 'data-user-id' => $this->request->data['User']['id'], 'data-module-id' => $module['Module']['id']));?>
								<input type="hidden" class="module-user-id-followed">
							</td>
							<td>
								<div class="btn-group suitable-modules" data-toggle="buttons" data-module-id="<?php echo $module['Module']['id']; ?>" data-user-id="<?php echo $this->request->data['User']['id']; ?>">
									<label class="btn btn-default btn-xs yes">
									<?php echo $this->Form->radio('test', array('suitable' => __('Yes')), $attributes1);?>
									</label>
									<label class="btn btn-default btn-xs no">
									<?php echo $this->Form->radio('test', array('not_suitable' => __('No')), $attributes1);?>
									</label>
									<label class="btn btn-default btn-xs maybe">
									<?php echo $this->Form->radio('test', array('maybe_suitable' => __("Don't know")), $attributes1);?>
									</label>
									<input type="hidden" class="module-user-id-suitable">
								</div>
							</td>
						</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="levelsSignification" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Levels signification'); ?></h4>
			</div>
			<div class="modal-body">
			<h4><?php echo __('Basic speaker'); ?></h4>
			<ul>
			 	<li>A1 Breakthrough or beginner</li>
			 	<li>A2 Waystage or elementary</li>
			 </ul>
			<h4><?php echo __('Independent speaker'); ?></h4>
			<ul>
				<li>B1 Threshold or intermediate</li>
				<li>B2 Vantage or upper intermediate</li>
			</ul>
			<h4><?php echo __('Proficient speaker'); ?></h4>
			<ul>
				<li>C1 Effective Operational Proficiency or advanced</li>
			 	<li>C2 Mastery or proficiency</li>
			</ul>
			<p>More info: http://www.deutsch-als-fremdsprache.org/en/faq/323-what-does-language-level-a1-a2-b1-b2-c1-and-c2-mean.html</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.user();';
echo 'Custom.competences();';
echo 'Custom.modules();';
$this->end();
?>
