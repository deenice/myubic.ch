<?php $this->assign('page_title', __('Collaborators')); ?>
<?php $this->assign('page_subtitle', __('Frontal animation')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-trophy"></i><?php echo __('List of all extras'); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Frontend'); ?>
	</div>
</div>