<?php $this->assign('page_title', $this->Html->link(__('Users'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Add a user'));?>
<?php $roles = Configure::read('Users.roles'); ?>
<?php $civilStatus = Configure::read('Users.civilstatus') ?>
<?php $sizes = Configure::read('Users.sizes');?>
<?php $attributes = array('legend' => false, 'label' => false, 'class' => 'toggle', 'value' => 2); ?>
<div class="portlet light bordered">
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<h3 class="form-section"><?php echo __('Connexion data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Username'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('username', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Le nom d'utilisateur doit être écrit en minuscule, sans espace et sans accent!</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('email', array('type' => 'email', 'label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Password'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('password', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Personal data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('First Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('first_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Last Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('last_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Civility'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('civility', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('ContactPeople.civilities'), 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Picture'); ?></label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
								<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new"><?php echo __('Select image'); ?></span>
								<span class="fileinput-exists"><?php echo __('Change'); ?></span>
								<?php echo $this->Form->input('thumbnail', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Gender'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('gender', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Users.gender'), 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date of birth'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('date_of_birth', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Zip'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('city', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Region'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('region', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => Configure::read('Countries')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Mobile'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('mobile', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Business phone'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('business_phone', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Nationality'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('nationality', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Civil status'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('civil_status',array('options' => $civilStatus, 'label' => false, 'empty' => true, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Wedding date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('wedding_date',array('label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Shirt size'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('shirt_size',array('options' => $sizes, 'label' => false, 'empty' => true, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Availability'); ?></label>
					<div class="col-md-9">
						<p class="alert alert-info">
							Le module sera affiché lorsque l'utilisateur sera sauvegardé.
						</p>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('availabilities_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Administrative data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Role'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('role', array('label' => false, 'class' => 'form-control', 'options' => $roles));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Function'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('function', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Ce champ est à remplir pour les collaborateurs fixes.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Trainee'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('trainee', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Section'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.section', array('class' => 'form-control bs-select', 'label' => false, 'multiple' => true, 'options' => $sections)); ?>
						<span class="help-block">Ce champ est à remplir pour les extras.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start of collaboration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_of_collaboration', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End of collaboration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('end_of_collaboration', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Work permit'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('work_permit', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('Users.work_permits')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('AVS number'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('avs_number', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Income tax assessment'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('income_tax_assessment', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Code barême impôt source'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('income_tax_code', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Non professional accident insurance'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('non_professional_accidents_insurance', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Postfinance account number'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('postfinance_account_number', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Bank name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Bank clearing'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_clearing', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP and city of the bank'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('bank_zip_city', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('IBAN number / Bank account'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('iban', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Wealthings data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('UBIC UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ubic_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Festiloc UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('festiloc_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Effet Gourmand UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('eg_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Ubic Group UUID'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ubic_group_uuid', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Competences'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Car'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('car', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Driving licence'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('driving_licence', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Trailer licence'); ?></label>
					<div class="col-md-9">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(1 => __('Yes')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(0 => __('No')), $attributes);?>
							</label>
							<label class="btn btn-default btn-sm">
							<?php echo $this->Form->radio('trailer_licence', array(2 => __("Don't know")), $attributes);?>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Other licences'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('other_licences', array('label' => false, 'class' => 'select2me form-control', 'multiple' => true, 'options' => Configure::read('Users.licences')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Talents'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.talents', array('type' => 'select', 'multiple' => true, 'data-select2-tags' => '', 'class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Languages'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.french_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('French'))); ?>
						<span class="help-block">
							<?php echo $this->Html->link(__('Levels signification'), '#levelsSignification', array('data-toggle' => 'modal')); ?>
						</span>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.german_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('German'))); ?>
					</div>
					<div class="col-md-3">
						<?php echo $this->Form->input('User.english_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('English'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Other mastered languages'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('User.other_languages', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Frontal animation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('frontend_animation', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Actual occupation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('actual_occupation', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Professional experience'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('professional_experience', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Internal data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Internal remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('internal_remarks', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Ce champ n'est pas montré aux extras!</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Minimal salary'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('minimal_salary', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<!--div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Group'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('group_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $groups));?>
					</div>
				</div-->
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<div class="modal fade" id="levelsSignification" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Levels signification'); ?></h4>
			</div>
			<div class="modal-body">
			<h4><?php echo __('Basic speaker'); ?></h4>
			<ul>
			 	<li>A1 Breakthrough or beginner</li>
			 	<li>A2 Waystage or elementary</li>
			 </ul>
			<h4><?php echo __('Independent speaker'); ?></h4>
			<ul>
				<li>B1 Threshold or intermediate</li>
				<li>B2 Vantage or upper intermediate</li>
			</ul>
			<h4><?php echo __('Proficient speaker'); ?></h4>
			<ul>
				<li>C1 Effective Operational Proficiency or advanced</li>
			 	<li>C2 Mastery or proficiency</li>
			</ul>
			<p>More info: http://www.deutsch-als-fremdsprache.org/en/faq/323-what-does-language-level-a1-a2-b1-b2-c1-and-c2-mean.html</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php
$this->start('init_scripts');
echo 'Custom.user();';
$this->end();
?>
