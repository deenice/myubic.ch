<?php $this->assign('page_title', $this->Html->link(__('Users'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $user['User']['full_name']);?>
<?php
$usersCivilStatus = Configure::read('Users.civilstatus');
$languagesLevels = Configure::read('Languages.levels');
$sizes = Configure::read('Users.sizes');
$assessments = array('yes' => __('Yes'), 'no' => __('No'), 'undefined' => __("Don't know"));
?>
<div class="row profile">
	<div class="col-md-12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom tabbable-bordered">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#overview" data-toggle="tab"><?php echo __('Overview'); ?></a>
				</li>
				<li>
					<a href="#history" data-toggle="tab"><?php echo __('History'); ?></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="overview">
					<div class="row">
						<div class="col-md-3">
							<ul class="list-unstyled profile-nav">
								<li>
									<?php if(!empty($thumbnail)): //debug($thumbnail) ?>
									<?php echo $this->Html->image(DS . $thumbnail[0]['Document']['url'], array('class' => 'img-responsive img-thumbnail')); ?>
									<?php else: ?>
									<img src="http://www.Userhold.it/400x300/EFEFEF/AAAAAA&amp;text=no+image" alt="" class="img-responsive" />
									<?php endif; ?>
								</li>
							</ul>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12 profile-info">
									<h1>
										<?php echo $user['User']['full_name']; ?>
										<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('controller' => 'users', 'action' => 'edit', $user['User']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
									</h1>
									<div class="col-md-3">
										<ul class="list-unstyled">
											<?php if(!empty($user['User']['email'])): ?>
											<li>
												<i class="fa fa-envelope"></i> <?php echo $user['User']['email']; ?>
											</li>
											<?php endif; ?>
											<?php if(!empty($user['User']['date_of_birth'])): ?>
											<li>
												<i class="fa fa-gift"></i> <?php echo $this->Time->format($user['User']['date_of_birth'], '%d %B %Y'); ?>
											</li>
											<?php endif; ?>
											<?php if(!empty($user['User']['function'])): ?>
											<li>
												<i class="fa fa-star-o"></i> <?php echo $user['User']['function']; ?>
											</li>
											<?php endif; ?>
										</ul>
									</div>
								</div>
								<!--end col-md-8-->
							</div>
							<!--end row-->
							<div class="tabbable tabbable-custom tabbable-custom-profile">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#profile" data-toggle="tab"><?php echo __('Profile'); ?></a>
									</li>
									<li>
										<a href="#availabilities" data-toggle="tab"><?php echo __('Availabilities'); ?></a>
									</li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="profile">
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
												<tbody>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Personal data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Name'); ?></strong></td>
														<td><?php echo $user['User']['full_name']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Date of birth'); ?></strong></td>
														<td><?php echo $this->Time->format($user['User']['date_of_birth'], '%d %B %Y'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Address'); ?></strong></td>
														<td>
															<?php echo $user['User']['address']; ?><br />
															<?php echo $user['User']['zip']; ?> <?php echo $user['User']['city']; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Phone'); ?></strong></td>
														<td>
															<?php echo $user['User']['phone']; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Nationality'); ?></strong></td>
														<td>
															<?php echo $user['User']['nationality']; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Civil status'); ?></strong></td>
														<td>
															<?php echo $usersCivilStatus[$user['User']['civil_status']]; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Shirt size'); ?></strong></td>
														<td>
															<?php echo $sizes[$user['User']['shirt_size']]; ?>
														</td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Administrative data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Function'); ?></strong></td>
														<td><?php echo $user['User']['function']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Start of collaboration'); ?></strong></td>
														<td><?php echo $this->Time->format($user['User']['start_of_collaboration'], '%d %B %Y'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('End of collaboration'); ?></strong></td>
														<td><?php echo $this->Time->format($user['User']['end_of_collaboration'], '%d %B %Y'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('AVS number'); ?></strong></td>
														<td><?php echo $user['User']['avs_number']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Income tax assessment'); ?></strong></td>
														<td><?php echo $assessments[$user['User']['income_tax_assessment']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Non professional accident insurance'); ?></strong></td>
														<td><?php echo $user['User']['non_professional_accidents_insurance'] ? __('Yes'):__('No'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Postfinance account number'); ?></strong></td>
														<td><?php echo $user['User']['postfinance_account_number']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Bank name'); ?></strong></td>
														<td><?php echo $user['User']['bank_name']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Bank clearing'); ?></strong></td>
														<td><?php echo $user['User']['bank_clearing']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('ZIP and city of the bank'); ?></strong></td>
														<td><?php echo $user['User']['bank_zip_city']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('IBAN number / Bank account'); ?></strong></td>
														<td><?php echo $user['User']['iban']; ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Competences'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Competences'); ?></strong></td>
														<td>
															<ul class="list-unstyled">
															<?php foreach($user['Competence'] as $competence): ?>
															<li><i class="fa fa-caret-right"></i> <?php echo $competence['name']; ?></li>	
															<?php endforeach; ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Driving licence'); ?></strong></td>
														<td><?php echo $user['User']['driving_licence'] ? __('Yes'):__('No'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Trailer licence'); ?></strong></td>
														<td><?php echo $user['User']['trailer_licence'] ? __('Yes'):__('No'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Car'); ?></strong></td>
														<td><?php echo $user['User']['car'] ? __('Yes'):__('No'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Talents'); ?></strong></td>
														<td>
															<ul class="list-unstyled">
															<?php foreach($user['Talents'] as $talent): ?>
															<li><i class="fa fa-check font-green"></i> <?php echo $talent['value']; ?></li>
															<?php endforeach; ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('French level'); ?></strong></td>
														<td><?php echo $languagesLevels[$user['User']['french_level']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('German level'); ?></strong></td>
														<td><?php echo $languagesLevels[$user['User']['german_level']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('English level'); ?></strong></td>
														<td><?php echo $languagesLevels[$user['User']['english_level']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Other mastered languages'); ?></strong></td>
														<td><?php echo $user['User']['other_languages']; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="availabilities">
										<div class="portlet-body">
											<?php if($user['User']['availabilities_infos']): ?>
											<div class="note note-info">
												<p><?php echo $user['User']['availabilities_infos'] ?></p>
											</div>
											<?php endif; ?>
											<table class="table table-bordered">
												<tbody>
													<tr>
														<td></td>
														<td><?php echo __('Morning') ?></td>
														<td><?php echo __('Afternoon') ?></td>
														<td><?php echo __('Evening') ?></td>
													</tr>
													<tr>
														<td><?php echo __('Monday'); ?></td>
														<?php echo $this->Availability->generateLine(array(106,105,107), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(108,109,110), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(111,112,113), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Tuesday'); ?></td>
														<?php echo $this->Availability->generateLine(array(114,115,116), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(117,118,119), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(120,121,122), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Wednesday'); ?></td>
														<?php echo $this->Availability->generateLine(array(123,124,125), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(126,127,128), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(129,130,131), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Thursday'); ?></td>
														<?php echo $this->Availability->generateLine(array(132,133,134), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(135,136,137), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(138,139,140), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Friday'); ?></td>
														<?php echo $this->Availability->generateLine(array(141,142,143), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(144,145,146), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(147,148,149), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Saturday'); ?></td>
														<?php echo $this->Availability->generateLine(array(150,151,152), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(153,154,155), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(156,157,158), $user['Availabilities']); ?>
													</tr>
													<tr>
														<td><?php echo __('Sunday'); ?></td>
														<?php echo $this->Availability->generateLine(array(159,160,161), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(162,163,164), $user['Availabilities']); ?>
														<?php echo $this->Availability->generateLine(array(165,166,167), $user['Availabilities']); ?>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--tab_1_2-->
				<div class="tab-pane" id="history">
					<p>Coming soon</p>
				</div>
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/dropzone/css/dropzone.css');
echo $this->Html->css('/metronic/global/plugins/fancybox/source/jquery.fancybox.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
echo $this->Html->css('/metronic/pages/css/portfolio.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
echo $this->Html->css('/metronic/pages/css/profile.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/dropzone/dropzone.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modal.js');

$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.init();';
$this->end();
?>
