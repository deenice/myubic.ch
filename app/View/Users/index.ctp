<?php $this->assign('page_title', $this->Html->link(__('Users'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="tabbable-custom tabbable-tabdrop" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#fixedCollaborators" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Fixed collaborators'); ?></a>
		</li>
		<li>
			<a href="#temporaryCollaborators" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Extras'); ?></a>
		</li>
		<li>
			<a href="#candidateCollaborators" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Candidates'); ?></a>
		</li>
		<li>
			<a href="#talents" data-toggle="tab"><i class="fa fa-star"></i> <?php echo __('Talents'); ?></a>
		</li>
		<li>
			<a href="#unsuitable" data-toggle="tab"><i class="fa fa-thumbs-o-down"></i> <?php echo __('Unsuitable'); ?></a>
		</li>
		<li>
			<a href="#retired" data-toggle="tab"><i class="fa fa-sign-out"></i> <?php echo __('Retired'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="fixedCollaborators">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('FixedUser', array(), array('aaSorting' => array(array( 1, 'asc' )))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="temporaryCollaborators">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Extras', array(), array('aaSorting' => array(array( 1, 'asc' )))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="candidateCollaborators">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Candidates', array(), array('aaSorting' => array(array( 1, 'asc' )))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="talents">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Talents'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="unsuitable">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Unsuitable'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="retired">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a user') . ' <i class="fa fa-plus"></i>', array('controller' => 'users', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Retired'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.user();';
$this->end();
?>
