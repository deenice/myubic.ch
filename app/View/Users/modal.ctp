<div class="modal-body">
	<div class="row">
		<div class="col-md-9">
			<h3 style="display:inline-block; margin-top: 0"><?php echo $user['User']['full_name']; ?></h3>
		</div>
		<div class="col-md-3 text-right">
		<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit')), array('controller' => 'users', 'action' => 'edit', $user['User']['id']), array('class' => 'btn btn-sm default', 'escape' => false)); ?>
		</div>
		<div class="col-md-12">
			<table class="table table-striped table-bordered">
				<tbody>
					<tr>
						<td><?php echo __('Email'); ?></td>
						<td><a href="mailto:<?php echo $user['User']['email']; ?>"><?php echo $user['User']['email']; ?></a></td>
						<td rowspan="3" style="width: 120px">
							<?php if(!empty($user['Portrait'][0]['url'])) echo $this->Html->image($this->ImageSize->crop(DS . $user['Portrait'][0]['url'], 120, 120)); ?>
						</td>
					</tr>
					<?php if(!empty($user['User']['phone'])): ?>
					<tr>
						<td><?php echo __('Phone'); ?></td>
						<td><?php echo $user['User']['phone']; ?></td>
					</tr>
					<?php endif; ?>
					<?php if(!empty($user['User']['mobile'])): ?>
					<tr>
						<td><?php echo __('Mobile'); ?></td>
						<td><?php echo $user['User']['mobile']; ?></td>
					</tr>
					<?php endif; ?>
					<?php if(!empty($user['User']['date_of_birth'])): ?>
					<tr>
						<td><?php echo __('Age'); ?></td>
						<td>
							<?php echo __('%s years old', date_diff(date_create($user['User']['date_of_birth']), date_create('today'))->y); ?> -
							<?php echo $this->Time->format('d.m.Y', $user['User']['date_of_birth']); ?>
						</td>
					</tr>
					<?php endif; ?>
					<tr>
						<td><?php echo __('Address'); ?></td>
						<td colspan="2">
							<?php echo $user['User']['address']; ?>, <?php echo $user['User']['zip_city']; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo __('Shirt size'); ?></td>
						<td colspan="2">
							<?php echo isset($sizes[$user['User']['shirt_size']]) ? $sizes[$user['User']['shirt_size']] : __('No shirt size defined'); ?>
						</td>
					</tr>
					<tr>
						<td><?php echo __('Languages'); ?></td>
						<td colspan="2">
							<?php if(!empty($user['User']['french_level'])): ?>
								<span class="language">
									<?php echo __('French') ?> <?php echo strtoupper($user['User']['french_level']); ?>
								</span>
							<?php endif; ?>
							<?php if(!empty($user['User']['german_level'])): ?>
								<span class="language">
									<?php echo __('German') ?> <?php echo strtoupper($user['User']['german_level']); ?>
								</span>
							<?php endif; ?>
							<?php if(!empty($user['User']['english_level'])): ?>
								<span class="language">
									<?php echo __('English') ?> <?php echo strtoupper($user['User']['english_level']); ?>
								</span>
							<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td><?php echo __('Car'); ?></td>
						<td colspan="2"><?php echo $user['User']['car'] ? __('Yes') : __('No'); ?></td>
					</tr>
					<tr>
						<td><?php echo __('Driving licence'); ?></td>
						<td colspan="2"><?php echo $user['User']['driving_licence'] ? __('Yes') : __('No'); ?></td>
					</tr>
					<tr>
						<td><?php echo __('Trailer licence'); ?></td>
						<td colspan="2"><?php echo $user['User']['trailer_licence'] ? __('Yes') : __('No'); ?></td>
					</tr>
					<tr>
						<td><?php echo __('Availabilities'); ?></td>
						<td colspan="2">
							<?php echo $this->element('get_staff_schedule_days', array('schedule' => $user['ScheduleDay'])); ?>
						</td>
					</tr>
					<?php if(!empty($user['Job'])): ?>
					<tr>
						<td><?php echo __('History'); ?></td>
						<td colspan="2">
							<div class="table-modal table-modal--jobs">
								<table class="table table-condensed table-striped">
									<thead>
										<tr>
											<th class="hidden">&nbsp;</th>
											<th class="date"><?php echo __('Date'); ?></th>
											<th class="job"><?php echo __('Job'); ?></th>
											<th class="event"><?php echo __('Event'); ?></th>
											<th class="client"><?php echo __('Client'); ?></th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($user['Job'] as $job): ?>
										<tr>
											<td class="hidden"><?php echo strtotime($job['Event']['confirmed_date']); ?></td>
											<td><?php echo $this->Time->format($job['Event']['confirmed_date'], '%d.%m.%Y'); ?></td>
											<td><?php echo $job['name']; ?></td>
											<td><?php echo $this->Html->link( empty($job['Event']['code_name']) ? $job['Event']['name'] : $job['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $job['Event']['id']), array('target' => '_blank', 'escape' => false)); ?></td>
											<td><?php echo $this->Html->link( $job['Event']['Client']['name'], array('controller' => 'clients', 'action' => 'view', $job['Event']['Client']['id']), array('target' => '_blank', 'escape' => false)); ?></td>
										</tr>
									<?php endforeach;?>
								</tbody>
							</table>
							</div>
						</td>
					</tr>
					<?php endif; ?>
					<?php if(!empty($user['Competence'])): ?>
					<tr>
						<td><?php echo __('Competences'); ?></td>
						<td colspan="2">
							<div class="table-modal table-modal--competences">
								<table class="table table-condensed table-striped">
									<thead>
										<tr>
											<th class="sector"><?php echo __('Sector'); ?></th>
											<th class="job"><?php echo __('Job'); ?></th>
											<th class="activity"><?php echo __('Activity'); ?> / <?php echo __('F&B Module'); ?></th>
											<th class="hierarchy"><?php echo __('Hierarchy'); ?></th>
										</tr>
									</thead>
									<tbody>
									<?php foreach($user['Competence'] as $competence): ?>
										<tr>
											<td><?php echo $sectors[$competence['sector']]; ?></td>
											<td><?php echo isset($jobs[$competence['job']]) ? $jobs[$competence['job']] : ''; ?></td>
											<td>
												<?php echo !empty($competence['Activity']) ? $competence['Activity']['name'] : ''; ?>
												<?php echo !empty($competence['FBModule']) ? $competence['FBModule']['name'] : ''; ?>
											</td>
											<td><?php echo $competence['hierarchy'] < 5 && !empty($competence['hierarchy']) ? $hierarchies[$competence['hierarchy']] : ''; ?></td>
										</tr>
									<?php endforeach;?>
								</tbody>
							</table>
							</div>
						</td>
					</tr>
					<?php endif; ?>
					<tr class="hidden">
						<td><?php echo __('Notes'); ?></td>
						<td colspan="2">si niveau high => mettre icone ou ligne en rouge</td>
					</tr>
					<tr class="hidden">
						<td><?php echo __('Rating'); ?></td>
						<td colspan="2"></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
