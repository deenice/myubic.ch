<!-- BEGIN LOGIN FORM -->
<?php echo $this->Form->create('User', array('class' => 'login-form')); ?>
<h3 class="form-title"><?php echo __("Login to your account"); ?></h3>
<div class="alert alert-danger display-hide">
	<button class="close" data-close="alert"></button>
	<span>Enter any username and password.</span>
</div>
<div class="form-group">
	<label class="control-label visible-ie8 visible-ie9"><?php echo __('Username'); ?></label>
	<div class="input-icon">
		<i class="fa fa-user"></i>
		<?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'class' => 'form-control placeholder-no-fix', 'placeholder' => __('Username'))); ?>
	</div>
</div>
<div class="form-group">
	<label class="control-label visible-ie8 visible-ie9"><?php echo __('Password'); ?></label>
	<div class="input-icon">
		<i class="fa fa-user"></i>
		<?php echo $this->Form->input('password', array('label' => false, 'div' => false, 'class' => 'form-control placeholder-no-fix', 'placeholder' => __('Password'))); ?>
	</div>
</div>
<div class="form-actions">
	<label class="checkbox">
	<?php echo $this->Form->checkbox('remember_me', array('value' => 1, 'label' => false, 'checked' => true)); ?><?php echo __("Remember me"); ?></label>
	<button type="submit" class="btn blue pull-right">
	<?php echo __('Login'); ?> <i class="m-icon-swapright m-icon-white"></i>
	</button>
</div>
<div class="create-account">
	<p>
		 Don't have an account yet ?&nbsp; <?php echo $this->Html->link(__('Create an account'), array('action' => 'register', 'controller' => 'users'), array('id' => 'register-btn')); ?>
	</p>
</div>
<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/pages/css/login-soft.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js');
echo $this->Html->script('/metronic/global/plugins/backstretch/jquery.backstretch.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');

$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/pages/scripts/login-soft.js');
$this->end();
$this->start('init_scripts');
echo 'Login.init();';
echo '// init background slide images
       $.backstretch([
        "'.$this->webroot.'metronic/pages/media/bg/1.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/2.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/3.jpg",
        "'.$this->webroot.'metronic/pages/media/bg/4.jpg"
        ], {
          fade: 1000,
          duration: 8000
    }
    );
';
$this->end();
?>
