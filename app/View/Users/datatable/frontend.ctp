<?php
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
		    '<i class="fa fa-search"></i> ' . __("View"), 
		    array('controller' => 'users', 'action' => 'view', $result['User']['id']), 
		    array('class' => 'btn default btn-xs', 'escape' => false)
		);
	$actions .= $this->Html->tag('');
	$actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"), 
	    array('controller' => 'users', 'action' => 'edit', $result['User']['id']), 
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	    );

	$portrait = empty($result['Portrait'][0]['id']) ? '' : $this->Html->image($this->ImageSize->crop($result['Portrait'][0]['url'], 60, 60));

	$sections = '';
	if(!empty($result['Section'])){
		foreach($result['Section'] as $section){
            $sections .= $this->Html->tag('span', $section['value'], array('class' => 'btn btn-xs bg-grey'));
        }
	}

    $this->dtResponse['aaData'][] = array(
        $portrait,
        $result['User']['full_name'],
        $result['User']['email'],
        $result['User']['phone'],
        $sections,
        $actions
    );
}