<?php
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'users', 'action' => 'modal', $result['User']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-user-ajax')
	);
	$actions .= $this->Html->tag('');
	$actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'users', 'action' => 'edit', $result['User']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );
	$actions .= $this->Html->link('<i class="fa fa-trash-o"></i> ' . __("Delete"),
    array('controller' => 'users', 'action' => 'delete', $result['User']['id']),
    array('class' => 'btn btn-danger btn-xs', 'escape' => false)
  );

	$portrait = empty($result['Portrait'][0]['id']) ? '' : $this->Html->image($this->ImageSize->crop($result['Portrait'][0]['url'], 60, 60));

  $this->dtResponse['aaData'][] = array(
    $portrait,
    $result['User']['full_name'],
    $result['User']['email'],
    $actions
  );
}
