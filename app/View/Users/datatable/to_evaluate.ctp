<?php
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
		    '<i class="fa fa-thumbs-o-up"></i> ' . __("Rise?"), 
            array('controller' => 'users', 'action' => 'rise', $result['User']['id'], $result['Job']['id']), 
            array('class' => 'btn default btn-xs', 'escape' => false)
		);
	$actions .= $this->Html->tag('');
	$actions .= $this->Html->link(
		'<i class="fa fa-times"></i> ' . __("Keep same hierarchy"), 
        array('controller' => 'users', 'action' => 'rise', $result['User']['id'], $result['Job']['id'], 0), 
        array('class' => 'btn default btn-xs', 'escape' => false)
	    );

	$portrait = empty($result['User']['Portrait'][0]['id']) ? '' : $this->Html->image($this->ImageSize->crop($result['User']['Portrait'][0]['url'], 60, 60));


    $this->dtResponse['aaData'][] = array(
        $portrait,
        $result['User1']['first_name'] . ' ' . $result['User1']['last_name'],
        $result['Job']['name'],
        $result['Event']['name'],
        $this->Time->format($result['Event']['confirmed_date'], '%d %B %Y'),
        $result['Client1']['name'],
        $actions
    );
}