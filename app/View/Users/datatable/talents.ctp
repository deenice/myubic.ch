<?php
foreach ($dtResults as $result) {

	$portrait = empty($result['Portrait'][0]['id']) ? '' : $this->Html->image($this->ImageSize->crop($result['Portrait'][0]['url'], 60, 60));


    $this->dtResponse['aaData'][] = array(
        $portrait,
        $result['Tag']['value'],
        $result['User']['full_name']
    );
}