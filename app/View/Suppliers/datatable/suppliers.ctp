<?php
$countries = Configure::read('Countries');
foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'suppliers', 'action' => 'edit', $result['Supplier']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

  $this->dtResponse['aaData'][] = array(
    $result['Supplier']['name'],
    $actions
  );
}
