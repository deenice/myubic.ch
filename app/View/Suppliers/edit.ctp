<?php $this->assign('page_title', $this->Html->link(__('Suppliers'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $this->request->data['Supplier']['name']);?>
<div class="tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#contact" data-toggle="tab"><i class="fa fa-phone"></i> <?php echo __('Contact person(s)'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
		<?php echo $this->Form->create('Supplier', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<?php echo $this->Form->input('Supplier.id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
					<?php echo $this->Form->input('Company..id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies, 'value' => $supplierCompanies));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Logo'); ?></label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">
								<?php if(!empty($this->request->data['Logo']['url'])): ?>
									<?php echo $this->Html->image(DS . $this->request->data['Logo']['url'], array('max-height' => 140, 'width' => 140)); ?>
								<?php else: ?>
									<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+logo" alt=""/>
								<?php endif; ?>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new"><?php echo __('Select image'); ?></span>
								<span class="fileinput-exists"><?php echo __('Change'); ?></span>
								<?php echo $this->Form->input('logo', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group" data-commune-select2>
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array($this->request->data['Supplier']['zip_city']), 'value' => empty($this->request->data['Supplier']['zip_city']) ? '' : $this->request->data['Supplier']['zip_city']));?>
						<?php echo $this->Form->input('zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'value' => $this->request->data['Supplier']['country'], 'options' => Configure::read('Countries')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Website'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('website', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<?php if($this->request->data['Supplier']['type'] == 'client'): ?>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Fidelity discount'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('festiloc_fidelity_discount', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline'));?>
						<span class="help-text">%</span>
						<span class="help-block">Pour les clients Festiloc</span>
					</div>
				</div>
				<?php endif; ?>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
        <div class="form-group">
          <label class="control-label col-md-3"><?php echo __('External supplier'); ?></label>
          <div class="col-md-9">
            <?php echo $this->Form->input('external_supplier', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
          </div>
        </div>
				<h3 class="form-section">Facturation</h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Business name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_business_name', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">À remplir si pour l'adresse de facturation la raison sociale serait différente.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_address', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group" data-commune-select2>
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_zip_city', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array($this->request->data['Supplier']['invoice_zip_city']), 'value' => empty($this->request->data['Supplier']['invoice_zip_city']) ? '' : $this->request->data['Supplier']['invoice_zip_city']));?>
						<?php echo $this->Form->input('invoice_zip', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'zip', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('invoice_city', array('label' => false, 'div' => false, 'class' => 'form-control', 'data-name' => 'city', 'type' => 'hidden')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('invoice_country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'value' => empty($this->request->data['Supplier']['invoice_country']) ? 'CH' : $this->request->data['Supplier']['invoice_country'], 'options' => Configure::read('Countries')));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
		</div>
		<div class="tab-pane" id="contact">
			<?php echo $this->Form->create('ContactPeople', array('url' => array('controller' => 'contact_peoples', 'action' => 'save'))); ?>
			<?php echo $this->Form->input('Client.id', array('type' => 'hidden', 'value' => $this->request->data['Supplier']['id'])); ?>
			<?php if(!empty($this->request->data['ContactPeople'])): ?>
			<?php foreach($this->request->data['ContactPeople'] as $k => $contact): ?>
			<div class="portlet light bg-grey-cararra contact-person">
				<div class="portlet-title">
					<div class="caption">
						<strong><span class="name"><?php echo $contact['full_name']; ?></span></strong>
						<span class="function"><?php echo $contact['function']; ?></span>
						<span class="department"><?php echo $contact['department']; ?></span>
					</div>
					<div class="actions">
						<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a contact person'), array('controller' => 'contactpersons', 'action' => 'add'), array('class' => 'btn btn-circle btn-default add-contact-person', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> ' . __('Remove a contact person'), array('controller' => 'contact_peoples', 'action' => 'delete', $contact['id']), array('class' => 'btn btn-circle btn-default remove-contact-person', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group row">
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.id.', array('type' => 'hidden', 'value' => $contact['id'])); ?>
							<?php echo $this->Form->input('ContactPeople.status.', array('type' => 'hidden', 'value' => 1)); ?>
							<?php echo $this->Form->input('ContactPeople.first_name.', array('label' => __('First name'), 'class' => 'form-control', 'value' => $contact['first_name']));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.last_name.', array('label' => __('Last name'), 'class' => 'form-control', 'value' => $contact['last_name']));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.civility.', array('label' => __('Civility'), 'class' => 'form-control bs-select', 'value' => $contact['civility'], 'empty' => true, 'options' => Configure::read('ContactPeople.civilities')));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.phone.', array('label' => __('Phone'), 'class' => 'form-control', 'value' => $contact['phone']));?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.email.', array('type' => 'email', 'label' => array('text' => __('Email'), 'class' => 'control-label'),'div' => array('class' => 'input input-icon right'), 'class' => 'form-control', 'value' => $contact['email']));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.function.', array('label' => __('Function'), 'class' => 'form-control', 'value' => $contact['function']));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.department.', array('label' => __('Department'), 'class' => 'form-control', 'value' => $contact['department']));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.language.', array('label' => __('Language of correspondence'), 'class' => 'form-control bs-select','empty' => true, 'value' => $contact['language'], 'options' => Configure::read('ContactPeople.languages')));?>
						</div>
						<!-- <div class="col-md-3">
							<label><?php echo __('Added on'); ?></label>
							<input type="text" readonly="readonly" class="form-control" value="<?php echo $this->Time->format($contact['modified'], '%d %B %Y %H:%M'); ?>">
						</div> -->
					</div>
				</div>
			</div>
			<?php endforeach; ?>
			<?php endif; ?>
			<div class="portlet light bg-grey-cararra contact-person empty">
				<div class="portlet-title">
					<div class="caption">
						<strong><span class="name"></span></strong>
						<span class="function"></span>
						<span class="department"></span>
					</div>
					<div class="actions">
						<a href="" class="btn btn-circle btn-default add-contact-person"><i class="fa fa-plus"></i> <?php echo __('Add a contact person');?></a>
					</div>
				</div>
				<div class="portlet-body">
					<div class="form-group row">
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.id.', array('type' => 'hidden', 'value' => ''));?>
							<?php echo $this->Form->input('ContactPeople.status.', array('type' => 'hidden', 'value' => 1));?>
							<?php echo $this->Form->input('ContactPeople.first_name.', array('label' => __('First name'), 'class' => 'form-control'));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.last_name.', array('label' => __('Last name'), 'class' => 'form-control'));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.civility.', array('label' => __('Civility'), 'class' => 'form-control bs-select', 'empty' => true, 'options' => Configure::read('ContactPeople.civilities')));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.phone.', array('label' => __('Phone'), 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.email.', array('type' => 'email', 'label' => array('text' => __('Email'), 'class' => 'control-label'), 'class' => 'form-control'));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.function.', array('label' => __('Function'), 'class' => 'form-control'));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.department.', array('label' => __('Department'), 'class' => 'form-control'));?>
						</div>
						<div class="col-md-3">
							<?php echo $this->Form->input('ContactPeople.language.', array('label' => __('Language of correspondence'), 'class' => 'form-control bs-select','empty' => true, 'options' => Configure::read('ContactPeople.languages')));?>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions" style="margin-top: 100px">
				<div class="row">
					<div class="col-md-12 text-center">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.clients();';
$this->end();
?>
