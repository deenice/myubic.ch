<?php $this->assign('page_title', $user['User']['full_name']); ?>
<?php $this->assign('page_subtitle', __('Home')); ?>
<?php //debug($user); ?>
<div class="row">
	<div class="col-md-3">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet bordered light profile-sidebar-portlet">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic">
						<?php echo $this->Html->image($this->ImageSize->crop($user['Portrait'][0]['url'], 200, 200), array('class' => 'img-responsive')); ?>
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name"><?php echo $user['User']['full_name']; ?></div>
						<div class="profile-usertitle-job"><?php echo $user['User']['function']; ?></div>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="profile-userbuttons">
						<?php foreach($user['Section'] as $section): ?>
						<button type="button" class="btn blue-madison btn-sm margin-bottom-5"><?php echo $section['value']; ?></button>
						<?php endforeach; ?>
						<div class="clearfix"></div>
						<?php if(!empty($user['User']['french_level']) AND $user['User']['french_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('French'); ?></button>
						<?php endif; ?>
						<?php if(!empty($user['User']['german_level']) AND $user['User']['german_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('German'); ?></button>
						<?php endif; ?>
						<?php if(!empty($user['User']['english_level']) AND $user['User']['english_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('English'); ?></button>
						<?php endif; ?>
					</div>
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu"></div>
					<!-- END MENU -->
				</div>

			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="row">
			<?php if(!empty($answers) && false): ?>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<span class="caption"><?php echo __('Waiting answers'); ?></span>
					</div>
					<div class="portlet-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><?php echo __('Date') ?></th>
									<th><?php echo __('Event') ?></th>
									<th><?php echo __('Job') ?></th>
									<th style="width: 35%"><?php echo __('My answer') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($answers as $answer): ?>
								<tr>
									<td><?php echo $this->Time->format($answer['RoundStep']['Round']['Job']['Event']['confirmed_date'], '%d %B %Y'); ?></td>
									<td>
										<?php echo $answer['RoundStep']['Round']['Job']['Event']['code_name']; ?><br />
										<?php echo $answer['RoundStep']['Round']['Job']['Event']['Client']['name']; ?>
									</td>
									<td>
										<?php echo $answer['RoundStep']['Round']['Job']['name']; ?><br />
										<?php echo $answer['RoundStep']['Round']['Job']['start_time']; ?>
										<?php echo $answer['RoundStep']['Round']['Job']['end_time']; ?>
									</td>
									<td>
										<?php echo $this->Html->link(__('Available'), array('controller' => 'round_answers', 'action' => 'process', $answer['RoundAnswer']['token'], 'interested'), array('class' => 'btn btn-xs btn-success')); ?>
										<?php echo $this->Html->link(__('Not interested'), array('controller' => 'round_answers', 'action' => 'process', $answer['RoundAnswer']['token'], 'not_interested'), array('class' => 'btn btn-xs btn-warning')); ?>
										<?php echo $this->Html->link(__('Not available'), array('controller' => 'round_answers', 'action' => 'process', $answer['RoundAnswer']['token'], 'not_available'), array('class' => 'btn btn-xs btn-danger')); ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(!empty($myEvents)): ?>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<span class="caption"><?php echo __('My next events'); ?></span>
					</div>
					<div class="portlet-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><?php echo __('Date') ?></th>
									<th><?php echo __('Event') ?></th>
									<th><?php echo __('Job') ?></th>
									<th><?php echo __('Schedule') ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($myEvents as $event): ?>
								<tr>
									<td><?php echo $this->Time->format($event['Job']['Event']['confirmed_date'], '%d %B %Y'); ?></td>
									<td>
										<?php echo $event['Job']['Event']['code_name']; ?><br />
										<?php echo $event['Job']['Event']['Client']['name']; ?>
									</td>
									<td><?php echo $event['Job']['name']; ?></td>
									<td><?php echo $this->Time->format('H:i', $event['Job']['start_time']); ?> - <?php echo $this->Time->format('H:i', $event['Job']['end_time']); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
