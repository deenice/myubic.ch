<?php $this->assign('page_title', $user['User']['full_name']); ?>
<?php $this->assign('page_subtitle', __('My profile')); ?>
<?php $civilStatus = array('single' => __('Single'), 'married' => __('Married'),'divorced' => __('Divorced'), 'separated' => __('Séparé'), 'widow' => __('Widow')); ?>
<?php $sizes = Configure::read('Users.sizes'); ?>
<?php $attributes = array('legend' => false, 'label' => false, 'class' => 'toggle'); ?>
<?php $attributes1 = array('legend' => false, 'label' => false, 'class' => 'toggle module-user-id-suitable-input', 'hiddenField' => false); ?>
<?php //debug($user); ?>
<div class="row">
	<div class="col-md-3" id="profile-menu">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered profile-sidebar-portlet">
					<!-- SIDEBAR USERPIC -->
					<div class="profile-userpic">
						<?php echo $this->Html->image($this->ImageSize->crop($user['Portrait'][0]['url'], 200, 200), array('class' => 'img-responsive')); ?>
					</div>
					<!-- END SIDEBAR USERPIC -->
					<!-- SIDEBAR USER TITLE -->
					<div class="profile-usertitle">
						<div class="profile-usertitle-name"><?php echo $user['User']['full_name']; ?></div>
						<?php if(!empty($user['User']['function'])): ?>
							<div class="profile-usertitle-job"><?php echo $user['User']['function']; ?></div>
						<?php endif ?>
					</div>
					<!-- END SIDEBAR USER TITLE -->
					<!-- SIDEBAR BUTTONS -->
					<div class="profile-userbuttons">
						<?php foreach($user['Section'] as $section): ?>
						<button type="button" class="btn blue-madison btn-sm margin-bottom-5"><?php echo $section['value']; ?></button>
						<?php endforeach; ?>
						<div class="clearfix"></div>
						<?php if(!empty($user['User']['french_level']) AND $user['User']['french_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('French'); ?></button>
						<?php endif; ?>
						<?php if(!empty($user['User']['german_level']) AND $user['User']['german_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('German'); ?></button>
						<?php endif; ?>
						<?php if(!empty($user['User']['english_level']) AND $user['User']['english_level'] <= 'b2'): ?>
							<button type="button" class="btn red-pink btn-sm"><?php echo __('English'); ?></button>
						<?php endif; ?>
					</div>
					<!-- END SIDEBAR BUTTONS -->
					<!-- SIDEBAR MENU -->
					<div class="profile-usermenu" id="profile-nav">
						<ul class="nav" >
							<li class="active">
								<a href="#connexion-data">
								<i class="icon-link"></i>
								<?php echo __('Connexion data'); ?> </a>
							</li>
							<li>
								<a href="#personal-data">
								<i class="icon-user"></i>
								<?php echo __('Personal data'); ?> </a>
							</li>
							<li>
								<a href="#competences">
								<i class="icon-star"></i>
								<?php echo __('Competences'); ?> </a>
							</li>
							<li>
								<a href="#bank-information">
								<i class="icon-wallet"></i>
								<?php echo __('Bank information') ?> </a>
							</li>
							<li>
								<a href="#availabilities">
								<i class="icon-calendar"></i>
								<?php echo __('Availabilities'); ?> </a>
							</li>
						</ul>
					</div>
					<!-- END MENU -->
				</div>

			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="row">
			<?php echo $this->Form->create('User', array('class' => 'form-horizontal', 'url' => array('controller' => 'users', 'action' => 'edit', $user['User']['id']))); ?>
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('section', array('type' => 'hidden', 'value' => implode(',', $sections))); ?>
			<?php echo $this->Form->input('talents', array('type' => 'hidden', 'value' => implode(',', $talents))); ?>
			<div class="col-md-12" id="connexion-data">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Connexion data'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Username'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('username', array('label' => false, 'class' => 'form-control', 'readonly' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Actual password'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('password_verification', array('label' => false, 'class' => 'form-control', 'type' => 'password'));?>
									<span class="help-block"><?php echo __("Leave blank if you don't want to update your password."); ?></span>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('New password'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('password_update', array('label' => false, 'class' => 'form-control', 'type' => 'password'));?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="personal-data">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Personal data'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('First name'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('first_name', array('label' => false, 'class' => 'form-control'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Last name'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('last_name', array('label' => false, 'class' => 'form-control'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('email', array('label' => false, 'class' => 'form-control'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Gender'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('gender', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Users.gender'), 'empty' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Date of birth'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('date_of_birth', array('type' => 'text', 'label' => false, 'required' => true, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control', 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Zip'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control', 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('City'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('city', array('label' => false, 'class' => 'form-control', 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('phone', array('label' => false, 'class' => 'form-control'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Nationality'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('nationality', array('label' => false, 'class' => 'form-control'));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Civil status'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('civil_status',array('options' => $civilStatus, 'label' => false, 'empty' => true, 'class' => 'form-control bs-select', 'required' => true));?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Shirt size'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('shirt_size',array('options' => $sizes, 'label' => false, 'empty' => true, 'class' => 'form-control bs-select'));?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="competences">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Competences'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Car'); ?></label>
							<div class="col-md-9">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('car', array(1 => __('Yes')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('car', array(0 => __('No')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('car', array(2 => __("Don't know")), $attributes);?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Driving licence'); ?></label>
							<div class="col-md-9">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('driving_licence', array(1 => __('Yes')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('driving_licence', array(0 => __('No')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('driving_licence', array(2 => __("Don't know")), $attributes);?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Trailer licence'); ?></label>
							<div class="col-md-9">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('trailer_licence', array(1 => __('Yes')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('trailer_licence', array(0 => __('No')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('trailer_licence', array(2 => __("Don't know")), $attributes);?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Other licences'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('other_licences', array('label' => false, 'class' => 'select2me form-control', 'multiple' => true, 'options' => Configure::read('Users.licences'), 'value' => !empty($this->request->data['User']['other_licences']) ? explode(',',$this->request->data['User']['other_licences']):''));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Languages'); ?></label>
							<div class="col-md-3">
								<?php echo $this->Form->input('User.french_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('French'))); ?>
								<span class="help-block">
									<?php echo $this->Html->link(__('Levels signification'), '#levelsSignification', array('data-toggle' => 'modal')); ?>
								</span>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('User.german_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('German'))); ?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('User.english_level', array('class' => 'form-control bs-select', 'options' => Configure::read('Languages.levels'), 'empty' => __('Select your level'), 'label' => __('English'))); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Other mastered languages'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('User.other_languages', array('class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Actual occupation'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('actual_occupation', array('class' => 'form-control', 'label' => false)); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Professional experience'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('professional_experience', array('class' => 'form-control', 'label' => false)); ?>
								<span class="help-block"><?php echo __('Student job, training, etc'); ?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="bank-information">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Bank information'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('AVS number'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('avs_number', array('label' => false, 'class' => 'form-control', 'required' => true));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Income tax assessment'); ?></label>
							<div class="col-md-9">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('income_tax_assessment', array(1 => __('Yes')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('income_tax_assessment', array(0 => __('No')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('income_tax_assessment', array(2 => __("Don't know")), $attributes);?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Non professional accident insurance'); ?></label>
							<div class="col-md-9">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('non_professional_accidents_insurance', array(1 => __('Yes')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('non_professional_accidents_insurance', array(0 => __('No')), $attributes);?>
									</label>
									<label class="btn btn-default btn-sm">
									<?php echo $this->Form->radio('non_professional_accidents_insurance', array(2 => __("Don't know")), $attributes);?>
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Postfinance account number'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('postfinance_account_number', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Bank name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('bank_name', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Bank clearing'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('bank_clearing', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('ZIP and city of the bank'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('bank_zip_city', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('IBAN number / Bank account'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('iban', array('label' => false, 'class' => 'form-control', 'required' => true));?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" id="availabilities">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Availabilities'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<p>
							Pour modifier vos disponibilités, cliquez sur une heure et sélectionner votre statut de disponibilité. <br> Vous avez la possibilité de sélectionner toute la journée également.
						</p>
						<?php echo $this->element('Users/availabilities', array('user' => $user));?>
						<div class="form-group">
							<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('availabilities_infos', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn blue" name="data[destination]" value="profile"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
				<button type="submit" class="btn blue" name="data[destination]" value="dashboard"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<div class="modal fade" id="levelsSignification" tabindex="-1" role="basic" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Levels signification'); ?></h4>
			</div>
			<div class="modal-body">
			<h4><?php echo __('Basic speaker'); ?></h4>
			<ul>
			 	<li>A1 Breakthrough or beginner</li>
			 	<li>A2 Waystage or elementary</li>
			 </ul>
			<h4><?php echo __('Independent speaker'); ?></h4>
			<ul>
				<li>B1 Threshold or intermediate</li>
				<li>B2 Vantage or upper intermediate</li>
			</ul>
			<h4><?php echo __('Proficient speaker'); ?></h4>
			<ul>
				<li>C1 Effective Operational Proficiency or advanced</li>
			 	<li>C2 Mastery or proficiency</li>
			</ul>
			<p>More info: http://www.deutsch-als-fremdsprache.org/en/faq/323-what-does-language-level-a1-a2-b1-b2-c1-and-c2-mean.html</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<?php
$this->start('init_scripts');
echo 'Custom.me();';
$this->end();
?>
