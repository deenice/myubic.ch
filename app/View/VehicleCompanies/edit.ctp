<?php $this->assign('page_title', $this->Html->link(__('Vehicle companies'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $this->request->data['VehicleCompany']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['VehicleCompany']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('VehicleCompany', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('type' => 'hidden'));?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('ZIP'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('City'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('city', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<h3 class="form-section"><?php echo __('Booking informations'); ?></h3>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Booking method'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('booking_method', array('label' => false, 'empty' => true, 'class' => 'form-control bs-select', 'options' => array('phone' => __('Phone'), 'email' => __('Email'), 'website'=> __('Website'))));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Pick-up hours'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('booking_pickup_hours', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Drop-off hours'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('booking_dropoff_hours', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Informations'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('booking_infos', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Emergency informations'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('emergency_infos', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<h3 class="form-section"><?php echo __('Contact person'); ?></h3>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('First name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('contact_person_first_name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Last name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('contact_person_last_name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Email'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('contact_person_email', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Phone'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('contact_person_phone', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>


