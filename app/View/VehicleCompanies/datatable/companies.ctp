<?php
foreach ($dtResults as $result) {

	$address = $result['VehicleCompany']['address'];
	$address .= $this->Html->tag('br');
	$address .= $result['VehicleCompany']['zip'] . ' ' . $result['VehicleCompany']['city'];
    
    $contact = $result['VehicleCompany']['contact_person_first_name'];
    $contact .= ' ' . $result['VehicleCompany']['contact_person_last_name'];
    $contact .= $this->Html->tag('br');
    $contact .= ' ' . $result['VehicleCompany']['contact_person_email'];
    $contact .= $this->Html->tag('br');
    $contact .= ' ' . $result['VehicleCompany']['contact_person_phone'];


	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"), 
	    array('controller' => 'vehicle_companies', 'action' => 'edit', $result['VehicleCompany']['id']), 
	    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
	);
	$actions .= $this->Html->tag('br');
	$actions .= $this->Html->link(
	    '<i class="fa fa-plus"></i> ' . __("Add a model"), 
	    array('controller' => 'vehicle_company_models', 'action' => 'add', $result['VehicleCompany']['id']), 
	    array('class' => 'btn green btn-xs', 'escape' => false)
	);
	$actions .= $this->Html->link(
	    '<i class="fa fa-plus"></i> ' . __("Add a vehicle"), 
	    array('controller' => 'vehicles', 'action' => 'add', $result['VehicleCompany']['id']), 
	    array('class' => 'btn green btn-xs', 'escape' => false)
	);

    $this->dtResponse['aaData'][] = array(
        $result['VehicleCompany']['name'],
        $address,
        $contact,
        $actions
    );
}