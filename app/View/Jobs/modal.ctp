<?php echo $this->Form->create('Job', array('class' => 'form-horizontal form-job', 'url' => array('controller' => 'jobs', 'action' => 'edit', empty($job) ? '' : $job['Job']['id']))); ?>
<?php echo empty($job) ? '' : $this->Form->input('Job.id', array('value' => $job['Job']['id'], 'type' => 'hidden')); ?>
<?php echo $this->Form->input('Job.event_id', array('value' => $event['Event']['id'], 'type' => 'hidden')); ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo empty($job) ? __('New job') : $job['Job']['name']; ?></h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.start_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($job) ? $event['Event']['start_hour'] : $job['Job']['start_time']));?>
      <?php echo $this->Form->input('Job.end_time', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker timepicker-24 input-inline input-small', 'value' => empty($job) ? $event['Event']['end_hour'] : $job['Job']['end_time']));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Staff amount'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.staff_needed', array('label' => false, 'class' => 'form-control input-small', 'type' => 'number', 'value' => empty($job) ? 1 : $job['Job']['staff_needed'], 'min' => 1));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Sector'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.sector', array('label' => false, 'class' => 'form-control bs-select input-sector', 'options' => Configure::read('Competences.sectors'), 'empty' => true, 'value' => empty($job) ? '' : $job['Job']['sector']));?>
    </div>
  </div>
  <div class="form-group group-jobs">
    <label class="control-label col-md-3"><?php echo __('Job'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.job1', array('label' => false, 'class' => 'form-control bs-select input-jobs input-fb-jobs', 'options' => Configure::read('Competences.fb_jobs'), 'empty' => true, 'value' => empty($job) ? '' : $job['Job']['job']));?>
      <?php echo $this->Form->input('Job.job2', array('label' => false, 'class' => 'form-control bs-select input-jobs input-logistics-jobs', 'options' => Configure::read('Competences.logistics_jobs'), 'empty' => true, 'value' => empty($job) ? '' : $job['Job']['job']));?>
      <?php echo $this->Form->input('Job.job3', array('label' => false, 'class' => 'form-control bs-select input-jobs input-animation-jobs', 'options' => Configure::read('Competences.animation_jobs'), 'empty' => true, 'value' => empty($job) ? '' : $job['Job']['job']));?>
      <?php echo $this->Form->input('Job.job4', array('label' => false, 'class' => 'form-control bs-select input-jobs input-other-jobs', 'options' => Configure::read('Competences.other_jobs'), 'empty' => true, 'value' => empty($job) ? '' : $job['Job']['job']));?>
    </div>
  </div>
  <div class="form-group group-activities">
    <label class="control-label col-md-3"><?php echo __('Activity'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.activity_id', array('label' => false, 'class' => 'form-control bs-select input-activities', 'options' => $activities, 'empty' => true, 'data-live-search' => true, 'value' => empty($job['Job']['activity_id']) ? '' : $job['Job']['activity_id']));?>
    </div>
  </div>
  <div class="form-group group-fb-modules">
    <label class="control-label col-md-3"><?php echo __('F&B Module'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.fb_module_id', array('label' => false, 'class' => 'form-control bs-select input-fb-modules', 'options' => $fbmodules, 'empty' => true, 'data-live-search' => true, 'value' => empty($job['Job']['fb_module_id']) ? '' : $job['Job']['fb_module_id']));?>
    </div>
  </div>
  <div class="form-group group-hierarchies">
    <label class="control-label col-md-3"><?php echo __('Hierarchy'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.hierarchy', array('label' => false, 'class' => 'form-control bs-select input-hierarchies', 'options' => Configure::read('Competences.hierarchies'), 'empty' => true, 'value' => empty($job['Job']['hierarchy']) ? '' : $job['Job']['hierarchy']));?>
    </div>
  </div>
  <div class="form-group group-languages">
    <label class="control-label col-md-3"><?php echo __('Language'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.languages', array('label' => false, 'class' => 'form-control bs-select input-languages', 'options' => Configure::read('Jobs.languages'), 'empty' => true, 'multiple' => true, 'value' => !empty($job['Job']['languages']) ? explode(',', $job['Job']['languages']) : ''));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Staff'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.user_ids', array('label' => false, 'class' => 'form-control bs-select input-inline', 'data-live-search' => true, 'multiple' => true, 'options' => $users, 'value' => empty($job) ? '' : $job['user_ids']));?>
      <span class="help-block">Vous pouvez sélectionner directement un extra dans la liste déroulante.</span>
      <div id="feedback"></div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Outfit'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.outfit_id', array('label' => false, 'class' => 'form-control bs-select input-outfits', 'data-live-search' => true, 'data-default' => !empty($job['Job']['outfit_id']) ? $job['Job']['outfit_id'] : '', 'options' => !empty($job['Job']['outfit_id']) ? array($job['Job']['outfit_id'] => $job['Outfit']['name']) : array(), 'value' => !empty($job['Job']['outfit_id']) ? $job['Job']['outfit_id'] : ''));?>
    </div>
  </div>
  <div class="form-group group-remarks">
    <label class="control-label col-md-3"><?php echo __('Recruiting remarks'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.remarks', array('label' => false, 'class' => 'form-control input-remarks', 'value' => empty($job['Job']['remarks']) ? '' : $job['Job']['remarks']));?>
    </div>
  </div>
  <div class="form-group group-remarks">
    <label class="control-label col-md-3"><?php echo __('Engagement confirmation remarks'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('Job.engagement_remarks', array('label' => false, 'class' => 'form-control input-engagement-remarks', 'value' => empty($job['Job']['engagement_remarks']) ? '' : $job['Job']['engagement_remarks']));?>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <button type="button" class="btn btn-danger delete-job" data-job-id="<?php echo empty($job) ? '' : $job['Job']['id']; ?>"><?php echo __('Delete'); ?></button>
  <button type="submit" class="btn btn-success"><?php echo __('Save'); ?></button>
</div>
<?php echo $this->Form->end(); ?>
