<?php $availabilities = Configure::read('Users.availabilities'); ?>
<div class="row">
  <div class="col-md-12">
    <?php if(!empty($job['Job']['remarks'])): ?>
    <div class="alert alert-info">
      <i class="fa fa-info-circle"></i>
      <?php echo $job['Job']['remarks']; ?>
    </div>
    <?php endif; ?>
    <?php echo $this->Form->create('RoundAnswer'); ?>
    <?php echo $this->Form->input('Round.id', array('type' => 'hidden', 'value' => $round['Round']['id'])); ?>
    <table class="table table-striped table-hover table-staff" data-job-id="<?php echo $job['Job']['id']; ?>">
      <thead>
        <tr>
          <th style="width: 40px">&nbsp;</th>
          <th>&nbsp;</th>
          <th>&nbsp;</th>
          <?php foreach($round['RoundStep'] as $step): ?>
          <th class="text-center" style="width:8%">
            <a href="#" data-toggle="modal" data-target="#roundStep<?php echo $step['id']; ?>" class="<?php echo $step['sent'] ? '':'font-grey'; ?>">
              <small>#</small><?php echo $step['weight']; ?>
            </a>
            <?php if($step['sent']): ?>
              <i class="fa fa-check font-green"></i>
            <?php else: ?>
              <i class="fa fa-clock-o font-grey"></i>
            <?php endif; ?>
          </th>
          <?php endforeach; ?>
        </tr>
      </thead>
      <tbody>
        <?php $k = 0; foreach($users as $user): ?>
        <tr data-user-id="<?php echo $user['User']['id']; ?>" class="<?php echo ($k==1 && 1==2) ? 'blurred' : '';?> <?php echo $user['enrolled'] ? 'success':''; ?>">
          <td>
            <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $user['User']['id'], $event['Event']['confirmed_date'])); ?>" data-toggle="modal" data-target="#modal-user-ajax">
              <?php if(!empty($user['Portrait'][0]['url'])) echo $this->Html->image($this->ImageSize->crop(DS . $user['Portrait'][0]['url'], 40, 40)); ?>
            </a>
          </td>
          <td class="font-grey-silver">
            <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $user['User']['id'], $event['Event']['confirmed_date'])); ?>" data-toggle="modal" data-target="#modal-user-ajax"><?php echo $user['User']['full_name']; ?></a><br>
            <?php if(!empty($user['Competence'][0]['hierarchy'])): ?>
              <span class="btn btn-xs default"><?php echo $hierarchies[$user['Competence'][0]['hierarchy']]; ?></span>
            <?php endif; ?>
            <?php if(!empty($user['User']['french_level'])): ?>
              <?php echo $this->element('flag', array('language' => 'fr')); ?> <?php echo strtoupper($user['User']['french_level']); ?>
            <?php endif; ?>
            <?php if(!empty($user['User']['german_level'])): ?>
              <?php echo $this->element('flag', array('language' => 'de')); ?> <?php echo strtoupper($user['User']['german_level']); ?>
            <?php endif; ?>
            <?php if(!empty($user['User']['english_level'])): ?>
              <?php echo $this->element('flag', array('language' => 'gb')); ?> <?php echo strtoupper($user['User']['english_level']); ?>
            <?php endif; ?>
          </td>
          <td>
            <?php if($user['availability']['availability'] == 'yes'): ?>
            <span class="label label-sm label-success">
            <?php elseif($user['availability']['availability'] == 'maybe'): ?>
            <span class="label label-sm label-warning">
            <?php elseif($user['availability']['availability'] == 'no'): ?>
            <span class="label label-sm label-danger">
            <?php elseif($user['availability']['availability'] == 'unknown'): ?>
            <span class="label label-sm label-default">
            <?php endif; ?>
              <?php echo $availabilities[$user['availability']['availability']]; ?>
            </span>
            <?php if(!empty($user['availability']['data'])): ?>
              <br />
              <small>
              <?php if($user['availability']['data']['UserUnavailability']['model'] == 'job'): ?>
                <?php if($user['availability']['data']['UserUnavailability']['status'] == 'enrolled' || $user['availability']['data']['UserUnavailability']['status'] == 'interested'): ?>
                <?php echo __('Works already as a %s on %s for %s', sprintf('<strong>%s</strong><br />', $user['availability']['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link(empty($user['availability']['data']['Job']['Event']['code_name']) ? $user['availability']['data']['Job']['Event']['name'] : $user['availability']['data']['Job']['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $user['availability']['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['availability']['data']['Job']['Event']['Client']['name']); ?>
                <?php elseif($user['availability']['data']['UserUnavailability']['status'] == 'not_available'): ?>
                <?php echo __('Not available at this date: according to answer of job %s on %s for %s', sprintf('<br /><strong>%s</strong>', $user['availability']['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link($user['availability']['data']['Job']['Event']['name'], array('controller' => 'events', 'action' => 'work', $user['availability']['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['availability']['data']['Job']['Event']['Client']['name']); ?>
                <?php elseif($user['availability']['data']['UserUnavailability']['status'] == 'not_interested'): ?>
                <?php echo __('Was not interested for job %s on %s for %s', sprintf('<strong>%s</strong><br />', $user['availability']['data']['Job']['name']), sprintf('<strong>%s</strong>', $this->Html->link($user['availability']['data']['Job']['Event']['name'], array('controller' => 'events', 'action' => 'work', $user['availability']['data']['Job']['Event']['id']), array('target'=>'_blank'))), $user['availability']['data']['Job']['Event']['Client']['name']); ?>
                <?php endif; ?>
              <?php endif; ?>
              </small>
            <?php endif; ?>
          </td>
          <td class="text-center"><?php echo (!empty($user['answers']['FirstRoundStep'])) ? $this->element('round_answer', array('user_id' => $user['User']['id'], 'data' => $user['answers']['FirstRoundStep'])) : ''; ?></td>
          <td class="text-center"><?php echo (!empty($user['answers']['SecondRoundStep'])) ? $this->element('round_answer', array('user_id' => $user['User']['id'], 'data' => $user['answers']['SecondRoundStep'])) : ''; ?></td>
          <td class="text-center"><?php echo (!empty($user['answers']['ThirdRoundStep'])) ? $this->element('round_answer', array('user_id' => $user['User']['id'], 'data' => $user['answers']['ThirdRoundStep'])) : ''; ?></td>
        </tr>
        <?php if(!empty($user['User']['availabilities_infos'])): ?>
        <tr><td colspan="4"><small><strong><?php echo $user['User']['full_name']; ?></strong> <?php echo $user['User']['availabilities_infos']; ?></small></td></tr>
        <?php endif; ?>
        <?php $k++; endforeach; ?>
      </tbody>
    </table>
    <?php echo $this->Form->end(); ?>
  </div>
  <div class="col-md-12">
    <div class="">
      <?php if(!$round['RoundStep'][0]['sent']): ?>
        <a href="#" class="btn btn-success save-rounds" data-job-id="<?php echo $job['Job']['id']; ?>"><i class="fa fa-check"></i> <?php echo __('Save rounds'); ?></a>
      <?php endif; ?>
      <?php if($round['Round']['status'] != 'closed'): ?>
        <?php if(!$round['RoundStep'][1]['sent'] && $round['RoundStep'][0]['sent']): ?>
          <button type="button" class="btn btn-warning force-round-send-1" data-href="<?php echo Router::url(array('controller' => 'round_steps', 'action' => 'send', $round['RoundStep'][1]['id']));?>"> <i class="fa fa-envelope-o"></i> <?php echo __('Force sending 2nd round'); ?></button>
        <?php elseif(!$round['RoundStep'][2]['sent'] && $round['RoundStep'][1]['sent']): ?>
          <button type="button" class="btn btn-warning force-round-send-1" data-href="<?php echo Router::url(array('controller' => 'round_steps', 'action' => 'send', $round['RoundStep'][2]['id']));?>"> <i class="fa fa-envelope-o"></i> <?php echo __('Force sending 3rd round'); ?></button>
        <?php endif; ?>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php foreach($round['RoundStep'] as $step): ?>
  <div class="modal modal-round-step" id="roundStep<?php echo $step['id']; ?>">
    <?php echo $this->Form->create('RoundStep', array('class' => 'form-horizontal form-round-step', 'url' => array('controller' => 'round_steps', 'action' => 'save'))); ?>
    <?php echo $this->Form->input('RoundStep.id', array('type' => 'hidden', 'value' => $step['id'])); ?>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><?php echo __('Edit round step'); ?></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-md-4">
              <?php if($step['sent']): ?>
                <?php echo __('Sending date'); ?>
              <?php else: ?>
                <?php echo __('Expected sending date'); ?>
              <?php endif; ?>
            </label>
            <div class="col-md-8">
              <?php echo $this->Form->input('RoundStep.date', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control date-picker1', 'readonly' => $step['sent'], 'value' => ($step['sent']) ? $this->Time->format('d-m-Y', $step['sent_date']) : $this->Time->format('d-m-Y', $step['date'])));?>
              <span class="help-block">
                <?php if($step['weight'] == 1): ?>
                  <?php echo __('Second round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][1]['date'])); ?><br />
                  <?php echo __('Third round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][2]['date'])); ?>
                <?php elseif($step['weight'] == 2): ?>
                  <?php echo __('First round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][0]['date'])); ?><br />
                  <?php echo __('Third round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][2]['date'])); ?>
                <?php elseif($step['weight'] == 3): ?>
                  <?php echo __('First round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][0]['date'])); ?><br />
                  <?php echo __('Second round on %s', $this->Time->format('d.m.Y', $round['RoundStep'][1]['date'])); ?>
                <?php endif; ?>
              </span>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
          <?php if( ($step['date'] < date('Y-m-d') || $step['sent']) && false ): ?>
          <button type="button" class="btn default disabled"><?php echo __('Round has already been sent.'); ?></button>
          <?php else: ?>
          <button type="button" class="btn btn-warning force-round-send"><?php echo __('Force sending'); ?></button>
          <button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <?php echo $this->Form->end(); ?>
  </div>
<?php endforeach; ?>
