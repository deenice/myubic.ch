<?php $this->assign('page_title', $this->Html->link(__('Places'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add a commune'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-direction font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a commune'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Commune', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control', 'autofocus' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('ZIP'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('zip', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Canton'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('canton', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Communes.cantons'), 'empty' => true)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Country'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('country', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => Configure::read('Countries'), 'value' => 'CH')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Latitude'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('latitude', array('label' => false, 'class' => 'form-control', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Longitude'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('longitude', array('label' => false, 'class' => 'form-control', 'type' => 'text')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.communes();';
$this->end();
?>
