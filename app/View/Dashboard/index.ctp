<?php $this->assign('page_title', __('Dashboard')); ?>
<?php $this->assign('page_subtitle', __('Welcome on myubic!')); ?>
<div class="row dashboard">
	<?php if(!empty($offers)): ?>
	<div class="col-md-6">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('My offers'); ?></span>
        </div>
			</div>
			<div class="portlet-body">
					<table class="table table-striped table-offers">
						<thead>
							<tr>
								<th class="company">&nbsp;</th>
								<th class="dossier">Dossier</th>
								<th class="client">Client</th>
								<th class="date">Date(s)</th>
								<th>Statut CRM</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($offers as $event): ?>
							<tr>
								<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
								<td><?php echo $this->Html->link(empty($event['Event']['code']) ? 'xxx' : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
								<td><?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 35), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?></td>
								<td>
									<?php if(!empty($event['Event']['confirmed_date'])): ?>
										<?php echo $this->Time->format('d.m.y', $event['Event']['confirmed_date']); ?>
									<?php elseif(!empty($event['Date'])): ?>
										<?php foreach($event['Date'] as $k => $date): ?>
											<?php echo $this->Time->format('d.m.y', $date['date']); ?>
											<?php echo !empty($event['Date'][$k+1]) ? __('or') : ''; ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['Event']['period_start'])): ?>
										<?php echo $this->Time->format('d.m.y', $event['Event']['period_start']); ?> <?php echo __('to'); ?> <?php echo $this->Time->format('d.m.y', $event['Event']['period_end']); ?>
									<?php endif;?>
								</td>
								<td>
									<a href="javascript:;" data-crm-status-editable data-mode="" data-showbuttons="false" data-value="<?php echo empty($event['Event']['crm_status']) ? '' : $event['Event']['crm_status']; ?>" data-type="select" data-pk="<?php echo $event['Event']['id']; ?>">
										<?php echo !empty($event['Event']['crm_status']) ? $crmStatuses[$event['Event']['crm_status']] : ''; ?>
									</a>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if(!empty($tasks)): ?>
		<div class="col-md-6">
			<div class="portlet light tasks-widget" id="portlet-tasks">
				<div class="portlet-title">
					<div class="caption">
						<span class="caption-subject bold uppercase"><?php echo __('My today tasks');?> (<?php echo $numberOfTasks; ?>)</span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="panel-group accordion" id="tasks">
						<?php foreach($tasks as $event => $taskss): ?>
            <div class="panel panel-default">
              <div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#tasks" href="#collapse<?php echo hash('md5', $event); ?>"> <?php echo $event; ?> </a>
								</h4>
							</div>
							<div id="collapse<?php echo hash('md5', $event); ?>" class="panel-collapse collapse">
								<div class="panel-body">
									<ul class="task-list">
										<?php foreach($taskss as $task): ?>
										<li data-checklist-task-id="<?php echo $task['ChecklistTask']['id'];?>">

											<div class="task-checkbox">
												<input type="checkbox" name="name" value="">
											</div>
											<div class="task-title">
												<span class="task-title-sp">
													<?php if($task['ChecklistTask']['due_date'] < date('Y-m-d')): ?>
														<i class="fa fa-warning font-yellow"></i>
													<?php endif; ?>
													<a href="<?php echo Router::url(array('controller' => 'checklist_tasks', 'action' => 'modal', $task['ChecklistTask']['id'])); ?>" data-toggle="modal" data-target="#modal-calendar-task">
													<?php echo $task['ChecklistTask']['name']; ?></a><br>
													<small style="font-size: 90%">
														<?php echo $task['ChecklistBatch']['Checklist']['name']; ?> |
														<?php echo $task['ChecklistBatch']['name']; ?>
													</small>
												</span>
											</div>

										</li>
									<?php endforeach; ?>
									</ul>
									<br>
									<div class="text-right">
										<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Work on event')), array('controller' => 'events', 'action' => 'work', $events[$event]['id']), array('class' => 'btn btn-xs default', 'escape' => false, 'target' => '_blank')); ?>
										<?php echo $this->Html->link(sprintf('<i class="fa fa-search"></i> %s', __('View client')), array('controller' => 'clients', 'action' => 'view', $events[$event]['client_id']), array('class' => 'btn btn-xs default', 'escape' => false, 'target' => '_blank')); ?>
									</div>
                </div>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if(!empty($confirmed)): ?>
	<div class="col-md-6">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('My confirmed events'); ?></span>
        </div>
			</div>
			<div class="portlet-body">
					<table class="table table-striped table-confirmed">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Dossier</th>
								<th>Client</th>
								<th>Contenu</th>
								<th class="date">Date(s)</th>
								<th>Poste(s)</th>
								<th class="hidden">Suivi</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($confirmed as $event): $place = ''; ?>
							<tr class="danger1">
								<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
								<td><?php echo $this->Html->link(empty($event['Event']['code']) ? 'xxx' : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
								<td>
									<?php if(in_array($event['Event']['company_id'], array(4,5))): //MG, EG ?>
									<?php echo $event['Event']['name']; ?><br>
									<?php endif; ?>
									<?php if($event['Event']['type'] == 'mission'): ?>
									<?php echo $event['Event']['name']; ?>
									<?php else: ?>
									<?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 20), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?>
									<?php endif; ?>
								</td>
								<td>
									<!-- <span class="btn btn-danger btn-xs"><i class="fa fa-cutlery"></i> MG</span>
									<span class="btn btn-warning btn-xs"><i class="fa fa-beer"></i> OB</span>
									<span class="btn btn-default btn-xs"><i class="fa fa-truck"></i> Test</span> -->
									<?php if(!empty($event['SelectedActivity'])): ?>
										<?php foreach($event['SelectedActivity'] as $activity): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['SuggestedActivity'])): ?>
										<?php foreach($event['SuggestedActivity'] as $activity): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<?php if(!empty($event['SelectedFBModule'])): ?>
										<?php foreach($event['SelectedFBModule'] as $fb_module): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? '':$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php elseif(!empty($event['SuggestedFBModule'])): ?>
										<?php foreach($event['SuggestedFBModule'] as $fb_module): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? '':$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<?php if(!empty($event['VehicleReservation'])): ?>
										<?php foreach($event['VehicleReservation'] as $reservation): ?>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-truck"></i> %s', $reservation['slug']), array('controller' => 'vehicle_reservations', 'action' => 'view', $reservation['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
										<?php endforeach; ?>
									<?php endif; ?>
								</td>
								<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%d.%m.%y'); ?></td>
								<td>
									<?php if(!empty($event['Event']['number_of_jobs'])): ?>
										<?php echo $event['Event']['number_of_filled_jobs']; ?> / <?php echo $event['Event']['number_of_jobs']; ?>
									<?php endif; ?>
								</td>
								<td class="hidden">67%</td>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if(!empty($past)): ?>
	<div class="col-md-6">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
          <span class="caption-subject bold uppercase"><?php echo __('My past events'); ?></span>
        </div>
			</div>
			<div class="portlet-body">
					<table class="table table-striped table-confirmed">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Dossier</th>
								<th>Nom</th>
								<th>Client</th>
								<th>Date</th>
								<th>Contrôle</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($past as $event): //debug($event['Activity']); ?>
							<tr>
								<td><span class="btn btn-company btn-company-<?php echo $event['Company']['class']; ?> btn-xs"></span></td>
								<td><?php echo $this->Html->link(empty($event['Event']['code']) ? 'xxx' : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
								<td><?php echo $this->Text->truncate($event['Event']['name'], 30); ?></td>
								<td><?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 30), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?></td>
								<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%a %d.%m.%Y'); ?></td>
								<td>
									<!-- feedback, compta, heures -->
									<?php if(empty($event['Event']['feedback'])): ?>
										<span class="badge badge-warning badge-roundless"><i class="fa fa-warning"></i> <?php echo __('Feedback'); ?> </span>
									<?php else: ?>
										<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Feedback'); ?> </span>
									<?php endif; ?>
									<?php if($event['Event']['number_of_jobs']): ?>
										<?php if($event['Event']['number_of_validated_jobs']  == 0): ?>
											<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Hours'); ?> </span>
										<?php elseif($event['Event']['number_of_validated_jobs'] < $event['Event']['number_of_filled_jobs']): ?>
											<span class="badge badge-warning badge-roundless"><i class="fa fa-warning"></i> <?php echo __('Hours'); ?> </span>
										<?php elseif($event['Event']['number_of_validated_jobs'] >= $event['Event']['number_of_filled_jobs']): ?>
											<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Hours'); ?> </span>
										<?php endif; ?>
									<?php endif; ?>
									<?php if($event['Event']['accounting_match']): ?>
									<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Accounting'); ?> </span>
									<?php else: ?>
									<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Accounting'); ?> </span>
									<?php endif; ?>									
									<?php if($event['Event']['checklists_complete']): ?>
									<span class="badge badge-success badge-roundless"><i class="fa fa-check"></i> <?php echo __('Tasks'); ?> </span>
									<?php else: ?>
									<span class="badge badge-danger badge-roundless"><i class="fa fa-times"></i> <?php echo __('Tasks'); ?> </span>
									<?php endif; ?>
								</td>
								<td>
									<?php if(!empty($event['Note'])): ?>
										<a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $event['Event']['id'])); ?>" data-toggle="modal" data-target="#modal-note-list">
											<?php echo __n('1 note', '%d notes', 1, sizeof($event['Note'])); ?>
										</a>
									<?php else: ?>
										<a href="#" data-toggle="modal" data-target="#new-note" data-model="event" data-model-id="<?php echo $event['Event']['id']; ?>"><?php echo __('Add'); ?></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<div class="col-md-6 hidden">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light blue-soft" href="<?php echo Router::url(array('controller' => 'places', 'action' => 'index')); ?>">
				<div class="visual">
					<i class="fa fa-map-marker"></i>
				</div>
				<div class="details">
					<div class="number">
						<?php echo $numberOfPlaces; ?>
					</div>
					<div class="desc">
						 <?php echo __('Places'); ?>
					</div>
				</div>
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light purple-soft" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'index')); ?>">
				<div class="visual">
					<i class="fa fa-users"></i>
				</div>
				<div class="details">
					<div class="number">
						<?php echo $numberOfUsers; ?>
					</div>
					<div class="desc">
						 <?php echo __('Collaborators'); ?>
					</div>
				</div>
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light green-soft" href="<?php echo Router::url(array('controller' => 'activities', 'action' => 'index')); ?>">
				<div class="visual">
					<i class="fa fa-trophy"></i>
				</div>
				<div class="details">
					<div class="number">
						 <?php echo $numberOfActivities ? $numberOfActivities : 0; ?>
					</div>
					<div class="desc">
						 <?php echo __('Activities'); ?>
					</div>
				</div>
				</a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a class="dashboard-stat dashboard-stat-light red-soft" href="<?php echo Router::url(array('controller' => 'stock_items', 'action' => 'index')); ?>">
				<div class="visual">
					<i class="fa fa-cutlery"></i>
				</div>
				<div class="details">
					<div class="number">
						 <?php echo $numberOfItems; ?>
					</div>
					<div class="desc">
						 <?php echo __('Products Festiloc'); ?>
					</div>
				</div>
				</a>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.events();';
echo 'Custom.checklist_tasks();';
$this->end();
?>
