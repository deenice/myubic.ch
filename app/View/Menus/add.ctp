<?php $this->assign('page_title', $this->Html->link(__('Menu'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add a menu item'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-star font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a menu item'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Menu', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Parent'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('parent_id', array('label' => false, 'class' => 'form-control', 'options' => $menu, 'empty' => __('No parent'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Controller'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('controller', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Action'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('action', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Parameters'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('params', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Menu'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('menu', array('class' => 'form-control', 'label' => false)); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
