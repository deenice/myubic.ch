<div class="row reporting">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Hours by person'); ?></span>
				</div>
				<div class="actions btn-group">
					<?php echo $this->Html->link(__('By event'), array('controller' => 'reporting', 'action' => 'hours', 'events'), array('class' => 'btn default')); ?>
					<?php echo $this->Html->link(__('By person'), array('controller' => 'reporting', 'action' => 'hours', 'staff'), array('class' => 'btn default active')); ?>
				</div>
			</div>
			<div class="portlet-body">
				<ul class="nav nav-tabs">
					<?php $i=0;foreach($months as $month => $data): ?>
          <li class="<?php echo $i == 0 ? 'active':''; ?>">
						<?php if($month == '2016-05-25'): ?>
						<a href="#month<?php echo $month; ?>" data-toggle="tab"> <?php echo '25-31.05.2016'; ?> </a>
						<?php else: ?>
            <a href="#month<?php echo $month; ?>" data-toggle="tab"> <?php echo ucfirst($this->Time->format($month, '%B %Y')); ?> </a>
						<?php endif; ?>
          </li>
					<?php $i++; endforeach; ?>
        </ul>
				<div class="tab-content">
					<?php $j=0; foreach($staff as $month => $companies): //debug($companies); ?>
					<?php if(empty($companies)): ?>
					<div class="tab-pane <?php echo $j==0 ? 'active':''; ?>" id="month<?php echo $month; ?>">
						<div class="alert alert-warning">
							<p>
								Aucune donnée pour le moment...
							</p>
						</div>
					</div>
					<?php else: ?>
					<div class="tab-pane <?php echo $j==0 ? 'active':''; ?>" id="month<?php echo $month; ?>">
						<?php foreach($companies as $company => $items): ?>
						<div class="portlet">
							<div class="portlet-body">
								<table class="table table-striped">
									<thead>
										<tr>
											<th class="check hidden"></th>
											<th class="name"><?php echo __('Name'); ?></th>
											<th class="salary"><?php echo __('Salaire brut'); ?></th>
											<th class="events"><?php echo __('Number of events'); ?></th>
											<th class="hours"><?php echo __('Hours'); ?></th>
											<th class="salaries"><?php echo __('Salaries'); ?></th>
											<th class="actions"><?php echo __('Actions'); ?></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($items as $item): if(empty($item['User']['reports']['salaries'])) {continue;} ?>
										<tr>
											<td class="hidden"><input type="checkbox" name="name" value=""></td>
											<td>
												<span class="btn btn-xs btn-company btn-company-<?php echo $company; ?>"></span>
												<a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'modal', $item['User']['id'])); ?>" data-toggle="modal" data-target="#modal-user-ajax">
													<?php echo $item['User']['full_name']; ?>
												</a>
											</td>
											<td><?php echo sprintf('%s CHF', $item['User']['reports']['salary']); ?></td>
											<td><?php echo $item['User']['reports']['events']; ?></td>
											<td><?php echo $item['User']['reports']['hours']; ?></td>
											<td><?php echo implode(', ', $item['User']['reports']['salaries']); ?></td>
											<td>
												<a href="<?php echo Router::url(array('controller' => 'reporting', 'action' => 'modal', 'user', $item['User']['id'], $month)); ?>" data-toggle="modal" data-target="#modal-reporting-user">
													<?php echo __('See details'); ?>
												</a>
											</td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
					<?php endif; ?>
					<?php $j++; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $this->element('Modals/reporting-user'); ?>
