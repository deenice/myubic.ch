<?php $this->assign('page_title', $this->Html->link(__('Reporting'), array('action' => 'sales'))); ?>
<?php $this->assign('page_subtitle', __('Statistiques des ventes')); ?>
<div class="row hidden-print">
	<div class="col-md-12">
		<div class="pull-right">
			<ul class="pagination pagination-sm">
				<li>
					<?php echo $this->Html->link(sprintf('<i class="fa fa-angle-left"></i> %s', $year - 1), array('controller' => 'reporting', 'action' => 'sales', $year - 1), array('escape' => false)); ?>
				</li>
				<li class="active">
					<?php echo $this->Html->link($year, array('controller' => 'reporting', 'action' => 'sales', $year)); ?>
				</li>
				<li>
					<?php echo $this->Html->link(sprintf('%s <i class="fa fa-angle-right"></i>', $year + 1), array('controller' => 'reporting', 'action' => 'sales', $year + 1), array('escape' => false)); ?>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="row reporting">
	<div class="col-md-12">
		<?php foreach($sales as $company => $data): ?>
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase">
						<span class="btn btn-sm btn-company btn-company-<?php echo $company; ?>"></span>
						<?php echo $acronyms[$company]; ?>
					</span>
				</div>
				<div class="actions btn-group">
					<a href="#month<?php echo $company; ?>" data-toggle="tab" class="btn btn-sm default">
						<i class="fa fa-calendar-o"></i>
						<?php echo __('By month'); ?>
					</a>
					<a href="#person<?php echo $company; ?>" data-toggle="tab" class="btn btn-sm default">
						<i class="fa fa-user"></i>
						<?php echo __('By person'); ?>
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="tab-content">
					<div class="tab-pane active" id="month<?php echo $company; ?>">
						<table class="table table-striped sales">
							<thead>
								<tr>
									<th class="month"><?php echo __('Month'); ?></th>
									<th class="number"><?php echo __('Minimal'); ?></th>
									<th class="number"><?php echo __('Requested'); ?></th>
									<?php if($company != 'festiloc'): ?>
									<th class="number"><?php echo __('Null offers'); ?></th>
									<?php endif; ?>
									<th class="number"><?php echo __('Sent offers'); ?></th>
									<th class="number"><?php echo __('Accepted offers'); ?></th>
									<th class="number"><?php echo __('Refused offers'); ?></th>
									<th class="number"><?php echo __('Offers in progress'); ?></th>
                  <?php if($company == 'eg'): ?>
                    <th class="number popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Taux de vente projeté</b>=<br>Nombre d'offres acceptées<br>+ 25% des offres en cours<br>divisé par le nombre d'offres envoyées<br>x 100.<br>N.B. La date prise en compte est la date d'ouverture du dossier." data-placement="top"><?php echo __('Projected sell rate'); ?></th>
                  <?php else: ?>
                    <th class="number popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Taux de vente projeté</b>=<br>Nombre d'offres acceptées<br>+ 37% des offres en cours<br>divisé par le nombre d'offres envoyées<br>x 100.<br>N.B. La date prise en compte est la date d'ouverture du dossier." data-placement="top"><?php echo __('Projected sell rate'); ?></th>
                  <?php endif; ?>
									<th class="number popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Taux de vente effectif</b>=<br>Nombre d'offres acceptées<br>divisé par le nombre d'offres envoyées<br>x 100.<br>N.B. La date prise en compte est la date d'ouverture du dossier." data-placement="top"><?php echo __('Effective sell rate'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$total = $sent = $null = $accepted = $refused = $inprogress = $projected_sell_rate = $effective_sell_rate = $turnover = $average = 0;
								$numberOfProjectedMonths = 0;
								$numberOfEffectiveMonths = 0;
								foreach($data['month'] as $month => $values):
									$total += $values['total'];
									$sent += $values['sent'];
									if($company != 'festiloc') $null += $values['null'];
									$accepted += $values['accepted'];
									$refused += $values['refused'];
									$inprogress += $values['in_progress'];
									if($values['projected_sell_rate']){
										$projected_sell_rate += $values['projected_sell_rate'];
										$numberOfProjectedMonths++;
									}
									if($values['effective_sell_rate']){
										$effective_sell_rate += $values['effective_sell_rate'];
										$numberOfEffectiveMonths++;
									}
								?>
								<tr>
									<td><?php echo ucfirst($this->Time->format($month . ' 15', '%B')); ?></td>
									<td><?php echo isset($values['minimal_offers']) ? number_format($values['minimal_offers'], 0) : 0; ?></td>
									<?php if(isset($values['minimal_offers']) && $values['minimal_offers'] >= 0 && $values['minimal_offers'] <= $values['total'] && $values['total'] > 0):?>
									<td class="text-success">
									<?php elseif(isset($values['minimal_offers']) && $values['minimal_offers'] >= 0 && $values['minimal_offers'] > $values['total'] && $values['total'] > 0):?>
									<td class="text-danger">
									<?php else: ?>
									<td>
									<?php endif; ?>
										<?php if($values['total'] > 0): ?>
											<?php echo $this->Html->link($values['total'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'all', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['total']; ?>
										<?php endif; ?>
									</td>
									<?php if($company != 'festiloc'): ?>
									<td>
										<?php if($values['null'] > 0): ?>
											<?php echo $this->Html->link($values['null'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'null', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['null']; ?>
										<?php endif; ?>
									</td>
									<?php endif; ?>
									<td>
										<?php if($values['sent'] > 0): ?>
											<?php echo $this->Html->link($values['sent'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'sent', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['sent']; ?>
										<?php endif; ?>
									</td>
									<td>
										<?php if($values['accepted'] > 0): ?>
											<?php echo $this->Html->link($values['accepted'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'accepted', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['accepted']; ?>
										<?php endif; ?>
									</td>
									<td>
										<?php if($values['refused'] > 0): ?>
											<?php echo $this->Html->link($values['refused'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'refused', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['refused']; ?>
										<?php endif; ?>
									</td>
									<td>
										<?php if($values['in_progress'] > 0): ?>
											<?php echo $this->Html->link($values['in_progress'], array('controller' => 'reporting', 'action' => 'modal', 'offers', 0, sprintf('%s-%s', $month, $year), 'in_progress', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?>
										<?php else: ?>
											<?php echo $values['in_progress']; ?>
										<?php endif; ?>
									</td>
									<td><?php echo round($values['projected_sell_rate'], 2); ?></td>
									<?php if($values['in_progress'] == 0 && $values['effective_sell_rate'] >= Configure::read('Reporting.minimal_sell_rate')): ?>
									<td class="text-success">
									<?php elseif($values['in_progress'] == 0 && $values['effective_sell_rate'] > 0 && $values['effective_sell_rate'] < Configure::read('Reporting.minimal_sell_rate')): ?>
									<td class="text-danger">
									<?php else: ?>
									<td>
									<?php endif; ?>
									<?php echo round($values['effective_sell_rate'], 2); ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2"><?php echo __('Total'); ?></td>
									<td><?php echo $total; ?></td>
									<?php if($company != 'festiloc'): ?>
									<td><?php echo $null; ?></td>
									<?php endif; ?>
									<td><?php echo $sent; ?></td>
									<td><?php echo $accepted; ?></td>
									<td><?php echo $refused; ?></td>
									<td><?php echo $inprogress; ?></td>
									<td><?php echo $numberOfProjectedMonths == 0 ? 0 : round($projected_sell_rate/$numberOfProjectedMonths, 2); ?></td>
									<td><?php echo $numberOfEffectiveMonths == 0 ? 0 : round($effective_sell_rate/$numberOfEffectiveMonths, 2); ?></td>
								</tr>
							</tfoot>
						</table>
						<table class="table table-striped sales">
							<thead>
								<tr>
									<th class="month"><?php echo __('Month'); ?></th>
									<th class="month popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Acceptés</b><br>Nombre d'évènements réalisés et confirmés.<br><b>En cours</b><br>Nombre d'offres en cours dont la date de réalisation est dans le mois courant" data-placement="top"><?php echo __('Accepted / In progress'); ?></th>
									<th class="budget popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Budget</b><br>Budget annuel divisé par 12. Cette information est à titre indicatif. Les objectifs de vente sont basés sur le budget pondéré sur la saisonnalité." data-placement="top"><?php echo __('Budget'); ?></th>
									<th class="balanced_budget popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Budget pondéré</b><br>Budget réparti manuellement sur l'expérience saisonnale des dernières années." data-placement="top"><?php echo __('Balanced budget'); ?></th>
									<th class="average popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>CA moyen</b><br>CA projeté divisé par le nombre d'évènements acceptés." data-placement="top"><?php echo __('Avergared turnover'); ?></th>
									<?php if($company == 'eg'): ?>
										<th class="projected_turnover popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>CA projeté</b> =<br>100% du CA projeté ou du montant facturé (si inséré) des évènements confirmés et réalisés <br> + 25% des offres dont la date de réalisation est dans le mois courant." data-placement="top"><?php echo __('Projected turnover'); ?></th>
									<?php else: ?>
										<th class="projected_turnover popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>CA projeté</b> =<br>100% du CA projeté ou du montant facturé (si inséré) des évènements confirmés et réalisés <br> + 37% des offres dont la date de réalisation est dans le mois courant." data-placement="top"><?php echo __('Projected turnover'); ?></th>
									<?php endif; ?>
									<th class="effective_turnover popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>CA effectif</b>=<br>Somme des montants facturés des évènements acceptés.<br><em>Pour UBIC</em><br>Somme des montants répartis sur les secteurs Sortie d'entreprise, Team Building, Création d'évènements, Urban Gaming et Autres." data-placement="top"><?php echo __('Effective turnover'); ?></th>
									<?php if($company == 'ubic'): ?>
										<th class="effective_turnover_margin popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>CA margé</b>=<br>Somme des montants répartis sur les secteurs Sortie d'entreprise, Team Building, Création d'évènements, Urban Gaming." data-placement="top"><?php echo __('CA margé'); ?></th>
									<?php endif; ?>
									<th class="projected_result popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Résultat projeté</b><br>CA projeté moins le budget pondéré." data-placement="top"><?php echo __('Projected result'); ?></th>
									<th class="effective_result popovers" data-trigger="hover" data-container="body" data-html="true" data-content="<b>Résultat effectif</b><br>CA effectif moins le budget pondéré." data-placement="top"><?php echo __('Effective result'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$accepted = $in_progress = $budget = $balanced_budget = $averaged_turnover = $projected_turnover = $effective_turnover = $effective_turnover_margin = $projected_result = $effective_result = 0;
								foreach($data['month'] as $month => $values):
									$accepted += $values['accepted_events'];
									$in_progress += $values['in_progress_events'];
									$budget += $values['monthly_budget'];
									$balanced_budget += $values['balanced_budget'];
									$averaged_turnover += $values['averaged_turnover'];
									$projected_turnover += $values['projected_turnover'];
									$effective_turnover += $values['effective_turnover'];
									$effective_turnover_margin += $values['effective_turnover_margin'];
									$projected_result += $values['projected_result'];
									$effective_result += $values['effective_result'];
									$temp = date_parse($month);
									$modalDate = sprintf('%s-%s-15', $year, $temp['month']);
								?>
								<tr>
									<td><?php echo $this->Html->link(ucfirst($this->Time->format($month . ' 15', '%B')), array('controller' => 'reporting', 'action' => 'modal', 'repartition', 0, $modalDate, 'accepted', $company), array('data-toggle' => 'modal', 'data-target' => '#modal-reporting-offers')); ?></td>
									<td>
										<?php echo !empty($values['accepted_events']) ? $this->Html->link($values['accepted_events'], array('controller' => 'reporting', 'action' => 'modal', 'realisations', 0, $modalDate, 'accepted', $company), array('data-target' => '#modal-reporting-offers', 'data-toggle' => 'modal')) : $values['accepted_events']; ?> /
										<?php echo !empty($values['in_progress_events']) ? $this->Html->link($values['in_progress_events'], array('controller' => 'reporting', 'action' => 'modal', 'realisations', 0, $modalDate, 'in_progress', $company), array('data-target' => '#modal-reporting-offers', 'data-toggle' => 'modal')) : $values['in_progress_events']; ?>
									</td>
									<td>
										<a href="javascript:;" class="editable" data-pk="1" data-reporting-option-editable data-option-type="monthly_budget" data-company-id="<?php echo $companies_ids[$company]; ?>" data-year="<?php echo $year; ?>" data-month="<?php echo $month; ?>" data-value="<?php echo $values['monthly_budget']; ?>">
											<?php echo sprintf('%s CHF', number_format($values['monthly_budget'], 2, '.', "'")); ?>
										</a>									
									</td>
									<td>
										<a href="javascript:;" class="editable" data-pk="1" data-reporting-option-editable data-option-type="balanced_budget" data-company-id="<?php echo $companies_ids[$company]; ?>" data-year="<?php echo $year; ?>" data-month="<?php echo $month; ?>" data-value="<?php echo $values['balanced_budget']; ?>">
											<?php echo sprintf('%s CHF', number_format($values['balanced_budget'], 2, '.', "'")); ?>
										</a>
									</td>
									<td><?php echo sprintf('%s CHF', number_format($values['averaged_turnover'], 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($values['projected_turnover'], 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($values['effective_turnover'], 2, '.', "'")); ?></td>
									<?php if($company == 'ubic'): ?>
									<td><?php echo sprintf('%s CHF', number_format($values['effective_turnover_margin'], 2, '.', "'")); ?></td>
									<?php endif; ?>
									<td><span class="<?php echo $values['projected_result'] > 0 ? 'text-success':'text-danger'; ?>"><?php echo sprintf('%s CHF', number_format($values['projected_result'], 2, '.', "'")); ?></span></td>
									<td><span class="<?php echo $values['effective_result'] > 0 ? 'text-success':'text-danger'; ?>"><?php echo sprintf('%s CHF', number_format($values['effective_result'], 2, '.', "'")); ?></span></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td><?php echo __('Total'); ?></td>
									<td><?php echo $accepted; ?> / <?php echo $in_progress; ?></td>
									<td><?php echo sprintf('%s CHF', number_format($budget, 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($balanced_budget, 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($averaged_turnover/12, 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($projected_turnover, 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($effective_turnover, 2, '.', "'")); ?></td>
									<?php if($company == 'ubic'): ?>
									<td><?php echo sprintf('%s CHF', number_format($effective_turnover_margin, 2, '.', "'")); ?></td>
									<?php endif; ?>
									<td><?php echo sprintf('%s CHF', number_format($projected_result, 2, '.', "'")); ?></td>
									<td><?php echo sprintf('%s CHF', number_format($effective_result, 2, '.', "'")); ?></td>
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="tab-pane" id="person<?php echo $company; ?>">
						<?php if(!empty($data['person'])): ?>
						<table class="table table-striped persons">
							<thead>
								<tr>
									<th class="person"><?php echo __('Person'); ?></th>
									<th class="number"><?php echo __('Treated offers'); ?></th>
									<th class="number"><?php echo __('Realized offers'); ?></th>
									<?php if($company != 'festiloc'): ?>
									<th class="number"><?php echo __('No offer issued'); ?></th>
									<?php endif; ?>
									<th class="number"><?php echo __('Refused offers'); ?></th>
									<th class="number"><?php echo __('Accepted offers'); ?></th>
									<th class="conversion"><?php echo __('Conversion'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php
								$total = $sent = $null = $accepted = $refused = $inprogress = $sellrate = $turnover = $average = $realized = 0;
								foreach($data['person'] as $manager => $values):
									$total += $values['total'];
									$realized += $values['realized'];
									if($company != 'festiloc') $null += $values['null'];
									$refused += $values['refused'];
									$accepted += $values['accepted'];
								?>
								<tr>
									<td><?php echo $manager; ?></td>
									<td><?php echo $values['total']; ?></td>
									<td><?php echo $values['realized']; ?></td>
									<?php if($company != 'festiloc'): ?>
									<td><?php echo $values['null']; ?></td>
									<?php endif; ?>
									<td><?php echo $values['refused']; ?></td>
									<td><?php echo $values['accepted']; ?></td>
									<td><?php echo sprintf('%s%%', round($values['conversion'], 2)); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td>&nbsp;</td>
									<td><?php echo $total; ?></td>
									<td><?php echo $realized; ?></td>
									<?php if($company != 'festiloc'): ?>
									<td><?php echo $null; ?></td>
									<?php endif; ?>
									<td><?php echo $refused; ?></td>
									<td><?php echo $accepted; ?></td>
									<td><?php echo sprintf('%s%%', round(100 * $accepted / ($total - $null), 2)); ?></td>
								</tr>
							</tfoot>
						</table>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
</div>
<?php echo $this->element('Modals/reporting-offers'); ?>

<?php
$this->start('init_scripts');
echo 'Custom.reporting();';
$this->end();
?>