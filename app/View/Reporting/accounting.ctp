<?php $this->assign('page_title', $this->Html->link(__('Reporting'), array('action' => 'accounting'))); ?>
<?php $this->assign('page_subtitle', __('Accounting')); ?>

<div class="row">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase">
						<?php echo __('Accounting for events'); ?>
					</span>
				</div>
			</div>
			<div class="portlet-body">
				<ul class="nav nav-tabs">
					<?php $i=0;foreach($events as $month => $data): ?>
          <li class="<?php echo $i == 0 ? 'active':''; ?>">
            <a href="#month<?php echo $month; ?>" data-toggle="tab"> <?php echo ucfirst($this->Time->format($month, '%B %Y')); ?> </a>
          </li>
					<?php $i++; endforeach; ?>
        </ul>
				<div class="tab-content">
					<?php $j=0; foreach($events as $month => $data): ?>
						<div class="tab-pane <?php echo $j==0 ? 'active':''; ?>" id="month<?php echo $month; ?>">
							<table class="table table-striped accounting">
								<thead>
									<tr>
										<th class="date"><?php echo __('Date'); ?></th>
										<th class="client"><?php echo __('Company'); ?></th>
										<th class="number larger"><?php echo __('No dossier'); ?></th>
										<th class="number larger"><?php echo __('No acompte'); ?></th>
										<th class="number larger"><?php echo __('Montant acompte'); ?></th>
										<th class="number larger"><?php echo __('No facture'); ?></th>
										<th class="number"><?php echo __('Corporate outing'); ?></th>
										<th class="number"><?php echo __('Team Building'); ?></th>
										<th class="number"><?php echo __('Urban Gaming'); ?></th>
										<th class="number"><?php echo __('Events creation'); ?></th>
										<th class="number"><?php echo __('Effet Gourmand'); ?></th>
										<th class="number"><?php echo __('Maier Grill'); ?></th>
										<th class="number"><?php echo __('Other'); ?></th>
										<th class="number"><?php echo __('Total'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($data as $event): ?>
									<tr>
										<td><?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?></td>
										<td><?php echo $this->Html->link($this->Text->truncate($event['Client']['name'], 35), array('controller' => 'clients', 'action' => 'view', $event['Client']['id']), array('target' => '_blank')); ?></td>
										<td><?php echo $this->Html->link($event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id']), array('target' => '_blank')); ?></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="deposit_number" class="editable" data-value="<?php echo $event['Event']['deposit_number']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="deposit_amount" class="editable" data-value="<?php echo $event['Event']['deposit_amount']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="invoice_number" class="editable" data-value="<?php echo $event['Event']['invoice_number']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_corporate_outing" class="editable" data-value="<?php echo $event['Event']['repartition_corporate_outing']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_team_building" class="editable" data-value="<?php echo $event['Event']['repartition_team_building']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_urbangaming" class="editable" data-value="<?php echo $event['Event']['repartition_urbangaming']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_events_creation" class="editable" data-value="<?php echo $event['Event']['repartition_events_creation']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_effet_gourmand" class="editable" data-value="<?php echo $event['Event']['repartition_effet_gourmand']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_maiergrill" class="editable" data-value="<?php echo $event['Event']['repartition_maiergrill']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="repartition_misc" class="editable" data-value="<?php echo $event['Event']['repartition_misc']; ?>"></a></td>
										<td><a href="javascript:;" data-pk="<?php echo $event['Event']['id']; ?>" data-name="invoice_amount" class="editable" data-value="<?php echo $event['Event']['invoice_amount']; ?>"></a></td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					<?php $j++; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
