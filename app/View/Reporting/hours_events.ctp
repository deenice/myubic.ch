<div class="row reporting">
	<div class="col-md-12">
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject bold uppercase"><?php echo __('Hours by event'); ?></span>
				</div>
				<div class="actions btn-group">
					<?php echo $this->Html->link(__('By event'), array('controller' => 'reporting', 'action' => 'hours', 'events'), array('class' => 'btn default active')); ?>
					<?php echo $this->Html->link(__('By person'), array('controller' => 'reporting', 'action' => 'hours', 'staff'), array('class' => 'btn default')); ?>
				</div>
			</div>
			<div class="portlet-body">
				<ul class="nav nav-tabs">
					<?php $i=0;foreach($months as $month => $data): ?>
          <li class="<?php echo $i == 0 ? 'active':''; ?>">
            <a href="#month<?php echo $month; ?>" data-toggle="tab"> <?php echo ucfirst($this->Time->format($month, '%B %Y')); ?> </a>
          </li>
					<?php $i++; endforeach; ?>
        </ul>
				<div class="tab-content">
					<?php $j=0; foreach($events as $month => $companies): ?>
					<div class="tab-pane <?php echo $j==0 ? 'active':''; ?>" id="month<?php echo $month; ?>">
						<?php foreach($companies as $company => $events): ?>
						<div class="portlet">
							<div class="portlet-body">
								<table class="table table-striped hours">
									<thead>
										<tr>
											<th class="check"></th>
											<th class="code"></th>
											<th class="name"><?php echo __('Name'); ?></th>
											<th class="client"><?php echo __('Client'); ?></th>
											<th class="hours"><?php echo __('Hours'); ?></th>
											<th class="amount"><?php echo __('Salaries'); ?></th>
											<!-- <th class="actions"><?php echo __('Actions'); ?></th> -->
										</tr>
									</thead>
									<tbody>
										<?php foreach($events as $event): ?>
										<tr>
											<td><input type="checkbox" name="name" value=""></td>
											<td><?php echo $this->Html->link(empty($event['Event']['code']) ? __('Edit') : $event['Event']['code'], array('controller' => 'events', 'action' => 'work', $event['Event']['id'])); ?></td>
											<td>
												<span class="btn btn-xs btn-company btn-company-<?php echo $company; ?>"></span>
												<?php if($event['Event']['type'] == 'mission'): ?>
													<i class="fa fa-crosshairs"></i>
												<?php endif; ?>
												<?php echo $this->Text->truncate($event['Event']['name'], 50); ?>
											</td>
											<td>
												<?php echo $this->Text->truncate($event['Client']['name'], 40); ?>
											</td>
											<td><?php echo $event['Event']['costs']['hours']; ?></td>
											<td><?php echo sprintf('%s CHF', $event['Event']['costs']['salary']); ?></td>
											<!-- <td><a href="#">Voir les détails</a></td> -->
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<?php endforeach; ?>
					</div>
					<?php $j++; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
