<?php if(!empty($jobs)): ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo __('%s - Jobs during month of %s', $user['User']['full_name'], $this->Time->format($date, '%B %Y')); ?></h4>
	<div class="pull-right">
	</div>
</div>
<div class="modal-body">
	<table class="table table-striped">
		<thead>
			<tr>
				<th></th>
				<th><?php echo __('Date'); ?></th>
				<th><?php echo __('Event'); ?></th>
				<th><?php echo __('Schedule'); ?></th>
				<th><?php echo __('Rate'); ?></th>
				<th><?php echo __('Salary'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($jobs as $job): ?>
				<tr>
					<td><span class="btn btn-company btn-xs btn-company-<?php echo $job['Job']['Event']['Company']['class']; ?>"></span></td>
					<td><?php echo $this->Time->format('d.m.Y', $job['Job']['Event']['confirmed_date']); ?></td>
					<td style="width: 35%"><?php echo $this->Html->link(empty($job['Job']['Event']['code_name']) ? $job['Job']['Event']['name'] : $job['Job']['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $job['Job']['Event']['id']), array('target' => '_blank')); ?></td>
					<td><?php echo sprintf('%s - %s', $this->Time->format(empty($job['JobUser']['effective_start_hour']) ? $job['Job']['start_time'] : $job['JobUser']['effective_start_hour'], '%H:%M'), $this->Time->format(empty($job['JobUser']['effective_end_hour']) ? $job['Job']['end_time'] : $job['JobUser']['effective_end_hour'], '%H:%M')); ?></td>
					<td><?php echo empty($job['JobUser']['salary']) ? $job['Job']['salary'] : $job['JobUser']['salary']; ?> CHF / h</td>
					<td><?php echo $job['JobUser']['total']; ?> CHF</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php foreach($wealthingsText as $company => $strings): ?>
		<strong><?php echo $company; ?></strong>
		<textarea class="form-control" cols="30" rows="<?php echo sizeof($strings) + 1; ?>"><?php echo implode("\n", $strings); ?></textarea>
		<br>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if(!empty($events)): ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">
		<?php echo __($title, $this->Time->format($date, '%B %Y'), $company1['Company']['name']); ?>
		<?php echo $this->Html->link(sprintf('<i class="fa fa-print"></i> %s', __('Printable version')), $printUrl, array('escape' => false, 'class' => 'btn btn-sm btn-default pull-right hidden-print', 'style' => 'margin-right: 20px', 'target' => '_blank')); ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-striped table-reporting">
		<thead>
			<tr>
				<?php if($model == 'offers'): ?>
				<th><?php echo __('Opening date'); ?></th>
				<?php endif; ?>
				<th><?php echo __('Date'); ?></th>
				<th><?php echo __('Event'); ?></th>
				<th><?php echo __('Client'); ?></th>
				<th><?php echo __('Projected TO'); ?></th>
				<?php if($model == 'repartition' && in_array($company, array('ubic', 'ug'))): ?>
				<th><?php echo __('Corporate outing'); ?></th>
				<th><?php echo __('Team building'); ?></th>
				<th><?php echo __('Event planning'); ?></th>
				<th><?php echo __('Digital games'); ?></th>
				<th><?php echo __('Serious game'); ?></th>
				<th><?php echo __('Effet Gourmand'); ?></th>
				<th><?php echo __('Maiergrill'); ?></th>
				<th><?php echo __('Other'); ?></th>
				<th><?php echo __('Total'); ?></th>
				<?php elseif($model == 'repartition' && $company == 'eg'): ?>
				<th><?php echo __('EG direct'); ?></th>
				<th><?php echo __('EG UBIC'); ?></th>
				<th><?php echo __('Other'); ?></th>
				<th><?php echo __('Total'); ?></th>
				<?php elseif($model == 'repartition' && $company == 'mg'): ?>
				<th><?php echo __('MG direct'); ?></th>
				<th><?php echo __('MG UBIC'); ?></th>
				<th><?php echo __('Other'); ?></th>
				<th><?php echo __('Total'); ?></th>
				<?php else: ?>
				<th><?php echo __('Status'); ?></th>
				<th><?php echo __('Feeling'); ?></th>
				<?php endif; ?>
			</tr>
		</thead>
		<tbody>
			<?php
				$repartition_corporate_outing = 0;
				$repartition_team_building = 0;
				$repartition_events_creation = 0;
				$repartition_digital_games = 0;
				$repartition_serious_game = 0;
				$repartition_effet_gourmand = 0; // in an ubic event
				$repartition_maiergrill = 0; // in an ubic event
				$repartition_eg_direct = 0;
				$repartition_eg_ubic = 0;
				$repartition_mg_direct = 0;
				$repartition_mg_ubic = 0;
				$repartition_misc = 0;
				$invoice_amount = 0;
				$projected_turnover = 0;
			?>
			<?php foreach($events as $event): ?>
				<?php
					$repartition_corporate_outing += !empty($event['Event']['repartition_corporate_outing']) ? $event['Event']['repartition_corporate_outing'] : 0;
					$repartition_team_building += !empty($event['Event']['repartition_team_building']) ? $event['Event']['repartition_team_building'] : 0;
					$repartition_events_creation += !empty($event['Event']['repartition_events_creation']) ? $event['Event']['repartition_events_creation'] : 0;
					$repartition_digital_games += !empty($event['Event']['repartition_digital_games']) ? $event['Event']['repartition_digital_games'] : 0;
					$repartition_serious_game += !empty($event['Event']['repartition_serious_game']) ? $event['Event']['repartition_serious_game'] : 0;
					$repartition_effet_gourmand += !empty($event['Event']['repartition_effet_gourmand']) ? $event['Event']['repartition_effet_gourmand'] : 0;
					$repartition_maiergrill += !empty($event['Event']['repartition_maiergrill']) ? $event['Event']['repartition_maiergrill'] : 0;
					$repartition_misc += !empty($event['Event']['repartition_misc']) ? $event['Event']['repartition_misc'] : 0;
					$repartition_eg_direct += !empty($event['Event']['repartition_eg_direct']) ? $event['Event']['repartition_eg_direct'] : 0;
					$repartition_eg_ubic += !empty($event['Event']['repartition_eg_ubic']) ? $event['Event']['repartition_eg_ubic'] : 0;
					$repartition_mg_direct += !empty($event['Event']['repartition_mg_direct']) ? $event['Event']['repartition_mg_direct'] : 0;
					$repartition_mg_ubic += !empty($event['Event']['repartition_mg_ubic']) ? $event['Event']['repartition_mg_ubic'] : 0;
					$invoice_amount += !empty($event['Event']['invoice_amount']) ? $event['Event']['invoice_amount'] : 0;
					$projected_turnover += !empty($event['Event']['projected_turnover']) ? $event['Event']['projected_turnover'] : 0;
				?>
				<tr>
					<?php if($model == 'offers'): ?>
					<td><?php echo $this->Time->format($event['Event']['opening_date'], '%d %B %Y'); ?></td>
					<?php endif; ?>
					<td>
						<?php if(!empty($event['Event']['confirmed_date'])): ?>
							<?php echo $this->Time->format('d.m.Y', $event['Event']['confirmed_date']); ?>
						<?php elseif(!empty($event['Date'])): ?>
							<?php foreach($event['Date'] as $k => $date): ?>
								<?php echo $this->Time->format('d.m.Y', $date['date']); ?>
								<?php echo !empty($event['Date'][$k+1]) ? __('or') : ''; ?>
							<?php endforeach; ?>
						<?php elseif(!empty($event['Event']['period_start'])): ?>
							<?php echo $this->Time->format('d.m.Y', $event['Event']['period_start']); ?> <?php echo __('to'); ?> <?php echo $this->Time->format('d.m.Y', $event['Event']['period_end']); ?>
						<?php endif;?>
					</td>
					<td>
						<?php if($model == 'repartition'): ?>
							<span class="btn btn-xs btn-company btn-company-<?php echo $event['Company']['class']; ?>"></span>
						<?php endif; ?>
						<?php echo $this->Html->link(empty($event['Event']['code_name']) ? $event['Event']['name'] : $event['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $event['Event']['id']), array('target' => '_blank')); ?>
					</td>
					<td><?php echo $event['Client']['name']; ?></td>
					<td><?php echo sprintf('%s CHF', number_format($event['Event']['projected_turnover'], 2, '.', "'")); ?></td>
					<?php if($model == 'repartition' && in_array($company, array('ubic', 'ug'))): ?>
						<td><?php echo !is_null($event['Event']['repartition_corporate_outing']) ? sprintf('%s CHF', number_format($event['Event']['repartition_corporate_outing'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_team_building']) ? sprintf('%s CHF', number_format($event['Event']['repartition_team_building'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_events_creation']) ? sprintf('%s CHF', number_format($event['Event']['repartition_events_creation'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_digital_games']) ? sprintf('%s CHF', number_format($event['Event']['repartition_digital_games'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_serious_game']) ? sprintf('%s CHF', number_format($event['Event']['repartition_serious_game'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_effet_gourmand']) ? sprintf('%s CHF', number_format($event['Event']['repartition_effet_gourmand'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_maiergrill']) ? sprintf('%s CHF', number_format($event['Event']['repartition_maiergrill'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_misc']) ? sprintf('%s CHF', number_format($event['Event']['repartition_misc'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['invoice_amount']) ? sprintf('%s CHF', number_format($event['Event']['invoice_amount'], 2, '.', "'")) : ''; ?></td>
					<?php elseif($model == 'repartition' && $company == 'eg'): ?>
						<td><?php echo !is_null($event['Event']['repartition_eg_direct']) ? sprintf('%s CHF', number_format($event['Event']['repartition_eg_direct'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_eg_ubic']) ? sprintf('%s CHF', number_format($event['Event']['repartition_eg_ubic'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_misc']) ? sprintf('%s CHF', number_format($event['Event']['repartition_misc'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['invoice_amount']) ? sprintf('%s CHF', number_format($event['Event']['invoice_amount'], 2, '.', "'")) : ''; ?></td>
					<?php elseif($model == 'repartition' && $company == 'mg'): ?>
						<td><?php echo !is_null($event['Event']['repartition_mg_direct']) ? sprintf('%s CHF', number_format($event['Event']['repartition_mg_direct'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_mg_ubic']) ? sprintf('%s CHF', number_format($event['Event']['repartition_mg_ubic'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['repartition_misc']) ? sprintf('%s CHF', number_format($event['Event']['repartition_misc'], 2, '.', "'")) : ''; ?></td>
						<td><?php echo !is_null($event['Event']['invoice_amount']) ? sprintf('%s CHF', number_format($event['Event']['invoice_amount'], 2, '.', "'")) : ''; ?></td>
					<?php else: ?>
					<td><?php echo $statuses[$event['Event']['crm_status']]; ?></td>
					<td><?php echo sprintf('%s %%', number_format($event['Event']['feeling'], 0, '.', "'")); ?></td>
					<?php endif; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
		<?php if($model == 'repartition'): ?>
		<?php $total = $repartition_corporate_outing + $repartition_team_building + $repartition_events_creation + $repartition_digital_games + $repartition_serious_game + $repartition_effet_gourmand + $repartition_maiergrill + $repartition_misc;?>
		<tfoot>
			<tr>
				<td colspan="3"></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($projected_turnover, 2, '.', "'")); ?></td>
				<?php if($model == 'repartition' && in_array($company, array('ubic', 'ug'))): ?>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_corporate_outing, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_team_building, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_events_creation, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_digital_games, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_serious_game, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_effet_gourmand, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_maiergrill, 2, '.', "'")); ?></td>
				<?php elseif($model == 'repartition' && $company == 'eg'): ?>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_eg_direct, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_eg_ubic, 2, '.', "'")); ?></td>
				<?php elseif($model == 'repartition' && $company == 'mg'): ?>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_mg_direct, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_mg_ubic, 2, '.', "'")); ?></td>
				<?php endif; ?>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_misc, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format(($company == 'ubic') ? $total : ($invoice_amount - $total), 2, '.', "'")); ?></td>
			</tr>
		</tfoot>
		<?php endif; ?>
	</table>
</div>
<?php endif; ?>

<?php if(!empty($orders)): ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">
		<?php echo __($title, $this->Time->format($date, '%B %Y'), 'Festiloc'); ?>
		<?php echo $this->Html->link(sprintf('<i class="fa fa-print"></i> %s', __('Printable version')), $printUrl, array('escape' => false, 'class' => 'btn btn-sm btn-default pull-right hidden-print', 'style' => 'margin-right: 20px', 'target' => '_blank')); ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-striped table-reporting">
		<thead>
			<tr>
				<th><?php echo __('Date'); ?></th>
				<th><?php echo __('Event'); ?></th>
				<th><?php echo __('Client'); ?></th>
				<th><?php echo __('Total HT'); ?></th>
				<?php if($model == 'repartition'): ?>
				<th><?php echo __('Rental TO'); ?></th>
				<th><?php echo __('Other TO'); ?></th>
				<th><?php echo __('Transport TO'); ?></th>
				<?php endif; ?>
				<th><?php echo __('Status'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
				$repartition_rental = 0;
				$repartition_other = 0;
				$repartition_delivery_costs = 0;
				$total_ht = 0;
			?>
			<?php foreach($orders as $order): ?>
			<?php
				$repartition_rental += $order['StockOrder']['repartition_rental'];
				$repartition_other += $order['StockOrder']['repartition_other'];
				$repartition_delivery_costs += !is_null($order['StockOrder']['forced_delivery_costs']) ? $order['StockOrder']['forced_delivery_costs'] : $order['StockOrder']['delivery_costs'];
        if($order['StockOrder']['status'] == 'invoiced'){
          $ht = $order['StockOrder']['invoiced_total'] + $order['StockOrder']['total_deposit'] - $order['StockOrder']['tva'];
        } else {
          $ht = $order['StockOrder']['total_ht'] - $order['StockOrder']['fidelity_discount_amount'] - $order['StockOrder']['quantity_discount'];
        }
        $ht = round($ht / 0.05) * 0.05;
        $total_ht += $ht;
			?>
				<tr>
					<td>
						<?php if(!empty($order['StockOrder']['return_date'])): ?>
							<?php echo $this->Time->format('d.m.Y', $order['StockOrder']['return_date']); ?>
						<?php endif;?>
					</td>
					<td>
						<?php echo $this->Html->link(sprintf('%s %s', $order['StockOrder']['order_number'], $order['StockOrder']['name']), array('controller' => 'stock_orders', 'action' => 'edit', $order['StockOrder']['id']), array('target' => '_blank')); ?>
					</td>
					<td><?php echo $order['Client']['name']; ?></td>
					<td><?php echo sprintf('%s CHF', number_format($ht, 2, '.', "'")); ?></td>
					<?php if($model == 'repartition'): ?>
					<td><?php echo sprintf('%s CHF', number_format($order['StockOrder']['repartition_rental'], 2, '.', "'")); ?></td>
					<td><?php echo sprintf('%s CHF', number_format($order['StockOrder']['repartition_other'], 2, '.', "'")); ?></td>
					<td><?php echo sprintf('%s CHF', number_format(!is_null($order['StockOrder']['forced_delivery_costs']) ? $order['StockOrder']['forced_delivery_costs'] : $order['StockOrder']['delivery_costs'], 2, '.', "'")); ?></td>
					<?php endif; ?>
					<td><?php echo $statuses_fl[$order['StockOrder']['status']]; ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
		<?php if($model == 'repartition'): ?>
		<tfoot>
			<tr>
				<td colspan="3">&nbsp;</td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($total_ht, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_rental, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_other, 2, '.', "'")); ?></td>
				<td class="text-left"><?php echo sprintf('%s CHF', number_format($repartition_delivery_costs, 2, '.', "'")); ?></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		<?php endif; ?>
	</table>
</div>
<?php endif; ?>
