<?php $this->assign('page_title', $this->Html->link(__('Vehicles'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Add an unavailability'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add an unavailability'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">

		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('VehicleReservation', array('class' => 'form-horizontal')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies, 'empty' => true));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Model'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_company_model_id', array('label' => false, 'class' => 'form-control bs-select models', 'empty' => true, 'data-vehicles-list' => 'vehicles'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Vehicle'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_id', array('label' => false, 'class' => 'form-control bs-select', 'id' => 'vehicles'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('reason', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('VehicleReservations.reasons'), 'empty' => __('Specify the reason of the unavailability'), 'required' => true));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Dates'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('start', array('label' => false, 'div' => false, 'class' => 'form-control datetime-picker input-small input-inline', 'type' => 'text'));?>
					<span class="help-text"><?php echo __('to'); ?> </span>
					<?php echo $this->Form->input('end', array('label' => false, 'div' => false, 'class' => 'form-control datetime-picker input-small input-inline', 'type' => 'text'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Driver'); ?> / <?php echo __('Person in charge'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('driver_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $drivers, 'data-live-search' => true, 'required' => true, 'value' => AuthComponent::user('id')));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<!--button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button-->
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.vehicle_reservations();';
$this->end();
?>
