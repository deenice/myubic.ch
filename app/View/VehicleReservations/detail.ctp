<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title">
		<?php if($reservation['VehicleReservation']['cancelled']): ?>
			<span class="btn btn-sm btn-danger"><?php echo __('Cancelled'); ?></span>
		<?php endif; ?>
		<?php if(!empty($reservation['VehicleTour'])): ?>
		<?php echo __('Tour') . ' ' . $reservation['VehicleTour'][0]['code']; ?>
		<?php elseif(!empty($reservation['Event'])): ?>
		<?php echo __('Event') . ' ' . $reservation['Event'][0]['code_name']; ?>
		<?php elseif(!empty($reservation['VehicleReservation']['reason'])): ?>
		<?php echo __('Unavailability'); ?>
		<?php else: ?>
		Réservation
		<?php endif; ?>
	</h4>
</div>
<div class="modal-body">
	<div class="row">
		<div class="col-md-12">
			<?php if(!empty($event)): ?>
			<table class="table table-reservation-event">
				<thead>
					<tr>
						<?php if($event['type'] != 'mission'): ?>
						<th class="event"><?php echo __('Event'); ?></th>
						<th class="client"><?php echo __('Client'); ?></th>
						<th class="content"><?php echo __('Content'); ?></th>
						<th class="place"><?php echo __('Place'); ?></th>
						<?php else: ?>
						<th class="mission"><?php echo __('Mission'); ?></th>
						<?php endif; ?>
						<th class="manager"><?php echo __('Event manager'); ?></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $this->Html->link(empty($event['code_name']) ? $event['name'] : $event['code_name'], array('controller' => 'events', 'action' => 'work', $event['id']), array('target' => '_blank')); ?></td>
						<?php if($event['type'] != 'mission'): ?>
						<td><?php echo !empty($event['Client']['name']) ? $event['Client']['name'] : ''; ?></td>
						<td>
							<?php if(!empty($event['SelectedActivity'])): ?>
								<?php foreach($event['SelectedActivity'] as $activity): ?>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
								<?php endforeach; ?>
							<?php elseif(!empty($event['SuggestedActivity'])): ?>
								<?php foreach($event['SuggestedActivity'] as $activity): ?>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-trophy"></i> %s', $activity['Activity']['slug']), array('controller' => 'activities', 'action' => 'view', $activity['Activity']['id']), array('class' => 'btn btn-xs btn-info uppercase', 'target' => '_blank', 'escape' => false)); ?>
								<?php endforeach; ?>
							<?php endif; ?>
							<?php if(!empty($event['SelectedFBModule'])): ?>
								<?php foreach($event['SelectedFBModule'] as $fb_module): ?>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? '':$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
								<?php endforeach; ?>
							<?php elseif(!empty($event['SuggestedFBModule'])): ?>
								<?php foreach($event['SuggestedFBModule'] as $fb_module): ?>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-cutlery"></i> %s', empty($fb_module['FBModule']['slug']) ? '':$fb_module['FBModule']['slug']), array('controller' => 'fb_modules', 'action' => 'view', $fb_module['FBModule']['id']), array('class' => 'btn btn-xs btn-danger uppercase', 'target' => '_blank', 'escape' => false)); ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</td>
						<td>
							<?php if(!empty($event['ConfirmedEventPlace'])): ?>
								<?php foreach($event['ConfirmedEventPlace'] as $place): ?>
									<?php echo $place['Place']['name']; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</td>
						<?php endif; ?>
						<td><?php echo $event['PersonInCharge']['full_name']; ?></td>
					</tr>
				</tbody>
			</table>
			<?php endif; ?>
			<?php if(!empty($orders)): ?>
			<table class="table">
				<thead>
					<tr>
						<th style="width: 40%"><?php echo __('Order'); ?></th>
						<th style="width: 30%"><?php echo __('Address'); ?></th>
						<th style="width: 30%"><?php echo __('Volume'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($orders as $k => $order): ?>
					<?php
			        $numberOfPallets = 0;
			        if( !empty($order['number_of_pallets1']) ){
			        	$numberOfPallets = $order['number_of_pallets1'];
			        } elseif( !empty($order['number_of_pallets']) ){
			        	$numberOfPallets = $order['number_of_pallets'];
			        }
			        $numberOfRollis = 0;
			        if( !empty($order['number_of_rollis1']) ){
			        	$numberOfRollis = $order['number_of_rollis1'];
			        } elseif( !empty($order['number_of_rollis']) ){
			        	$numberOfRollis = $order['number_of_rollis'];
			        }
			        $xlSurcharge = 0;
			        if( !empty($order['xl_surcharge1']) ){
			        	$xlSurcharge = $order['xl_surcharge1'];
			        } elseif( !empty($order['xl_surcharge']) ){
			        	$xlSurcharge = $order['xl_surcharge'];
			        }
					?>
					<tr data-stockorder-id="<?php echo $order['id']; ?>">
						<td>
							<strong><?php echo __(strtoupper($order['StockOrdersVehicleTour']['type'])); ?></strong><br />
							<?php echo $order['order_number']; ?>
							<?php echo $order['name']; ?><br />
							<?php echo $order['Client']['name']; ?>
						</td>
						<td>
							<?php if($order['StockOrdersVehicleTour']['type'] == 'delivery'): ?>
								<?php echo $order['delivery_address']; ?><br />
								<?php echo $order['delivery_zip_city']; ?>
							<?php endif ?>
							<?php if($order['StockOrdersVehicleTour']['type'] == 'return'): ?>
								<?php echo $order['return_address']; ?><br />
								<?php echo $order['return_zip_city']; ?>
							<?php endif ?>
						</td>
						<td>
							<ul class="list-unstyled">
								<?php if(!empty($numberOfPallets)): ?>
									<li><?php echo __('Number of pallets'); ?>: <?php echo $numberOfPallets; ?></li>
								<?php endif; ?>
								<?php if(!empty($numberOfRollis)): ?>
									<li><?php echo __('Number of rollis'); ?>: <?php echo $numberOfRollis; ?></li>
								<?php endif; ?>
								<?php if(!empty($xlSurcharge)): ?>
									<li><?php echo __('XL surcharge'); ?>: <?php echo $xlSurcharge; ?></li>
								<?php endif; ?>
							</ul>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12 reservation" data-reservation-id="<?php echo $reservation['VehicleReservation']['id']; ?>">
					<?php if(empty($reservation['VehicleReservation']['reason'])): ?>
					<div class="row">
						<div class="col-md-6">
							<h4>
								<?php echo __('Select a model and/or a vehicle'); ?>
							</h4>
						</div>
						<div class="col-md-6 text-right">
							<?php if(!empty($reservation['VehicleReservation']['family'])): ?><h4><small><?php echo __('Desired vehicle/trailer: %s', $this->Html->tag('strong', $families[$reservation['VehicleReservation']['family']])); ?></small></h4><?php endif; ?>
						</div>
						<div class="clearfix"></div>
						<?php if(!empty($models)): ?>
						<div class="col-md-6">
							<?php echo $this->Form->input('models', array('class' => 'form-control bs-select models', 'label' => false, 'options' => $models, 'data-live-search' => true, 'empty' => __('Select the vehicle model'), 'value' => empty($reservation) ? '' : $reservation['VehicleCompanyModel']['id'], 'data-vehicles-list' => 'vehicles')); ?>
						</div>
						<?php else: ?>
						<div class="col-md-6">
							<?php echo $this->Form->input('families', array('class' => 'form-control bs-select families', 'label' => false, 'options' => $families, 'data-live-search' => true, 'empty' => __('Select the vehicle family'), 'value' => empty($reservation['VehicleReservation']['family']) ? '' : $reservation['VehicleReservation']['family'], 'data-vehicles-list' => 'vehicles')); ?>
						</div>
						<?php endif; ?>
						<div class="col-md-6">
							<?php echo $this->Form->input('vehicles', array('class' => 'form-control bs-select vehicles', 'label' => false, 'options' => $vehicles, 'empty' => __('Select the vehicle'), 'id' => 'vehicles', 'data-default' => empty($reservation) ? '' : $reservation['Vehicle']['id'], 'value' => empty($reservation) ? '' : $reservation['Vehicle']['id'])); ?>
						</div>
					</div>
					<?php endif; ?>
					<div class="row">
						<div class="col-md-12">
							<h4><?php echo __('Start'); ?></h4>
						</div>
						<?php if(empty($reservation['VehicleReservation']['reason'])): ?>
						<div class="col-md-4">
						<?php else: ?>
						<div class="col-md-12">
						<?php endif; ?>
							<?php echo $this->Form->input('start', array('type' => 'text', 'class' => 'form-control datetime-picker start', 'label' => false, 'value' => empty($reservation) ? '' : $this->Time->format($reservation['VehicleReservation']['start'], '%d-%m-%Y - %H:%M'))); ?>
						</div>
						<?php if(empty($reservation['VehicleReservation']['reason'])): ?>
						<div class="col-md-8">
							<?php echo $this->Form->input('start_place', array('class' => 'form-control start_place', 'label' => false, 'placeholder' => __('Start place'), 'value' => empty($reservation) ? '' : $reservation['VehicleReservation']['start_place'] )); ?>
						</div>
						<?php endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h4><?php echo __('End'); ?></h4>
						</div>
						<?php if(empty($reservation['VehicleReservation']['reason'])): ?>
						<div class="col-md-4">
						<?php else: ?>
						<div class="col-md-12">
						<?php endif; ?>
							<?php echo $this->Form->input('end', array('type' => 'text', 'class' => 'form-control datetime-picker end', 'label' => false, 'value' => empty($reservation) ? '' : $this->Time->format($reservation['VehicleReservation']['end'], '%d-%m-%Y - %H:%M'))); ?>
						</div>
						<?php if(empty($reservation['VehicleReservation']['reason'])): ?>
						<div class="col-md-8">
							<?php echo $this->Form->input('end_place', array('class' => 'form-control end_place', 'label' => false, 'placeholder' => __('End place'), 'value' => empty($reservation) ? '' : $reservation['VehicleReservation']['end_place'])); ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<?php echo $this->Form->input('confirmed', array('type' => 'hidden', 'value' => $reservation['VehicleReservation']['confirmed'])); ?>
	<?php echo $this->Form->input('archived', array('type' => 'hidden', 'value' => $reservation['VehicleReservation']['archived'])); ?>
	<?php echo $this->Form->input('tour', array('type' => 'hidden', 'value' => !empty($reservation['VehicleTour']) ? $reservation['VehicleTour'][0]['id'] : '')); ?>
	<?php echo $this->Form->input('event', array('type' => 'hidden', 'value' => !empty($reservation['Event']) ? $reservation['Event'][0]['id'] : '')); ?>
	<?php if($reservation['VehicleReservation']['cancelled']): ?>
	<button type="submit" class="btn btn-warning" data-mode="archive"><i class="fa fa-check"></i> J'ai traité l'annulation</button>
	<?php else: ?>
	<button type="button" class="btn default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo __('Close'); ?></button>
	<?php if(!empty($reservation['VehicleReservation']['reason'])): ?>
		<button type="submit" class="btn btn-danger" data-mode="delete"><i class="fa fa-trash-o"></i> <?php echo __('Delete'); ?></button>
	<?php endif; ?>
	<button type="submit" class="btn blue" data-mode="save"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
	<?php if(empty($reservation['VehicleReservation']['confirmed']) && empty($reservation['VehicleReservation']['reason'])): ?>
	<button type="submit" class="btn green" data-mode="confirm"><i class="fa fa-check"></i> <?php echo __('Confirm'); ?></button>
	<?php endif; ?>
	<?php endif; ?>
</div>
