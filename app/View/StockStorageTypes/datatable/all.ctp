<?php
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
		'<i class="fa fa-edit"></i> ' . __("Edit"),
		array('controller' => 'stock_storage_types', 'action' => 'edit', $result['StockStorageType']['id']),
		array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

	$dimensions = sprintf('%s cm x %s cm x %s cm, %s cm3, %s g', $result['StockStorageType']['width'], $result['StockStorageType']['depth'], $result['StockStorageType']['depth'], $result['StockStorageType']['volume'], $result['StockStorageType']['weight']);

	$this->dtResponse['aaData'][] = array(
		$result['StockStorageType']['name'],
		$dimensions,
		$actions
	);
}
