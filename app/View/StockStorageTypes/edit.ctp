<?php $this->assign('page_title', $this->Html->link(__('Storage types'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $storageType['StockStorageType']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-social-dropbox font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $storageType['StockStorageType']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockStorageType', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Empty weight'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('weight', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline')); ?>
						<span class="help-inline">g</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Width'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('width', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline')); ?>
						<span class="help-inline">cm</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Height'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('height', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline')); ?>
						<span class="help-inline">cm</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Length'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('depth', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline')); ?>
						<span class="help-inline">cm</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Volume'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('volume', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline')); ?>
						<span class="help-inline">cm3</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Preferred support'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('preferred_support', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockStorageTypes.supports'))); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Minimal number of pallets'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('min_number_of_pallets', array('label' => false, 'class' => 'form-control input-small', 'min' => 0)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Minimal number of rollis'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('min_number_of_rollis', array('label' => false, 'class' => 'form-control input-small', 'min' => 0)); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
