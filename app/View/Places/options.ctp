<?php $this->assign('page_title', __('Places')); ?>
<?php $this->assign('page_subtitle', __('Status of the options')); ?>
<div class="tabbable-custom nav-justified">
	<ul class="nav nav-tabs nav-justified">
		<li class="active">
			<a href="#confirmed" data-toggle="tab"><i class="fa fa-check"></i> <?php echo __('Confirmed'); ?></a>
		</li>
		<li>
			<a href="#prereserved" data-toggle="tab"><i class="fa fa-question"></i> <?php echo __('Prereserved / Option'); ?></a>
		</li>
		<li>
			<a href="#to_discuss" data-toggle="tab"><i class="fa fa-comment-o"></i> <?php echo __('To discuss'); ?></a>
		</li>
		<li>
			<a href="#occupied" data-toggle="tab"><i class="fa fa-ban"></i> <?php echo __('Occupied'); ?></a>
		</li>
		<li>
			<a href="#cancelled" data-toggle="tab"><i class="fa fa-times"></i> <?php echo __('Cancelled'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="confirmed">
			<?php if(!empty($options['confirmed'])): ?>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="confirmedOptions">
						<thead>
							<tr>
								<th><?php echo __('Name of the place'); ?></th>
								<th><?php echo __('Confirmed date of the event'); ?></th>
								<th><?php echo __('Event name'); ?></th>
								<th><?php echo __('Option valid until'); ?></th>
								<th><?php echo __('Made by'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($options['confirmed'] as $option): ?>
							<tr>
								<td><?php echo $option['Place']['name']; ?></td>
								<td><?php echo $option['Moment']['Event']['confirmed_date']; ?></td>
								<td><?php echo $option['Moment']['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($option['Option']['valid_until'], '%d %b %Y'); ?></td>
								<td><?php echo $option['User']['full_name']; ?></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php else: ?>
			<div class="alert alert-warning">
				<p>No options</p>
			</div>
			<?php endif; ?>
		</div>
		<div class="tab-pane" id="prereserved">
			<?php if(!empty($options['prereserved'])): ?>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="prereservedOptions">
						<thead>
							<tr>
								<th><?php echo __('Name of the place'); ?></th>
								<th><?php echo __('Confirmed date of the event'); ?></th>
								<th><?php echo __('Event name'); ?></th>
								<th><?php echo __('Option valid until'); ?></th>
								<th><?php echo __('Made by'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($options['prereserved'] as $option): ?>
							<tr>
								<td><?php echo $option['Place']['name']; ?></td>
								<td><?php echo $option['Event']['confirmed_date']; ?></td>
								<td><?php echo $option['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($option['Option']['valid_until'], '%d %b %Y'); ?></td>
								<td><?php echo $option['User']['full_name']; ?></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php else: ?>
			<div class="alert alert-warning">
				<p>No options</p>
			</div>
			<?php endif; ?>
		</div>
		<div class="tab-pane" id="to_discuss">
			<?php if(!empty($options['to_discuss'])): ?>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="to_discussOptions">
						<thead>
							<tr>
								<th><?php echo __('Name of the place'); ?></th>
								<th><?php echo __('Confirmed date of the event'); ?></th>
								<th><?php echo __('Event name'); ?></th>
								<th><?php echo __('Option valid until'); ?></th>
								<th><?php echo __('Made by'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($options['to_discuss'] as $option): ?>
							<tr>
								<td><?php echo $option['Place']['name']; ?></td>
								<td><?php echo $option['Event']['confirmed_date']; ?></td>
								<td><?php echo $option['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($option['Option']['valid_until'], '%d %b %Y'); ?></td>
								<td><?php echo $option['User']['full_name']; ?></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php else: ?>
			<div class="alert alert-warning">
				<p>No options</p>
			</div>
			<?php endif; ?>
		</div>
		<div class="tab-pane" id="occupied">
			<?php if(!empty($options['occupied'])): ?>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="occupiedOptions">
						<thead>
							<tr>
								<th><?php echo __('Name of the place'); ?></th>
								<th><?php echo __('Confirmed date of the event'); ?></th>
								<th><?php echo __('Event name'); ?></th>
								<th><?php echo __('Option valid until'); ?></th>
								<th><?php echo __('Made by'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($options['occupied'] as $option): ?>
							<tr>
								<td><?php echo $option['Place']['name']; ?></td>
								<td><?php echo $option['Event']['confirmed_date']; ?></td>
								<td><?php echo $option['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($option['Option']['valid_until'], '%d %b %Y'); ?></td>
								<td><?php echo $option['User']['full_name']; ?></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php else: ?>
			<div class="alert alert-warning">
				<p>No options</p>
			</div>
			<?php endif; ?>
		</div>
		<div class="tab-pane" id="cancelled">
			<?php if(!empty($options['cancelled'])): ?>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="cancelledOptions">
						<thead>
							<tr>
								<th><?php echo __('Name of the place'); ?></th>
								<th><?php echo __('Confirmed date of the event'); ?></th>
								<th><?php echo __('Event name'); ?></th>
								<th><?php echo __('Option valid until'); ?></th>
								<th><?php echo __('Made by'); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($options['cancelled'] as $option): ?>
							<tr>
								<td><?php echo $option['Place']['name']; ?></td>
								<td><?php echo $option['Event']['confirmed_date']; ?></td>
								<td><?php echo $option['Event']['name']; ?></td>
								<td><?php echo $this->Time->format($option['Option']['valid_until'], '%d %b %Y'); ?></td>
								<td><?php echo $option['User']['full_name']; ?></td>
							</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>
			<?php else: ?>
			<div class="alert alert-warning">
				<p>No options</p>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.places();';
$this->end();
?>