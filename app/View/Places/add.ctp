<?php $this->assign('page_title', $this->Html->link(__('Places'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('Add a place'));?>
<?php $types = Configure::read('Places.types'); ksort($types); ?>
<?php $placesContactPreferences = Configure::read('Places.contactPreferences'); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-pointer font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a place'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Place', array('class' => 'form-horizontal', 'type' => 'file')); ?>
		<?php echo $this->Form->input('User.id', array('value' => AuthComponent::user('id'))); ?>
		<?php echo $this->Form->input('LastModifiedBy.id', array('value' => AuthComponent::user('id'))); ?>
			<div class="form-actions" style="border:0">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
			<div class="form-body">
				<h3 class="form-section"><?php echo __('Common data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('type', array('options' => $types, 'label' => false, 'class' => 'form-control bs-select'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Private place'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('private', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<span class="help-block">À cocher si le lieu est chez le client.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Default place for planning'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('planning_default', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<span class="help-block">À cocher si c'est un lieu par défaut pour le module de "Planning".</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Website'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('website', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
						<span class="help-block"><?php echo __('URL has to start with http://'); ?></span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Picture/Logo'); ?></label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
								<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=aucune+image" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
							<div>
								<span class="btn default btn-file">
								<span class="fileinput-new"><?php echo __('Select image'); ?></span>
								<span class="fileinput-exists"><?php echo __('Change'); ?></span>
								<?php echo $this->Form->input('thumbnail', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
							</div>
						</div>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Geographical data'); ?></h3>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group" data-commune-select2>
					<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ZipCity', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array()));?>
						<?php echo $this->Form->input('zip', array('label' => false, 'div' => false, 'class' => 'form-control zip', 'rel' => 'return', 'data-name' => 'zip', 'type' => 'hidden')); ?>
						<?php echo $this->Form->input('city', array('label' => false, 'div' => false, 'class' => 'form-control city', 'rel' => 'return', 'data-name' => 'city', 'type' => 'hidden')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Latitude'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('latitude', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Longitude'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('longitude', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Administrative data'); ?></h3>
				<h4 class="form-section"><?php echo __('Contact person'); ?></h4>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Phone 1'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_phone1', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Phone 2'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_phone2', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Email'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_person_email', array('type' => 'email', 'label' => false, 'class' => 'form-control', 'type' => 'email'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Office hours'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('office_hours', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Contact preference pre-reservation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_preference_prereservation', array('label' => false, 'class' => 'form-control bs-select', 'options' => $placesContactPreferences));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Contact preference reservation'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_preference_reservation', array('label' => false, 'class' => 'form-control bs-select', 'options' => $placesContactPreferences));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Contact concierge'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('contact_concierge', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Negociated prices'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('negociated_prices', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Conditions'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('conditions', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Par exemple: Prise des clefs, retour des clefs, condition de nettoyage, caution, spécificité de location, priorité (villageois...), etc</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Unavailabilities'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('unavailabilities', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Miscellaneous'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('administrative_misc', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Accessibility'); ?></h3>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Parking'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('parking', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<?php echo $this->Form->input('parking_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Public transports'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('public_transports', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<?php echo $this->Form->input('public_transports_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
						<span class="help-block">
							<?php echo __('Information about the nearest train/bus stop'); ?>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Reduced mobility'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('reduced_mobility', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<?php echo $this->Form->input('reduced_mobility_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
					<span class="help-block">Doit répondre aux critères: Accès en chaise roulante, toilette adaptée, largeur de porte, terrain adéquat</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Sleep in'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('sleep', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
						<?php echo $this->Form->input('sleep_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Available resources'); ?></h3>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo ('Equipment'); ?></label>
					<div class="col-md-9">
					<?php foreach($materials as $k => $item): ?>
						<div class="col-md-4 col-sm-6">
							<?php echo $this->Form->input('Material.'.$k.'.id', array('class' => 'form-control', 'type' => 'checkbox', 'label' => $item['Tag']['value'], 'div' => false, 'value' => $item['Tag']['id'])); ?>
						</div>
					<?php endforeach; ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Technical'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('technical', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Spécifier le matériel à disposition</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Kitchen incl. dishes'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('dishes', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">
							Spécifier le matériel à disposition, comme lave-vaisselle, four, plaque. Vaisselle pour nombre de personnes.
						</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Furniture'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('furniture', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Par exemple nombre de bancs, chaises, nombre et grandeur des tables</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Miscellaneous'); ?></label>
					<div class="col-md-9">
						<?php //echo $this->Form->input('misc', array('label' => false, 'class' => 'form-control', 'placeholder' => 'under development'));?>
						<?php echo $this->Form->input('miscellaneous_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('available_resources_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Food and beverage'); ?></h3>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Caterer'); ?></label>
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-3">
								<span class="help-inline"><?php echo __('Internal'); ?></span>
							<?php echo $this->Form->input('internal_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							</div>
							<div class="col-md-3">
								<span class="help-inline"><?php echo __('External'); ?></span>
							<?php echo $this->Form->input('external_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							</div>
							<div class="col-md-12">
								<?php echo $this->Form->input('caterer_constraints', array('label' => false, 'class' => 'form-control margin-top-10'));?>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Conditions'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('wine', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Specifications'); ?></h3>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Logistics'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('logistics', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<h3 class="form-section"><?php echo __('Technical data'); ?></h3>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Maximum number of persons'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('max_number_of_persons', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Maximum surface'); ?></label>
					<div class="col-md-3">
						<?php echo $this->Form->input('max_surface', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
						<span class="help-inline">m2</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Number of rooms'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('number_of_rooms', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Ideal for'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Place.ideal_for', array('type' => 'hidden', 'class' => 'form-control select2', 'label' => false, 'data-category' => 'ideal_for')); ?>
						<span class="help-block">L'ajout de nouveaux tags est désactivé.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('ideal_for_infos', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Spécifier quelle activité est recommandée ou pas.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label"><?php echo __('Recommended for'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('activities', array('label' => false, 'class' => 'bs-select form-control input-sm', 'multiple' => true, 'options' => $activities)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Potential'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('degree', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Places.degrees')));?>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('To check'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('to_check', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">Spécifier les champs qu'il faut absolument vérifier.</span>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="control-label col-md-3"><?php echo __('Internal reference'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('referer', array('label' => false, 'class' => 'form-control bs-select', 'options' => $fixedCollaborators, 'empty' => array(0 => __('Select a collaborator'))));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
