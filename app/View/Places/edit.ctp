<?php $this->assign('page_title', $this->Html->link(__('Places'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $this->request->data['Place']['name']);?>
<?php $types = Configure::read('Places.types'); ksort($types); //debug($this->request->data); ?>
<?php
$placesContactPreferences = Configure::read('Places.contactPreferences');
$documentsImagesGroups = Configure::read('Documents.Images.groups');
?>
<div class="tabbable-custom nav-justified">
	<ul class="nav nav-tabs nav-justified">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#configurations" data-toggle="tab"><i class="fa fa-puzzle-piece"></i> <?php echo __('Configurations'); ?></a>
		</li>
		<li>
			<a href="#photos" data-toggle="tab"><i class="fa fa-image"></i> <?php echo __('Photos'); ?></a>
		</li>
		<li>
			<a href="#documents" data-toggle="tab"><i class="fa fa-files-o"></i> <?php echo __('Documents'); ?></a>
		</li>
		<li>
			<a href="#activities" data-toggle="tab"><i class="fa fa-trophy"></i> <?php echo __('Activities'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
			<!-- BEGIN FORM-->
			<?php echo $this->Form->create('Place', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<?php echo $this->Form->input('User.id', array('value' => $this->request->data['Place']['user_id'])); ?>
			<?php echo $this->Form->input('LastModifiedBy.id', array('value' => AuthComponent::user('id'))); ?>
				<?php echo $this->Form->input('id'); ?>
				<div class="form-actions" style="border:0; margin-top: 10px">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
							<button type="button" class="btn default pull-right" data-toggle="modal" data-target="#new-note" data-model="place" data-model-id="<?php echo $this->request->data['Place']['id']; ?>"><i class="fa fa-sticky-note-o"></i> <?php echo __('Add a note'); ?></button>
						</div>
					</div>
				</div>
				<div class="form-body">
					<h3 class="form-section"><?php echo __('Common data'); ?></h3>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('type', array('options' => $types, 'label' => false, 'class' => 'form-control bs-select'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Private place'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('private', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							<span class="help-block">À cocher si le lieu est chez le client. Surtout utilisé par Effet Gourmand.</span>
						</div>
					</div>
  				<div class="form-group">
  					<label for="" class="control-label col-md-3"><?php echo __('Default place for planning'); ?></label>
  					<div class="col-md-9">
  						<?php echo $this->Form->input('planning_default', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
  						<span class="help-block">À cocher si c'est un lieu par défaut pour le module de "Planning".</span>
  					</div>
  				</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Website'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('website', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
							<span class="help-block"><?php echo __('URL has to start with http://'); ?></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Picture/Logo'); ?></label>
						<div class="col-md-9">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									<?php if(!empty($thumbnail)): ?>
									<?php echo $this->Html->image(DS . $thumbnail[0]['Document']['url'], array('max-height' => 150, 'width' => 200)); ?>
									<?php else: ?>
									<img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
									<?php endif; ?>
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
								<div>
									<span class="btn default btn-file">
									<span class="fileinput-new"><?php echo __('Select image'); ?></span>
									<span class="fileinput-exists"><?php echo __('Change'); ?></span>
									<?php echo $this->Form->input('thumbnail', array('label' => false, 'class' => '', 'type' => 'file', 'div' => false));?>
									</span>
									<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"><?php echo __('Remove'); ?> </a>
								</div>
							</div>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Geographical data'); ?></h3>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Address'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('address', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group" data-commune-select2>
						<label class="control-label col-md-3"><?php echo __('ZIP / City'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('ZipCit', array('type' => 'select', 'label' => false, 'class' => 'form-control', 'data-name' => 'zipcity', 'options' => array($this->request->data['Place']['zip_city']), 'value' => $this->request->data['Place']['zip_city']));?>
							<?php echo $this->Form->input('zip', array('label' => false, 'div' => false, 'class' => 'form-control zip', 'rel' => 'return', 'data-name' => 'zip', 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('city', array('label' => false, 'div' => false, 'class' => 'form-control city', 'rel' => 'return', 'data-name' => 'city', 'type' => 'hidden')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Latitude'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('latitude', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Longitude'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('longitude', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Administrative data'); ?></h3>
					<h4 class="form-section"><?php echo __('Contact person'); ?></h4>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_person_name', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Phone 1'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_person_phone1', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Phone 2'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_person_phone2', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Email'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_person_email', array('type' => 'email', 'label' => false, 'class' => 'form-control', 'type' => 'email'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Office hours'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('office_hours', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Contact preference pre-reservation'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_preference_prereservation', array('label' => false, 'class' => 'form-control bs-select', 'options' => $placesContactPreferences, 'empty' => array(0 => '--')));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Contact preference reservation'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_preference_reservation', array('label' => false, 'class' => 'form-control bs-select', 'options' => $placesContactPreferences, 'empty' => array(0 => '--')));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Contact concierge'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('contact_concierge', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Negociated prices'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('negociated_prices', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Conditions'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('conditions', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">Par exemple: Prise des clefs, retour des clefs, condition de nettoyage, caution, spécificité de location, priorité (villageois...), etc</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Unavailabilities'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('unavailabilities', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Miscellaneous'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('administrative_misc', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Accessibility'); ?></h3>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Parking'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('parking', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							<?php echo $this->Form->input('parking_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Public transports'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('public_transports', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							<?php echo $this->Form->input('public_transports_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
							<span class="help-block">
								<?php echo __('Information about the nearest train/bus stop'); ?>
							</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Reduced mobility'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('reduced_mobility', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							<?php echo $this->Form->input('reduced_mobility_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Sleep in'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('sleep', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
							<?php echo $this->Form->input('sleep_infos', array('label' => false, 'class' => 'form-control margin-top-10'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Available resources'); ?></h3>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo ('Equipment'); ?></label>
						<div class="col-md-9">
						<?php foreach($materials as $k => $item): ?>
							<div class="col-md-4 col-sm-6">
								<?php echo $this->Form->input('Material.'.$k.'.id', array('class' => 'form-control', 'type' => 'checkbox', 'label' => $item['Tag']['value'], 'div' => false, 'checked' => !empty($tags['material']) && in_array($item['Tag']['id'], $tags['material']), 'value' => $item['Tag']['id'])); ?>
							</div>
						<?php endforeach; ?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Technical'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('technical', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">Spécifier le matériel à disposition</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Kitchen incl. dishes'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('dishes', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">
								Spécifier le matériel à disposition, comme lave-vaisselle, four, plaque. Vaisselle pour nombre de personnes.
							</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Furniture'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('furniture', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">Par exemple nombre de bancs, chaises, nombre et grandeur des tables</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Miscellaneous'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('miscellaneous_infos', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('available_resources_infos', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Food and beverage'); ?></h3>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Caterer'); ?></label>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-3">
									<span class="help-inline">Internal</span>
								<?php echo $this->Form->input('internal_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
								</div>
								<div class="col-md-3">
									<span class="help-inline">External</span>
								<?php echo $this->Form->input('external_caterer', array('label' => false, 'div' => false, 'class' => 'make-switch input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
								</div>
								<div class="col-md-12">
									<?php echo $this->Form->input('caterer_constraints', array('label' => false, 'class' => 'form-control margin-top-10'));?>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Conditions'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('wine', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Specifications'); ?></h3>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Logistics'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('logistics', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<h3 class="form-section"><?php echo __('Technical data'); ?></h3>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Maximum number of persons'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('Place.max_number_of_persons', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Maximum surface'); ?></label>
						<div class="col-md-3">
							<?php echo $this->Form->input('Place.max_surface', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
							<span class="help-inline">m2</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Number of rooms'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('number_of_rooms', array('label' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Ideal for'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('Place.ideal_for', array('type' => 'hidden', 'class' => 'form-control select2', 'label' => false, 'data-category' => 'ideal_for')); ?>
							<span class="help-block">L'ajout de nouveaux tags est désactivé.</span>
							<?php //echo $this->Form->input('ideal_for', array('label' => false, 'class' => 'form-control', 'options' => Configure::read('Places.ideal_for')));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('ideal_for_infos', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">Spécifier quelle activité est recommandée ou pas.</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Potential'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('degree', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Places.degrees')));?>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('To check'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('to_check', array('label' => false, 'class' => 'form-control'));?>
							<span class="help-block">Spécifier les champs qu'il faut absolument vérifier.</span>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Internal reference'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('referer', array('label' => false, 'class' => 'form-control bs-select', 'options' => $fixedCollaborators, 'empty' => array(0 => '--')));?>
						</div>
					</div>
					<?php if(AuthComponent::user('id') != $this->request->data['Place']['user_id']): ?>
					<div class="form-group">
						<label for="" class="control-label col-md-3"><?php echo __('Checked'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('checked', array('label' => false, 'div' => false, 'class' => 'make-switch input-inline', 'data-on-text' => __('Yes'), 'data-off-text' => __('No')));?>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<!-- END FORM-->
		</div>
		<div class="tab-pane fade" id="configurations">
			<?php echo $this->Form->create('Configuration', array('class' => 'form-horizontal', 'url'=> array('controller'=>'configurations', 'action'=>'save'))); ?>
			<?php echo $this->Form->input('Place.id', array('value' => $this->request->data['Place']['id']));?>
			<div class="form-body sortable" id="sortable_portlets">
				<?php if(empty($place['Configuration'])): ?>
				<div class="portlet light bg-grey-cararra configuration-form portlet-sortable">
					<?php echo $this->Form->input('Configuration.0.id'); ?>
					<?php echo $this->Form->input('Configuration.0.weight', array('type' => 'hidden', 'class' => 'weight')); ?>
					<div class="portlet-title">
						<div class="caption"><i class="fa fa-arrows"></i> <?php echo __('New configuration'); ?></div>
						<div class="actions">
							<a href="#" class="btn btn-circle btn-default btn-sm add-configuration"><i class="fa fa-plus"></i> <?php echo __('Add a configuration'); ?> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.0.name', array('label' => false, 'class' => 'form-control configuration-name'));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Description'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.0.description', array('label' => false, 'class' => 'form-control configuration-description'));?>
							</div>
						</div>
						<?php if(1==2): ?>
						<iv class="form-group">
							<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.0.configuration_type', array('type' => 'hidden', 'class' => 'form-control select2', 'label' => false, 'data-category' => 'configuration_type')); ?>
							</div>
						</div>
						<?php endif; ?>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Surface'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.0.surface', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline'));?>
								<span class="help-inline">m2</span>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Volume'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.0.volume', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline'));?>
								<span class="help-inline">m3</span>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Number of persons'); ?></label>
							<div class="col-md-9">
								<span class="help-inline"><?php echo __('From'); ?></span>
								<?php echo $this->Form->input('Configuration.0.min_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline'));?>
								<span class="help-inline"><?php echo __('to'); ?></span>
								<?php echo $this->Form->input('Configuration.0.max_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline'));?>
							</div>
						</div>
					</div>
				</div>
				<?php else: //debug($this->request->data)?>
				<?php foreach ($place['Configuration'] as $k => $config):?>
					<div class="portlet light bg-grey-cararra configuration-form">
					<?php echo $this->Form->input('Configuration.'.$k.'.id', array('value' => $config['id'])); ?>
					<?php echo $this->Form->input('Configuration.'.$k.'.weight', array('type' => 'hidden', 'class' => 'weight', 'value' => $config['weight'])); ?>
						<div class="portlet-title">
							<div class="caption">
								<?php echo $config['name']; ?>
							</div>
							<div class="actions">
								<a href="#" class="btn btn-circle btn-default btn-sm add-configuration"><i class="fa fa-plus"></i> <?php echo __('Add a configuration'); ?> </a>
								<a href="#" class="btn btn-circle btn-default btn-sm remove-configuration" data-id="<?php echo $config['id']; ?>"><i class="fa fa-trash-o"></i> <?php echo __('Remove this configuration'); ?> </a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="form-group">
								<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('Configuration.'.$k.'.name', array('label' => false, 'class' => 'form-control configuration-name', 'value' => $config['name']));?>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-3"><?php echo __('Description'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('Configuration.'.$k.'.description', array('label' => false, 'class' => 'form-control configuration-description', 'value' => $config['description']));?>
								</div>
							</div>
							<?php if(1==2): ?>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Type'); ?></label>
								<div class="col-md-9">
								<?php echo $this->Form->input('Configuration.'.$k.'.configuration_type', array(
									'type' => 'hidden',
									'class' => 'form-control select2',
									'label' => false,
									'data-category' => 'configuration_type',
									'value' => !empty($tags['Configuration'][$config['id']]) ? implode(',', $tags['Configuration'][$config['id']]):'')); ?>
								</div>
							</div>
							<?php endif; ?>
							<div class="form-group">
								<label for="" class="control-label col-md-3"><?php echo __('Surface'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('Configuration.'.$k.'.surface', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline', 'value' => $config['surface']));?>
									<span class="help-inline">m2</span>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-3"><?php echo __('Volume'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('Configuration.'.$k.'.volume', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline', 'value' => $config['volume']));?>
									<span class="help-inline">m3</span>
								</div>
							</div>
							<div class="form-group">
								<label for="" class="control-label col-md-3"><?php echo __('Number of persons'); ?></label>
								<div class="col-md-9">
									<span class="help-inline"><?php echo __('From'); ?></span>
									<?php echo $this->Form->input('Configuration.'.$k.'.min_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline', 'value' => $config['min_number_of_persons']));?>
									<span class="help-inline"><?php echo __('to'); ?></span>
									<?php echo $this->Form->input('Configuration.'.$k.'.max_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-xsmall input-inline', 'value' => $config['max_number_of_persons']));?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						</div>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="tab-pane fade" id="photos">
			<?php if(!empty($documents['photos'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default">Attribuer une catégorie</button>
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
						<ul class="dropdown-menu" role="menu" id="internalPhotosActions">
						<?php foreach(Configure::read('Documents.Images.groups') as $key => $group): ?>
							<li>
								<a href="#" data-group="<?php echo $key; ?>">
								<?php echo $group; ?> </a>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
					<h3><?php echo __('Existing photos'); ?></h3>
					<?php foreach($documents['photos'] as $k => $doc): ?>
						<?php $title = $doc['Document']['name']; ?>
						<div class="col-md-3 col-sm-4 mix" data-rel="internal<?php echo $k; ?>" data-id="<?php echo $doc['Document']['id']; ?>">
							<div class="mix-inner">
								<?php echo $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $title; ?>
											<div class="pull-right">
												<input type="checkbox" class="selectme" data-id="<?php echo $doc['Document']['id']; ?>">
											</div>
										</h4>
										<small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small><br />
										<small class="group">
										<?php if(!empty($doc['Document']['group'])): ?>
										<?php echo $documentsImagesGroups[$doc['Document']['group']]; ?>
										<?php endif; ?>
										</small>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-search"></i> ' . __('See'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['Document']['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#internal' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="internal<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit photo</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
											<?php echo $this->Form->input('group', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['group'], 'class' => 'form-control bs-select', 'id' => 'group'.$doc['Document']['id'], 'label' => __('Category'), 'options' => Configure::read('Documents.Images.groups'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __('New photos'); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Place']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'place')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'places')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="documents">
			<?php if(!empty($documents['documents'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default">Attribuer une catégorie</button>
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
						<ul class="dropdown-menu" role="menu" id="documentsActions">
						<?php foreach(Configure::read('Documents.Documents.groups') as $key => $group): ?>
							<li>
								<a href="#" data-group="<?php echo $key; ?>">
								<?php echo $group; ?> </a>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
					<h3><?php echo __('Existing documents'); ?></h3>
					<?php foreach($documents['documents'] as $k => $doc): ?>
						<?php $title = $doc['Document']['name']; ?>
						<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
						<div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
							<div class="mix-inner">
								<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $title; ?>
											<div class="pull-right">
												<input type="checkbox" class="selectme" data-id="<?php echo $doc['Document']['id']; ?>">
											</div>
										</h4>
										<small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-eye"></i> ' . __('See'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button',
													'target' => '_blank'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['Document']['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#documents' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="documents<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit document</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
											<?php echo $this->Form->input('group', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['group'], 'class' => 'form-control', 'id' => 'group'.$doc['Document']['id'], 'label' => __('Category'), 'options' => Configure::read('Documents.Documents.groups'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __("New documents"); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'documentsForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Place']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'document')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'places')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf,doc,docx')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="activities">
			<?php echo $this->Form->create('Place', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('Place.id'); ?>
			<div class="form-body">
				<?php if(empty($place['Activity'])): ?>
				<?php echo $this->Form->input('Activity.0.category', array('type' => 'hidden', 'value' => 'place')); ?>
				<div class="portlet light bg-grey-cararra activity-form">
					<div class="portlet-title">
						<div class="caption"><?php echo __('New activity'); ?></div>
						<div class="actions">
							<a href="#" class="btn btn-circle btn-default btn-sm add-activity"><i class="fa fa-plus"></i> <?php echo __('Add an activity'); ?> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.0.name', array('label' => false, 'class' => 'form-control activity-name'));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Description'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.0.description', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Price'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.0.price', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Duration'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.0.duration', array('label' => false, 'class' => 'form-control'));?>
							</div>
						</div>
					</div>
				</div>
				<?php else: ?>
				<?php foreach ($place['Activity'] as $k => $activity): ?>
				<div class="portlet light bg-grey-cararra activity-form">
				<?php echo $this->Form->input('Activity.'.$k.'.id', array('value' => $activity['id'])); ?>
				<?php echo $this->Form->input('Activity.'.$k.'.category', array('type' => 'hidden', 'value' => 'place', 'value' => $activity['category'])); ?>
					<div class="portlet-title">
						<div class="caption"><?php echo $activity['name']; ?></div>
						<div class="actions">
							<a href="#" class="btn btn-circle btn-default btn-sm add-activity"><i class="fa fa-plus"></i> <?php echo __('Add an activity'); ?> </a>
							<a href="#" class="btn btn-circle btn-default btn-sm remove-activity" data-id="<?php echo $activity['id']; ?>"><i class="fa fa-trash-o"></i> <?php echo __('Remove this activity'); ?> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Name'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.'.$k.'.name', array('label' => false, 'class' => 'form-control activity-name', 'value' => $activity['name']));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Description'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.'.$k.'.description', array('label' => false, 'class' => 'form-control', 'value' => $activity['description']));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Price'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.'.$k.'.price', array('label' => false, 'class' => 'form-control', 'value' => $activity['price']));?>
							</div>
						</div>
						<div class="form-group">
							<label for="" class="control-label col-md-3"><?php echo __('Duration'); ?></label>
							<div class="col-md-9">
								<?php echo $this->Form->input('Activity.'.$k.'.duration', array('label' => false, 'class' => 'form-control', 'value' => $activity['duration']));?>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
				<div class="form-actions">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						</div>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Portfolio.init();';
echo 'Custom.places();';
$this->end();
?>
