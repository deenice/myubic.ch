<?php
$placesTypes = Configure::read('Places.types');
$placesDegrees = Configure::read('Places.degrees');
$placesIdealFor = Configure::read('Places.ideal_for');
$configurationsOptions = Configure::read('Configurations.options');
$placesOptions = Configure::read('Places.options');
$documentsImagesGroups = Configure::read('Documents.Images.groups');
$documentsDocumentsGroups = Configure::read('Documents.Documents.groups');
?>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title muted"><?php echo $place['Place']['name']; ?></h3>
<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-xs-12">
		<a class="btn btn-lg blue hidden-print pull-right" onclick="javascript:window.print();">
		<?php echo __('Print'); ?> <i class="fa fa-print"></i>
		</a>
	</div>
</div>
<div class="portlet light">
	<div class="portlet-body">
		<div class="invoice">
			<div class="row">
				<div class="col-md-4">
					<p><i class="fa fa-map-marker"></i> <?php echo $place['Place']['address']; ?>, <?php echo $place['Place']['zip_city']; ?></p>
					<?php if(!empty($place['Place']['contact_person_name'])): ?>
					<p>
						<i class="fa fa-phone"></i> 
						<?php echo $place['Place']['contact_person_name']; ?>
						<?php echo !empty($place['Place']['contact_person_phone1']) ? $place['Place']['contact_person_phone1'] : ''; ?>
					</p>
					<?php endif; ?>
					<?php if(!empty($place['Place']['contact_person_name'])): ?>
					<p><i class="fa fa-globe"></i> <?php echo $place['Place']['website']; ?></p>
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<?php if(!empty($place['Place']['type'])): ?>
					<p>
						<i class="fa fa-home"></i> <?php echo $placesTypes[$place['Place']['type']]; ?>
					</p>
					<?php endif; ?>
					<?php if(!empty($place['Place']['degree'])): ?>
					<p>
						<i class="fa fa-star"></i> <?php echo $placesDegrees[$place['Place']['degree']]; ?>
					</p>
					<?php endif; ?>
				</div>
				<div class="col-md-4">
					<p>
						<i class="fa fa-user"></i> <?php echo __('Created by %s on %s', array($place['User']['full_name'], $this->Time->format('d.m.Y h:s', strtotime($place['Place']['created'])))); ?>
					</p>
					<p>
						<i class="fa fa-user"></i> <?php echo __('Last modification made by %s on %s', array($place['LastModifiedBy']['full_name'], $this->Time->format('d.m.Y h:s', strtotime($place['Place']['modified'])))); ?>
					</p>
				</div>
			</div>
			<hr/>
			<div class="row">
				<div class="col-md-12">
					<h4><?php echo __('Common data'); ?></h4>
					<table class="table table-striped table-bordered">
							<tr>
								<td><strong><?php echo __('Name'); ?></strong></td>
								<td><?php echo $place['Place']['name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Type'); ?></strong></td>
								<td><?php echo $placesTypes[$place['Place']['type']]; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Description'); ?></strong></td>
								<td><?php echo $place['Place']['description']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Website'); ?></strong></td>
								<td>
									<a href="<?php echo $place['Place']['website']; ?>" target="_blank">
										<?php echo __('Visit website'); ?>
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Geographical data'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Address'); ?></strong></td>
								<td><?php echo $place['Place']['address']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('ZIP / City'); ?></strong></td>
								<td><?php echo $place['Place']['zip_city']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Latitude'); ?></strong></td>
								<td><?php echo $place['Place']['latitude']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Longitude'); ?></strong></td>
								<td><?php echo $place['Place']['longitude']; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Administrative data'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Negociated prices'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['negociated_prices']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Conditions'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['conditions']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Unavailabilities'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['unavailabilities']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Miscellaneous'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['administrative_misc']); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Contact person'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Name'); ?></strong></td>
								<td><?php echo $place['Place']['contact_person_name']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Phone 1'); ?></strong></td>
								<td><?php echo $place['Place']['contact_person_phone1']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Phone 2'); ?></strong></td>
								<td><?php echo $place['Place']['contact_person_phone2']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Email'); ?></strong></td>
								<td><?php echo $place['Place']['contact_person_email']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Office hours'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['office_hours']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact preference pre-reservation'); ?></strong></td>
								<td><?php echo $place['Place']['contact_preference_prereservation']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Contact preference reservation'); ?></strong></td>
								<td><?php echo $place['Place']['contact_preference_reservation']; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Accessibility'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Parking'); ?></strong></td>
								<td>
									<?php echo ($place['Place']['parking'] == 1) ? __('Yes') : __('No'); ?>
									<?php echo $place['Place']['parking_infos'] ? '<br>'.nl2br($place['Place']['parking_infos']) : ''; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Public transports'); ?></strong></td>
								<td>
									<?php echo ($place['Place']['public_transports'] == 1) ? __('Yes') : __('No'); ?>
									<?php echo $place['Place']['public_transports_infos'] ? '<br>'.nl2br($place['Place']['public_transports_infos']) : ''; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Reduced mobility'); ?></strong></td>
								<td>
									<?php echo ($place['Place']['reduced_mobility'] == 1) ? __('Yes') : __('No'); ?>
									<?php echo $place['Place']['reduced_mobility'] ? '<br>'.nl2br($place['Place']['reduced_mobility_infos']) : ''; ?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Sleep in'); ?></strong></td>
								<td>
									<?php echo ($place['Place']['sleep'] == 1) ? __('Yes') : __('No'); ?>
									<?php echo $place['Place']['sleep'] ? '<br>'.nl2br($place['Place']['sleep_infos']) : ''; ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Technical data'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Maximum number of persons'); ?></strong></td>
								<td><?php echo $place['Place']['max_number_of_persons']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Maximum surface'); ?></strong></td>
								<td><?php echo $place['Place']['max_surface']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Number of rooms'); ?></strong></td>
								<td><?php echo $place['Place']['number_of_rooms']; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Ideal for'); ?></strong></td>
								<td>
									<ul class="list-unstyled">
									<?php foreach($place['IdealFor'] as $item): ?>
										<li><i class="fa fa-check font-green"></i> <?php echo __($item['value']); ?></li>
									<?php endforeach; ?>
									</ul>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Remarks'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['ideal_for_infos']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Potential'); ?></strong></td>
								<td><?php echo $placesDegrees[$place['Place']['degree']]; ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('To check'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['to_check']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Internal reference'); ?></strong></td>
								<td><?php echo $place['Referer']['first_name']; ?> <?php echo $place['Referer']['last_name']; ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Available resources'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Equipment'); ?></strong></td>
								<td>
									<ul class="list-unstyled">
										<?php foreach($place['Material'] as $material): ?>
											<li><i class="fa fa-check font-green"></i> <?php echo $material['value']; ?></li>
										<?php endforeach; ?>
									</ul>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Technical'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['technical']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Kitchen incl. dishes'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['dishes']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Furniture'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['furniture']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Miscellaneous'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['miscellaneous_infos']); ?></td>
							</tr>
							<tr>
								<td><strong><?php echo __('Remarks'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['available_resources_infos']); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Food and beverage'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Caterer'); ?></strong></td>
								<td>
									<?php
									if($place['Place']['internal_caterer'] == 1 && $place['Place']['external_caterer'] == 1){
										echo __('Internal and external');
									} elseif($place['Place']['internal_caterer'] == 1 && $place['Place']['external_caterer'] == 0){
										echo __('Internal');
									} elseif($place['Place']['internal_caterer'] == 0 && $place['Place']['external_caterer'] == 1){
										echo __('External');
									} else {
										echo __('No');
									}
									if(!empty($place['Place']['caterer_constraints'])){
										echo '<br>'.nl2br($place['Place']['caterer_constraints']);
									}
									?>
								</td>
							</tr>
							<tr>
								<td><strong><?php echo __('Conditions'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['wine']); ?></td>
							</tr>							
						</tbody>
					</table>
				</div>
				<div class="col-md-12">
					<h4><?php echo __('Specifications'); ?></h4>
					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><strong><?php echo __('Logistics'); ?></strong></td>
								<td><?php echo nl2br($place['Place']['logistics']); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/dropzone/css/dropzone.css');
echo $this->Html->css('/metronic/global/plugins/fancybox/source/jquery.fancybox.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
echo $this->Html->css('/metronic/pages/css/portfolio.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css');
echo $this->Html->css('/metronic/pages/css/invoice.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/dropzone/dropzone.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modal.js');

$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.init();';
$this->end();
?>
