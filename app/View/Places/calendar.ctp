<?php $this->assign('page_title', __('Places')); ?>
<?php $this->assign('page_subtitle', __('Calendar')); ?>

<div class="portlet light">
  <div class="portlet-body">
		<div class="row hidden">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="uppercase bold"><?php echo __('Filters'); ?></span>
						</div>
						<div class="tools">
							<a href="" class="collapse" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<?php echo $this->Form->create('PlaceFilters'); ?>
						<div class="row">
							<div class="col-md-3">
								<?php echo $this->Form->input('Company', array('label' => false, 'options' => $companies, 'class' => 'form-control bs-select', 'empty' => __('Select a company'))); ?>
							</div>
							<div class="col-md-3">
								<?php echo $this->Form->input('Place', array('label' => false, 'options' => $companies, 'class' => 'form-control bs-select', 'empty' => __('Select a company'))); ?>
							</div>
						</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div>
    <div class="row">
			<div class="col-md-12">
				<div class="portlet light calendar">
					<div class="portlet-title">
						<div class="row">
							<div class="col-m-9" style="margin-bottom: 10px">
								<div class="row">
									<div class="col-md-4">
										<div class="calendar-badges">
											<span class="badge badge-roundless badge--confirmed">&nbsp;&nbsp;</span> <?php echo __('Confirmed'); ?>
											<span class="badge badge-roundless badge--prereserved">&nbsp;&nbsp;</span> <?php echo __('Prereserved / Option'); ?>
											<span class="badge badge-roundless badge--cancelled">&nbsp;&nbsp;</span> <?php echo __('Cancelled'); ?>
										</div>
										<div class="calendar-badges">
											<span class="badge badge-roundless badge--occupied">&nbsp;&nbsp;</span> <?php echo __('Occupied'); ?>
											<span class="badge badge-roundless badge--pending">&nbsp;&nbsp;</span> <?php echo __('Pending'); ?>
											<span class="badge badge-roundless badge--undefined">&nbsp;&nbsp;</span> <?php echo __('Undefined'); ?>
										</div>
									</div>
									<div class="col-md-3 filters">
										<label for="" class="control-label"><?php echo __('Filter by option(s)'); ?></label>
										<?php echo $this->Form->input('option', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'multiple' => true, 'options' => Configure::read('Places.options_for_calendar'), 'data-field' => 'option')); ?>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div id="placesCalendar"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
    </div>
  </div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.places();';
$this->end();
?>
