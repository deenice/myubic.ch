<?php
$placesTypes = Configure::read('Places.types');
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'places', 'action' => 'view', $result['Place']['id']),
    array('class' => 'btn default btn-xs', 'escape' => false)
	);
	$actions .= $this->Html->tag('');
	$actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'places', 'action' => 'edit', $result['Place']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
  );

	if(!empty($result['Note'])){
		$actions .= $this->Html->link('<i class="fa fa-files-o"></i> ' . __("Notes (%s)", sizeof($result['Note'])),
	    array('controller' => 'notes', 'action' => 'modal', 'list', 'Place', $result['Place']['id']),
	    array('class' => 'btn default btn-xs', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal-note-list')
	  );
	} else {
    $actions .= $this->Html->link('<i class="fa fa-sticky-note-o"></i> ' . __("Add a note"),
	    '#',
	    array('class' => 'btn default btn-xs', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#new-note', 'data-model' => 'place', 'data-model-id' => $result['Place']['id'])
	  );
  }

	$lastModification = $this->Time->format('d.m.Y H:i', strtotime($result['Place']['modified']));
	$lastModification .= ' ' . __('by') . ' ';
	$lastModification .= $result['LastModifiedBy']['first_name'] . ' ' . $result['LastModifiedBy']['last_name'];

  $this->dtResponse['aaData'][] = array(
    $result['Place']['checked'] ? $this->Html->tag('i', '', array('class' => 'fa fa-check-circle font-green')) : $this->Html->tag('i', '', array('class' => 'fa fa-circle-thin')),
    $this->Html->link($result['Place']['name'], array('controller' => 'places', 'action' => 'view', $result['Place']['id'])),
    $result['Place']['zip_city'],
    __($placesTypes[$result['Place']['type']]),
    $lastModification,
    $actions
  );
}
