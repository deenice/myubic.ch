<?php $this->assign('page_title', __('Search places')); ?>
<div class="row">
	<div class="col-md-12">
		<div class="booking-search">
			<?php echo $this->Form->create('Search', array('class' => 'form')); ?>
				<div class="row form-group">
					<div class="col-md-6">
						<label class="control-label"><?php echo __('Commune'); ?></label>
						<?php echo $this->Form->input('ZipCity', array('label' => false, 'class' => 'form-control zip-city', 'data-default' => $this->request->data['Search']['zip'] . ' ' . $this->request->data['Search']['city']));?>
						<?php echo $this->Form->input('city', array('type' => 'hidden', 'class' => 'city'));?>
						<?php echo $this->Form->input('zip', array('type' => 'hidden', 'class' => 'zip'));?>
					</div>
					<div class="col-md-6">
						<label class="control-label"><?php echo __('Client'); ?></label>
						<?php echo $this->Form->input('client', array('label' => false, 'class' => 'form-control', 'options' => $clients)); ?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<label class="control-label"><?php echo __('Number of persons'); ?></label>
						<div>
							<span class="help-inline">From</span>
							<?php echo $this->Form->input('min_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
							<span class="help-inline">to</span>
							<?php echo $this->Form->input('.max_number_of_persons', array('label' => false, 'div' => false, 'class' => 'form-control input-small input-inline'));?>
						</div>
					</div>
					<div class="col-md-6">
						<label class="control-label"><?php echo __('Event date'); ?></label>
						<?php echo $this->Form->input('event_date', array('type' => 'text', 'label' => false, 'class' => 'form-control form-control-inline input-medium date-picker', 'data-size' => 16, 'data-date-format' => 'yyyy-mm-dd'));?>
					</div>
				</div>
				<div class="row form-group">
					<div class="col-md-6">
						<label class="control-list"><?php echo __('Period'); ?></label>
						<div class="checkbox-list">
							<label class="checkbox-inline">
							<input type="checkbox" name="optionsRadios2" value="option1"/>
							<?php echo __('Morning/Afternoon'); ?></label>
							<label class="checkbox-inline">
							<input type="checkbox" name="optionsRadios2" value="option2" checked/>
							<?php echo __('Afternoon/Evening'); ?></label>
						</div>
					</div>
					<div class="col-md-6">
						<label class="control-label">Persons</label>
						<div class="input-icon">
							<i class="fa fa-user"></i>
							<input class="form-control" size="16" type="text" placeholder="1 Room, 2 Adults, 0 Children"/>
						</div>
					</div>
				</div>
				<div class="panel-group accordion" id="accordion1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
							<?php echo __('Advanced criteria'); ?></a>
							</h4>
						</div>
						<div id="collapse_1" class="panel-collapse collapse">
							<div class="panel-body">
								<?php echo $this->Form->input('parking'); ?>
								<?php echo $this->Form->input('reduced_mobility'); ?>
								<?php echo $this->Form->input('misc'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="text-center">
					<button class="btn blue margin-top-20">SEARCH <i class="m-icon-swapright m-icon-white"></i></button>					
				</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>
<?php if(isset($configurations)): //debug($configurations);?>
	<h3><?php echo __('Search results'); ?></h3>
	<?php foreach ($configurations as $key => $item): //debug($item)?>
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-map-marker"></i><?php echo $item['Place']['name']; ?>
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse" data-original-title="" title="">
					</a>
					<a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
					</a>
					<a href="javascript:;" class="reload" data-original-title="" title="">
					</a>
					<a href="javascript:;" class="remove" data-original-title="" title="">
					</a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-2">
						<?php echo $this->Html->image('place_pictures/' . $item['Place']['picture'], array('class' => 'img-responsive')); ?>
					</div>
					<div class="col-md-4">
						<p><i class="fa fa-map-marker"></i> <?php echo $item['Place']['zip_city']; ?> - <?php echo $item['Place']['distance']; ?> km</p>
						<?php if(isset($item['Configuration']['name'])): ?>
							<strong><?php echo $item['Configuration']['name']; ?></strong>
							<p>
								<?php echo $item['Configuration']['surface']; ?>
								<?php echo $item['Configuration']['volume']; ?>
								From <?php echo $item['Configuration']['min_number_of_persons']; ?> to <?php echo $item['Configuration']['max_number_of_persons']; ?> persons
							</p>
						<?php endif; ?>
					</div>
					<div class="col-md-3">
						<p>
							<i class="fa fa-user"></i> <?php echo $item['Place']['contact_person_name']; ?><br />
							<i class="fa fa-phone"></i> <?php echo $item['Place']['contact_person_phone']; ?><br />
							<i class="fa fa-envelope-o"></i> <?php echo $item['Place']['contact_person_email']; ?><br />
						</p>
					</div>
					<div class="col-md-3">
						<p>
							<i class="fa fa-user"></i> <?php echo $item['Place']['contact_person_name']; ?><br />
							<i class="fa fa-phone"></i> <?php echo $item['Place']['contact_person_phone']; ?><br />
							<i class="fa fa-envelope-o"></i> <?php echo $item['Place']['contact_person_email']; ?><br />
						</p>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>	
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/dropzone/css/dropzone.css');
echo $this->Html->css('/metronic/global/plugins/fancybox/source/jquery.fancybox.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-modal/css/bootstrap-modal.css');
echo $this->Html->css('/metronic/pages/css/portfolio.css');
echo $this->Html->css('/metronic/pages/css/search.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/dropzone/dropzone.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-modal/js/bootstrap-modal.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
echo $this->Html->script('/metronic/pages/scripts/form-dropzone.js');
echo $this->Html->script('/metronic/pages/scripts/portfolio.js');
echo $this->Html->script('/metronic/scripts/custom.js');
$this->end();
$this->start('init_scripts');
//echo 'ComponentsPickers.init();';
//echo 'ComponentsDropdowns.init();';
echo 'Custom.init();';
$this->end();
?>
