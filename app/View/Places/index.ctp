<?php $this->assign('page_title', $this->Html->link(__('Places'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', __('List')); ?>
<?php $types = Configure::read('Places.types'); ?>
<div class="tabbable-custom tabbable-tabdrop" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#allPlaces" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('All'); ?></a>
		</li>
		<li>
			<a href="#incompletePlaces" data-toggle="tab"><i class="fa fa-question"></i> <?php echo __('Incomplete'); ?></a>
		</li>
		<li>
			<a href="#toBeCheckedPlaces" data-toggle="tab"><i class="fa fa-support"></i> <?php echo __('To be checked'); ?></a>
		</li>
		<li>
			<a href="#privatePlaces" data-toggle="tab"><i class="fa fa-lock"></i> <?php echo __('Private places'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="allPlaces">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a place') . ' <i class="fa fa-plus"></i>', array('controller' => 'places', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Places', array(), array(array(1 => 'asc'))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="incompletePlaces">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a place') . ' <i class="fa fa-plus"></i>', array('controller' => 'places', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('IncompletePlaces', array(), array(array(1 => 'asc'))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="toBeCheckedPlaces">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a place') . ' <i class="fa fa-plus"></i>', array('controller' => 'places', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('ToBeCheckedPlaces', array(), array(array(1 => 'asc'))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="privatePlaces">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a place') . ' <i class="fa fa-plus"></i>', array('controller' => 'places', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('PrivatePlaces', array(), array(array(1 => 'asc'))); ?>
				</div>
			</div>
		</div>
	</div>
</div>
