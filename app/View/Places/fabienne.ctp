<?php foreach($places as $placeId => $data): $k = 1; ?>
	<h3><?php echo $this->Html->link($data['name'], array('controller' => 'places', 'action' => 'edit', $placeId), array('target' => '_blank')); ?></h3>
	<div class="row">
		<?php foreach($data['images'] as $img): ?>
		<div class="col-md-4 col-xs-4">
			<?php echo $this->Html->image(DS . $img, array('class' => 'img-responsive margin-bottom-5')); ?>
		</div>
		<?php if( $k%3 == 0): ?><div class="clearfix"></div><?php endif; ?>
		<?php $k++; endforeach; ?>
	</div>	
<?php endforeach; ?>