<?php
$this->assign('page_title', __('Places'));
$this->assign('page_subtitle', $place['Place']['name'] . ' <small>'.$place['Place']['zip_city'].'</small>');
$placesTypes = Configure::read('Places.types');
$placesDegrees = Configure::read('Places.degrees');
$placesIdealFor = Configure::read('Places.ideal_for');
$configurationsOptions = Configure::read('Configurations.options');
$placesOptions = Configure::read('Places.options');
$documentsImagesGroups = Configure::read('Documents.Images.groups');
$documentsDocumentsGroups = Configure::read('Documents.Documents.groups');
?>
<div class="row profile">
	<div class="col-md-12">
		<!--BEGIN TABS-->
		<div class="tabbable tabbable-custom tabbable-bordered nav-justified">
			<ul class="nav nav-tabs nav-justified">
				<li class="active">
					<a href="#overview" data-toggle="tab"><?php echo __('Overview'); ?></a>
				</li>
				<li>
					<a href="#agenda" data-toggle="tab"><?php echo __('Agenda'); ?></a>
				</li>
				<li>
					<a href="#photos" data-toggle="tab"><?php echo __('Photos'); ?></a>
				</li>
				<li>
					<a href="#documents" data-toggle="tab"><?php echo __('Documents'); ?></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="overview">
					<div class="row">
						<div class="col-md-3">
							<ul class="list-unstyled profile-nav">
								<li>
									<?php if(!empty($thumbnail)): //debug($thumbnail) ?>
									<?php echo $this->Html->image(DS . $thumbnail[0]['Document']['url'], array('class' => 'img-responsive img-thumbnail')); ?>
									<?php else: ?>
									<img src="http://www.placehold.it/400x300/EFEFEF/AAAAAA&amp;text=no+image" alt="" class="img-responsive" />
									<?php endif; ?>
								</li>
								<li>
									<div id="gmap_marker" class="gmaps" style="margin-top:20px" data-lat="<?php echo $place['Place']['latitude']; ?>"  data-lng="<?php echo $place['Place']['longitude']; ?>" data-title="<?php echo $place['Place']['name']; ?>"></div>
								</li>
								<li>
									<a href="https://www.google.ch/maps/place/<?php echo urlencode($place['Place']['address'].','.$place['Place']['zip_city']); ?>" class="btn btn-primary btn-block margin-top-10" target="_blank"><i class="fa fa-map-marker"></i> <?php echo __('Open in Google Maps'); ?></a>
								</li>
							</ul>
						</div>
						<div class="col-md-9">
							<div class="row">
								<div class="col-md-12 profile-info">
									<h1>
										<?php echo $place['Place']['name']; ?>
										<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('controller' => 'places', 'action' => 'edit', $place['Place']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
									</h1>
									<p><?php echo !empty($place['Place']['descrition']) ? $place['Place']['descrition'] : ''; ?></p>
									<div class="row" style="padding: 20px 0">
										<div class="col-md-4">
											<ul class="list-unstyled">
												<?php if(!empty($place['Place']['address'])): ?>
												<li>
													<i class="fa fa-map-marker"></i> <?php echo $place['Place']['address']; ?>, <?php echo $place['Place']['zip_city']; ?>
												</li>
												<?php endif; ?>
												<?php if(!empty($place['Place']['contact_person_name'])): ?>
												<li>
													<i class="fa fa-phone"></i>
													<?php echo $place['Place']['contact_person_name']; ?>
													<?php echo !empty($place['Place']['contact_person_phone1']) ? '<a href="tel:'.$place['Place']['contact_person_phone1'].'">'.$place['Place']['contact_person_phone1'].'</a>' : ''; ?>
												</li>
												<?php endif; ?>
												<?php if(!empty($place['Place']['website'])): ?>
												<li>
													<i class="fa fa-globe"></i> <a href="<?php echo $place['Place']['website']; ?>" target="_blank">
														<?php echo __('Visit website'); ?>
													</a>
												</li>
												<?php endif; ?>
											</ul>
										</div>
										<div class="col-md-2">
											<ul class="list-unstyled">
												<?php if(!empty($place['Place']['type'])): ?>
												<li>
													<i class="fa fa-home"></i> <?php echo $placesTypes[$place['Place']['type']]; ?>
												</li>
												<?php endif; ?>
												<?php if(!empty($place['Place']['degree'])): ?>
												<li>
													<i class="fa fa-star"></i> <?php echo $placesDegrees[$place['Place']['degree']]; ?>
												</li>
												<?php endif; ?>
											</ul>
										</div>
										<div class="col-md-6">
											<ul class="list-unstyled">
												<li>
													<i class="fa fa-user"></i> <?php echo __('Created by %s on %s', array($place['User']['full_name'], $this->Time->format('d.m.Y h:s', strtotime($place['Place']['created'])))); ?>
												</li>
												<li>
													<i class="fa fa-user"></i> <?php echo __('Last modification made by %s on %s', array($place['LastModifiedBy']['full_name'], $this->Time->format('d.m.Y h:s', strtotime($place['Place']['modified'])))); ?>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!--end col-md-8-->
							</div>
							<!--end row-->
							<div class="tabbable tabbable-custom">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#data" data-toggle="tab"><?php echo __('Informations'); ?></a>
									</li>
									<li class="">
										<a href="#configurations" data-toggle="tab"><?php echo __('Configurations'); ?></a>
									</li>
									<?php if(!empty($place['Activity'])): ?>
									<li>
										<a href="#activities" data-toggle="tab"><?php echo __('Activities'); ?></a>
									</li>
									<?php endif; ?>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="data">
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
												<tbody>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Common data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Name'); ?></strong></td>
														<td><?php echo $place['Place']['name']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Type'); ?></strong></td>
														<td><?php echo $placesTypes[$place['Place']['type']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Description'); ?></strong></td>
														<td><?php echo $place['Place']['description']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Website'); ?></strong></td>
														<td>
															<a href="<?php echo $place['Place']['website']; ?>" target="_blank">
																<?php echo __('Visit website'); ?>
															</a>
														</td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Geographical data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Address'); ?></strong></td>
														<td><?php echo $place['Place']['address']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('ZIP / City'); ?></strong></td>
														<td><?php echo $place['Place']['zip_city']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Latitude'); ?></strong></td>
														<td><?php echo $place['Place']['latitude']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Longitude'); ?></strong></td>
														<td><?php echo $place['Place']['longitude']; ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Administrative data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Negociated prices'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['negociated_prices']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Conditions'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['conditions']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Unavailabilities'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['unavailabilities']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Miscellaneous'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['administrative_misc']); ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Contact person'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Name'); ?></strong></td>
														<td><?php echo $place['Place']['contact_person_name']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Phone 1'); ?></strong></td>
														<td><?php echo $place['Place']['contact_person_phone1']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Phone 2'); ?></strong></td>
														<td><?php echo $place['Place']['contact_person_phone2']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Email'); ?></strong></td>
														<td><?php echo $place['Place']['contact_person_email']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Office hours'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['office_hours']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Contact preference pre-reservation'); ?></strong></td>
														<td><?php echo $place['Place']['contact_preference_prereservation']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Contact preference reservation'); ?></strong></td>
														<td><?php echo $place['Place']['contact_preference_reservation']; ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Accessibility'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Parking'); ?></strong></td>
														<td>
															<?php echo ($place['Place']['parking'] == 1) ? __('Yes') : __('No'); ?>
															<?php echo $place['Place']['parking_infos'] ? '<br>'.nl2br($place['Place']['parking_infos']) : ''; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Public transports'); ?></strong></td>
														<td>
															<?php echo ($place['Place']['public_transports'] == 1) ? __('Yes') : __('No'); ?>
															<?php echo $place['Place']['public_transports_infos'] ? '<br>'.nl2br($place['Place']['public_transports_infos']) : ''; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Reduced mobility'); ?></strong></td>
														<td>
															<?php echo ($place['Place']['reduced_mobility'] == 1) ? __('Yes') : __('No'); ?>
															<?php echo $place['Place']['reduced_mobility'] ? '<br>'.nl2br($place['Place']['reduced_mobility_infos']) : ''; ?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Sleep in'); ?></strong></td>
														<td>
															<?php echo ($place['Place']['sleep'] == 1) ? __('Yes') : __('No'); ?>
															<?php echo $place['Place']['sleep'] ? '<br>'.nl2br($place['Place']['sleep_infos']) : ''; ?>
														</td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Available resources'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Equipment'); ?></strong></td>
														<td>
															<ul class="list-unstyled">
																<?php foreach($place['Material'] as $material): ?>
																	<li><i class="fa fa-check font-green"></i> <?php echo $material['value']; ?></li>
																<?php endforeach; ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Technical'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['technical']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Kitchen incl. dishes'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['dishes']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Furniture'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['furniture']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Miscellaneous'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['miscellaneous_infos']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Remarks'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['available_resources_infos']); ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Food and beverage'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Caterer'); ?></strong></td>
														<td>
															<?php
															if($place['Place']['internal_caterer'] == 1 && $place['Place']['external_caterer'] == 1){
																echo __('Internal and external');
															} elseif($place['Place']['internal_caterer'] == 1 && $place['Place']['external_caterer'] == 0){
																echo __('Internal');
															} elseif($place['Place']['internal_caterer'] == 0 && $place['Place']['external_caterer'] == 1){
																echo __('External');
															} else {
																echo __('No');
															}
															if(!empty($place['Place']['caterer_constraints'])){
																echo '<br>'.nl2br($place['Place']['caterer_constraints']);
															}
															?>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Conditions'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['wine']); ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Specifications'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Logistics'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['logistics']); ?></td>
													</tr>
													<tr>
														<td colspan="2" class="bg-grey-steel"><?php echo __('Technical data'); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Maximum number of persons'); ?></strong></td>
														<td><?php echo $place['Place']['max_number_of_persons']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Maximum surface'); ?></strong></td>
														<td><?php echo $place['Place']['max_surface']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Number of rooms'); ?></strong></td>
														<td><?php echo $place['Place']['number_of_rooms']; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Ideal for'); ?></strong></td>
														<td>
															<ul class="list-unstyled">
															<?php foreach($place['IdealFor'] as $item): ?>
																<li><i class="fa fa-check font-green"></i> <?php echo __($item['value']); ?></li>
															<?php endforeach; ?>
															</ul>
														</td>
													</tr>
													<tr>
														<td><strong><?php echo __('Remarks'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['ideal_for_infos']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Potential'); ?></strong></td>
														<td><?php echo $placesDegrees[$place['Place']['degree']]; ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('To check'); ?></strong></td>
														<td><?php echo nl2br($place['Place']['to_check']); ?></td>
													</tr>
													<tr>
														<td><strong><?php echo __('Internal reference'); ?></strong></td>
														<td><?php echo $place['Referer']['first_name']; ?> <?php echo $place['Referer']['last_name']; ?></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="configurations">
										<div class="portlet-body">
											<table class="table table-striped table-bordered">
											<thead>
											<tr>
												<th>Name</th>
												<th>Number of persons</th>
												<th>Surface</th>
												<th>Volume</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach($place['Configuration'] as $config): //debug($config)?>
											<tr>
												<td><strong><?php echo $config['name']; ?></strong></td>
												<td>
													<?php echo __('From %s to %s persons', $config['min_number_of_persons'], $config['max_number_of_persons']); ?>
												</td>
												<td>
													<?php if(!empty($config['surface'])): ?>
													<?php echo $config['surface']; ?> m2
													<?php endif; ?>
												</td>
												<td>
													<?php if(!empty($config['volume'])): ?>
													<?php echo $config['volume']; ?> m3
													<?php endif; ?>
												</td>
											</tr>
											<?php endforeach; ?>
											</tbody>
											</table>
										</div>
									</div>
									<div class="tab-pane" id="activities">
									<?php if(!empty($place['Activity'])): ?>
										<div class="row">
											<?php foreach($place['Activity'] as $activity): ?>
												<div class="col-md-6">
													<div class="panel panel-default">
														<div class="panel-heading">
															<h3 class="panel-title"><?php echo $activity['name']; ?></h3>
														</div>
														<div class="panel-body">
															 <ul class="list-unstyled">
															<li><?php echo nl2br($activity['price']); ?></li>
															<li><?php echo nl2br($activity['duration']); ?></li>
														</ul>
														<?php echo $activity['description']; ?>
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
									</div>
										<!--tab-pane-->
									<!--tab-pane-->
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--tab_1_2-->
				<div class="tab-pane" id="agenda">
					<div class="portlet light">
						<div class="portlet-title">
							<div class="col-md-12">
								<div class="text-center">
									<ul class="pagination pagination-sm">
										<li>
											<?php echo $this->Html->link(sprintf('<i class="fa fa-angle-left"></i> %s', $year - 1), array('controller' => 'places', 'action' => 'view', $place['Place']['id'], 'year' => $year - 1), array('escape' => false)); ?>
										</li>
										<li class="active">
											<?php echo $this->Html->link($year, array('controller' => 'places', 'action' => 'view', $place['Place']['id'], 'year' => $year)); ?>
										</li>
										<li>
											<?php echo $this->Html->link(sprintf('%s <i class="fa fa-angle-right"></i>', $year + 1), array('controller' => 'places', 'action' => 'view', $place['Place']['id'], 'year' => $year + 1), array('escape' => false)); ?>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">
									<?php foreach($months as $key => $month): ?>
									<?php if(empty($options[$key])){continue;} ?>
									<h4><?php echo __($month); ?></h4>
									<table class="table table-striped table-place-history">
										<thead>
											<tr>
												<th class="date"><?php echo __('Date'); ?></th>
												<th class="reason"><?php echo __('Reason'); ?></th>
												<th class="status"><?php echo __('Status'); ?></th>
											</tr>
										</thead>
										<tbody>
											<?php if(!empty($options[$key])): ?>
											<?php foreach($options[$key] as $option): ?>
												<tr>
													<td><?php echo $this->Time->format('d.m.Y', $option['Event']['confirmed_date']); ?></td>
													<td><?php echo $this->Html->link($option['Event']['code_name'], array('controller' => 'events', 'action' => 'work', $option['Event']['id'], 'target' => '_blank')); ?></td>
													<td><?php echo empty($option['option']) ? '' : $placesOptions[$option['option']]; ?></td>
												</tr>
											<?php endforeach; ?>
											<?php endif; ?>
										</tbody>
									</table>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end tab-pane-->
				<div class="tab-pane" id="photos">
					<?php if(!empty($documents['photos'])): ?>
					<div class="row mix-grid">
						<div class="col-md-12">
							<?php foreach($documents['photos'] as $groupName => $group): ?>
								<h3><?php echo strtoupper($documentsImagesGroups[$groupName]); ?></h3>
									<?php foreach($group as $k => $doc): ?>
									<?php $title = $doc['Document']['name']; ?>
									<div class="col-md-3 col-sm-4 mix" data-rel="internal<?php echo $k; ?>">
										<div class="mix-inner">
											<?php echo $this->Html->link(
										    $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')),
										    DS . $doc['Document']['url'],
										    array('data-rel' => 'fancybox-button', 'escape' => false, 'class' => 'fancybox-button')
										); ?>
										</div>
									</div>
								<?php endforeach; ?>
								<div class="clearfix"></div>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
					<!--end row-->
				</div>
				<!--end tab-pane-->
				<div class="tab-pane" id="clients-photos">
					<?php if(!empty($documents['client-photo'])): ?>
					<div class="row mix-grid">
						<div class="col-md-12">
							<?php foreach($documents['client-photo'] as $groupName => $group): ?>
								<h3><?php echo strtoupper($documentsImagesGroups[$groupName]); ?></h3>
									<?php foreach($group as $k => $doc): ?>
									<?php $title = $doc['Document']['name']; ?>
									<div class="col-md-3 col-sm-4 mix" data-rel="internal<?php echo $k; ?>">
										<div class="mix-inner">
											<?php echo $this->Html->link(
										    $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')),
										    DS . $doc['Document']['url'],
										    array('data-rel' => 'fancybox-button', 'escape' => false, 'class' => 'fancybox-button')
										); ?>
										</div>
									</div>
								<?php endforeach; ?>
								<div class="clearfix"></div>
							<?php endforeach; ?>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div class="tab-pane" id="documents">
					<?php if(!empty($documents['documents'])): ?>
					<div class="portlet-body">
						<table class="table table-striped table-bordered table-advance table-hover">
							<tbody>
							<?php foreach($documents['documents'] as $groupName => $group): ?>
								<tr>
									<td colspan="2"><strong><?php echo strtoupper($documentsDocumentsGroups[$groupName]); ?></strong></td>
								</tr>
								<?php foreach($group as $k => $doc): ?>
								<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
								<tr>
									<td>
									<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('width' => '24px')); ?>
									<?php echo $this->Html->link($doc['Document']['name'], DS . $doc['Document']['url'], array('target' => '_blank')); ?></td>
									<td>
										<?php echo $this->Html->link(__('Download'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
										<?php echo $this->Html->link(__('Share'), '#', array('class' => 'btn btn-primary btn-sm')); ?>
									</td>
								</tr>
							<?php endforeach; ?>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
				</div>
				<!--end tab-pane-->
			</div>
		</div>
		<!--END TABS-->
	</div>
</div>
<!-- END PAGE CONTENT-->
<?php
$this->start('page_level_plugins');
echo $this->Html->script('http://maps.google.com/maps/api/js?sensor=false&language=fr');
echo $this->Html->script('/metronic/theme/assets/global/plugins/gmaps/gmaps.min.js');
$this->end();
$this->start('init_scripts');
echo 'Portfolio.init();';
echo 'Custom.gmap();';
echo 'Custom.place();';
$this->end();
?>
