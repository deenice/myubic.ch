<?php
$types = Configure::read('Regions.types');
foreach ($dtResults as $result) {

	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'regions', 'action' => 'edit', $result['Region']['id']),
	    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $this->dtResponse['aaData'][] = array(
      $result['Region']['name'],
      $types[$result['Region']['type']],
      $actions
  );
}
