<?php

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'f_b_modules', 'action' => 'edit', $result['FBModule']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $this->dtResponse['aaData'][] = array(
    $result['FBModule']['name'],
    $result['FBCategory']['name'],
    $actions
  );
}
