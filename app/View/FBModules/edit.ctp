<?php $this->assign('page_title', $this->Html->link(__('F&B Modules'), array('action' => 'index')));?>
<?php $this->assign('page_subtitle', $this->request->data['FBModule']['name']);?>

<div class="tabbable-custom" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#infos" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#photos" data-toggle="tab"><i class="fa fa-image"></i> <?php echo __('Photos') ?></a>
		</li>
		<li>
			<a href="#documents" data-toggle="tab"><i class="fa fa-files-o"></i> <?php echo __('Documents') ?></a>
		</li>
		<li>
			<a href="#planning" data-toggle="tab"><i class="fa fa-hourglass-o"></i> <?php echo __('Planning') ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="infos">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-cup font-blue-madison"></i>
						<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['FBModule']['name']; ?></span>
					</div>
				</div>
				<div class="portlet-body form">
					<?php echo $this->Form->create('FBModule', array('class' => 'form-horizontal')); ?>
					<?php echo $this->Form->input('FBModule.id'); ?>
					<!-- BEGIN FORM-->
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Acronym'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('slug', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('fb_category_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => __('Select a category'), 'options' => $fb_categories)); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Execution'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('execution', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Sales informations'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('sales_infos', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Realisation informations'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('realisation_infos', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Buffet setting up'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('buffet_setting_up', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Room setting up'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('room_setting_up', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Back office setting up'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('backoffice_setting_up', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Practical informations'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('practical_infos', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('Other'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('misc', array('label' => false, 'class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3"><?php echo __('With competences?'); ?></label>
								<div class="col-md-9">
									<?php echo $this->Form->input('with_competences', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
								</div>
							</div>
						</div>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-offset-3 col-md-9">
									<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
									<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
									<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete')), array('controller' => 'f_b_modules', 'action' => 'delete', $this->request->data['FBModule']['id']), array('escape' => false, 'class' => 'btn btn-danger bootbox')); ?>
								</div>
							</div>
						</div>
					<!-- END FORM-->
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="photos">
			<?php if(!empty($documents['photos'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<h3><?php echo __('Existing photos'); ?></h3>
					<?php foreach($documents['photos'] as $k => $doc): ?>
						<?php $title = $doc['Document']['name']; ?>
						<div class="col-md-3 col-sm-4 mix" data-rel="internal<?php echo $k; ?>" data-id="<?php echo $doc['Document']['id']; ?>">
							<div class="mix-inner">
								<?php echo $this->Html->image(DS . $doc['Document']['url'], array('class' => 'img-responsive')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $title; ?>
										</h4>
										<small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-search"></i> ' . __('See'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['Document']['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#internal' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="internal<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit photo</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __('New photos'); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['FBModule']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'fb_module_photo')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'fb_modules')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="documents">
			<?php if(!empty($documents['documents'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<h3><?php echo __('Existing documents'); ?></h3>
					<?php foreach($documents['documents'] as $k => $doc): ?>
						<?php $title = $doc['Document']['name']; ?>
						<?php $ext = pathinfo($doc['Document']['url'], PATHINFO_EXTENSION); ?>
						<div class="col-md-2 col-sm-4 mix">
							<div class="mix-inner">
								<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title"><?php echo $title; ?></h4>
										<small><?php echo __('Added by %s', $doc['Owner']['full_name']); ?></small>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-eye"></i> ' . __('See'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button',
													'target' => '_blank'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['Document']['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['Document']['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#fbModule' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="fbModule<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit document</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['Document']['id'], 'value' => $doc['Document']['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['Document']['id'], 'label' => __('New name'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __('New documents'); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'fbmoduledocuments', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['FBModule']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'fb_module_document')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'fb_modules')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf, doc, docx, xls, xlsx, ppt, pptx')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="planning">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-calendar font-blue-madison"></i>
						<span class="caption-subject font-blue-madison bold uppercase">Planning</span>
					</div>
				</div>
				<div class="portlet-body form">
					<?php echo $this->Form->create('FBModule'); ?>
					<?php echo $this->Form->input('FBModule.id'); ?>
					<?php echo $this->Form->input('FBModule.name', array('type' => 'hidden')); ?>
						<table class="table table-striped table-fb-module-planning">
							<thead>
								<tr>
									<th class="duration"><?php echo __('Duration'); ?></th>
									<th class="name"><?php echo __('Name'); ?></th>
									<th class="remarks"><?php echo __('Remarks'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($this->request->data['PlanningBoardMoment'])): ?>
									<?php foreach($this->request->data['PlanningBoardMoment'] as $k => $moment): ?>
										<tr>
											<td>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', $k), array('value' => $this->Time->format('G:i', $moment['default_duration']), 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
												<span class="help-inline">min</span>
											</td>
											<td>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', $k), array('value' => $moment['name'], 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
											</td>
											<td>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', $k), array('type' => 'text', 'value' => $moment['remarks'], 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.id', $k), array('value' => $moment['id'], 'type' => 'hidden')); ?>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', $k), array('value' => $this->request->data['FBModule']['id'], 'type' => 'hidden')); ?>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', $k), array('value' => 'fb_module', 'type' => 'hidden')); ?>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', $k), array('value' => $k, 'type' => 'hidden')); ?>
											</td>
										</tr>
									<?php endforeach; ?>
									<tr>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', sizeof($this->request->data['PlanningBoardMoment'])), array('value' => '0:00', 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
											<span class="help-inline">min</span>
										</td>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', sizeof($this->request->data['PlanningBoardMoment'])), array('value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
										</td>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', sizeof($this->request->data['PlanningBoardMoment'])), array('type' => 'text', 'value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', sizeof($this->request->data['PlanningBoardMoment'])), array('value' => $this->request->data['FBModule']['id'], 'type' => 'hidden')); ?>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', sizeof($this->request->data['PlanningBoardMoment'])), array('value' => 'fb_module', 'type' => 'hidden')); ?>
												<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', sizeof($this->request->data['PlanningBoardMoment'])), array('value' => sizeof($this->request->data['PlanningBoardMoment']), 'type' => 'hidden')); ?>
										</td>
									</tr>
								<?php else: ?>
								<?php for($i=0;$i<3;$i++): ?>
									<tr>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_duration', $i), array('value' => '0:00', 'type' => 'text', 'class' => 'form-control input-inline timepicker-24', 'div' => false, 'label' => false)); ?>
											<span class="help-inline">min</span>
										</td>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.name', $i), array('value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
										</td>
										<td>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.remarks', $i), array('type' => 'text', 'value' => '', 'class' => 'form-control', 'div' => false, 'label' => false)); ?>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.default_id', $i), array('value' => $this->request->data['FBModule']['id'], 'type' => 'hidden')); ?>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.type', $i), array('value' => 'fb_module', 'type' => 'hidden')); ?>
											<?php echo $this->Form->input(sprintf('PlanningBoardMoment.%s.weight', $i), array('value' => $i, 'type' => 'hidden')); ?>
										</td>
									</tr>
								<?php endfor; ?>
								<?php endif; ?>
							</tbody>
						</table>
						<div class="form-actions">
							<div class="col-md-12">
								<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
								<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button> 
							</div>             
						</div>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
$this->start('init_scripts');
//echo 'Custom.fb_modules();';
$this->end();
?>
