<?php $this->assign('page_title', __('Vehicles')); ?>
<?php $this->assign('page_subtitle', __('Planification')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car"></i><?php echo __('Planification of the vehicles'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-angle-double-left"></i> ' . __('Previous week'), array('controller' => 'vehicles', 'action' => 'plan', $company_id, $previousWeek), array('class' => 'btn default', 'escape' => false)); ?>
			<?php echo $this->Html->link(__('Next week') . ' <i class="fa fa-angle-double-right"></i>', array('controller' => 'vehicles', 'action' => 'plan', $company_id, $nextWeek), array('class' => 'btn default', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php if(!empty($orders)): ?>
		<div class="row">
		<?php foreach($orders as $date => $tours): ksort($tours); ?>
				<div class="col-md-12">
					<h3 class="bold uppercase"><?php echo $this->Time->format($date, '%d %B %Y'); ?></h3>
				</div>
				<?php $counter = 0; foreach($tours as $tour => $orders): ksort($orders); ?>
				<?php
				if(empty($reservations[$date][$tour]['vehicle'])){
					$vehicleReservation = array();
				} else {
					$vehicleReservation = $reservations[$date][$tour]['vehicle'];
				}
				if(empty($reservations[$date][$tour]['trailer'])){
					$trailerReservation = array();
				} else {
					$trailerReservation = $reservations[$date][$tour]['trailer'];
				}
				?>
				<div class="col-md-12">
					<div class="portlet bordered light tour" data-tour-id="<?php echo $tour; ?>" data-date="<?php echo $date; ?>">
						<div class="portlet-title">
							<div class="caption">
								<span class="uppercase bold font-blue-madison"><?php echo __('Tour') . ' ' . $codes[$tour]; ?></span>
							</div>
							<div class="actions">
								<?php echo $this->Html->link('<i class="fa fa-map-marker"></i> ' . __('View route on Google Maps'), array('controller' => 'vehicle_tours', 'action' => 'generateRouteUrl', $tour), array('class' => 'btn btn-xs default', 'escape' => false, 'target' => '_blank')); ?>
								<?php echo $this->Html->link('<i class="fa fa-truck"></i> ' . __('Documents for driver'), array('controller' => 'vehicle_tours', 'action' => 'documents', $tour . '.pdf'), array('class' => 'btn btn-xs default', 'escape' => false, 'target' => '_blank')); ?>
							</div>
						</div>
						<div class="portlet-body">
							<table class="table tour-orders">
								<thead>
									<tr>
										<th style="width: 2%"></th>
										<th style="width: 28%"><?php echo __('Order'); ?></th>
										<th style="width: 30%"><?php echo __('Address'); ?></th>
										<th style="width: 20%"><?php echo __('Volume'); ?></th>
										<th style="width: 20%"><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>
								<?php foreach($orders as $k => $order): //debug($order);exit; ?>
									<?php
							        if($order['mode'] == 'delivery'){
							        	$stockorderVehicletourId = $order['DeliveryTour'][0]['StockOrdersVehicleTour']['id'];
							        	$weight = $order['DeliveryTour'][0]['StockOrdersVehicleTour']['weight'];
							        }
							        if($order['mode'] == 'return'){
							        	$stockorderVehicletourId = $order['ReturnTour'][0]['StockOrdersVehicleTour']['id'];
							        	$weight = $order['ReturnTour'][0]['StockOrdersVehicleTour']['weight'];
							        }
									?>
									<tr data-stockorder-id="<?php echo $order['StockOrder']['id']; ?>" data-weight="<?php echo $weight; ?>" data-stockorder-vehicletour-id="<?php echo $stockorderVehicletourId; ?>">
										<td style="vertical-align: middle"><i class="fa fa-arrows font-grey"></i></td>
										<td>
											<strong><?php echo __(strtoupper($order['mode'])); ?></strong><br />
											<?php echo $order['StockOrder']['order_number']; ?>
											<?php echo $order['StockOrder']['name']; ?><br />
											<?php echo $order['Client']['name']; ?>
										</td>
										<td>
											<?php if($order['mode'] == 'delivery'): ?>
												<?php echo $order['StockOrder']['delivery_address']; ?><br />
												<?php echo $order['StockOrder']['delivery_zip_city']; ?>
											<?php endif ?>
											<?php if($order['mode'] == 'return'): ?>
												<?php echo $order['StockOrder']['return_address']; ?><br />
												<?php echo $order['StockOrder']['return_zip_city']; ?>
											<?php endif ?>
										</td>
										<td>
											<ul class="list-unstyled">
												<?php if(!empty($order['StockOrder']['packaging']['pallets'])): ?>
													<li><?php echo __('Number of pallets'); ?>: <?php echo $order['StockOrder']['packaging']['pallets']; ?></li>
												<?php endif; ?>
												<?php if(!empty($order['StockOrder']['packaging']['pallets_xl'])): ?>
													<li><?php echo __('Number of pallets XL'); ?>: <?php echo $order['StockOrder']['packaging']['pallets_xl']; ?></li>
												<?php endif; ?>
												<?php if(!empty($order['StockOrder']['packaging']['rollis'])): ?>
													<li><?php echo __('Number of rollis'); ?>: <?php echo $order['StockOrder']['packaging']['rollis']; ?></li>
												<?php endif; ?>
												<?php if(!empty($order['StockOrder']['packaging']['rollis_xl'])): ?>
													<li><?php echo __('Number of rollis XL'); ?>: <?php echo $order['StockOrder']['packaging']['rollis_xl']; ?></li>
												<?php endif; ?>
											</ul>
										</td>
										<td>
											<?php echo $this->Html->link('<i class="fa fa-file-o"></i> ' . __('View PDF'), array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0'), array('class' => 'btn btn-sm default', 'escape' => false, 'target' => '_blank')); ?>
										</td>
									</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							<div class="row">
								<div class="col-md-6 reservation" data-model-type="vehicle" data-reservation-id="<?php echo empty($vehicleReservation) ? '' : $vehicleReservation['VehicleReservation']['id']; ?>">
									<?php echo $this->Form->input('confirmed', array('type' => 'hidden', 'value' => empty($vehicleReservation) ? '' : $vehicleReservation['VehicleReservation']['confirmed'], 'class' => 'confirmed')); ?>
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('Select a model and/or a vehicle'); ?></h4>
										</div>
										<div class="col-md-6">
											<?php echo $this->Form->input('models', array('class' => 'form-control bs-select models', 'label' => false, 'options' => $models['vehicles'], 'empty' => __('Select the vehicle model'), 'value' => empty($vehicleReservation) ? '' : $vehicleReservation['VehicleCompanyModel']['id'], 'data-vehicles-list' => 'vehicles' . str_replace('-','',$date) . $tour)); ?>
										</div>
										<div class="col-md-6">
											<?php echo $this->Form->input('vehicles', array('class' => 'form-control bs-select vehicles', 'label' => false, 'options' => array(), 'empty' => __('Select the vehicle'), 'id' => 'vehicles' . str_replace('-','',$date) . $tour, 'data-default' => empty($vehicleReservation) ? '' : $vehicleReservation['Vehicle']['id'])); ?>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('Start'); ?></h4>
										</div>
										<div class="col-md-4">
											<?php echo $this->Form->input('start', array('type' => 'text', 'class' => 'form-control datetime-picker start', 'label' => false, 'value' => empty($vehicleReservation) ? '' : $this->Time->format($vehicleReservation['VehicleReservation']['start'], '%d-%m-%Y - %H:%M'), 'placeholder' => $this->Time->format($date, '%d-%m-%Y - 08:00'))); ?>
										</div>
										<div class="col-md-8">
											<?php echo $this->Form->input('start_place', array('class' => 'form-control start_place', 'label' => false, 'placeholder' => __('Start place'), 'value' => empty($vehicleReservation) ? '' : $vehicleReservation['VehicleReservation']['start_place'] )); ?>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('End'); ?></h4>
										</div>
										<div class="col-md-4">
											<?php echo $this->Form->input('end', array('type' => 'text', 'class' => 'form-control datetime-picker end', 'label' => false, 'value' => empty($vehicleReservation) ? '' : $this->Time->format($vehicleReservation['VehicleReservation']['end'], '%d-%m-%Y - %H:%M'), 'placeholder' => $this->Time->format($date, '%d-%m-%Y - 08:00'))); ?>
										</div>
										<div class="col-md-8">
											<?php echo $this->Form->input('end_place', array('class' => 'form-control end_place', 'label' => false, 'placeholder' => __('End place'), 'value' => empty($vehicleReservation) ? '' : $vehicleReservation['VehicleReservation']['end_place'])); ?>
										</div>
									</div>
								</div>
								<div class="col-md-6 reservation" data-model-type="trailer" data-reservation-id="<?php echo empty($trailerReservation) ? '' : $trailerReservation['VehicleReservation']['id']; ?>">
									<?php echo $this->Form->input('confirmed', array('type' => 'hidden', 'value' => empty($trailerReservation) ? '' : $trailerReservation['VehicleReservation']['confirmed'], 'class' => 'confirmed')); ?>
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('Select a model and/or a trailer'); ?></h4>
										</div>
										<div class="col-md-6">
											<?php echo $this->Form->input('models', array('class' => 'form-control bs-select models', 'label' => false, 'options' => $models['trailers'], 'empty' => __('Select the trailer model'), 'value' => empty($trailerReservation) ? '' : $trailerReservation['VehicleCompanyModel']['id'], 'data-vehicles-list' => 'trailers' . str_replace('-','',$date) . $tour)); ?>
										</div>
										<div class="col-md-6">
											<?php echo $this->Form->input('trailers', array('class' => 'form-control bs-select trailers', 'label' => false, 'options' => array(), 'empty' => __('Select the trailer'), 'id' => 'trailers' . str_replace('-','',$date) . $tour, 'data-default' => empty($trailerReservation) ? '' : $trailerReservation['Vehicle']['id'])); ?>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('Start'); ?></h4>
										</div>
										<div class="col-md-4">
											<?php echo $this->Form->input('start', array('type' => 'text', 'class' => 'form-control datetime-picker start', 'label' => false, 'value' => empty($trailerReservation) ? '' : $this->Time->format($trailerReservation['VehicleReservation']['start'], '%d-%m-%Y - %H:%M'), 'placeholder' => $this->Time->format($date, '%d-%m-%Y - 08:00'))); ?>
										</div>
										<div class="col-md-8">
											<?php echo $this->Form->input('start_place', array('class' => 'form-control start_place', 'label' => false, 'placeholder' => __('Start place'), 'value' => empty($trailerReservation) ? '' : $trailerReservation['VehicleReservation']['start_place'] )); ?>
										</div>
									</div>	
									<div class="row">
										<div class="col-md-12">
											<h4><?php echo __('End'); ?></h4>
										</div>
										<div class="col-md-4">
											<?php echo $this->Form->input('end', array('type' => 'text', 'class' => 'form-control datetime-picker end', 'label' => false, 'value' => empty($trailerReservation) ? '' : $this->Time->format($trailerReservation['VehicleReservation']['end'], '%d-%m-%Y - %H:%M'), 'placeholder' => $this->Time->format($date, '%d-%m-%Y - 08:00'))); ?>
										</div>
										<div class="col-md-8">
											<?php echo $this->Form->input('end_place', array('class' => 'form-control end_place', 'label' => false, 'placeholder' => __('End place'), 'value' => empty($trailerReservation) ? '' : $trailerReservation['VehicleReservation']['end_place'])); ?>
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col-md-12 text-center" style="margin-top: 20px">
										<?php echo $this->Html->link('<i class="fa fa-check-square-o"></i> ' . __('Save reservations'), '#', array('class' => 'btn btn-success save-reservation', 'escape' => false)); ?>
									</div>
								</div>								
							</div>																		
						</div>
					</div>
				</div>
				<?php if($counter % 2 == 1): ?><div class="clearfix"></div><?php endif; ?>
				<?php $counter++; endforeach; ?>
				</tbody>
			</table>
		<?php endforeach; ?>
		</div>
		<?php endif; ?>
		<br /><br /><br /><br />
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.vehicles();';
$this->end();
?>


