<?php $this->assign('page_title', __('Vehicles')); ?>
<?php $this->assign('page_subtitle', __('Reservations')); ?>
<div class="portlet light calendar">
	<div class="portlet-title">
		<div class="labels">
			<span class="label bg-green-jungle">&nbsp;&nbsp;&nbsp;</span> <?php echo __('Confirmed'); ?>&nbsp;&nbsp;&nbsp;
			<span class="label" style="background-color: #999">&nbsp;&nbsp;&nbsp;</span> <?php echo __('Not confirmed'); ?>&nbsp;&nbsp;&nbsp;
			<span class="label" style="background-color: #E87E04">&nbsp;&nbsp;&nbsp;</span> <?php echo __('Conflicts'); ?>&nbsp;&nbsp;&nbsp;
			<span class="label" style="background-color: #DE8384">&nbsp;&nbsp;&nbsp;</span> <?php echo __('Unavailabilities'); ?>&nbsp;&nbsp;&nbsp;
			<?php echo $this->Html->link(sprintf('<i class="fa fa-truck"></i> %s', __('Ask for a vehicle/trailer')), 'javascript:;', array('data-toggle' => 'modal', 'data-target' => '#modal-ask-vehicle', 'class' => 'btn btn-sm btn-success', 'escape' => false)); ?>
		</div>

	</div>
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<div id="reservations"></div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"></div>
	</div>
</div>
<div class="modal fade" id="modal-unavailability" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.vehicles();';
$this->end();
?>
