<?php $this->assign('page_title', $this->Html->link(__('Vehicles'), array('action' => 'index'))); ?>
<?php $this->assign('page_subtitle', $this->request->data['Vehicle']['name']);?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->request->data['Vehicle']['name']; ?></span>
		</div>
	</div>
	<div class="portlet-body form">

		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Vehicle', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Matriculation / Internal number'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('matriculation', array('label' => false, 'class' => 'form-control'));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $companies));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Model'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('vehicle_company_model_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $models));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Fuel'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('fuel', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Vehicles.fuel')));?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control bs-select'));?>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-9">
					<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
					<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.vehicles();';
$this->end();
?>
