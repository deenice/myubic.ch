<?php

$types = Configure::read('Vehicles.types');
$families = Configure::read('Vehicles.families');

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
	    '<i class="fa fa-edit"></i> ' . __("Edit"),
	    array('controller' => 'vehicles', 'action' => 'edit', $result['Vehicle']['id']),
	    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
	);

    if(empty($result['VehicleCompanyModel']['family'])){
        $family = '';
    } else {
        $family = $families[$result['VehicleCompanyModel']['family']];
    }

    $this->dtResponse['aaData'][] = array(
        $result['Vehicle']['name'],
        $result['Vehicle']['matriculation'],
        $family,
        $result['VehicleCompany']['name'],
        $result['VehicleCompanyModel']['name'],
        $actions
    );
}
