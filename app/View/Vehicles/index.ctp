<?php $this->assign('page_title', __('Vehicles')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-car"></i><?php echo __('List of all vehicles'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a vehicle'), array('controller' => 'vehicles', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Vehicles'); ?>
	</div>
</div>