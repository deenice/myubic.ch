<?php

foreach ($dtResults as $result) {

	$actions = $this->Html->link(
    '<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'missions', 'action' => 'edit', $result['Mission']['id']),
    array('class' => 'btn btn-warning btn-xs', 'escape' => false)
	);

  $this->dtResponse['aaData'][] = array(
    $result['Mission']['name'],
    $actions
  );
}
