<?php $this->assign('page_title', $this->Html->link(__('Missions'), array('controller' => 'missions', 'action' => 'index'))); ?>
<?php $this->assign('page_subtitle', __('Add a mission'));?>

<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-crosshairs font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a mission'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Mission', array('class' => 'form-horizontal')); ?>
		<?php echo $this->Form->input('Mission.user_id', array('value' => AuthComponent::user('id'), 'type' => 'hidden')); ?>
		<?php echo $this->Form->input('Mission.id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-3">
						<?php echo __('Opening date'); ?>
					</label>
					<div class="col-md-9">
						<?php echo $this->Form->input('opening_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16, 'value' => date('d-m-Y')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => $companies)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Person in charge'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('resp_id', array('label' => false, 'class' => 'form-control bs-select', 'data-live-search' => true, 'options' => $managers)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('confirmed_date', array('type' => 'text', 'label' => false, 'class' => 'form-control date-picker', 'data-size' => 16, 'data-date-format' => 'yyyy-mm-dd'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start hour'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End hour'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('end_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small timepicker-24'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('remarks', array('label' => false, 'class' => 'form-control')); ?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						<button type="submit" class="btn blue" value="work" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and work on event'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
