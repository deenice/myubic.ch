<?php $this->assign('page_title', __('Missions')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-trophy"></i><?php echo __('List of all missions'); ?>
		</div>
		<div class="actions">
			<?php echo $this->Html->link('<i class="fa fa-plus"></i> ' . __('Add a mission'), array('controller' => 'events', 'action' => 'add', 'type' => 'mission'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('Mission'); ?>
	</div>
</div>
