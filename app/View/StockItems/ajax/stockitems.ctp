<?php foreach($data as $item): //debug($place)?>
	<tr>
		<td><?php echo $item['StockItem']['code']; ?></td>
		<td><?php echo $this->Html->link($item['StockItem']['name'], array('controller' => 'stockitems', 'action' => 'view', $item['StockItem']['id'])); ?></td>
		<td><?php echo $item['StockCategory']['name']; ?></td>
		<td><?php echo $item['StockCategory']['code']; ?></td>
		<td><?php echo $item['StockSection']['name']; ?></td>
		<td><?php echo $item['StockSection']['code']; ?></td>
		<td><?php echo $item['StockFamily']['name']; ?></td>
		<td><?php echo $item['StockFamily']['code']; ?></td>
		<td>
			<?php echo $this->Html->link('<i class="fa fa-search"></i> View', array('controller' => 'stockitems', 'action' => 'view', $item['StockItem']['id']), array('class' => 'btn default btn-xs', 'escape' => false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('controller' => 'stockitems', 'action' => 'edit', $item['StockItem']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
		</td>
	</tr>
<?php endforeach; ?>