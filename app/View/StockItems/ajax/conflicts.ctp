<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title"><?php echo __('Conflicts'); ?></h4>
</div>
<div class="modal-body">
	<?php echo $this->fetch("content"); ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Close'); ?></button>
</div>