<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	* {
		margin: 0;
		padding: 0;
	}
	@page {
		margin-left: 49px !important;
		margin-right: 49px !important;
		margin-top: 28px !important;
		margin-bottom: 28px !important;
	}
	body {
		font-weight: 300;
		font-family: 'Helvetica', sans-serif;
	}
	p {
		margin: 0;
		padding: 0;
	}
	#header {
		width: 100%;
		margin-bottom: 27px;
	}
	#header .logo,
	#header .title,
	#header .address {
		float: left;
	}
	#header .logo {
		width: 183px;
	}
	#header .logo img {
		width: 167px;
		height: 56px;
	}
	#header .title {
		width: 215px;
	}
	#header .title h3 {
		font-size: 17px;
		line-height: 14px;
		font-weight: 600;
		margin: 0;
		padding: 0;
	}
	#header .title h4 {
		font-size: 14px;
		line-height: 11px;
		font-weight: 300;
		margin: 5px 0 0 0;
		padding: 0;
	}
	#header .address p {
		font-size: 8px;
		line-height: 9px;
		margin: 0;
	}

	#products {
		border-collapse: collapse;
		margin-bottom: 27px;
		width: 100%;
	}
	#products thead {

	}
	#products thead th {
		color: #771318;
		text-indent: 0;
		border-bottom: 2px solid #771318;
		vertical-align: bottom;
		text-align: left;
		padding-bottom: 3px;
		margin-bottom: 0;
		font-size: 8px;
		line-height: 7px;
		font-weight: 300;
	}
	#products thead th.first {
		width: 12%;
		font-size: 12px;
	}
	#products thead th.second {
		width: 33%;
	}
	#products thead th.third {
		width: 5%px;
		padding-left: 5px;
	}
	#products thead th.divider {
		width: 2%;
	}
	#products thead th p {
		font-weight: 300;
		display: block;
		line-height: 10px;
	}
	#products thead th strong {
		font-weight: 600;
	}

	#products tbody tr {
		margin-top: 20px;
	}
	#products tbody tr td {
		padding: 5px 0;
		border-bottom: 1px solid #771318;
		vertical-align: top;
	}
	#products tbody tr td.image {
		vertical-align: middle;
	}
	#products tbody tr td.image img {
		width: 76px;
	}
	#products tbody tr td.price {
		padding-left: 5px;
	}
	#products tbody tr td.divider {
		border-bottom: 0;
		width: 10px;
	}
	#products tbody tr td.bottom {
		border-top: 2px solid #771318;
		border-bottom: 0;
		height: 1px;
	}
	#products tbody tr td p {
		margin: 0;
		padding: 0;
		font-size: 10px;
		line-height: 10px;
	}
</style>

<table id="header" cellpadding="0" cellspacing="0">
	<tr>
		<td class="logo">
			<img src="<?php echo IMAGES; ?>festiloc_gastro_logo.jpg" class="img-responsive" alt="" width="80%">
		</td>
		<td class="title">
			<h3>liste de prix<br>location vaisselle</h3>
			<h4>prix <?php echo date('Y'); ?></h4>
		</td>
		<td class="address">
			<p>
				Festiloc<br />
				Route du Tir Fédéral 10<br />
				1762 Givisiez<br />
				026 676 01 17<br />
				www.festiloc.ch
			</p>
		</td>
	</tr>
</table>
<?php foreach($articles as $subcategoryArticles): ?>
<table id="products">
	<thead>
		<tr>
			<th class="first">
				<p>location</p>
				<p><strong><?php echo strtolower($subcategoryArticles[0]['StockSection']['name']); ?></strong></p>
			</th>
			<th class="second">
				n&deg; ref<br />
				nom article
			</th>
			<th class="third">
				prix [CHF]
			</th>
			<th class="divider"></th>
			<th class="first">
			</th>
			<th class="second">
				n&deg; ref<br />
				nom article
			</th>
			<th class="third">
				prix [CHF]
			</th>
		</tr>
	</thead>
	<tbody>
		<?php $i = 0; foreach ($subcategoryArticles as $key => $article):?>
		<?php if($i%2==0): ?><tr><?php endif; ?>
			<td class="image">
				<?php $imageUrl = ''; ?>
				<?php if(!empty($article['Document'])): ?>
				<?php foreach($article['Document'] as $doc){
					if(strpos($doc['url'], '_1_S') && strlen($doc['filename']) == 15){
						$imageUrl = 'http://localhost/myubic/' . $doc['url'];
					}
					}?>
				<img src="<?php echo $imageUrl; ?>">
				<?php endif; ?>
			</td>
			<td class="infos">
				<p><strong><?php echo $article['StockItem']['code']; ?></strong></p>
				<p><?php echo $article['StockItem']['name']; ?></p>
				<?php if(1==2): ?>
				<p><?php echo $article['StockFamily']['name']; ?></p>
				<?php endif; ?>
			</td>
			<td class="price">
				<p><?php echo $article['StockItem']['price']; ?></p>
			</td>
			<td class="divider">&nbsp;</td>
		<?php if($i%2==1): ?></tr><?php endif; ?>
		<?php $i++; endforeach; ?>
		<tr><td class="bottom" colspan="7">&nbsp;</td></tr>
	</tbody>
</table>
<?php endforeach; ?>
