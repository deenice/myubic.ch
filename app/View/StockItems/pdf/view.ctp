<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
	* {
		margin: 0;
		padding: 0;
	}
	@page {
		margin-left: 49px !important;
		margin-right: 49px !important;
		margin-top: 28px !important;
		margin-bottom: 28px !important;
	}
	body {
		font-weight: 300;
		font-family: 'Cabin', sans-serif;
	}
	p {
		margin: 0;
		padding: 0;
	}
	#header {
		width: 100%;
		padding-bottom: 27px;
	}
	#header .logo, 
	#header .title,
	#header .address {
		float: left;
	}
	#header .logo {
		width: 183px;
	}
	#header .logo img {
		width: 167px;
		height: 56px;
	}
	#header .title {
		width: 215px;
	}
	#header .title h3 {
		font-size: 17px;
		line-height: 14px;
		font-weight: 600;
		margin: 0;
		padding: 0;
	}
	#header .title h4 {
		font-size: 14px;
		line-height: 11px;
		font-weight: 300;
		margin: 5px 0 0 0;
		padding: 0;
	}
	#header .address p {
		font-size: 8px;
		line-height: 9px;
		margin: 0;
	}

	#products {
		border-collapse: collapse;
	}
	#products thead {
		
	}
	#products thead th {
		color: #771318;
		text-indent: 0;
		border-bottom: 2px solid #771318;
		vertical-align: bottom;
		text-align: left;
		padding-bottom: 3px;
		margin-bottom: 0;
		font-size: 8px;
		line-height: 7px;
		font-weight: 300;
	}
	#products thead th.first {
		width: 84px;
		font-size: 12px;
	}
	#products thead th.second {
		width: 98px;
	}
	#products thead th.third {
		width: 52px;
	}
	#products thead th.divider {
		width: 10px;
	}
	#products thead th p {
		font-weight: 300;
		display: block;
		line-height: 10px;
	}
	#products thead th strong {
		font-weight: 600;
	}

	#products tbody tr {
		margin-top: 20px;
	}
	#products tbody tr td {
		padding: 5px 0;
		height: 48px;
		border-bottom: 1px solid #771318;
	}
	#products tbody tr td.divider {
		border-bottom: 0;
		width: 10px;
	}
	#products tbody tr td.bottom {
		border-top: 2px solid #771318;
		border-bottom: 0;
		height: 1px;
	}
	#products tbody tr td p {
		margin: 0;
		padding: 0;
		font-size: 10px;
		line-height: 11px;
	}
</style>

<table id="header" cellpadding="0" cellspacing="0">
	<tr>
		<td class="logo">
			<img src="http://www.festiloc.ch/data/web/festiloc2.ch/templates/img/logo.png">
		</td>
		<td class="title">
			<h3>liste de prix<br>location vaisselle</h3>
			<h4>prix 2015</h4>
		</td>
		<td class="address">
			<p>
				Festiloc<br />
				Route du Tir Fédéral 10<br />
				1762 Givisiez<br />
				026 676 01 17<br />
				www.festiloc.ch
			</p>
		</td>
	</tr>
</table>

<table id="products">
	<thead>
		<tr>
			<th class="first">
				<p>location</p>
				<p><strong>assiettes</strong></p>
			</th>
			<th class="second">
				n&deg; ref<br />
				nom article
			</th>
			<th class="third">
				prix [CHF]
			</th>
			<th class="divider"></th>
			<th class="first">
			</th>
			<th class="second">
				n&deg; ref<br />
				nom article
			</th>
			<th class="third">
				prix [CHF]
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
			<td class="divider">&nbsp;</td>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
		</tr>
		<tr>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
			<td class="divider">&nbsp;</td>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
		</tr>
		<tr>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
			<td class="divider">&nbsp;</td>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
		</tr>
		<tr>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
			<td class="divider">&nbsp;</td>
			<td class="image">
				<img src="http://localhost:8888/myubic/app/webroot/files/images/4050006_1_L.jpg" style="width: 76px; height: 48px;">
			</td>
			<td class="infos">
				<p><strong>101-4012</strong></p>
				<p>Assiette plate</p>
				<p>Baroque</p>
				<p>23 cm</p>
			</td>
			<td class="price">
				<p>2520.00</p>
			</td>
		</tr>
		<tr><td class="bottom" colspan="7">&nbsp;</td></tr>
	</tbody>
</table>

<div class="pricelist">
	
	<div class="clearfix"></div>
	<!--div class="products">
		<div class="legend">
			<div class="first">
				<p>
					location<br />
					<strong>assiettes</strong>
				</p>
			</div>
			<div class="second">
				<p>
					no réf.<br />
					nom article
				</p>
			</div>
			<div class="third">
				<p>prix [CHF]</p>
			</div>

			<div class="first" style="margin-left: 15px">
				<p>&nbsp;</p>
			</div>
			<div class="second">
				<p>
					no réf.<br />
					nom article
				</p>
			</div>
			<div class="third">
				<p>prix [CHF]</p>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php $i=0; while($i++<10): ?>
		<div class="product">
			<div class="image">
				<img src="http://placehold.it/200x100">
			</div>
			<div class="infos">
				<p>
					<strong>1010105</strong><br />
					Assiette plate<br />
					Baroque<br />
					23 cm
				</p>
			</div>
			<div class="price">
				<p>
					2520.00
				</p>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php if($i%2==0): ?><div class="clearfix"></div><?php endif; ?>
		<?php endwhile; ?>
	</div-->
</div>