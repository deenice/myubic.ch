<?php $this->assign('page_title', __('Stock items')); ?>
<?php $this->assign('page_subtitle', __('Stickers')); ?>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<span class="caption-subject bold uppercase"><i class="fa fa-tags"></i> <?php echo __('Stickers'); ?></span>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->Form->create('Sticker', array('class' => 'form-horizontal')); ?>
		<div class="form-body">
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Stock inventory'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('stock_inventory_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $inventories)); ?>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3"><?php echo __('Codes'); ?></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('codes', array('label' => false, 'class' => 'form-control', 'value' => $code)); ?>
					<span class="help-block">Par exemple: 1010101,1234567,5987462</span>
				</div>
			</div>
		</div>
		<div class="form-actions">
			<div class="row">
				<div class="col-md-offset-3 col-md-2">
					<button type="submit" class="btn btn-success btn-block"><i class="fa fa-tags"></i> <?php echo __('Generate stickers'); ?></button>
				</div>
				<div class="col-md-6">
					<span class="help-block"><i class="fa fa-warning"></i> Si le champ <strong>Codes</strong> est rempli, il va supplanter le champ <strong>Inventaire</strong>.</span>
				</div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
$this->end();
?>
