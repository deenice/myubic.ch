<?php
foreach ($dtResults as $result) {

    $actions = $this->Html->link(
            '<i class="fa fa-search"></i> ' . __("View"), 
            array('controller' => 'stock_items', 'action' => 'view', $result['StockItem']['id']), 
            array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
        );
    $actions .= $this->Html->tag('');
    $actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"), 
        array('controller' => 'stock_items', 'action' => 'edit', $result['StockItem']['id']), 
        array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
        );
    $this->dtResponse['aaData'][] = array(
        $this->Html->image($this->ImageSize->crop($result['Document'][0]['url'], 60, 60)),
        $result['StockItem']['code'],
        $result['StockItem']['name'],
        $result['StockCategory']['name'],
        $result['StockSection']['name'],
        $result['StockFamily']['name'],
        $this->Availability->getActualStock($result['StockItem']['id']) . ' / ' . $result['StockItem']['quantity'],
        $actions
    );
}