<?php
foreach ($dtResults as $result) {

  $actions = $this->Html->link(
    '<i class="fa fa-search"></i> ' . __("View"),
    array('controller' => 'stock_items', 'action' => 'view', $result['StockItem']['id']),
    array('class' => 'btn default btn-xs margin-bottom-5', 'escape' => false)
  );
  $actions .= $this->Html->tag('');
  $actions .= $this->Html->link('<i class="fa fa-edit"></i> ' . __("Edit"),
    array('controller' => 'stock_items', 'action' => 'edit', $result['StockItem']['id']),
    array('class' => 'btn btn-warning btn-xs margin-bottom-5', 'escape' => false)
  );
  $this->dtResponse['aaData'][] = array(
    empty($result['Document'][0]['url']) ? $this->Html->image('http://placehold.it/60x60') : $this->Html->image($this->ImageSize->crop($result['Document'][0]['url'], 60, 60, $result['Document'][0]['id'])),
    $result['StockItem']['code'],
    $result['StockItem']['name'],
    $this->Html->link($result['StockCategory']['name'], array('controller' => 'stock_categories', 'action' => 'edit', $result['StockCategory']['id'])),
    $this->Html->link($result['StockSection']['name'], array('controller' => 'stock_sections', 'action' => 'edit', $result['StockSection']['id'])),
    $this->Html->link($result['StockFamily']['name'], array('controller' => 'stock_families', 'action' => 'edit', $result['StockFamily']['id'])),
    $this->Availability->getActualStock($result['StockItem']['id']) . ' / ' . $result['StockItem']['quantity'],
    $actions
  );
}
