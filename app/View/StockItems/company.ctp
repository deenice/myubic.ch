<?php $this->assign('page_title', __('Stock items')); ?>
<?php $this->assign('page_subtitle', __('Stock items of company %s', $company['Company']['name'])); ?>
<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<span class="caption-subject bold uppercase"><?php echo __('Stock items of company %s', $company['Company']['name']); ?></span>
		</div>
		<div class="actions">
			<div class="btn-group">
				<a class="btn btn-default " href="javascript:;" data-toggle="dropdown">
			    <i class="fa fa-plus"></i> Ajouter
			    <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<?php echo $this->Html->link(__('Stock section'), array('controller' => 'stock_sections', 'action' => 'add', $company['Company']['id']), array('escape' => false)); ?>
					</li>
					<li>
						<?php echo $this->Html->link(__('Stock family'), array('controller' => 'stock_families', 'action' => 'add', $company['Company']['id']), array('escape' => false)); ?>
					</li>
					<li>
						<?php echo $this->Html->link(__('Stock item'), array('controller' => 'stock_items', 'action' => 'add', $company['Company']['id']), array('escape' => false)); ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="portlet-body">
		<?php echo $this->DataTable->render('byCompany'); ?>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockitems();';
$this->end();
?>
