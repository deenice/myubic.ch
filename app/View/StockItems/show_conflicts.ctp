<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $crm_statuses = Configure::read('Events.crm_status'); ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	<h4 class="modal-title"><?php echo $stockitem['StockItem']['code']; ?> <?php echo $stockitem['StockItem']['name']; ?></h4>
</div>
<div class="modal-body">
	<?php //debug($conflicts); ?>
	<?php foreach($conflicts as $timestamp => $conflict): ?>
		<h4 class="bg-grey-silver" style="padding: 5px"><strong><?php echo $this->Time->format($timestamp, '%d %B %Y'); ?></strong></h4>
		<?php if(!empty($conflict['message'])): ?>
			<div class="alert alert-warning">
				<p>
					La quantité demandée est supérieure au stock disponible du produit.
				</p>
			</div>
		<?php else: ?>
		<table class="table">
			<thead>
				<tr>
					<th style="width: 35%">#</th>
					<th style="width: 35%">Quantité commandée</th>
					<th style="width: 30%">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($conflict['events'])): $k = 0;?>
					<?php foreach($conflict['events'] as $eventId => $event): if(empty($event)){continue;} ?>
						<tr>
							<td>
								<?php echo $event['Event']['code_name']; ?><br>
								<?php echo $event['Client']['name']; ?><br>
								<small><?php echo $crm_statuses[$event['Event']['crm_status']]; ?></small>
							</td>
							<td>
								<?php echo $quantities[$timestamp][$event['Event']['id']]; ?>
								<small style="display: block">
								<?php echo $siu_statuses[$event['unavailability_status']]; ?><br>
								<?php echo $this->Time->format('d.m.Y H:i', $event['unavailability_start']); ?> - 
								<?php echo $this->Time->format('d.m.Y H:i', $event['unavailability_end']); ?>
								</small>
							</td>
							<td>
								<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Work on event')), array('controller' => 'events', 'action' => 'work', $event['Event']['id']), array('class' => 'btn btn-sm default', 'escape' => false, 'target'=> '_blank')); ?>
					    </td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
				<?php if(!empty($conflict['orders'])): ?>
					<?php foreach($conflict['orders'] as $orderId => $order): ?>
						<tr>
							<td>
								<?php echo $order['StockOrder']['id'] == 1 ? $order['StockOrder']['name'] : $order['StockOrder']['order_number']; ?><br />
								<?php if(!empty($order['Client']['name'])): ?>
								<?php echo $order['Client']['name']; ?><br />
								<?php endif; ?>
								<?php echo $order['StockOrder']['id'] == 1 ? '' : $statuses[$order['StockOrder']['status']]; ?>
							</td>
							<td><?php echo $quantities[$timestamp][$order['StockOrder']['id']]; ?></td>
							<td>
								<?php if($order['StockOrder']['id'] != 1): ?>
								<?php echo $this->Html->link(
					            '<i class="fa fa-file-o"></i> ' . __('View PDF'),
					            array('controller' => 'stock_orders', 'action' => 'invoice', $order['StockOrder']['id'] . '.pdf', '?' => 'download=0'),
					            array('class' => 'btn default btn-sm', 'escape' => false, 'target' => '_blank')
					        ); ?>
								<?php endif; ?>
					    </td>
						</tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Close'); ?></button>
</div>
