<?php $this->assign('page_title', __('Festiloc'));?>
<?php $this->assign('page_subtitle', __('Add an unavailability'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-cart font-blue-hoki"></i>
			<span class="caption-subject font-blue-hoki bold uppercase"><?php echo __('Add an unavailability'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockItemUnavailability', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Stock item'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_item_id', array('label' => false, 'class' => 'form-control bs-select'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('start_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'div' => false, 'class' => 'form-control', 'options' => Configure::read('StockItemUnavailability.statuses')));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue"><i class="fa fa-check"></i> <?php echo __('Save'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.css');
echo $this->Html->css('/metronic/global/plugins/select2/select2.css');
echo $this->Html->css('/metronic/global/plugins/jquery-multi-select/css/multi-select.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datepicker/css/datepicker3.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
echo $this->Html->css('/metronic/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css');
echo $this->Html->css('/metronic/global/plugins/dropzone/css/dropzone.css');
echo $this->Html->css('/metronic/global/plugins/fancybox/source/jquery.fancybox.css');
echo $this->Html->css('/metronic/pages/css/portfolio.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-select/bootstrap-select.min.js');
echo $this->Html->script('/metronic/global/plugins/select2/select2.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-multi-select/js/jquery.multi-select.js');
echo $this->Html->script('/metronic/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
echo $this->Html->script('/metronic/global/plugins/jquery-mixitup/jquery.mixitup.min.js');
echo $this->Html->script('/metronic/global/plugins/fancybox/source/jquery.fancybox.pack.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/global/scripts/metronic.js');
echo $this->Html->script('/metronic/scripts/layout.js');
echo $this->Html->script('/metronic/scripts/demo.js');
echo $this->Html->script('/metronic/pages/scripts/components-dropdowns.js');
echo $this->Html->script('/metronic/pages/scripts/components-pickers.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.prepare();';
echo 'Custom.init();';
echo 'Custom.stockitem();';
$this->end();
?>