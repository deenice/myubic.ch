<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('Conflicts')); ?>
<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $statuses1 = Configure::read('StockItemUnavailability.statuses'); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="icon-energy"></i><?php echo __('Resolve conflicts'); ?>
		</div>
	</div>
	<div class="portlet-body">
	<?php foreach($data as $date => $items): $counter = 0; ?>
		<h3 class="bold uppercase" style="padding: 5px 10px"><?php echo $this->Time->format($date, '%e %B %Y'); ?></h3>
		<div class="row">
		<?php foreach($items as $i => $item): ?>
			<?php if(sizeof($item['orders']) == 1 && isset($item['orders']['manual'])){continue;} ?>
			<div class="col-md-6">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<span class="uppercase bold"><?php echo $item['stock_item']['code'] . ' ' . $item['stock_item']['name']; ?></span>
							<h5>
								<?php echo __('Total quantity'); ?>: <?php echo $item['stock_item']['quantity']; ?> | 
								<strong><?php echo __('Missing quantity'); ?>: <?php echo abs($item['stock_item']['availability']); ?></strong>
							</h5>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table">
							<thead>
								<tr>
									<th><?php echo __('Order'); ?></th>
									<th><?php echo __('Quantity'); ?></th>
									<th><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($item['orders'] as $order): ?>
									<tr>
										<td>
											<?php if($order['stock_order_id'] == 1): ?>
											<?php echo __('Manual unavailability'); ?>
											<?php else: ?>
											<?php echo $order['stock_order_id'] . ' / ' . $order['client_id']; ?><br />
											<?php echo $order['client_name']; ?><br />
											<?php echo __($statuses[$order['status']]); ?>
											<?php endif; ?>
										</td>
										<td>
											<?php echo $order['quantity']; ?>
											<?php echo (!empty($order['unavailability_status'])) ? $this->Html->tag('br') . __($statuses1[$order['unavailability_status']]) : ''; ?>
										</td>
										<td>
											<?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), array('controller' => 'stock_orders', 'action' => 'edit', $order['stock_order_id']), array('class' => 'btn btn-xs btn-warning margin-bottom-5', 'target' => '_blank', 'escape' => false)); ?>
											<br />
											<?php echo $this->Html->link('<i class="fa fa-reply"></i> ' . __('Modify return'), array('controller' => 'festiloc', 'action' => 'depot', 'return', $order['stock_order_id']), array('class' => 'btn btn-xs btn-primary margin-bottom-5', 'target' => '_blank', 'escape' => false)); ?>
											<?php if($order['stock_order_id'] == 1): ?>
											<br />
											<?php echo $this->Html->link('<i class="fa fa-ban"></i> ' . __('Modify unavailability'), array('controller' => 'stock_item_unavailabilities', 'action' => 'edit', $order['unavailability_id']), array('class' => 'btn btn-xs btn-info margin-bottom-5', 'target' => '_blank', 'escape' => false)); ?>
											<?php endif; ?>
											<br />
											<?php echo $this->Html->link(
									            '<i class="fa fa-file-o"></i> ' . __('View PDF'),
									            array('controller' => 'stock_orders', 'action' => 'invoice', $order['stock_order_id'] . '.pdf', '?' => 'download=0'),
									            array('class' => 'btn default btn-xs', 'escape' => false, 'target' => '_blank')
									        ); ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php if($counter++%2 == 1): ?><div class="clearfix"></div><?php endif; ?>
		<?php endforeach; ?>
		</div>
	<?php endforeach; ?>
	</div>
</div>
