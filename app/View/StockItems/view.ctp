<?php $this->assign('page_title', $this->Html->link($company['Company']['name'], array('controller' => 'stock_items', 'action' => 'company', $company['Company']['id'])));?>
<?php $this->assign('page_subtitle', $stockitem['StockItem']['code'] . ' - ' . $stockitem['StockItem']['name']);?>
<?php $statuses = Configure::read('StockOrders.status'); ?>
<div class="row profile">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo $stockitem['StockItem']['name']; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<table class="table table-striped">
									<tbody>
										<tr>
											<td><strong><?php echo __('Category'); ?></strong></td>
											<td><?php echo $stockitem['StockCategory']['name']; ?></td>
										</tr>
										<tr>
											<td><strong><?php echo __('Section'); ?></strong></td>
											<td><?php echo $stockitem['StockSection']['name']; ?></td>
										</tr>
										<tr>
											<td><strong><?php echo __('Family'); ?></strong></td>
											<td><?php echo $stockitem['StockFamily']['name']; ?></td>
										</tr>
										<tr>
											<td><strong><?php echo __('Price'); ?></strong></td>
											<td><?php echo $stockitem['StockItem']['price']; ?> CHF</td>
										</tr>
										<?php if(!empty($stockitem['StockItem']['replacement_price'])): ?>
										<tr>
											<td><strong><?php echo __('Replacement price'); ?></strong></td>
											<td><?php echo $stockitem['StockItem']['replacement_price']; ?> CHF</td>
										</tr>
										<?php endif; ?>
										<?php if(!empty($stockitem['StockItem']['weight'])): ?>
										<tr>
											<td><strong><?php echo __('Weight'); ?></strong></td>
											<td><?php echo $stockitem['StockItem']['weight']; ?> g</td>
										</tr>
										<?php endif; ?>
										<?php if(!empty($stockitem['StockItem']['volume'])): ?>
										<tr>
											<td><strong><?php echo __('Volume'); ?></strong></td>
											<td><?php echo $stockitem['StockItem']['volume']; ?> m3</td>
										</tr>
										<?php endif; ?>
										<?php if(!empty($stockitem['StockItem']['width'])): ?>
										<tr>
											<td><strong><?php echo __('Dimensions'); ?></strong></td>
											<td>
												<?php echo $stockitem['StockItem']['width']; ?>cm  x
												<?php echo $stockitem['StockItem']['height']; ?>cm  x
												<?php echo $stockitem['StockItem']['depth']; ?>cm
											</td>
										</tr>
										<?php endif; ?>
										<?php if(!empty($stockitem['StockItem']['description'])): ?>
										<tr>
											<td><strong><?php echo __('Description'); ?></strong></td>
											<td><?php echo nl2br($stockitem['StockItem']['description']); ?></td>
										</tr>
										<?php endif; ?>
									</tbody>
								</table>
							</div>
							<?php if($this->User->hasRights(array('action' => 'edit'))): ?>
							<div class="col-md-4 hidden-print">
								<?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), array('action' => 'edit', $stockitem['StockItem']['id']), array('class' => 'btn btn-primary btn-block', 'target' => '_blank', 'escape'=> false)); ?>
							</div>
							<div class="col-md-4 hidden-print">
								<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> ' . __('Delete'), array('action' => 'delete', $stockitem['StockItem']['id']), array('class' => 'btn btn-danger btn-block disabled', 'target' => '_blank', 'escape'=> false)); ?>
							</div>
							<div class="col-md-4 hidden-print">
								<?php echo $this->Html->link('<i class="fa fa-tags"></i> ' . __('Sticker'), array('action' => 'stickers', 'code' => $stockitem['StockItem']['code']), array('class' => 'btn btn-default btn-block', 'target' => '_blank', 'escape'=> false)); ?>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-image font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Photo'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="text-center">
									<?php if(!empty($stockitem['Document'][0]['url'])): ?>
									<?php echo $this->Html->image(DS . $stockitem['Document'][0]['url'], array('class' => 'img-responsive')); ?>
									<?php else: ?>
										<img src="http://www.placehold.it/1000" class="img-responsive">
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 hidden-print">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Availabale quantity'); ?> - <?php echo $this->Time->format(date('Y-m-d'), '%d %B %Y') ; ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								<div class="text-center">
									<h1 style="font-size: 400%; margin:0"><?php echo $actualQuantity; ?><small> / <?php echo $stockitem['StockItem']['quantity']; ?></small></h1>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<?php if(!empty($quantities)): ?>
			<div class="col-md-12 hidden-print">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-bar-chart-o font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Availabilities for next days'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-6 margin-botton-5">
								<div class="input-group">
									<input type="text" class="form-control" name="daterange" value="<?php echo date('d.m.Y'); ?> à <?php echo date('d.m.Y', strtotime("+10 days")); ?>" data-stock-item-id="<?php echo $stockitem['StockItem']['id']; ?>">
									<span class="input-group-btn">
									<button class="btn default date-range-toggle" type="button"><i class="fa fa-calendar"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div id="chart_1" class="chart" style="height: 500px; padding-top: 20px"></div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if(!empty($orders)): ?>
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Stock orders'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
						<table class="table">
							<thead>
								<tr>
									<th style="width: 35%"><?php echo __('Order'); ?></th>
									<th style="width: 15%"><?php echo __('Status'); ?></th>
									<th style="width: 35%"><?php echo __('Date of service'); ?></th>
									<th style="width: 20%"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($orders as $key => $batch): ?>
									<tr>
										<td>
											<strong><?php echo $batch['StockOrder']['order_number']; ?></strong>
											<?php echo $batch['StockOrder']['name']; ?><br />
											<?php echo $batch['StockOrder']['Client']['name']; ?>

										</td>
										<td><?php echo __($statuses[$batch['StockOrder']['status']]); ?></td>
										<td>
											<?php echo $this->Time->format($batch['StockOrder']['service_date_begin'], '%d %B %Y'); ?>
											<?php if($batch['StockOrder']['service_date_begin'] != $batch['StockOrder']['service_date_end']): ?>
												 - <?php echo $this->Time->format($batch['StockOrder']['service_date_end'], '%d %B %Y'); ?>
											<?php endif; ?>
										</td>
										<td>
											<?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), array('controller' => 'stock_orders', 'action' => 'edit', $batch['StockOrder']['id']), array('class' => 'btn btn-xs btn-warning margin-bottom-5', 'escape' => false, 'target' => '_blank')); ?><br />
											<?php echo $this->Html->link('<i class="fa fa-file-o"></i> ' . __('View PDF'), array('controller' => 'stock_orders', 'action' => 'invoice', $batch['StockOrder']['id'] . '.pdf', '?' => 'download=0'), array('class' => 'btn btn-xs default', 'escape' => false, 'target' => '_blank')); ?>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<div class="col-md-12 hidden-print">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-link font-blue-madison"></i>
							<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('From the same family'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
						<?php foreach ($stockFromSameFamily as $key => $item): ?>
							<div class="col-md-4">
								<div class="row margin-bottom-5">
									<div class="col-md-5">
										<?php if(!empty($item['Document'][0]['url'])): ?>
										<?php echo $this->Html->image(DS . $item['Document'][0]['url'], array('class' => 'img-responsive')); ?>
										<?php else: ?>
											<img src="http://www.placehold.it/500" class="img-responsive">
										<?php endif; ?>
									</div>
									<div class="col-md-7">
										<small>#<?php echo $item['StockItem']['code']; ?><br />
										<?php echo $this->Html->link($item['StockItem']['name'], array('action' => 'view', $item['StockItem']['id'])); ?>
										</small>
									</div>
								</div>
							</div>
						<?php endforeach ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/amcharts.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/serial.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/pie.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/radar.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/themes/light.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/themes/patterns.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amcharts/themes/chalk.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/ammap/ammap.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/amcharts/amstockcharts/amstock.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-daterangepicker/moment.min.js');
echo $this->Html->script('/metronic/theme/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js');
$this->end();
$this->start('init_scripts');
echo 'Custom.stockitemChart();';
$this->end();
?>
