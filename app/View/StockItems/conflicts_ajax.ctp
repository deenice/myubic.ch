<?php $statuses = Configure::read('StockOrders.status'); ?>
<?php $statuses1 = Configure::read('StockItemUnavailability.statuses'); ?>
<?php foreach($conflicts as $date => $stockitems): ?>
	<h3 class="bold uppercase" style="padding: 5px 10px"><?php echo $this->Time->format($date, '%e %B %Y'); ?></h3>
	<div class="row">
	<?php $i=0; foreach($stockitems as $stockitemId => $data): ?>
		<div class="col-md-6 <?php echo $data['class']; ?>">
			<div class="portlet bordered light">
				<div class="portlet-title">
					<div class="caption">
						<h4 style="margin: 0" class="bold uppercase"><?php echo $data['stockitem']['StockItem']['code']; ?> <?php echo $data['stockitem']['StockItem']['name']; ?></h4>
						<h5><?php echo __('Total quantity'); ?>: <?php echo $data['stockitem']['StockItem']['quantity']; ?> | 
						<strong><?php echo __('Missing quantity'); ?>: <?php echo abs($data['availability']); ?></strong></h5>
					</div>
				</div>
				<div class="portlet-body">
					<table class="table">
						<thead>
							<tr>
								<th><?php echo __('Order'); ?></th>
								<th><?php echo __('Quantity'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<?php if(1==1): ?>
						<tbody>
							<?php foreach($stockorders[$date][$stockitemId] as $order): ?>
								<tr>
									<td>
										<?php echo $order['StockOrder']['order_number']; ?><br />
										<?php echo $order['Client']['name']; ?><br />
										<?php echo __($statuses[$order['StockOrder']['status']]); ?>
									</td>
									<td>
										<?php echo (!empty($order['batch'])) ? $order['batch']['StockOrderBatch']['quantity'] : ''; ?>
										<?php echo (!empty($order['unavailability_status'])) ? $this->Html->tag('br') . __($statuses1[$order['unavailability_status']]) : ''; ?>
									</td>
									<td>
										<?php echo $this->Html->link('<i class="fa fa-edit"></i> ' . __('Edit'), array('controller' => 'stock_orders', 'action' => 'edit', $order['StockOrder']['id']), array('class' => 'btn btn-xs btn-warning', 'target' => '_blank', 'escape' => false)); ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<?php endif; ?>
					</table>
				</div>
			</div>
		</div>
		<?php if($i%2==1 && $i != 0): ?><div class="clearfix"></div><?php endif; ?>
	<?php $i++; endforeach; ?>
	</div>
<?php endforeach; ?>