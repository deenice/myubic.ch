<?php $this->assign('page_title', __('Stockitems')); ?>
<?php $this->assign('page_subtitle', __('Import')); ?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-upload"></i><?php echo __('Import tool'); ?>
		</div>
		<div class="actions">
			<a href="#" class="btn btn-success start-import-images"><i class="fa fa-upload"></i> <?php echo __('Start importing'); ?></a>
		</div>
	</div>
	<div class="portlet-body">
		<p>Les images à importer sont à mettre dans le dossier app/webroot/files/import/images.</p>
		<table class="table table-striped table-import-images">
			<tbody>
				<?php foreach($images as $k => $image): ?>
					<tr class="waiting" data-filename="<?php echo $image; ?>">
						<td class="number"><?php echo $k+1; ?></td>
						<td class="filename"><?php echo $image; ?></td>
						<td class="status">
							<i class="fa fa-clock-o"></i>
							<i class="fa fa-spinner fa-spin"></i>
							<i class="fa fa-check"></i>
							<i class="fa fa-times"></i>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<?php
$this->start('init_scripts');
echo 'Custom.stockitems();';
$this->end();
?>
