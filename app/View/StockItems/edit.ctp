<?php $this->assign('page_title', $companies[$stockitem['StockItem']['company_id']]);?>
<?php $this->assign('page_subtitle', $stockitem['StockItem']['code_name']);?>
<div class="tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#technical" data-toggle="tab"><i class="fa fa-cogs"></i> <?php echo __('Technical features'); ?></a>
		</li>
		<li>
			<a href="#depot" data-toggle="tab"><i class="fa fa-cube"></i> <?php echo __('Storage informations'); ?></a>
		</li>
		<li>
			<a href="#unavailability" data-toggle="tab"><i class="fa fa-ban"></i> <?php echo __('Unavailabilities'); ?></a>
		</li>
		<li>
			<a href="#photos" data-toggle="tab"><i class="fa fa-photo"></i> <?php echo __('Photos'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('StockItem', array('class' => 'form-horizontal', 'type' => 'file')); ?>
			<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Code'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('code', array('label' => false, 'class' => 'form-control', 'readonly' => AuthComponent::user('id') == 190 ? false : true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Description'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('description', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reference'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('reference', array('label' => false, 'class' => 'form-control'));?>
						<span class="help-block">
							Fournisseur, Référence du produit, Prix, Remarques
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('company_id', array('label' => false, 'class' => 'form-control bs-select', 'data-company' => '', 'options' => $companies, 'empty' => true, 'required' => true, 'data-value' => empty($stockitem['StockItem']['company_id']) ? '' : $stockitem['StockItem']['company_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_category_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-category' => '', 'options' => $categories, 'empty' => true, 'required' => true, 'data-value' => empty($stockitem['StockItem']['stock_category_id']) ? '' : $stockitem['StockItem']['stock_category_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Section'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_section_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-section' => '', 'options' => $sections, 'empty' => true, 'required' => true, 'data-value' => empty($stockitem['StockItem']['stock_section_id']) ? '' : $stockitem['StockItem']['stock_section_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Family'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_family_id', array('label' => false, 'class' => 'form-control bs-select', 'data-stock-family' => '', 'options' => $families, 'empty' => true, 'required' => true, 'data-value' => empty($stockitem['StockItem']['stock_family_id']) ? '' : $stockitem['StockItem']['stock_family_id']));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Supplier'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('supplier_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $suppliers, 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Price'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('price', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Replacement price'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('replacement_price', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-small'));?>
						<span class="help-inline">CHF</span>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Total quantity'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('quantity', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Default reprocessing duration'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('default_reconditionning_duration', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group hidden">
					<label class="control-label col-md-3"><?php echo __('Status'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('Stockitems.status')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Status informations'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('status_infos', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Online'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('online', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('To process'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('to_process', array('label' => false, 'class' => 'bs-select form-control', 'options' => Configure::read('StockItems.checkStatuses')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Archived'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('archived', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Subject to discount'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('subject_to_discount', array('label' => false, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'data-size' => 'small'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Stock inventory'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('StockInventory.ids', array('label' => false, 'options' => $inventories, 'multiple' => 'checkbox', 'value' => empty($stockitem['inventories']) ? '':$stockitem['inventories']));?>
						<span class="help-block">Sélectionnez l'inventaire durant lequel le produit a été modifié. Peut rester vide.</span>
					</div>
				</div>
			</div>
			<div class="form-actions" style="padding: 50px 0">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> ' . __('Delete'), array('controller' => 'stock_items', 'action' => 'delete', $stockitem['StockItem']['id']), array('class' => 'btn btn-danger deleteStockItem', 'escape' => false)); ?>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
		</div>
		<div class="tab-pane" id="depot">
			<?php echo $this->Form->create('StockItem', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Stock location'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_location_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'options' => $locations));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Alley'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('alley', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Position'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('position', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Storage type'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_storage_type_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $storageTypes, 'empty' => __('None')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Storage type amount'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('stock_storage_amount', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="tab-pane" id="technical">
			<?php echo $this->Form->create('StockItem', array('class' => 'form-horizontal')); ?>
			<?php echo $this->Form->input('id'); ?>
			<div class="form-body">
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-warning"><p><i class="fa fa-warning"></i> Veuillez respecter les unités de mesure!</p></div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Weight'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('weight', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">g</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Width'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('width', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">cm</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Height'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('height', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">cm</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Length'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('depth', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">cm</span>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Capacity'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('capacity', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">cl</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Diameter'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('diameter', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">cm</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Amperage'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('amperage', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">A</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Voltage'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('voltage', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-medium bs-select', 'options' => Configure::read('StockItems.voltages'), 'empty' => true));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Electrical outlet'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('electrical_outlet', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-medium bs-select', 'options' => Configure::read('StockItems.electrical_outlets'), 'empty' => true));?>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Power'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('power', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
								<span class="help-inline">W</span>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Gas comsumption'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('gas_consumption', array('label' => false, 'div' => false, 'class' => 'form-control input-inline'));?>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="control-label col-md-4"><?php echo __('Size'); ?></label>
							<div class="col-md-8">
								<?php echo $this->Form->input('size', array('label' => false, 'div' => false, 'class' => 'form-control input-inline input-medium bs-select', 'empty' => true, 'options' => Configure::read('Users.sizes')));?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions" style="padding: 100px 0">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
		<div class="tab-pane" id="unavailability">
			<?php echo $this->Form->create('StockItemUnavailability', array('class' => 'form-horizontal', 'controller' => 'stock_item_unavailabilities', 'action' => 'add')); ?>
			<?php echo $this->Form->input('StockItemUnavailability.stock_item_id', array('type' => 'hidden', 'value' => $stockitem['StockItem']['id'])); ?>
			<?php echo $this->Form->input('StockItemUnavailability.stock_order_id', array('type' => 'hidden', 'value' => 1)); ?>
				<div class="form-body">
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('start_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
							<span class="help-inline"><?php echo __('to'); ?></span>
							<?php echo $this->Form->input('end_date', array('label' => false, 'div' => false, 'class' => 'form-control date-picker input-inline', 'type' => 'text')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('status', array('label' => false, 'div' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('StockItemUnavailability.statuses')));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('remarks', array('label' => false, 'div' => false, 'class' => 'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"><?php echo __('Quantity'); ?></label>
						<div class="col-md-9">
							<?php echo $this->Form->input('quantity', array('label' => false, 'div' => false, 'class' => 'form-control input-small', 'type' => 'number'));?>
						</div>
					</div>
				</div>
				<div class="form-actions" style="padding: 50px 0">
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn blue" name="data[destination]" value="index"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
							<button type="submit" class="btn blue" name="data[destination]" value="edit"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
						</div>
					</div>
				</div>
			<?php echo $this->Form->end(); ?>
			<?php if(!empty($stockitem['StockItemUnavailability'])): $statuses = Configure::read('StockItemUnavailability.statuses') ?>
			<div class="portlet light">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-list font-blue-madison"></i>
						<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Unavailabilities'); ?></span>
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<th><?php echo __('Start date'); ?></th>
										<th><?php echo __('End date'); ?></th>
										<th><?php echo __('Stock order'); ?></th>
										<th><?php echo __('Quantity'); ?></th>
										<th><?php echo __('Status'); ?></th>
										<th><?php echo __('Remarks'); ?></th>
										<th><?php echo __('Actions'); ?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach($stockitem['StockItemUnavailability'] as $item): ?>
										<tr>
											<td><?php echo $this->Time->format('d.m.Y', $item['start_date']); ?></td>
											<td><?php echo $this->Time->format('d.m.Y', $item['end_date']); ?></td>
											<td>
												<?php if($item['stock_order_id'] == 1): ?>
												<?php echo __('Manual unavailability'); ?>
												<?php else: ?>
												<?php echo (empty($item['StockOrder'])) ? '' : $this->Html->link($item['StockOrder']['order_number'], array('controller' => 'stock_orders', 'action' => 'edit', $item['StockOrder']['id']), array('target' => '_blank')); ?></td>
												<?php endif; ?>
											<td><?php echo $item['quantity']; ?></td>
											<td><?php echo $statuses[$item['status']]; ?></td>
											<td><?php echo $item['remarks']; ?></td>
											<td>
												<?php echo $this->Html->link(sprintf('<i class="fa fa-edit"></i> %s', __('Edit')), array('controller' => 'stock_item_unavailabilities', 'action' => 'edit', $item['id']), array('class' => 'btn btn-xs btn-warning', 'escape' => false)); ?>
												<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete')), array('controller' => 'stock_item_unavailabilities', 'action' => 'delete', $item['id']), array('class' => 'btn btn-xs btn-danger delete-unavailability', 'escape' => false)); ?>
											</td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<div class="tab-pane fade" id="photos">
			<?php if(!empty($stockitem['Document'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<h3><?php echo __('Existing photos'); ?></h3>
					<?php foreach($stockitem['Document'] as $k => $doc): $title = $doc['name']; ?>
						<div class="col-md-3 col-sm-4 mix" data-rel="photo<?php echo $k; ?>" data-id="<?php echo $doc['id']; ?>">
							<div class="mix-inner bg-grey">
								<?php echo $this->Html->image(DS . $doc['url'], array('class' => 'img-responsive', 'style' => 'margin:0 auto')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $title; ?>
										</h4>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-search"></i> ' . __('See'),
												DS . $doc['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#photo' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="photo<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit photo</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['id'], 'value' => $doc['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['id'], 'label' => __('New name'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __('New photos'); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'internalPhotosForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['StockItem']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'stockitem')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'stockitems')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'jpg,jpeg,png,bmp,tiff,gif')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Portfolio.init();';
echo 'Custom.stockitems();';
echo 'Custom.stockitem();';
$this->end();
?>
