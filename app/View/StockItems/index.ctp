<?php $this->assign('page_title', __('Festiloc')); ?>
<?php $this->assign('page_subtitle', __('List of all stock items')); ?>
<div class="tabbable-custom" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#online" data-toggle="tab"><i class="fa fa-eye"></i> <?php echo __('Online'); ?></a>
		</li>
		<li>
			<a href="#tocheck" data-toggle="tab"><i class="fa fa-warning"></i> <?php echo __('To check'); ?></a>
		</li>
		<li>
			<a href="#offline" data-toggle="tab"><i class="fa fa-eye-slash"></i> <?php echo __('Offline'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="online">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a stock item') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_items', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
						<?php echo $this->Html->link(__('Add a stock item unavailability') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_item_unavailabilities', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Online', array(), array('aaSorting' => array(array( 1, 'asc' )))); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tocheck">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a stock item') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_items', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('ToCheck'); ?>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="offline">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a stock item') . ' <i class="fa fa-plus"></i>', array('controller' => 'stock_items', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<?php echo $this->DataTable->render('Offline', array(), array('aaSorting' => array(array( 1, 'asc' )))); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.stockitems();';
$this->end();
?>
