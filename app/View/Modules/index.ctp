<?php $this->assign('page_title', __('Modules')); ?>
<?php $this->assign('page_subtitle', __('List')); ?>
<div class="tabbable-custom" id="tabs">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#all" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('All modules'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="upcoming">
			<div class="portlet light">
				<div class="portlet-title">
					<div class="actions">
						<?php echo $this->Html->link(__('Add a module') . ' <i class="fa fa-plus"></i>', array('controller' => 'modules', 'action' => 'add'), array('class' => 'btn btn-sm green', 'escape' => false)); ?>
					</div>
				</div>
				<div class="portlet-body">
					<div class="table">
						<table class="table table-striped table-bordered table-hover" id="modules">
						<thead>
							<tr>
								<th><?php echo __('Name'); ?></th>
								<th><?php echo __('Category'); ?></th>
								<th><?php echo __('Subcategory'); ?></th>
								<th><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($modules as $key => $module):?>
							<tr>
								<td><?php echo $module['Module']['name']; ?></td>
								<td><?php echo $module['ModuleCategory']['name']; ?></td>
								<td><?php echo $module['ModuleSubcategory']['name']; ?></td>
								<td>
									<?php echo $this->Html->link('<i class="fa fa-search"></i> View', array('controller' => 'modules', 'action' => 'view', $module['Module']['id']), array('class' => 'btn default btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-edit"></i> Edit', array('controller' => 'modules', 'action' => 'edit', $module['Module']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
									<?php echo $this->Html->link('<i class="fa fa-trash-o"></i> Delete', array('controller' => 'modules', 'action' => 'delete', $module['Module']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false)); ?>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.modules();';
$this->end();
?>