<?php $this->assign('page_title', __('Modules'));?>
<?php $this->assign('page_subtitle', $module['Module']['name']);?>
<div class="row profile">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-shopping-cart font-blue-sharp"></i>
							<span class="caption-subject font-blue-sharp bold uppercase"><?php echo __('Informations'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<div class="row">
							<div class="col-md-12">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
											tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
											quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
											consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
											cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
											proident, sunt in culpa qui officia deserunt mollit anim id est laborum.			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet light bordered">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-users font-blue-sharp"></i>
							<span class="caption-subject font-blue-sharp bold uppercase"><?php echo __('Following people have not followed this module'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table">
							<thead>
								<tr>
									<th><?php echo __('Name'); ?></th>
									<th><?php echo __('Email'); ?></th>
									<th><?php echo __('Phone'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($users1 as $user): ?>
								<tr>
									<td><?php echo $user['User']['full_name']; ?></td>
									<td><?php echo $user['User']['email']; ?></td>
									<td><?php echo $user['User']['phone']; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="col-md-12">
				<div class="portlet light">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-users font-blue-sharp"></i>
							<span class="caption-subject font-blue-sharp bold uppercase"><?php echo __('Following people have followed this module'); ?></span>
						</div>
					</div>
					<div class="portlet-body">
						<table class="table">
							<thead>
								<tr>
									<th><?php echo __('Name'); ?></th>
									<th><?php echo __('Email'); ?></th>
									<th><?php echo __('Phone'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($users as $user): ?>
								<tr>
									<td><?php echo $user['User']['full_name']; ?></td>
									<td><?php echo $user['User']['email']; ?></td>
									<td><?php echo $user['User']['phone']; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>