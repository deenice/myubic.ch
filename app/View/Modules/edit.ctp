<?php $this->assign('page_title', __('Modules'));?>
<?php $this->assign('page_subtitle', $this->request->data['Module']['name']);?>
<div class="tabbable-custom">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#main" data-toggle="tab"><i class="fa fa-list"></i> <?php echo __('Main data'); ?></a>
		</li>
		<li>
			<a href="#documents" data-toggle="tab"><i class="fa fa-files-o"></i> <?php echo __('Documents'); ?></a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="main">
		<?php echo $this->Form->create('Module', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('module_category_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $categories, 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Subcategory'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('module_subcategory_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $subcategories));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		</div>
		<div class="tab-pane" id="documents">
			<?php if(!empty($this->request->data['Document'])): ?>
			<div class="row mix-grid">
				<div class="col-md-12">
					<div class="btn-group pull-right">
						<button type="button" class="btn btn-default">Attribuer une catégorie</button>
						<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="fa fa-angle-down"></i></button>
						<ul class="dropdown-menu" role="menu" id="documentsActions">
						<?php foreach(Configure::read('Documents.Documents.groups') as $key => $group): ?>
							<li>
								<a href="#" data-group="<?php echo $key; ?>">
								<?php echo $group; ?> </a>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
					<h3><?php echo __('Existing documents'); ?></h3>
					<?php foreach($this->request->data['Document'] as $k => $doc): ?>
						<?php $title = $doc['name']; ?>
						<?php $ext = pathinfo($doc['url'], PATHINFO_EXTENSION); ?>
						<div class="col-md-2 col-sm-4 mix" data-rel="clients<?php echo $k; ?>">
							<div class="mix-inner">
								<?php echo $this->Html->image('icons' . DS . $ext . '.png', array('class' => 'img-responsive')); ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<?php echo $title; ?>
											<div class="pull-right">
												<input type="checkbox" class="selectme" data-id="<?php echo $doc['id']; ?>">
											</div>													
										</h4>
									</div>
									<div class="panel-body text-center">
										 <div class="btn-group">
											<?php echo $this->Html->link(
												'<i class="fa fa-eye"></i> ' . __('See'),
												DS . $doc['url'],
												array(
													'escape' => false,
													'class' => 'fancybox-button btn btn-default btn-xs',
													'title' => $title,
													'data-rel' => 'fancybox-button',
													'target' => '_blank'
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-times"></i> ' . __('Remove'),
												DS . $doc['url'],
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs delete-document',
													'data-document-id' => $doc['id']
												)
											); ?>
											<?php echo $this->Html->link(
												'<i class="fa fa-pencil"></i> ' . __('Edit'),
												'#documents' . $k,
												array(
													'escape' => false,
													'class' => 'btn btn-default btn-xs',
													'title' => $title,
													'data-toggle' => 'modal'
												)
											); ?>
										</div>
									</div>
								</div>
							</div>										
						</div>
						<div id="documents<?php echo $k; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Edit document</h4>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-md-12">
											<?php echo $this->Form->input('newName', array('data-id' => $doc['id'], 'value' => $doc['name'], 'class' => 'form-control', 'id' => 'newName'.$doc['id'], 'label' => __('New name'))); ?>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
										<button type="button" class="btn blue edit-document">Save</button>
									</div>
								</div>
							</div>									
						</div>
					<?php endforeach; ?>
				</div>
			</div>	
			<?php endif; ?>
			<div class="row">
				<div class="col-md-12">
					<h3><?php echo __("New documents"); ?></h3>
					<?php echo $this->Form->create('Document', array('type' => 'file', 'class' => 'dropzone form-horizontal bg-grey', 'id' => 'documentsForm', 'url' => array('controller' => 'documents', 'action' => 'upload'))); ?>
					<?php echo $this->Form->input('Document.parent_id', array('type' => 'hidden', 'value' => $this->request->data['Module']['id'])); ?>
					<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => 'module')); ?>
					<?php echo $this->Form->input('Document.controller', array('type' => 'hidden', 'value' => 'modules')); ?>
					<?php echo $this->Form->input('Document.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
					<?php echo $this->Form->input('Document.extensions', array('type' => 'hidden', 'value' => 'pdf,doc,docx')); ?>
					<?php echo $this->Form->end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.modules();';
$this->end();
?>


