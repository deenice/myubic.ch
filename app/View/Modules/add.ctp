<?php $this->assign('page_title', __('Modules'));?>
<?php $this->assign('page_subtitle', __('Add a module'));?>
<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-graduation-cap font-blue-madison"></i>
			<span class="caption-subject font-blue-madison bold uppercase"><?php echo __('Add a module'); ?></span>
		</div>
	</div>
	<div class="portlet-body form">
		<!-- BEGIN FORM-->
		<?php echo $this->Form->create('Module', array('class' => 'form-horizontal')); ?>
			<div class="form-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Name'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('name', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true)); ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Category'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('module_category_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $categories, 'empty' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Subcategory'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('module_subcategory_id', array('label' => false, 'class' => 'form-control bs-select'));?>
					</div>
				</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn blue" value="index" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and exit'); ?></button>
						<button type="submit" class="btn blue" value="edit" name="data[destination]"><i class="fa fa-check"></i> <?php echo __('Save and continue'); ?></button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		<!-- END FORM-->
	</div>
</div>
<?php
$this->start('init_scripts');
echo 'Custom.modules();';
$this->end();
?>


