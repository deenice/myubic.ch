<?php echo $this->Form->create('PlanningBoard', array('url' => array('controller' => 'planning_boards', 'action' => 'edit', empty($board['PlanningBoard']['id']) ? '' : $board['PlanningBoard']['id']), 'class' => 'form-horizontal')); ?>
<?php echo $this->Form->input('PlanningBoard.id', array('type' => 'hidden', 'value' => empty($board['PlanningBoard']['id']) ? '' : $board['PlanningBoard']['id'])); ?>
<?php echo $this->Form->input('PlanningBoard.model', array('type' => 'hidden', 'value' => empty($board['PlanningBoard']['model']) ? $model : $board['PlanningBoard']['model'])); ?>
<?php echo $this->Form->input('PlanningBoard.model_id', array('type' => 'hidden', 'value' => empty($board['PlanningBoard']['model_id']) ? $model_id : $board['PlanningBoard']['model_id'])); ?>
<?php if(!empty($weight)): ?>
<?php echo $this->Form->input('PlanningBoard.weight', array('type' => 'hidden', 'value' => $weight)); ?>
<?php endif; ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo empty($board['PlanningBoard']['name']) ? __('New board') : $board['PlanningBoard']['name']; ?></h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('PlanningBoard.name', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => empty($board['PlanningBoard']['name']) ? '' : $board['PlanningBoard']['name']));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Type'); ?></label>
    <div class="col-md-9">
      <div class="radio-list">
      <?php foreach(Configure::read('PlanningBoards.types') as $key => $type): ?>
        <label class="">
          <input type="radio" name="data[PlanningBoard][type]" id="radio<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo (!empty($board['PlanningBoard']['type']) && $key == $board['PlanningBoard']['type']) ? 'checked="checked"':'';?>> <?php echo $type; ?>
        </label>
      <?php endforeach; ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <?php if(!empty($board['PlanningBoard']['id'])): ?>
    <button type="button" class="btn btn-danger delete-planning-board" data-href="<?php echo Router::url(array('controller' => 'planning_boards', 'action' => 'delete', $board['PlanningBoard']['id'])); ?>"><?php echo __('Delete'); ?></button>
  <?php endif; ?>
  <button type="submit" class="btn btn-success"><?php echo __('Save'); ?></button>
</div>
<?php echo $this->Form->end(); ?>
