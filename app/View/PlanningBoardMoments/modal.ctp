<?php echo $this->Form->create('PlanningBoardMoment', array('url' => array('controller' => 'planning_board_moments', 'action' => 'edit', empty($moment['PlanningBoardMoment']['id']) ? '' : $moment['PlanningBoardMoment']['id']), 'class' => 'form-horizontal')); ?>
<?php echo $this->Form->input('PlanningBoardMoment.id', array('type' => 'hidden', 'value' => empty($moment['PlanningBoardMoment']['id']) ? '' : $moment['PlanningBoardMoment']['id'])); ?>
<?php echo $this->Form->input('PlanningBoardMoment.planning_board_id', array('type' => 'hidden', 'value' => empty($moment['PlanningBoardMoment']['planning_board_id']) ? '' : $moment['PlanningBoardMoment']['planning_board_id'])); ?>
<?php echo $this->Form->input('PlanningBoardMoment.weight', array('type' => 'hidden', 'value' => empty($moment['PlanningBoardMoment']['weight']) ? '' : $moment['PlanningBoardMoment']['weight'])); ?>
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title"><?php echo empty($moment['PlanningBoardMoment']['name']) ? __('New moment') : $moment['PlanningBoardMoment']['name']; ?></h4>
</div>
<div class="modal-body">
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Name'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('PlanningBoardMoment.name', array('label' => false, 'div' => false, 'autofocus' => true, 'class' => 'form-control', 'value' => empty($moment['PlanningBoardMoment']['name']) ? '' : $moment['PlanningBoardMoment']['name']));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Type'); ?></label>
    <div class="col-md-9">
      <div class="row radio-list">
      <?php foreach(Configure::read('Moments.types') as $key => $type): ?>
        <label class="col-md-6">
          <input type="radio" name="data[PlanningBoardMoment][type]" id="radio<?php echo $key; ?>" value="<?php echo $key; ?>" <?php echo (!empty($moment['PlanningBoardMoment']['type']) && $key == $moment['PlanningBoardMoment']['type']) ? 'checked="checked"':'';?>> <?php echo $type; ?>
        </label>
      <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Remarks'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('PlanningBoardMoment.remarks', array('label' => false, 'div' => false, 'class' => 'form-control', 'value' => !empty($moment['PlanningBoardMoment']['remarks']) ? $moment['PlanningBoardMoment']['remarks'] : ''));?>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('PlanningBoardMoment.start', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker-24 input-inline input-small', 'value' => !empty($moment['PlanningBoardMoment']['start']) ? $moment['PlanningBoardMoment']['start'] : '0:00'));?>
      <?php echo $this->Form->input('PlanningBoardMoment.end', array('label' => false, 'div' => false, 'type' => 'text', 'class' => 'form-control timepicker-24 input-inline input-small', 'value' => !empty($moment['PlanningBoardMoment']['end']) ? $moment['PlanningBoardMoment']['end'] : '0:00'));?>
    </div>
  </div>
  <?php if($board['PlanningBoard']['type'] != 'general'): ?>
  <div class="form-group">
    <label class="control-label col-md-3"><?php echo __('Add in general planning'); ?></label>
    <div class="col-md-9">
      <?php echo $this->Form->input('PlanningBoardMoment.add_in_general', array('label' => false, 'type' => 'checkbox', 'value' => 1, 'class' => 'make-switch', 'data-on-text' => __('Yes'), 'data-off-text' => __('No'), 'checked' => true));?>
    </div>
  </div>
  <?php endif; ?>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
  <?php if(!empty($moment['PlanningBoardMoment']['id'])): ?>
    <button type="button" class="btn btn-danger delete-planning-moment" data-href="<?php echo Router::url(array('controller' => 'planning_board_moments', 'action' => 'delete', $moment['PlanningBoardMoment']['id'])); ?>"><?php echo __('Delete'); ?></button>
  <?php endif; ?>
  <button type="submit" class="btn btn-success"><?php echo __('Save'); ?></button>
</div>
<?php echo $this->Form->end(); ?>
