<div class="btn-group btn-group-xs btn-group-solid">
<?php foreach($schedule as $k => $hour): ?>
  <?php if($hour['availability'] == 'yes'): ?>
    <a class="btn btn-hour btn-success"><?php echo sprintf("%02d:00", $hour['hour']); ?></a>
  <?php elseif($hour['availability'] == 'maybe'): ?>
    <a class="btn btn-hour btn-warning"><?php echo sprintf("%02d:00", $hour['hour']); ?></a>
  <?php elseif($hour['availability'] == 'no'): ?>
    <a class="btn btn-hour btn-danger"><?php echo sprintf("%02d:00", $hour['hour']); ?></a>
  <?php elseif($hour['availability'] == 'unknown'): ?>
    <a class="btn btn-hour grey"><?php echo sprintf("%02d:00", $hour['hour']); ?></a>
  <?php endif; ?>
  <?php if($k==11): ?><br><?php endif; ?>
<?php endforeach; ?>
</div>
