<div class="event__documents-pi-footer">
  <div class="left">
    Urban Gaming <span class="bullet">&bull;</span> Rte du Petit-Moncor 1c <span class="bullet">&bull;</span> 1752 Villars-sur-Glâne
  </div>
  <div class="logo">
    <div class="logo__page-number">
      <?php echo $_GET['page']; ?>
    </div>
    <div class="logo__logo">
      <?php echo $this->Html->image('pdf/practical-info-sticker-ug.png'); ?>
    </div>
  </div>
  <div class="right">
    T 036 409 70 19 <span class="bullet">&bull;</span> info@urbangaming.ch <span class="bullet">&bull;</span> www.urbangaming.ch
  </div>
</div>