<div class="event__documents-pi-contact">
  <div class="row">
    <div class="col-xs-12">
      <h3 class="h3">Contact</h3>
      <div class="row">
          <div class="col-xs-5">Jusqu'au jour de votre évènement</div>
          <div class="col-xs-3"><?php echo $event['PersonInCharge']['full_name']; ?></div>
          <div class="col-xs-4">
            <?php if(!empty($event['PersonInCharge']['business_phone'])): ?>
              <?php echo $event['PersonInCharge']['business_phone']; ?>
            <?php else: ?>
              +41 (0)26 409 70 00
             <?php endif; ?>
          </div>
        </div>
        <?php if(!empty($emdo)): ?>
        <div class="row">
          <div class="col-xs-5"><?php echo ucfirst($this->Time->format($event['Event']['confirmed_date'], '%A %d.%m.%Y')); ?>, <strong>uniquement</strong></div>
          <div class="col-xs-3"><?php echo $emdo['User']['full_name']; ?></div>
          <div class="col-xs-4">
            <?php if(!empty($emdo['User']['mobile'])): ?>
              <?php echo $emdo['User']['mobile']; ?>
            <?php elseif(!empty($emdo['User']['phone'])): ?>
              <?php echo $emdo['User']['phone']; ?>
            <?php endif; ?>
          </div>
        </div>
        <?php else: ?>
        <div class="row"> 
          <div class="col-xs-5"><?php echo ucfirst($this->Time->format($event['Event']['confirmed_date'], '%A %d.%m.%Y')); ?>, <strong>uniquement</strong></div>
          <div class="col-xs-7">Vous sera transmis ultérieurement.</div>
        </div>
        <?php endif; ?>
      </table>
    </div>
  </div>
</div>
