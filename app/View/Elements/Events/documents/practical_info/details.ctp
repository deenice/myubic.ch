<div class="event__documents-pi-details">
  <div class="row">
    <div class="col-xs-12">
      <h3 class="h3"><?php echo __('Details'); ?></h3>
      <table>
        <tr>
          <th><?php echo __('Date'); ?></th>
          <td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A %d.%m.%Y'); ?></td>
        </tr>
        <?php if(!empty($event['Region'])): ?>
        <tr>
          <th><?php echo __('Region'); ?></th>
          <td>
            <?php foreach($event['Region'] as $region): ?>
              <?php echo $region['name']; ?>
            <?php endforeach; ?>
          </td>
        </tr>
        <?php endif; ?>
        <tr>
          <th><?php echo __('Activity'); ?></th>
          <td>
            <?php if(!empty($event['SelectedActivity'])): ?>
              <?php foreach($event['SelectedActivity'] as $k => $activity): ?>
                <?php echo $activity['Activity']['name']; ?>
                <?php if(isset($event['SelectedActivity'][$k+1])): ?>, <?php endif; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          </td>
        </tr>
        <?php if(!empty($clientArrival)): ?>
        <tr>
          <th>Rendez-vous</th>
          <td>
            <?php echo $this->Time->format('H:i', $clientArrival['PlanningBoardMoment']['start']); ?>,
            <?php echo $clientArrival['Place'][0]['name']; ?>, <?php echo $clientArrival['Place'][0]['address']; ?>, <?php echo $clientArrival['Place'][0]['zip_city']; ?>
          </td>
        </tr>
        <?php endif; ?>
        <?php if(!empty($clientDeparture)): ?>
        <tr>
          <th>Fin</th>
          <td>
            <?php echo $this->Time->format('H:i', empty($clientDeparture['PlanningBoardMoment']['end']) ? $clientDeparture['PlanningBoardMoment']['start'] : $clientDeparture['PlanningBoardMoment']['end']); ?>
            <?php if(!empty($clientDeparture['Place'])): ?>, <?php echo $clientDeparture['Place'][0]['name']; ?>, <?php echo $clientDeparture['Place'][0]['address']; ?>, <?php echo $clientDeparture['Place'][0]['zip_city']; ?><?php endif; ?>
          </td>
        </tr>
        <?php endif; ?>
      </table>
    </div>
  </div>
</div>
