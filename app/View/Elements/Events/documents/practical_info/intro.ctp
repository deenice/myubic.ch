<div class="event__documents-pi-intro">
  <div class="row">
    <div class="col-xs-12">
      <h2>Infos pratiques</h2>
      <h4><?php echo $event['Client']['name']; ?></h4>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <p>
        <br><br><br>
        Mesdames, Messieurs, <br><br>
        Voici quelques informations pour compléter ou formaliser votre prochaine sortie. <br>
        L'ensemble du team de <?php echo $event['Company']['name']; ?> se réjouit de participer à l'organisation de votre évènement.
      </p>
    </div>
  </div>
</div>
