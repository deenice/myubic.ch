<div class="event__documents-pi-header">
  <div class="row">
    <div class="col-xs-6">
      <br>
      <br>
      <p>
        Villars-sur-Glâne, le <?php echo $this->Time->format(date('Y-m-d'), '%d %B %Y'); ?>
      </p>
    </div>
    <div class="col-xs-6 sticker sticker--ubic pull-right">
      <?php echo $this->Html->image('pdf/practical-info-sticker-ubic.png'); ?>
    </div>
    <div class="col-xs-6 sticker sticker--ug pull-right">
      <?php echo $this->Html->image('pdf/practical-info-sticker-ug.png'); ?>
    </div>
  </div>
</div>
