<div class="event__documents-pi-program">
  <div class="row">
    <div class="col-xs-12">
      <h3 class="h3">Programme</h3>
      <table>
        <?php foreach($moments as $moment): ?>
        <tr>
          <th>
            <?php if(!empty($moment['PlanningBoardMoment']['start']) && empty($moment['PlanningBoardMoment']['end'])): ?>
            <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['start']); ?>
            <?php elseif(empty($moment['PlanningBoardMoment']['start']) && !empty($moment['PlanningBoardMoment']['end'])):?>
            <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['end']); ?>
            <?php elseif(!empty($moment['PlanningBoardMoment']['start']) && !empty($moment['PlanningBoardMoment']['end'])):?>
            <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['start']); ?> - <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['end']); ?>
            <?php endif; ?>
          </th>
          <td><?php echo $moment['PlanningBoardMoment']['name']; ?></td>
        </tr>
        <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>
