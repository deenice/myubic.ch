<?php if(!empty($event['SelectedActivity'])): ?>
<div class="event__documents-pi-misc">
  <div class="row">
    <div class="col-xs-12">
      <h3 class="h3">Tenue et matériel à prévoir</h3>
      <?php foreach($event['SelectedActivity'] as $activity): ?>
        <?php foreach($activity['Activity']['OutfitAssociation'] as $ass): ?>
          <ul>
          <?php foreach($ass['Outfit']['OutfitItem'] as $item): ?>
            <li><?php echo $item['name']; ?></li>
          <?php endforeach; ?>
          </ul>
        <?php endforeach; ?>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<?php endif; ?>
