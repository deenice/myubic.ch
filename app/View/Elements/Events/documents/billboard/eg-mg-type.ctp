<?php if(in_array($event['Event']['company_id'], array(4,5))): ?>
<div class="event__documents-eg-mg-type">
  <div class="label bg-black">
    <?php if($event['Event']['eg_mg_type'] == 'rent'): ?>VM
    <?php elseif($event['Event']['eg_mg_type'] == 'event'): ?>EC
    <?php else: ?>N/A
    <?php endif; ?>
  </div>
</div>
<?php endif; ?>
