<div class="event__documents-bb-infos">
  <h2><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A %d.%m'); ?></h2>
  <h3>
    <?php echo sprintf('%s-%s / %s PAX', $this->Time->format('H:i', $event['Event']['start_hour']), $this->Time->format('H:i', $event['Event']['end_hour']), $event['Event']['pax']); ?>
  </h3>
  <?php if(!empty($event['ConfirmedEventPlace'])): ?>
    <?php foreach($event['ConfirmedEventPlace'] as $place): ?>
      <p><?php echo $place['Place']['name']; ?>, <?php echo $place['Place']['zip_city']; ?></p>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
