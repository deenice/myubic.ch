<div class="row event__documents-bb-footer">
  <div class="col-xs-3">
    <?php echo __('Coordinator'); ?>
  </div>
  <div class="col-xs-4">
    <?php echo $event['PersonInCharge']['first_name']; ?>
  </div>
  <div class="col-xs-2">
    <?php foreach($event['Event']['languages'] as $lang): ?>
      <span class="flag-icon flag-icon-<?php echo $lang == 'en' ? 'gb' : $lang; ?>"></span>&nbsp;
    <?php endforeach; ?>
  </div>
  <div class="col-xs-3 text-right">
    <?php echo $event['Company']['name']; ?>
  </div>
</div>
