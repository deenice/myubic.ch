<div class="event__documents-bb-planning">
  <?php if(!empty($event['BillboardPlanning'])): ?>
    <?php foreach($event['BillboardPlanning']['PlanningBoardMoment'] as $moment): ?>
      <div class="row moment">
        <div class="col-xs-3">
          <?php echo $this->Time->format('H:i', $moment['start']); ?>
        </div>
        <div class="col-xs-9">
          <?php echo $moment['name']; ?>
        </div>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
