<div class="event__documents-bb-jobs">
  <?php if(!empty($event['Job'])): ?>
    <ul>
      <?php foreach($event['Job'] as $job): ?>
        <li>
          <strong style="display: inline-block; width: 20px; text-align: center"><?php echo $job['staff_needed']; ?></strong> <?php echo $jobs[$job['job']]; ?>
          <?php if(!empty($job['hierarchy'])): ?>
          <?php echo $hierarchies[$job['hierarchy']]; ?>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
      <?php if($event['Company']['id'] == 6): ?>
        <li><?php echo __('Tablets'); ?></li>
      <?php endif; ?>
    </ul>
  <?php endif; ?>
</div>

<?php if(in_array($event['Event']['company_id'], array(4,5))): ?>
  <div class="event__documents-bb-jobs">
    <ul>
      <?php if($event['Event']['grills_rondo']): ?>
        <li><strong style="display: inline-block; width: 20px; text-align: center"><?php echo $event['Event']['grills_rondo']; ?></strong> RONDO</li>
      <?php endif; ?>
      <?php if($event['Event']['grills_torro_l']): ?>
        <li><strong style="display: inline-block; width: 20px; text-align: center"><?php echo $event['Event']['grills_torro_l']; ?></strong> TORRO L</li>
      <?php endif; ?>
      <?php if($event['Event']['grills_torro_s']): ?>
        <li><strong style="display: inline-block; width: 20px; text-align: center"><?php echo $event['Event']['grills_torro_s']; ?></strong> TORRO S</li>
      <?php endif; ?>
    </ul>
  </div>
<?php endif; ?>
