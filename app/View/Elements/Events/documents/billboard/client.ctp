<div class="event__documents-client">

  <?php if(!empty($event['Client']['Logo'])): ?>
  <div class="event__documents-client-logo">
    <?php echo $this->Html->image(DS . $event['Client']['Logo']['url']); ?>
  </div>
  <?php endif; ?>

  <div class="event__documents-client-title">
    <h1>
      <?php if(($event['Event']['company_id'] == 4 && $event['Client']['id'] == 2986) || ($event['Event']['company_id'] == 5 && $event['Client']['id'] == 2972)): ?>
        <?php echo $event['Event']['name']; ?>
      <?php else: ?>
        <?php echo $event['Client']['name']; ?>
      <?php endif; ?>
    </h1>
  </div>

</div>
