<div class="event__documents-client hidden">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-condensed table-striped">
        <tbody>
          <?php foreach($planning as $k => $moment): ?>
            <tr>
              <th style="width: 35%"><?php if($k==0): ?>Programme client<?php endif; ?></th>
              <td style="width: 15%"><?php echo $this->Time->format('H:i', empty($moment['PlanningBoardMoment']['start']) ? $moment['PlanningBoardMoment']['end'] : $moment['PlanningBoardMoment']['start']); ?></td>
              <td style="width: 50%"><?php echo $moment['PlanningBoardMoment']['name']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
