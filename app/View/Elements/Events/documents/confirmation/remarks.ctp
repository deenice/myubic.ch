<div class="event__documents-thanks">
  <p>
    Merci de nous confirmer par retour d'e-mail que tout est en ordre pour toi. <br>
    Au plaisir de collaborer avec toi pour cet évènement.
  </p>
</div>
<?php if(!empty($extra['Job']['engagement_remarks'])): ?>
<div class="event__documents-remarks">
  <strong>Remarques</strong>
  <p class="font-red">
    <?php echo nl2br($extra['Job']['engagement_remarks']);?>
  </p>
</div>
<?php endif; ?>
