<div class="event__documents-staff">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-condensed table-striped">
        <thead>
          <tr>
            <th colspan="4">Personnel engagé en tant que</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($staff as $extra): ?>
            <tr>
              <td style="width: 25%">
                <?php if(!empty($extra['Job']['languages'])): $languages = explode(',', $extra['Job']['languages']); ?>
                  <?php foreach($languages as $language): if($language == 'fr'){continue;} ?>
                    <span class="flag-icon flag-icon-<?php echo $language == 'en' ? 'gb' : $language; ?>"></span>&nbsp;
                  <?php endforeach; ?>
                <?php endif; ?>
                <?php echo $jobs[$extra['Job']['job']]; ?>
              </td>
              <td style="width: 25%"><?php echo $extra['User']['full_name']; ?></td>
              <td style="width: 25%">
                <?php if($extra['User']['role'] == 'fixed' && $extra['JobUser']['salary'] == Configure::read('Users.salary_for_fixed')): ?>
                  Interne
                <?php elseif($extra['User']['trainee'] && $extra['JobUser']['salary'] == Configure::read('Users.salary_for_trainee')): ?>
                  Interne
                <?php else: ?>
                  <?php echo $extra['JobUser']['salary']; ?>
                <?php endif; ?>
              </td>
              <td style="width: 25%"><?php echo $extra['User']['phone']; ?></td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
