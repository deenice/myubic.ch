<ul>

<?php if(!empty($moment['Client'])): ?>
<?php foreach($moment['Client'] as $resource): ?>
  <li class="client"><?php echo $resource['name']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['Manager'])): ?>
<?php foreach($moment['Manager'] as $resource): ?>
  <li class="manager"><?php echo $resource['full_name']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['Extra'])): ?>
<?php foreach($moment['Extra'] as $resource): ?>
  <li class="extra"><?php echo $resource['full_name']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['Activity'])): ?>
<?php foreach($moment['Activity'] as $resource): ?>
  <li class="activity"><?php echo $resource['name']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['FBModule'])): ?>
<?php foreach($moment['FBModule'] as $resource): ?>
  <li class="fb_module"><?php echo $resource['name']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['Place'])): ?>
<?php foreach($moment['Place'] as $resource): ?>
  <li class="place"><?php echo $resource['name']; ?>, <?php echo $resource['zip_city']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

<?php if(!empty($moment['Vehicle'])): ?>
<?php foreach($moment['Vehicle'] as $resource): ?>
  <li class="vehicle"><?php echo $resource['name_number']; ?></li>
<?php endforeach; ?>
<?php endif; ?>

</ul>
