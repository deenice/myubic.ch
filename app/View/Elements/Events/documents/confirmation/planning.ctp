<div class="event__documents-planning row">
	<div class="col-xs-12">
		<table class="table table-condensed table-striped">
		  <thead>
		    <tr>
		      <th colspan="2" class="schedule"><?php echo __('Schedule'); ?></th>
          <th class="steps"><?php echo __('Steps');?></th>
          <th class="resources"><?php echo __('Resources'); ?></th>
		    </tr>
		  </thead>
      <tbody>
        <?php foreach($planning['PlanningBoardMoment'] as $moment): ?>
          <tr>
            <td><?php echo $this->Time->format('H:i', $moment['start']); ?></td>
            <td><?php echo $this->Time->format('H:i', $moment['end']); ?></td>
            <td>
              <span class="bold"><?php echo $moment['name']; ?></span>
              <?php if(!empty($moment['remarks'])): ?>
                <br><small><?php echo $moment['remarks']; ?></small>
              <?php endif; ?>
            </td>
            <td>
              <?php echo $this->element('Events/documents/confirmation/resource', array('moment' => $moment)); ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
		</table>
  </div>
</div>
