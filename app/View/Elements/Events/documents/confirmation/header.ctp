<div class="event__documents-header row-fluid">
  <div class="col-xs-3 event__documents-logo">
    <?php echo !empty($event['Company']['Logo']) ? $this->Html->image(DS . $event['Company']['Logo']['url']) : ''; ?>
  </div>
  <div class="col-xs-9">
    <?php if($title == 'client'): ?>
    <h1><?php echo $event['Client']['name']; ?></h1>
    <h2><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A, %d %B %Y'); ?></h2>
    <?php elseif($title == 'extra'): ?>
      <h1 class="uppercase"><?php echo $extra['User']['full_name']; ?></h1>
      <h2>Confirmation d'engagement</h2>
    <?php elseif($title == 'staff'): ?>
      <h1 class="uppercase"><?php echo $event['Client']['name']; ?></h1>
      <h2>Personnel - <?php echo $this->Time->format($event['Event']['confirmed_date'], '%A, %d %B %Y'); ?></h2>
    <?php elseif($title == 'feedback'): ?>
      <h1 class="uppercase"><?php echo $event['Client']['name']; ?></h1>
      <h2>Feedback</h2>
    <?php endif; ?>
  </div>
  <br>
  <div class="col-xs-12 event__documents-infos">
    <div class="col-xs-7">
      <table>
        <tr>
          <th><?php echo __('Name'); ?></th>
          <td><?php echo $event['Client']['name']; ?></td>
        </tr>
        <tr>
          <th><?php echo __('Company contact'); ?></th>
          <td><?php echo $event['ContactPeople']['full_name']; ?></td>
        </tr>
        <tr>
          <th><?php echo __('ZIP / City'); ?></th>
          <td><?php echo $event['Client']['zip_city']; ?></td>
        </tr>
        <tr>
          <th><?php echo __('Coordinator'); ?></th>
          <td><?php echo $event['PersonInCharge']['full_name']; ?></td>
        </tr>
      </table>
    </div>
    <div class="col-xs-5">
      <table>
        <tr>
          <th><?php echo __('Day'); ?></th>
          <td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A'); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Date'); ?></th>
          <td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%d %B %y'); ?></td>
        </tr>
        <tr>
          <th><?php echo __('Client arrival'); ?></th>
          <td>
            <?php if(!empty($clientArrival)): ?>
              <?php echo $this->Time->format('H:i', $clientArrival['PlanningBoardMoment']['start']); ?>
            <?php else: ?>
              Non défini
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <th><?php echo __('Place'); ?></th>
          <td>
            <?php if(!empty($event['ConfirmedEventPlace'])): ?>
            <?php foreach($event['ConfirmedEventPlace'] as $k => $place): ?>
            <?php echo $place['Place']['name']; ?> <br> <?php echo $place['Place']['zip_city']; ?>
            <?php if(isset($event['ConfirmedEventPlace'][$k+1])): ?><br><?php endif; ?>
            <?php endforeach; ?>
            <?php else: ?>
              <?php echo __('No confirmed place'); ?>
            <?php endif; ?>
          </td>
        </tr>
        <tr>
          <th><?php echo __('PAX'); ?></th>
          <td>
            <?php if(!empty($event['Event']['confirmed_number_of_persons'])): ?>
              <?php echo $event['Event']['confirmed_number_of_persons']; ?>
            <?php elseif(!empty($event['Event']['min_number_of_persons'])): ?>
              <?php echo $event['Event']['min_number_of_persons']; ?>
              <?php if(!empty($event['Event']['max_number_of_persons']) && $event['Event']['min_number_of_persons'] != $event['Event']['max_number_of_persons']): ?>
                 - <?php echo $event['Event']['max_number_of_persons']; ?>
              <?php endif; ?>
            <?php endif; ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
</div>
