<?php if(!empty($extra['Job']['Outfit'])): ?>
<div class="event__documents-outfit">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-condensed table-striped">
        <tbody>
          <?php foreach($extra['Job']['Outfit']['OutfitItem'] as $k => $item): ?>
          <tr>
            <th style="width: 15%"><?php if($k==0): ?>Tenue<?php endif; ?></th>
            <td style="width: 20%">
              <?php if($k==0): ?>
                <?php if(!empty($extra['Job']['Activity'])): ?>
                  <?php echo $extra['Job']['Activity']['name']; ?>
                <?php elseif(!empty($extra['Job']['FBModule'])): ?>
                  <?php echo $extra['Job']['FBModule']['name']; ?>
                <?php endif; ?>
              <?php endif; ?>
            </td>
            <td style="width: 65%"><?php echo $item['name']; ?></td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php endif; ?>
