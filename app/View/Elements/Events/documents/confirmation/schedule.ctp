<div class="event__documents-schedule">
  <div class="row">
    <div class="col-xs-12">
      <table class="table table-condensed table-striped">
        <thead>
          <tr>
            <th colspan="4">Horaire d'engagement et lieu de rendez-vous</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($extra['moments'] as $k => $moment): ?>
          <tr>
            <th style="width: 15%"><?php echo $k==0 ? $extra['User']['first_name'] : ''; ?></th>
            <td style="width: 20%"><?php echo $moment['PlanningBoardMoment']['name']; ?></td>
            <td style="width: 15%">
              <?php if(!empty($moment['PlanningBoardMoment']['start']) && empty($moment['PlanningBoardMoment']['end'])): ?>
                <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['start']); ?>
              <?php elseif(empty($moment['PlanningBoardMoment']['start']) && !empty($moment['PlanningBoardMoment']['end'])): ?>
                <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['end']); ?>
              <?php elseif(!empty($moment['PlanningBoardMoment']['start']) && !empty($moment['PlanningBoardMoment']['end'])): ?>
                <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['start']); ?> - 
                <?php echo $this->Time->format('H:i', $moment['PlanningBoardMoment']['end']); ?>
              <?php endif; ?>
            </td>
            <td style="width: 50%">
              <?php if(!empty($moment['Place'])): ?>
                <?php foreach($moment['Place'] as $place): ?>
                  <?php echo $place['name']; ?>
                <?php endforeach; ?>
              <?php endif; ?>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="col-xs-12" style="margin-bottom: 10px">
      <span>
        <?php if($extra['JobUser']['route'] == 'no' || empty($extra['JobUser']['route'])): ?>
        Tes trajets ne sont pas comptés dans tes heures.
        <?php elseif($extra['JobUser']['route'] == 'yes'): ?>
        Tes trajets sont comptés dans tes heures.
        <?php elseif($extra['JobUser']['route'] == 'duration'): ?>
        Un trajet de <?php echo $this->Time->format('G:i', $extra['JobUser']['route_duration']); ?> est compté dans tes heures.
        <?php endif; ?>
      </span>
    </div>
    <?php if(!empty($carSharingStarts)): ?>
      <div class="col-xs-12">
        <table class="table table-condensed table-striped">
          <thead>
            <tr>
              <th colspan="2">Covoiturage possible</th>
            </tr>
          </thead>
          <tbody>
          <?php foreach($carSharingStarts as $start): ?>
          <tr>
            <td style="width: 15%"><?php echo $this->Time->format('H:i', $start['PlanningBoardMoment']['start']); ?></td>
            <td>Départ de <?php echo $start['Place'][0]['name']; ?></td>
          </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
        <span class="font-red">Nous te remercions de nous indiquer par retour d'email si tu souhaites covoiturer.</span>
      </div>
    <?php endif; ?>
    <div class="col-xs-12">
      <small>Nous nous permettons de déduire les temps de pause imprévus.</small>
    </div>
  </div>
</div>

