<div class="col-xs-12">
  <div class="event__documents-h-staff">
    <div class="event__documents-h-staff-header">
      <div class="row">
        <div class="col-xs-12">
          <div class="name"><?php echo $extra['User']['full_name']; ?></div>
        </div>
      </div>
      <div class="row line">
        <div class="col-xs-3"><?php echo empty($extra['User']['mobile']) ? $extra['User']['phone'] : $extra['User']['mobile']; ?></div>
        <div class="col-xs-3"><?php echo $jobs[$extra['Job']['job']]; ?></div>
        <div class="col-xs-3">
          <?php if($extra['User']['role'] == 'fixed' && $extra['JobUser']['salary'] == Configure::read('Users.salary_for_fixed')): ?>
              Interne
            <?php elseif($extra['User']['trainee'] && $extra['JobUser']['salary'] == Configure::read('Users.salary_for_trainee')): ?>
              Interne
            <?php else: ?>
              <?php echo $extra['JobUser']['salary']; ?>
            <?php endif; ?>
        </div>
        <div class="col-xs-3"></div>
      </div>
      <div class="row line">
        <div class="col-xs-4 bold">
          Horaire et lieu de convocation
        </div>
        <div class="col-xs-8">
          <?php if(!empty($extra['convocation'])): ?>
            <?php echo $this->Time->format('H:i', $extra['convocation']['PlanningBoardMoment']['start']); ?>
            <?php if(!empty($extra['convocation']['Place'])): ?> - 
            <?php echo $extra['convocation']['Place'][0]['name']; ?>, 
            <?php echo $extra['convocation']['Place'][0]['zip_city']; ?>
            <?php endif; ?>            
          <?php endif; ?>
        </div>
      </div>
      <div class="row line">
        <div class="col-xs-4 bold">
          Horaire confirmé
        </div>
        <div class="col-xs-2">
          <?php if(!empty($extra['mandate_start'])): ?>
          <?php echo $this->Time->format('H:i', empty($extra['mandate_start']['PlanningBoardMoment']['end']) ? $extra['mandate_start']['PlanningBoardMoment']['start'] : $extra['mandate_start']['PlanningBoardMoment']['end']); ?>
          <?php else: ?>
          <?php echo $this->Time->format('H:i', $extra['Job']['start_time']); ?>
          <?php endif; ?>
           -
          <?php if(!empty($extra['mandate_end'])): ?>
          <?php echo $this->Time->format('H:i', empty($extra['mandate_end']['PlanningBoardMoment']['end']) ? $extra['mandate_end']['PlanningBoardMoment']['start'] : $extra['mandate_end']['PlanningBoardMoment']['end']); ?>
          <?php else: ?>
          <?php echo $this->Time->format('H:i', $extra['Job']['end_time']); ?>
          <?php endif; ?>
        </div>
        <div class="col-xs-3 bold">
          Trajet
        </div>
        <div class="col-xs-3">
          <?php if($extra['JobUser']['route'] == 'yes'): ?><?php echo __('Yes'); ?><?php endif; ?>
          <?php if($extra['JobUser']['route'] == 'no'): ?><?php echo __('No'); ?><?php endif; ?>
          <?php if($extra['JobUser']['route'] == 'duration'): ?><?php echo $this->Time->format('G:i', $extra['JobUser']['route_duration']); ?><?php endif; ?>
        </div>
      </div>
    </div>
    <div class="event__documents-h-staff-hours">
      <div class="row line">
        <div class="col-xs-4 bold">Début:</div>
        <div class="col-xs-3 bold">Fin:</div>
        <div class="col-xs-5 bold">Total heure brut</div>
      </div>
      <div class="row line">
        <div class="col-xs-6 bold">Signature:</div>
        <div class="col-xs-6 bold">Visa EM:</div>
      </div>
    </div>
  </div>
</div>
