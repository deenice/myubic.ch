<tr class="box">
  <th>
    <?php echo $title; ?>
    <?php if($subtitle): ?><small><?php echo $subtitle; ?></small><?php endif; ?>
  </th>
</tr>
<tr class="box">
  <?php if(isset($placeholder)): ?>
  <td style="vertical-align: bottom; color: #333 !important; padding: 5px;">
    <?php echo $placeholder; ?>
  </td>
  <?php else: ?>
  <td>&nbsp;</td>
  <?php endif; ?>
</tr>

