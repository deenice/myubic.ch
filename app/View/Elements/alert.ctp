<div class="">
	<div class="alert alert-<?php echo isset($type) ? $type : 'info'; ?>">
		<p>
		<?php if($type == 'success'): ?><i class="fa fa-check"></i>&nbsp;
		<?php elseif($type == 'warning'): ?><i class="fa fa-warning"></i>&nbsp;
		<?php elseif($type == 'danger'): ?><i class="fa fa-times-circle"></i>&nbsp;
		<?php else: ?><i class="fa fa-info-circle"></i>&nbsp;<?php endif; ?>
		<?php echo h($message); ?>
		</p>
	</div>
</div>