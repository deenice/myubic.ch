<?php
global $counter;
echo $this->Form->input(
	'competence',
	array(
		'id' => false,
		'label' => false,
		'div' => false,
		'checked' => empty($id) ? false : true,
		'data-sector' => $sector,
		'data-job' => $job,
		'data-user-id' => $user_id,
		'data-radio' => in_array($job, array('deliverer', 'griller')) ? false : true,
		'data-activity-id' => !empty($activity_id) ? $activity_id : '',
		'data-fb-module-id' => !empty($fb_module_id) ? $fb_module_id : '',
		'data-competence-id' => !empty($id) ? $id : '',
		'data-hierarchy' => $hierarchy >= 0 ? $hierarchy : '',
		'rel' => !empty($rel) ? $rel : '',
		'data-cell-id' => $counter++
	)
); ?>
