<?php 
$id = '';
switch($type){
		case 'delivery':
			$class = 'primary';
			$icon = 'fa-arrow-right';
			$title = __('Delivery');
		break;
		case 'return':
			$class = 'info';
			$icon = 'fa-arrow-left';
			$title = __('Return');
		break;
		case 'route':
			$class = 'warning';
			$icon = 'fa-truck';
			$title = __('Route');
		break;
		case 'loading':
			$class = 'success';
			$icon = 'fa-exchange';
			$title = __('Loading / Unloading');
		break;
		case 'break':
			$class = 'danger';
			$icon = 'fa-coffee';
			$title = __('Break');
		break;
		default:
			$class = '';
			$icon = '';
		break;
	}
	if(!empty($empty) AND $empty) {
		if($type == 'break'){
			$id = 'empty_break';
		}
		if($type == 'loading'){
			$id = 'empty_loading';
		}
		$type .= ' empty hidden';
	}
?>
<li class="sortable <?php echo $type; ?>" id="<?php echo $id; ?>">
	<div class="col1">
		<div class="cont">
			<div class="cont-col1">
				<div class="label label-<?php echo $class; ?> label-sm">
					<i class="fa <?php echo $icon; ?>"></i>
				</div>
			</div>
			<div class="cont-col2">
				<div class="desc">
					<span class="uppercase bold"><?php echo $title; ?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="col2">
		<div class="date">
			<i class="fa fa-spin fa-spinner"></i>
		</div>
	</div>
</li>
