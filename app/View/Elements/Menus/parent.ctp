<li class="menu-dropdown mega-menu-dropdown mega-menu-full">
	<?php if($parent['Menu']['menu'] == 'extra-menu'): ?>
		<?php echo $this->Html->link(sprintf('%s <span class="arrow"></span>', AuthComponent::user('full_name')), unserialize($parent['Menu']['url']), array('escape' => false)); ?>
	<?php else: ?>
		<?php echo $this->Html->link(sprintf('%s <span class="arrow"></span>', __($parent['Menu']['name'])), unserialize($parent['Menu']['url']), array('escape' => false)); ?>
	<?php endif; ?>
	<ul class="dropdown-menu">
		<li>
			<div class="mega-menu-content">
				<div class="row">
					<?php foreach($parent['children'] as $child): ?>
						<div class="col-md-2">
							<ul class="mega-menu-submenu">
								<li><h3><?php echo __($child['Menu']['name']); ?></h3></li>
								<?php foreach($child['children'] as $childd): ?>
									<li>
										<?php echo $this->Html->link(__($childd['Menu']['name']), unserialize($childd['Menu']['url'])); ?>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</li>
	</ul>
</li>
