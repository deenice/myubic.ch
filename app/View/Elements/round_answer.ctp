<?php if(empty($data['RoundAnswer'])): ?>
  <input type="checkbox" class="round_step_checkbox">
<?php else: ?>
  <?php if(empty($data['RoundAnswer'][0]['answer'])): ?>
    <a href="javascript:;" class="set-waiting" data-answer="waiting"><i class="fa fa-minus font-grey-silver"></i></a>
  <?php else: ?>
    <?php if($data['RoundAnswer'][0]['answer'] == 'waiting'): ?>
      <a href="javascript:;" class="set-waiting" data-answer="null"><i class="fa fa-clock-o font-grey-silver"></i></a>
    <?php elseif($data['RoundAnswer'][0]['answer'] == 'interested'): ?>
      <i class="fa fa-check font-green"></i>
    <?php elseif($data['RoundAnswer'][0]['answer'] == 'not_interested'): ?>
      <i class="fa fa-thumbs-down text-warning"></i>
    <?php elseif($data['RoundAnswer'][0]['answer'] == 'not_available'): ?>
      <i class="fa fa-ban font-red"></i>
    <?php endif; ?>
  <?php endif; ?>
<?php endif; ?>
<?php echo $this->Form->input('RoundAnswer.checked.', array('type' => 'hidden', 'value' => empty($data['RoundAnswer']) ? 0 : 1, 'class' => 'checked')); ?>
<?php echo $this->Form->input('RoundAnswer.round_step_id.', array('type' => 'hidden', 'value' => $data['id'])); ?>
<?php echo $this->Form->input('RoundAnswer.user_id.', array('type' => 'hidden', 'value' => $user_id)); ?>
<?php echo $this->Form->input('RoundAnswer.answer.', array('type' => 'hidden', 'value' => empty($data['RoundAnswer'][0]['answer']) ? '' : $data['RoundAnswer'][0]['answer'])); ?>
<?php echo $this->Form->input('RoundAnswer.id.', array('type' => 'hidden', 'value' => empty($data['RoundAnswer'][0]['id']) ? '' : $data['RoundAnswer'][0]['id'])); ?>
