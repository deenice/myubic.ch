<p>Salut <?php echo $userFirstName; ?>,<br><br>

Voici une proposition de job faite pour toi ! Es-tu disponible et intéressé(-e) par le poste suivant :
<ul>
<li><?php echo $eventDate; ?></li>
<li>Event: <?php echo $eventName; ?></li>
<li>Client : <?php echo $clientName; ?></li>
<li>Nb pax: <?php echo $eventPax; ?> PAX</li>
<?php if(!empty($languages)): ?><li>Langue pendant l'évènement: <?php echo implode(', ', $languages); ?></li><?php endif; ?>
<?php if(!empty($placeName)): ?><li><?php echo $placeName; ?></li><?php endif; ?>
</ul>
Ta mission : <?php echo $jobJob; ?>, <?php echo (!empty($jobActivityName)) ?  $jobActivityName : ''; ?> (<?php echo $jobSalary; ?> CHF / heure)<br />
Horaire approximatif de ton extra
<?php if($job != 'mg_service_crew' && $job != 'service_crew' && $job != 'test'): ?>
(calculé depuis Villars-sur-Glâne)
<?php endif; ?>
<br>
de <?php echo $jobStartTime; ?> à <?php echo $jobEndTime; ?>.
<?php if($job == 'test' || $job == 'service_crew'):?>
<br>
Merci de te rendre directement sur place.
<?php endif; ?>
<?php if(!empty($remarks)): ?>
<br><br>
<?php echo $remarks; ?>
<?php endif; ?>

<br /><br />

<strong>Info complémentaire</strong><br>
Je te remercie de répondre <strong>uniquement</strong> au moyen des boutons ci-dessous. Tu peux en tout temps voir sur ton profil myubic la liste de tes prochains évènements.
<br><br>
<?php if(!empty($message)): ?>
	<?php echo nl2br($message); ?><br /> <br />
<?php endif ?>


Si tu es intéressé par le poste, clique sur ce bouton:
<br />
<br />
<?php echo $this->element('Emails/button', array('background' => '#1BBC9B', 'title' => __("I'm interested"), 'href' => $confirmLink)); ?>
<br />
<br />
Si tu n'es pas intéressé par ce poste mais que tu restes disponible pour d'autres postes, merci de cliquer sur ce bouton:
<br />
<br />
<?php echo $this->element('Emails/button', array('background' => '#E87E04', 'title' => __("I'm not interested"), 'href' => $notInterestedLink)); ?><br />
<br />
Si tu n'es pas disponible à cette date, merci de cliquer sur ce bouton:
<br />
<br />
<?php echo $this->element('Emails/button', array('background' => '#E7505A', 'title' => __("I'm not available"), 'href' => $notAvailableLink)); ?>
<br />
<br />
Toute l'équipe d'Une-bonne-idée.ch te souhaite une belle journée.<br />
<strong><?php echo $senderFirstName; ?></strong>
</p>
<p></p>
<p>*** Ce mail a été envoyé automatiquement. ***</p>
