<div class="row">
  <div class="col-xs-12 text-center">
    <h1><?php echo $event['Client']['name']; ?></h1>
    <h3 style="border: 0"><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A %d %B %Y'); ?></h3>
  </div>
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr>
        <th>Arrivée dépôt</th>
        <td>
          <?php if(!empty($depotArrival)): ?>
            <?php echo $this->Time->format('H:i', $depotArrival['PlanningBoardMoment']['start']); ?>
          <?php else: ?>
            Non spécifié
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <th>Départ dépôt</th>
        <td>
          <?php if(!empty($depotLeaving)): ?>
            <?php echo $this->Time->format('H:i', $depotLeaving['PlanningBoardMoment']['start']); ?>
          <?php else: ?>
            Non spécifié
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <th>Event manager</th>
        <td>
          <?php if(!empty($emdo)): ?>
            <?php echo $emdo['User']['full_name']; ?>
          <?php else: ?>
            Non spécifié
          <?php endif; ?>
        </td>
      </tr>
      <tr>
        <th>Event coordinator</th>
        <td><?php echo $event['PersonInCharge']['full_name']; ?></td>
      </tr>
    </table>
  </div>
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr>
        <th>Préparé par</th>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th>Date</th>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr><th colspan="2">Commandes à recevoir</th></tr>
      <?php foreach($suppliers as $supplier): if(empty($supplier['orders']) || is_null($supplier['Supplier']['id']) || $supplier['Supplier']['id'] == 3195){continue;} ?>
      <tr>
        <td style="width: 70%"><?php echo $supplier['Supplier']['name']; ?></td>
        <td><input type="text" name="" value="" style="width: 28px; height: 20px; border: 1px solid #666;"></td>
      </tr>
      <?php endforeach; ?>
      <tr>
        <td>Autre</td>
        <td><input type="text" name="" value="" style="width: 28px; height: 20px; border: 1px solid #666;"></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </div>
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr><th colspan="2">Manque F&amp;B</th></tr>
      <?php for($i=0;$i<6;$i++): ?><tr><th>&nbsp;</th><td>&nbsp;</td></tr><?php endfor; ?>     
    </table>
  </div>
</div>
<div class="row">
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr><th colspan="2">Manque Activités</th></tr>
      <?php for($i=0;$i<6;$i++): ?><tr><th>&nbsp;</th><td>&nbsp;</td></tr><?php endfor; ?>     
    </table>
  </div>
  <div class="col-xs-6">
    <table class="bg-grey" style="width: 100%">
      <tr><th colspan="2">Manque Logistique/autres</th></tr>
      <?php for($i=0;$i<6;$i++): ?><tr><th>&nbsp;</th><td>&nbsp;</td></tr><?php endfor; ?>     
    </table>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <table class="bg-grey" style="width: 100%">
      <tr><th colspan="2">Remarques pour l'Event Manager</th></tr>
      <?php for($i=0;$i<10;$i++): ?><tr><th>&nbsp;</th><td>&nbsp;</td></tr><?php endfor; ?>     
    </table>
  </div>
</div>