<div class="row">
  <div class="col-xs-12">
    <?php foreach($orders as $order): ?>
      <?php foreach($order['OrderGroup'] as $k => $group): if(empty($group['OrderItem'])){continue;} ?>
        <table class="table table-striped table-orders">

          <thead>
            <tr>
              <th class="check"><?php if($k == 0): ?>Check dépôt<?php endif; ?></th>
              <th class="quantity"><?php if($k == 0): ?>Qté<?php endif; ?></th>
              <th class="name"><?php if($k == 0): ?>Nom<?php endif; ?></th>
              <?php if($show_depot): ?><th class="supplier"><?php if($k == 0): ?>Depot<?php endif; ?></th><?php endif; ?>
              <th class="remarks"><?php if($k == 0): ?>Commentaires<?php endif; ?></th>
              <?php if($showAlley): ?><th class="alley">&nbsp;</th><?php endif; ?>
            </tr>
          </thead>

          <tbody>
            <tr class="title bold uppercase">
              <td colspan="7" class="bg-grey"><?php echo empty($order['Order']['name']) ? $order['name'] : $order['Order']['name']; ?> - <?php echo $group['name']; ?></td>
            </tr>
            <?php foreach($group['OrderItem'] as $item): ?>
              <?php
              $quantity = $item['quantity'];
              $number_of_containers = $item['number_of_containers'];
              $option = $item['quantity_option'];
              $unit = $item['quantity_unit'];
              if($quantity == floor($quantity) && is_numeric($quantity)){
                $quantity = intval($quantity);
              }
              if($option  == 'quantity_per_person' && !empty($unit) && empty($number_of_containers)){
                if($quantity >= 1000 && $unit == 'g'){
                  $unit = 'kg';
                  $quantity = $quantity / 1000;
                }
                if($quantity > 100 && $unit == 'cl'){
                  $unit = 'l';
                  $quantity = $quantity / 100;
                }
              }
              if(!empty($number_of_containers)){
                $unit = '';
              }
              ?>
              <tr>
                <td><i class="fa fa-square-o"></i></td>
                <td>
                  <?php if(!empty($item['number_of_containers'])): ?>
                    <?php echo $item['number_of_containers']; ?>
                  <?php else: ?>
                    <?php echo $quantity; ?>
                  <?php endif; ?>
                  <?php echo $unit; ?>
                </td>
                <td>
                  <?php echo !empty($item['StockItem']) ? $item['StockItem']['code_name'] : $item['name']; ?>
                  <?php if($item['quantity_option'] == 'pieces_per_person_per_team' && !empty($event['SelectedActivity'][0]['number_of_teams']) && !empty($event['SelectedActivity'][0]['number_of_persons_per_team'])): ?>
                    <em>
                      <?php echo __('%s batches of %s', $event['SelectedActivity'][0]['number_of_teams'], $event['SelectedActivity'][0]['number_of_persons_per_team']); ?>
                    </em>
                  <?php endif; ?>
                </td>
                <?php if($show_depot): ?>
                <td><?php echo !empty($item['StockItem']['StockLocation']) ? $item['StockItem']['StockLocation']['name'] : ''; ?></td>
                <?php endif; ?>
                <td><small><?php echo $item['remarks']; ?></small></td>
                <?php if($showAlley): ?>
                  <td class="text-right">
                    <?php if(!empty($item['StockItem']['alley'])): ?>
                      <?php echo $item['StockItem']['alley']; ?>
                    <?php endif; ?>
                    <?php if(!empty($item['StockItem']['position'])): ?>
                       - <?php echo $item['StockItem']['position']; ?>
                    <?php endif; ?>
                  </td>
                <?php endif; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php endforeach; ?>
    <?php endforeach; ?>
  </div>
</div>
