<div class="row" style="margin-bottom: 20px">
	<div class="col-xs-3 logo">
		<?php echo $this->Html->image(DS . $event['Company']['Logo']['url']); ?>
	</div>
	<div class="col-xs-5">
		<table class="table table-condensed table-infos">
			<tr>
				<td class="bold">Nom</td>
				<td><?php echo $event['Event']['code_name'];?></td>
			</tr>
			<tr>
				<td class="bold">Client</td>
				<td><?php echo $event['Client']['name'];?></td>
			</tr>
			<tr>
				<td class="bold">Date</td>
				<td><?php echo $this->Time->format($event['Event']['confirmed_date'], '%A %d %B %Y');?></td>
			</tr>
			<tr>
				<td class="bold">Coordinateur</td>
				<td><?php echo $event['PersonInCharge']['full_name'];?></td>
			</tr>
			<tr>
				<td class="bold">Lieu</td>
				<td><?php echo !empty($event['ConfirmedEventPlace']) ? $event['ConfirmedEventPlace'][0]['Place']['name'] : __('No place defined'); ?></td>
			</tr>
		</table>
	</div>
	<div class="col-xs-4">
		<table class="table table-condensed table-infos">
			<tr>
				<td class="bold">PAX</td>
				<td class="text-right">
					<?php if(!empty($event['SelectedActivity'][0]['number_of_persons'])):?>
						<?php echo $event['SelectedActivity'][0]['number_of_persons']; ?>
					<?php elseif(!empty($event['Event']['confirmed_number_of_persons']) && empty($event['SelectedActivity']['number_of_persons'])):?>
						<?php echo $event['Event']['confirmed_number_of_persons']; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="bold">Nombre d'animateurs</td>
				<td class="text-right"><?php echo empty($event['SelectedActivity'][0]['number_of_animators']) ? 0 : $event['SelectedActivity'][0]['number_of_animators'];?></td>
			</tr>
			<tr>
				<td class="bold">Nombre d'équipes</td>
				<td class="text-right"><?php echo empty($event['SelectedActivity'][0]['number_of_teams']) ? 0 : $event['SelectedActivity'][0]['number_of_teams'];?></td>
			</tr>
			<tr>
				<td class="bold">Nombre de groupes</td>
				<td class="text-right"><?php echo empty($event['SelectedActivity'][0]['number_of_groups']) ? 0 : $event['SelectedActivity'][0]['number_of_groups'];?></td>
			</tr>
			<tr>
				<td class="bold">Pers. / Equipe</td>
				<td class="text-right"><?php echo empty($event['SelectedActivity'][0]['number_of_persons_per_team']) ? 0 : $event['SelectedActivity'][0]['number_of_persons_per_team'];?></td>
			</tr>
		</table>
	</div>
  <?php if($supplier['Supplier']['id'] == 3855 && in_array($event['Event']['company_id'], array(2,4))): ?>
  <div class="row">
    <div class="col-xs-12 text-center" style="font-size: 20px">
      <strong class="uppercase">No de client: </strong>
      <?php if($event['Event']['company_id'] == 2): ?>561<?php endif; ?>
      <?php if($event['Event']['company_id'] == 4): ?>564<?php endif; ?>
      <br><br>
    </div>
  </div>
  <?php endif; ?>
	<div class="col-xs-12">
		<h3>
			<?php if(!empty($mode) && $mode == 'locations'): ?>
				<?php echo __('Preparation sheet - %s', $location['StockLocation']['name']); ?>
			<?php elseif(!empty($mode) && $mode == 'suppliers'): ?>
				<?php echo __('Supplier sheet - %s', $supplier['Supplier']['name']); ?>
			<?php else: ?>
				<?php echo __('Control sheet'); ?>
			<?php endif; ?>
		</h3>
	</div>
</div>
