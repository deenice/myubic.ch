<div class="panel panel-default">
	<div class="panel-heading panel-heading-width-<?php echo $batch['ChecklistBatch']['progress']; ?>">
		<h4 class="panel-title">
			<a class="accordion-toggle accordion-toggle-styled <?php echo $classes['panel-title']; ?>" data-context="#context_<?php echo $batch['ChecklistBatch']['id']; ?>" data-toggle="collapse" data-parent="#accordiontasks" href="#accordionbatch<?php echo $batch['ChecklistBatch']['id']; ?>">
				<i class="fa fa-check-circle font-green-haze complete-batch complete-batch--<?php echo $batch['ChecklistBatch']['progress'] == 100 ? 'visible':'hidden'; ?>"></i>&nbsp;&nbsp;
				<?php echo $batch['ChecklistBatch']['name']; ?>
				<span class="closed_tasks"><?php echo empty($batch['ChecklistBatch']['closed_tasks']) ? 0 : $batch['ChecklistBatch']['closed_tasks']; ?></span> /
				<span class="total_tasks"><?php echo empty($batch['ChecklistBatch']['total_tasks']) ? 0 : $batch['ChecklistBatch']['total_tasks']; ?></span>
				<?php if($batch['ChecklistBatch']['progress'] < 100): ?>
				<span class="calendar-context-toggle font-grey-cascade" data-toggle="context" data-target="#context_<?php echo $batch['ChecklistBatch']['id']; ?>">
					<i class="fa fa-calendar"></i>
				</span>
				<?php endif; ?>
			</a>
		</h4>
		<div class="checklist-batch-context" id="context_<?php echo $batch['ChecklistBatch']['id']; ?>">
			<ul class="dropdown-menu">
				<li>
					<a href="javascript:;"><i class="fa fa-calendar-o"></i> <?php echo __('Due date'); ?></a>
					<div class="due-date-picker" data-checklist-batch-id="<?php echo $batch['ChecklistBatch']['id']; ?>"> </div>
				</li>
			</ul>
		</div>
	</div>
	<div id="accordionbatch<?php echo $batch['ChecklistBatch']['id']; ?>" class="panel-collapse <?php echo $classes['panel-collapse']; ?>">
		<div class="panel-body tasks-widget">
			<ul class="task-list">
				<?php foreach($batch['ChecklistTask'] as $task): ?>
					<li data-checklist-task-id="<?php echo $task['id']; ?>" data-checklist-batch-id="<?php echo $batch['ChecklistBatch']['id']; ?>" class="sortable" data-toggle="context" data-target="#task-actions<?php echo $task['id']; ?>">
						<div class="task-checkbox">
							<?php echo $this->Form->input('task', array('type' => 'checkbox', 'label' => false, 'div' => false, 'value' => $task['done'], 'checked' => $task['done'], 'id' => '#task'.$task['id'])); ?>
						</div>
						<div class="task-title">
							<span class="task-title-sp<?php echo $task['done'] ? ' task-done':''; ?>">
								<?php echo $this->Html->link($task['name'], '#', array('class' => $task['done'] ? '' : 'editable', 'data-pk' => $task['id'], 'data-name' => 'name')); ?>
								<?php if(!empty($task['due_date'])): ?>
								<span class="pull-right">
								<?php if($task['due_date'] < date('Y-m-d')): ?>
									<i class="fa fa-warning text-warning"></i>
								<?php endif; ?>
								<?php echo $this->Time->format('d.m.Y', $task['due_date']); ?>
								</span>
								<?php endif; ?>
							</span>
						</div>
						<div id="task-actions<?php echo $task['id']; ?>">
						  <ul class="dropdown-menu">
						    <li><a href="<?php echo Router::url(array('controller' => 'checklist_tasks', 'action' => 'delete', $task['id'])); ?>" class="delete-task" data-checklist-batch-id="<?php echo $task['checklist_batch_id']; ?>"><i class="fa fa-times font-red"></i> <?php echo __('Delete'); ?></a></li>
								<li class="divider"></li>
								<li>
									<a href="javascript:;"><i class="fa fa-calendar-o"></i> <?php echo __('Due date'); ?></a>
									<div class="due-date-picker" data-date1="<?php echo empty($task['due_date']) ? '' : $this->Time->format('d.m.Y', $task['due_date']); ?>" data-checklist-task-id="<?php echo $task['id']; ?>" data-checklist-batch-id="<?php echo $batch['ChecklistBatch']['id']; ?>"> </div>
								</li>
							</ul>
						</div>
					</li>
				<?php endforeach; ?>
				<li class="new-task">
					<div class="task-checkbox">
						<input type="checkbox" name="name" value="">
					</div>
					<div class="task-title">
						<span class="task-title-sp">
							<span class="new-task-input"><input type="text" placeholder="<?php echo __('New task'); ?>" name="name" value="" class="form-control input-sm" data-checklist-batch-id="<?php echo $batch['ChecklistBatch']['id']; ?>"></span>
						</span>
					</div>
				</li>
			</ul>
			<div class="text-right" style="margin-top: 10px">
				<?php echo $this->Html->link(sprintf('<i class="fa fa-trash-o"></i> %s', __('Delete group')), array('controller' => 'checklist_batches', 'action' => 'delete', $batch['ChecklistBatch']['id']), array('class' => 'btn btn-xs btn-danger delete-batch', 'escape' => false)); ?>
			</div>
		</div>
	</div>
</div>
