<hr>
<div class="row">
  <div class="col-md-12 text-right">
    <?php if($notes): ?>
      <a href="<?php echo Router::url(array('controller' => 'notes', 'action' => 'modal', 'list', 'Event', $id, $category)); ?>" data-toggle="modal" data-target="#modal-note-list" class="btn btn-sm dark">
        <i class="fa fa-list"></i> <?php echo __('All notes');?>
      </a>
    <?php endif; ?>
    <a href="#" class="btn btn-xs btn-default" data-toggle="modal" data-target="#new-note" data-model="<?php echo $model;?>" data-model-id="<?php echo $id;?>" data-category="<?php echo $category;?>"><i class="fa fa-clipboard"></i> <?php echo __('Add a note');?></a>
  </div>
</div>
