<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a class="accordion-toggle accordion-toggle-styled <?php echo $classes['panel-title']; ?>" data-toggle="collapse" data-parent="#accordionorders" href="#accordionorder<?php echo $group['OrderGroup']['id']; ?>">
				<?php echo $group['OrderGroup']['name']; ?>
			</a>
		</h4>
	</div>
	<div id="accordionorder<?php echo $group['OrderGroup']['id']; ?>" class="panel-collapse <?php echo $classes['panel-collapse']; ?>">
		<div class="panel-body">
			<table class="table table-striped table-order-items table-order-items--portlet">
				<thead>
					<tr>
						<th><?php echo __('Quantity'); ?></th>
						<th><?php echo __('Name'); ?></th>
						<th class="actions"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($group['OrderItem'] as $item): ?>
						<tr class="<?php echo $item['quantity_option']; ?>">
							<td class="quantity">
								<?php if(!empty($item['number_of_containers'])): ?>
									<?php echo $this->Form->input('number_of_containers', array('label' => false, 'div' => false, 'value' => $item['number_of_containers'], 'class' => 'form-control input-inline input-sm input-xsmall', 'data-href' => Router::url(array('controller' => 'order_items', 'action' => 'updateField')), 'data-order-item-id' => $item['id'], 'data-field' => 'number_of_containers')); ?>
									<small>(<?php echo (is_numeric($item['quantity']) && floor($item['quantity']) != $item['quantity']) ? $item['quantity'] : number_format($item['quantity'],0); ?><?php echo $item['quantity_unit']; ?>)</small>
								<?php else: ?>
									<?php echo $this->Form->input('quantity', array('label' => false, 'div' => false, 'value' => (is_numeric($item['quantity']) && floor($item['quantity']) != $item['quantity']) ? $item['quantity'] : number_format($item['quantity'],0), 'class' => 'form-control input-inline input-sm input-xsmall', 'data-href' => Router::url(array('controller' => 'order_items', 'action' => 'updateField')), 'data-order-item-id' => $item['id'], 'data-field' => 'quantity')); ?>
									<?php echo $item['quantity_unit']; ?>
								<?php endif; ?>
							</td>
							<td class="name">
								<?php if(!empty($item['StockItem'])): ?>
									<?php echo $item['StockItem']['code_name']; ?>
								<?php else: ?>
									<?php echo $item['name']; ?>
								<?php endif; ?>
								<?php if(!empty($item['remarks'])): ?>
									<br><small><?php echo $item['remarks']; ?></small>
								<?php endif; ?>
							</td>
							<td>
								<?php echo $this->Html->link('<i class="fa fa-times font-red"></i>', array('controller' => 'order_items', 'action' => 'delete', $item['id']), array('class' => 'delete-order-item', 'escape' => false)); ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
