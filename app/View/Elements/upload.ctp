<?php echo $this->Form->create('Document', array('id' => $id, 'type' => 'file', 'url' => array('action' => 'uploadFiles'))); ?>
<?php echo $this->Form->input($foreign); ?>
<?php echo $this->Form->input('Document.category', array('type' => 'hidden', 'value' => $category)); ?>
	<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
	<div class="row fileupload-buttonbar">
		<div class="col-lg-7">
			<!-- The fileinput-button span is used to style the file input field as button -->
			<span class="btn green fileinput-button">
				<i class="fa fa-plus"></i>
				<span><?php echo __("Add files"); ?></span>
				<?php echo $this->Form->input('files.', array('type' => 'file', 'multiple', 'div' => false, 'label' => false));?>
			</span>
			<button type="submit" class="btn blue start" name="destination" value="<?php echo $destination; ?>">
				<i class="fa fa-upload"></i>
				<span><?php echo __("Upload files"); ?></span>
			</button>
		</div>
		<!-- The global progress information -->
		<div class="col-lg-5 fileupload-progress fade">
			<!-- The global progress bar -->
			<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
				<div class="progress-bar progress-bar-success" style="width:0%;">
				</div>
			</div>
			<!-- The extended global progress information -->
			<div class="progress-extended">
				 &nbsp;
			</div>
		</div>
	</div>
	<div id="<?php echo $id; ?>_documents"></div>
<?php echo $this->Form->end(); ?>
<?php
$this->start('page_level_styles');
echo $this->Html->css('/metronic/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css');
echo $this->Html->css('/metronic/global/plugins/jquery-file-upload/css/jquery.fileupload.css');
echo $this->Html->css('/metronic/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css');
$this->end();
$this->start('page_level_plugins');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js');
//echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/vendor/load-image.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js');
echo $this->Html->script('/metronic/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js');
$this->end();
$this->start('page_level_scripts');
echo $this->Html->script('/metronic/pages/scripts/form-fileupload.js');
echo $this->Html->script('/metronic/pages/scripts/custom.js');
$this->end();
$this->start('init_scripts');
//echo 'FormFileUpload.init();';
echo 'Custom.uploadForm("#'.$id.'");';
$this->end();
?>

