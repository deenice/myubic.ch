<a href="#" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modal<?php echo $id; ?>"><i class="fa fa-info-circle"></i> <?php echo __('Remarks'); ?></a>

<div class="modal" id="modal<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id=""><?php echo $title; ?></h4>
      </div>
      <div class="modal-body">
        <?php echo nl2br($remarks); ?>
      </div>
    </div>
  </div>
</div>
