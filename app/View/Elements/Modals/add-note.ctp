<div class="modal" id="new-note">
	<?php echo $this->Form->create('Note', array('class' => 'form-horizontal', 'url' => array('controller' => 'notes', 'action' => 'add'))); ?>
	<?php echo $this->Form->input('Note.model', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('Note.model_id', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('Note.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
	<?php echo $this->Form->input('Note.date', array('type' => 'hidden', 'value' => date('Y-m-d'))); ?>
	<?php echo $this->Form->input('Note.id', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('Note.category', array('type' => 'hidden', 'value' => '')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title new"><?php echo __('New note'); ?></h4>
				<h4 class="modal-title edit"><?php echo __('Edit note'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Title'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('title', array('label' => false, 'class' => 'form-control'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Message'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('message', array('label' => false, 'class' => 'form-control', 'type' => 'textarea'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Level'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('level', array('label' => false, 'class' => 'form-control bs-select', 'options' => array('normal' => __('Normal'), 'high' => __('High'))));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Sticky'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('sticky', array('label' => false, 'class' => 'form-control make-switch'));?>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
				<button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
