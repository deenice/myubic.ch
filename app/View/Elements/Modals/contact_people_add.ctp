<div class="modal fade" id="new-contact-people" tabindex="-1" aria-hidden="true">
	<?php echo $this->Form->create('ContactPeople', array('controller' => 'contact_peoples', 'action' => 'add')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo __('Add a new contact person'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label"><?php echo __('First name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.first_name', array('class' => 'form-control', 'label' => false)); ?>
							<?php echo $this->Form->input('ContactPeople.client_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden')); ?>
							<?php echo $this->Form->input('ContactPeople.status', array('value' => 1, 'type' => 'hidden')); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Last name'); ?></label>
							<?php echo $this->Form->input('ContactPeople.last_name', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Civility'); ?></label>
							<?php echo $this->Form->input('ContactPeople.civility', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.civilities'))); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Email'); ?></label>
							<?php echo $this->Form->input('ContactPeople.email', array('type' => 'email', 'class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Phone'); ?></label>
							<?php echo $this->Form->input('ContactPeople.phone', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Function'); ?></label>
							<?php echo $this->Form->input('ContactPeople.function', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Department'); ?></label>
							<?php echo $this->Form->input('ContactPeople.department', array('class' => 'form-control', 'label' => false)); ?>
						</div>
						<div class="form-group">
							<label class="control-label"><?php echo __('Language'); ?></label>
							<?php echo $this->Form->input('ContactPeople.language', array('class' => 'form-control bs-select', 'label' => false, 'options' => Configure::read('ContactPeople.languages'))); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
				<button type="submit" class="btn blue"><?php echo __('Save'); ?></button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<?php echo $this->Form->end(); ?>
	<!-- /.modal-dialog -->
</div>
