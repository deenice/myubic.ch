<?php global $COMPANIES; ?>
<div class="modal" id="modal-ask-vehicle">
	<?php echo $this->Form->create('Event', array('class' => 'form-horizontal', 'url' => array('controller' => 'events', 'action' => 'add_reservation'))); ?>
	<?php echo $this->Form->input('Event.type', array('value' => 'mission', 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('Event.opening_date', array('value' => date('Y-m-d'), 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('Event.user_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
	<?php echo $this->Form->input('Event.resp_id', array('type' => 'hidden', 'value' => AuthComponent::user('id'))); ?>
	<?php echo $this->Form->input('Event.crm_status', array('type' => 'hidden', 'value' => 'confirmed')); ?>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo __('Ask for a vehicle/trailer'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Reason'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Event.name', array('label' => false, 'class' => 'form-control', 'required' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Company'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Event.company_id', array('label' => false, 'class' => 'form-control bs-select', 'options' => $COMPANIES));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Date'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Event.confirmed_date', array('label' => false, 'class' => 'form-control date-picker', 'type' => 'text', 'required' => true));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Schedule'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('Event.start_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small input-inline timepicker-24'));?>
						<span class="help-inline"><?php echo __('to'); ?></span>
						<?php echo $this->Form->input('Event.end_hour', array('type' => 'text', 'label' => false, 'div' => false, 'class' => 'form-control input-small input-inline timepicker-24'));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Family'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('VehicleReservation.family', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('VehiclesTrailers.families')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Start place'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('VehicleReservation.start_place', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('VehicleTours.locations')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('End place'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('VehicleReservation.end_place', array('label' => false, 'class' => 'form-control bs-select', 'options' => Configure::read('VehicleTours.locations')));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Driver forth'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('VehicleReservation.driver_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'data-live-search', 'options' => $drivers));?>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3"><?php echo __('Driver back'); ?></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('VehicleReservation.back_driver_id', array('label' => false, 'class' => 'form-control bs-select', 'empty' => true, 'data-live-search', 'options' => $drivers));?>
					</div>
				</div>
				<div class="alert alert-info">
					<p><i class="fa fa-info-circle"></i> Une mission et une demande de réservation vont être automatiquement créées.</p>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close'); ?></button>
				<button type="submit" class="btn btn-primary"><?php echo __('Save'); ?></button>
			</div>
		</div>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
