<?php
$id = '';
$title = '';
$description = '';
$liClasses = array();
$stockOrderId = '';
$value = '';
switch($type){
		case 'start':
			$liClasses = array('start');
			$labelClass = 'default start-point';
			$icon = 'fa-home';
			$spinner = false;
			$title = __('Start');
			$value = $tour['VehicleTour']['start_location'];
			if($tour['VehicleTour']['start_location'] == 'other'){
				$value .= '|' . $tour['VehicleTour']['start_location_custom'];
				$value .= '|' . $tour['VehicleTour']['start_address'];
				$value .= '|' . $tour['VehicleTour']['start'];
			}
		break;
		case 'end':
			$liClasses = array('end');
			$labelClass = 'default end-point';
			$icon = 'fa-home';
			$title = __('End');
			$spinner = false;
			$value = $tour['VehicleTour']['end_location'];
			if($tour['VehicleTour']['end_location'] == 'other'){
				$value .= '|' . $tour['VehicleTour']['end_location_custom'];
				$value .= '|' . $tour['VehicleTour']['end_address'];
			}
		break;
		case 'delivery':
			$liClasses = array('order', 'delivery', 'sortable');
			$labelClass = 'primary';
			$icon = 'fa-arrow-right';
			$title = $pdf == true ? __('Delivery') : $this->Html->link(__('Delivery'), array('controller' => 'stock_orders', 'action' => 'invoice', $moment['VehicleTourMoment']['stock_order_id'] . '.pdf', '?' => 'download=0'), array('target' => '_blank'));
			$spinner = true;
			$stockOrderId = $moment['VehicleTourMoment']['stock_order_id'];
		break;
		case 'return':
			$liClasses = array('order', 'return', 'sortable');
			$labelClass = 'info';
			$icon = 'fa-arrow-left';
			$title = $pdf == true ? __('Return') : $this->Html->link(__('Return'), array('controller' => 'stock_orders', 'action' => 'invoice', $moment['VehicleTourMoment']['stock_order_id'] . '.pdf', '?' => 'download=0'), array('target' => '_blank'));
			$spinner = true;
			$stockOrderId = $moment['VehicleTourMoment']['stock_order_id'];
		break;
		case 'route':
			$liClasses = array('route', 'sortable');
			$labelClass = 'warning';
			$icon = 'fa-truck';
			$title = __('Route');
			$spinner = true;
		break;
		case 'loading':
			$liClasses = array('loading', 'sortable', 'editable');
			$labelClass = 'success';
			$icon = 'fa-exchange';
			$title = __('Loading');
			$spinner = true;
			$value = empty($moment['VehicleTourMoment']['duration']) ? '' : $moment['VehicleTourMoment']['duration'];
		break;
		case 'unloading':
			$liClasses = array('unloading', 'sortable', 'editable');
			$labelClass = 'success';
			$icon = 'fa-exchange';
			$title = __('Unloading');
			$spinner = true;
			$value = empty($moment['VehicleTourMoment']['duration']) ? '' : $moment['VehicleTourMoment']['duration'];
		break;
		case 'break':
			$liClasses = array('break', 'sortable', 'editable');
			$labelClass = 'danger';
			$icon = 'fa-coffee';
			$title = __('Break');
			$spinner = true;
			$value = empty($moment['VehicleTourMoment']['duration']) ? '' : $moment['VehicleTourMoment']['duration'];
		break;
		case 'free':
			$liClasses = array('free', 'sortable', 'editable');
			$labelClass = 'default';
			$icon = 'fa-cog';
			$title = $moment['VehicleTourMoment']['description'];
			$description = $moment['VehicleTourMoment']['description'];
			$moment['VehicleTourMoment']['description'] = __('%s minutes', $moment['VehicleTourMoment']['duration']);
			$spinner = true;
			$value = empty($moment['VehicleTourMoment']['duration']) ? '' : $moment['VehicleTourMoment']['duration'];
		break;
		default:
		break;
	}
	if(!empty($empty) AND $empty) {
		if($type == 'break'){
			$id = 'empty_break';
		}
		if($type == 'loading'){
			$id = 'empty_loading';
		}
		array_push($liClasses, 'empty');
		array_push($liClasses, 'hidden');
	}
?>
<li class="<?php echo implode(' ', $liClasses); ?>" id="<?php echo $id; ?>" data-stockorder-id="<?php echo $stockOrderId; ?>" data-vehicle-tour-moment-id="<?php echo empty($moment) ? '' : $moment['VehicleTourMoment']['id']; ?>" data-type="<?php echo $type; ?>" data-value="<?php echo $value; ?>" data-description="<?php echo $description; ?>">
	<div class="col1">
		<div class="cont">
			<div class="cont-col1">
				<div class="label label-<?php echo $labelClass; ?> label-sm">
					<i class="fa <?php echo $icon; ?>"></i>
				</div>
			</div>
			<div class="cont-col2">
				<div class="desc">
					<span class="uppercase bold"><?php echo $title; ?></span>
					<span class="infos"><?php echo $moment['VehicleTourMoment']['description']; ?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="col2">
		<div class="date"><?php echo empty($moment['VehicleTourMoment']['hour']) ? '' : $this->Time->format($moment['VehicleTourMoment']['hour'], '%H:%M'); ?></div>
	</div>
</li>
