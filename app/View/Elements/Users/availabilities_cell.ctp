<?php if($item['availability'] == 'yes') $class = array('success'); ?>
<?php if($item['availability'] == 'no') $class = array('danger'); ?>
<?php if($item['availability'] == 'maybe') $class = array('warning'); ?>
<?php if($item['availability'] == 'unknown') $class = array('bg-grey'); ?>
<td class="<?php echo implode(',', $class); ?> availability-cell" data-id="<?php echo $item['id']; ?>" data-availability="<?php echo $item['availability']; ?>">
  <div class="drag drag-left"></div>
  <?php echo sprintf("%02d:00", $item['hour']); ?>
  <div class="drag drag-right"></div>
</td>
