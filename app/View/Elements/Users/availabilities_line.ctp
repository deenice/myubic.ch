<?php $days = Configure::read('Availabilities.days'); ?>

<tr data-day="<?php echo $day; ?>">
  <td rowspan="2" style="vertical-align: middle">
    <strong><?php echo $days[$day]; ?></strong><br />
  </td>
  <?php foreach($data as $k => $item): if($item['hour'] < 6 || $item['hour'] > 17) continue; unset($data[$k]); ?>
  <?php echo $this->element('Users/availabilities_cell', array('item' => $item)); ?>
  <?php endforeach; ?>
</tr>

<tr data-day="<?php echo $day; ?>">
  <?php foreach($data as $k => $item): ?>
  <?php echo $this->element('Users/availabilities_cell', array('item' => $item)); ?>
  <?php endforeach; ?>
</tr>

<tr class="actions" data-day="<?php echo $day; ?>">
  <td colspan="13" class="text-left">
    <div class="btn-group btn-group-sm options hidden">
      <button type="button" class="btn btn-success" data-day="<?php echo $day; ?>" data-availability="yes"><?php echo __("I'm available"); ?></button>
      <button type="button" class="btn btn-warning" data-day="<?php echo $day; ?>" data-availability="maybe"><?php echo __("I'm maybe available"); ?></button>
      <button type="button" class="btn btn-danger" data-day="<?php echo $day; ?>" data-availability="no"><?php echo __("I'm not available"); ?></button>
      <button type="button" class="btn btn-default" data-day="<?php echo $day; ?>" data-availability="unknown"><?php echo __("I don't know"); ?></button>
    </div>
    <div class="btn-group btn-group-sm options hidden pull-right">
      <button type="button" class="btn btn-default whole-day" data-day="<?php echo $day; ?>"><?php echo __("Select whole day"); ?></button>
    </div>
  </td>
</tr>
