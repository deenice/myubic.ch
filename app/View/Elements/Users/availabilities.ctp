<table class="table text-center table-condensed table-ligh1t table-bordered table-availabilities">
  <tbody>
    <?php echo $this->element('Users/availabilities_line', array('day' => 1, 'data' => $user['MondayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 2, 'data' => $user['TuesdayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 3, 'data' => $user['WednesdayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 4, 'data' => $user['ThursdayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 5, 'data' => $user['FridayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 6, 'data' => $user['SaturdayAvailability'])); ?>
    <?php echo $this->element('Users/availabilities_line', array('day' => 7, 'data' => $user['SundayAvailability'])); ?>
  </tbody>
</table>

<table class="table table-condensed table-bordered">
  <tbody>
    <tr>
      <td class="success" style="width: 30px"></td>
      <td><?php echo __("I'm available"); ?></td>
      <td class="warning" style="width: 30px"></td>
      <td><?php echo __("I'm maybe available"); ?></td>
      <td class="danger" style="width: 30px"></td>
      <td><?php echo __("I'm not available"); ?></td>
      <td class="bg-grey" style="width: 30px"></td>
      <td><?php echo __("I don't know"); ?></td>
    </tr>
  </tbody>
</table>
