<?php
$color = empty($color) ? '#fff' : $color;
$background = empty($background) ? '#ccc' : $background;
$href = empty($href) ? '#' : $href;
$width = empty($width) ? 200 : $width;
$height = empty($height) ? 30 : $height;
$size = empty($size) ? 13 : $size;
?>
<div><!--[if mso]>
  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="<?php echo $href;?>" style="height:<?php echo $height;?>px;v-text-anchor:middle;width:<?php echo $width;?>px;" stroke="f" fillcolor="<?php echo $background;?>">
    <w:anchorlock/>
    <center>
  <![endif]-->
      <a href="<?php echo $href;?>"
style="background-color:<?php echo $background;?>;color:<?php echo $color;?>;display:inline-block;font-family:sans-serif;font-size:<?php echo $size;?>px;font-weight:bold;line-height:<?php echo $height;?>px;text-align:center;text-decoration:none;width:<?php echo $width;?>px;-webkit-text-size-adjust:none;"><?php echo $title;?></a>
  <!--[if mso]>
    </center>
  </v:rect>
<![endif]--></div>
