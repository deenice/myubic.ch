<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher',
    'MaintenanceMode.MaintenanceMode'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

CakePlugin::load('Tools');
CakePlugin::load('DebugKit');
CakePlugin::load('MaintenanceMode');
CakePlugin::load('CakePdf', array('bootstrap' => true, 'routes' => true));
CakePlugin::load('Utility', array('bootstrap' => true, 'routes' => true));
//CakePlugin::load('AclExtras');
CakePlugin::load('MenuBuilder');
CakePlugin::load('Math');
CakePlugin::load('Icing');
CakePlugin::load('ClearCache');
CakePlugin::load('DataTable');
//CakePlugin::load('Users', array('routes' => false));

Configure::write('Session', array(
    'timeout' => 3000,
    'cookie' => 'myubic'
));

/**
 * My custom global variables
 */
Configure::write('Metronic.version', 3);
Configure::write('Config.language', 'fra');
Configure::write('DocumentManager.baseDir', 'files');
if($_SERVER["SERVER_ADDR"] == "::1"){
	Configure::write('GoogleMapsAPIKey', '');
} else {
	//Configure::write('GoogleMapsAPIKey', 'AIzaSyD-WzY4uDkXuE8igMiVDtbnWV-PlI7Sqws');
	Configure::write('GoogleMapsAPIKey', 'AIzaSyBOshFIvFWwaZ30AFTpSAxiouBxk-Xqr4Q');
}
Configure::write('Models.names', array(
	'Client' => __('Client'),
	'ContactPeople' => __('Contact person'),
	'StockOrder' => __('Stock order'),
	'Event' => __('Event'),
));
Configure::write('Companies.acronyms', array(
	'ubic' => __('Une-Bonne-Idée.ch'),
	'ug' => __('Urban Gaming'),
	'eg' => __('Effet Gourmand'),
	'mg' => __('Maier Grill'),
	'fl' => __('Festiloc'),
	'festiloc' => __('Festiloc')
));
Configure::write('Places.types', array(
	'vault' => __('Vault'),
	'convention_hall' => __('Convention hall'),
	'refuge' => __('Refuge'),
	'castle' => __('Castle'),
	'chalet' => __('Chalet'),
	'couvert' => __('Couvert'),
	'soccer_refreshment_room' => __('Soccer refreshment room'),
	'square' => __('Square'),
	'shooting_range' => __('Shooting range'),
	'room' => __('Room'),
	'hotel_restaurant' => __('Restaurant/Hotel'),
	'club_concert_hall' => __('Club - Concert hall'),
	'theater' => __('Theater'),
	'public_place' => __('Public place'),
	'museum' => __('Museum'),
	'camp' => __('Camp'),
	'mobile' => __('Mobile'),
	'school' => __('School'),
	'farm' => __('Farm'),
	'flat' => __('Flat'),
	'individual_home' => __('Individual Home'),
  'meadow' => __('Meadow'),
	'company' => __('Company'),
	'other' => __('Other')
));
Configure::write('Places.degrees', array(
	4 => __('UBIC / Safe bet'),
	3 => __('Potential / To be challenged'),
	2 => __('Casual'),
	1 => __('Never tried'),
	0 => __('No interest')
));
Configure::write('Places.contactPreferences', array(
	'email' => __('Email'),
	'phone' => __('Phone'),
	'online_calendar' => __('Online calendar'),
	'form' => __('Form')
));
Configure::write('Places.ideal_for', array(
	'indoor_activity' => __('Indoor activity'),
	'outdoor_activity' => __('Outdoor activity'),
	'seminar' => __('Seminar'),
	'meal' => __('Meal'),
	'cocktail_dinner' => __('Cocktail dinner'),
	'urbangaming' => __('Urban Gaming')
));
Configure::write('Places.options', array(
	'free' => __('Free'),
	'confirmed' => __('Confirmed'),
	'prereserved' => __('Prereserved / Option'),
	'pending' => __('Pending'),
	'occupied' => __('Occupied'),
	'managed_by_client' => __('Managed by client'),
	'cancelled' => __('Cancelled'),
	'not_interested' => __('Not interested')
));
Configure::write('Places.options_for_calendar', array(
	'confirmed' => __('Confirmed'),
	'prereserved' => __('Prereserved / Option'),
	'pending' => __('Pending'),
	'occupied' => __('Occupied'),
	'cancelled' => __('Cancelled')
));
Configure::write('Users.roles', array('fixed' => __('Fixed'), 'temporary' => __('Temporary'), 'candidate' => __('Candidate'), 'retired' => __('Retired'), 'unsuitable' => __('Unsuitable')));
Configure::write('Users.civilstatus', array('single' => __('Single'), 'married' => __('Married'),'divorced' => __('Divorced'), 'separated' => __('Séparé'), 'widow' => __('Widow')));
Configure::write('Users.sizes', array(
	'S' => 'S',
	'M' => 'M',
	'L' => 'L',
	'XL' => 'XL',
));
Configure::write('Users.gender', array('F' => __('Woman'), 'M' => __('Man')));
Configure::write('Users.work_permits', array('b' => __('B (résident longue durée)'), 'l' => __('L (résident courte durée, pour une activité de moins d\'un an)'), 'c' => __('C (autorisation d\'établissement)'), 'g' => __('G (permis pour frontalier)')));
Configure::write('Users.availabilities', array(
	'no' => __('Not available'),
	'maybe' => __('Maybe available'),
	'yes' => __('Available'),
	'unknown' => __("Does not know")
));
Configure::write('Users.availabilitiesTagsByDay', array(
	'monday' 	=> array(105,107,108,110,111,113),
	'tuesday' 	=> array(114,116,117,119,120,122),
	'wednesday' => array(123,125,126,128,129,131),
	'thursday' 	=> array(132,134,135,137,138,140),
	'friday' 	=> array(141,143,144,146,147,149),
	'saturday' 	=> array(150,152,153,155,156,158),
	'sunday' 	=> array(159,161,162,164,165,167)
));
Configure::write('Users.licences', array(
	'C1' => 'C1',
	'D1' => 'D1',
	'C1E' => 'C1E',
	'forklift_operator' => __('Forklift operator')
));
Configure::write('Users.minimal_salary', 27.00);
Configure::write('Users.salary_for_fixed', 35.00);
Configure::write('Users.salary_for_trainee', 15.00);
Configure::write('Availabilities.days', array(
	1 => __('Monday'),
	2 => __('Tuesday'),
	3 => __('Wednesday'),
	4 => __('Thursday'),
	5 => __('Friday'),
	6 => __('Saturday'),
	7 => __('Sunday')
));
Configure::write('Hours.weights', array(
	6 => 0,
	7 => 1,
	8 => 2,
	9 => 3,
	10 => 4,
	11 => 5,
	12 => 6,
	13 => 7,
	14 => 8,
	15 => 9,
	16 => 10,
	17 => 11,
	18 => 12,
	19 => 13,
	20 => 14,
	21 => 15,
	22 => 16,
	23 => 17,
	0 => 18,
	1 => 19,
	2 => 20,
	3 => 21,
	4 => 22,
	5 => 23
));
Configure::write('Configurations.options', array(
	'free' => __('Free'),
	'confirmed' => __('Confirmed'),
	'prereserved' => __('Prereserved / Option'),
	'to_discuss' => __('To discuss'),
	'occupied' => __('Occupied'),
	'cancelled' => __('Cancelled'),
	'not_interested' => __('Not interested')
));
Configure::write('Documents.Images.allowedExtensions', 'png,jpg,jpeg,bmp,tiff,gif');
Configure::write('Documents.Documents.allowedExtensions', 'pdf,doc,docx,txt,xls,xlsx');
Configure::write('Documents.Images.groups', array(
	'indoor' => __('Indoor'),
	'outdoor' => __('Outdoor'),
	'placement' => __('Placement'),
	'client' => __('Client'),
	'misc_images' => __('Miscellaneous'),
	'undefined' => __('Undefined')
));
Configure::write('Documents.Documents.groups', array(
	'plans' => __('Plans'),
	'organisation' => __('Organisation'),
	'menus' => __('Menus'),
	'misc_documents' => __('Miscellaneous')
));
Configure::write('Events.status', array(
	'elaboration' => __('Offer'),
	'confirmed' => __('Confirmed'),
	'refused' => __('Refused'),
	'validation' => __('In process of validation'),
	'cancelled' => __('Cancelled'),
	'no_offer' => __('No issued offer')
));
Configure::write('Events.crm_status', array(
	'new' => __('New demand'),
	'in_progress' => __('In progress offer'),
	'delivered' => __('Delivered offer'),
	'to_adapt' => __('Offer to adapt'),
	'relaunched' => __('Offer to relaunch'),
	'pending' => __('Pending offer'),
	'confirmed' => __('Confirmed offer'),
	'done' => __('Validated and closed event'),
	'null' => __('Not valid / null offer'),
	'refused' => __('Refused offer'),
	'partner' => __('Transmitted to partner')
));
Configure::write('Missions.status', array(
	'confirmed' => __('Confirmed'),
	'done' => __('Validated and closed mission')
));
// for events index, default filters
Configure::write('Events.crm_status_default', array(
	'new' => __('New demand'),
	'in_progress' => __('In progress offer'),
	'delivered' => __('Delivered offer'),
	'to_adapt' => __('Offer to adapt'),
	'relaunched' => __('Offer to relaunch'),
	'pending' => __('Pending offer')
));
Configure::write('Events.activity_types', array(
	'entreprise' => __('Corporate outing'),
	'teambuilding' => __('Team building'),
	'event' => __('Event planning'),
	'other' => __('Other')
));
Configure::write('Events.eg_mg_types', array(
	'rent' => __('Rent'),
	'event' => __('Event catering')
));
Configure::write('Events.ubic_knowledge', array(
	'former_client' => __('Former client'),
	'google' => __('Google'),
	'word_of_mouth' => __('Word of mouth'),
	'former_contact' => __('Former client contact'),
	'former_demand' => __('Former demand'),
	'promotion' => __('Marketing promotion'),
	'partner' => __('Partner')
));
Configure::write('Events.contact_method', array(
	'form' => __('Form'),
	'mail' => __('Mail'),
	'phone' => __('Phone')
));
Configure::write('Events.colors', array(
	'#ECD078',
	'#D95B43',
	'#C02942',
	'#7E4C73',
	'#53777A',
	'#CFF09E',
	'#A8DBA8',
	'#79BD9A',
	'#3B8686',
	'#0B486B',
	'#556270',
	'#FF6B6B',
	'#C44D58',
	'#4BC0A1',
	'#D3CE3D',
	'#60B99A',
	'#C44D58',
	'#FD5D61',
	'#73626E',
	'#B38184'
));
Configure::write('Events.needs_analysis', array(
	'standard' => __('Standard'),
	'creation' => __('Events creation')
));
Configure::write('Checklists.models', array(
	'event' => __('Event'),
	'festiloc' => __('Festiloc'),
	'client' => __('Client'),
	'none' => __('None')
));
Configure::write('Notes.categories', array(
	'client' => __('Client'),
	'closing' => __('Closing'),
	'activities' => __('Activities'),
	'fb_modules' => __('F&B Modules'),
	'places' => __('Places'),
	'recruiting' => __('Recruiting'),
	'accounting' => __('Accounting'),
	'hours' => __('Hours'),
	'moments' => __('Moments'),
	'planning' => __('Planning'),
	'vehicles' => __('Vehicles')
));
Configure::write('Moments.types', array(
	'route' => __('Route'),
	'setting' => __('Setting up'),
	'activity' => __('Activity'),
	'staff' => __('Staff'),
	'fb' => __('F&B'),
	'client' => __('Client'),
  'client_arrival' => __('Client arrival'),
  'client_departure' => __('Client departure'),
  'mandate_start' => __("Mandate's beginning"),
  'mandate_end' => __('End of mandate'),
  'appointment' => __('Appointment'),
	'loading' => __('Loading'),
	'unloading' => __('Unloading'),
	'start' => __('Start'),
	'end' => __('End'),
	'car_sharing_start' => __('Car sharing start'),
	'break' => __('Break'),
	'tiding_up' => __('Tiding up'),
	'custom' => __('Customised')
));
Configure::write('PlanningBoardMoments.types', Configure::read('Moments.types'));
Configure::write('Languages.levels', array(
	'a1' => 'A1',
	'a2' => 'A2',
	'b1' => 'B1',
	'b2' => 'B2',
	'c1' => 'C1',
	'c2' => 'C2'
));
Configure::write('Competences.sectors', array(
	'animation' => __('Animation'),
	'fb' => __('F&B'),
	'logistics' => __('Logistics'),
	'other' => __('Other')
));
Configure::write('Competences.fb_jobs', array(
	'event_manager' => __('Event manager'),
	'event_assistant' => __('Event assistant'),
	'service_crew' => __('Service crew'),
	'test' => __('Test'),
	'deliverer' => __('Deliverer'),
	'griller' => __('Griller'),
	'bar_manager' => __('Bar manager'),
	'bar_crew' => __('Bar crew'),
	0 => '----- OBSOLETES! -----',
	'cook' => __('Cook'),
	'griller' => __('Griller'),
	'chinese_fondue' => __('Chinese fondue'),
	'fondue_kit' => __('Fondue kit'),
	'burger_party' => __('Burger Party'),
	'barbecue' => __('Barbecue'),
	'regional_picnic' => __('Regional picnic'),
	'meal_from_cauldron' => __('Meal from cauldron'),
	'land_sea_duo' => __('Land and sea duo'),
	'misc_meals' => __('Misc meals'),
	'mg_event_manager' => __('Event Manager MG'),
	'mg_event_assistant' => __('Event assistant MG'),
	'mg_service_crew' => __('Service crew MG'),
	'mg_deliverer' => __('Deliverer MG')
));
Configure::write('Competences.logistics_jobs', array(
	'driver' => __('Driver'),
	'fl_logistician' => __('Logistician FL'),
	'mg_logistician' => __('Logistician MG'),
	'ubic_logistician' => __('Logistician UBIC'),
	'fl_deliveryman' => __('Deliveryman FL'),
	//'mg_deliveryman' => __('Deliveryman MG'),
	'sound_light_system' => __('Sound and light system'),
	'dishwasher' => __('Dishwasher'),
	'handler' => __('Handler'),
));
Configure::write('Competences.animation_jobs', array(
	'animator' => __('Animator'),
	'animator_facilitator' => __('Animator facilitator'),
	//'presenter' => __('Presenter'),
	//'presenter_facilitator' => __('Presenter facilitator'),
	'facilitator' => __('Facilitator'),
	'coach' => __('Coach'),
	'urban_leader' => __('Urban Leader'),
	'urban_coach' => __('Urban Coach'),
	// 'coordinator' => __('Coordinator'),
	// 'coordinator_facilitator' => __('Coordinator facilitator')
));
Configure::write('Competences.other_jobs', array(
	'coordinator' => __('Coordinator'),
	'cook' => __('Cook'),
	'host' => __('Host / Hostess'),
	//'griller' => __('Griller'),
	'presenter' => __('Presenter'),
	'coat_check_staff' => __('Coat-check staff'),
	'photograph' => __('Photograph')
));
Configure::write('Competences.hierarchies', array(
	4 => 'MPP',
	3 => 'EMDO',
	2 => 'Basique',
	1 => 'Test',
	0 => __('Unsuitable')
));
Configure::write('Competences.jobs', array_merge(Configure::read('Competences.animation_jobs'), Configure::read('Competences.logistics_jobs'), Configure::read('Competences.fb_jobs')));
Configure::write('Jobs.languages', array(
	'fr' => __('French'),
	'de' => __('German'),
	'en' => __('English'),
	'it' => __('Italian')
));
Configure::write('MaintenanceMode', array(
    'enabled' => false,
    'view' =>   array(
        'layout' => 'maintenance',
        'template' => 'MaintenanceMode/index'
    ),
    'ip_filters' => array('178.197.224.186', '172.20.10.4', 'localhost')
));
Configure::write('Stockitems.status', array(
	'ok' => __('Available'),
	'maintenance' => __('In maintenance'),
	'out_of_order' => __('Out of order'),
	'archived' => __('Archived')
));
Configure::write('StockItems.checkStatuses', array(
	'unknown' => __('Unknown'),
	'to_check' => __('To check / complete'),
	'complete' => __('Complete')
));
Configure::write('StockItems.voltages', array(
	140 => __('140 V'),
	400 => __('400 V')
));
Configure::write('StockItems.electrical_outlets', array(
	'euro_16a' => __('Euro 16A'),
	'euro_32a' => __('Euro 32A'),
	'euro_63a' => __('Euro 63A'),
	't12_13a' => __('T12 13A'),
	't15_16a' => __('T15 16A')
));
Configure::write('StockItems.import_images_dir', 'files/import/images');
Configure::write('StockOrders.status', array(
	'offer' => __('Offer'),
	'confirmed' => __('Confirmed'),
	'to_proceed' => __('To proceed'),
	'in_progress' => __('In progress'),
	'processed' => __('Processed / Ready to deliver'),
	'delivered' => __('Delivered'),
	'to_invoice' => __('To invoice'),
	'invoiced' => __('Invoiced'),
	'cancelled' => __('Cancelled')
));
Configure::write('StockOrders.status_for_depot', array(
	'confirmed' => __('Confirmed'),
	'in_progress' => __('In progress'),
	'processed' => __('Processed / Ready to deliver'),
	'delivered' => __('Delivered')
));
Configure::write('StockOrders.delivery_modes', array(
	'client' => __('On the spot by the client'),
	'festiloc' => __('Festiloc'),
	'logista' => __('Logista'),
	'zumwald' => __('Zumwald')
));
Configure::write('StockOrders.return_modes', array(
	'client' => __('Client'),
	'festiloc' => __('Festiloc'),
	'logista' => __('Logista'),
	'zumwald' => __('Zumwald')
));
Configure::write('StockOrders.delivery_moments', array(
	'day' => __('During day'),
	'morning' => __('Morning'),
	'afternoon' => __('Afternoon'),
	'hour' => __('Specified time')
));
Configure::write('StockOrders.return_moments', array(
	'day' => __('During day'),
	'morning' => __('Morning'),
	'afternoon' => __('Afternoon'),
	'hour' => __('Specified time')
));
Configure::write('StockOrders.transporters', array(
	'festiloc' => __('Festiloc'),
	'logista' => __('Logista'),
	'zumwald' => __('Zumwald'),
	'other' => __('Other')
));
Configure::write('StockOrders.fields', array(
	'name' => __('Name'),
	'delivery_address' => __('Address'),
	'delivery_zip' => __('ZIP'),
	'delivery_zip_city' => __('ZIP / City'),
	'delivery_city' => __('City'),
	'delivery_mode' => __('Delivery mode'),
	'delivery_place_id' => __('Place'),
	'return_address' => __('Address'),
	'return_place_id' => __('Place'),
	'return_zip' => __('ZIP'),
	'return_zip_city' => __('ZIP / City'),
	'return_city' => __('City'),
	'return_mode' => __('Return mode'),
	'return_place_id' => __('Return place'),
));
Configure::write('StockOrders.cancellation_reasons', array(
	'cancelled_event' => __('Cancelled event'),
	'competition' => __('Competition'),
	'expensive_stockitems' => __('Stock items too expensive'),
	'expensive_delivery_costs' => __('Delivery costs too expensive'),
	'demand_not_met' => __('Demand is not met'),
	'none' => __('No reason')
));
Configure::write('StockOrders.export_status', array(
	'not_exported' => __('Not exported'),
	'to_export' => __('To export'),
	'exported' => __('Exported')
));
Configure::write('StockOrders.types', array(
	'rental' => __('Rental'),
	'sale' => __('Sale'),
	'delivery' => __('Delivery'),
	'withdrawal' => __('Withdrawal')
));
Configure::write('StockOrders.ignored_fields', array(
	'status',
	'client_id',
	'contact_people_id',
	'delivery_contact_people_id',
	'return_contact_people_id',
	'total_ht',
	'fidelity_discount_amount',
	'tva',
	'net_total',
	'modified',
	'created',
	'number_of_products',
	'id',
	'import_id',
	'zip',
	'city',
	'packaging_costs',
	'order_number',
	'version_id_read',
	'forced_net_total',
	'quantity_discount',
	'distance_covered',
	'fidelity_discount',
	'quantity_discount_percentage',
	'delivery_distance',
	'invoice_email',
	'return',
	'delivery',
	'controlled',
	'delivery_transportation_costs',
	'return_transportation_costs',
	'forced_delivery_costs',
	'delivery_costs',
	'fidelity_discount_percentage',
	'extrahours_costs',
	'number_of_pallets1',
	'number_of_rollis1',
	'xl_surcharge1',
	'delivery_transporter_id',
	'return_transporter_id',
	'controlled_by',
	'notify_cancellation',
	'first_name',
	'last_name',
	'full_name',
	'full_name_phone_email',
	'deposit',
	'delivery_tour_id',
	'return_tour_id',
	'invoiced_total',
	'cancellation_reason',
	'cancellation_remarks',
	'number_of_pallets_xl',
	'number_of_pallets_xl1',
	'number_of_rollis_xl',
	'number_of_rollis_xl1',
	'xl_surcharge',
	'delivery_return_same',
	'festiloc_fidelity_discount',
	'notify_depot',
	'user_id',
	'to_check',
	'uuid',
	'alley',
	'position',
	'company_id',
	'number_of_items_per_container',
	'stock_storage_type_id',
	'stock_storage_amount'
));
Configure::write('StockItemUnavailability.statuses', array(
	'potential' => __('Potential'),
	'confirmed' => __('Confirmed'),
	'reconditionning' => __('Reconditionning'),
	'reparation' => __('Reparation'),
	'other' => __('Other')
));
Configure::write('ContactPeople.civilities', array(
	'ms' => __('Ms'),
	'mr' => __('Mr'),
	'ms_mr' => __('Ms and Mr'),
	'dr' => __('Dr'),
));
Configure::write('ContactPeople.civilities1', array(
	'ms' => __('Miss'),
	'mr' => __('Mister'),
	'ms_mr' => __('Miss and Mister'),
	'dr' => __('Doctor')
));
Configure::write('ContactPeople.languages', array(
	'fr' => __('French'),
	'de' => __('German'),
	'en' => __('English'),
	'it' => __('Italian')
));
Configure::write('Communes.cantons', array(
	'AG' => 'AG',
	'AI' => 'AI',
	'AR' => 'AR',
	'BE' => 'BE',
	'BL' => 'BL',
	'BS' => 'BS',
	'FR' => 'FR',
	'GE' => 'GE',
	'GL' => 'GL',
	'GR' => 'GR',
	'JU' => 'JU',
	'LU' => 'LU',
	'NE' => 'NE',
	'NW' => 'NW',
	'OW' => 'OW',
	'SG' => 'SG',
	'SH' => 'SH',
	'SO' => 'SO',
	'SZ' => 'SZ',
	'TG' => 'TG',
	'TI' => 'TI',
	'UR' => 'UR',
	'VD' => 'VD',
	'VS' => 'VS',
	'ZG' => 'ZG',
	'ZH' => 'ZH'
));
Configure::write('Vehicles.families', array(
	'truck' => __('Truck'),
	'moving_van' => __('Moving van raising plate'),
	'van_big' => __('Van big'),
	'van_big_raising_plate' => __('Van big raising plate'),
	'van_small' => __('Van small'),
	'car' => __('Car'),
	'other' => __('Other')

));
Configure::write('Trailers.families', array(
	'trailer_plain' => __('Trailer plain'),
	'trailer_cold' => __('Trailer cold'),
	'trailer_plate' => __('Trailer with plate')
));
Configure::write('VehiclesTrailers.families', array_merge(Configure::read('Vehicles.families'), Configure::read('Trailers.families')));
Configure::write('Vehicles.types', array(
	'vehicle' => __('Vehicle'),
	'trailer' => __('Trailer')
));
Configure::write('Vehicles.fuel', array(
	'unleaded' => __('Unleaded petrol'),
	'diesel' => __('Diesel')
));
Configure::write('VehicleReservations.reasons', array(
	'reparation' => __('Reparation'),
	'private' => __('Private'),
	'other' => __('Other')
));
Configure::write('VehicleTours.locations', array(
	'festiloc' => __('Festiloc'),
	'ubic' => __('UBIC'),
	'easyloc' => __('Easyloc'),
	'zumwald' => __('Zumwald'),
	'other' => __('Other')
));
Configure::write('VehicleTourMoments.types', array('break' => __('Break'), 'loading' => __('Loading'), 'unloading' => __('Unloading'), 'free' => __('Custom field')));
Configure::write('CakePdf', array(
  'engine' => 'CakePdf.WkHtmlToPdf',
  'orientation' => 'portrait',
	'encoding' => 'UTF-8',
   'download' => true
));
Configure::write('StockStorageTypes.supports', array('none' => __('None'), 'pallet' => __('Pallet'), 'rolli' => __('Rolli')));
Configure::write('Countries', array(
	'AF' => __('Afghanistan'),
	'AX' => __('Aland Islands'),
	'AL' => __('Albania'),
	'DZ' => __('Algeria'),
	'AS' => __('American Samoa'),
	'AD' => __('Andorra'),
	'AO' => __('Angola'),
	'AI' => __('Anguilla'),
	'AQ' => __('Antarctica'),
	'AG' => __('Antigua And Barbuda'),
	'AR' => __('Argentina'),
	'AM' => __('Armenia'),
	'AW' => __('Aruba'),
	'AU' => __('Australia'),
	'AT' => __('Austria'),
	'AZ' => __('Azerbaijan'),
	'BS' => __('Bahamas'),
	'BH' => __('Bahrain'),
	'BD' => __('Bangladesh'),
	'BB' => __('Barbados'),
	'BY' => __('Belarus'),
	'BE' => __('Belgium'),
	'BZ' => __('Belize'),
	'BJ' => __('Benin'),
	'BM' => __('Bermuda'),
	'BT' => __('Bhutan'),
	'BO' => __('Bolivia'),
	'BA' => __('Bosnia And Herzegovina'),
	'BW' => __('Botswana'),
	'BV' => __('Bouvet Island'),
	'BR' => __('Brazil'),
	'IO' => __('British Indian Ocean Territory'),
	'BN' => __('Brunei Darussalam'),
	'BG' => __('Bulgaria'),
	'BF' => __('Burkina Faso'),
	'BI' => __('Burundi'),
	'KH' => __('Cambodia'),
	'CM' => __('Cameroon'),
	'CA' => __('Canada'),
	'CV' => __('Cape Verde'),
	'KY' => __('Cayman Islands'),
	'CF' => __('Central African Republic'),
	'TD' => __('Chad'),
	'CL' => __('Chile'),
	'CN' => __('China'),
	'CX' => __('Christmas Island'),
	'CC' => __('Cocos (Keeling) Islands'),
	'CO' => __('Colombia'),
	'KM' => __('Comoros'),
	'CG' => __('Congo'),
	'CD' => __('Congo, Democratic Republic'),
	'CK' => __('Cook Islands'),
	'CR' => __('Costa Rica'),
	'CI' => __('Cote D\'Ivoire'),
	'HR' => __('Croatia'),
	'CU' => __('Cuba'),
	'CY' => __('Cyprus'),
	'CZ' => __('Czech Republic'),
	'DK' => __('Denmark'),
	'DJ' => __('Djibouti'),
	'DM' => __('Dominica'),
	'DO' => __('Dominican Republic'),
	'EC' => __('Ecuador'),
	'EG' => __('Egypt'),
	'SV' => __('El Salvador'),
	'GQ' => __('Equatorial Guinea'),
	'ER' => __('Eritrea'),
	'EE' => __('Estonia'),
	'ET' => __('Ethiopia'),
	'FK' => __('Falkland Islands (Malvinas)'),
	'FO' => __('Faroe Islands'),
	'FJ' => __('Fiji'),
	'FI' => __('Finland'),
	'FR' => __('France'),
	'GF' => __('French Guiana'),
	'PF' => __('French Polynesia'),
	'TF' => __('French Southern Territories'),
	'GA' => __('Gabon'),
	'GM' => __('Gambia'),
	'GE' => __('Georgia'),
	'DE' => __('Germany'),
	'GH' => __('Ghana'),
	'GI' => __('Gibraltar'),
	'GR' => __('Greece'),
	'GL' => __('Greenland'),
	'GD' => __('Grenada'),
	'GP' => __('Guadeloupe'),
	'GU' => __('Guam'),
	'GT' => __('Guatemala'),
	'GG' => __('Guernsey'),
	'GN' => __('Guinea'),
	'GW' => __('Guinea-Bissau'),
	'GY' => __('Guyana'),
	'HT' => __('Haiti'),
	'HM' => __('Heard Island & Mcdonald Islands'),
	'VA' => __('Holy See (Vatican City State)'),
	'HN' => __('Honduras'),
	'HK' => __('Hong Kong'),
	'HU' => __('Hungary'),
	'IS' => __('Iceland'),
	'IN' => __('India'),
	'ID' => __('Indonesia'),
	'IR' => __('Iran, Islamic Republic Of'),
	'IQ' => __('Iraq'),
	'IE' => __('Ireland'),
	'IM' => __('Isle Of Man'),
	'IL' => __('Israel'),
	'IT' => __('Italy'),
	'JM' => __('Jamaica'),
	'JP' => __('Japan'),
	'JE' => __('Jersey'),
	'JO' => __('Jordan'),
	'KZ' => __('Kazakhstan'),
	'KE' => __('Kenya'),
	'KI' => __('Kiribati'),
	'KR' => __('Korea'),
	'KW' => __('Kuwait'),
	'KG' => __('Kyrgyzstan'),
	'LA' => __('Lao People\'s Democratic Republic'),
	'LV' => __('Latvia'),
	'LB' => __('Lebanon'),
	'LS' => __('Lesotho'),
	'LR' => __('Liberia'),
	'LY' => __('Libyan Arab Jamahiriya'),
	'LI' => __('Liechtenstein'),
	'LT' => __('Lithuania'),
	'LU' => __('Luxembourg'),
	'MO' => __('Macao'),
	'MK' => __('Macedonia'),
	'MG' => __('Madagascar'),
	'MW' => __('Malawi'),
	'MY' => __('Malaysia'),
	'MV' => __('Maldives'),
	'ML' => __('Mali'),
	'MT' => __('Malta'),
	'MH' => __('Marshall Islands'),
	'MQ' => __('Martinique'),
	'MR' => __('Mauritania'),
	'MU' => __('Mauritius'),
	'YT' => __('Mayotte'),
	'MX' => __('Mexico'),
	'FM' => __('Micronesia, Federated States Of'),
	'MD' => __('Moldova'),
	'MC' => __('Monaco'),
	'MN' => __('Mongolia'),
	'ME' => __('Montenegro'),
	'MS' => __('Montserrat'),
	'MA' => __('Morocco'),
	'MZ' => __('Mozambique'),
	'MM' => __('Myanmar'),
	'NA' => __('Namibia'),
	'NR' => __('Nauru'),
	'NP' => __('Nepal'),
	'NL' => __('Netherlands'),
	'AN' => __('Netherlands Antilles'),
	'NC' => __('New Caledonia'),
	'NZ' => __('New Zealand'),
	'NI' => __('Nicaragua'),
	'NE' => __('Niger'),
	'NG' => __('Nigeria'),
	'NU' => __('Niue'),
	'NF' => __('Norfolk Island'),
	'MP' => __('Northern Mariana Islands'),
	'NO' => __('Norway'),
	'OM' => __('Oman'),
	'PK' => __('Pakistan'),
	'PW' => __('Palau'),
	'PS' => __('Palestinian Territory, Occupied'),
	'PA' => __('Panama'),
	'PG' => __('Papua New Guinea'),
	'PY' => __('Paraguay'),
	'PE' => __('Peru'),
	'PH' => __('Philippines'),
	'PN' => __('Pitcairn'),
	'PL' => __('Poland'),
	'PT' => __('Portugal'),
	'PR' => __('Puerto Rico'),
	'QA' => __('Qatar'),
	'RE' => __('Reunion'),
	'RO' => __('Romania'),
	'RU' => __('Russian Federation'),
	'RW' => __('Rwanda'),
	'BL' => __('Saint Barthelemy'),
	'SH' => __('Saint Helena'),
	'KN' => __('Saint Kitts And Nevis'),
	'LC' => __('Saint Lucia'),
	'MF' => __('Saint Martin'),
	'PM' => __('Saint Pierre And Miquelon'),
	'VC' => __('Saint Vincent And Grenadines'),
	'WS' => __('Samoa'),
	'SM' => __('San Marino'),
	'ST' => __('Sao Tome And Principe'),
	'SA' => __('Saudi Arabia'),
	'SN' => __('Senegal'),
	'RS' => __('Serbia'),
	'SC' => __('Seychelles'),
	'SL' => __('Sierra Leone'),
	'SG' => __('Singapore'),
	'SK' => __('Slovakia'),
	'SI' => __('Slovenia'),
	'SB' => __('Solomon Islands'),
	'SO' => __('Somalia'),
	'ZA' => __('South Africa'),
	'GS' => __('South Georgia And Sandwich Isl.'),
	'ES' => __('Spain'),
	'LK' => __('Sri Lanka'),
	'SD' => __('Sudan'),
	'SR' => __('Suriname'),
	'SJ' => __('Svalbard And Jan Mayen'),
	'SZ' => __('Swaziland'),
	'SE' => __('Sweden'),
	'CH' => __('Switzerland'),
	'SY' => __('Syrian Arab Republic'),
	'TW' => __('Taiwan'),
	'TJ' => __('Tajikistan'),
	'TZ' => __('Tanzania'),
	'TH' => __('Thailand'),
	'TL' => __('Timor-Leste'),
	'TG' => __('Togo'),
	'TK' => __('Tokelau'),
	'TO' => __('Tonga'),
	'TT' => __('Trinidad And Tobago'),
	'TN' => __('Tunisia'),
	'TR' => __('Turkey'),
	'TM' => __('Turkmenistan'),
	'TC' => __('Turks And Caicos Islands'),
	'TV' => __('Tuvalu'),
	'UG' => __('Uganda'),
	'UA' => __('Ukraine'),
	'AE' => __('United Arab Emirates'),
	'GB' => __('United Kingdom'),
	'US' => __('United States'),
	'UM' => __('United States Outlying Islands'),
	'UY' => __('Uruguay'),
	'UZ' => __('Uzbekistan'),
	'VU' => __('Vanuatu'),
	'VE' => __('Venezuela'),
	'VN' => __('Viet Nam'),
	'VG' => __('Virgin Islands, British'),
	'VI' => __('Virgin Islands, U.S.'),
	'WF' => __('Wallis And Futuna'),
	'EH' => __('Western Sahara'),
	'YE' => __('Yemen'),
	'ZM' => __('Zambia'),
	'ZW' => __('Zimbabwe')
));
Configure::write('Menus.missing_translations', array(
	__('See all'),
	__('Sales'),
	__('Add'),
	__('Common resources'),
	__('All stock items'),
	__('All stock orders'),
	__('All clients'),
	__('Tools'),
	__('Depot'),
	__('Companies'),
	__('Models'),
	__('Vehicles / Trailers'),
	__('Needs / Reservations'),
	__('Tasks calendar'),
	__('Order models'),
	__('Offers during month of %s'),
	__('Accepted offers during month of %s'),
	__('Null offers during month of %s'),
	__('Refused offers during month of %s'),
	__('Sent offers during month of %s'),
	__('Offers in progress during month of %s'),
	__('Offers during month of %s - %s'),
	__('Accepted offers during month of %s - %s'),
	__('Null offers during month of %s - %s'),
	__('Refused offers during month of %s - %s'),
	__('Sent offers during month of %s - %s'),
	__('Offers in progress during month of %s - %s'),
	__('Repartition of month %s - %s'),
	__('Missions calendar'),
	__('January'),
	__('February'),
	__('March'),
	__('April'),
	__('May'),
	__('June'),
	__('July'),
	__('August'),
	__('September'),
	__('October'),
	__('November'),
	__('December'),
	__('Evaluation of competences'),
  __('invoice_method'),
  __('payment_method')
));
Configure::write('Orders.quantity_options', array(
	'fixed' => __('Fixed'),
	'pieces_per_person' => __('Number of pieces per person'),
	'pieces_per_group' => __('Number of pieces per group'),
	'pieces_per_animator' => __('Number of pieces per animator'),
	'pieces_per_team' => __('Number of pieces per team'),
	'pieces_per_staff' => __('Number of pieces per staff'),
	'pieces_per_person' => __('Number of pieces per person'),
	'pieces_per_person_per_team' => __('Number of pieces per person per team'),
	'pieces_per_table' => __('Number of pieces per table'),
	'pieces_per_buffet' => __('Number of pieces per buffet'),
	'quantity_per_person' => __('Quantity per person'),
	'interval' => __('Interval'),
	'clothing_size' => __('Number of pieces per clothing size')
));
Configure::write('Orders.quantity_units', array(
	'g' => __('g'),
	'cl' => __('cl'),
	'pce' => __('pce')
));
Configure::write('Regions.types', array(
	'city' => __('City'),
	'district' => __('District'),
	'canton' => __('Canton'),
	'region' => __('Region')
));
Configure::write('Outfits.types', array(
	'default' => __('Default'),
	'activity' => __('Activity'),
	'fb_module' => __('Module F&B'),
	'client' => __('Client')
));
Configure::write('PlanningBoards.types', array(
	'general' => __('General'),
	'logistics' => __('Logistics'),
  'animation' => __('Animation'),
  'fb' => __('F&B'),
	'setting_up' => __('Setting up'),
  'billboard' => __('Billboard'),
  'practical_info' => __('Practical info'),
  'custom' => __('Customised')
));
Configure::write('StockOrders.invoice_methods', array(
	'invoice' => __('Invoice'),
	'deposit' => __('Deposit'),
	'on_site' => __('On site payment')
));
Configure::write('StockOrders.payment_methods', array(
	'cash' => __('Cash'),
	'credit_card' => __('Credit card'),
	'debit_card' => __('Debit card')
));
Configure::write('Reporting.balanced_sell_rate', 0.37);
Configure::write('Reporting.balanced_sell_rate1', 0.25);
Configure::write('Reporting.balanced_sell_rate_festiloc', 0.5);
Configure::write('Reporting.minimal_sell_rate', 45);
setlocale(LC_ALL, 'fra');
ini_set('memory_limit', '1024M');

CakeLog::config('myubic', array(
  'engine' => 'FileLog',
  'path' => APP . '/tmp/logs/'
));
Configure::write('Debug.server', 'local');
if($_SERVER['SERVER_NAME'] == 'localhost'){
	Configure::write('Debug.server', 'local');
}
if($_SERVER['SERVER_NAME'] == 'test.myubic.ch'){
	Configure::write('Debug.server', 'test');
}
if($_SERVER['SERVER_NAME'] == 'www.myubic.ch'){
	Configure::write('Debug.server', 'prod');
}
